#ifndef VISUALIZATIONHANDLER_H
#define VISUALIZATIONHANDLER_H

#include <QObject>
#include <iterator>
#include <map>

#include "./Model3dHandler.h"
#include "./CanvasHandler.h"

#include "./viz3d/VtkFboItem.h"

#include "utils/custom_color_map.h"
#include "object_tree/Domain.h"
#include "object_tree/Region.h"
#include "object_tree/SurfaceItem.h"
#include "Constants.h"
#include "./viz3d/CommandProcessor.h"

#include "cmake_configure_git_version.hpp"

class VisualizationHandler:public QObject 
{
    Q_OBJECT

      Q_PROPERTY(QString currentGitHash READ getCurrentGitHash);
  
    Q_PROPERTY(bool showSaveFileDialog MEMBER m_showSaveFileDialog NOTIFY showSaveFileDialogChanged);
    Q_PROPERTY(bool showOpenFileDialog MEMBER m_showOpenFileDialog NOTIFY showOpenFileDialogChanged);

    Q_PROPERTY(bool stratigraphic_visibility MEMBER m_stratigraphy_visibility WRITE setStratigraphicVisibility NOTIFY notifyVisibilityChanged);
    Q_PROPERTY(bool structural_visibility MEMBER m_structural_visibility WRITE setStructuralVisibility NOTIFY notifyVisibilityChanged);
    Q_PROPERTY(bool region_visibility MEMBER m_region_visibility WRITE setRegionVisibility NOTIFY notifyVisibilityChanged);
    Q_PROPERTY(bool domain_visibility MEMBER m_domain_visibility WRITE setDomainVisibility NOTIFY notifyVisibilityChanged);

    //Q_PROPERTY(double veFactor MEMBER m_VE_factor WRITE setVeFactor)
    Q_PROPERTY(double veFactor MEMBER m_VE_factor NOTIFY veFactorChanged)

    Q_PROPERTY(bool flat_mode MEMBER m_flat_mode NOTIFY flatModeChanged)

    Q_PROPERTY(bool tree_open MEMBER m_tree_open NOTIFY treeOpenFlagChanged)

    Q_PROPERTY(bool viz_flag MEMBER m_viz NOTIFY vizFlagChanged)

public:
    /**
    * Method that generates the single instance
    */
    static VisualizationHandler* getInstance(QQmlEngine*, VtkFboItem* vtkFboItem, CanvasHandler* canvasHandler, std::shared_ptr<CommandProcessor> commandProcessor);

    /**
    * Destructor.
    */
    ~VisualizationHandler();

    /**
    * Method to load objects from a loaded model inside the data structure
    * return void
    */
    void initData();

    /**
    * Method to return surfaceList
    * @return std::vector<ObjectIdentifier>
    */
    std::vector < std::shared_ptr<SurfaceItem>> getSurfaceList();

    /**
    * Method to return regionList
    * @return std::vector< std::shared_ptr<Region> >
    */
    std::vector< std::shared_ptr<Region> > getRegionList();
    
    /**
    * Method to return domainList
    * @return std::vector< std::shared_ptr<Domain> >
    */
    std::vector< std::shared_ptr<Domain> > getDomainList();

    /**
    * Method to turn on/off the visibility of an object
    * @param index of the object
    * @param checkState of the visibility checkbox
    * @return void
    */
    Q_INVOKABLE void changeObjectVisibility(int index, int parentTypeId, int checkState);

    /**
    * Method to turn on/off the visibility of object groups
    * @param parentTypeId of the object group
    * @param checkState of the (visibility) group checkbox
    * @return void
    */
    Q_INVOKABLE void setItemVisibilityAll(int parentTypeId, int checkState);

    /**
    * Method to return the color map
    * @return vector<QString>
    */
    Q_INVOKABLE std::vector<QString> getColorList();

    /**
    * Method to return the surface color map
    * @return vector<QString>
    */
    Q_INVOKABLE std::vector<QString> getSurfaceColorList();

    /**
    * Method to return the surface color map
    * @return vector<QString>
    */
    Q_INVOKABLE std::vector<QString> getRegionColorList();

    /**
    * Method to return the visibility map
    * @return vector<QString>
    */
    Q_INVOKABLE std::vector<bool> getVisibilityList();

    /**
    * Method that reset VisualizationHandler properties to its initial state.
    * @return void
    */
    //Q_INVOKABLE void reset();

    /**
    * Method that sets the value of the VizFlag
    * This is an invokable method that may be called from QML
    * @param bool value
    * @return void
    */
    Q_INVOKABLE void setVizFlag(bool value);

    /**
    * Method that returns the value of the VizFlag
    * This is an invokable method that may be called from QML
    * @return bool
    */
    Q_INVOKABLE bool getVizFlag();

    /**
    * Method that saves 3D model and associated data objects
    * This is an invokable method that may be called from QML
    * @param const QUrl& path
    * @return std::string
    */
    Q_INVOKABLE std::string getAbsPath(const QUrl& path);

    /**
    * Method that saves 3D model and associated data objects
    * This is an invokable method that may be called from QML
    * @param const QUrl& path
    * @return QUrl
    */
    Q_INVOKABLE QUrl getLocalPath(const QUrl& path);

    /**
    * Method that loads 3D model and associated data objects
    * This is an invokable method that may be called from QML
    * @param const QUrl& path
    * @return void
    */
    Q_INVOKABLE void loadModel(const QUrl& path);

    /**
    * Method that changes the dimension of the 3D bounding box
    * This is an invokable method that may be called from QML
    * @return void
    */
    Q_INVOKABLE void changeDimension();

    /**
    * Method that export sketches to the iRAP Grid
    * This is an invokable method that may be called from QML
    * @return bool
    */
    Q_INVOKABLE bool exportToIRAPgrid();

    /**
    * Method that initiates surface generation using the curve drawn on the Canvas
    * This is an invokable method that gets called from QML
    * @return void
    */
    Q_INVOKABLE void addSurfacesFromSBIM() const;

    /**
    * Method that captures the object tree drawer open or closed signal
    * This is an invokable method that may be called from QML
    * @return void
    */
    Q_INVOKABLE void updateMainWindowSize(bool shrink, int position);

    /**
    * Method that sets cross section number
    * This is an invokable method that gets called from QML
    * @return void
    */
    Q_INVOKABLE void setCrossSection(const double crossSectionNumber);

    /**
    * Method that sets the cross section plan orientation
    * This is an invokable method that gets called from QML
    * @return void
    */
    //Q_INVOKABLE void setPlaneOrientation(const int planeOrientation);
    Q_INVOKABLE void setPlaneOrientation(const int planeOrientation, int csID);
    

    /**
    * Method that sets the source region
    * This is an invokable method that gets called from QML
    * @return void
    */
    Q_INVOKABLE void setSourceRegion(int sourceRegion, int sourceParentTypeId);
    
    void removeRegionFromDomainWithoutUpdatingView(int regionId, int domainId);

    /**
    * Method that sets the source domain from which a region needs to be removed
    * This is an invokable method that gets called from QML
    * @return void
    */
    Q_INVOKABLE void setRegionForRemovalFromDomain(int sourceDomain, int sourceParentTypeId,int regionId);

    /**
    * Method that sets the destintation domain
    * This is an invokable method that gets called from QML
    * @return void
    */
    Q_INVOKABLE void setDestinationDomain(int destinationDomain);

    /**
    * Method that determines the new domain id based existing highest domain id
    * @return void
    */
    int determineNewDomainId();

    /**
    * Method that creates a domain first then sets it as the destintation domain
    * This is an invokable method that gets called from QML
    * @return void
    */
    Q_INVOKABLE void createAndsetDestinationDomain();

    /**
    * Method that adds a region to a domain
    * @return void
    */
    //Q_INVOKABLE void dropToDomain(int destinationDomain);
    Q_INVOKABLE void dropToDomain();

    /**
    * Method that returns the preview status
    * @return bool
    */
    Q_INVOKABLE bool getPreviewFlag();

    /**
    * Method that saves SketchData' to disk
    * @param fileUrl
    * @return void
    */
    Q_INVOKABLE void saveSbimData(QUrl fileUrl);

    /**
    * Method that loads SbimData from disk
    * @param fileUrl
    * @return void
    */
    Q_INVOKABLE void loadSbimData(QUrl fileUrl);

    /**
    * Method that returns domain color
    * @param domain id
    * @return QString
    */
    QString getDomainColorbyId(int domainId);

    /**
    * Method that updates the list of domains from the model
    * @return void
    */
    void updateDomainListFromModel();

    /**
    * Helper method for debugging
    * @param vector input
    * @return void
    */
    void printVector(std::vector<int> const& input);
    
    /**
    * Helper method for debugging
    * @param vector input
    * @return void
    */
    void printVector(std::vector<double> const& input);

    /**
    * Helper method for accessing the flat_mode
    * @return bool
    */
    bool isFlattening();

    /**
    * Method to return the surface count
    * @return int
    */
    Q_INVOKABLE int getSurfaceCount();

    /**
    * Method to return the surface 
    * @param surface id
    * @return SurfaceItem
    */
    std::shared_ptr<SurfaceItem> getSurfaceById(int id);

signals:
    void surfaceMapUpdated(std::vector<QString> colorMap, std::vector<bool> visibilityMap);
    
    void refreshSubmittedCurvesAfterUndoRedo(std::vector<QString> colorMap, std::vector<bool> visibilityMap);
    
    //Handling Domain Viz
    void newDomainColorReady(std::vector<QString> colorMap);
    void newDomainCreated(QString domainName, QString domainDescription);
    void domainListUpdated(std::vector< std::shared_ptr<Domain> > domains);
    void domainNameListEdited(std::vector< std::shared_ptr<Domain> > domains);
    void domainDeleted(int index);
    
    //Handling Region Viz 
    void clearRegionCurveBox();
    void regionsAdded(std::vector< std::shared_ptr<Region> >);
    void regionsRemoved();
    void regionListUpdated(std::vector< std::shared_ptr<Region> > regions);
    void regionsDisplayed();

    //Handling Object details form data
    void itemDescriptionUpdated(QString itemPrefix);
    void duplicateItemNameDetected(QString itemPrefix);
    void duplicateRegionDropDetected(QString source, QString destination);

    //Handling Viz on the TreeView
    void surfaceListUpdated(std::vector< std::shared_ptr<SurfaceItem> > surfaces);
    void surfaceColorUpdated(int id, int type_id, QString color);
    void surfaceVisibilityUpdated(int id, int type_id, bool flag);

    void surfaceColorListUpdated(std::vector< std::shared_ptr<SurfaceItem> > surfaces);
    void surfaceVisibilityListUpdated(std::vector< std::shared_ptr<SurfaceItem> > surfaces);
    void parentGroupVisibilityUpdated(int groupId, bool visibility);
    void regionColorUpdated(int id, int type_id, QString color);
    void regionColorListUpdated(std::vector< std::shared_ptr<Region> > regions);
    
    //Reset Visualization
    void resetViz();

    //Reset New Signal
    void openDimensionDialog();

    //File Dialog
    void showSaveFileDialogChanged();
    void showOpenFileDialogChanged();

    void resizeMainWindow(bool shrink);

    //Changing cross-section#
    void crossSectionChanged(int crossSectionNumber);

    //Changing cross-section plane orientation
    void planeOrientationChanged(int planeOrientation);

    //Region visibility changes
    void regionVisibilityListUpdated(std::vector< std::shared_ptr<Region> > regions);
    void regionMapUpdated(std::vector<QString> regionColorMap, std::vector<bool> regionVisibilityMap);

    //Domain visibility change
    void domainVisibilityListUpdated(std::vector< std::shared_ptr<Domain> > domains);
    void domainMapUpdated(QString domainMap);
    
    void notifyVisibilityChanged();

    //Signals for dropToDomain
    void dropToDomainReady();
    void dropToDomainNotAllowed();

    //Signal to display Preview
    void previewTurnedOn();

    //Signal to remove Preview
    void previewTurnedOff();

    void addPreview();
    void completedPreview();

    void enableUndoRedoButtons();
    
    void modelLoaded(std::string modelPath);
    void modelLoadingStarted(const QUrl& path);
    void modelLoadingCompleted();

    void crossSectionImageRemoved(int directionId, int crossSectionId);

    void crossSectionImageLoaded(int directionId, int crossSectionId, QString imagePath);

    void imageRemoveButtonEnabled(int direction);

    void imageRemoveButtonDisabled(int direction);

    void imageRemovalCompleted();

    void fdWindowClosed();

    void surfaceMerged(std::vector< std::shared_ptr<SurfaceItem> > surfaces);

    /**< Q_PROPERTY: signal related to NOTIFY.*/
    void veFactorChanged(double veFactor);		

    void imageDataLoaded(QString jsonData);

    void flatModeChanged(bool flag);			/**< Q_PROPERTY: signal related to NOTIFY.*/

    void flatHeightAdjusted(bool flat_status, double flat_height);			/**< Signal to noritfy VtkFboItem*/

    void surfaceToFlatSelected(int surface_id);			/**< Signal to notify the CanvasHandler*/

    void treeOpenFlagChanged(bool flag);			/**< Q_PROPERTY: signal related to NOTIFY.*/

    void vizFlagChanged(bool flag);

    void meshProcessingCompleted();

    void domainCreatedForRegion(int domain_id, int region_id);
            
public slots:
    void setItemColor(int item_id, int parent_type_id, QString color);
    void addNewColorData();
    
    void createNewDomain(QString name, QString description);
    void addToDomainList(int id, QString name, QString description);
    
    void removeSingleObject(int parentTypeId, int dataId, int parentId);

    QString getItemName(int parentTypeId, int itemDataId, int parentId);
    QString getItemDescription(int parentTypeId, int itemDataId);
    QString getItemColor(int parentTypeId, int itemDataId);
    QString getItemType(int parentTypeId, int itemDataId);
    void saveItemFormData(int parentTypeId, int itemDataId, QString itemNameText, QString itemDescription);
    void fillRegionsList();
    void clearRegionsList();
    void initiateSurfaceSubmissoin();
    void loadMeshes();
    void updateSurfaceList();
    void addSubmittedSurfaces();

    void undo3dSurfaces();
    void redo3dSurfaces();
        
    Q_INVOKABLE void reset();

    void setBoundingBoxDimension(double size_x, double size_y, double size_z);

    void clearViews();

    void setPreviewFlag();

    void resetPreviewFlag();

    void initiateSurfacePreview();

    void removeSurfacePreview();

    void clearSurfacePreview();

    void addSurfacePreview();

    void setCurrentRuleOperator(int id);

    void checkPreviewButtonStatus();

    void togglePreviewButtonStaus();

    void updateCrossSection();

    void fillDomainList();

    void adjustRegionColorIfExistInDomainList(int region_id, QString color);

    void removeCrossSectionImage(int directionId, int crossSectionId);

    void loadCrossSectionImage(int directionId, int crossSectionId, QString imagePath);

    void enableImageRemoveButton(int direction);

    void disableImageRemoveButton(int direction);

    void completeImageRemoval(int directionId, int crossSectionId);

    void setSourceSurface(int parentId, int surfaceId, int surfaceParentTypeId);
    void setDestinationSurface(int surfaceId, int surfaceParentTypeId);
    void unmergeSurfaceFromGroup(int parentTypeId, int surfaceId, int parentId);
    void saveChildSurfaceData(int parentId, int parentTypeId, int itemDataId, QString itemNameText, QString itemDescription);

    void saveImageDb(QUrl fileUrl, QString image_data_json);
    void loadImageDb(QUrl fileUrl);

    void startFlattening(int surface_id, double flat_height);
    void stopFlattening();

    void setFlattenedSurfaceId(int id);

    void process3DMesh(bool previewFlag);

    void addRegionToTheCreatedDomain(int domain_id, int region_id);

    void takeCareOFRegionVizFlagUpdate();
        
private:
    /**
    * Private Constructor.
    */
    VisualizationHandler(VtkFboItem* vtkFboItem, CanvasHandler* canvasHandler, std::shared_ptr<CommandProcessor> commandProcessor);

    bool isSurfaceNameUnique(QString newItemName);

    bool isItemNameUnique(QString newItemName, int parentTypeId);

    QString getSurfaceName(int dataId);

    QString getSurfaceDescription(int dataId);

    QString getSurfaceColor(int dataId);

    QString getChildSurfaceColor(int dataId);

    std::shared_ptr<SurfaceItem> findChildSurface(int dataId, int parentId);

    void saveSurfaceData(int parentTypeId, int itemDataId, QString itemNameText, QString itemDescription);
    
    void saveChildSurfaceData(int parentTypeId, int itemDataId, QString itemNameText, QString itemDescription);

    void updateSingleSurfaceData(std::shared_ptr<SurfaceItem> surface,int parentTypeId, int itemDataId, QString itemNameText, QString itemDescription);
    
    void changeSingleSurfaceColor(int itemDataId, int type_id, QString newColor);

    void changeSingleRegionColor(int itemDataId, int type_id, QString newColor);
    
    void changeSingleDomainColor(int itemDataId, int type_id, QString newColor);

    void changeSingleSurfaceVisibility(int itemDataId, int typeId, bool visiblilty);
    
    void changeSingleDomainVisibility(int itemDataId, bool visiblilty);

    void changeAllSurfaceVisibility(int surfaceCategory, bool visibility);
    
    void changeAllRegionVisibility(bool visibility);
    
    void changeAllDomainVisibility(bool visibility);

    void changeSingleRegionVisibility(int itemDataId, bool visiblilty);

    bool doesTheRegionExistInsideAnyOtherDomain(int region_id);

    QString getRegionsParentDomainIfExists(int region_id);

    void removeRegionPointerListFromDomains();

    void setStratigraphicVisibility(bool visibility);
    void setStructuralVisibility(bool visibility);
    void setRegionVisibility(bool visibility);
    void setDomainVisibility(bool visibility);

    void surfaceMergeAction();

    QString prepareDomainMap();

    QString getQString(std::string str);

    std::shared_ptr<Region> getRegionById(int id);
    std::shared_ptr<Domain> getDomainById(int id);

    void removeSurfaceFromSurfaceList(int surfaceId);
    void addSurfaceIntoSurfaceList(std::shared_ptr<SurfaceItem> surface);
    void changeChildrenSurfaceColorAndVisibility(int surfaceId, int type_id, QString parentColor, bool parentVisibility);

    std::shared_ptr<SurfaceItem> createSurfaceItem(int category, int surfaceId, std::vector<QString> surfaceColorList, std::vector<bool> surfaceVisibilityList);

    void changeChildSurfaceVisibility(const std::shared_ptr<SurfaceItem>& parent_surface, int typeId, bool visiblilty);

    std::vector<QString> getSurfaceColorMap();
    
    std::vector<bool> getSurfaceVisibilityMap();

    //Return surface color QMap
    QMap<int, QString> getSurfaceColorQmap();

    //Return surface visibility QMap
    QMap<int, bool> getSurfaceVisibilityQmap();

    // Reset view before loading a new model
    void prepareToLoadModel();

    //- Singleton object handling of this class
    static bool instanceFlag;
    static VisualizationHandler* single;

    //- Surface list
    std::vector< std::shared_ptr<SurfaceItem> > surfaceList;
    
    //- Region list
    std::vector< std::shared_ptr<Region> > regionList;

    //- Domain list
    std::vector< std::shared_ptr<Domain> > domainList;

    //- Facade for the stratmod library and the rules processor
    Model3dHandler* m_model3dHandler;

    //- Facade for the CanvasHandler
    CanvasHandler* m_canvasHandler;

    //- Instance of the QML type that contains the VTK FrameBuffer Object for 3DViz
    VtkFboItem* m_vtkFboItem = nullptr;

    //- Selected object 
    //ObjectIdentifier selected_object;

    //- Custom colormap
    CustomColorMap m_customColorMap;

    //- Surface Visibility List
    std::vector <bool> m_visibilityMap;
    
    //- Region Visibility List
    std::vector <bool> m_region_visibilityMap;

    //- domain Visibility List
    std::vector <bool> m_domain_visibilityMap;

    //- domain Visibility List
    std::vector <std::vector<int> > m_domain_region_ids;

    //- Regions visualization flag
    bool m_viz = false;

    //- Domain Colormap
    CustomColorMap m_domainColorMap;

    //- Region Colormap
    CustomColorMap m_regionColorMap;

    //- Surface Colormap
    CustomColorMap m_surfaceColorMap;

    

    //- 3D bounding box dimensions 
    double m_boxDimensionX = 0.0;
    double m_boxDimensionY = 0.0;
    double m_boxDimensionZ = 0.0;

    //- Flags to show File dialog 
    bool m_showSaveFileDialog = false;
    bool m_showOpenFileDialog = false;
    

    //- Current cross-section position number 
    double m_crossSectionNumber = 64.0;

    //- Cross-section plane orientation marker; 0-front; 1-top
    //int m_currentPlaneOrientationId = 0;

    int m_currentPlaneOrientationId = Direction::Value::LENGTH; // 1-WIDTH, 2-LENGTH, 3-HEIGHT


    //- Drag and drop a region to a domain
    int m_source_region = -1;
    int m_source_region_parent_type_id = -1;
    int m_destination_domain = -1;
    int m_source_domain = -1;
    
    std::shared_ptr<CommandProcessor> m_commandProcessor;

    //- Stratigraphy parent visibility
    bool m_stratigraphy_visibility = true;

    //- Structural parent visibility
    bool m_structural_visibility = true;

    //- Region parent visibility
    bool m_region_visibility = true;

    //- Domain parent visibility
    bool m_domain_visibility = false;

    //- Preview flag
    bool m_preview_flag = false;

    //- Rule operator id
    int m_rule_operator_id=0;

    //- Preview button pressed status
    bool m_preview_button_status = false;
    
    //-Merge and Unmerge Surfaces
    int m_desination_surface_id = -1;
    int m_destination_surface_parent_type_id = -1;
    int m_source_surface_id = -1;
    int m_source_surface_parent_type_id = -1;
    int m_parent_id_of_source_surface = -1;

    double m_VE_factor = 1.0;					/**< Vertical Exxageration scale factor*/

    //-Surface colr and visibility map
    QMap<int, QString> m_surfaceColorQmap;
    QMap<int, bool> m_surfaceVisibilityQmap;

    //- Flattening mode indicator
    bool m_flat_mode = false;				/**< Bounding Box: new height of the box when Flattening is on.*/

    //- Flat height
    double m_flat_height;

    //- Flattened surface id
    int m_flat_surface_id = -1;

    //- Flattened surface id
    bool m_tree_open = false;

    // QString m_currentGitHash;
  
public:
  Q_INVOKABLE QString getCurrentGitHash() const { return
    CMAKE_CONFIGURE_GIT_HASH;

    
     }
  // void setCurrentGitHash(const QString& v) { m_currentGitHash = v; }
  
  
};

#endif // VISUALIZATIONHANDLER_H
