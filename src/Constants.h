#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QObject>

/**
* Class TreeNodeParentType
* It contains the type of parent node of nodes in the object tree
*/
class TreeNodeParentType : public QObject
{
	Q_OBJECT
public:
	enum Value {
		Null, STRATIGRAPHIC, STRUCTURAL,
		REGION, DOMAIN_MAIN, DOMAIN_CHILD, 
		STRATIGRAPHIC_CHILD, STRUCTURAL_CHILD,
	};
	Q_ENUM(Value)

private:
	//- Constructor private to prevent instantiation
	explicit TreeNodeParentType();
};

/**
* Class Comments
* Constants used to control which comments are visible during debug.
*/
class Comments : public QObject
{
	Q_OBJECT
	Q_PROPERTY(bool DEV_SICILIA READ DEV_SICILIA CONSTANT)
	Q_PROPERTY(bool DEV_FAZILA READ DEV_FAZILA CONSTANT)

public:
	//- QML side
	bool DEV_SICILIA() const { return dev_sicilia; }
	bool DEV_FAZILA() const { return dev_fazila; }

	//- CPP side: SBIM Classes
	static bool SBIM_CROSS_SECTION() { return false; }
	static bool SBIM_FILE_HANDLER() { return false; }
	static bool SBIM_SKETCH() { return false; }
	static bool SBIM_SKETCH_HANDLER() { return false; }
	static bool SBIM_SKETCH_MAP_VIEW() { return false; }
	static bool SBIM_SKETCH_STACK() { return false; }
	static bool SBIM_SKETCH_TEMPLATE() { return false; }
	static bool SBIM_SURFACE() { return false; }

private:
	bool dev_sicilia = true;
	bool dev_fazila = false;
};

/**
* Class MainWindowConstants
* It has the constants to be used in the application.
*/
class MainWindowConstants : public QObject
{
	Q_OBJECT
	Q_PROPERTY(int WIDTH READ WIDTH CONSTANT)
	Q_PROPERTY(int HEIGHT READ HEIGHT CONSTANT)

	Q_PROPERTY(QString BACKGROUND_CANVAS_2D READ BACKGROUND_CANVAS_2D CONSTANT)
	Q_PROPERTY(QString BACKGROUND_CANVAS_3D READ BACKGROUND_CANVAS_3D CONSTANT)

public:
	int WIDTH() const { return width; }
	int HEIGHT() const { return height; }

	QString BACKGROUND_CANVAS_2D() const { return background_canvas_2d; }
	QString BACKGROUND_CANVAS_3D() const { return background_canvas_3d; }

private:
	int width = 1024;
	int height = 700;

	QString background_canvas_2d = "white";
	QString background_canvas_3d = "white";
};

/**
* Class ButtonCanvasConstants
* It has the properties for the style of the button canvas.
*/
class ButtonCanvasConstants : public QObject
{
	Q_OBJECT
	Q_PROPERTY(int WIDTH READ WIDTH CONSTANT)
	Q_PROPERTY(int HEIGHT READ HEIGHT CONSTANT)

	Q_PROPERTY(int BORDER_WIDTH_ACTIVE READ BORDER_WIDTH_ACTIVE CONSTANT)
	Q_PROPERTY(int BORDER_WIDTH READ BORDER_WIDTH CONSTANT)
	Q_PROPERTY(int BORDER_RADIUS READ BORDER_RADIUS CONSTANT)

	Q_PROPERTY(int TOOLTIP_FONT_PIXEL_SIZE READ TOOLTIP_FONT_PIXEL_SIZE CONSTANT)

	Q_PROPERTY(QString ICON_COLOR_DOWN READ ICON_COLOR_DOWN CONSTANT)
	Q_PROPERTY(QString ICON_COLOR_UP READ ICON_COLOR_UP CONSTANT)
	Q_PROPERTY(QString ICON_COLOR_ENABLE READ ICON_COLOR_ENABLE CONSTANT)
	Q_PROPERTY(QString ICON_COLOR_DISABLE READ ICON_COLOR_DISABLE CONSTANT)

	Q_PROPERTY(QString BACKGROUND_COLOR_DOWN READ BACKGROUND_COLOR_DOWN CONSTANT)
	Q_PROPERTY(QString BACKGROUND_COLOR_UP READ BACKGROUND_COLOR_UP CONSTANT)
	Q_PROPERTY(QString BACKGROUND_COLOR_ENABLE READ BACKGROUND_COLOR_ENABLE CONSTANT)
	Q_PROPERTY(QString BACKGROUND_COLOR_DISABLE READ BACKGROUND_COLOR_DISABLE CONSTANT)

	Q_PROPERTY(QString BORDER_COLOR_DOWN READ BORDER_COLOR_DOWN CONSTANT)
	Q_PROPERTY(QString BORDER_COLOR_UP READ BORDER_COLOR_UP CONSTANT)
	Q_PROPERTY(QString BORDER_COLOR_ENABLE READ BORDER_COLOR_ENABLE CONSTANT)
	Q_PROPERTY(QString BORDER_COLOR_DISABLE READ BORDER_COLOR_DISABLE CONSTANT)

public:
	int WIDTH() const { return width; }
	int HEIGHT() const { return height; }

	int BORDER_WIDTH_ACTIVE() const { return border_width_active; }
	int BORDER_WIDTH() const { return border_width; }
	int BORDER_RADIUS() const { return border_radius; }

	int TOOLTIP_FONT_PIXEL_SIZE() const { return tooltip_font_pixel_size; }

	QString ICON_COLOR_DOWN() const { return icon_color_down; }
	QString ICON_COLOR_UP() const { return icon_color_up; }
	QString ICON_COLOR_ENABLE() const { return icon_color_enable; }
	QString ICON_COLOR_DISABLE() const { return icon_color_disable; }

	QString BACKGROUND_COLOR_DOWN() const { return background_color_down; }
	QString BACKGROUND_COLOR_UP() const { return background_color_up; }
	QString BACKGROUND_COLOR_ENABLE() const { return background_color_enable; }
	QString BACKGROUND_COLOR_DISABLE() const { return background_color_disable; }

	QString BORDER_COLOR_DOWN() const { return border_color_down; }
	QString BORDER_COLOR_UP() const { return border_color_up; }
	QString BORDER_COLOR_ENABLE() const { return border_color_enable; }
	QString BORDER_COLOR_DISABLE() const { return border_color_disable; }

private:
	int width = 64;
	int height = 64;

	int border_width_active = 4;
	int border_width = 2;
	int border_radius = 6;

	int tooltip_font_pixel_size = 20;

	QString icon_color_down = "#404040";
	QString icon_color_up = "#606060";
	QString icon_color_enable = "#404040";
	QString icon_color_disable = "#C6C6C6";

	QString background_color_down = "#74d0f0";
	QString background_color_up = "white";
	QString background_color_enable = "#74d0f0";
	QString background_color_disable = "white";

	QString border_color_down = "#404040";
	QString border_color_up = "#606060";
	QString border_color_enable = "#404040";
	QString border_color_disable = "#C6C6C6";
};


/**
* Class ButtonFloatingConstants
* It has the properties for the style of the button floating.
*/
class ButtonFloatingConstants : public QObject
{
	Q_OBJECT
		Q_PROPERTY(int WIDTH READ WIDTH CONSTANT)
		Q_PROPERTY(int HEIGHT READ HEIGHT CONSTANT)

		Q_PROPERTY(int BORDER_WIDTH_ACTIVE READ BORDER_WIDTH_ACTIVE CONSTANT)
		Q_PROPERTY(int BORDER_WIDTH READ BORDER_WIDTH CONSTANT)
		Q_PROPERTY(int BORDER_RADIUS READ BORDER_RADIUS CONSTANT)

		Q_PROPERTY(int TOOLTIP_FONT_PIXEL_SIZE READ TOOLTIP_FONT_PIXEL_SIZE CONSTANT)

		Q_PROPERTY(QString ICON_COLOR_DOWN READ ICON_COLOR_DOWN CONSTANT)
		Q_PROPERTY(QString ICON_COLOR_UP READ ICON_COLOR_UP CONSTANT)
		Q_PROPERTY(QString ICON_COLOR_ENABLE READ ICON_COLOR_ENABLE CONSTANT)
		Q_PROPERTY(QString ICON_COLOR_DISABLE READ ICON_COLOR_DISABLE CONSTANT)

		Q_PROPERTY(QString BACKGROUND_COLOR_DOWN READ BACKGROUND_COLOR_DOWN CONSTANT)
		Q_PROPERTY(QString BACKGROUND_COLOR_UP READ BACKGROUND_COLOR_UP CONSTANT)
		Q_PROPERTY(QString BACKGROUND_COLOR_ENABLE READ BACKGROUND_COLOR_ENABLE CONSTANT)
		Q_PROPERTY(QString BACKGROUND_COLOR_DISABLE READ BACKGROUND_COLOR_DISABLE CONSTANT)

		Q_PROPERTY(QString BORDER_COLOR_DOWN READ BORDER_COLOR_DOWN CONSTANT)
		Q_PROPERTY(QString BORDER_COLOR_UP READ BORDER_COLOR_UP CONSTANT)
		Q_PROPERTY(QString BORDER_COLOR_ENABLE READ BORDER_COLOR_ENABLE CONSTANT)
		Q_PROPERTY(QString BORDER_COLOR_DISABLE READ BORDER_COLOR_DISABLE CONSTANT)

public:
	int WIDTH() const { return width; }
	int HEIGHT() const { return height; }

	int BORDER_WIDTH_ACTIVE() const { return border_width_active; }
	int BORDER_WIDTH() const { return border_width; }
	int BORDER_RADIUS() const { return border_radius; }

	int TOOLTIP_FONT_PIXEL_SIZE() const { return tooltip_font_pixel_size; }

	QString ICON_COLOR_DOWN() const { return icon_color_down; }
	QString ICON_COLOR_UP() const { return icon_color_up; }
	QString ICON_COLOR_ENABLE() const { return icon_color_enable; }
	QString ICON_COLOR_DISABLE() const { return icon_color_disable; }

	QString BACKGROUND_COLOR_DOWN() const { return background_color_down; }
	QString BACKGROUND_COLOR_UP() const { return background_color_up; }
	QString BACKGROUND_COLOR_ENABLE() const { return background_color_enable; }
	QString BACKGROUND_COLOR_DISABLE() const { return background_color_disable; }

	QString BORDER_COLOR_DOWN() const { return border_color_down; }
	QString BORDER_COLOR_UP() const { return border_color_up; }
	QString BORDER_COLOR_ENABLE() const { return border_color_enable; }
	QString BORDER_COLOR_DISABLE() const { return border_color_disable; }

private:
	int width = 112;
	int height = 112;

	int border_width_active = 4;
	int border_width = 2;
	int border_radius = 16;

	int tooltip_font_pixel_size = 16;

	QString icon_color_down = "#404040";
	QString icon_color_up = "#606060";
	QString icon_color_enable = "#404040";
	QString icon_color_disable = "#C6C6C6";

	QString background_color_down = "#74d0f0";
	QString background_color_up = "white";
	QString background_color_enable = "#74d0f0";
	QString background_color_disable = "white";

	QString border_color_down = "#404040";
	QString border_color_up = "#606060";
	QString border_color_enable = "#404040";
	QString border_color_disable = "#C6C6C6";
};


/**
* Class ButtonToolBarConstants
* It has the properties for the style of the button toolbar.
*/
class ButtonToolBarConstants : public QObject
{
	Q_OBJECT
		Q_PROPERTY(int WIDTH READ WIDTH CONSTANT)
		Q_PROPERTY(int HEIGHT READ HEIGHT CONSTANT)

		Q_PROPERTY(int BORDER_WIDTH_ACTIVE READ BORDER_WIDTH_ACTIVE CONSTANT)
		Q_PROPERTY(int BORDER_WIDTH READ BORDER_WIDTH CONSTANT)
		Q_PROPERTY(int BORDER_RADIUS READ BORDER_RADIUS CONSTANT)

		Q_PROPERTY(int TOOLTIP_FONT_PIXEL_SIZE READ TOOLTIP_FONT_PIXEL_SIZE CONSTANT)

		Q_PROPERTY(QString ICON_COLOR_DOWN READ ICON_COLOR_DOWN CONSTANT)
		Q_PROPERTY(QString ICON_COLOR_UP READ ICON_COLOR_UP CONSTANT)
		Q_PROPERTY(QString ICON_COLOR_ENABLE READ ICON_COLOR_ENABLE CONSTANT)
		Q_PROPERTY(QString ICON_COLOR_DISABLE READ ICON_COLOR_DISABLE CONSTANT)

		Q_PROPERTY(QString BACKGROUND_COLOR_DOWN READ BACKGROUND_COLOR_DOWN CONSTANT)
		Q_PROPERTY(QString BACKGROUND_COLOR_UP READ BACKGROUND_COLOR_UP CONSTANT)
		Q_PROPERTY(QString BACKGROUND_COLOR_ENABLE READ BACKGROUND_COLOR_ENABLE CONSTANT)
		Q_PROPERTY(QString BACKGROUND_COLOR_DISABLE READ BACKGROUND_COLOR_DISABLE CONSTANT)

		Q_PROPERTY(QString BORDER_COLOR_DOWN READ BORDER_COLOR_DOWN CONSTANT)
		Q_PROPERTY(QString BORDER_COLOR_UP READ BORDER_COLOR_UP CONSTANT)
		Q_PROPERTY(QString BORDER_COLOR_ENABLE READ BORDER_COLOR_ENABLE CONSTANT)
		Q_PROPERTY(QString BORDER_COLOR_DISABLE READ BORDER_COLOR_DISABLE CONSTANT)

public:
	int WIDTH() const { return width; }
	int HEIGHT() const { return height; }

	int BORDER_WIDTH_ACTIVE() const { return border_width_active; }
	int BORDER_WIDTH() const { return border_width; }
	int BORDER_RADIUS() const { return border_radius; }

	int TOOLTIP_FONT_PIXEL_SIZE() const { return tooltip_font_pixel_size; }

	QString ICON_COLOR_DOWN() const { return icon_color_down; }
	QString ICON_COLOR_UP() const { return icon_color_up; }
	QString ICON_COLOR_ENABLE() const { return icon_color_enable; }
	QString ICON_COLOR_DISABLE() const { return icon_color_disable; }

	QString BACKGROUND_COLOR_DOWN() const { return background_color_down; }
	QString BACKGROUND_COLOR_UP() const { return background_color_up; }
	QString BACKGROUND_COLOR_ENABLE() const { return background_color_enable; }
	QString BACKGROUND_COLOR_DISABLE() const { return background_color_disable; }

	QString BORDER_COLOR_DOWN() const { return border_color_down; }
	QString BORDER_COLOR_UP() const { return border_color_up; }
	QString BORDER_COLOR_ENABLE() const { return border_color_enable; }
	QString BORDER_COLOR_DISABLE() const { return border_color_disable; }

private:
	int width = 64;
	int height = 64;

	int border_width_active = 4;
	int border_width = 2;
	int border_radius = 8;

	int tooltip_font_pixel_size = 20;

	QString icon_color_down = "#404040";
	QString icon_color_up = "#707070";
	QString icon_color_enable = "#404040";
	QString icon_color_disable = "#C6C6C6";

	QString background_color_down = "#74d0f0";
	QString background_color_up = "white";
	QString background_color_enable = "#74d0f0";
	QString background_color_disable = "white";

	QString border_color_down = "#404040";
	QString border_color_up = "#707070";
	QString border_color_enable = "#404040";
	QString border_color_disable = "#C6C6C6";
};

/**
* Class CanvasAction
* It contains the enumeration of the actions related to the mouse events.
* Each action can only occur one at a time. If the user is Sketching, he cannot Translate for example. 
*/
class CanvasAction : public QObject
{
	Q_OBJECT

public:
	enum Value {
		Null,					//- 
		FB_SKETCH,				//- Floating Button: Sketch
		FB_SKETCH_LINE,			//- Floating Button: Sketch Line
		FB_SKETCH_CIRCLE,		//- Floating Button: Sketch Circle
		FB_SKETCH_ELLIPSE,		//- Floating Button: Sketch Ellipse
		FB_TRANSLATE,			//- Floating Button: Translate
		FB_ROTATE,				//- Floating Button: Rotate
		FB_SCALE,				//- Floating Button: Scale	
		FB_SHOW_GUIDELINES,		//- Floating Button: Guidelines
		FB_FIX_GUIDELINES,		//- Floating Button: Fix Guidelines
		FB_ZOOM,				//- Floating Button: Zoom	
		TB_START_SKETCH_REGION,	//- ToolBar Button: Sketch Region (enable)
		TB_STOP_SKETCH_REGION,	//- ToolBar Button: Sketch Region (disable)
		TB_START_FLATTENING,	//- ToolBar Button: Flattening (enable)
		TB_STOP_FLATTENING,		//- ToolBar Button: Flattening (disable)
		FV_SMOOTH_SKETCH,		//- Front View Button: Smooth Sketch
		FV_CLEAR_SKETCH,		//- Front View Button: Clear Sketch
		FV_UNDO_SKETCH,			//- Front View Button: Undo Sketch
		FV_REDO_SKETCH,			//- Front View Button: Redo Sketch
		FV_SAVE_SKETCH_TEMPLATE,//- Front View Button: Save Sketch Template
		MV_SMOOTH_SKETCH,		//- Map View Button: Smooth Sketch
		MV_CLEAR_SKETCH,		//- Map View Button: Clear Sketch
		MV_UNDO_SKETCH,			//- Map View Button: Undo Sketch
		MV_REDO_SKETCH,			//- Map View Button: Redo Sketch
		MV_SAVE_SKETCH_TEMPLATE	//- Map View Button: Save Sketch Template
	};
	Q_ENUM(Value)

private:
	//- Constructor private to prevent instantiation
	explicit CanvasAction();
};


/**
* Class Direction
* It contains the different type of directions for the cross section sketch.
*/
class Direction : public QObject
{
	Q_OBJECT
public:
	enum Value {Null, WIDTH, LENGTH, HEIGHT};
	Q_ENUM(Value)
private:
	//- Constructor private to prevent instantiation
	explicit Direction();
};


/**
* Class CanvasID
* It contains identifiers for each canvas.
*/
class CanvasID : public QObject
{
	Q_OBJECT
public:
	enum Value { Null, FRONT_VIEW, MAP_VIEW, VTK_VIEW };
	Q_ENUM(Value)
private:
	//- Constructor private to prevent instantiation
	explicit CanvasID();
};


/**
* Class Mouse
* It contains the buttons and actions from the mouse device.
*/
class Mouse : public QObject
{
	Q_OBJECT
public:
	enum Button { NO_BUTTON, LEFT, RIGHT, MIDDLE };
	Q_ENUM(Button)

	enum Action { NO_ACTION, CLICK, DOUBLE_CLICK, NOPRESS, MOVE, RELEASE, PRESS, PRESS_AND_HOLD};
	Q_ENUM(Action)
private:
	//- Constructor private to prevent instantiation
	explicit Mouse();
};



/**
* Class ButtonDialogConstants
* It has the properties for the style of the button related to object tree dialogs.
*/
class ButtonDialogConstants : public QObject
{
	Q_OBJECT
		Q_PROPERTY(int BUTTON_WIDTH READ BUTTON_WIDTH CONSTANT)
		Q_PROPERTY(int BUTTON_HEIGHT READ BUTTON_HEIGHT CONSTANT)

		
		Q_PROPERTY(int OBJECT_TREE_DIALOG_WIDTH READ OBJECT_TREE_DIALOG_WIDTH CONSTANT)
		Q_PROPERTY(int OBJECT_TREE_DIALOG_HEIGHT READ OBJECT_TREE_DIALOG_HEIGHT CONSTANT)

		Q_PROPERTY(int BORDER_WIDTH_ACTIVE READ BORDER_WIDTH_ACTIVE CONSTANT)
		Q_PROPERTY(int BORDER_WIDTH READ BORDER_WIDTH CONSTANT)
		Q_PROPERTY(int BORDER_RADIUS READ BORDER_RADIUS CONSTANT)

		Q_PROPERTY(int BUTTON_FONT_PIXEL_SIZE READ BUTTON_FONT_PIXEL_SIZE CONSTANT)
		Q_PROPERTY(int DIALOG_INPUT_FONT_PIXEL_SIZE READ DIALOG_INPUT_FONT_PIXEL_SIZE CONSTANT)
		Q_PROPERTY(int DIALOG_HEADER_FONT_PIXEL_SIZE READ DIALOG_HEADER_FONT_PIXEL_SIZE CONSTANT)

		Q_PROPERTY(int BUTTON_DROPSHADOW_SAMPLE_SIZE READ BUTTON_DROPSHADOW_SAMPLE_SIZE CONSTANT)

		Q_PROPERTY(QString BACKGROUND_DEFAULT_COLOR READ BACKGROUND_DEFAULT_COLOR CONSTANT)
		Q_PROPERTY(QString BACKGROUND_COLOR_DOWN READ BACKGROUND_COLOR_DOWN CONSTANT)
		Q_PROPERTY(QString BACKGROUND_COLOR_UP READ BACKGROUND_COLOR_UP CONSTANT)
		Q_PROPERTY(QString BACKGROUND_COLOR_ENABLE READ BACKGROUND_COLOR_ENABLE CONSTANT)
		Q_PROPERTY(QString BACKGROUND_COLOR_DISABLE READ BACKGROUND_COLOR_DISABLE CONSTANT)

		Q_PROPERTY(QString BORDER_COLOR_DOWN READ BORDER_COLOR_DOWN CONSTANT)
		Q_PROPERTY(QString BORDER_COLOR_UP READ BORDER_COLOR_UP CONSTANT)
		Q_PROPERTY(QString BORDER_COLOR_ENABLE READ BORDER_COLOR_ENABLE CONSTANT)
		Q_PROPERTY(QString BORDER_COLOR_DISABLE READ BORDER_COLOR_DISABLE CONSTANT)

public:
	int BUTTON_WIDTH() const { return button_width; }
	int BUTTON_HEIGHT() const { return button_height; }

	int OBJECT_TREE_DIALOG_WIDTH() const { return object_tree_dialog_width; }
	int OBJECT_TREE_DIALOG_HEIGHT() const { return object_tree_dialog_height; }

	int BORDER_WIDTH_ACTIVE() const { return border_width_active; }
	int BORDER_WIDTH() const { return border_width; }
	int BORDER_RADIUS() const { return border_radius; }

	int BUTTON_FONT_PIXEL_SIZE() const { return button_font_pixel_size; }
	int DIALOG_INPUT_FONT_PIXEL_SIZE() const { return dialog_input_font_pixel_size; }
	int DIALOG_HEADER_FONT_PIXEL_SIZE() const { return dialog_header_font_pixel_size; }
		
	int BUTTON_DROPSHADOW_SAMPLE_SIZE() const { return button_dropshadow_sample_size; }
	
	QString BACKGROUND_DEFAULT_COLOR() const { return background_default_color; }
	QString BACKGROUND_COLOR_DOWN() const { return background_color_down; }
	QString BACKGROUND_COLOR_UP() const { return background_color_up; }
	QString BACKGROUND_COLOR_ENABLE() const { return background_color_enable; }
	QString BACKGROUND_COLOR_DISABLE() const { return background_color_disable; }

	QString BORDER_COLOR_DOWN() const { return border_color_down; }
	QString BORDER_COLOR_UP() const { return border_color_up; }
	QString BORDER_COLOR_ENABLE() const { return border_color_enable; }
	QString BORDER_COLOR_DISABLE() const { return border_color_disable; }

private:
	int button_width = 100;
	int button_height = 35;

	int object_tree_dialog_width = 800;
	int object_tree_dialog_height = 400;

	int border_width_active = 4;
	int border_width = 2;
	int border_radius = 3;

	int button_font_pixel_size =20;
	int dialog_input_font_pixel_size = 24;
	int dialog_header_font_pixel_size = 28;
	
	int button_dropshadow_sample_size = 20;

	QString background_default_color = "#4E5BF2";
	QString background_color_down = "#74d0f0";
	QString background_color_up = "white";
	QString background_color_enable = "#74d0f0";
	QString background_color_disable = "white";

	QString border_color_down = "#FFC107";
	QString border_color_up = "#606060";
	QString border_color_enable = "#404040";
	QString border_color_disable = "#C6C6C6";
};

/**
* Class ObjectTreeConstants
* It has the properties for the style of the treeview elements.
*/
class ObjectTreeConstants : public QObject
{
	Q_OBJECT
		Q_PROPERTY(int CHECKBOX_WIDTH READ CHECKBOX_WIDTH CONSTANT)
		Q_PROPERTY(int CHECKBOX_HEIGHT READ CHECKBOX_HEIGHT CONSTANT)

		Q_PROPERTY(int CHECKBOX_BACKGROUND_WIDTH READ CHECKBOX_BACKGROUND_WIDTH CONSTANT)
		Q_PROPERTY(int CHECKBOX_BACKGROUND_HEIGHT READ CHECKBOX_BACKGROUND_HEIGHT CONSTANT)

		Q_PROPERTY(QString CHECKBOX_INDICATOR_ACTIVE_COLOR READ CHECKBOX_INDICATOR_ACTIVE_COLOR CONSTANT)
		Q_PROPERTY(QString CHECKBOX_INDICATOR_INACTIVE_COLOR READ CHECKBOX_INDICATOR_INACTIVE_COLOR CONSTANT)
		
		Q_PROPERTY(QString CHECKBOX_BACKGROUND_ACTIVE_COLOR READ CHECKBOX_BACKGROUND_ACTIVE_COLOR CONSTANT)
		Q_PROPERTY(QString CHECKBOX_BACKGROUND_INACTIVE_COLOR READ CHECKBOX_BACKGROUND_INACTIVE_COLOR CONSTANT)

		Q_PROPERTY(QString CHECKBOX_ICON_DOWN_COLOR READ CHECKBOX_ICON_DOWN_COLOR CONSTANT)
		Q_PROPERTY(QString CHECKBOX_ICON_UP_COLOR READ CHECKBOX_ICON_UP_COLOR CONSTANT)

		Q_PROPERTY(int TREE_ROW_HEIGHT READ TREE_ROW_HEIGHT CONSTANT)

		Q_PROPERTY(QString DRAG_FAILURE_COLOR READ DRAG_FAILURE_COLOR CONSTANT)
		Q_PROPERTY(QString DRAG_SUCCESS_COLOR READ DRAG_SUCCESS_COLOR CONSTANT)
		Q_PROPERTY(QString DRAG_ENTER_COLOR READ DRAG_ENTER_COLOR CONSTANT)

		Q_PROPERTY(QString ADD_BUTTON_COLOR READ ADD_BUTTON_COLOR CONSTANT)
		Q_PROPERTY(QString SAVE_BUTTON_COLOR READ SAVE_BUTTON_COLOR CONSTANT)
		Q_PROPERTY(QString REMOVE_BUTTON_COLOR READ REMOVE_BUTTON_COLOR CONSTANT)
		Q_PROPERTY(QString CLOSE_BUTTON_COLOR READ CLOSE_BUTTON_COLOR CONSTANT)

		Q_PROPERTY(QString ROW_SELECTION_COLOR READ ROW_SELECTION_COLOR CONSTANT)

		Q_PROPERTY(int NOTES_FORM_WIDTH READ NOTES_FORM_WIDTH CONSTANT)
		Q_PROPERTY(int NOTES_FORM_HEIGHT READ NOTES_FORM_HEIGHT CONSTANT)

		Q_PROPERTY(int SURFACE_FORM_WIDTH READ SURFACE_FORM_WIDTH CONSTANT)
		Q_PROPERTY(int SURFACE_FORM_HEIGHT READ SURFACE_FORM_HEIGHT CONSTANT)

		
		Q_PROPERTY(QString NOTES_FORM_TEXTAREA_COLOR READ NOTES_FORM_TEXTAREA_COLOR CONSTANT)
		Q_PROPERTY(int NOTES_FORM_TEXTAREA_HEIGHT READ NOTES_FORM_TEXTAREA_HEIGHT CONSTANT)


public:
	int CHECKBOX_WIDTH() const { return cbox_width; }
	int CHECKBOX_HEIGHT() const { return cbox_height; }

	int CHECKBOX_BACKGROUND_WIDTH() const { return cbox_background_width; }
	int CHECKBOX_BACKGROUND_HEIGHT() const { return cbox_background_height; }

	QString CHECKBOX_INDICATOR_ACTIVE_COLOR() const { return cbox_indicator_active_color; }
	QString CHECKBOX_INDICATOR_INACTIVE_COLOR() const { return cbox_indicator_inactive_color; }

	QString CHECKBOX_BACKGROUND_ACTIVE_COLOR() const { return cbox_background_active_color; }
	QString CHECKBOX_BACKGROUND_INACTIVE_COLOR() const { return cbox_background_inactive_color; }
	
	QString CHECKBOX_ICON_DOWN_COLOR() const { return cbox_icon_down_color; }
	QString CHECKBOX_ICON_UP_COLOR() const { return cbox_icon_up_color; }

	int TREE_ROW_HEIGHT() const { return object_tree_row_height; }

	QString DRAG_FAILURE_COLOR() const { return drag_failure_color; }
	QString DRAG_SUCCESS_COLOR() const { return drag_success_color; }
	QString DRAG_ENTER_COLOR() const { return drag_enter_color; }
	

	QString ADD_BUTTON_COLOR() const { return add_button_color; }
	QString SAVE_BUTTON_COLOR() const { return save_button_color; }
	QString REMOVE_BUTTON_COLOR() const { return remove_button_color; }
	QString CLOSE_BUTTON_COLOR() const { return close_button_color; }

	QString ROW_SELECTION_COLOR() const { return row_selection_color; }

	int NOTES_FORM_WIDTH() const { return notes_form_width; }
	int NOTES_FORM_HEIGHT() const { return notes_form_height; }

	int SURFACE_FORM_WIDTH() const { return surface_form_width; }
	int SURFACE_FORM_HEIGHT() const { return surface_form_height; }

	QString NOTES_FORM_TEXTAREA_COLOR() const { return notes_form_textarea_color; }
	int NOTES_FORM_TEXTAREA_HEIGHT() const { return notes_form_textarea_height; }

private:
	int cbox_width = 26;
	int cbox_height = 26;

	int cbox_background_width = 100;
	int cbox_background_height = 40;

	QString cbox_indicator_active_color = "#595959";
	QString cbox_indicator_inactive_color = "#737373"; 

	QString cbox_background_active_color = "#bdbebf";
	QString cbox_background_inactive_color = "#eeeeee";

	QString cbox_icon_down_color = "white";
	QString cbox_icon_up_color = "#74d0f0";
	
	int object_tree_row_height = 60;//50; 

	QString drag_failure_color = "#EF9A9A";//"salmon"
	QString drag_success_color = "#A5D6A7"; //"lightgreen"
	QString drag_enter_color = "#90CAF9";//"lightblue"


	QString add_button_color = "#90CAF9";
	QString save_button_color = "#8BC34A";
	QString remove_button_color = "#F44336";
	QString close_button_color = "#404040";

	QString row_selection_color = "#B0BEC5";//"#9E9E9E";//"#B39DDB";

	int notes_form_width = 400;
	int notes_form_height = 400;

	int surface_form_width = 400;
	int surface_form_height = 400;

	QString notes_form_textarea_color = "#EEEEEE";
	int notes_form_textarea_height = 75;

};

/**
* Class GeologicRules
* It contains the buttons and actions from the mouse device.
*/
class GeologicRules : public QObject
{
	Q_OBJECT
public:
	enum Operator { NO_RULE, RA, RB, RAI, RBI };
	Q_ENUM(Operator)

private:
	//- Constructor private to prevent instantiation
	explicit GeologicRules();
};

/**
* Class ButtonFileMenuConstants
* It has the properties for the style of the button file menu items.
*/
class ButtonFileMenuConstants : public QObject
{
	Q_OBJECT
		Q_PROPERTY(int WIDTH READ WIDTH CONSTANT)
		Q_PROPERTY(int HEIGHT READ HEIGHT CONSTANT)

		Q_PROPERTY(int BORDER_WIDTH_ACTIVE READ BORDER_WIDTH_ACTIVE CONSTANT)
		Q_PROPERTY(int BORDER_WIDTH READ BORDER_WIDTH CONSTANT)
		Q_PROPERTY(int BORDER_RADIUS READ BORDER_RADIUS CONSTANT)

		Q_PROPERTY(int TOOLTIP_FONT_PIXEL_SIZE READ TOOLTIP_FONT_PIXEL_SIZE CONSTANT)

		Q_PROPERTY(QString ICON_COLOR_DOWN READ ICON_COLOR_DOWN CONSTANT)
		Q_PROPERTY(QString ICON_COLOR_UP READ ICON_COLOR_UP CONSTANT)
		Q_PROPERTY(QString ICON_COLOR_ENABLE READ ICON_COLOR_ENABLE CONSTANT)
		Q_PROPERTY(QString ICON_COLOR_DISABLE READ ICON_COLOR_DISABLE CONSTANT)

		Q_PROPERTY(QString BACKGROUND_COLOR_DOWN READ BACKGROUND_COLOR_DOWN CONSTANT)
		Q_PROPERTY(QString BACKGROUND_COLOR_UP READ BACKGROUND_COLOR_UP CONSTANT)
		Q_PROPERTY(QString BACKGROUND_COLOR_ENABLE READ BACKGROUND_COLOR_ENABLE CONSTANT)
		Q_PROPERTY(QString BACKGROUND_COLOR_DISABLE READ BACKGROUND_COLOR_DISABLE CONSTANT)

		Q_PROPERTY(int DROPSHADOW_HORIZONTAL_OFFSET READ DROPSHADOW_HORIZONTAL_OFFSET CONSTANT)
		Q_PROPERTY(int DROPSHADOW_VERTICAL_OFFSET READ DROPSHADOW_VERTICAL_OFFSET CONSTANT)
		
		Q_PROPERTY(QString BORDER_COLOR_ENABLE READ BORDER_COLOR_ENABLE CONSTANT)
		Q_PROPERTY(QString BORDER_COLOR_DISABLE READ BORDER_COLOR_DISABLE CONSTANT)

public:
	int WIDTH() const { return width; }
	int HEIGHT() const { return height; }

	int BORDER_WIDTH_ACTIVE() const { return border_width_active; }
	int BORDER_WIDTH() const { return border_width; }
	int BORDER_RADIUS() const { return border_radius; }

	int TOOLTIP_FONT_PIXEL_SIZE() const { return tooltip_font_pixel_size; }

	QString ICON_COLOR_DOWN() const { return icon_color_down; }
	QString ICON_COLOR_UP() const { return icon_color_up; }
	QString ICON_COLOR_ENABLE() const { return icon_color_enable; }
	QString ICON_COLOR_DISABLE() const { return icon_color_disable; }

	QString BACKGROUND_COLOR_DOWN() const { return background_color_down; }
	QString BACKGROUND_COLOR_UP() const { return background_color_up; }
	QString BACKGROUND_COLOR_ENABLE() const { return background_color_enable; }
	QString BACKGROUND_COLOR_DISABLE() const { return background_color_disable; }

	int DROPSHADOW_HORIZONTAL_OFFSET() const { return dropshadow_horizontal_offset; }
	int DROPSHADOW_VERTICAL_OFFSET() const { return dropshadow_vertical_offset; }
	
	QString BORDER_COLOR_ENABLE() const { return border_color_enable; }
	QString BORDER_COLOR_DISABLE() const { return border_color_disable; }

private:
	int width = 64;
	int height = 64;

	int border_width_active = 5;
	int border_width = 4;
	int border_radius = 5;

	int tooltip_font_pixel_size = 20;

	QString icon_color_down = "#404040";
	QString icon_color_up = "#707070";
	QString icon_color_enable = "#404040";
	QString icon_color_disable = "#C6C6C6";

	QString background_color_down = "#81D4FA";
	QString background_color_up = "#EEEEEE";
	QString background_color_enable = "#81D4FA";//"#74d0f0";
	QString background_color_disable = "#EEEEEE";

	int dropshadow_horizontal_offset = 3;
	int dropshadow_vertical_offset = 3;
	
	QString border_color_enable = "lightblue";//"#404040";
	QString border_color_disable = "lightgrey";//"#C6C6C6";
};

/**
* Class OpenDialogConstants
* It has the properties for the style of the OpenDialog.
*/
class OpenDialogConstants : public QObject
{
	Q_OBJECT
		Q_PROPERTY(int WIDTH READ WIDTH CONSTANT)
		Q_PROPERTY(int HEIGHT READ HEIGHT CONSTANT)

		Q_PROPERTY(int ICON_WIDTH READ ICON_WIDTH CONSTANT)
		Q_PROPERTY(int ICON_HEIGHT READ ICON_HEIGHT CONSTANT)

		Q_PROPERTY(int TITLE_FONT_PIXEL_SIZE READ TITLE_FONT_PIXEL_SIZE CONSTANT)
		Q_PROPERTY(int BUTTON_FONT_PIXEL_SIZE READ BUTTON_FONT_PIXEL_SIZE CONSTANT)
		Q_PROPERTY(int FORM_ELEMENT_FONT_PIXEL_SIZE READ FORM_ELEMENT_FONT_PIXEL_SIZE CONSTANT)

		Q_PROPERTY(int BUTTON_WIDTH READ BUTTON_WIDTH CONSTANT)
		Q_PROPERTY(int BUTTON_HEIGHT READ BUTTON_HEIGHT CONSTANT)

		Q_PROPERTY(QString BUTTON_FONT_COLOR READ BUTTON_FONT_COLOR CONSTANT)
		
public:
	int WIDTH() const { return width; }
	int HEIGHT() const { return height; }

	int ICON_WIDTH() const { return icon_width; }
	int ICON_HEIGHT() const { return icon_height;}
	
	int TITLE_FONT_PIXEL_SIZE() const { return title_font_pixel_size; }
	int BUTTON_FONT_PIXEL_SIZE() const { return button_font_pixel_size; }
	int FORM_ELEMENT_FONT_PIXEL_SIZE() const { return form_element_font_pixel_size; }

	QString BUTTON_FONT_COLOR() const { return button_font_color; }
	
	int BUTTON_WIDTH() const { return button_width; }
	int BUTTON_HEIGHT() const { return button_height; }


private:
	int width = 800; // 600;
	int height = 600; // 500;

	int icon_width = 75;
	int icon_height = 75;
	
	int title_font_pixel_size = 18;
	int button_font_pixel_size = 16;
	int form_element_font_pixel_size = 16;

	QString button_font_color = "#B39DDB";
	
	int button_width = 70;
	int button_height = 70;

};
#endif

/*
* Constants in QML files to be listed here:
* 
* FileMenu.qml
*	buttonNew: receiver.setEvent(20)
* 
* FrontSketchWindow.qml and TopSketchWindow.qml
*	canvasID (I still don't know if I will need this ID anymore)
*	Mouse Identifiers: buttons, click
* 
*/