/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef RRMTREEMODEL_H
#define RRMTREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include <QObject>
#include <QtQml>

#include "./Model3dHandler.h"
#include "./VisualizationHandler.h"
#include "Domain.h"
#include "SurfaceItem.h"

class TreeItem;

//- Object data structure to identify elements of each node in the Treeview
struct ObjectData
{
    //- object id: for the surface it's surface_id and for the region it's region id
    int data_id;
    //- position on the tree view
    int indentation;
    //- Type id for each object
    int type_id;
    //- Type name for the each object
    QString type_name;
    //- Parent id of a node; default value is -1 if not set
    int parent_id = -1;
    //- flat status of a surface if exists
    bool flat_status = false;
};


//! [0]
class RrmTreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    enum TreeModelRoles
    {
        TreeModelRoleName = Qt::UserRole + 1,
        TreeModelRoleVisible = Qt::UserRole + 2,
        TreeModelRoleColor = Qt::UserRole + 3,
        TreeModelRoleVolume = Qt::UserRole + 4,
        TreeModelRoleProportion = Qt::UserRole + 5
    };

    /**
    * Constructor.
    */
    explicit RrmTreeModel(QObject* parent = 0);

    /**
    * Copy constructor.
    * @param other a const reference to another RrmTreeModel.
    */
    RrmTreeModel(const RrmTreeModel& other);

    /**
    * Assignment operator
    * @param other a const reference to another RrmTreeModel.
    */
    RrmTreeModel& operator=(const RrmTreeModel& other);

    /**
    * Destructor.
    */
    ~RrmTreeModel();

    /**
    * Method that generates the single instance of the RrmTreeModel
    * @param QqmlEngine.
    */
    static RrmTreeModel* getInstance(QQmlEngine*);

    /** ************Start: QAbstractItemModel interface ***************************************/
    QVariant data(const QModelIndex& index, int role) const Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex& index) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation,
        int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column,
        const QModelIndex& parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex& index) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex& parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex& parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const override;
    /** **********End: QAbstractItemModel interface ******************************************/

    /**
    * Method that adds Column data
    * @param columnStrings
    * @param position
    * @param corresponding parent and id of the object to be displayed.
    * @return void
    */
    void addObjectItem(QStringList columnStrings, int position, int surface_id);

    /**
    * Method that adds Row data
    * @param columnStrings
    * @param position
    * @param parent reference to the TreeItem
    * @param corresponding parent and id of the object to be displayed.
    * @return List
    */
    QList<QVariant> addRowData(QStringList columnStrings, int position, TreeItem* parent, int surface_id);
    QList<QVariant> addRowData(QStringList columnStrings, int position, TreeItem* parent, int element_id, int parent_id);

    /**
    * Method that creates a TreeITem
    * @param columnStrings
    * @param position
    * @param parent reference to the TreeItem
    * @param corresponding parent and id of the object to be displayed.
    * @return TreeItem
    */
    TreeItem* createItem(QStringList columnStrings, int position, TreeItem* parent, int surface_id);
    TreeItem* createItem(QStringList columnStrings, int position, TreeItem* parent, int element_id, int parent_id);

    /**
    * Method that removes children from a TreeItem
    * @return void
    */
    void removeChildren(TreeItem* parent);

    /**
    * Method that removes rows from a TreeItem
    * @param position
    * @param row number
    * @param parent index
    * @return void
    */
    bool removeRows(int position, int rows, const QModelIndex& parent) override;

    /**
    * Method that returns node data by the index
    * This is an invokable method that may be called from QML
    * @param index
    * @return QVariant
    */
    Q_INVOKABLE QVariant getNodeDataByIndex(const QModelIndex& index);

    /**
    * Method that returns parent TreeItem
    * @param columnStrings
    * @param position
    * @param object id
    * @return TreeItem
    */
    TreeItem* getItemParent(QStringList columnStrings, int position, int id);

    /**
    * Method that returns the index of TreeItem
    * @param TreeItem referene
    * @return QModelIndex
    */
    QModelIndex getIndexByNode(TreeItem* item);

    /**
    * Method that returns the TreeItem reference
    * @param item index
    * @return TreeItem
    */
    TreeItem* getNodeByIndex(const QModelIndex& index);

    /**
    * Method that places the TreeItem on the Treeview
    * @param item reference
    * @param item number
    * @param position
    * @param item prefix to be used
    * @param volume data
    * @return void
    */
    void placeItem(TreeItem* item, int itemNumber, int position, QString itemPrefix, QString volume);

    /**
    * Method that places the domain on the Treeview
    * @param item reference
    * @param item number
    * @param position
    * @param item prefix to be used
    * @param volume data
    * @param volume color
    * @param visibility
    * @return void
    */
    TreeItem* placeDomain(TreeItem* item, int itemNumber, int position, QString itemPrefix, QString volume, QString color, bool visibility);

    /**
    * Method that places the region on the Treeview
    * @param item reference
    * @param item number
    * @param position
    * @param item prefix to be used
    * @param volume data
    * @param volume color
    * @param visibility
    * @return void
    */
    void placeRegion(TreeItem* itemParent, int itemNumber, int position, QString regionName, QString volume, QString color, bool visibility);

    /**
    * Method that places the domain child on the Treeview
    * @param item reference
    * @param item number
    * @param position
    * @param item prefix to be used
    * @param volume data
    * @param volume color
    * @param domain id
    * @param visibility
    * @return void
    */
    void placeDomainChild(TreeItem* itemParent, int itemNumber, int position, QString regionName, QString volume, QString color, QString domainId, bool visibility);

    /**
    * Method that places the surface object on the Treeview
    * @param item reference
    * @param item number
    * @param position
    * @param item prefix to be used
    * @param volume data
    * @param volume color
    * @param visibility
    * @return void
    */
    //void placeSurface(TreeItem* itemParent, int itemNumber, int position, QString itemName, QString color, bool visibility);
    TreeItem* placeSurface(TreeItem* itemParent, int itemNumber, int position, QString itemName, QString color, bool visibility);
    
    /**
    * Method that updates the surface list
    * @param surface list
    * @return void
    */
    void updateSurfaceChildren(std::vector<int> surfaceList);

    /**
    * Method that removes regions
    * This is an invokable method that may be called from QML
    * @return void
    */
    Q_INVOKABLE void removeRegions();

    /**
    * Method that removes domain children
    * @return void
    */
    void removeDomainChildren();

    /**
    * Method that returns child indices
    * This is an invokable method that may be called from QML
    * @return QVariantList
    */
    Q_INVOKABLE QVariantList getChildrenIndices(const QModelIndex& index);

signals:
    void treeObjectsUpdated();
    void expandTreeNodes();
    void expandDomainColorMap(int index);
    void domainNameListUpdated();
    void domainNodesUpdated();
    void regionNodesUpdated();
    void surfaceNodesItemsUpdated();

public slots:
    void updateSurfaceNodes(std::vector<QString> colorMap, std::vector<bool> visibilityMap);
    void updateSingleSurfaceNodeColor(int index, QString color, int parent_type_id);
    void updateDomainNodes(std::vector< std::shared_ptr<Domain> > domains);
    void reloadDomains();
    void updateRegionNodes(std::vector< std::shared_ptr<Region> > regions);
    void reloadRegions();
    void updateSurfaceItems(std::vector< std::shared_ptr<SurfaceItem> > surfaces);
    void reloadSurfaces();
    void reset();
    void updateParentNode(int parentTypeId, bool visibility);

private:
    //- Singleton object handling of this class
    static bool instanceFlag;
    static RrmTreeModel* single;

    /**
    * Method that initializes tree data
    * @return void
    */
    void initTreeData();

    /**
    * Method that generates custom type
    * @param text
    * @param position
    * @param id
    * #param parent type id of each custom type
    * @return QVariant
    */
    QVariant newCustomType(const QString& text, int position, int surface_id, int parent_type);
    QVariant newCustomType(const QString& text);

    /**
    * Method that initializes tree model data
    * @return void
    */
    void setupModelData(TreeItem* parent);

    /**
    * Method that adds row to the last child
    * @param columnStrings
    * @param position
    * @param corresponding parent and id of the object to be displayed.
    * @return List
    */
    QList<QVariant> addRowToLastChild(QStringList columnStrings, int position, int surface_id);

    /**
    * Method that returns the column string
    * @param column name
    * @param visibility
    * @param color
    * @param volume
    * @return List
    */
    QStringList getColumnString(QString name, QString visibility, QString color, QString volume);

    /**
    * Method that returns the column string
    * @param column name
    * @param visibility
    * @param color
    * @param volume
    * @param parent id
    * @return List
    */
    QStringList getColumnString(QString name, QString visibility, QString color, QString volume, QString parent_id);

    /**
    * Method that returns TreeITem
    * @param model index
    * @return TreeItem
    */
    TreeItem* getItem(const QModelIndex& index) const;

    /**
    * Method that sets item data
    * @param model index
    * @param position
    * @param type name
    * @return void
    */
    void setItemData(int id, int position, int type_id, QString type_name);

    /**
    * Method that sets item data
    * @param model index
    * @param position
    * @param type name
    * @param parent id
    * @return void
    */
    void setItemData(int id, int position, int type_id, QString type_name, int parent_id);

    /**
    * Method that sets item data
    * @param model index
    * @param position
    * @param type name
    * @param parent id
    * @param flat status
    * @return void
    */
    void RrmTreeModel::setItemData(int id, int position, int type_id, QString type_name, int parent_id, bool flat_staus);

    /**
    * Method that adds a domain TreeItem
    * @param model index
    * @param model volume
    * @param model color
    * @param model visibility
    * @return TreeItem
    */
    TreeItem* addDomain(QString domainName, int index, double volume, QString color, bool visibility);

    /**
    * Method that adds a domain' child
    * @param reference to parent item
    * @param name
    * @param model index
    * @param model volume
    * @param model color
    * @param model domain id
    * @param visibility
    * @return TreeItem
    */
    void addDomainChild(TreeItem* parent, QString name, int index, double volume, QString color, int domain_id, bool visibility);

    /**
    * Method that adds a region
    * @param model index
    * @param model volume
    * @param model color
    * @param model visibility
    * @return void
    */
    void addRegion(QString regionName, int index, double volume, QString color, bool visibility);

    /**
    * Method that adds a stratigrahic surface
    * @param model surface name
    * @param model index
    * @param model surface category
    * @param model color
    * @param model visibility
    * @return void
    */
    //void addStratigraphy(QString surfaceName, int index, int category, QString color, bool visibility);
    TreeItem* addStratigraphy(QString surfaceName, int index, int category, QString color, bool visibility, bool flat_status);

    /**
    * Method that adds a structural surface
    * @param model surface name
    * @param model index
    * @param model surface category
    * @param model color
    * @param model visibility
    * @return void
    */
    void addStructure(QString surfaceName, int index, int category, QString color, bool visibility);

    /**
    * Method that adds clears all parent nodes
    * @return void
    */
    void clearAllParentNodes();

    /**
    * Method that returns the total volume of all regions
    * @return double
    */
    double getTotalVolumeOfRegions();

    /**
    * Method that adds a stratigrahic surface
    * @param surface name
    * @param index
    * @param surface category
    * @param color
    * @param visibility
    * @param flat_status
    * @return void
    */
    //void addSurfaceChild(TreeItem* surfaceItem, QString surfaceName, int index, int category, QString color, bool visibility, int position, int node_type_id, QString node_type_name, int parent_surface_id);
    void addSurfaceChild(TreeItem* surfaceItem, QString surfaceName, int index, int category, QString color, bool visibility, int position, int node_type_id, QString node_type_name, int parent_surface_id, bool flat_status);

    //- Reference to tree root 
    TreeItem* rootItem;

    //- Hash for Treeview role name mapping
    QHash<int, QByteArray> m_roleNameMapping;

    //- Tree's top level parent item list
    QList<TreeItem*> m_parentsList;

    //- Column indentations 
    QList<int> m_indentations;

    //- Referene to stratigraphic surface parent
    TreeItem* m_surfaceParent;

    //- Referene to region parent
    TreeItem* m_regionParent;

    //- Referene to structural surface parent
    TreeItem* m_structural_surfaceParent;

    //- Referene to domain parent
    TreeItem* m_domainParent;

    //- Facade for the stratmod library and the rules processor
    Model3dHandler* m_model3dHandler;

    //- Facade for the VisualizationHandler
    VisualizationHandler* m_vizHandler;

    //- Private property for each element of a node
    ObjectData m_node_element;

    //- ColorList from the VisualizationHandler
    std::vector<QString> m_colorList;

    //- VisibilityList from the VisualizationHandler
    std::vector<bool> m_visibilityList;

    //- Surface List from the CanvasHandler
    std::vector<int> m_surfaceList;

    //- List of domains
    std::vector< std::shared_ptr<Domain> > m_domainList;

    //- Domain ColorList 
    std::vector<QString> m_domainColorList;

    //- List of regions
    std::vector< std::shared_ptr<Region> > m_regionList;

    //- List of surfaces
    std::vector< std::shared_ptr<SurfaceItem> > m_surfaceItemList;

    //- Stratigraphy parent visibility
    bool m_stratigraphy_visibility = true;

    //- Structural parent visibility
    bool m_structural_visibility = true;

    //- Region parent visibility
    bool m_region_visibility = true;

    //- Domain parent visibility
    bool m_domain_visibility = false;
};
//! [0]

#endif // RRMTREEMODEL_H
