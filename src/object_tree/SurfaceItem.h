#ifndef SURFACEITEM_H
#define SURFACEITEM_H

#include <vector>
#include <memory>
#include <QObject>

class SurfaceItem
{
public:
    SurfaceItem(int id, QString name, QString desc, QString color, int category, bool visibility);

    /**
    * Copy Constructor.
    * @param other a const reference to another SurfaceItem
    */
    SurfaceItem(const SurfaceItem& obj);

    /**
    * Assignment operator
    * @param new SurfaceItem obj a const reference to another SurfaceItem.
    */
    SurfaceItem& operator=(const SurfaceItem& obj);

    void reName(QString newName);
    void setDescription(QString desc);
    int getId();
    QString getDescription();
    QString getName();
    QString getColor();
    void setColor(QString
        color);
    double getCategory();
    void setCategory(int category);
    void setVisibility(bool visibility);
    bool getVisibility();

    std::vector<std::shared_ptr<SurfaceItem>> getSurfaceChildList();
    void setChildList(std::vector<std::shared_ptr<SurfaceItem>> childList);
    void addToChildList(std::shared_ptr<SurfaceItem> surface);
    void clearChildList();

    int getParentId();
    void setParentId(int parentId);

    int getGroupId();
    void setGroupId(int groupId);

    bool isFlattened();
    void flatten(bool flag);

private:
    int m_id;
    int m_parent_id = -1; // To determine the item level on the tree
    int m_group_id = -1; // To determine which surface group the surface belongs to 
    QString m_name;
    QString m_description;
    int m_category;
    QString m_color;
    bool m_visibility;
    bool m_flattened = false;

    std::vector< std::shared_ptr<SurfaceItem> > m_childList;
};

#endif // SURFACEITEM_H
