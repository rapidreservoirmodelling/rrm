#ifndef REGION_H
#define REGION_H

#include <vector>
#include <QObject>

class Region
{
public:
    Region(int id,QString name, QString desc, QString color, double volume, bool visibility);
    
    void reName(QString newName);
    void setDescription(QString desc);
    int getId();
    QString getDescription();
    QString getName();
    QString getColor();
    void setColor(QString color);
    double getVolume();
    void setVolume(double volume);
    bool getVisibility();
    void setVisibility(bool visibilty);

private:
    int m_id;
    QString m_name;
    QString m_description;
    double m_volume;
    QString m_color;
    bool m_visibility;

};

#endif // REGION_H
