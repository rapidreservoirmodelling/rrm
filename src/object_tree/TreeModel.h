#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QStandardItemModel>
#include <QModelIndex>
#include <QVariant>
#include <QObject>

struct Item
{
    QString name;
    QString color_button_flag;
    QString parent_group_name;
    QString visibility;
    QString color;
    QString volume;
};

class TreeModel : public QStandardItemModel
{
    Q_OBJECT
    public:
        explicit TreeModel(QObject *parent = 0);

        virtual ~TreeModel() = default;

         enum TreeModel_Roles
         {
                TreeModel_Role_Name = Qt::DisplayRole,
                TreeModel_Role_Color = Qt::WhatsThisRole,
                TreeModel_Role_Volume = Qt::UserRole,
                TreeModel_Role_ColorFlag = Qt::UserRole + 1, 
                TreeModel_Role_Visibility = Qt::UserRole + 2, 
         };

        Q_INVOKABLE bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);
        Q_INVOKABLE QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
        QHash<int, QByteArray> roleNames() const override;
        //QModelIndex parent(const QModelIndex& index) const override;

        void updateData();
    
     private:
            void initData();
            void addEntry( const QString& name, const QString& type, const QString& description );
            QStandardItem* addItem( const QString& name, const QString& parent_group_name, const QString& color,const QString& volume, bool color_flag, bool visibility);

            QStandardItem * addGroup(const QString& name, const QString& parent);


            QStandardItem* getBranch( const QString& branchName );
            QHash<int, QByteArray> m_roleNameMapping;


            QStandardItem *rootItem = invisibleRootItem();

            
};

#endif // TREEMODEL_H
