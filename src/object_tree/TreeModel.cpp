#include "TreeModel.h"
#include "./Model3dHandler.h";

#include <QStandardItemModel>
#include <QDebug>

TreeModel::TreeModel(QObject *parent) :
    QStandardItemModel(parent)
{
    QVector<QVariant> rootData;

    m_roleNameMapping[TreeModel_Role_Name] = "name_role";
    m_roleNameMapping[TreeModel_Role_Color] = "color_role";
    m_roleNameMapping[TreeModel_Role_Volume] = "volume_role";
    m_roleNameMapping[TreeModel_Role_ColorFlag] = "color_flag_role";
    m_roleNameMapping[TreeModel_Role_Visibility] = "visibility_role";

    initData();
}


QStandardItem * TreeModel::addGroup(const QString& name, const QString& parent)
{
    QStandardItem* group = new QStandardItem;

    if(parent !="")
    {
        //qDebug()<<"Bingo1";
        QList<QStandardItem *> itemList = findItems(parent, Qt::MatchExactly | Qt::MatchRecursive, 0);
        QStandardItem* childEntry = new QStandardItem;

        if (!itemList.isEmpty())
        {
            //qDebug()<<"Bingo2";
            QStandardItem* groupEntry = itemList.first();
            childEntry = new QStandardItem( name );
            childEntry->setData(childEntry->text(), TreeModel_Role_Name );
            childEntry->setData("", TreeModel_Role_Color);
            childEntry->setData("", TreeModel_Role_Volume);
            childEntry->setData(QVariant("false").toString(), TreeModel_Role_ColorFlag);
            childEntry->setData(QVariant("true").toString(), TreeModel_Role_Visibility);

            groupEntry->appendRow( childEntry );
        }
        else
        {
            //qDebug()<<"Bingo3";
            group = new QStandardItem;
            //group->setText( name );
            group->setData( name, TreeModel_Role_Name );
            group->setData("", TreeModel_Role_Color);
            group->setData("", TreeModel_Role_Volume);
            group->setData("false", TreeModel_Role_ColorFlag);
            group->setData("true", TreeModel_Role_Visibility);

            rootItem->appendRow(group);
        }

    }
    else
    {
        //qDebug()<<"Bingo4";
        group = new QStandardItem;
        //group->setData( "", MyTreeModel_Role_Name );
        group->setText( name );
        rootItem->appendRow(group);
    }
    return group;
}
void TreeModel::addEntry( const QString& name, const QString& type, const QString& description )
{
    auto childEntry = new QStandardItem( name );
    childEntry->setData( description, TreeModel_Role_Color);

    QStandardItem* entry = getBranch( type );
    entry->appendRow( childEntry );
}

/*QString name;
QString parent_group_name;
QString visibility;
QString color_button_flag;
QString color;
QString volume;
*/
QStandardItem *TreeModel::addItem( const QString& name,
                                         const QString& parent_group_name,
                                         const QString& color,
                                         const QString& volume, 
                                        bool color_flag,
                                        bool visibility)
{
    QString color_flag_s = QVariant(color_flag).toString();
    QString visibility_s = QVariant(visibility).toString();

    QList<QStandardItem *> itemList = findItems(parent_group_name, Qt::MatchExactly | Qt::MatchRecursive, 0);
    QStandardItem* childEntry = new QStandardItem;

    beginResetModel();
    if (!itemList.isEmpty())
    {
        QStandardItem* groupEntry = itemList.first();
        childEntry = new QStandardItem( name );
        childEntry->setData(color, TreeModel_Role_Color );
        childEntry->setData( volume, TreeModel_Role_Volume );
        childEntry->setData(color_flag_s, TreeModel_Role_ColorFlag);
        childEntry->setData(visibility_s, TreeModel_Role_Visibility);

        groupEntry->appendRow( childEntry );
    }
    else {
        childEntry = new QStandardItem( name );
        childEntry->setData(color, TreeModel_Role_Color);
        childEntry->setData(volume, TreeModel_Role_Volume);
        childEntry->setData(color_flag_s, TreeModel_Role_ColorFlag);
        childEntry->setData(visibility_s, TreeModel_Role_Visibility);

        QStandardItem* entry = getBranch(parent_group_name);
        entry->appendRow( childEntry );
    }
    this->itemChanged(childEntry);
    
    endResetModel();
    return childEntry;
}

void TreeModel::initData()
{
    //addGroup("Stratigraphy", "Objects");
    //addGroup("Structural", "Objects");
    //addGroup("Regions", "Objects");
    
    this->addItem( "Surface1", "Stratigraphy", " #ff471a", "10.5", true, true );//Red
    this->addItem( "Surface2", "Stratigraphy", "#3366ff", "15.4", true, true); //Blue
    this->addItem( "Surface3", "Structural", "#ffff00", "13.5", true, true);//Yellow
    this->addItem( "Surface4", "Structural", "#ff8000", "11.2", true, true);//Orange

    this->addItem( "Region1", "Regions", "#e6e6e6", "10.5", true, true);
    this->addItem( "Region2", "Regions", "#ccffcc", "9.8", true, true);

    this->addItem( "Surface1", "Region1", " #ff471a", "10.5", true, true); //Red
    this->addItem( "Surface2", "Region1", "#3366ff", "15.4", true, true); //Blue

    this->addItem( "Surface3", "Region2", "#ffff00", "13.5", true, true);//Yellow
    this->addItem( "Surface4", "Region2", "#ff8000", "11.2", true, true);//Orange

    addGroup("Domains", "Objects");
    addGroup("Domain 1", "Domains");

}

QStandardItem *TreeModel::getBranch(const QString &branchName)
{
    QStandardItem* entry;
    auto entries = this->findItems( branchName );
    if ( entries.count() > 0 )
    {
        entry = entries.at(0);
    }
    else
    {
        entry = new QStandardItem( branchName );
        this->appendRow( entry );
    }
    return entry;
}

QHash<int, QByteArray> TreeModel::roleNames() const
{
    return m_roleNameMapping;
}

void TreeModel::updateData()
{
    this->addItem("Surface5", "Stratigraphy", "#ffff00", "10.5", true, true);
}

bool TreeModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    bool result = false;
    if (role != Qt::EditRole)
        return false;

    QStandardItem* myitem = itemFromIndex(index);
    const int row = index.row();
    const int column = role;

    switch (column)
    {
    case TreeModel_Role_Name:
        myitem->setData(value, TreeModel_Role_Name);
        result = true;
        break;
    case TreeModel_Role_Color:
        myitem->setData(value, TreeModel_Role_Color);
        result = true;
        break;
    case TreeModel_Role_Volume:
        myitem->setData(value, TreeModel_Role_Volume);
        result = true;
        break;
    case TreeModel_Role_ColorFlag:
        myitem->setData(value, TreeModel_Role_ColorFlag);
        break;
    case TreeModel_Role_Visibility:
        myitem->setData(value, TreeModel_Role_Visibility);
        break;
    }

    if (result)
        emit dataChanged(index, index, { Qt::DisplayRole, Qt::EditRole });

    return result;
}

QVariant TreeModel::data(const QModelIndex& index, int role) const 
{
    QVariant variant;

    QStandardItem* myitem = itemFromIndex(index);

    const int row = index.row();
    const int column = role;

    switch (column)
    {
        case TreeModel_Role_Name:
            variant = myitem->data(TreeModel_Role_Name);
            break;
        case TreeModel_Role_Color:
            variant = myitem->data(TreeModel_Role_Color);
            break;
        case TreeModel_Role_Volume:
            variant = myitem->data(TreeModel_Role_Volume);
            break;
        case TreeModel_Role_ColorFlag:
            variant = myitem->data(TreeModel_Role_ColorFlag);
            break;
        case TreeModel_Role_Visibility:
            variant = myitem->data(TreeModel_Role_Visibility);
            break;
    }

    return variant;
}

