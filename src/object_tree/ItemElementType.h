#ifndef ITEMELEMENTTYPE_H
#define ITEMELEMENTTYPE_H

#include <QObject>

class ItemElementType : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(int indentation READ indentation WRITE setIndentation NOTIFY indentationChanged)
    Q_PROPERTY(int dataId READ dataId WRITE setDataId NOTIFY dataIdChanged)
    //Q_PROPERTY(int parentType READ parentType WRITE setParentType NOTIFY parentTypeChanged)
    Q_PROPERTY(int parentTypeId READ parentTypeId WRITE setParentTypeId NOTIFY parentTypeIdChanged)
    Q_PROPERTY(QString parentTypeName READ parentTypeName WRITE setParentTypeName NOTIFY parentTypeNameChanged)
    Q_PROPERTY(int parentId READ parentId WRITE setParentId NOTIFY parentIdChanged)
    Q_PROPERTY(bool flattened READ flatStatus WRITE setFlatStatus NOTIFY flatStatusChanged)

public:
    explicit ItemElementType(QObject *parent = 0);

    /**
    * Copy constructor.
    * @param other a const reference to another ElementType.
    */
    ItemElementType(const ItemElementType &other);

    /**
    * Assignment operator
    * @param newElementType a const reference to another ElementType.
    */
    ItemElementType& operator=(const ItemElementType& newElementType);
    ~ItemElementType();

    QString text();
    void setText(QString text);

    int indentation();
    void setIndentation(int indentation);

    int dataId();
    void setDataId(int data_id);

    int parentTypeId();
    void setParentTypeId(int parentTypeId);

    QString parentTypeName();
    void setParentTypeName(QString parentTypeName);

    int parentId();
    void setParentId(int parentId);

    bool flatStatus();
    void setFlatStatus(bool status);

signals:
    void textChanged();
    void indentationChanged();
    void dataIdChanged();
    void parentTypeIdChanged();
    void parentTypeNameChanged();
    void parentIdChanged();
    void flatStatusChanged();

private:
    QString myText;
    int myIndentation;
    int m_dataId;
    int m_parentType_id;
    QString m_parentType_name;
    int m_parent_id;
    bool m_flat = false;
};
Q_DECLARE_METATYPE(ItemElementType)
#endif // ITEMELEMENTTYPE_H
