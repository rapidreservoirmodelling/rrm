/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "RrmTreeModel.h"

#include "TreeItem.h"
#include "ItemElementType.h"
#include "./utils/surface_colors.h"

#include <QStringList>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <thread> 

bool RrmTreeModel::instanceFlag = false;
RrmTreeModel* RrmTreeModel::single = NULL;

RrmTreeModel::RrmTreeModel(QObject* parent) : QAbstractItemModel(parent)
{
    m_roleNameMapping[TreeModelRoleName] = "title";
    m_roleNameMapping[TreeModelRoleVisible] = "visible";
    m_roleNameMapping[TreeModelRoleColor] = "color";
    m_roleNameMapping[TreeModelRoleVolume] = "volume";
    m_roleNameMapping[TreeModelRoleProportion] = "proportion";

    QList<QVariant> rootData;
    rootData << "Title" << "Summary";
    rootItem = new TreeItem(rootData);
    m_parentsList << rootItem;

    m_model3dHandler = Model3dHandler::getInstance(nullptr);
    //m_vizHandler = VisualizationHandler::getInstance(NULL, nullptr, NULL);

    initTreeData();
}

RrmTreeModel::RrmTreeModel(const RrmTreeModel& other) : RrmTreeModel()
{

}

RrmTreeModel& RrmTreeModel::operator=(const RrmTreeModel& other)
{
    return *this;
}

void RrmTreeModel::initTreeData()
{
    m_indentations << 0;

    QStringList columnStrings = {};
    int position = 0;//level-1
    //Row 1
    //QStringList columnStrings1 = { "Stratigraphy", "", "", "" };
    QStringList columnStrings1 = { "Stratigraphy", QString::number(m_stratigraphy_visibility), "", "" };
    m_surfaceParent = getItemParent(columnStrings1, position, 1);

    //Row 2
    QStringList columnStrings2 = { "Structural", QString::number(m_structural_visibility), "", "" };
    m_structural_surfaceParent = getItemParent(columnStrings2, position, 2);

    //Row 3
    QStringList columnStrings3 = { "Regions", QString::number(m_region_visibility), "", "" };
    m_regionParent = getItemParent(columnStrings3, position, 3);

    //Row 4
    QStringList columnStrings4 = { "Domains", QString::number(m_domain_visibility), "", "" };
    m_domainParent = getItemParent(columnStrings4, position, 4);

}

RrmTreeModel::~RrmTreeModel()
{
    instanceFlag = false;
    delete rootItem;
}

RrmTreeModel* RrmTreeModel::getInstance(QQmlEngine* qmlEngine)
{
    if (!instanceFlag)
    {
        single = new RrmTreeModel();
        instanceFlag = true;
        //set this instance to QML root context
        QQmlContext* rootContext = qmlEngine->rootContext();
        rootContext->setContextProperty("theModel", QVariant::fromValue(single));
        return single;
    }
    else
    {
        return single;
    }
}

int RrmTreeModel::columnCount(const QModelIndex& parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

QVariant RrmTreeModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != TreeModelRoleName && role != TreeModelRoleVisible && role != TreeModelRoleColor && role != TreeModelRoleVolume)
        return QVariant();

    TreeItem* item = static_cast<TreeItem*>(index.internalPointer());

    //int testRole = role - Qt::UserRole - 1;
    //qDebug()<<"Role:" << testRole;
    return item->data(role - Qt::UserRole - 1);

}

Qt::ItemFlags RrmTreeModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

QVariant RrmTreeModel::headerData(int section, Qt::Orientation orientation,
    int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex RrmTreeModel::index(int row, int column, const QModelIndex& parent)
const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeItem* parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem* childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex RrmTreeModel::parent(const QModelIndex& index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem* childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem* parentItem = childItem->parentItem();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int RrmTreeModel::rowCount(const QModelIndex& parent) const
{
    TreeItem* parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

QHash<int, QByteArray> RrmTreeModel::roleNames() const
{
    return m_roleNameMapping;
}

QVariant RrmTreeModel::newCustomType(const QString& text, int position, int surface_id, int parent_type = 0)
{
    ItemElementType* t = new ItemElementType(this);
    t->setText(text);
    t->setIndentation(position);
    t->setDataId(surface_id); //t->setSurfaceId(surface_id);
    t->setParentTypeId(parent_type);//t->setParentType(parent_type);
    QVariant v;
    v.setValue(t);
    return v;
}

QVariant RrmTreeModel::newCustomType(const QString& text)
{
    ItemElementType* t = new ItemElementType(this);
    t->setText(text);
    t->setIndentation(m_node_element.indentation);
    t->setDataId(m_node_element.data_id);
    t->setParentTypeId(m_node_element.type_id);
    t->setParentTypeName(m_node_element.type_name);
    t->setParentId(m_node_element.parent_id);
    t->setFlatStatus(m_node_element.flat_status);
    QVariant v;
    v.setValue(t);
    return v;
}

QList<QVariant> RrmTreeModel::addRowToLastChild(QStringList columnStrings, int position, int surface_id)
{
    QList<QVariant> columnData;
    //for (int column = 0; column < 4; ++column)
    for (int column = 0; column < columnStrings.size(); ++column)
    {
        columnData << newCustomType(columnStrings[column], position, surface_id);

    }

    /*if (position > m_indentations.last()) {
        // The last child of the current parent is now the new parent
        // unless the current parent has no children.

        if (m_parentsList.last()->childCount() > 0) {
            m_parentsList << m_parentsList.last()->child(m_parentsList.last()->childCount()-1);
            m_indentations << position;
        }
    } else {
        while (position < m_indentations.last() && m_parentsList.count() > 0) {
            m_parentsList.pop_back();
            m_indentations.pop_back();
        }
    }
    */
    return columnData;
}

void RrmTreeModel::addObjectItem(QStringList columnStrings, int position, int surface_id)
{
    QList<QVariant> columnData;

    columnData = addRowToLastChild(columnStrings, position, surface_id);
    TreeItem* item1 = new TreeItem(columnData, m_parentsList.last());
    m_parentsList.last()->appendChild(item1);

}

void RrmTreeModel::setupModelData(TreeItem* parent)
{
    m_parentsList << parent;
    m_indentations << 0;

    int position = 0;//level-1


    ///////Dummy Data /////////////
    //Row 1
    QStringList columnStrings = { "Stratigraphy", "false", "", "10.5" };
    addObjectItem(columnStrings, position, 0);

    //Children of Row1
    position = 4; //level-2
    QStringList columnStrings11 = { "Surface1", "true", "#ff471a", "11.2" };
    addObjectItem(columnStrings11, position, 1);

    QStringList columnStrings12 = { "Surface2", "true", "#3366ff", "15.2" };
    addObjectItem(columnStrings12, position, 2);


    //Row 2
    position = 0;
    QList<QVariant> columnData2;
    QStringList columnStrings2 = { "Regions", "false", "", "9.1" };
    addObjectItem(columnStrings2, position, 0);

    //Children of Row2
    position = 4;
    QStringList columnStrings21 = { "Region1", "true", "#ff8000", "5.2" };
    addObjectItem(columnStrings21, position, 0);

    //Children of Row2 - another
    position = 8;
    QStringList columnStrings22 = { "Surface1", "true", "#80ffcc", "4.2" };
    addObjectItem(columnStrings22, position, 1);

    QStringList columnStrings23 = { "Surface2", "true", "#ff6101", "1.2" };
    addObjectItem(columnStrings23, position, 2);


}

QVariant RrmTreeModel::getNodeDataByIndex(const QModelIndex& index)
{
    //qDebug()<<"Pressed row:" <<index.row() << " col:" << index.column();
    if (!index.isValid())
        return QVariant();
    TreeItem* node = static_cast<TreeItem*>(index.internalPointer());
    //return node->data(index.column());
    return node->data(index.column());

}

TreeItem* RrmTreeModel::getItemParent(QStringList columnStrings, int position, int id)
{
    QList<QVariant> columnData;

    columnData = addRowToLastChild(columnStrings, position, id);
    TreeItem* item1 = new TreeItem(columnData, m_parentsList.last());
    m_parentsList.last()->appendChild(item1);

    return item1;
}

TreeItem* RrmTreeModel::createItem(QStringList columnStrings, int position, TreeItem* parent, int surface_id)
{
    QList<QVariant> columnData;

    columnData = addRowData(columnStrings, position, parent, surface_id);
    TreeItem* item1 = new TreeItem(columnData, parent);
    parent->appendChild(item1);

    return item1;
}

TreeItem* RrmTreeModel::createItem(QStringList columnStrings, int position, TreeItem* parent, int element_id, int parent_id)
{
    QList<QVariant> columnData;

    columnData = addRowData(columnStrings, position, parent, element_id, parent_id);
    TreeItem* item1 = new TreeItem(columnData, parent);
    parent->appendChild(item1);

    return item1;
}

QStringList RrmTreeModel::getColumnString(QString name, QString visibility, QString color, QString volume)
{
    QStringList columnStrings;

    columnStrings.append(name);
    columnStrings.append(visibility);
    columnStrings.append(color);
    columnStrings.append(volume);

    return columnStrings;
}

/*void RrmTreeModel::placeItem(TreeItem *itemParent, int itemNumber, int position, QString itemPrefix, QString volume)
{
    qDebug() << "Item number: " << itemNumber << ", color: " << m_colorList.at(itemNumber);
    QString name = itemPrefix;
    name.append(QString::number(itemNumber + 1));
    //QStringList columnStrings = getColumnString(name, "true", stub::getSurfaceQStringColor(itemNumber + 1), volume);
    QStringList columnStrings = getColumnString(name, "true", m_colorList.at(itemNumber), volume);
    TreeItem* item = createItem(columnStrings, position, itemParent, itemNumber + 1);

}*/

void RrmTreeModel::placeItem(TreeItem* itemParent, int itemNumber, int position, QString itemName, QString volume)
{
    //qDebug() << "Item number: " << itemNumber << ", color: " << m_colorList.at(itemNumber);
    QStringList columnStrings = getColumnString(itemName, "true", m_colorList.at(itemNumber), volume);
    TreeItem* item = createItem(columnStrings, position, itemParent, itemNumber + 1);

}

TreeItem* RrmTreeModel::placeDomain(TreeItem* itemParent, int itemNumber, int position, QString itemName, QString volume, QString color, bool visibility)
{
    //qDebug() << "Item number: " << itemNumber << ", color: " << m_domainColorList.at(itemNumber);
    QString visibilityText = visibility ? "true" : "false";
    QStringList columnStrings = getColumnString(itemName, visibilityText, color, volume);
    TreeItem* item = createItem(columnStrings, position, itemParent, itemNumber + 1);

    return item;
}
TreeItem* RrmTreeModel::getItem(const QModelIndex& index) const
{
    if (index.isValid()) {
        TreeItem* item = static_cast<TreeItem*>(index.internalPointer());
        if (item)
            return item;
    }
    return rootItem;
}


QModelIndex RrmTreeModel::getIndexByNode(TreeItem* item)
{
    QVector<int> positions;
    QModelIndex result;
    if (item) {
        do
        {
            int pos = item->row();
            positions.append(pos);
            item = item->parentItem();
        } while (item != nullptr);


        for (int i = positions.size() - 2; i >= 0; i--)
        {
            result = index(positions[i], 0, result);
        }
    }
    return result;
}

QList<QVariant> RrmTreeModel::addRowData(QStringList columnStrings, int position, TreeItem* parent, int surface_id)
{
    QList<QVariant> columnData;
    //for (int column = 0; column < 4; ++column)
    for (int column = 0; column < columnStrings.size(); ++column)
    {
        //columnData << newCustomType(columnStrings[column], position, surface_id, parent_type_id);
        columnData << newCustomType(columnStrings[column]);

    }

    /*if (position > m_indentations.last()) {
        // The last child of the current parent is now the new parent
        // unless the current parent has no children.

        if (parent->childCount() > 0) {
            m_parentsList << parent->child(parent->childCount() - 1);
            m_indentations << position;
        }
    }
    else {
        while (position < m_indentations.last() && m_parentsList.count() > 0) {
            m_parentsList.pop_back();
            m_indentations.pop_back();
        }
    }
    */
    return columnData;
}

QList<QVariant> RrmTreeModel::addRowData(QStringList columnStrings, int position, TreeItem* parent, int element_id, int parent_id)
{
    QList<QVariant> columnData;
    //for (int column = 0; column < 4; ++column)
    for (int column = 0; column < columnStrings.size(); ++column)
    {
        //columnData << newCustomType(columnStrings[column], position, surface_id, parent_type_id);
        columnData << newCustomType(columnStrings[column]);

    }

    return columnData;
}

void RrmTreeModel::removeChildren(TreeItem* parent)
{
    int numberOfRows = parent->childCount();

    if (numberOfRows >= 1)
    {
        //Observation: removeRows method deletes leaves of each child item but the clear method 
        //deletes all child items at one go. So, using the clear method for now to achieve speed
        beginResetModel();
        parent->clear();
        //QModelIndex parentIndex = getIndexByNode(parent);
        //removeRows(0, numberOfRows, parentIndex);
        endResetModel();

    }


}

bool RrmTreeModel::removeRows(int position, int rows, const QModelIndex& parent)
{
    TreeItem* parentItem = getItem(parent);
    if (!parentItem)
        return false;

    beginRemoveRows(parent, position, position + rows - 1);
    const bool success = parentItem->removeChildren(position, rows);
    endRemoveRows();

    return success;
}

TreeItem* RrmTreeModel::getNodeByIndex(const QModelIndex& index)
{
    if (!index.isValid())
        return nullptr;
    return static_cast<TreeItem*>(index.internalPointer());
}

void RrmTreeModel::updateSurfaceChildren(std::vector<int> surfaceList)
{
    //qDebug() << "RrmTreeModel: Going to update the Tree:" << surfaceList;

    //Load the color list
    //m_colorList = m_vizHandler->getColorList();

    m_indentations << 0;

    int position = 4;//level-2

    removeChildren(m_surfaceParent);
    removeChildren(m_structural_surfaceParent);

    bool firstStratigarphicSurface = true;
    bool firstStructuralSurface = true;

    int i = 0;
    position = 4;
    //qDebug() << "Number of rows of the surfaceParent:" << surfaceList.size();

    beginInsertRows(getIndexByNode(m_surfaceParent), 0, surfaceList.size());
    foreach(int eachSurfaceCategory, surfaceList)
    {
        if (eachSurfaceCategory == 0) //for stratigraphic surfaces
        {
            if (firstStratigarphicSurface)
            {
                removeChildren(m_surfaceParent);
                firstStratigarphicSurface = false;
            }

            setItemData(i, position, 1, "Stratigraphic");

            QString name = "Horizon";
            name.append(QString::number(i + 1));

            placeItem(m_surfaceParent, i, position, name, "");
        }
        else if (eachSurfaceCategory == 1) //for structural surfaces
        {
            if (firstStructuralSurface)
            {
                removeChildren(m_structural_surfaceParent);
                firstStructuralSurface = false;
            }

            setItemData(i, position, 2, "Structural");

            QString name = "Surface";
            name.append(QString::number(i + 1));

            placeItem(m_structural_surfaceParent, i, position, name, "");

        }
        i++;
    }
    endInsertRows();

}

/*
void RrmTreeModel::updateRegions()
{
    //Load volume list
    std::vector<double> volume_list = m_model3dHandler->getRegionList();

    if(m_regionParent->childCount() > 0)
        removeChildren(m_regionParent);

    if (volume_list.size() > 0)
    {
        //bool firstRegion = true;
        int position = 4;

        beginInsertRows(getIndexByNode(m_regionParent), 0, volume_list.size());

        for (int i = 0; i < volume_list.size(); i++)
        {

            setItemData(i, position, 3, "Region");

            QString name = "Region";
            name.append(QString::number(i + 1));

            placeItem(m_regionParent, i, position, name, QString::number(volume_list[i]));
        }


        endInsertRows();
    }

    //qDebug() << "Bingo:2 - Treeview update region list done" ;
}
*/
/*void RrmTreeModel::update(std::vector<int> surfaceList)
{
    //updateRegions();
    m_surfaceList = surfaceList;
    //updateSurfaceChildren(surfaceList);
    emit(treeObjectsUpdated());
}*/

void RrmTreeModel::updateSurfaceNodes(std::vector<QString> colorMap, std::vector<bool> visibilityMap)
{
    //qDebug() << "RrmTreeModel::updateSurfaceNodes: ";
    //qDebug() << colorMap;
    //qDebug() << m_surfaceList;

    m_colorList = colorMap;
    //m_visibilityList = visibilityMap;
    updateSurfaceChildren(m_surfaceList);

    emit(expandTreeNodes());
}

void RrmTreeModel::removeRegions()
{
    removeChildren(m_regionParent);
    emit(expandTreeNodes());
}

void RrmTreeModel::reset()
{
    if (m_regionParent->childCount() > 0)
        removeChildren(m_regionParent);

    if (m_surfaceParent->childCount() > 0)
        removeChildren(m_surfaceParent);

    if (m_structural_surfaceParent->childCount() > 0)
        removeChildren(m_structural_surfaceParent);

    if (m_domainParent->childCount() > 0)
        removeChildren(m_domainParent);
       
}

void RrmTreeModel::setItemData(int id, int position, int type_id, QString type_name)
{
    m_node_element.data_id = id;
    m_node_element.indentation = position;
    //m_node_element.data_name = prefix.append(QString::number(id + 1));
    m_node_element.type_id = type_id;
    m_node_element.type_name = type_name;
}

void RrmTreeModel::setItemData(int id, int position, int type_id, QString type_name, int parent_id)
{
    m_node_element.data_id = id;
    m_node_element.indentation = position;
    //m_node_element.data_name = prefix.append(QString::number(id + 1));
    m_node_element.type_id = type_id;
    m_node_element.type_name = type_name;
    m_node_element.parent_id = parent_id;
}

void RrmTreeModel::setItemData(int id, int position, int type_id, QString type_name, int parent_id, bool flattened)
{
    m_node_element.data_id = id;
    m_node_element.indentation = position;
    //m_node_element.data_name = prefix.append(QString::number(id + 1));
    m_node_element.type_id = type_id;
    m_node_element.type_name = type_name;
    m_node_element.parent_id = parent_id;
    m_node_element.flat_status = flattened;
}

void RrmTreeModel::updateSingleSurfaceNodeColor(int index, QString color, int parent_type_id)
{
    for (int i = 0; i < m_colorList.size(); i++)
    {
        if (i == index)
        {
            //qDebug() << "RrmTreeModel::updateSingleSurfaceNodeColor - Matched index: " << index;
            m_colorList[i] = color;

            break;
        }
    }

    //qDebug() << m_colorList;

}

void RrmTreeModel::updateDomainNodes(std::vector< std::shared_ptr<Domain> > domains)
{
    if (m_domainList.size() != 0)
        m_domainList.clear();
    try {
        for (const std::shared_ptr<Domain>& domain : domains)
        {
            m_domainList.push_back(domain);
        }

        emit(domainNodesUpdated());
    }
    catch (int err) {
        //qDebug() << "RrmTreeModel::updateDomainNodes: error";
    }

}

void RrmTreeModel::reloadDomains()
{
    //qDebug() << "RrmTreeModel::reloadDomains()";
    //removeChildren(m_domainParent);

    removeDomainChildren();

    try {
        for (const std::shared_ptr<Domain>& domain : m_domainList)
        {
            TreeItem* newDomainItem = addDomain(domain->getName(), domain->getId(), domain->getVolume(), domain->getColor(), domain->getVisibility());

            std::vector<std::shared_ptr<Region>> domain_child_regions = domain->getRegionPointerList();

            if (domain_child_regions.size() != 0)
            {
                for (const std::shared_ptr<Region>& region : domain_child_regions)
                {
                    //Add domain child regions
                    addDomainChild(newDomainItem, region->getName(), region->getId(), region->getVolume(), region->getColor(), domain->getId(), region->getVisibility());
                }

            }
        }
        emit(expandTreeNodes());

    }
    catch (int err) {
        //qDebug() << "RrmTreeModel::reloadDomains: error";
    }
}

void RrmTreeModel::removeDomainChildren()
{
    int numberOfRows = m_domainParent->childCount();
    //qDebug() << "RrmTreeModel::removeDomainChildren(): before - " << numberOfRows;
    if (numberOfRows > 0)
    {
        beginResetModel();
        m_domainParent->clear();
        endResetModel();
    }
    numberOfRows = m_domainParent->childCount();
    //qDebug() << "RrmTreeModel::removeDomainChildren(): after - " << numberOfRows;
}

//void RrmTreeModel::addDomain(QString domainName, int index)
TreeItem* RrmTreeModel::addDomain(QString domainName, int index, double volume, QString color, bool visibility)
{
    //qDebug() << "RrmTreeModel::addDomain";


    int position = 4;

    //qDebug() << "RrmTreeModel::addDomain - total volume = " << getTotalVolumeOfRegions() << ", this region voulme=" << volume << "string form: " << QString::number(volume, 'G', 3);
    double proportion = (volume / getTotalVolumeOfRegions()) * 100;
    //qDebug() << "RrmTreeModel::addDomain - proportion = " << proportion;

    //beginInsertRows(getIndexByNode(m_domainParent), index, index);
    setItemData(index, position, 4, "Domain");
    //TreeItem* domainItem = placeDomain(m_domainParent, index, position, domainName, QString::number(volume, 'G', 3), color, visibility);
    TreeItem* domainItem = placeDomain(m_domainParent, index, position, domainName, QString::number(volume, 'G', 3) + " (" + QString::number(proportion, 'G', 3) + "%)", color, visibility);

    //endInsertRows();
    endResetModel();
    return domainItem;
}

void RrmTreeModel::updateRegionNodes(std::vector< std::shared_ptr<Region> > regions)
{
    if (m_regionList.size() != 0)
        m_regionList.clear();
    try {
        //for (const std::shared_ptr<Region>& region : regions)
        for (int i= regions.size()-1; i>=0; i--)
        {
            m_regionList.push_back(regions[i]);
        }

        emit(regionNodesUpdated());
    }
    catch (int err) {
        //qDebug() << "RrmTreeModel::updateRegionNodes: error";
    }
}

void RrmTreeModel::reloadRegions()
{
    //qDebug() << "RrmTreeModel::reloadRegions()";

    removeChildren(m_regionParent);

    try {
        //for (int i = m_regionList.size() - 1; i >= 0; i--)
        for (const std::shared_ptr<Region>& region : m_regionList)
        {
            //const std::shared_ptr<Region>& region = m_regionList[i];
            addRegion(region->getName(), region->getId(), region->getVolume(), region->getColor(), region->getVisibility());
        }
        emit(expandTreeNodes());

    }
    catch (int err) {
        //qDebug() << "RrmTreeModel::reloadRegions: error";
    }
}

void RrmTreeModel::addRegion(QString regionName, int index, double volume, QString color, bool visibility)
{
    //qDebug() << "RrmTreeModel::addRegion";


    int position = 4;

    //beginInsertRows(getIndexByNode(m_regionParent), index, index);

    //qDebug() << "RrmTreeModel::addRegion - total volume = " << getTotalVolumeOfRegions() << ", this region voulme=" << volume << "string form: " << QString::number(volume, 'G', 3);
    double proportion = (volume / getTotalVolumeOfRegions()) * 100;
    //qDebug() << "RrmTreeModel::addRegion - proportion = " << proportion;

    setItemData(index, position, 3, "Region");
    //placeRegion(m_regionParent, index, position, regionName, QString::number(volume, 'G', 3), color, visibility);
    placeRegion(m_regionParent, index, position, regionName, QString::number(volume, 'G', 3) + " (" + QString::number(proportion, 'G', 3) + "%)", color, visibility);

    //endInsertRows();
    endResetModel();
}

void RrmTreeModel::placeRegion(TreeItem* itemParent, int itemNumber, int position, QString regionName, QString volume, QString color, bool visibility)
{
    QString visibilityText = visibility ? "true" : "false";

    QStringList columnStrings = getColumnString(regionName, visibilityText, color, volume);
    TreeItem* item = createItem(columnStrings, position, itemParent, itemNumber);
}

void RrmTreeModel::updateSurfaceItems(std::vector< std::shared_ptr<SurfaceItem> > surfaces)
{
    if (m_surfaceItemList.size() != 0)
        m_surfaceItemList.clear();
    try {
        for (const std::shared_ptr<SurfaceItem>& surface : surfaces)
        {
            m_surfaceItemList.push_back(surface);
        }

        emit(surfaceNodesItemsUpdated());
    }
    catch (int err) {
        qDebug() << "RrmTreeModel::updateSurfaceNodes: error";
    }
}

void RrmTreeModel::reloadSurfaces()
{
    //qDebug() << "RrmTreeModel::reloadSurfaces()";

    removeChildren(m_surfaceParent);
    removeChildren(m_structural_surfaceParent);

    try {
        for (const std::shared_ptr<SurfaceItem>& surface : m_surfaceItemList)
        {
            if (surface->getCategory() == 1)//if (surface->getCategory() == 0)
            {
                //qDebug() << "RrmTreeModel::reloadSurfaces(): Stratigraphic " << surface->getName() <<", "<< surface->getId()  <<", " << surface->getCategory() <<", "<< surface->getColor() << ", visible:" <<surface->getVisibility();
                TreeItem* surfaceItem = addStratigraphy(surface->getName(), surface->getId(), surface->getCategory(), surface->getColor(), surface->getVisibility(), surface->isFlattened());

                std::vector< std::shared_ptr<SurfaceItem> > surfaceChildList = surface->getSurfaceChildList();
                for (const std::shared_ptr<SurfaceItem>& surfaceChild : surfaceChildList)
                {
                    if (surfaceChild->getCategory() == 1)
                    {
                        addSurfaceChild(surfaceItem, surfaceChild->getName(), surfaceChild->getId(), surfaceChild->getCategory(), surfaceChild->getColor(), surfaceChild->getVisibility(), 8, 6, "STRATIGRAPHIC_CHILD", surface->getId(), surfaceChild->isFlattened());
                    }
                }
            }
            else if (surface->getCategory() == 2)//else if (surface->getCategory() == 1)
                addStructure(surface->getName(), surface->getId(), surface->getCategory(), surface->getColor(), surface->getVisibility());
        }
        emit(expandTreeNodes());

    }
    catch (int err) {
        //qDebug() << "RrmTreeModel::reloadSurfaces: error";
    }
}

TreeItem* RrmTreeModel::addStratigraphy(QString surfaceName, int index, int category, QString color, bool visibility, bool flat_status)
{
    //qDebug() << "RrmTreeModel::addStratigraphy";


    int position = 4;

    //beginInsertRows(getIndexByNode(m_regionParent), index, index);

    //setItemData(index, position, 1, "Stratigraphic"); // Commenting this out to add flat_status of a stratigraphic surface with default parent_id 1
    setItemData(index, position, 1, "Stratigraphic", -1, flat_status);
    TreeItem * surfaceItem = placeSurface(m_surfaceParent, index, position, surfaceName, color, visibility);

    //endInsertRows();
    endResetModel();

    return surfaceItem;

}

void RrmTreeModel::addStructure(QString surfaceName, int index, int category, QString color, bool visibility)
{
    //qDebug() << "RrmTreeModel::addStructure";


    int position = 4;

    //beginInsertRows(getIndexByNode(m_regionParent), index, index);

    setItemData(index, position, 2, "Structural");
    placeSurface(m_structural_surfaceParent, index, position, surfaceName, color, visibility);

    //endInsertRows();
    endResetModel();
}

TreeItem* RrmTreeModel::placeSurface(TreeItem* itemParent, int itemNumber, int position, QString itemName, QString color, bool visibility)
{
    //qDebug() << "Item number: " << itemNumber << ", color: " << m_colorList.at(itemNumber);
    QString visibilityText = visibility ? "true" : "false";
    QStringList columnStrings = getColumnString(itemName, visibilityText, color, "");
    TreeItem* item = createItem(columnStrings, position, itemParent, itemNumber);

    return item;
}

void RrmTreeModel::addDomainChild(TreeItem* parent, QString name, int index, double volume, QString color, int domain_id, bool visibility)
{
    qDebug() << "RrmTreeModel::addDomainChild: Domain id = " << domain_id;


    int position = 8;

    //qDebug() << "RrmTreeModel::addDomainChild - total volume = " << getTotalVolumeOfRegions() << ", this domain voulme=" << volume << "string form: " << QString::number(volume, 'G', 3);
    double proportion = (volume / getTotalVolumeOfRegions()) * 100;
    //qDebug() << "RrmTreeModel::addDomainChild - proportion = " << proportion;

    //beginInsertRows(getIndexByNode(m_regionParent), index, index);

    //setItemData(index, position, 5, "DomainChild"); //Parent-type-id of a domain child is 5
    setItemData(index, position, 5, "DomainChild", domain_id); //Parent-type-id of a domain child is 5
    //placeDomainChild(parent, index, position, name, QString::number(volume), color, QString::number(domain_id), visibility);
    placeDomainChild(parent, index, position, name, QString::number(volume, 'G', 3) + " (" + QString::number(proportion, 'G', 3) + "%)", color, QString::number(domain_id), visibility);

    //endInsertRows();
    endResetModel();
}

void RrmTreeModel::placeDomainChild(TreeItem* itemParent, int itemNumber, int position, QString regionName, QString volume, QString color, QString domainId, bool visibility)
{
    //qDebug() << "Item number: " << itemNumber << ", color: " << m_domainColorList.at(itemNumber);
    QString visibilityText = visibility ? "true" : "false";

    QStringList columnStrings = getColumnString(regionName, visibilityText, color, volume, domainId);
    TreeItem* item = createItem(columnStrings, position, itemParent, itemNumber);
}

QStringList RrmTreeModel::getColumnString(QString name, QString visibility, QString color, QString volume, QString parent_id)
{
    QStringList columnStrings;

    columnStrings.append(name);
    columnStrings.append(visibility);
    columnStrings.append(color);
    columnStrings.append(volume);
    columnStrings.append(parent_id);

    return columnStrings;
}

void RrmTreeModel::clearAllParentNodes()
{
    beginResetModel();
    removeRows(0, 4, getIndexByNode(rootItem));
    endResetModel();
}

void RrmTreeModel::updateParentNode(int parentTypeId, bool visibility)
{
    switch (parentTypeId)
    {
    case 1:
        clearAllParentNodes();
        m_stratigraphy_visibility = visibility;
        break;
    case 2:
        clearAllParentNodes();
        m_structural_visibility = visibility;
        break;
    case 3:
        clearAllParentNodes();
        m_region_visibility = visibility;
        break;
    case 4:
        clearAllParentNodes();
        m_domain_visibility = visibility;
        break;
    }

    initTreeData();
    reloadSurfaces();
    reloadRegions();
    reloadDomains();
    //emit(expandTreeNodes());
}

QVariantList RrmTreeModel::getChildrenIndices(const QModelIndex& index)
{
    QVariantList indexes;
    TreeItem* parent = getNodeByIndex(index);

    for (int i = 0; i != parent->childCount(); ++i) {
        TreeItem* child = parent->child(i);
        indexes.push_back(createIndex(i, 0, reinterpret_cast<quintptr>(child)));
    }

    return indexes;

}

double RrmTreeModel::getTotalVolumeOfRegions()
{
    //qDebug() << "VisualizationHandler::getTotalVolumeofRegions: ";

    double totalVolume = 0.0;
    for (const std::shared_ptr<Region>& region : m_regionList)
    {
        totalVolume += region->getVolume();

    }

    return totalVolume;
}
                  
void RrmTreeModel::addSurfaceChild(TreeItem* surfaceItem, QString surfaceName, int index, int category, QString color, bool visibility, int position, int node_type_id, QString node_type_name, int parent_surface_id, bool flat_status)
{
    /*setItemData(index, position, node_type_id, node_type_name);
    placeSurface(m_surfaceParent, index, position, surfaceName, color, visibility);

    endResetModel();
    */

    qDebug() << "RrmTreeModel::addSurfaceChild: Surface id = " << index;

    
    //setItemData(index, position, node_type_id, node_type_name, parent_surface_id);  // adding a child of a surface
    setItemData(index, position, node_type_id, node_type_name, parent_surface_id, flat_status);  // adding a child of a surface with flat_status

    //placeDomainChild(parent, index, position, name, QString::number(volume), color, QString::number(domain_id), visibility);
    placeDomainChild(surfaceItem, index, position, surfaceName, "", color, QString::number(parent_surface_id), visibility);

    //endInsertRows();
    endResetModel();
}