#include "ItemElementType.h"

ItemElementType::ItemElementType(QObject *parent) : QObject(parent), myIndentation(0)
{
}

ItemElementType::ItemElementType(const ItemElementType &other) : ItemElementType()
{
    myText = other.myText;
    myIndentation = other.myIndentation;
    m_dataId = other.m_dataId;
}

ItemElementType& ItemElementType::operator=(const ItemElementType& newElementType)
{
    myText = newElementType.myText;
    myIndentation = newElementType.myIndentation;
    m_dataId = newElementType.m_dataId;

    return *this;
}

ItemElementType::~ItemElementType()
{
}

QString ItemElementType::text()
{
    return myText;
}

void ItemElementType::setText(QString text)
{
    myText = text;
    emit textChanged();
}

int ItemElementType::indentation()
{
    return myIndentation;
}

void ItemElementType::setIndentation(int indentation)
{
    myIndentation = indentation;
    emit indentationChanged();
}

int ItemElementType::dataId()
{
    return m_dataId;
}

void ItemElementType::setDataId(int data_id)
{
    m_dataId = data_id;
    emit dataIdChanged();
}

int ItemElementType::parentTypeId()
{
    return m_parentType_id;
}

void ItemElementType::setParentTypeId(int parentTypeId)
{
    m_parentType_id = parentTypeId;
    emit parentTypeIdChanged();
}

QString ItemElementType::parentTypeName()
{
    return m_parentType_name;
}

void ItemElementType::setParentTypeName(QString parentTypeName)
{
    m_parentType_name = parentTypeName;
    emit parentTypeNameChanged();
}

int ItemElementType::parentId()
{
    return m_parent_id;
}

void ItemElementType::setParentId(int parent_id)
{
    m_parent_id = parent_id;
    emit parentIdChanged();
}

bool ItemElementType::flatStatus()
{
    return m_flat;
}

void ItemElementType::setFlatStatus(bool status)
{
    m_flat = status;
    emit flatStatusChanged();
}