#include "Region.h"
#include "Domain.h"

Domain::Domain(int id, QString name, QString desc) 
	:m_id(id),m_name(name), m_description(desc)
{
}

/*void Domain::addToRegionList(int region)
{
	m_regionList.push_back(region);
}*/

void Domain::addToRegionList(Region region)
{
	m_regionList.push_back(region);
}

void Domain::addToRegionPointerList(std::shared_ptr<Region> r)
{
	m_regionPointerList.push_back(r);
}

void Domain::reName(QString newName)
{
	m_name = newName;
}

void Domain::setDescription(QString desc)
{
	m_description = desc;
}

std::vector<Region> Domain::getRegionList()
{
	return m_regionList;
}

std::vector<std::shared_ptr<Region>> Domain::getRegionPointerList()
{
	return m_regionPointerList;
}

void Domain::setRegionPointerList(std::vector<std::shared_ptr<Region>> regionList)
{
	if (m_regionPointerList.size() != 0)
		m_regionPointerList.clear();
	m_regionPointerList = regionList;
}

int Domain::getId()
{
	return m_id;
}

QString Domain::getDescription()
{
	return m_description;
}

QString Domain::getName()
{
	return m_name;
}
void Domain::clearRegionPointerList()
{
	if(m_regionPointerList.size() !=0)
		m_regionPointerList.clear();
}

void Domain::setColor(QString color)
{
	m_color = color;
}
QString Domain::getColor()
{
	return m_color;
}

void Domain::setVolume(double volume)
{
	m_volume += volume;
}
double Domain::getVolume()
{
	return m_volume;
}

bool Domain::getVisibility()
{
	return m_visibility;
}

void Domain::setVisibility(bool visibilty)
{
	m_visibility = visibilty;
}
