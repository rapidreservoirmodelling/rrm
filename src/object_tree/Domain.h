#ifndef DOMAIN_H
#define DOMAIN_H

#include <QObject>
#include <vector>
#include <memory>

class Region;

class Domain
{
public:
    Domain(int id,QString name, QString desc);
    
    void addToRegionList(Region region);
    void addToRegionPointerList(std::shared_ptr<Region> r);
    void reName(QString newName);
    void setDescription(QString desc);
    std::vector<Region> getRegionList();
    
    std::vector<std::shared_ptr<Region>> getRegionPointerList();
    void setRegionPointerList(std::vector<std::shared_ptr<Region>> regionList);
        
    int getId();
    QString getDescription();
    QString getName();
    void clearRegionPointerList();
    void setColor(QString color);
    QString getColor();

    void setVolume(double volume);
    double getVolume();

    bool getVisibility();
    void setVisibility(bool visibilty);
        
private:
    int m_id;
    QString m_name = "";
    QString m_description = "";
    double m_volume = 0.0;
    QString m_color ="";
    bool m_visibility;

    std::vector<Region> m_regionList;
    std::vector< std::shared_ptr<Region> > m_regionPointerList = {};
    
};

#endif // DOMAIN_H
