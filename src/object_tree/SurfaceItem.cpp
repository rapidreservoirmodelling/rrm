#include "SurfaceItem.h"

#include <Qdebug.h>

SurfaceItem::SurfaceItem(int id, QString name, QString desc, QString color, int category, bool visibility)
	:m_id(id), m_name(name), m_description(desc), m_color(color), m_category(category), m_visibility(visibility)
{
}

SurfaceItem::SurfaceItem(const SurfaceItem& obj)
{
	m_id = obj.m_id;
	m_name = obj.m_name;
	m_description = obj.m_description;
	m_color = obj.m_color;
	m_category = obj.m_category;
	m_visibility = obj.m_visibility;
}

SurfaceItem& SurfaceItem::SurfaceItem::operator=(const SurfaceItem& obj)
{
	m_id = obj.m_id;
	m_name = obj.m_name;
	m_description = obj.m_description;
	m_color = obj.m_color;
	m_category = obj.m_category;
	m_visibility = obj.m_visibility;

	return *this;
}

void SurfaceItem::reName(QString newName)
{
	m_name = newName;
}

void SurfaceItem::setDescription(QString desc)
{
	m_description = desc;
}

int SurfaceItem::getId()
{
	return m_id;
}

QString SurfaceItem::getDescription()
{
	return m_description;
}

QString SurfaceItem::getName()
{
	return m_name;
}

QString SurfaceItem::getColor()
{
	return m_color;
}

void SurfaceItem::setColor(QString color)
{
	m_color = color;
}

double SurfaceItem::getCategory()
{
	return m_category;
}

void SurfaceItem::setCategory(int category)
{
	m_category = category;
}

void SurfaceItem::setVisibility(bool visibility)
{
	////qDebug() << "SurfaceItem::setVisibility: " << "new visibility:" << visibility;
	m_visibility = visibility;
}

bool SurfaceItem::getVisibility()
{
	return m_visibility;
}

std::vector<std::shared_ptr<SurfaceItem>> SurfaceItem::getSurfaceChildList()
{
	return m_childList;
}

void SurfaceItem::setChildList(std::vector<std::shared_ptr<SurfaceItem>> surfaceList)
{
	if (m_childList.size() != 0)
		m_childList.clear();
	m_childList = surfaceList;
}


void SurfaceItem::addToChildList(std::shared_ptr<SurfaceItem> surface)
{
	m_childList.push_back(surface);
}

void SurfaceItem::clearChildList()
{
	m_childList.clear();
}

int SurfaceItem::getParentId()
{
	return m_parent_id;
}

void SurfaceItem::setParentId(int parentId)
{
	m_parent_id = parentId;
}

int SurfaceItem::getGroupId()
{
	return m_group_id;
}

void SurfaceItem::setGroupId(int groupId)
{
	m_group_id = groupId;
}

bool SurfaceItem::isFlattened()
{
	return m_flattened;
}

void SurfaceItem::flatten(bool flag)
{
	m_flattened = flag;
}