#ifndef COMPOUNDSURFACE_H
#define COMPOUNDSURFACE_H

#include <QObject>
#include <vector>
#include <memory>

class SurfaceItem;

class CompoundSurface
{
public:
    CompoundSurface(int id, QString name, QString desc);
    
    void addToSurfacePointerList(std::shared_ptr<SurfaceItem> r);
    void reName(QString newName);
    void setDescription(QString desc);
    
    std::vector<std::shared_ptr<SurfaceItem>> getSurfacePointerList();
    void setRegionPointerList(std::vector<std::shared_ptr<SurfaceItem>> regionList);
        
    int getId();
    QString getDescription();
    QString getName();
    void clearSurfacePointerList();
    void setColor(QString color);
    QString getColor();

    //void setVolume(double volume);
    //double getVolume();

    bool getVisibility();
    void setVisibility(bool visibilty);
        
private:
    int m_id;
    QString m_name;
    QString m_description;
    //double m_volume = 0.0;
    QString m_color;
    bool m_visibility;

    std::vector< std::shared_ptr<SurfaceItem> > m_surfacePointerList;
    
};

#endif // COMPOUNDSURFACE_H
