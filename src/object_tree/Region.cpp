#include "Region.h"

Region::Region(int id, QString name, QString desc, QString color, double volume, bool visibility)
	:m_id(id),m_name(name), m_description(desc), m_color(color), m_volume(volume), m_visibility(visibility)
{
}

void Region::reName(QString newName)
{
	m_name = newName;
}

void Region::setDescription(QString desc)
{
	m_description = desc;
}

int Region::getId()
{
	return m_id;
}

QString Region::getDescription()
{
	return m_description;
}

QString Region::getName()
{
	return m_name;
}

QString Region::getColor()
{
	return m_color;
}

void Region::setColor(QString color)
{
	m_color = color;
}

double Region::getVolume()
{
	return m_volume;
}

void Region::setVolume(double volume)
{
	m_volume = volume;
}

bool Region::getVisibility()
{
	return m_visibility;
}

void Region::setVisibility(bool visibilty)
{
	m_visibility = visibilty;
}
