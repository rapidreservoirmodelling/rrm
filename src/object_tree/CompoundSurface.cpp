#include "SurfaceItem.h"
#include "CompoundSurface.h"

CompoundSurface::CompoundSurface(int id, QString name, QString desc) 
	:m_id(id),m_name(name), m_description(desc)
{
}

void CompoundSurface::addToSurfacePointerList(std::shared_ptr<SurfaceItem> r)
{
	m_surfacePointerList.push_back(r);
}

void CompoundSurface::reName(QString newName)
{
	m_name = newName;
}

void CompoundSurface::setDescription(QString desc)
{
	m_description = desc;
}

std::vector<std::shared_ptr<SurfaceItem>> CompoundSurface::getSurfacePointerList()
{
	return m_surfacePointerList;
}

void CompoundSurface::setRegionPointerList(std::vector<std::shared_ptr<SurfaceItem>> surfaceList)
{
	if (m_surfacePointerList.size() != 0)
		m_surfacePointerList.clear();
	m_surfacePointerList = surfaceList;
}

int CompoundSurface::getId()
{
	return m_id;
}

QString CompoundSurface::getDescription()
{
	return m_description;
}

QString CompoundSurface::getName()
{
	return m_name;
}
void CompoundSurface::clearSurfacePointerList()
{
	if(m_surfacePointerList.size() !=0)
		m_surfacePointerList.clear();
}

void CompoundSurface::setColor(QString color)
{
	m_color = color;
}
QString CompoundSurface::getColor()
{
	return m_color;
}

/*void CompoundSurface::setVolume(double volume)
{
	m_volume += volume;
}
double CompoundSurface::getVolume()
{
	return m_volume;
}
*/
bool CompoundSurface::getVisibility()
{
	return m_visibility;
}

void CompoundSurface::setVisibility(bool visibilty)
{
	m_visibility = visibilty;
}
