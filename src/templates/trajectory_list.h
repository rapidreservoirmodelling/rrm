#ifndef TRAJECTORY_LIST_H
#define TRAJECTORY_LIST_H

#include <QObject>
#include <QVector>

struct TrajectoryItem
{
	bool selected;
	QString description;
};

class TrajectoryList : public QObject
{
	Q_OBJECT 

public:
	explicit TrajectoryList(QObject* parent = nullptr);

	QVector<TrajectoryItem> items() const;

	bool setItemAt(int index, const TrajectoryItem& item);

signals:
	void preItemAppended();
	void postItemAppended();
	void preItemRemoved(int index);
	void postItemRemoved();
	
public slots:
	void appendItem();
	void removeCompletedItems();
	void resetList();

private:
	QVector<TrajectoryItem> mItems;
};

#endif // TRAJECTORY_LIST_H
