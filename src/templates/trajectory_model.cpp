#include "trajectory_model.h"
#include "trajectory_list.h"

#include <QDebug>

TrajectoryModel::TrajectoryModel(QObject* parent)
	: QAbstractListModel(parent)
	, mList(nullptr)
{
	
}


int TrajectoryModel::rowCount(const QModelIndex& parent) const
{
	if (parent.isValid() || !this->mList)
		return 0;

	return this->mList->items().size();
}


QVariant TrajectoryModel::data(const QModelIndex& index, int role) const
{
	if (!index.isValid() || !this->mList)
		return QVariant();

	const TrajectoryItem item = this->mList->items().at(index.row());
	switch (role)
	{
	case SelectedRole: return QVariant(item.selected);
	case DescriptionRole: return QVariant(item.description);
	}

	return QVariant();
}


bool TrajectoryModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
	if (!this->mList)
		return false;

	TrajectoryItem item = this->mList->items().at(index.row());
	switch (role) {
	case SelectedRole:
		item.selected = value.toBool();
		break;
	case DescriptionRole:
		item.description = value.toString();
		break;
	}

	if (this->mList->setItemAt(index.row(), item)) {
		emit dataChanged(index, index, QVector<int>() << role);
		return true;
	}

	return false;
}


Qt::ItemFlags TrajectoryModel::flags(const QModelIndex& index) const
{
	if (!index.isValid())
		return Qt::NoItemFlags;

	return Qt::ItemIsEditable;
}


QHash<int, QByteArray> TrajectoryModel::roleNames() const
{
	QHash<int, QByteArray> names;
	names[SelectedRole] = "selected";
	names[DescriptionRole] = "description";
	return names;
}


TrajectoryList* TrajectoryModel::list() const
{
	return this->mList;
}


void TrajectoryModel::setList(TrajectoryList* list)
{
	beginResetModel();

	if (this->mList)
		this->mList->disconnect(this);

	this->mList = list;

	if (this->mList)
	{
		connect(this->mList, &TrajectoryList::preItemAppended, this, [=]() {
			const int index = this->mList->items().size();
			beginInsertRows(QModelIndex(), index, index);
			});

		connect(this->mList, &TrajectoryList::postItemAppended, this, [=]() {
			endInsertRows();
			});

		connect(this->mList, &TrajectoryList::preItemRemoved, this, [=](int index) {
			beginRemoveRows(QModelIndex(), index, index);
			});

		connect(this->mList, &TrajectoryList::postItemRemoved, this, [=]() {
			endRemoveRows();
			});
	}

	endResetModel();
}

