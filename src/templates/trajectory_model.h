#ifndef TRAJECTORY_MODEL_H
#define TRAJECTORY_MODEL_H

#include <QAbstractListModel>

class TrajectoryList;

class TrajectoryModel : public QAbstractListModel
{
	Q_OBJECT
	Q_PROPERTY(TrajectoryList* list READ list WRITE setList)

public:
	explicit TrajectoryModel(QObject* parent = nullptr);

	enum {
		SelectedRole = Qt::UserRole,
		DescriptionRole 
	};

	int rowCount(const QModelIndex& parent = QModelIndex()) const override;

	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

	bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

	Qt::ItemFlags flags(const QModelIndex& index) const override;

	virtual QHash<int, QByteArray> roleNames() const override;

	TrajectoryList* list() const;
	void setList(TrajectoryList* list);


private:
	TrajectoryList* mList;
};

#endif // TRAJECTORY_MODEL_H

