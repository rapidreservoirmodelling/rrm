#include "trajectory_list.h"

#include <QDebug>

TrajectoryList::TrajectoryList(QObject* parent) : QObject(parent)
{
	//mItems.append({ true, QStringLiteral("Trajectory 1") });
	//mItems.append({ false, QStringLiteral("Trajectory 2") });
}


QVector<TrajectoryItem> TrajectoryList::items() const
{
	return this->mItems;
}


bool TrajectoryList::setItemAt(int index, const TrajectoryItem& item)
{
	//- Verify if the index is valid:
	if (index < 0 || index > this->mItems.size())
		return false;

	//- Verify if the new item has identical values to the old one:
	const TrajectoryItem& oldItem = this->mItems.at(index);
	if (item.selected == oldItem.selected && item.description == oldItem.description)
		return false;

	this->mItems[index] = item;
	return true;
}


void TrajectoryList::appendItem()
{	
	emit preItemAppended();

	TrajectoryItem item;
	item.selected = false;
	QString text = "Template " + QString::number(this->mItems.size() + 1);
	item.description = text;
	this->mItems.append(item);

	emit postItemAppended();
}


void TrajectoryList::removeCompletedItems()
{
	for (int i = 0; i < this->mItems.size(); )
	{
		if (this->mItems.at(i).selected)
		{
			emit preItemRemoved(i);
			this->mItems.removeAt(i);
			emit postItemRemoved();
		}
		else {
			++i;
		}
	}
}

void TrajectoryList::resetList()
{	
	int mSize = this->mItems.size();
	for (int i = 0; i < mSize; i++)
	{
		emit preItemRemoved(0);
		this->mItems.removeAt(0);
		emit postItemRemoved();
	}
}