
/**
* @file sketch_stack.cpp
* @author Sicilia Judice
* @brief File containing the class SketchStack
*/

#include <QDebug>

#include "sketch_stack.h"


SketchStack::SketchStack()
{
    this->m_stack.clear();
}

SketchStack::SketchStack(const SketchStack& newStack) : SketchStack()
{
    this->m_stack = newStack.m_stack;
    this->last = newStack.last;
    this->is_empty = newStack.is_empty;
}

SketchStack& SketchStack::operator=(const SketchStack& newStack)
{
    this->m_stack = newStack.m_stack;
    this->last = newStack.last;
    this->is_empty = newStack.is_empty;

    return *this;
}

SketchStack::~SketchStack()
{
    this->clear();
}


bool SketchStack::isEmpty()
{
    return this->is_empty;
}


void SketchStack::clear()
{
    if( !this->is_empty )
    {
        for( auto i: this->m_stack)
        {
            i.clear();
        }
    }
    this->m_stack.clear();
    this->last = 0;
    this->is_empty = true;
}


size_t SketchStack::getStackSize()
{
    return this->m_stack.size();
}


Curve2D SketchStack::getCurve()
{
    if(this->is_empty)
        return Curve2D();
    else
        return this->m_stack[this->last];
}


Curve2D SketchStack::getCurve( size_t i )
{
    if(this->is_empty)
        return Curve2D();
    else
        return this->m_stack[i];
}


void SketchStack::setCurve(Curve2D newCurve)
{
    if(this->is_empty)
    {
        this->clear();
        this->push( Curve2D() );
    }

    this->m_stack[last] = newCurve;

    if (Comments::SBIM_SKETCH_STACK())
    {
        //qDebug() << "\n SketchStack::setCurve(): \t m_stack[last].size = " << m_stack[last].size();
    }
}


void SketchStack::push(Curve2D newCurve)
{
    this->m_stack.push_back(newCurve);
    this->last = this->m_stack.size()-1;
    this->is_empty = false;

    if (Comments::SBIM_SKETCH_STACK())
    {
        //qDebug() << "\n SketchStack::push():";
        //qDebug() << "\t last: " << last;
        //qDebug() << "\t size: " << m_stack.size();
    }
}


void SketchStack::pushLastCurve()
{
    if (Comments::SBIM_SKETCH_STACK())
    {
        //qDebug() << "\n SketchStack::pushLastCurve():";
        //qDebug() << "\t last: " << last;
        //qDebug() << "\t size: " << m_stack.size();
    }

    push(this->m_stack[this->last]);
}


void SketchStack::pop()
{
    if( !this->is_empty )
    {
        this->m_stack.pop_back();
        if(this->last > 0 )
            this->last = this->m_stack.size()-1;
        else
            this->is_empty = true;
    }
}


void SketchStack::popUntilCurrentPosition()
{
    if( !this->is_empty )
    {
        size_t condition = (this->m_stack.size()-1) - this->last;
        for( size_t i=0; i<condition; i++ )
        {
            this->m_stack.pop_back();
        }
    }
}


bool SketchStack::isSketchModified()
{
    if(this->m_stack.size() > 0 )
    {
        if(this->m_stack[this->last] != this->m_stack[this->last-1] )
            return true;
    }

    return false;
}


bool SketchStack::undo()
{
    if (this->is_empty)
    {
        return false;
    } else {
        if(this->last > 0 )
            this->last--;
        return true;
    }
}


bool SketchStack::redo()
{
    if(this->last < this->m_stack.size()-1 )
    {
        this->last++;
        return true;
    }

    return false;
}


void SketchStack::printInfo()
{
    if (Comments::SBIM_SKETCH_STACK())
    {
        //qDebug() << "\n SketchStack::printInfo():";
        //qDebug() << "\t Stack size:     " << m_stack.size();
        //qDebug() << "\t Last position:  " << last;
    }
}

