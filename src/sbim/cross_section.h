
/**
* @file cross_section.h
* @author Sicilia Judice
* @brief File containing the class CrossSection
*/

#ifndef CROSS_SECTION_H
#define CROSS_SECTION_H

#include <vector>

#include "sketch.h"
#include "Constants.h"

/**
* Class CrossSection will manage each sketch inside each cross section.
* 
*/

class CrossSection {

public:

	/**
	* Constructor.
	*/
	CrossSection();


	/**
	* Copy constructor.
	* @param newCrossSection a const reference to another cross section.
	*/
	CrossSection(const CrossSection& newCrossSection);


	/**
	* Assignment operator
	* @param newCrossSection a const reference to another cross section.
	*/
	CrossSection& operator=(const CrossSection& newCrossSection);


	/**
	* Destructor.
	*/
	~CrossSection();


	/**
	* Method to clear the cross section.
	* @return void
	*/
	void clear();


	/**
	* Method to set the ID of the cross section.
	* @param id is the numerical identification.
	* @return void
	*/
	void setId(unsigned int id);


	/**
	* Method to get the ID of the cross section.
	* @return unsigned int
	*/
	unsigned int getID();


	/**
	* Method to set the direction of the cross section.
	* @param direction is the numerical identification of the direction.
	* @return void
	*/
	void setDirection(unsigned int direction);


	/**
	* Method to get the direction of the cross section.
	* @return unsigned int
	*/
	unsigned int getDirection();


	/**
	* Method that returns the highlight index.
	* Returning -1 means that there is no sketch highlighted.
	* @return int
	*/
	int getHighlightIndex();


	/**
	* Method that returns the select index.
	* Returning -1 means that there is no sketch selected.
	* @return int
	*/
	int getSelectIndex();
	

	/**
	* Method that returns the flag isOverSketch.
	* @return bool
	*/
	bool getOverSketchFlag();
	

	/**
	* Method that return TRUE if the sketch is an oversketch.
	* @return bool.
	*/
	bool isOverSketch();


	/**
	* Method that return TRUE if the sketch is clear.
	* @return bool.
	*/
	bool isSketchClean();


	/**
	* Method to create a new sketch in the collection.
	* If there is no parameter, it will create a new empty sketch.
	* @param curve is the curve that represents the new sketch.
	* @return void
	*/
	void addNewSketch();
	void addNewSketch(Curve2D curve);

	//- DELETE
	/**
	* Method to create a new empty sketch in the collection.
	* @return void
	*/
	//void addSketch();


	/**
	* Method to create a new sketch in the collection.
	* For now it is adding always in the last position of the collection.
	* @param curve is the curve that represents the new sketch.
	* @return void
	*/
	void addSketch(Curve2D curve);


	/**
	* Method that copy previous sketch and add to the history of it.
	* It returns TRUE if the copy is done.
	* @return bool
	*/
	bool copyPreviousSketch();


	/**
	* Method that returns the current sketch in the cross section.
	* @param i is the index of the collection
	* @return Curve2D
	*/
	Curve2D getSketch(unsigned int i);
	Curve2D getSketch();


	/**
	* Method that returns the highlighted sketch in the cross section.
	* @return Curve2D
	*/
	Curve2D getHighlightedSketch();


	/**
	* Method that returns the selected sketch in the cross section.
	* @return Curve2D
	*/
	Curve2D getSelectedSketch();


	/**
	* Method that returns the current oversketch in the cross section.
	* @return Curve2D
	*/
	Curve2D getOverSketch();


	/**
	* Method that add a point to the sketch.
	* @param px is the x coordinate of the point.
	* @param py is the y coordinate of the point.
	* @return void
	*/
	void addSketchPoint(double px, double py);

	void addSketchLine(double ax, double ay, double bx, double by);

	void addSketchCircle(double cx, double cy, double radiusX, double radiusY);


	/**
	* Method that returns TRUE if oversketch is applied.
	* @return bool
	*/
	bool applyOverSketch();


	/**
	* Method to validate the sketch.
	* @return void
	*/
	void validateSketch();


	/**
	* Method to update sketch
	* @return bool
	*/
	bool updateSketch();


	/**
	* Method to reset the highlight index to -1.
	* @return void
	*/
	void resetHighlight();


	/**
	* Method to reset the select index to -1.
	* @return void
	*/
	void resetSelect();


	/**
	* Method that returns TRUE if there is a sketch to be highlighted.
	* @param mouse_x is the x coordinate of the mouse pointer.
	* @param mouse_y is the y coordinate of the mouse pointer.
	* @return bool
	*/
	bool highlightSketch(double mouse_x, double mouse_y);


	/**
	* Method that returns TRUE if the sketch is highlighted.
	* @return bool
	*/
	bool isSketchHighlighted();


	/**
	* Method that returns TRUE if the sketch is highlighted.
	* The select flag will be updated to the highlight flag.
	* @return bool
	*/
	bool selectHighlightedSketch();


	/**
	* Method that return TRUE if the sketch is selected.
	* @return bool
	*/
	bool isSketchSelected();
	bool isSketchSelected(double mouse_x, double mouse_y);


	/**
	* Method that return the number of sketches inside the cross section.
	* It is used when the user submits the sketch to create the 3D surface.
	* If there is only one sketch, its cross section and direction will be updated inside the parameters.
	* If there is more than one sketch, these parameters will not make difference.
	* @param crossSection to store the cross section of the sketch.
	* @param direction to store the direction of the sketch.
	* @return unsigned int
	*/
	unsigned int getNumberOfSketches(int& crossSection, int& direction);
	unsigned int getNumberOfSketches();


	/**
	* Method that returns the points of one sketch used to create the surface.
	* Points coordinate are (x, y).
	* @return Curve2D
	*/
	Curve2D getExtrudedSurfacePoints();


	/**
	* Method to clear the current sketch.
	* If there is any sketch in the current cross section, it will first clear the sketch.
	* Then it will erase the memory allocated to this sketch.
	* @return void
	*/
	void clearCurrentSketch();


	/**
	* Method that returns TRUE if there is at least one sketch available.
	* It returns false if there is no sketch or if the last state of the history is a clean sketch.
	* @return bool
	*/
	bool hasSketch();


	/**
	* Method that returns TRUE if the current sketch is near the mouse pointer.
	* @param mouse_x is the x coordinate of the mouse pointer.
	* @param mouse_y is the y coordinate of the mouse pointer.
	* @return double
	*/
	double distanceFromSketchToMousePointer(double mouse_x, double mouse_y);


	/**
	* Method to apply the smooth filter in the current sketch.
	* @return void
	*/
	void applySmooth();


	/**
	* Method to apply the rotation over the current sketch.
	* @param angle of rotation.
	* @return void.
	*/
	void applyRotation(double angle);
	void applyRotation(double angle, double center_x, double center_y);


	/**
	* Method to apply the scale over the current sketch.
	* @param ex is the factor of the scale in x axis.
	* @param ey is the factor of the scale in y axis.
	* @return void.
	*/
	void applyScale(double ex, double ey);
	void applyScaleMouse(double mouse_x, double mouse_y);


	/**
	* Method to apply the translation over the current sketch.
	* @param dx is the factor of translation in x axis.
	* @param dy is the factor of translation in y axis.
	* @return void.
	*/
	void applyTranslation(double dx, double dy);


	/**
	* Method that returns TRUE if the current edition is a modified sketch in comparison to the previous one.
	* @return bool
	*/
	bool isSketchModified();


	/**
	* Method that apply the undo over the history of the sketch.
	* It returns true if the undo was successful.
	* @return bool
	*/
	bool undoSketch();


	/**
	* Method that apply the redo over the history of the sketch.
	* It returns true if the redo was successful.
	* @return bool
	*/
	bool redoSketch();


	/**
	* Method to print information
	*/
	void printInfo();


private:

	unsigned int m_id;			/**< A numerical identification to the cross section. */
	unsigned int m_direction;	/**< A numerical identification to the direction of the cross section. */

	std::vector<std::shared_ptr<Sketch>> m_sketches;	/**< A collection of sketches used to create a surface. */

	bool is_overSketch;			/**< Flag that indicates if the sketch is in fact an oversketch.*/
	bool is_sketchClean;		/**< Flag that indicates if the current sketch was clean by the Clear button.*/

	int m_highlight;			/**< An index to represent the sketch that is highlighted (index of the collection). If there is no highlight, this index will be reset to -1*/
	int m_select;				/**< An index to keep track of which sketch has been selected from the GUI. if no sketch is selected, this index will be reset to -1*/

};

#endif //SURFACE_H

