#include "sketch_handler.h"

#include <QDebug>
#include <vector>

SketchHandler::SketchHandler(QObject* parent) : QObject(parent)
{
    this->reset();
}


void SketchHandler::reset()
{
    this->m_sketchedPoint_x = 0.0;
    this->m_sketchedPoint_y = 0.0;

    this->m_sketchLine_Length = 1.0;
    this->m_sketchCircle_RadiusX = 1.0;
    this->m_sketchCircle_RadiusY = 1.0;

    this->m_surfaceID = 1.0;

    this->is_overSketch = false;
    this->is_sketchClean = false;

    this->startMovePoint_x = 0.0;
    this->startMovePoint_y = 0.0;
    this->firstPoint_x = 0.0;
    this->firstPoint_y = 0.0;
    this->isMoving = false;

    this->m_surfaces.clear();

    this->m_trajectory.clear();
    this->is_Trajectory_overSketch = false;
    this->is_trajectoryClean = false;
    this->is_TrajectorySelected = false;
}


int SketchHandler::getPreviousSketchCrossSection(int crossSection)
{
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;
        if (this->m_surfaces[index]->isEditable())
        {
            return this->m_surfaces[index]->getPreviousSketchCrossSection(crossSection);
        }
    }

    return -1;
}

int SketchHandler::getNextSketchCrossSection(int crossSection)
{
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;
        if (this->m_surfaces[index]->isEditable())
        {
            return this->m_surfaces[index]->getNextSketchCrossSection(crossSection);
        }
    }

    return -1;
}


std::vector<int> SketchHandler::getSurfacesID()
{
    std::vector<int> response;

    for (int i=0; i < this->m_surfaces.size(); i++)
    {
        if(this->m_surfaces[i]->isVisible())
            response.push_back(this->m_surfaces[i]->getId());
    }

    return response;
}


void SketchHandler::setSurfaceID(int id)
{
    this->m_surfaceID = id;
}


bool SketchHandler::setSurfaceVisibility(int surfaceID, bool flag)
{
    for (int i=0; i<this->m_surfaces.size(); i++)
    {
        if (this->m_surfaces[i]->getId() == surfaceID && this->m_surfaces[i]->isVisible() != flag)
        {
                this->m_surfaces[i]->setVisibility(flag);
                return true;
        }
    }

    return false;
}


void SketchHandler::setSketchedPoint(double x, double y)
{
    this->m_sketchedPoint_x = x;
    this->m_sketchedPoint_y = y;

    if (Comments::SBIM_SKETCH_HANDLER())
    {        
        //qDebug() << "\n SketchHandler::setSketchedPoint(): \t ( " << m_sketchedPoint_x << ", " << m_sketchedPoint_y << ")";
    }
}


void SketchHandler::setSketchLineLength(double length)
{
    this->m_sketchLine_Length = length;
}


void SketchHandler::setSketchCircleRadiusX(double radius)
{
    this->m_sketchCircle_RadiusX = radius;
}


void SketchHandler::setSketchCircleRadiusY(double radius)
{
    this->m_sketchCircle_RadiusY = radius;
}


void SketchHandler::delegateSketchAction(int canvasAction, int crossSection, int direction)
{
    this->delegateSketchAction(Mouse::Button::NO_BUTTON, Mouse::Action::NO_ACTION,
        canvasAction, crossSection, direction);
}

void SketchHandler::delegateSketchAction(int buttonID, int buttonAction, 
    int canvasAction, int crossSection, int direction)
{
    std::vector<double> response;    
    bool success = false;
    
    switch (canvasAction)
    {
    case CanvasAction::Value::FB_SKETCH:
        if (buttonID == Mouse::Button::NO_BUTTON) {
            emit(updateHighlightSketch(this->highlightSketch(crossSection, direction)));
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK) {
            emit(updateSelectSketch(this->selectSketch(crossSection, direction)));
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::PRESS) {
            emit(updateDirectionButtons());

            this->is_overSketch = this->isOverSketch(crossSection, direction);

            this->SketchPress(crossSection, direction);
            this->is_sketchClean = false;
            response = this->SketchMove(crossSection, direction);

            if (this->is_overSketch)
                emit updateOverSketch(response);
            else 
                emit updateSketch();
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::MOVE) {
            response = this->SketchMove(crossSection, direction);
            if (this->is_overSketch)
                emit updateOverSketch(response);
            else
                emit updateSketch();
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::RELEASE) {
            response = this->SketchRelease(crossSection, direction);
            if (response.size() > 2)
                emit updatePreviewFlag(true);
            else 
                emit updatePreviewFlag(false);

            if (!this->hasSketch())
            {
                this->is_sketchClean = false;
                emit(enableDirectionButtons());
            }
            
            emit updateUsedCrossSections(crossSection, direction);
            emit updateSketch();
        }
        break;

    case CanvasAction::Value::FB_SKETCH_LINE:
        if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK) {
            emit(updateDirectionButtons());
            this->is_overSketch = false; // this->isOverSketch(crossSection, direction);
            this->SketchLineClick(crossSection, direction);
            emit updatePreviewFlag(true);
            if (!this->hasSketch())
            {
                this->is_sketchClean = false;
                emit(enableDirectionButtons());
            }
            emit updateSketch();
        }
        break;

    case CanvasAction::Value::FB_SKETCH_CIRCLE:
        if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK) {
            emit(updateDirectionButtons());
            this->is_overSketch = false;
            this->SketchCircleClick(crossSection, direction);
            emit updatePreviewFlag(true);
            if (!this->hasSketch())
            {
                this->is_sketchClean = false;
                emit(enableDirectionButtons());
            }
            emit updateSketch();
        }
        break;

    case CanvasAction::Value::FB_SKETCH_ELLIPSE:
        if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK) {
            emit(updateDirectionButtons());
            this->is_overSketch = false;
            this->SketchEllipseClick(crossSection, direction);
            emit updatePreviewFlag(true);
            if (!this->hasSketch())
            {
                this->is_sketchClean = false;
                emit(enableDirectionButtons());
            }
            emit updateSketch();
        }
        break;

    case CanvasAction::Value::FB_TRANSLATE:        
        if (buttonID == Mouse::Button::NO_BUTTON) {
            emit(updateHighlightSketch(highlightSketch(crossSection, direction)));
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK) {
            emit(updateSelectSketch(this->selectSketch(crossSection, direction)));
        }
        else if (this->isSketchHighlighted(crossSection, direction))
        {
            this->selectHighlightedSketch(crossSection, direction);
            if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::PRESS) {
                response = this->TranslatePress(crossSection, direction);
            }
            else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::MOVE) {
                response = this->TranslateMove(crossSection, direction);
            }
            else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::RELEASE) {
                response = this->TranslateRelease(crossSection, direction);
            }
            emit updateSketch();
        }
        break;


    case CanvasAction::Value::FB_ROTATE:
        if (buttonID == Mouse::Button::NO_BUTTON) {
            emit(updateHighlightSketch(highlightSketch(crossSection, direction)));
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK) {
            emit(updateSelectSketch(this->selectSketch(crossSection, direction)));
        }
        else if (this->isSketchHighlighted(crossSection, direction))
        {
            this->selectHighlightedSketch(crossSection, direction);
            if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::PRESS) {
                response = this->RotatePress(crossSection, direction);
            }
            else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::MOVE) {
                response = this->RotateMove(crossSection, direction);
            }
            else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::RELEASE) {
                response = this->RotateRelease(crossSection, direction);
            }
            emit updateSketch();
        }
        break;


    case CanvasAction::Value::FB_SCALE:
        if (buttonID == Mouse::Button::NO_BUTTON) {
            emit(updateHighlightSketch(highlightSketch(crossSection, direction)));
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK) {
            emit(updateSelectSketch(this->selectSketch(crossSection, direction)));
        }
        else if (this->isSketchHighlighted(crossSection, direction))
        {
            this->selectHighlightedSketch(crossSection, direction);
            if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::PRESS) {
                response = this->ScalePress(crossSection, direction);
            }
            else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::MOVE) {
                response = this->ScaleMove(crossSection, direction);
            }
            else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::RELEASE) {
                response = this->ScaleRelease(crossSection, direction);
            }
            emit updateSketch();
        }
        break;


    case CanvasAction::Value::FV_SMOOTH_SKETCH:
    case CanvasAction::Value::MV_SMOOTH_SKETCH:
        response = this->smoothSketch(crossSection, direction);
        emit updateSketch();
        break;


    case CanvasAction::Value::FV_CLEAR_SKETCH:
    case CanvasAction::Value::MV_CLEAR_SKETCH:
        this->clearSketch(crossSection, direction);
        this->is_sketchClean = true;
        response.clear();
        if(!this->hasSketch())
            emit(enableDirectionButtons());        
        emit updateSketch();
        break;


    case CanvasAction::Value::FV_REDO_SKETCH:
    case CanvasAction::Value::MV_REDO_SKETCH:
        response = this->redo(crossSection, direction, success);
        if (!success)
            response.clear();
        emit updateSketch();
        break;


    case CanvasAction::Value::FV_UNDO_SKETCH:
    case CanvasAction::Value::MV_UNDO_SKETCH:
        response = this->undo(crossSection, direction, success);
        if (success)
            this->is_sketchClean = false;
        else
            response.clear();
        if (this->hasSketch())
            emit(updateDirectionButtons());
        emit updateSketch();
        break;

    default:
        break;
    }
}


void SketchHandler::delegateTrajectoryAction(int canvasAction, int crossSection, int direction)
{
    this->delegateTrajectoryAction(Mouse::Button::NO_BUTTON, Mouse::Action::NO_ACTION,
        canvasAction, crossSection, direction);
}


void SketchHandler::delegateTrajectoryAction(int buttonID, int buttonAction,
    int canvasAction, int crossSection, int direction)
{
    std::vector<double> response;
    bool success = false;
    bool highlight_TR;

    switch (canvasAction)
    {
    
    case CanvasAction::Value::FB_SKETCH:
        if (buttonID == Mouse::Button::NO_BUTTON) {
            emit(updateHighlightTrajectory(this->highlightTrajectory()));
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK) {
            emit(updateSelectTrajectory(this->selectTrajectory()));
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::PRESS) {
            emit(updateDirectionButtons());

            if (this->is_trajectoryClean)
                this->is_Trajectory_overSketch = false;
            else
                this->is_Trajectory_overSketch = this->hasTrajectory();

            this->TrajectoryPress(crossSection, direction);
            this->is_trajectoryClean = false;
            response = this->TrajectoryMove();

            if (this->is_Trajectory_overSketch)
                emit updateTrajectoryOverSketch(response);
            else
                emit updateTrajectory();            
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::MOVE) {
            response = this->TrajectoryMove();
            if (this->is_Trajectory_overSketch)
                emit updateTrajectoryOverSketch(response);
            else
                emit updateTrajectory();
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::RELEASE) {
            response = this->TrajectoryRelease();
            emit updateTrajectory();
        }
        break;
    
    
    case CanvasAction::Value::FB_TRANSLATE:
        highlight_TR = this->highlightTrajectory();
        if (buttonID == Mouse::Button::NO_BUTTON) {
            emit(updateHighlightTrajectory(highlight_TR));
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK) {
            emit(updateSelectTrajectory(this->selectTrajectory()));
        }
        else if (highlight_TR)
        {
            if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::PRESS) {
                response = this->TranslateTrajectoryPress();
            }
            else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::MOVE) {
                response = this->TranslateTrajectoryMove();
            }
            else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::RELEASE) {
                response = this->TranslateTrajectoryRelease();
            }
            emit updateTrajectory();
        }
        break;
    
    
    case CanvasAction::Value::FB_ROTATE:
        highlight_TR = this->highlightTrajectory();
        if (buttonID == Mouse::Button::NO_BUTTON) {
            emit(updateHighlightTrajectory(highlight_TR));
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK) {
            emit(updateSelectTrajectory(this->selectTrajectory()));
        }
        else if (highlight_TR)
        {
            if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::PRESS) {
                response = this->RotateTrajectoryPress();
            }
            else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::MOVE) {
                response = this->RotateTrajectoryMove();
            }
            else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::RELEASE) {
                response = this->RotateTrajectoryRelease();
            }
            emit updateTrajectory();
        }        
        break;
    
    
    case CanvasAction::Value::FB_SCALE:
        highlight_TR = this->highlightTrajectory();
        if (buttonID == Mouse::Button::NO_BUTTON) {
            emit(updateHighlightTrajectory(highlight_TR));
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK) {
            emit(updateSelectTrajectory(this->selectTrajectory()));
        }
        else if (highlight_TR)
        {
            if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::PRESS) {
                response = this->ScaleTrajectoryPress();
            }
            else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::MOVE) {
                response = this->ScaleTrajectoryMove();
            }
            else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::RELEASE) {
                response = this->ScaleTrajectoryRelease();
            }
            emit updateTrajectory();
        }        
        break;
    
    
    case CanvasAction::Value::MV_SMOOTH_SKETCH:
        response = this->smoothTrajectory();
        emit updateTrajectory();
        break;
                
    
    case CanvasAction::Value::MV_CLEAR_SKETCH:
        this->clearTrajectory();
        this->is_trajectoryClean = true;
        response.clear();
        if (!this->hasSketch())
            emit(enableDirectionButtons());
        emit updateTrajectory();
        break;
    
    
    case CanvasAction::Value::MV_REDO_SKETCH:
        response = this->redoTrajectory(success);
        if (!success)
            response.clear();
        emit updateTrajectory();
        break;
    
    
    case CanvasAction::Value::MV_UNDO_SKETCH:
        response = this->undoTrajectory(success);
        if (success)
            this->is_trajectoryClean = false;
        else
            response.clear();
        if (this->hasSketch())
            emit(updateDirectionButtons());
        emit updateTrajectory();
        break;
    
    default:
        break;
    }
}


void SketchHandler::updateSketchDataStructure()
{
    //- Update surfaces data structure
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;
        if (this->m_surfaces[index]->isEditable())
        {
            if (!this->m_surfaces[index]->hasSketch())
            {
                this->m_surfaces.pop_back();
            }
        }
    }

    //- Update trajectory data strcuture
    this->m_trajectory.clear();

    //- Update flags
    this->is_overSketch = false;
    this->is_sketchClean = false;
    this->is_trajectoryClean = false;
    this->is_Trajectory_overSketch = false;
}


bool SketchHandler::isOverSketch(int crossSection, int direction)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            return this->m_surfaces[index]->isOverSketch(crossSection, direction);
        }
    }
    return false;
}


bool SketchHandler::hasSketch()
{
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;
        if (this->m_surfaces[index]->isEditable())
            if (this->m_surfaces[index]->hasSketch())
                return true;
    }

    if (this->m_trajectory.getSketch().size())
    {
        return true;
    }

    return false;
}


bool SketchHandler::hasSketch(int crossSection, int direction)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            //- Verify if there is any sketch in the current cross section:
            if (this->m_surfaces[index]->hasCrossSection(crossSection, direction))
            {
                return true;
            }
        }
    }
    return false;
}


bool SketchHandler::hasTrajectory()
{
    return this->m_trajectory.hasHistory();
}


unsigned int SketchHandler::getNumberOfSketches(int crossSection, int direction)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            return this->m_surfaces[index]->getNumberOfCurrentSketches(crossSection, direction);
        }
    }

    return 0;
}


unsigned int SketchHandler::getNumberOfSketches(int i)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            return this->m_surfaces[index]->getNumberOfSketches(i);
        }
    }

    return 0;
}


unsigned int SketchHandler::getNumberOfSketchesSubmitted(int i)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;
        return this->m_surfaces[index]->getNumberOfSketches(i);
    }

    return 0;
}


unsigned int SketchHandler::getNumberOfSketchContours()
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            return this->m_surfaces[index]->getNumberOfSketches();
        }
    }

    return 0;
}


unsigned int SketchHandler::getNumberOfCrossSections()
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            return this->m_surfaces[index]->getNumberOfCrossSections();
        }
    }

    return 0;
}


unsigned int SketchHandler::getNumberOfCrossSectionsSubmitted()
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;
        return this->m_surfaces[index]->getNumberOfCrossSections();
    }

    return 0;
}


int SketchHandler::getCrossSectionID(unsigned int i)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            return this->m_surfaces[index]->getCrossSectionID(i);
        }
    }
    return -1;
}


int SketchHandler::getCrossSectionIDSubmitted(unsigned int i)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;
        return this->m_surfaces[index]->getCrossSectionID(i);
    }
    return -1;
}


std::vector<double> SketchHandler::returnSketchContour(unsigned int csIndex, unsigned int sketchIndex, int &surfaceID)
{
    std::vector<double> sketchedCurve;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;
        surfaceID = this->m_surfaces[index]->getId();

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            sketchedCurve = this->fromCurve2DToVector(this->m_surfaces[index]->getSketch(csIndex, sketchIndex));
        }
    }
    return sketchedCurve;
}


std::vector<double> SketchHandler::returnSketchContourSubmitted(unsigned int csIndex, unsigned int sketchIndex, int& surfaceID)
{
    std::vector<double> sketchedCurve;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;
        surfaceID = this->m_surfaces[index]->getId();
        sketchedCurve = this->fromCurve2DToVector(this->m_surfaces[index]->getSketch(csIndex, sketchIndex));
    }
    return sketchedCurve;
}


std::vector<double> SketchHandler::returnSketch(int crossSection, int direction)
{
    std::vector<double> sketchedCurve;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            sketchedCurve = this->fromCurve2DToVector(this->m_surfaces[index]->getCurrentSketch(crossSection, direction));
        }
    }

    return sketchedCurve;
}


std::vector<double> SketchHandler::returnSketch(int crossSection, int direction, unsigned int i)
{
    std::vector<double> sketchedCurve;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            sketchedCurve = this->fromCurve2DToVector(this->m_surfaces[index]->getCurrentSketch(crossSection, direction, i));
        }
    }

    return sketchedCurve;
}


std::vector<double> SketchHandler::returnHighlightedSketch(int crossSection, int direction)
{
    std::vector<double> sketchedCurve;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            sketchedCurve = this->fromCurve2DToVector(this->m_surfaces[index]->getHighlightedSketch(crossSection, direction));
        }
    }

    return sketchedCurve;
}


std::vector<double> SketchHandler::returnSelectedSketch(int crossSection, int direction)
{
    std::vector<double> sketchedCurve;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            sketchedCurve = this->fromCurve2DToVector(this->m_surfaces[index]->getSelectedSketch(crossSection, direction));
        }
    }

    return sketchedCurve;
}


std::vector<double> SketchHandler::returnOverSketch(int crossSection, int direction)
{
    std::vector<double> sketchedCurve;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            sketchedCurve = this->fromCurve2DToVector(this->m_surfaces[index]->getCurrentOverSketch(crossSection, direction));
        }
    }

    return sketchedCurve;
}


std::vector<double> SketchHandler::returnTrajectory()
{
    std::vector<double> sketchedCurve;
    sketchedCurve = this->fromCurve2DToVector(this->m_trajectory.getSketch());
    return sketchedCurve;
}


bool SketchHandler::highlightSketch(int crossSection, int direction)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            return this->m_surfaces[index]->isSketchHighlighted(crossSection, direction,
                this->m_sketchedPoint_x, this->m_sketchedPoint_y);
        }
    }

    return false;
}


bool SketchHandler::highlightTrajectory()
{
    //- Verify if there is a current trajectory:
    if (this->m_trajectory.hasHistory())
    {
        //- Verify if the current trajectory isn't cleared:
        if (this->m_trajectory.getSketch().size())
        {
            double dist;
            dist = this->m_trajectory.distanceTo(this->m_sketchedPoint_x, this->m_sketchedPoint_y);
            if (dist < 20)
            {
                return true;
            }
        }
    }

    return false;
}


int SketchHandler::getHighlightIndex(int crossSection, int direction)
{
    unsigned int response = -1;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            response = this->m_surfaces[index]->getHighlightIndex(crossSection, direction);
        }
    }

    return response;
}


int SketchHandler::getSelectIndex(int crossSection, int direction)
{
    unsigned int response = -1;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            response = this->m_surfaces[index]->getSelectIndex(crossSection, direction);
        }
    }

    return response;
}


bool SketchHandler::isSketchHighlighted(int crossSection, int direction)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            return this->m_surfaces[index]->isSketchHighlighted(crossSection, direction);
        }
    }
    return false;
}


bool SketchHandler::selectSketch(int crossSection, int direction)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            return this->m_surfaces[index]->isSketchSelected(crossSection, direction,
                this->m_sketchedPoint_x, this->m_sketchedPoint_y);
        }
    }
    return false;
}


bool SketchHandler::selectHighlightedSketch(int crossSection, int direction)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            return this->m_surfaces[index]->isSketchSelected(crossSection, direction);
        }
    }
    return false;
}


bool SketchHandler::selectTrajectory()
{
    if (this->highlightTrajectory())
    {
        this->is_TrajectorySelected = true;        
    }
    else {
        this->is_TrajectorySelected = false;
    }

    return this->is_TrajectorySelected;
}


void SketchHandler::resetSelect()
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            this->m_surfaces[index]->resetSelected();
            emit(updateSelectSketch(false));
        }
    }
}

std::vector<double> SketchHandler::smoothSketch(int crossSection, int direction)
{
    std::vector<double> response;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            this->m_surfaces[index]->applySmooth(crossSection, direction);
            response = this->returnSketch(crossSection, direction);
        }
    }

    return response;
}


std::vector<double> SketchHandler::smoothTrajectory()
{
    std::vector<double> response;

    //- Verify if there is a current trajectory:
    if (this->m_trajectory.hasHistory())
    {
        //- Verify if the current trajectory isn't cleared:
        if (this->m_trajectory.getSketch().size())
        {
            this->m_trajectory.copyPreviousHistory();
            this->m_trajectory.smooth();
            this->m_trajectory.addHistory();
            response = this->fromCurve2DToVector(this->m_trajectory.getSketch());
        }
    }

    return response;
}


void SketchHandler::clearSketch(int crossSection, int direction)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            this->m_surfaces[index]->clearCurrentSketch(crossSection, direction);
        }
        else
        {
            if (Comments::SBIM_SKETCH_HANDLER())
            {
                //qDebug() << "\n SketchHandler::clearSketch() \t  No sketch in progress.";
            }
        }
    }
    else {
        if (Comments::SBIM_SKETCH_HANDLER())
        {
            //qDebug() << "\n SketchHandler::clearSketch() \t Need to sketch first.";
        }
    }
}


void SketchHandler::clearTrajectory()
{
    //- Verify if there is a current trajectory:
    if (this->m_trajectory.hasHistory())
    {
        //- Verify if the current trajectory isn't cleared:
        if (this->m_trajectory.getSketch().size())
        {
            this->m_trajectory.copyPreviousHistory();
            this->m_trajectory.clearSketch();
            this->m_trajectory.addHistory();
        }
    }
}


std::vector<double> SketchHandler::loadSketchTemplate(int crossSection, int direction, std::vector<double> newSketch)
{
    std::vector<double> response;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        Curve2D curve;
        curve = this->fromVectorToCurve2D(newSketch);
        //curve.translate(Point2D(20, 20));

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            //- Verify if there is any sketch in the current cross section:
            if (this->m_surfaces[index]->hasCrossSection(crossSection, direction))
            {
                this->m_surfaces[index]->addSketch(crossSection, direction, curve);
            }
            else {
                //- It means that the user is sketching in a new cross section:
                this->m_surfaces[index]->addNewCrossSection(crossSection, direction, curve);
            }

            response = this->returnSketch(crossSection, direction);
        }
        else {
            //- It means the user is adding a new surface:
            std::shared_ptr<Surface> newSurface = std::make_shared<Surface>();

            newSurface->setId(this->m_surfaces.size() + 1);
            newSurface->addNewCrossSection(crossSection, direction, curve);

            this->m_surfaces.push_back(newSurface);

            response = this->returnSketch(crossSection, direction);
        }
    }

    return response;
}


void SketchHandler::copyTrajectoryTemplate(std::vector<double> path, int crossSection, int direction)
{
    Curve2D newPath;
    newPath = this->fromVectorToCurve2D(path);

    //- Verify if there was a sketch before but it was clean:
    if (this->is_trajectoryClean)
    {
        this->m_trajectory.addSketch(newPath);
    }
    //- Verify if there is a trajectory and this is now an oversketch:
    else if (this->m_trajectory.hasHistory())
    {
        this->m_trajectory.copyPreviousHistory();
        this->m_trajectory.addSketch(newPath);
    }
    //- It means the user is sketching a new trajectory
    else
    {
        this->m_trajectory.addSketch(newPath);
    }
}


bool SketchHandler::submitSketch(bool& isExtruded, std::vector<double> &points, 
    double boxDimension, int boxDiscretization, int &surfaceID, int& csID, int direction)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {    
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            this->m_surfaces[index]->resetHighlight();
            this->m_surfaces[index]->resetSelected();
            surfaceID = this->m_surfaces[index]->getId(); 
            csID = -1;

            int crossSection, direction;
            size_t numberOfSketches;
            numberOfSketches = this->m_surfaces[index]->getNumberOfSketches(crossSection, direction);

            //- Verify if the surface uses only one sketch to be created:
            if (numberOfSketches == 1)
            {
                isExtruded = true;
                csID = crossSection;
                points = fromCurve2DToVector(this->m_surfaces[index]->getExtrudedSurfacePoints(crossSection, direction));
            }
            else if (numberOfSketches > 1)
            {
                isExtruded = false;
                if (direction == Direction::Value::LENGTH)
                {
                    points = this->m_surfaces[index]->getSurfacePoints_Length(boxDimension, boxDiscretization);
                }
                else if (direction == Direction::Value::WIDTH)
                {
                    points = this->m_surfaces[index]->getSurfacePoints_Width(boxDimension, boxDiscretization);
                }
                
            }

            //- To verify if there is any sketch being submitted
            //- To avoid this situation: If the user sketches and then clear it, then we have an empty sketch available 
            if (points.size())
            {
                //- Once the sketches are submitted, the surface can be considered as non editable. 
                this->m_surfaces[index]->setEditionMode(false);

                if (Comments::SBIM_SKETCH_HANDLER())
                {
                    //qDebug() << "\n SketchHandler::submitSketch(): \t number of surfaces: " << m_surfaces.size();
                }
                return true;
            }
        }
    }

    return false;
}


bool SketchHandler::submitPreview(bool& isExtruded, std::vector<double>& points,
    double boxDimension, int boxDiscretization, int &surfaceID, int& csID, int direction)
{
    //- Verify if there is at least one surface:
    if (m_surfaces.size())
    {
        auto index = m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (m_surfaces[index]->isEditable())
        {
            surfaceID = this->m_surfaces[index]->getId();
            csID = -1;

            int crossSection, direction;
            size_t numberOfSketches;
            numberOfSketches = m_surfaces[index]->getNumberOfSketches(crossSection, direction);

            //- Verify if the surface uses only one sketch to be created:
            if (numberOfSketches == 1)
            {
                csID = crossSection;
                isExtruded = true;
                points = fromCurve2DToVector(m_surfaces[index]->getExtrudedSurfacePoints(crossSection, direction));
            }
            else if (numberOfSketches > 1)
            {
                isExtruded = false;
                if (direction == Direction::Value::LENGTH)
                {
                    points = m_surfaces[index]->getSurfacePoints_Length(boxDimension, boxDiscretization);
                } 
                else if (direction == Direction::Value::WIDTH)
                {
                    points = m_surfaces[index]->getSurfacePoints_Width(boxDimension, boxDiscretization);
                }
                
            }

            //- To verify if there is any sketch being submitted
            //- To avoid this situation: If the user sketches and then clear it, then we have an empty sketch available 
            if (points.size())
            {
                //- Once the sketches are submitted, the surface can be considered as non editable. 
                //m_surfaces[index]->setEditionMode(false);
                if (Comments::SBIM_SKETCH_HANDLER())
                {
                    //qDebug() << "\n SketchHandler::submitPreview(): \t number of surfaces: " << m_surfaces.size();
                }
                return true;
            }
        }
    }

    return false;
}


bool SketchHandler::submitSketch(bool& isExtruded, std::vector<double>& points, std::vector<double>& path,
    double boxDimension, int boxDiscretization, int& surfaceID, int& csID, int direction)
{
    bool response;
    response = this->submitSketch(isExtruded, points, boxDimension, boxDiscretization, surfaceID, csID, direction);

    if (this->m_trajectory.hasHistory())
    {
        path = this->fromCurve2DToVector(this->m_trajectory.getSketch());
        this->m_trajectory.clear();
    }

    return response;
}


bool SketchHandler::submitPreview(bool& isExtruded, std::vector<double>& points, std::vector<double>& path,
    double boxDimension, int boxDiscretization, int& surfaceID, int& csID, int direction)
{
    bool response;
    response = submitPreview(isExtruded, points, boxDimension, boxDiscretization, surfaceID, csID, direction);

    if (m_trajectory.hasHistory())
    {
        path = fromCurve2DToVector(m_trajectory.getSketch());
        //m_trajectory.clear();
    }

    return response;
}


bool SketchHandler::submitSketchContour(std::vector<double>& points,
    double boxDimension, int boxDiscretization, int &surfaceID)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            surfaceID = this->m_surfaces[index]->getId();

            points = this->m_surfaces[index]->getSurfacePoints_Height(boxDimension, boxDiscretization);

            //- To verify if there is any sketch being submitted
            //- To avoid this situation: If the user sketches and then clear it, then we have an empty sketch available 
            if (points.size())
            {
                //- Once the sketches are submitted, the surface can be considered as non editable. 
                this->m_surfaces[index]->setEditionMode(false);
                return true;
            }
        }
    }

    return false;
}


bool SketchHandler::submitSketchContourPreview(std::vector<double>& points,
    double boxDimension, int boxDiscretization, int &surfaceID)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;
        ////qDebug() << "\nPreview contour: index " << index;
        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            surfaceID = this->m_surfaces[index]->getId();
            ////qDebug() << "\nPreview contour: surfaceID " << surfaceID;
            points = this->m_surfaces[index]->getSurfacePoints_Height(boxDimension, boxDiscretization);

            //- To verify if there is any sketch being submitted
            //- To avoid this situation: If the user sketches and then clear it, then we have an empty sketch available 
            if (points.size())
            {
                return true;
            }
        }
    }

    return false;
}


void SketchHandler::SketchPress(int crossSection, int direction)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            //- It means the user is adding a sketch to the current surface:
            this->editSketchSurface(crossSection, direction, this->is_sketchClean);
        }
        else {
            //- It means the user is adding a new surface:
            this->createNewSurface(crossSection, direction);
        }
    }
    else {
        //- It means the user is adding a new surface:
        this->createNewSurface(crossSection, direction);
    }
}


std::vector<double> SketchHandler::SketchMove(int crossSection, int direction)
{
    std::vector<double> response;
    auto index = this->m_surfaces.size() - 1;

    //- Verify if the current surface is editable (sketching in progress):
    if (this->m_surfaces[index]->isEditable())
    {
        //- Add sketch point and Update view:
        this->m_surfaces[index]->addPoint(this->m_sketchedPoint_x, this->m_sketchedPoint_y,
            crossSection, direction);

        if (this->is_overSketch)
        {
            response = this->returnOverSketch(crossSection, direction);
        }
        //- Add over sketch point and Update view:
        else {
            response = this->returnSketch(crossSection, direction);
        }
    }

    return response;
}


std::vector<double> SketchHandler::SketchRelease(int crossSection, int direction)
{
    std::vector<double> response;
    auto index = this->m_surfaces.size() - 1;

    //- Verify if the current surface is editable (sketching in progress):
    if (this->m_surfaces[index]->isEditable())
    {
        if (this->is_overSketch)
        {
            //- It means that the sketch made was in fact an oversketching and need to be analyzed:
            this->m_surfaces[index]->applyOverSketch(crossSection, direction);
        }
        else {
            //- It means it is the first sketch made:
            this->is_sketchClean = !this->m_surfaces[index]->updateSKetch(crossSection, direction);
        }

        response = this->returnSketch(crossSection, direction);
    }
    
    return response;
}

void SketchHandler::SketchLineClick(int crossSection, int direction)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is not editable:
        if (!this->m_surfaces[index]->isEditable())
        {
            //- It means the user is adding a new surface:
            this->createNewSurface(crossSection, direction);
        }
    }
    else {
        //- It means the user is adding a new surface:
        this->createNewSurface(crossSection, direction);
    }
    
    //- Add a sketch line to the current surface:
    this->addSketchLine(crossSection, direction);
}


void SketchHandler::addSketchLine(int crossSection, int direction)
{
    auto index = this->m_surfaces.size() - 1;

    //- Verify if the current surface is editable (sketching in progress):
    if (this->m_surfaces[index]->isEditable())
    {
        if (this->m_surfaces[index]->hasCrossSection(crossSection, direction))
        {
            this->m_surfaces[index]->copyPreviousSketch(crossSection, direction);
            this->m_surfaces[index]->addSketch(crossSection, direction, Curve2D());
        }
        else {
            //- It means that the user is sketching in a new cross section:
            this->m_surfaces[index]->addNewCrossSection(crossSection, direction);
        }
    }
    else {
        //- It means that the user is sketching in a new cross section:
        this->m_surfaces[index]->addNewCrossSection(crossSection, direction);
    }
    this->m_surfaces[index]->addSketchLine(crossSection, direction, this->m_sketchedPoint_y, this->m_sketchLine_Length);
}


void SketchHandler::SketchCircleClick(int crossSection, int direction)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is not editable:
        if (!this->m_surfaces[index]->isEditable())
        {
            //- It means the user is adding a new surface:
            this->createNewSurface(crossSection, direction);
        }
    }
    else {
        //- It means the user is adding a new surface:
        this->createNewSurface(crossSection, direction);
    }

    //- Add a sketch line to the current surface:
    this->addSketchCircle(crossSection, direction);
}


void SketchHandler::addSketchCircle(int crossSection, int direction)
{
    auto index = this->m_surfaces.size() - 1;

    //- Verify if the current surface is editable (sketching in progress):
    if (this->m_surfaces[index]->isEditable())
    {
        if (this->m_surfaces[index]->hasCrossSection(crossSection, direction))
        {
            this->m_surfaces[index]->copyPreviousSketch(crossSection, direction);
            this->m_surfaces[index]->addSketch(crossSection, direction, Curve2D());
        }
        else {
            //- It means that the user is sketching in a new cross section:
            this->m_surfaces[index]->addNewCrossSection(crossSection, direction);
        }
    }
    else {
        //- It means that the user is sketching in a new cross section:
        this->m_surfaces[index]->addNewCrossSection(crossSection, direction);
    }
    this->m_surfaces[index]->addSketchCircle(crossSection, direction, this->m_sketchedPoint_x, this->m_sketchedPoint_y, this->m_sketchCircle_RadiusX, this->m_sketchCircle_RadiusX);
}


void SketchHandler::SketchEllipseClick(int crossSection, int direction)
{
    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is not editable:
        if (!this->m_surfaces[index]->isEditable())
        {
            //- It means the user is adding a new surface:
            this->createNewSurface(crossSection, direction);
        }
    }
    else {
        //- It means the user is adding a new surface:
        this->createNewSurface(crossSection, direction);
    }

    //- Add a sketch line to the current surface:
    this->addSketchEllipse(crossSection, direction);
}


void SketchHandler::addSketchEllipse(int crossSection, int direction)
{
    auto index = this->m_surfaces.size() - 1;

    //- Verify if the current surface is editable (sketching in progress):
    if (this->m_surfaces[index]->isEditable())
    {
        if (this->m_surfaces[index]->hasCrossSection(crossSection, direction))
        {
            this->m_surfaces[index]->copyPreviousSketch(crossSection, direction);
            this->m_surfaces[index]->addSketch(crossSection, direction, Curve2D());
        }
        else {
            //- It means that the user is sketching in a new cross section:
            this->m_surfaces[index]->addNewCrossSection(crossSection, direction);
        }
    }
    else {
        //- It means that the user is sketching in a new cross section:
        this->m_surfaces[index]->addNewCrossSection(crossSection, direction);
    }
    this->m_surfaces[index]->addSketchCircle(crossSection, direction, this->m_sketchedPoint_x, this->m_sketchedPoint_y, this->m_sketchCircle_RadiusX, this->m_sketchCircle_RadiusY);
}


std::vector<double> SketchHandler::TranslatePress(int crossSection, int direction)
{
    std::vector<double> response;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            if (this->m_surfaces[index]->copyPreviousSketch(crossSection, direction))
            {
                response = this->returnSketch(crossSection, direction);
                this->startMovePoint_x = this->m_sketchedPoint_x;
                this->startMovePoint_y = this->m_sketchedPoint_y;
                this->isMoving = true;
            }
        }
    }

    return response;
}


std::vector<double> SketchHandler::TranslateMove(int crossSection, int direction)
{
    std::vector<double> response;

    if (this->isMoving)
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            double dx = this->m_sketchedPoint_x - this->startMovePoint_x;
            double dy = this->m_sketchedPoint_y - this->startMovePoint_y;

            this->m_surfaces[index]->applyTranslation(dx, dy, crossSection, direction);
            response = this->returnSketch(crossSection, direction);

            this->startMovePoint_x = this->m_sketchedPoint_x;
            this->startMovePoint_y = this->m_sketchedPoint_y;
        }
    }

    return response;
}


std::vector<double> SketchHandler::TranslateRelease(int crossSection, int direction)
{
    std::vector<double> response;

    this->isMoving = false;
    auto index = this->m_surfaces.size() - 1;

    //- Verify if the current surface is editable (sketching in progress):
    if (this->m_surfaces[index]->isEditable())
    {
        if (this->m_surfaces[index]->isSketchModified(crossSection, direction))
        {
            response = this->returnSketch(crossSection, direction);
        }
    }

    return response;
}


std::vector<double> SketchHandler::RotatePress(int crossSection, int direction)
{
    std::vector<double> response;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            if (this->m_surfaces[index]->copyPreviousSketch(crossSection, direction))
            {
                response = this->returnSketch(crossSection, direction);
                this->startMovePoint_x = this->m_sketchedPoint_x;
                this->startMovePoint_y = this->m_sketchedPoint_y;
                this->firstPoint_x = this->m_sketchedPoint_x;
                this->firstPoint_y = this->m_sketchedPoint_y;
            }
        }
    }

    return response;
}


std::vector<double> SketchHandler::RotateMove(int crossSection, int direction)
{
    std::vector<double> response;

    auto index = this->m_surfaces.size() - 1;

    //- Verify if the current surface is editable (sketching in progress):
    if (this->m_surfaces[index]->isEditable())
    {
        if (this->m_sketchedPoint_y > this->startMovePoint_y || this->m_sketchedPoint_x < this->startMovePoint_x)
            this->m_surfaces[index]->applyRotation(0.025, crossSection, direction, 
                this->firstPoint_x, this->firstPoint_y);
        else
            this->m_surfaces[index]->applyRotation(-0.025, crossSection, direction,
                this->firstPoint_x, this->firstPoint_y);

        response = this->returnSketch(crossSection, direction);

        this->startMovePoint_x = this->m_sketchedPoint_x;
        this->startMovePoint_y = this->m_sketchedPoint_y;
    }

    return response;
}


std::vector<double> SketchHandler::RotateRelease(int crossSection, int direction)
{
    std::vector<double> response;

    auto index = this->m_surfaces.size() - 1;

    //- Verify if the current surface is editable (sketching in progress):
    if (this->m_surfaces[index]->isEditable())
    {
        this->m_surfaces[index]->validateSketch(crossSection, direction);
        response = this->returnSketch(crossSection, direction);
    }
    
    return response;
}


std::vector<double> SketchHandler::ScalePress(int crossSection, int direction)
{
    std::vector<double> response;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            if (this->m_surfaces[index]->copyPreviousSketch(crossSection, direction))
            {
                response = this->returnSketch(crossSection, direction);
                this->startMovePoint_x = this->m_sketchedPoint_x;
                this->startMovePoint_y = this->m_sketchedPoint_y;
            }
        }
    }

    return response;
}


std::vector<double> SketchHandler::ScaleMove(int crossSection, int direction)
{
    std::vector<double> response;
    auto index = this->m_surfaces.size() - 1;

    //- Verify if the current surface is editable (sketching in progress):
    if (this->m_surfaces[index]->isEditable())
    {
        if (this->m_sketchedPoint_x < this->startMovePoint_x) //|| this->m_sketchedPoint_y < this->startMovePoint_y)
            this->m_surfaces[index]->applyScale(0.975, 0.975, crossSection, direction);
        else
            this->m_surfaces[index]->applyScale(1.025, 1.025, crossSection, direction);

        response = this->returnSketch(crossSection, direction);
        this->startMovePoint_x = this->m_sketchedPoint_x;
        this->startMovePoint_y = this->m_sketchedPoint_y;
    }

    return response;
}


std::vector<double> SketchHandler::ScaleRelease(int crossSection, int direction)
{
    std::vector<double> response;
    auto index = this->m_surfaces.size() - 1;

    //- Verify if the current surface is editable (sketching in progress):
    if (this->m_surfaces[index]->isEditable())
    {
        this->m_surfaces[index]->validateSketch(crossSection, direction);
        response = this->returnSketch(crossSection, direction);
    }

    return response;
}


std::vector<double> SketchHandler::undo(int crossSection, int direction, bool& undo)
{
    std::vector<double> response;
    undo = false;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            undo = this->m_surfaces[index]->undoSketch(crossSection, direction);
            if (undo)
            {
                response = this->returnSketch(crossSection, direction);
            }
        }
    }

    return response;
}


std::vector<double> SketchHandler::redo(int crossSection, int direction, bool& redo)
{
    std::vector<double> response;
    redo = false;

    //- Verify if there is at least one surface:
    if (this->m_surfaces.size())
    {
        auto index = this->m_surfaces.size() - 1;

        //- Verify if the current surface is editable (sketching in progress):
        if (this->m_surfaces[index]->isEditable())
        {
            redo = this->m_surfaces[index]->redoSketch(crossSection, direction);
            if (redo)
            {
                response = this->returnSketch(crossSection, direction);
            }
        }
    }

    return response;
}


void SketchHandler::TrajectoryPress(int crossSection, int direction)
{
    //- Verify if there was a sketch before but it was clean:
    if (this->is_trajectoryClean)
    {
        this->m_trajectory.addSketch(Curve2D());
    }
    //- Verify if there is a trajectory and this is now an oversketch:
    else if (this->m_trajectory.hasHistory())
    {
        this->m_trajectory.copyPreviousHistory();
    }
    //- It means the user is sketching a new trajectory
    else
    {
        this->m_trajectory.addSketch();
    }    
}


std::vector<double> SketchHandler::TrajectoryMove()
{
    std::vector<double> response;

    //- Add over sketch point and Update view:
    if (this->is_Trajectory_overSketch)
    {
        this->m_trajectory.addCoordinate_OverSketch(this->m_sketchedPoint_x, this->m_sketchedPoint_y);
        response = this->fromCurve2DToVector(this->m_trajectory.getOverSketch());
    }
    //- Add sketch point and Update view:
    else {
        this->m_trajectory.addCoordinate(this->m_sketchedPoint_x, this->m_sketchedPoint_y);
        this->m_trajectory.addHistory();

        response = this->fromCurve2DToVector(this->m_trajectory.getSketch());
    }

    return response;
}


std::vector<double> SketchHandler::TrajectoryRelease()
{
    std::vector<double> response;

    if (this->is_Trajectory_overSketch)
    {
        //- It means that the sketch made was in fact an oversketching and need to be analyzed:
        if (this->m_trajectory.overSketch())
        {
            /* #if not defined(MODEL_WITH_PATH_GUIDED_SURFACES) */
            /* //- If sketch is not valid, turn it into monotonic! */
            /* if (!this->m_trajectory.isValid_Y()) */
            /* { */
            /*     this->m_trajectory.turnIntoMonotonic_Y(); */
            /* } */
            /* #endif */
            this->m_trajectory.addHistory();
        }
        else
        {
            this->m_trajectory.clearOverSketch();
        }
    }
    else {
        //- It means it is the first sketch made:
        //- Verify if the sketch has only one point: it means it was a click by the user and not a sketch
        if (this->m_trajectory.getSketchSize() < 2)
        {
            this->m_trajectory.clearSketch();
            this->is_trajectoryClean = true;
        }
        else {
            this->m_trajectory.smooth();

            //- If sketch is not valid, turn it into monotonic!
            if (!this->m_trajectory.isValid_Y())
            {
                /* this->m_trajectory.turnIntoMonotonic_Y(); */
            }
            this->m_trajectory.addHistory();
            this->m_trajectory.clearOverSketch();
        }
    }

    response = this->fromCurve2DToVector(this->m_trajectory.getSketch());
    return response;
}


std::vector<double> SketchHandler::TranslateTrajectoryPress()
{
    std::vector<double> response;

    //- Verify if there is a current trajectory:
    if (this->m_trajectory.hasHistory())
    {
        //- Verify if the current trajectory isn't cleared:
        if (this->m_trajectory.getSketch().size())
        {
            this->m_trajectory.copyPreviousHistory();
            response = this->fromCurve2DToVector(this->m_trajectory.getSketch());
            this->startMovePoint_x = this->m_sketchedPoint_x;
            this->startMovePoint_y = this->m_sketchedPoint_y;
            this->isMoving = true;
        }
    }

    return response;
}


std::vector<double> SketchHandler::TranslateTrajectoryMove()
{
    std::vector<double> response;

    if (this->isMoving)
    {
        double dx = this->m_sketchedPoint_x - this->startMovePoint_x;
        double dy = this->m_sketchedPoint_y - this->startMovePoint_y;

        this->m_trajectory.translate(Point2D(dx, dy));
        this->m_trajectory.addHistory();

        response = this->fromCurve2DToVector(this->m_trajectory.getSketch());

        this->startMovePoint_x = this->m_sketchedPoint_x;
        this->startMovePoint_y = this->m_sketchedPoint_y;
    }

    return response;
}


std::vector<double> SketchHandler::TranslateTrajectoryRelease()
{
    std::vector<double> response;

    this->isMoving = false;

    if (this->m_trajectory.isModified())
    {
        response = this->fromCurve2DToVector(this->m_trajectory.getSketch());
    }
    else {
        this->m_trajectory.removeHistory();
    }

    return response;
}


std::vector<double> SketchHandler::RotateTrajectoryPress()
{
    std::vector<double> response;

    //- Verify if there is a current trajectory:
    if (this->m_trajectory.hasHistory())
    {
        //- Verify if the current trajectory isn't cleared:
        if (this->m_trajectory.getSketch().size())
        {
            this->m_trajectory.copyPreviousHistory();
            response = this->fromCurve2DToVector(this->m_trajectory.getSketch());
            this->startMovePoint_x = this->m_sketchedPoint_x;
            this->startMovePoint_y = this->m_sketchedPoint_y;
        }
    }

    return response;
}


std::vector<double> SketchHandler::RotateTrajectoryMove()
{
    std::vector<double> response;

    if (this->m_sketchedPoint_y > this->startMovePoint_y)
    {
        this->m_trajectory.rotate(-0.025);
        this->m_trajectory.addHistory();
    }
    else {
        this->m_trajectory.rotate(0.025);
        this->m_trajectory.addHistory();
    }

    response = this->fromCurve2DToVector(this->m_trajectory.getSketch());
    this->startMovePoint_x = this->m_sketchedPoint_x;
    this->startMovePoint_y = this->m_sketchedPoint_y;

    return response;
}


std::vector<double> SketchHandler::RotateTrajectoryRelease()
{
    std::vector<double> response;

    if (!this->m_trajectory.isValid_Y())
    {
        /* this->m_trajectory.turnIntoMonotonic_Y(); */
    }
    this->m_trajectory.addHistory();
    response = this->fromCurve2DToVector(this->m_trajectory.getSketch());

    return response;
}


std::vector<double> SketchHandler::ScaleTrajectoryPress()
{
    std::vector<double> response;

    //- Verify if there is a current trajectory:
    if (this->m_trajectory.hasHistory())
    {
        //- Verify if the current trajectory isn't cleared:
        if (this->m_trajectory.getSketch().size())
        {
            this->m_trajectory.copyPreviousHistory();
            response = this->fromCurve2DToVector(this->m_trajectory.getSketch());
            this->startMovePoint_x = this->m_sketchedPoint_x;
            this->startMovePoint_y = this->m_sketchedPoint_y;
        }
    }

    return response;
}


std::vector<double> SketchHandler::ScaleTrajectoryMove()
{
    std::vector<double> response;
    if (this->m_sketchedPoint_x < this->startMovePoint_x) //|| this->m_sketchedPoint_y < this->startMovePoint_y)
    {
        this->m_trajectory.scale(Point2D(0.975, 0.975));
        this->m_trajectory.addHistory();
    } else {
        this->m_trajectory.scale(Point2D(1.025, 1.025));
        this->m_trajectory.addHistory();
    }

    response = this->fromCurve2DToVector(this->m_trajectory.getSketch());
    this->startMovePoint_x = this->m_sketchedPoint_x;
    this->startMovePoint_y = this->m_sketchedPoint_y;
 
    return response;
}


std::vector<double> SketchHandler::ScaleTrajectoryRelease()
{
    std::vector<double> response;

    if (!this->m_trajectory.isValid_Y())
    {
        /* this->m_trajectory.turnIntoMonotonic_Y(); */
    }
    this->m_trajectory.addHistory();
    response = this->fromCurve2DToVector(this->m_trajectory.getSketch());

    return response;
}


std::vector<double> SketchHandler::undoTrajectory(bool& undo)
{
    std::vector<double> response;
    undo = false;

    //- Verify if there is a current trajectory:
    if (this->m_trajectory.hasHistory())
    {
        undo = this->m_trajectory.undo();
        if (undo)
        {
            response = this->fromCurve2DToVector(this->m_trajectory.getSketch());
        }
    }

    return response;
}


std::vector<double> SketchHandler::redoTrajectory(bool& redo)
{
    std::vector<double> response;
    redo = false;

    //- Verify if there is a current trajectory:
    if (this->m_trajectory.hasHistory())
    {
        redo = this->m_trajectory.redo();
        if (redo)
        {
            response = this->fromCurve2DToVector(this->m_trajectory.getSketch());
        }
    }
    
    return response;
}


std::vector<double> SketchHandler::editSketchSurface(int crossSection, int direction, bool is_sketchClean)
{
    std::vector<double> response;
    auto index = this->m_surfaces.size() - 1;

    //- Verify if the current surface is editable (sketching in progress):
    if (this->m_surfaces[index]->isEditable())
    {
        //- Verify if there was a sketch before but it was clean:
        if (is_sketchClean)
        {
            this->m_surfaces[index]->addSketch(crossSection, direction, Curve2D());
        }
        //- Verify if there is any sketch in the current cross section:
        else if (this->m_surfaces[index]->hasCrossSection(crossSection, direction))
        {
            if (direction == Direction::Value::LENGTH || direction == Direction::Value::WIDTH)
            {
                //- It means that the user is oversketching:
                if (this->m_surfaces[index]->copyPreviousSketch(crossSection, direction))
                {
                    response = this->returnSketch(crossSection, direction);
                }
            }
            else {
                if (this->is_overSketch)
                { 
                    //- It means that the user is oversketching:
                    if (this->m_surfaces[index]->copyPreviousSketch(crossSection, direction))
                    {
                        response = this->returnHighlightedSketch(crossSection, direction);
                    }
                }
                else {
                    //- It means that the user is adding a new sketch in MapView
                    this->m_surfaces[index]->addSketch(crossSection, direction);
                }
            }
        }
        else {
            //- It means that the user is sketching in a new cross section:
            this->m_surfaces[index]->addNewCrossSection(crossSection, direction);
        }
    }

    return response;
}


void SketchHandler::createNewSurface(int crossSection, int direction)
{
    std::shared_ptr<Surface> newSurface = std::make_shared<Surface>();

    newSurface->setId(this->m_surfaceID);
    newSurface->addNewCrossSection(crossSection, direction);

    this->m_surfaces.push_back(newSurface);
}


std::vector<double> SketchHandler::fromCurve2DToVector(Curve2D curve)
{
    std::vector<double> newCurve;

    for (size_t i = 0; i < curve.size(); i++)
    {
        newCurve.push_back(curve[i].x());
        newCurve.push_back(curve[i].y());
    }

    return newCurve;
}


Curve2D SketchHandler::fromVectorToCurve2D(std::vector<double> curve)
{
    Curve2D newCurve;

    for (size_t i = 0; i < curve.size(); i += 2)
    {
        newCurve.push_back(Point2D(curve[i], curve[i + 1]));
    }

    return newCurve;
}
