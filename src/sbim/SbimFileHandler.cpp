#include "SbimFileHandler.h"

#include <QDebug>
#include <QString>
#include <QObject>
#include <QUrl>
#include <QFile>


SbimFileHandler::SbimFileHandler(QString fileName)
{
    this->m_file_name = fileName;
}

void SbimFileHandler::InitSampleData()
{
    //Sample data
    std::vector<double> curve1={1.1,2.1,3.1,4.1,5.1,6.1};
    std::vector<double> curve2={5.2,6.2,7.2,8.2,9.2,10.2};
    std::vector<double> curve3={11.3,12.3,13.3,14.3,15.3,16.3};
    std::vector<std::vector<double>> curves;

    curves.push_back(curve1);
    curves.push_back(curve2);
    curves.push_back(curve3);

    std::vector<int> sIds={1,2,3,4,5};

    std::vector<int> csIds={11,12,13,14,15};

    //Contour data
    this->m_contour_data.curves = curves;
    this->m_contour_data.surface_id =sIds;
    this->m_contour_data.cross_section_id =csIds;

    if (Comments::SBIM_FILE_HANDLER())
    {
        //qDebug() << " \n SbimFileHandler::initSampleData():";
        //qDebug() << " \t- Curve list:" << this->m_contour_data.curves;
        //qDebug() << " \t- Cross_section_ids:" << this->m_contour_data.cross_section_id;
        //qDebug() << " \t- Surface_ids:" << this->m_contour_data.surface_id;
    }

    //Trajectory data
    this->m_trajectory_data.curves = curves;
    this->m_trajectory_data.surface_id = sIds;
}


void SbimFileHandler::setContourData(SketchData contour)
{
    this->m_contour_data.curves = contour.curves;
    this->m_contour_data.cross_section_id = contour.cross_section_id;
    this->m_contour_data.surface_id = contour.surface_id;
}


SketchData SbimFileHandler::getContourData()
{
    return this->m_contour_data;
}


void SbimFileHandler::setTrajectoryData(SketchData trajectory)
{
    this->m_trajectory_data.curves = trajectory.curves;
    this->m_trajectory_data.cross_section_id = trajectory.cross_section_id;
    this->m_trajectory_data.surface_id = trajectory.surface_id;
}


SketchData SbimFileHandler::getTrajectoryData()
{
    return this->m_trajectory_data;
}


void SbimFileHandler::resetDataStructure()
{
    //clean contour data structure
    this->m_contour_data.curves = {};
    this->m_contour_data.surface_id ={};
    this->m_contour_data.cross_section_id ={};

    //clean trajectory data structure
    this->m_trajectory_data.curves = {};
    this->m_trajectory_data.surface_id = {};
    this->m_trajectory_data.cross_section_id = {};
}


void SbimFileHandler::save()
{
    if (this->m_file_name.isEmpty())
    {
        if (Comments::SBIM_FILE_HANDLER()) 
        {
            //qDebug() << "\n SbimFileHandler::save(): \t File name required!";
        }
        return;
    }

    QFile file(this->m_file_name);

    if (!file.open(QIODevice::WriteOnly))
    {
        if (Comments::SBIM_FILE_HANDLER())
        {
            //qDebug() << "\n SbimFileHandler::save(): \t Could not open file for writing";
        }
        return;
    }

    createJson();

    if(this->m_json_string !="")
    {
        QTextStream out(&file);
        out << this->m_json_string;
    }
    else {
        if (Comments::SBIM_FILE_HANDLER())
        {
            //qDebug() << "\n SbimFileHandler::save(): \t Json string emlty!";
        }
    }

    file.flush();
    file.close();
}


void SbimFileHandler::load()
{
    if (this->m_file_name.isEmpty())
    {
        if (Comments::SBIM_FILE_HANDLER())
        {
            //qDebug() << "\n SbimFileHandler::load(): \t File name required!";
        }
        return;
    }

    QFile file(this->m_file_name);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        if (Comments::SBIM_FILE_HANDLER())
        {
            //qDebug() << "\n SbimFileHandler::load(): \t Could not open the file for reading";
        }
        return;
    }

    QTextStream in(&file);
    QString myText = in.readAll();
    this->m_json_string = myText;

    parseJson();

    file.close();
}


QJsonArray SbimFileHandler::getCurvesListObject(std::vector<std::vector<double>> curves_list)
{
    QJsonArray json_curves_list;
    QJsonObject json_curve_object;

    for (size_t i = 0; i < curves_list.size(); i ++)
    {
        std::vector<double> each_curve = curves_list[i];
        
        if (Comments::SBIM_FILE_HANDLER())
        {
            //qDebug() << "\n SbimFileHandler::getCurvesListObject(): \t Curves list: " << each_curve;
        }

        QJsonObject each_curve_object;
        QJsonArray each_curve_json_array;
        for (size_t j = 0; j < each_curve.size(); j++)
        {
            each_curve_json_array.push_back(each_curve[j]);
            //each_curve_json_array.push_back(each_curve[j + 1]);
        }//end: for j
            
        json_curves_list.push_back(QJsonValue(each_curve_json_array));
    }//end for i

    return json_curves_list;
}


QJsonArray SbimFileHandler::getSurfaceListObject(std::vector<int> surface_list)
{
    QJsonArray json_surface_list;
    QJsonObject json_surface_object;

    for (size_t i = 0; i < surface_list.size(); i ++)
    {
        json_surface_list.push_back(surface_list[i]);

    }//end for i

    return json_surface_list;
}


QJsonArray SbimFileHandler::getCrossSectionListObject(std::vector<int> cross_section_list)
{
    QJsonArray json_cross_section_list;
    QJsonObject json_cross_section_object;

    for (size_t i = 0; i < cross_section_list.size(); i ++)
    {
        json_cross_section_list.push_back(cross_section_list[i]);

    }//end for i

    return json_cross_section_list;
}


QJsonObject SbimFileHandler::getContoursObject()
{
    std::vector<std::vector<double>> curves = this->m_contour_data.curves;
    std::vector<int> cross_section_id = this->m_contour_data.cross_section_id;
    std::vector<int> surface_id = this->m_contour_data.surface_id;

    QJsonObject contour_object;
    contour_object.insert(QString(DATA_CURVES_LABEL), QJsonValue(getCurvesListObject(curves)));
    contour_object.insert(QString(DATA_CS_ID_LABEL), QJsonValue(getCrossSectionListObject(cross_section_id)));
    contour_object.insert(QString(DATA_SURFACE_ID_LABEL), QJsonValue(getSurfaceListObject(surface_id)));

    return contour_object;
}

QJsonObject SbimFileHandler::getTrajectoriesObject()
{
    std::vector<std::vector<double>> curves = this->m_trajectory_data.curves;
    std::vector<int> cross_section_id = this->m_trajectory_data.cross_section_id;
    std::vector<int> surface_id = this->m_trajectory_data.surface_id;
    
    QJsonObject trajectory_object;
    trajectory_object.insert(QString(DATA_CURVES_LABEL), QJsonValue(getCurvesListObject(curves)));
    trajectory_object.insert(QString(DATA_CS_ID_LABEL), QJsonValue(getCrossSectionListObject(cross_section_id)));
    trajectory_object.insert(QString(DATA_SURFACE_ID_LABEL), QJsonValue(getSurfaceListObject(surface_id)));
    
    return trajectory_object;
}


void SbimFileHandler::createJson()
{
    QJsonObject combined_object;

    combined_object.insert(QString(CONTOUR_LABEL), QJsonValue(getContoursObject()));
    combined_object.insert(QString(TRAJECTORY_LABEL), QJsonValue(getTrajectoriesObject()));

    QJsonObject final_object;
    final_object.insert(QString(MAIN_LABEL),QJsonValue(combined_object));

    QJsonDocument jsonDoc(final_object);
    this->m_json_string = jsonDoc.toJson();

    if (Comments::SBIM_FILE_HANDLER())
    {
        //qDebug() << "\n SbimFileHandler::createJson(): \t" << m_json_string.toStdString().c_str();
    }
}


std::vector<std::vector<double>> SbimFileHandler::parseCurveList(QJsonArray curvesArray)
{
    std::vector<std::vector<double>> curves;

    for(int i=0; i< curvesArray.size(); i++)
    {
        QJsonArray eachCurveArray = curvesArray[i].toArray();
        
        if (Comments::SBIM_FILE_HANDLER())
        {
            //qDebug() << "\n SbimFileHandler::parseCurveList(): \t Each Curve list:" << eachCurveArray;
        }

        std::vector <double> singleArray;
        for(int j=0; j< eachCurveArray.size(); j++)
        {
            singleArray.push_back(eachCurveArray[j].toDouble());

        }
        curves.push_back(singleArray);
    }

    return curves;
}


std::vector<int> SbimFileHandler::parseIdList(QJsonArray idArray)
{
    std::vector<int> id_list;
    for(int i=0; i< idArray.size(); i++)
    {
        id_list.push_back(idArray[i].toInt());
    }

    return id_list;
}


void SbimFileHandler::parseContourData(QJsonObject contoursInfo)
{
    QJsonArray contours_curve = contoursInfo[DATA_CURVES_LABEL].toArray();
    QJsonArray contours_cross_section_id = contoursInfo[DATA_CS_ID_LABEL].toArray();
    QJsonArray contours_surface_id = contoursInfo[DATA_SURFACE_ID_LABEL].toArray();

    this->m_contour_data.curves = parseCurveList(contours_curve);
    this->m_contour_data.cross_section_id = parseIdList(contours_cross_section_id);
    this->m_contour_data.surface_id = parseIdList(contours_surface_id);

    if (Comments::SBIM_FILE_HANDLER())
    {
        //qDebug() << "\n SbimFileHandler::parseContourData()";
        //qDebug() << "\t- Curve list:" << this->m_contour_data.curves;
        //qDebug() << "\t- Cross_section_ids:" << this->m_contour_data.cross_section_id;
        //qDebug() << "\t- Surface_ids:" << this->m_contour_data.surface_id;
    }
}


void SbimFileHandler::parseTrajectoryData(QJsonObject trajectoriesInfo)
{
    QJsonArray trajectory_curve = trajectoriesInfo[DATA_CURVES_LABEL].toArray();
    QJsonArray trajectory_cross_section_id = trajectoriesInfo[DATA_CS_ID_LABEL].toArray();
    QJsonArray trajectory_surface_id = trajectoriesInfo[DATA_SURFACE_ID_LABEL].toArray();
    
    this->m_trajectory_data.curves = parseCurveList(trajectory_curve);
    this->m_trajectory_data.cross_section_id = parseIdList(trajectory_cross_section_id);
    this->m_trajectory_data.surface_id = parseIdList(trajectory_surface_id);

    if (Comments::SBIM_FILE_HANDLER())
    {
        //qDebug() << "\n SbimFileHandler::parseTrajectoryData():";
        //qDebug() << "\t- Curves:" << this->m_trajectory_data.curves;
        //qDebug() << "\t- Cross_section_ids::" << this->m_trajectory_data.cross_section_id;
        //qDebug() << "\t- Surface_ids:" << this->m_trajectory_data.surface_id;
    }
}


void SbimFileHandler::parseJson()
{
    //Reset Data strucutre
    resetDataStructure();

    QJsonDocument jsonResponse = QJsonDocument::fromJson(this->m_json_string.toUtf8());

    QJsonObject jsonObject = jsonResponse.object();

    QJsonObject sbimInfo = jsonObject[MAIN_LABEL].toObject();

    QJsonObject contoursInfo = sbimInfo[CONTOUR_LABEL].toObject();
    parseContourData(contoursInfo);

    QJsonObject trajectoriesInfo = sbimInfo[TRAJECTORY_LABEL].toObject();
    parseTrajectoryData(trajectoriesInfo);
}
