
/**
* @file sketch.cpp
* @author Sicilia Judice
* @brief File containing the class Sketch
*/

#include <QDebug>

#include <math.h>

#include "sketch.h"

Sketch::Sketch()
{
    this->clear();
}


Sketch::Sketch(const Sketch& newSketch) : Sketch()
{
    this->m_curve = newSketch.m_curve;
    this->m_overSketchCurve = newSketch.m_overSketchCurve;
    this->m_history = newSketch.m_history;
}


Sketch& Sketch::operator=(const Sketch& newSketch)
{
    this->m_curve = newSketch.m_curve;
    this->m_overSketchCurve = newSketch.m_overSketchCurve;
    this->m_history = newSketch.m_history;

	return *this;
}


Sketch::~Sketch()
{
    this->clear();
}


void Sketch::clear()
{
    this->m_curve.clear();
    this->m_overSketchCurve.clear();
    this->m_history.clear();
}


void Sketch::clearSketch()
{
    this->m_curve.clear();
}


void Sketch::clearOverSketch()
{
    this->m_overSketchCurve.clear();
}


void Sketch::addCoordinate(double x, double y)
{
    this->m_curve.add(Point2D(x, y));
}


void Sketch::addCoordinate_OverSketch(double x, double y)
{
    this->m_overSketchCurve.add(Point2D(x, y));
}


unsigned int Sketch::getSketchSize()
{
    return this->m_curve.size();
}


Curve2D Sketch::getSketch()
{
    return this->m_history.getCurve();
}


Curve2D Sketch::getOverSketch()
{
    return this->m_overSketchCurve;
}


void Sketch::addSketch()
{
    this->m_history.push(Curve2D());
}


void Sketch::addSketch(Curve2D curve)
{
    this->m_curve.clear();
    this->m_curve = curve;
    this->m_history.push(curve);
}


double Sketch::distanceTo(double point_x, double point_y)
{
    return (this->m_curve.distanceTo(Point2D(point_x, point_y)));
}


bool Sketch::closeCurve()
{
    Point2D first, last;
    first = this->m_curve.atBegin();
    last = this->m_curve.atEnd();

    double x = first.x() - last.x();
    double y = first.y() - last.y();
    double dist = sqrt(pow(x, 2) + pow(y, 2));

    if (dist < 20)
    {
        this->m_curve.close(true);
        this->m_history.getCurve().close(true);
        this->m_history.getCurve().push_back(first);
        return true;
    }

    return false;
}


void Sketch::smooth()
{
    this->m_curve.lineFilter(0.5, 4);
    this->m_curve.meanFilter();
    this->m_curve.meanFilter();
}


bool Sketch::overSketch()
{
    //- Make sure that the oversketch isn't only one point (one click near the sketch)
    if (this->m_overSketchCurve.size() > 1)
    {
        double err_ = 1.0;
        double aprox_factor_ = 20.0;

        Curve2D rest, modified_sketch;

        this->m_overSketchCurve.lineFilter(0.5, 4);
        this->m_overSketchCurve.meanFilter();
        this->m_overSketchCurve.meanFilter();

        modified_sketch = this->m_overSketchCurve.overSketch(this->m_history.getCurve(), rest, err_, aprox_factor_);

        if (this->m_history.getCurve() != modified_sketch)
        {
            this->m_overSketchCurve.clear();
            this->m_curve.clear();
            this->m_curve = modified_sketch;
            this->m_curve.meanFilter();
            return true;
        }
    }
    this->m_overSketchCurve.clear();
    this->m_history.pop();
    return false;
}


bool Sketch::hasHistory()
{
    return !(this->m_history.isEmpty());
}


void Sketch::copyPreviousHistory()
{
    this->m_history.popUntilCurrentPosition();
    this->m_history.pushLastCurve();
    this->m_curve = this->m_history.getCurve();
}


void Sketch::addHistory()
{
    this->m_history.setCurve(this->m_curve);
}


void Sketch::removeHistory()
{
    this->m_history.pop();
}


bool Sketch::isModified()
{
    return this->m_history.isSketchModified();
}


bool Sketch::undo()
{
    return this->m_history.undo();
}


bool Sketch::redo()
{
    return this->m_history.redo();
}


void Sketch::translate(Point2D newPos)
{
    this->m_curve.translate(newPos);
}


void Sketch::rotate(double angle)
{
    //- Verify if the curve isn't empty
    if (this->m_curve.size())
    {
        double px = this->m_curve[this->m_curve.size() / 2].x();
        double py = this->m_curve[this->m_curve.size() / 2].y();
        Point2D center(px, py);
        this->m_curve.translate(Point2D(center.x() * (-1), center.y() * (-1)));

        double newX, newY;
        for (unsigned int i = 0; i < this->m_curve.size(); i++)
        {
            newX = this->m_curve[i].x() * cos(angle) - this->m_curve[i].y() * sin(angle);
            newY = this->m_curve[i].x() * sin(angle) + this->m_curve[i].y() * cos(angle);
            this->m_curve[i].x() = newX;
            this->m_curve[i].y() = newY;
        }

        this->m_curve.translate(Point2D(center.x(), center.y()));
    }
}


void Sketch::rotate(double angle, double center_x, double center_y)
{
    //- Verify if the curve isn't empty
    if (this->m_curve.size())
    {
        this->m_curve.translate(Point2D(center_x * (-1), center_y * (-1)));

        double newX, newY;
        for (unsigned int i = 0; i < this->m_curve.size(); i++)
        {
            newX = this->m_curve[i].x() * cos(angle) - this->m_curve[i].y() * sin(angle);
            newY = this->m_curve[i].x() * sin(angle) + this->m_curve[i].y() * cos(angle);
            this->m_curve[i].x() = newX;
            this->m_curve[i].y() = newY;
        }

        this->m_curve.translate(Point2D(center_x, center_y));
    }
}


void Sketch::scale(Point2D scale)
{
    //- Verify if the curve isn't empty
    if (this->m_curve.size())
    {
        double px = this->m_curve[this->m_curve.size() / 2].x();
        double py = this->m_curve[this->m_curve.size() / 2].y();
        Point2D center(px, py);
        this->m_curve.translate(Point2D(center.x() * (-1), center.y() * (-1)));
        this->m_curve.scale(scale);
        this->m_curve.translate(Point2D(center.x(), center.y()));
    }
}


void Sketch::scale(double mouse_x, double mouse_y, int i)
{
    //- Verify if the curve isn't empty
    if (this->m_curve.size())
    {
        double px = this->m_curve[this->m_curve.size() / 2].x();
        double py = this->m_curve[this->m_curve.size() / 2].y();
        Point2D center(px, py);
        double scale;

        if (mouse_x > px || mouse_y < py)
            scale = 0.975;
        else
            scale = 1.025;

        this->m_curve.translate(Point2D(center.x() * (-1), center.y() * (-1)));
        this->m_curve.scale(Point2D(scale, scale));
        this->m_curve.translate(Point2D(center.x(), center.y()));
    }
}


bool Sketch::isValid()
{
    //- Verify if the curve isn't empty
    if (this->m_curve.size())
    {
        //- Verify if the sketch was created from left to right
        if (this->m_curve[0].x() < this->m_curve[this->m_curve.size() - 1].x())
        {
            for (unsigned int i = 0; i < this->m_curve.size() - 1; i++)
            {
                if (this->m_curve[i + 1].x() < this->m_curve[i].x())
                    return false;
            }
        }
        else {
            if (Comments::SBIM_SKETCH())
            {
                //qDebug() << "\n Sketch::isValid(): \t size: " << m_curve.size();
            }

            for (unsigned int i = this->m_curve.size() - 2; i > 0; i--)
            {
                if (this->m_curve[i + 1].x() < this->m_curve[i].x())
                    return false;
            }
        }
    }
    
    return true;
}


void Sketch::turnIntoMonotonic()
{
    //- Verify if the curve isn't empty
    if (this->m_curve.size())
    {
        Curve2D monotonic;

        //- Verify if the sketch was created from left to right
        if (this->m_curve[0].x() < this->m_curve[this->m_curve.size() - 1].x())
        {
            monotonic.add(Point2D(this->m_curve[0].x(), this->m_curve[0].y()));

            for (unsigned int i = 1; i < this->m_curve.size(); i++)
            {
                unsigned int j = monotonic.size() - 1;

                if (this->m_curve[i].x() > monotonic[j].x())
                {
                    monotonic.add(Point2D(this->m_curve[i].x(), this->m_curve[i].y()));
                }
            }
        }
        else {
            monotonic.add(Point2D(this->m_curve[this->m_curve.size() - 1].x(), this->m_curve[this->m_curve.size() - 1].y()));

            for (unsigned int i = this->m_curve.size() - 2; i > 0; i--)
            {
                unsigned int j = monotonic.size() - 1;

                if (this->m_curve[i].x() > monotonic[j].x())
                {
                    monotonic.add(Point2D(this->m_curve[i].x(), this->m_curve[i].y()));
                }
            }
        }
        this->m_curve.clear();
        this->m_curve = monotonic;
    }
}


bool Sketch::isValid_Y()
{
    //- Verify if the curve isn't empty
    if (this->m_curve.size())
    {
        //- Verify if the trajectory was created from top to bottom
        if (this->m_curve[0].y() < this->m_curve[this->m_curve.size() - 1].y())
        {
            for (unsigned int i = 0; i < this->m_curve.size() - 1; i++)
            {
                if (this->m_curve[i + 1].y() > this->m_curve[i].y())
                    return false;
            }
        }
        else {
            for (unsigned int i = this->m_curve.size() - 2; i > 0; i--)
            {
                if (this->m_curve[i + 1].y() > this->m_curve[i].y())
                    return false;
            }
        }
    }
    return true;
}


void Sketch::turnIntoMonotonic_Y()
{
    //- Verify if the curve isn't empty
    if (this->m_curve.size())
    {
        Curve2D monotonic;

        //- Verify if the trajectory was created from up to bottom
        if (this->m_curve[0].y() < this->m_curve[this->m_curve.size() - 1].y())
        {
            monotonic.add(Point2D(this->m_curve[0].x(), this->m_curve[0].y()));

            for (unsigned int i = 1; i < this->m_curve.size(); i++)
            {
                unsigned int j = monotonic.size() - 1;

                if (this->m_curve[i].y() > monotonic[j].y())
                {
                    monotonic.add(Point2D(this->m_curve[i].x(), this->m_curve[i].y()));
                }
            }
        }
        else {
            monotonic.add(Point2D(this->m_curve[this->m_curve.size() - 1].x(), this->m_curve[this->m_curve.size() - 1].y()));

            for (unsigned int i = this->m_curve.size() - 2; i > 0; i--)
            {
                unsigned int j = monotonic.size() - 1;

                if (this->m_curve[i].y() > monotonic[j].y())
                {
                    monotonic.add(Point2D(this->m_curve[i].x(), this->m_curve[i].y()));
                }
            }
        }
        this->m_curve.clear();
        this->m_curve = monotonic;
    }
}

void Sketch::printInfo()
{
    if (Comments::SBIM_SKETCH())
    {
        //qDebug() << "\n Sketch::printInfo():";
        //qDebug() << "\t- Current curve size: \t" << this->m_curve.size();
        //qDebug() << "\t- OverSketch curve size: \t" << this->m_overSketchCurve.size();
        //qDebug() << "\t- History: \t" << this->m_history.isEmpty();
        this->m_history.printInfo();
    }
}