
/**
* @file sketch_template.cpp
* @author Sicilia Judice
* @brief File containing the class SketchTemplate
*/

#include "sketch_template.h"

#include <QDebug>

SketchTemplate::SketchTemplate()
{
	this->reset();
}


SketchTemplate::SketchTemplate(const SketchTemplate& newSketchTemplate)
{
	this->list = newSketchTemplate.list;
}


SketchTemplate& SketchTemplate::operator=(const SketchTemplate& newSketchTemplate)
{
	this->list = newSketchTemplate.list;

	return *this;
}


SketchTemplate::~SketchTemplate()
{
	this->reset();
}


void SketchTemplate::reset()
{
	//- Deallocate memory for each sketch template in the list:
	for (auto i : this->list)
		i.clear();

	this->list.clear();
}


size_t SketchTemplate::getNumberOfSketchTemplates()
{
	return this->list.size();
}


bool SketchTemplate::saveSketchTemplate(std::vector<double> curve)
{
	if (isDuplicated(curve))
	{
		if (Comments::SBIM_SKETCH_TEMPLATE())
		{
			//qDebug() << "\n SketchTemplate::saveSketchTemplate(): \t Sketch Template already saved!";
		}
		return false;
	}
	else {
		this->list.push_back(curve);
		return true;
	}
}


std::vector<double> SketchTemplate::loadSketchTemplate()
{
	std::vector<double> response;
	
	//- If the list is not empty copy the last sketch template saved
	if(this->list.size())
		response = this->list[this->list.size() - 1];

	return response;
}


std::vector<double> SketchTemplate::getTemplates()
{
	std::vector<double> response;

	for (int i = 0; i < this->list.size(); i++)
	{
		response.reserve(response.size() + this->list[i].size());
		response.insert(response.end(), this->list[i].begin(), this->list[i].end());
	}

	if (Comments::SBIM_SKETCH_TEMPLATE())
	{
		//qDebug() << "\n SketchTemplate::getTemplates(): \t  List and Response size: " << list.size() << ", " << response.size();
	}

	return response;
}


std::vector<double> SketchTemplate::getTemplate(size_t i)
{
	return this->list[i];
}


bool SketchTemplate::isDuplicated(std::vector<double> curve)
{
	for (size_t i = 0; i < this->list.size(); i++)
	{
		if (curve == this->list[i])
		{
			return true;
		}
	}

	return false;
}


