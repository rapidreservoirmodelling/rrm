
/**
* @file surface.h
* @author Sicilia Judice
* @brief File containing the class Surface
*/

#ifndef SURFACE_H
#define SURFACE_H

#include <vector>

#include "sketch.h"
#include "Constants.h"
#include "cross_section.h"

/**
* Surface, in SBIM context, is a collection of sketches and its associated rules.
* Each sketch belongs to a specific cross section.
* An editable surface means that it is still being created, so its sketches can be edited.
* Or it means that the surface was turn to editable through an undo process.
* When the surface is submitted to the 3D model, it is no longer editable.
* Every surface has an ID different to zero. A zero ID means that this surface is empty.
*/

class Surface {

public:

	/**
	* Constructor.
	*/
	Surface();


	/**
	* Copy constructor.
	* @param newSurface a const reference to another surface.
	*/
	Surface(const Surface& newSurface);


	/**
	* Assignment operator
	* @param newSurface a const reference to another surface.
	*/
	Surface& operator=(const Surface& newSurface);


	/**
	* Destructor.
	*/
	~Surface();


	/**
	* Method to clear the surface.
	* Clear the collection of sketches, turn the ID to 0 and the editable flag to true.
	* @return void
	*/
	void clear();


	/**
	* Method to clear the current sketch.
	* If there is any sketch in the current cross section, it will first clear the sketch.
	* Then it will erase the memory allocated to this sketch.
	* @param crossSection is the current cross section where the sketch is being edited.
	* @param direction is the direction of the cross section.
	* @return void
	*/
	void clearCurrentSketch(size_t crossSection, size_t direction);


	/**
	* Method to set if the surface is editable or not.
	* @param status is TRUE when the surface is editable, FALSE otherwise.
	* @return void.
	*/
	void setEditionMode(bool status);


	/**
	* Method that returns the value of editable flag.
	* @return bool
	*/
	bool isEditable();


	/**
	* Method that returns the value of visible flag.
	* @return bool
	*/
	bool isVisible();


	/**
	* Method that sets the visibility of the surface.
	* @param flag defines if the surface is visible or not
	* @return void
	*/
	void setVisibility(bool flag);


	/**
	* Method that returns the ID of the surface.
	* @return int
	*/
	int getId();


	/**
	* Method that sets the ID of the surface.
	* @param newId is the ID of the surface.
	* @return void
	*/
	void setId(int newId);


	/**
	* Method that return the number of cross sections.
	* @return unsigned int
	*/
	unsigned int getNumberOfCrossSections();


	/**
	* Method that return the cross section ID.
	* Returning -1 means that there is no cross section.
	* @return int
	*/
	int getCrossSectionID(unsigned int index);

	int getPreviousSketchCrossSection(int crossSection);
	int getNextSketchCrossSection(int crossSection);


	/**
	* Method that return the number of sketches.
	* @return unsigned int
	*/
	unsigned int getNumberOfSketches();
	unsigned int getNumberOfSketches(int index);


	/**
	* Method that return the number of sketches inside the current cross section.
	* @param crossSection is the ID of the cross section where the sketch belongs.
	* @param direction is the ID of the direction of the cross section.
	* @return unsigned int
	*/
	unsigned int getNumberOfCurrentSketches(int crossSection, int direction);


	/**
	* Method that return the number of sketches.
	* It is used when the user submits the sketch to create the 3D surface.
	* If there is only one sketch, its cross section and direction will be updated inside the parameters.
	* If there is more than one sketch, these parameters will not make difference.
	* @param crossSection to store the cross section of the sketch.
	* @param direction to store the direction of the sketch.
	* @return unsigned int
	*/
	unsigned int getNumberOfSketches(int &crossSection, int &direction);


	/**
	* Method that returns true if the current cross section and its respective direction exists in the collection of sketches.
	* If it exists it means that the editing operators occurs over the history of that cross section.
	* If it does not exist, it means that a new sketch need to be added. But this is not deal in this function.
	* @return bool
	*/
	bool hasCrossSection(size_t crossSection, size_t direction);

	/**
	* Method that returns TRUE if there is at least one sketch available.
	* It returns false if there is no sketch or if the last state of the history is a clean sketch.
	* @return bool
	*/
	bool hasSketch();


	/**
	* Method to allocate memory for a new cross section.
	* If there is no parameter curve, the new sketch inside the cross section will be empty.
	* @param crossSection is the ID of the cross section where the sketch belongs.
	* @param direction is the ID of the direction of the cross section.
	* @param curve is the new sketch inside the new cross section.
	* @return void
	*/
	void addNewCrossSection(size_t crossSection, size_t direction);
	void addNewCrossSection(size_t crossSection, size_t direction, Curve2D curve);
	
	
	//- DELETE
	/**
	* Method to allocate memory for a new sketch.
	* @param crossSection is the ID of the cross section where the sketch belongs.
	* @param direction is the ID of the direction of the cross section.
	* @return void
	*/
	//void addNewSketch(size_t crossSection, size_t direction);
	//void addNewSketch(size_t crossSection, size_t direction, Curve2D curve);


	/**
	* Method to add sketch to the history.
	* @param crossSection is the ID of the cross section where the sketch belongs.
	* @param direction is the ID of the direction of the cross section.
	* @param curve the sketch to be added.
	* @return void
	*/
	void addSketch(size_t crossSection, size_t direction, Curve2D curve);
	void addSketch(size_t crossSection, size_t direction);


	/**
	* Method to add a horizontal sketch to the history.
	* @param crossSection is the ID of the cross section where the sketch belongs.
	* @param direction is the ID of the direction of the cross section.
	* @param py is the y coordinate of the sketch point (height of the line).
	* @return void
	*/
	void addSketchLine(size_t crossSection, size_t direction, double py, double length);
	
	
	/**
	* Method to add a circle sketch to the history.
	* @param crossSection is the ID of the cross section where the sketch belongs.
	* @param direction is the ID of the direction of the cross section.
	* @param px is the x coordinate of the sketch point.
	* @param py is the y coordinate of the sketch point.
	* @param radius is the radius of the circle.
	* @return void
	*/
	void addSketchCircle(size_t crossSection, size_t direction, double px, double py, double radiusX, double radiusY);
	
	
	/**
	* Method to copy the previous sketch to the top of the stack (history).
	* It returns true if the copy was made.
	* @param crossSection is the id of the cross section where we need to copy the sketch. It is used to locate the sketch inside the collection of sketches.
	* @param direction is the direction of the cross section.
	* @return void
	*/
	bool copyPreviousSketch(size_t crossSection, size_t direction);


	/**
	* Method to add a coordinate to the current sketch.
	* The index will identify which is the current sketch.
	* If it is not an oversketch, the method will add the update to the history of the sketch.
	* @param px is the x coordinate of the sketch point.
	* @param py is the y coordinate of the sketch point.
	* @param crossSection is the id of the cross section where the sketch belongs.
	* @param direction is the direction of the cross section.
	* @return void
	*/
	void addPoint(double px, double py, size_t crossSection, size_t direction);


	void addLine(double ax, double ay, double bx, double by, size_t crossSection, size_t direction);

	/**
	* Method that updates the flag oversketch.
	* @param crossSection is the numerical identification of the cross section.
	* @param direction is the numerical identification of the direction of the cross section.
	* @return bool
	*/
	bool isOverSketch(int crossSection, int direction);


	/**
	* Method to apply the oversketch process in the current sketch.
	* It returns true if the process succeed.
	* @param crossSection is the id of the cross section where the sketch belongs.
	* @param direction is the direction of the cross section.
	* @return bool
	*/
	bool applyOverSketch(size_t crossSection, size_t direction);


	/**
	* Method that returns TRUE if the current sketch is near the mouse pointer.
	* @param crossSection is the id of the cross section where the sketch belongs.
	* @param direction is the direction of the cross section.
	* @param mouse_x is the x coordinate of the mouse pointer.
	* @param mouse_y is the y coordinate of the mouse pointer.
	* @return double
	*/
	double distanceFromSketchToMousePointer(size_t crossSection, size_t direction, double mouse_x, double mouse_y);


	/**
	* Method that returns the index of the sketch highlighted.
	* If it returns -1 there is no sketch highlighted.
	* @param crossSection is the numerical identification of the cross section.
	* @param direction is the numerical identification of the direction of the cross section.
	* @return int
	*/
	int getHighlightIndex(int crossSection, int direction);


	/**
	* Method that returns the index of the sketch selected.
	* If it returns -1 there is no sketch selected.
	* @param crossSection is the numerical identification of the cross section.
	* @param direction is the numerical identification of the direction of the cross section.
	* @return int
	*/
	int getSelectIndex(int crossSection, int direction);


	/**
	* Method that returns TRUE if the current sketch is highlighted.
	* @param crossSection is the id of the cross section where the sketch belongs.
	* @param direction is the direction of the cross section.
	* @param mouse_x is the x coordinate of the mouse pointer.
	* @param mouse_y is the y coordinate of the mouse pointer.
	* @return bool
	*/
	bool isSketchHighlighted(size_t crossSection, size_t direction, double mouse_x, double mouse_y);
	bool isSketchHighlighted(size_t crossSection, size_t direction);


	/**
	* Method to select the current sketch if the same is highlighted.
	* @param crossSection is the id of the cross section where the sketch belongs.
	* @param direction is the direction of the cross section.
	* @return bool
	*/
	bool isSketchSelected(size_t crossSection, size_t direction);
	bool isSketchSelected(size_t crossSection, size_t direction, double mouse_x, double mouse_y);


	/**
	* Method that reset the highlight flag to false for each sketch.
	* @return void
	*/
	void resetHighlight();


	/**
	* Method that reset the select index to -1.
	* @return void
	*/
	void resetSelected();


	/**
	* Method to apply the smooth filter in the current sketch.
	* @param crossSection is the id of the cross section where the sketch belongs.
	* @param direction is the direction of the cross section.
	* @return void
	*/
	void applySmooth(size_t crossSection, size_t direction);


	/**
	* Method to apply the rotation over the current sketch.
	* @param angle of rotation.
	* @param crossSection is the id of the cross section where the sketch belongs.
	* @param direction is the direction of the cross section.
	* @return void.
	*/
	void applyRotation(double angle, size_t crossSection, size_t direction);
	void applyRotation(double angle, size_t crossSection, size_t direction, double center_x, double center_y);


	/**
	* Method to apply the scale over the current sketch.
	* @param ex is the factor of the scale in x axis.
	* @param ey is the factor of the scale in y axis.
	* @param crossSection is the id of the cross section where the sketch belongs.
	* @param direction is the direction of the cross section.
	* @return void.
	*/
	void applyScale(double ex, double ey, size_t crossSection, size_t direction);
	void applyScale(size_t crossSection, size_t direction, double mouse_x, double mouse_y);


	/**
	* Method to apply the translation over the current sketch.
	* @param dx is the factor of translation in x axis.
	* @param dy is the factor of translation in y axis.
	* @param crossSection is the id of the cross section where the sketch belongs.
	* @param direction is the direction of the cross section.
	* @return void.
	*/
	void applyTranslation(double dx, double dy, size_t crossSection, size_t direction);


	/**
	* Method that returns TRUE if the current edition is a modified sketch in comparison to the previous one.
	* @param crossSection is the id of the cross section where the sketch belongs.
	* @param direction is the direction of the cross section.
	* @return bool
	*/
	bool isSketchModified(size_t crossSection, size_t direction);


	/**
	* Method to update the first sketch in a collection and to set the mode to oversketch.
	* It will return TRUE if the sketch was updated, FALSE if it was cleared
	* @param crossSection is the id of the cross section where the sketch belongs.
	* @param direction is the direction of the cross section.
	* @return bool
	*/
	bool updateSKetch(size_t crossSection, size_t direction);


	/**
	* Method that return the sketch at i position (Model Coordinate System).
	* @param csIndex is the indice of the cross section the has the sketch contour.
    * @param sketchIndex is the indice of the sketch contour.
	* @return Curve2D
	*/
	Curve2D getSketch(unsigned int csIndex, unsigned int sketchIndex);


	/**
	* Method that return the curve of the current sketch (Model Coordinate System).
	* @param crossSection is the id of the cross section where we need to get the sketch.
	* @param direction is the direction of the cross section.
	* @param i is the index position inside the sketch collection
	* @return Curve2D
	*/
	Curve2D getCurrentSketch(size_t crossSection, size_t direction);
	Curve2D getCurrentSketch(size_t crossSection, size_t direction, unsigned int i);


	/**
	* Method that return the curve of the highlighted sketch (Model Coordinate System).
	* @param crossSection is the id of the cross section where we need to get the sketch.
	* @param direction is the direction of the cross section.
	* @return Curve2D
	*/
	Curve2D getHighlightedSketch(size_t crossSection, size_t direction);


	/**
	* Method that return the curve of the selected sketch (Model Coordinate System).
	* @param crossSection is the id of the cross section where we need to get the sketch.
	* @param direction is the direction of the cross section.
	* @return Curve2D
	*/
	Curve2D getSelectedSketch(size_t crossSection, size_t direction);


	/**
	* Method that return the curve of the current oversketch (Model Coordinate System).
	* @param crossSection is the id of the cross section where we need to get the over sketch.
	* @param direction is the direction of the cross section.
	* @return Curve2D
	*/
	Curve2D getCurrentOverSketch(size_t crossSection, size_t direction);


	/**
	* Method that returns the points of one sketch used to create the surface.
	* Points coordinate are (x, y).
	* @param crossSection is the id of the cross section where the sketch belongs.
	* @param direction is the direction of the cross section.
	* @return Curve2D
	*/
	Curve2D getExtrudedSurfacePoints(size_t crossSection, size_t direction);


	/**
	* Method that returns the points of all sketches used to create the surface in LENGTH direction.
	* Points coordinate are (x, z, y).
	* @param box_size dimension of the bounding box in z direction
	* @param discretization is the disretization along the length direction of the 3D Box
	* @return std::vector<double>
	*/
	std::vector<double> getSurfacePoints_Length(double box_size, int discretization);


	/**
	* Method that returns the points of all sketches used to create the surface in WIDTH direction.
	* Points coordinate are (z, x, y).
	* @param box_size dimension of the bounding box in z direction
	* @param discretization is the disretization along the width direction of the 3D Box
	* @return std::vector<double>
	*/
	std::vector<double> getSurfacePoints_Width(double box_size, int discretization);


	/**
	* Method that returns the points of all sketches used to create the surface in HEIGHT direction.
	* Points coordinate are (x, z, y).
	* @param box_size dimension of the bounding box in h direction
	* @param discretization is the disretization along the height direction of the 3D Box
	* @return std::vector<double>
	*/
	std::vector<double> getSurfacePoints_Height(double box_size, int discretization);


	/**
	* Method that apply the undo over the history of the sketch.
	* It returns true if the undo was successful.
	* @param crossSection is the id of the cross section where we need to apply the undo.
	* @param direction is the direction of the cross section.
	* @return bool
	*/
	bool undoSketch(size_t crossSection, size_t direction);


	/**
	* Method that apply the redo over the history of the sketch.
	* It returns true if the redo was successful.
	* @param crossSection is the id of the cross section where we need to apply the redo.
	* @param direction is the direction of the cross section.
	* @return bool
	*/
	bool redoSketch(size_t crossSection, size_t direction);


	/**
	* Method that validate the sketch.
	* If the sketch is not valid, it will turn the sketch into a monotonic curve.
	* Once the sketch was changed, it will smooth and update the history of the sketch.
	* @return void
	*/
	void validateSketch(size_t crossSection, size_t direction);


	/**
	* Method to print info of surface object, for debugs purpose.
	* @return void
	*/
	void printInfo();


private:

	int m_id;					/**< A numerical identification to the surface. */

	bool is_editable;			/**< A flag to indicate if the sketches can be edited or not. A surface becomes not editable when it is submitted to the 3D model.*/

	bool is_visible;			/**< A flag to indicate if the surface is visible or not (it is not visible after an undo for example).*/

	size_t index;				/**< An index to keep track of which sketch we are editing.*/

	std::vector<std::shared_ptr<CrossSection>> m_crossSections; /**< A collection of cross sections used to create a surface. */


	//--- METHODS

	bool updateIndex(size_t crossSection, size_t direction);		/**< Method to update the index according to the cross section ID and its respective direction. Returns true of the updated was successful.*/

};

#endif //SURFACE_H

