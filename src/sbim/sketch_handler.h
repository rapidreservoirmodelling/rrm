#ifndef SKETCHHANDLER_H
#define SKETCHHANDLER_H

#include <memory>
#include <vector>

#include "surface.h"
#include "libs/polygonalcurve/CurveN.hpp"

#include <QObject>
#include "Constants.h"

/**
* It is an interface between the GUI and SBIM context.
* It knows the state of the GUI: for instance, it knows which cross section is being displayed,
* if the sketch is a new curve or a correction, etc.
* It will receive the data from CanvasHandler
*/
class SketchHandler : public QObject 
{
    Q_OBJECT

public:

    /**
    * Constructor.
    */
    explicit SketchHandler(QObject* parent = 0);


    /**
    * Method that reset SketchHandler properties to its initial state.
    * @return void
    */
    void reset();


    int getPreviousSketchCrossSection(int crossSection);
    int getNextSketchCrossSection(int crossSection);


    /**
    * Method that returns a list of the surfaces ID.
    * @return std::vector<int>
    */
    std::vector<int> getSurfacesID();


    /**
    * Method to update the surfaceID to be used when creating a surface.
    * @return void
    */
    void setSurfaceID(int id);


    /**
    * Method that update visibility of a specific surface.
    * @param surfaceID is the ID of the surface
    * @param flag is the visibility 
    * @return bool
    */
    bool setSurfaceVisibility(int surfaceID, bool flag);


    /**
    * Method that receives the sketched point from CanvasHandler (Model Coordinate System).
    * @param x coordinate.
    * @param y coordinate.
    * @return void
    */
    void setSketchedPoint(double x, double y);


    /**
    * method to set the length of the horizontal line.
    * @param lenght
    * @return void
    */
    void setSketchLineLength(double length);


    /**
    * method to set the radius of the sketch circle.
    * @param radius
    * @return void
    */
    void setSketchCircleRadiusX(double radius);
    void setSketchCircleRadiusY(double radius);
    
    
    /**
    * Method that reeives from CanvasHandler actions related to the sketch.
    * @param buttonID identifies which button from the mouse is in use
	* @param buttonAction identifies which action from the mouse is in use
    * @param canvasAction identifies which action is being performed over the sketch
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return void
    */
    void delegateSketchAction(int buttonID, int buttonAction, int canvasAction, int crossSection, int direction);
    void delegateSketchAction(int canvasAction, int crossSection, int direction);


    /**
    * Method that reeives from CanvasHandler actions related to the trajectory.
    * @param buttonID identifies which button from the mouse is in use
    * @param buttonAction identifies which action from the mouse is in use
    * @param canvasAction identifies which action is being performed over the sketch
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return void
    */
    void delegateTrajectoryAction(int buttonID, int buttonAction, int canvasAction, int crossSection, int direction);
    void delegateTrajectoryAction(int canvasAction, int crossSection, int direction);

    
    /**
    * Method that will be called when the user selects different direction from the GUI.
    * It will verify if there is still sketch memory that was cleared before changing the direction.
    * This memory needs to be deleted.
    * @return void
    */
    void updateSketchDataStructure();


    /**
    * Method that updates the flag oversketch.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return bool
    */
    bool isOverSketch(int crossSection, int direction);


    /**
    * Method that returns TRUE if there is at least one sketch or one trajectory.
    * It will return FALSE if there is no sketch available (after using CLEAR button).
    * @return bool
    */
    bool hasSketch();

   
    /**
    * Method that returns if the cross section has a sketch.
    * This method is used to verify if the sketching attempt by the user is indeed a sketch or an oversketch.
    * This is because we can only have one sketch at a time at the same cross section.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return bool
    */
    bool hasSketch(int crossSection, int direction);


    /**
    * Method that returns if there is a current trajectory being sketched.
    * This method is used to verify if the sketching attempt by the user is indeed a sketch or an oversketch.
    * This is because we can only have one sketch at a time at the same cross section.
    * @return bool
    */
    bool hasTrajectory();

    
    /**
    * Method that return the number of sketches inside the current cross section.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return unsigned int
    */
    unsigned int getNumberOfSketches(int crossSection, int direction);
    unsigned int getNumberOfSketches(int i);
    unsigned int getNumberOfSketchesSubmitted(int i);


    /**
    * Method that return the number of sketch contours that is current editable.
    * It is used right before when the user submit a surface created in MapView.
    * @return unsigned int
    */
    unsigned int getNumberOfSketchContours();


    /**
    * Method that return the number of cross sections with sketch on it.
    * @return unsigned int
    */
    unsigned int getNumberOfCrossSections();
    unsigned int getNumberOfCrossSectionsSubmitted();


    /**
    * Method that return the cross section ID.
    * Returning -1 means that there is no cross section.
    * @return int
    */
    int getCrossSectionID(unsigned int i);
    int getCrossSectionIDSubmitted(unsigned int i);

    
    /**
    * Method that returns the sketch contour at position i (Model Coordinate System).
    * @param csIndex is the indice of the cross section the has the sketch contour.
    * @param sketchIndex is the indice of the sketch contour.
    * @return std::vector<double>
    */
    std::vector<double> returnSketchContour(unsigned int csIndex, unsigned int sketchIndex, int &surfaceID);
    std::vector<double> returnSketchContourSubmitted(unsigned int csIndex, unsigned int sketchIndex, int& surfaceID);


    /**
    * Method that returns the current sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @param i is the index position of the sketch collection
    * @return std::vector<double>
    */
    std::vector<double> returnSketch(int crossSection, int direction);  
    std::vector<double> returnSketch(int crossSection, int direction, unsigned int i);


    /**
    * Method that returns the highlited sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> returnHighlightedSketch(int crossSection, int direction);


    /**
    * Method that returns the selected sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> returnSelectedSketch(int crossSection, int direction);

    
    /**
    * Method that returns the current over sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> returnOverSketch(int crossSection, int direction);


    /**
    * Method that returns the current sketch of the trajectory (Model Coordinate System).
    * @return std::vector<double>
    */
    std::vector<double> returnTrajectory();


    /**
    * Method that return TRUE if the current sketch is near the mouse pointer
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return bool
    */
    bool highlightSketch(int crossSection, int direction);


    /**
    * Method that return TRUE if the current trajectory is near the mouse pointer
    * @return bool
    */
    bool highlightTrajectory();


    /**
    * Method that returns the index of the sketch highlighted.
    * If it returns -1 there is no sketch highlighted.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return int
    */
    int getHighlightIndex(int crossSection, int direction);
    

    /**
    * Method that returns the index of the sketch selected.
    * If it returns -1 there is no sketch selected.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return int
    */
    int getSelectIndex(int crossSection, int direction);

    
    /**
    * Method that verify if the current sketch is highlighted.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return bool
    */
    bool isSketchHighlighted(int crossSection, int direction);


    /**
    * Method to select the sketch when the user click on it with the LEFT mouse button.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return bool
    */
    bool selectSketch(int crossSection, int direction);

    
    /**
    * Method to select the sketch that is highlighted when the user click on it with the LEFT mouse button.
    * used in MapView when there is more than one sketch.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return bool
    */
    bool selectHighlightedSketch(int crossSection, int direction);


    /**
    * Method to select the current trajectory when the user click on it with the LEFT mouse button.
    * @return bool
    */
    bool selectTrajectory();


    /**
    * Method to reset the selected index
    * @return void
    */
    void resetSelect();


    /**
    * Method to apply the smooth filter over the current sketch.
    * It returns the smoothed sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> smoothSketch(int crossSection, int direction);


    /**
    * Method to apply the smooth filter over the current trajectory.
    * It returns the smoothed trajectory (Model Coordinate System).
    * @return std::vector<double>
    */
    std::vector<double> smoothTrajectory();


    /**
    * Method to clear the current sketch.
    * This action is added as a step inside the history, so the user can undo if necessary.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return void
    */
    void clearSketch(int crossSection, int direction);


    /**
    * Method to clear the current sketch.
    * This action is added as a step inside the history, so the user can undo if necessary.
    * @return void
    */
    void clearTrajectory();

    
    /**
    * Method to load the saved template as a sketch in the current cross section.
    * It returns the new sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @param newSketch is the sketch template already transformed into Curve2D type (Mode Coordinate System)
    * @return std::vector<double>
    */
    std::vector<double> loadSketchTemplate(int crossSection, int direction, std::vector<double> newSketch);


    /**
    * Method to copy the trajectory template selected by the user.
    * @param path is the trajectory copied by the user.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return void
    */
    void copyTrajectoryTemplate(std::vector<double> path, int crossSection, int direction);


    /**
    * Method to submit the collection of sketches to create a 3D model.
    * This method is called by CanvasHandler to verify if there is sketches to be submitted.
    * The communication with Model3D will be deal inside CanvasHandler.
    * @param isExtruded is a flag to store if the surface creation was by Extruded Surface or not.
    * @param points store the collection of points to be added as surface into the 3D Model
    * @param boxDimension used in the creation of the surface
    * @param boxDiscretization used in the creation of the surface
    * @param surfaceID keeps track of the surface ID
    * @param direction is the numerical identification of the direction of the cross section.
    * @return bool
    */
    bool submitSketch(bool &isExtruded, std::vector<double>& points, double boxDimension, int boxDiscretization, int &surfaceID, int& csID, int direction);
    bool submitPreview(bool& isExtruded, std::vector<double>& points, double boxDimension, int boxDiscretization, int &surfaceID, int& csID, int direction);


    /**
    * Method to submit the collection of sketches to create a 3D model.
    * This method is called by CanvasHandler to verify if there is sketches and trajectory to be submitted.
    * The communication with Model3D will be deal inside CanvasHandler.
    * @param isExtruded is a flag to store if the surface creation was by Extruded Surface or not.
    * @param points store the collection of points to be added as surface into the 3D Model
    * @param path stores the trajectory
    * @param boxDimension used in the creation of the surface
    * @param boxDiscretization used in the creation of the surface
    * @param surfaceID stores the ID of the new surface
    * @param direction is the numerical identification of the direction of the cross section.
    * @return bool
    */
    bool submitSketch(bool& isExtruded, std::vector<double>& points, std::vector<double>& path, double boxDimension, int boxDiscretization, int& surfaceID, int& csID, int direction);
    bool submitPreview(bool& isExtruded, std::vector<double>& points, std::vector<double>& path, double boxDimension, int boxDiscretization, int& surfaceID, int& csID, int direction);


    /**
    * Method to submit the collection of sketches in the MapView to create a 3D model.
    * This method is called by CanvasHandler to verify if there is sketches to be submitted.
    * The communication with Model3D will be deal inside CanvasHandler.
    * @param points store the collection of points to be added as surface into the 3D Model
    * @param boxDimension used in the creation of the surface
    * @param boxDiscretization used in the creation of the surface
    * @param surfaceID keeps track of the surface ID.
    * @return bool
    */
    bool submitSketchContour(std::vector<double>& points, double boxDimension, int boxDiscretization, int &surfaceID);
    bool submitSketchContourPreview(std::vector<double>& points, double boxDimension, int boxDiscretization, int &surfaceID);


    /**
    * Method to deal with the PRESS event of the mouse when the current action is a Sketching.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return void
    */
    void SketchPress(int crossSection, int direction);

    
    /**
    * Method to deal with the MOVE event of the mouse when the current action is a Sketching.
    * It will return the updated curve to be refreshed on the visalization side.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> SketchMove(int crossSection, int direction);

    
    /**
    * Method to deal with the RELEASE event of the mouse when the current action is a Sketching.
    * It will return the updated curve to be refreshed on the visalization side.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> SketchRelease(int crossSection, int direction);


    /**
    * Method to deal with the CLICK event of the mouse when the current action is a Sketching a Line.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return void
    */
    void SketchLineClick(int crossSection, int direction);

    
    /**
    * Method to add a horizontal sketch.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return void
    */
    void addSketchLine(int crossSection, int direction);


    /**
    * Method to deal with the CLICK event of the mouse when the current action is a Sketching Circle.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return void
    */
    void SketchCircleClick(int crossSection, int direction);


    /**
    * Method to add a sketch circle.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return void
    */
    void addSketchCircle(int crossSection, int direction);

    
    /**
    * Method to deal with the CLICK event of the mouse when the current action is a Sketching Ellipse.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return void
    */
    void SketchEllipseClick(int crossSection, int direction);


    /**
    * Method to add a sketch ellipse.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return void
    */
    void addSketchEllipse(int crossSection, int direction);

    
    /**
    * Method to deal with the PRESS event of the mouse when the current action is Translate.
    * It returns the updated sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> TranslatePress(int crossSection, int direction);


    /**
    * Method to deal with the MOVE event of the mouse when the current action is Translate.
    * It returns the updated sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> TranslateMove(int crossSection, int direction);


    /**
    * Method to deal with the RELEASE event of the mouse when the current action is Translate.
    * It returns the updated sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> TranslateRelease(int crossSection, int direction);


    /**
    * Method to deal with the PRESS event of the mouse when the current action is Rotate.
    * It returns the updated sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> RotatePress(int crossSection, int direction);


    /**
    * Method to deal with the MOVE event of the mouse when the current action is Rotate.
    * It returns the updated sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> RotateMove(int crossSection, int direction);


    /**
    * Method to deal with the RELEASE event of the mouse when the current action is Rotate.
    * It returns the updated sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> RotateRelease(int crossSection, int direction);


    /**
    * Method to deal with the PRESS event of the mouse when the current action is Scale.
    * It returns the updated sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> ScalePress(int crossSection, int direction);


    /**
    * Method to deal with the MOVE event of the mouse when the current action is Scale.
    * It returns the updated sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> ScaleMove(int crossSection, int direction);


    /**
    * Method to deal with the RELEASE event of the mouse when the current action is Scale.
    * It returns the updated sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return std::vector<double>
    */
    std::vector<double> ScaleRelease(int crossSection, int direction);

    
    /**
    * Method to undo the current edition in the sketch.
    * It returns the updated sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @param undo flag to indicate if the undo was done.
    * @return std::vector<double>
    */
    std::vector<double> undo(int crossSection, int direction, bool &undo);
    
    
    /**
    * Method to redo the last edition in the sketch.
    * It returns the updated sketch (Model Coordinate System).
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @param redo flag to indicate if the redo was done.
    * @return std::vector<double>
    */
    std::vector<double> redo(int crossSection, int direction, bool &redo);


    /**
    * Method to deal with the PRESS event of the mouse when the current action is a Sketching
    * and the user is sketching a trajectory in Map View.
    * @param crossSection is the numerical identification of the cross section.
    * @param direction is the numerical identification of the direction of the cross section.
    * @return void
    */
    void TrajectoryPress(int crossSection, int direction);


    /**
    * Method to deal with the MOVE event of the mouse when the current action is a Sketching
    * and the user is sketching a trajectory in Map View.
    * It will return the updated curve to be refreshed on the visalization side.
    * @return std::vector<double>
    */
    std::vector<double> TrajectoryMove();


    /**
    * Method to deal with the RELEASE event of the mouse when the current action is a Sketching
    * and the user is sketching a trajectory in Map View.
    * It will return the updated curve to be refreshed on the visalization side.
    * @return std::vector<double>
    */
    std::vector<double> TrajectoryRelease();


    /**
    * Method to deal with the PRESS event of the mouse when the current action is Translate.
    * It returns the updated trajectory (Model Coordinate System).
    * @return std::vector<double>
    */
    std::vector<double> TranslateTrajectoryPress();


    /**
    * Method to deal with the MOVE event of the mouse when the current action is Translate.
    * It returns the updated trajectory (Model Coordinate System).
    * @return std::vector<double>
    */
    std::vector<double> TranslateTrajectoryMove();


    /**
    * Method to deal with the RELEASE event of the mouse when the current action is Translate.
    * It returns the updated trajectory (Model Coordinate System).
    * @return std::vector<double>
    */
    std::vector<double> TranslateTrajectoryRelease();


    /**
    * Method to deal with the PRESS event of the mouse when the current action is Rotate.
    * It returns the updated trajectory (Model Coordinate System).
    * @return std::vector<double>
    */
    std::vector<double> RotateTrajectoryPress();


    /**
    * Method to deal with the MOVE event of the mouse when the current action is Rotate.
    * It returns the updated trajectory (Model Coordinate System).
    * @return std::vector<double>
    */
    std::vector<double> RotateTrajectoryMove();


    /**
    * Method to deal with the RELEASE event of the mouse when the current action is Rotate.
    * It returns the updated trajectory (Model Coordinate System).
    * @return std::vector<double>
    */
    std::vector<double> RotateTrajectoryRelease();


    /**
    * Method to deal with the PRESS event of the mouse when the current action is Scale.
    * It returns the updated trajectory (Model Coordinate System).
    * @return std::vector<double>
    */
    std::vector<double> ScaleTrajectoryPress();


    /**
    * Method to deal with the MOVE event of the mouse when the current action is Scale.
    * It returns the updated trajectory (Model Coordinate System).
    * @return std::vector<double>
    */
    std::vector<double> ScaleTrajectoryMove();


    /**
    * Method to deal with the RELEASE event of the mouse when the current action is Scale.
    * It returns the updated trajectory (Model Coordinate System).
    * @return std::vector<double>
    */
    std::vector<double> ScaleTrajectoryRelease();


    /**
    * Method to undo the current edition in the trajectory.
    * It returns the updated trajectory (Model Coordinate System).
    * @param undo flag to indicate if the undo was done.
    * @return std::vector<double>
    */
    std::vector<double> undoTrajectory(bool& undo);


    /**
    * Method to redo the last edition in the trajectory.
    * It returns the updated trajectory (Model Coordinate System).
    * @param redo flag to indicate if the redo was done.
    * @return std::vector<double>
    */
    std::vector<double> redoTrajectory(bool& redo);

signals:
    void updateSelectTrajectory(bool response);
    void updateHighlightTrajectory(bool response);
    void updateSelectSketch(bool response);
    void updateHighlightSketch(bool response);
    void updateSketch();
    void updateOverSketch(std::vector<double> curve);
    void updateTrajectory();
    void updateTrajectoryOverSketch(std::vector<double> curve);
    void updatePreviewFlag(bool flag);
    void updateDirectionButtons();
    void enableDirectionButtons();
    void updateUsedCrossSections(int id, int direction);


private:

    double m_sketchedPoint_x;   /**< X coordinate of the sketched point (Model Coordinate System)*/
    double m_sketchedPoint_y;   /**< Y coordinate of the sketched point (Model Coordinate System)*/
    
    double m_sketchLine_Length;          /**< Length of the horizontal line.*/
    double m_sketchCircle_RadiusX;        /**< radius of the sketch circle.*/
    double m_sketchCircle_RadiusY;        /**< radius of the sketch circle.*/

    int m_surfaceID;              /**< keep track of the surface ID.*/

    bool is_overSketch;			/**< Flag that indicates if the sketch is in fact an oversketch.*/
    bool is_sketchClean;		/**< Flag that indicates if the current sketch was clean by the Clear button.*/

    double startMovePoint_x;    /**< Coordinate x of the initial point used in the transformations. */
    double startMovePoint_y;    /**< Coordinate y of the initial point used in the transformations. */
    double firstPoint_x;        /**< Coordinate x of the first point used in the transformations (select point).*/
    double firstPoint_y;        /**< Coordinate x of the first point used in the transformations (select point).*/
    bool isMoving = false;      /**< Flag used to indicate that the translation is occuring. */

    std::vector<std::shared_ptr<Surface>> m_surfaces;   /**< A collection of sketches used to create the surfaces, representing the 3D model.*/

    Sketch m_trajectory;            /**< Trajectory: A Sketch that represents the trajectory in Map View.*/
    bool is_Trajectory_overSketch;	/**< Trajectory: Flag that indicates if the sketch of the trajectory is in fact an oversketch.*/
    bool is_trajectoryClean;		/**< Trajectory: Flag that indicates if the current sketch of the trajectory was clean by the Clear button.*/
    bool is_TrajectorySelected;     /**< Trajectory: Flag that indicates if the current sketch of the trajectory is selected by the click of the mouse.*/
        
    std::vector<double> editSketchSurface(int crossSection, int direction, bool is_sketchClean); /**< Method responsible to keep track of the right set of sketches to be edited.*/

    void createNewSurface(int crossSection, int direction);  /**< Method responsible to create a new surface and set all the atributes necessary.*/

    //- Data Structure Mappings
    std::vector<double> fromCurve2DToVector(Curve2D curve);
    Curve2D fromVectorToCurve2D(std::vector<double> curve);

};

#endif  // SKETCHHANDLER_H

