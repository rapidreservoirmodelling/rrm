
/**
* @file sketch_map_view.cpp
* @author Sicilia Judice
* @brief File containing the class SketchMapView
*/

#include "sketch_map_view.h"

#include <QDebug>

SketchMapView::SketchMapView()
{
	this->reset();
}


SketchMapView::SketchMapView(const SketchMapView& newSketchMapView)
{
	this->curves = newSketchMapView.curves;
	this->crossSection_ID = newSketchMapView.crossSection_ID;
	this->surface_ID = newSketchMapView.surface_ID;

	this->currentCurves = newSketchMapView.currentCurves;
	this->current_crossSection_ID = newSketchMapView.current_crossSection_ID;
	this->current_surface_ID = newSketchMapView.current_surface_ID;

	this->index = newSketchMapView.index;
}


SketchMapView& SketchMapView::operator=(const SketchMapView& newSketchMapView)
{
	this->curves = newSketchMapView.curves;
	this->crossSection_ID = newSketchMapView.crossSection_ID;
	this->surface_ID = newSketchMapView.surface_ID;

	this->currentCurves = newSketchMapView.currentCurves;
	this->current_crossSection_ID = newSketchMapView.current_crossSection_ID;
	this->current_surface_ID = newSketchMapView.current_surface_ID;

	this->index = newSketchMapView.index;

	return *this;
}


SketchMapView::~SketchMapView()
{
	this->reset();
}


void SketchMapView::reset()
{
	//- Deallocate memory for each curve in the list:
	for (auto i : this->curves)
		i.clear();
	this->curves.clear();
	this->crossSection_ID.clear();
	this->surface_ID.clear();

	//- Deallocate memory for each curve in the current curves list:
	for (auto i : this->currentCurves)
		i.clear();
	this->currentCurves.clear();
	this->current_crossSection_ID.clear();
	this->current_surface_ID.clear();

	this->index = -1;
}

void SketchMapView::undoSurface(int surfaceID)
{
	if (this->index > -1)
	{
		while ((this->index >=0) && (this->surface_ID[this->index] == surfaceID))
		{
			this->index--;
		}
	}		

	if (Comments::SBIM_SKETCH_MAP_VIEW())
	{
		//qDebug() << "\n SketchMapView::undoSurface(): \t index: " << this->index;
	}
}

void SketchMapView::redoSurface(int surfaceID)
{
	if (this->surface_ID.size())
	{
		int cond = this->surface_ID.size() - 1;
		while ((this->index < cond)  && (this->surface_ID[index + 1] == surfaceID))
		{
			this->index++;
		}
	}

	if (Comments::SBIM_SKETCH_MAP_VIEW())
	{
		//qDebug() << "\n SketchMapView::redoSurface(): \t index: " << this->index;
	}
}

std::vector<std::vector<double>> SketchMapView::getCurveList()
{
	//- TODO
	return this->curves;
}

void SketchMapView::setCurveList(std::vector<std::vector<double>> newList)
{
	for (auto i : this->curves)
		i.clear();
	this->curves.clear();

	this->curves = newList;
	this->index = this->curves.size() - 1;

	if (Comments::SBIM_SKETCH_MAP_VIEW())
	{
		//qDebug() << "\nSketchMapView::setCurveList(): \tsize: " << this->curves.size() << ", index: " << index;
	}
}

std::vector<int> SketchMapView::getCrossSectionIDList()
{
	return this->crossSection_ID;
}

void SketchMapView::setCrossSectionIDList(std::vector<int> newList)
{
	this->crossSection_ID.clear();
	this->crossSection_ID = newList;

	if (Comments::SBIM_SKETCH_MAP_VIEW())
	{
		//qDebug() << "\n SketchMapView::setCrossSectionIDList(): " << this->crossSection_ID.size();
	}
}

std::vector<int> SketchMapView::getSurfaceIDList()
{
	return this->surface_ID;
}

void SketchMapView::setSurfaceIDList(std::vector<int> newList)
{
	this->surface_ID.clear();
	this->surface_ID = newList;

	if (Comments::SBIM_SKETCH_MAP_VIEW())
	{
		//qDebug() << "\n SketchMapView::setSurfaceIDList(): \t" << this->surface_ID.size();
	}
}

void SketchMapView::setDataList(std::vector<std::vector<double>> curves, 
	std::vector<int> csIDs, std::vector<int> surfaceIDs)
{	
	this->setCurveList(curves);
	this->setCrossSectionIDList(csIDs);
	this->setSurfaceIDList(surfaceIDs);
}


unsigned int SketchMapView::getNumberOfCurves()
{
	return this->index + 1;//return this->curves.size();
}


unsigned int SketchMapView::getNumberOfCurrentCurves()
{
	return this->currentCurves.size();
}


void SketchMapView::saveCurve(std::vector<double> curve, int csID, int surfaceID)
{
	this->curves.push_back(curve);
	this->crossSection_ID.push_back(csID);
	this->surface_ID.push_back(surfaceID);

	this->index = this->curves.size() - 1;

	if (Comments::SBIM_SKETCH_MAP_VIEW())
	{
		//qDebug() << "\n SketchMapView::saveCurve():";
		//qDebug() << "\t- Curves: " << curves.size();
		//qDebug() << "\t- csID: " << crossSection_ID;
		//qDebug() << "\t- surfaceID: " << surface_ID;
		//qDebug() << "\t- index: " << index;
	}
}


void SketchMapView::cleanData()
{
	if (Comments::SBIM_SKETCH_MAP_VIEW())
	{
		//qDebug() << "\n SketchMapView::cleanData(): ";
		//qDebug() << "\t- curves size: " << this->curves.size();
		//qDebug() << "\t- index: " << this->index;
	}
	
	if(this->curves.size())
	{
		int cond = this->curves.size() - 1;
		if (this->index < cond)
		{
			for (int i = this->curves.size() - 1; i > this->index; i--)
			{
				this->curves.pop_back();
				this->crossSection_ID.pop_back();
				this->surface_ID.pop_back();
			}
		}
	}
}


void SketchMapView::saveCurrentCurve(std::vector<double> curve, int csID, int surfaceID)
{
	this->currentCurves.push_back(curve);
	this->current_crossSection_ID.push_back(csID);
	this->current_surface_ID.push_back(surfaceID);
}


void SketchMapView::resetCurrentCurves()
{
	//- Deallocate memory for each curve in the currentCurves list:
	for (auto i : this->currentCurves)
		i.clear();
	this->currentCurves.clear();
	this->current_crossSection_ID.clear();
	this->current_surface_ID.clear();
}


std::vector<double> SketchMapView::loadCurve()
{
	std::vector<double> response;
	
	//- If the list is not empty copy the last curve saved
	if (this->index > -1)
		response = this->curves[this->index];

	/*
	if(this->curves.size())
		response = this->curves[this->curves.size() - 1];
	*/

	return response;
}


std::vector<double> SketchMapView::getCurves()
{
	std::vector<double> response;

	//for (int i = 0; i < this->curves.size(); i++)
	for (int i = 0; i <= this->index; i++)
	{
		response.reserve(response.size() + this->curves[i].size());
		response.insert(response.end(), this->curves[i].begin(), this->curves[i].end());
	}

	if (Comments::SBIM_SKETCH_MAP_VIEW())
	{
		//qDebug() << "\nSketchMapView::getCurves(): \t " << curves.size() << ", " << response.size();
	}

	return response;
}


std::vector<double> SketchMapView::getCurve(unsigned int i)
{
	return this->curves[i];
}


std::vector<double> SketchMapView::getCurrentCurves()
{
	std::vector<double> response;

	for (int i = 0; i < this->currentCurves.size(); i++)
	{
		response.reserve(response.size() + this->currentCurves[i].size());
		response.insert(response.end(), this->currentCurves[i].begin(), this->currentCurves[i].end());
	}

	return response;
}


std::vector<double> SketchMapView::getCurrentCurve(unsigned int i)
{
	return this->currentCurves[i];
}


std::vector<int> SketchMapView::getCurrentSurfaceIDList()
{
	return this->current_surface_ID;
}


bool SketchMapView::isDuplicated(std::vector<double> curve)
{
	for (size_t i = 0; i < this->curves.size(); i++)
	{
		if (curve == this->curves[i])
		{
			return true;
		}
	}

	return false;
}


