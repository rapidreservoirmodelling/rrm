/***************************************************************************

* This class handles storing and loading of SketchData.

* Copyright (C) 2021, Fazilatur Rahman.

* It converts the SketchData to JSON string and saves it to the disk.
* Plus, when the model gets opened from .rrm file it loads the SketchData
* from the corresponding .json file.

*****************************************************************************/

#ifndef SBIMFILEHANDLER_H
#define SBIMFILEHANDLER_H

#include "SketchData.hpp"

#include <QObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include "Constants.h"

#define MAIN_LABEL          "SBIM DATA"
#define CONTOUR_LABEL       "CONTOURS"
#define TRAJECTORY_LABEL    "TRAJECTORIES"
#define DATA_CURVES_LABEL       "CURVES"
#define DATA_CS_ID_LABEL        "CROSS_SECTION_IDs"
#define DATA_SURFACE_ID_LABEL   "SURFACE_IDs"

class SbimFileHandler : public QObject
{
    Q_OBJECT

public:
    
    /**
    * Constructor.
    */
    SbimFileHandler(QString fileName);

    /**
    * Method that saves the SBIM data to disk.
    * @return void
    */
    Q_INVOKABLE void save();
    
    /**
    * Method that laods the SBIM data from disk.
    * @return void
    */
    Q_INVOKABLE void load();

    /**
    * Method that initializes the internal data structure with SBIM contour data
    * @return void
    */
    void setContourData(SketchData contour);
    
    /**
    * Method that returns the SBIM contour data
    * @return SketchData
    */
    SketchData getContourData();

    /**
    * Method that initializes the internal data structure with SBIM trajecotry data
    * @return void
    */
    void setTrajectoryData(SketchData trajectory);
    
    /**
    * Method that returns the SBIM trajectory data
    * @return SketchData
    */
    SketchData getTrajectoryData();

    /**
    * Method that resets the internal data structure
    * @return void
    */
    void resetDataStructure();


private:
    //Initialization example method
    void InitSampleData(); //Test data
    
    //Helper functions for creating JsonObject
    QJsonArray getCurvesListObject(std::vector<std::vector<double>> curves_list);
    QJsonArray getSurfaceListObject(std::vector<int> surface_list);
    QJsonArray getCrossSectionListObject(std::vector<int> cross_section_list);
    QJsonObject getContoursObject();
    QJsonObject getTrajectoriesObject();

    //Helper functions for parsing Json string
    std::vector<std::vector<double>> parseCurveList(QJsonArray curvesArray);
    std::vector<int> parseIdList(QJsonArray idArray);
    void parseContourData(QJsonObject contoursInfo);
    void parseTrajectoryData(QJsonObject trajectoriesInfo);

    //Method that creates json string from SketchData
    void createJson();

    //Method that converts json string to SketchData
    void parseJson();

    //Internal data structure
    SketchData m_contour_data;
    SketchData m_trajectory_data;

    QString m_json_string;

    QString m_file_name;

};


#endif // SBIMFILEHANDLER_H
