/**
* @file sketch_map_view.h
* @author Sicilia Judice
* @brief File containing the class SketchMapView
*/

#ifndef SKETCHMAPVIEW_H
#define SKETCHMAPVIEW_H

#include <vector>
#include "Constants.h"

class SketchMapView {

public:

	/**
	* Constructor.
	*/
	SketchMapView();


	/**
	* Copy constructor.
	* @param newSketchMapView a const reference to another sketch map view.
	*/
	SketchMapView(const SketchMapView& newSketchMapView);


	/**
	* Assignment operator
	* @param newSketchMapView a const reference to another sketch map view.
	*/
	SketchMapView& operator=(const SketchMapView& newSketchMapView);


	/**
	* Destructor.
	*/
	~SketchMapView();


	/**
	* Method to reset the sketch map view to its initial state.
	* @return void
	*/
	void reset();


	/**
	* Methods to update the index after UNDO or REDO a surface.
	* @param surfaceID is the ID of the surface that was changed.
	* @return void
	*/
	void undoSurface(int surfaceID);
	void redoSurface(int surfaceID);

	
	/**
	* Method to return the list of curves.
	* @return std::vector<std::vector<double>>
	*/
	std::vector<std::vector<double>> getCurveList();

	/**
	* Method to set the list of curves (from a load file).
	* @return void
	*/
	void setCurveList(std::vector<std::vector<double>> newList);


	/**
	* Method that return the list of cross section ID.
	* @return std::vector<int>
	*/
	std::vector<int> getCrossSectionIDList();

	/**
	* Method to set the list of cross section ID (from a load file).
	* @return void
	*/
	void setCrossSectionIDList(std::vector<int> newList);


	/**
	* Method that return the list of surface ID.
	* @return std::vector<int>
	*/
	std::vector<int> getSurfaceIDList();

	/**
	* Method to set the list of surface ID (from a load file).
	* @return void
	*/
	void setSurfaceIDList(std::vector<int> newList);


	/**
	* Method to set all the lists (from a load file).
	* @return void
	*/
	void setDataList(std::vector<std::vector<double>> curves, std::vector<int> csIDs, std::vector<int> surfaceIDs);


	/**
	* Method to return the size of the list of curves.
	* @return unsigned int
	*/
	unsigned int getNumberOfCurves();  //unsigned int getNumberOfSketchContours();


	/**
	* Method to return the size of the list of current curves.
	* @return unsigned int
	*/
	unsigned int getNumberOfCurrentCurves(); //unsigned int getNumberOfCurrentContours();


	/**
	* Method to save a new sketch map view on the list.
	* @param curve is the geometry of a sketch to be saved.
	* @param csID is the cross section id of the sketch.
	* @param surfaceID is the ID of the surface created byt the sketch.
	* @return void
	*/
	void saveCurve(std::vector<double> curve, int csID, int surfaceID); //void saveSketchContour(std::vector<double> curve, int csID, int surfaceID);


	/**
	* Method to clean the lists according to the index position before saving a new curve.
	*/
	void cleanData();


	/**
	* Method to save a current sketch on the list.
	* @param curve is the geometry of the current sketch to be saved.
	* @param csID is the cross section id of the sketch.
	* @param surfaceID is the ID of the surface created by the sketch.
	* @return void
	*/
	void saveCurrentCurve(std::vector<double> curve, int csID, int surfaceID); //void saveCurrentContour(std::vector<double> curve, int csID, int surfaceID);


	/**
	* Method to reset the current sketch list.
	* Every time a new sketch is made, the list needs to be empty and filled up again.
	* @return void
	*/
	void resetCurrentCurves(); //void resetCurrentContours();

	
	/**
	* Method to load the last saved curve on the list.
	* @return std::vector<double>
	*/
	std::vector<double> loadCurve(); //std::vector<double> loadSketchContour();


	/**
	* Method to get all the curves combined in one single vector.
	* @return std::vector<double>
	*/
	std::vector<double> getCurves(); //std::vector<double> getContours();
	

	/**
	* Method to get the curve at position i.
	* @param i is the position in the list.
	* @return std::vector<double>
	*/
	std::vector<double> getCurve(unsigned int i); //std::vector<double> getContour(unsigned int i);


	/**
	* Method to get all the current curves combined in one single vector.
	* @return std::vector<double>
	*/
	std::vector<double> getCurrentCurves(); //std::vector<double> getCurrentContours();


	/**
	* Method to get the current curve at position i.
	* @param i is the position in the currentCurves list.
	* @return std::vector<double>
	*/
	std::vector<double> getCurrentCurve(unsigned int i); //std::vector<double> getCurrentContour(unsigned int i);


	/**
	* Method that return the list of current surface ID.
	* @return std::vector<int>
	*/
	std::vector<int> getCurrentSurfaceIDList();



private:
	std::vector<std::vector<double>> curves;	/**< Collection of submitted sketches. */
	std::vector<int> crossSection_ID;			/**< Store the cross section ID for each curve in the list. */
	std::vector<int> surface_ID;				/**< Store the surface ID for each curve in the list. */

	std::vector<std::vector<double>> currentCurves;		/**< Collection of current curves (before being submitted). */
	std::vector<int> current_crossSection_ID;			/**< Store the cross section ID for each current curve. */
	std::vector<int> current_surface_ID;				/**< Store the surface ID for each current curve. */

	int index;	/**< To keep track of UNDO and REDO surfaces.*/

	bool isDuplicated(std::vector<double> curve);
};

#endif //SKETCHMAPVIEW_H