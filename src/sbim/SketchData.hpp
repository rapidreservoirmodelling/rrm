/***************************************************************************

* It's the structure of SketchData that supports the SbimFileHandler class.

* Copyright (C) 2021, Fazilatur Rahman.

* It provides the data definition for SbimFileHandler

*****************************************************************************/

#ifndef SKETCHDATA_H
#define SKETCHDATA_H

#include <vector>

typedef struct SketchData
{
    std::vector<std::vector<double>> curves;
    std::vector<int> cross_section_id;
    std::vector<int> surface_id;
};

#endif // SKETCHDATA_H
