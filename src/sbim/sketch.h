
/**
* @file sketch.h
* @author Sicilia Judice
* @brief File containing the class Sketch
*/


#ifndef SKETCH_H
#define SKETCH_H

#include <memory>
#include <vector>

#include "libs/polygonalcurve/CurveN.hpp"
#include "sketch_stack.h"

#include "Constants.h"

/**
* A Sketch is a collection of points. 
* It is an editable curve that has history (can be undone).
*/

class Sketch {

public:

	/**
	* Constructor.
	*/
	Sketch();


	/**
	* Copy constructor.
	* @param newSketch a const reference to another sketch.
	*/
	Sketch(const Sketch& newSketch);


	/**
	* Assignment operator
	* @param newSketch a const reference to another sketch.
	*/
	Sketch& operator=(const Sketch& newSketch);


	/**
	* Destructor.
	*/
	~Sketch();


    /**
    * Method to clear the sketch, i.e., clear the curves that represents the current sketch and oversketch.
    * Need to clear the stack that contains the history.
    * @return void
    */
    void clear();


    /**
    * Method to clear the current sketch.
    * This action will be added into the history, so the user can undo if necessary.
    * @return void
    */
    void clearSketch();
    
    
    /**
    * Method to clear the over sketch curve.
    * @return void
    */
    void clearOverSketch();


    /**
    * Method to add a coordinate into the curve that represents the current sketch.
    * @return void
    */
    void addCoordinate(double x, double y);


    /**
    * Method to add a coordinate into the curve that represents the over sketch curve.
    * @return void
    */
    void addCoordinate_OverSketch(double x, double y);


    /**
    * Method that returns the size of the current curve.
    * @return unsigned int
    */
    unsigned int getSketchSize();


    /**
    * Method that returns the curve that represents the current state of the sketch.
    * It returns the last curve added to the history.
    * @return Curve2D
    */
    Curve2D getSketch();


    /**
    * Method that returns the curve that represents the over sketch process.
    * @return Curve2D
    */
    Curve2D getOverSketch();

    
    /**
    * Method that add an empty instance of a curve to the stack. 
    * @return void
    */
    void addSketch();


    /**
    * Method that add a curve to the stack.
    * @return void
    */
    void addSketch(Curve2D curve);


    /**
    * Method that returns the distance between the sketch and a point.
    * @param point_x is the x coordinate of the point.
    * @param point_y is the y coordinate of the point.
    * @return double
    */
    double distanceTo(double point_x, double point_y);


    /**
    * Method that verify if the first and last point of the curve is close.
    * If so, the curve will be closed and the method will return TRUE.
    * @return bool 
    */
    bool closeCurve();


    /**
    * Method to smooth the sketch.
    * @return void
    */
    void smooth();

    
    /**
    * Method to apply the over sketch process: uses the over sketch curve to correct the current sketch.
    * If the correction is applied it returns true.
    * @return bool
    */
    bool overSketch();


    /**
    * Method that returns TRUE if the sketch has history, FALSE otherwise.
    * @return bool
    */
    bool hasHistory();


    /**
    * Method that clear the stack until the current position (indicated by its flag last).
    * Then it pushes the current state in the stack.
    * @return void
    */
    void copyPreviousHistory();


    /**
    * Method that add the current sketch into the history.
    * @return void
    */
    void addHistory();
    

    /**
    * Method that removes the last sketch added into the history (pops a state from the stack).
    * @return void
    */
    void removeHistory();


    /**
    * Method that verifies if the current state is different from the previous one.
    * Important for the translation process: if the user selects the translation button, click on the scene but release
    * the mouse without translating the sketch, it means it was not modified. So there is no need to add this into the history.
    * @return bool
    */
    bool isModified();


    /**
    * Method that invokes the undo functionality from the history.
    * It moves the flag of the history one position back, if possible, and use this as the current sketch.
    * @return bool
    */
    bool undo();


    /**
    * Method that invokes the redo functionality from the history.
    * It moves the flag of the history one position forward, if possible, and use this as the current sketch.
    * @return bool
    */
    bool redo();


    /**
    * Method that translates the current sketch to a new position.
    * @param newPos is the new position.
    * @return void
    */
    void translate(Point2D newPos);


    /**
    * Method that rotates the current sketch by an angle.
    * The sketch will be rotated around its middle point.
    * @param angle of rotation.
    * @return void
    */
    void rotate(double angle);
    void rotate(double angle, double center_x, double center_y);

    
    /**
    * Method that scales the current sketch.
    * The sketch will be scaled around its middle point.
    * @param scale factor.
    * @return void
    */
    void scale(Point2D scale);
    void scale(double mouse_x, double mouse_y, int i);


    /**
    * Method that verifies is the sketch is monotonic (x[i] < x[i+1]). 
    * It returns TRUE if it is monotonic. FALSE otherwise.
    * @return bool
    */
    bool isValid();


    /**
    * Method that turn a sketch into a monotonic curve.
    * Any point which its x coordinate is bigger than the previous point will be discarded.
    * @return void
    */
    void turnIntoMonotonic();


    /**
    * Method that verifies is the sketch is monotonic in Y (y[i] < y[i+1]).
    * It returns TRUE if it is monotonic. FALSE otherwise.
    * @return bool
    */
    bool isValid_Y();


    /**
    * Method that turn a sketch into a monotonic curve in Y axis.
    * Any point which its y coordinate is bigger than the previous point will be discarded.
    * @return void
    */
    void turnIntoMonotonic_Y();

    
    /**
    * Method to print the state of the sketch.
    */
    void printInfo();


private:

    Curve2D m_curve;					/**< A collection of bidimensional points that represents the current sketch. */

  	Curve2D m_overSketchCurve;			/**< A collection of bidimensional points to be used in the over sketching process as a correction input. */

	SketchStack m_history;				/**< A stack to store the history of the sketch. */

};


#endif //SKETCH_H