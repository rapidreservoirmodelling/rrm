
/**
* @file surface.cpp
* @author Sicilia Judice
* @brief File containing the class Surface
* @date   2020-03-12
*/

#include <QDebug>

#include "surface.h"


Surface::Surface()
{
	this->clear();
}


Surface::Surface(const Surface& newSurface) : Surface()
{
	this->m_id = newSurface.m_id;
	this->is_editable = newSurface.is_editable;
	this->index = newSurface.index;
	this->m_crossSections = newSurface.m_crossSections;
}


Surface& Surface::operator=(const Surface& newSurface)
{
	this->m_id = newSurface.m_id;
	this->is_editable = newSurface.is_editable;
	this->index = newSurface.index;
	this->m_crossSections = newSurface.m_crossSections;

	return *this;
}


Surface::~Surface()
{
	this->m_crossSections.clear();
}


void Surface::clear()
{
	//- Deallocate memory for each cross section in the collection:
	for (auto i : this->m_crossSections)
		i->clear();
	this->m_crossSections.clear();

	this->m_id = -1;
	this->is_editable = true;
	this->is_visible = true;
	this->index = 0;	
}


void Surface::clearCurrentSketch(size_t crossSection, size_t direction)
{
	if (this->updateIndex(crossSection, direction))
	{
		//- Verify if the current sketch is selected
		if (this->m_crossSections[this->index]->isSketchSelected())
		{
			this->m_crossSections[this->index]->clearCurrentSketch();
		}
	}
}


void Surface::setEditionMode(bool status)
{
	this->is_editable = status;
}


bool Surface::isEditable()
{
	return this->is_editable;
}


bool Surface::isVisible()
{
	return this->is_visible;
}


void Surface::setVisibility(bool flag)
{
	this->is_visible = flag;
}


int Surface::getId()
{
	return this->m_id;
}


void Surface::setId(int newId)
{
	this->m_id = newId;
}


unsigned int Surface::getNumberOfCrossSections()
{
	return this->m_crossSections.size();
}


int Surface::getCrossSectionID(unsigned int index)
{
	//- Verify if index is valid
	if (index < this->m_crossSections.size())
	{
		return this->m_crossSections[index]->getID();
	}

	return -1;
}


int Surface::getPreviousSketchCrossSection(int crossSection)
{
	int response = -1;
	if (this->m_crossSections.size())
	{		
		for (int i = 0; i < this->m_crossSections.size(); i++)
		{
			if (Comments::SBIM_SURFACE())
			{
				//qDebug() << "\n Surface::getPreviousSketchCrossSection()";
				//qDebug() << "\t Previous: " << this->m_crossSections[i]->getID() << " < " << crossSection
				//	<< " & " << this->m_crossSections[i]->getID() << " >= " << response;
			}
			
			if (this->m_crossSections[i]->getID() < crossSection &&
				static_cast<int>(this->m_crossSections[i]->getID()) > response)
			{
				response = this->m_crossSections[i]->getID();
			}
		}
	}
	
	if( response >= 0 )
		return response;

	return -1;
}

int Surface::getNextSketchCrossSection(int crossSection)
{
	int response = 1000000;
	if (this->m_crossSections.size())
	{		
		for (int i = 0; i < this->m_crossSections.size(); i++)
		{
			if (this->m_crossSections[i]->getID() > crossSection &&
				this->m_crossSections[i]->getID() < response)
			{
				response = this->m_crossSections[i]->getID();
			}
		}
	}

	if (response == 1000000)
		return -1;
	else
		return response;
}


unsigned int Surface::getNumberOfSketches()
{
	unsigned int response = 0;

	//- For all cross sections inside the collection
	for (unsigned int i = 0; i < this->m_crossSections.size(); i++)
	{
		response += this->m_crossSections[i]->getNumberOfSketches();
	}

	return response;
}


unsigned int Surface::getNumberOfSketches(int index)
{
	return this->m_crossSections[index]->getNumberOfSketches();
}


unsigned int Surface::getNumberOfCurrentSketches(int crossSection, int direction)
{
	unsigned int response = 0;

	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		response = this->m_crossSections[this->index]->getNumberOfSketches();
	}

	return response;
}


unsigned int Surface::getNumberOfSketches(int& crossSection, int& direction)
{
	unsigned int response = 0;

	//- For all cross sections inside the collection
	for (unsigned int i = 0; i < this->m_crossSections.size(); i++)
	{
		response += this->m_crossSections[i]->getNumberOfSketches(crossSection, direction);
	}

	return response;
}


bool Surface::hasCrossSection(size_t crossSection, size_t direction)
{
	return this->updateIndex(crossSection, direction);
}


bool Surface::hasSketch()
{
	for (unsigned int i = 0; i < this->m_crossSections.size(); i++)
	{
		if(this->m_crossSections[i]->hasSketch())
			return true;
	}

	return false;
}


void Surface::addNewCrossSection(size_t crossSection, size_t direction)
{
	std::shared_ptr<CrossSection> newCrossSection = std::make_shared<CrossSection>();

	newCrossSection->setId(crossSection);
	newCrossSection->setDirection(direction);
	newCrossSection->addNewSketch();

	this->m_crossSections.push_back(newCrossSection);
	this->index = m_crossSections.size() - 1;
}


void Surface::addNewCrossSection(size_t crossSection, size_t direction, Curve2D curve)
{
	std::shared_ptr<CrossSection> newCrossSection = std::make_shared<CrossSection>();

	newCrossSection->setId(crossSection);
	newCrossSection->setDirection(direction);
	newCrossSection->addNewSketch(curve);

	this->m_crossSections.push_back(newCrossSection);
	this->index = m_crossSections.size() - 1;
}


//- DELETE
/*
void Surface::addNewSketch(size_t crossSection, size_t direction)
{
	std::shared_ptr<CrossSection> newCrossSection = std::make_shared<CrossSection>();

	newCrossSection->setId(crossSection);
	newCrossSection->setDirection(direction);
	newCrossSection->addNewSketch();

	this->m_crossSections.push_back(newCrossSection);
	this->index = m_crossSections.size() - 1;
}
*/

//- DELETE
/*
void Surface::addNewSketch(size_t crossSection, size_t direction, Curve2D curve)
{
	std::shared_ptr<CrossSection> newCrossSection = std::make_shared<CrossSection>();
 
	newCrossSection->setId(crossSection);
	newCrossSection->setDirection(direction);
	newCrossSection->addNewSketch(curve);

	this->m_crossSections.push_back(newCrossSection);
	this->index = m_crossSections.size() - 1;
}
*/


void Surface::addSketch(size_t crossSection, size_t direction, Curve2D curve)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		this->m_crossSections[this->index]->addSketch(curve);
	}
}


void Surface::addSketch(size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		this->m_crossSections[this->index]->resetSelect();
		this->m_crossSections[this->index]->addNewSketch();
	}
}


void Surface::addSketchLine(size_t crossSection, size_t direction, double py, double length)
{
	if (Comments::SBIM_SURFACE())
	{
		//qDebug() << "\n Surface::addSketchLine(): \t line: ( 0.0, " << py << ") to (" << length << ", " << py << ")";
	}
	
	this->addLine(0.0, py, length, py, crossSection, direction);
}


void Surface::addSketchCircle(size_t crossSection, size_t direction, double px, double py, double radiusX, double radiusY)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		this->m_crossSections[this->index]->addSketchCircle(px, py, radiusX, radiusY);
	}
}


bool Surface::copyPreviousSketch(size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		return this->m_crossSections[this->index]->copyPreviousSketch();
	}

	return false;
}


void Surface::addPoint(double px, double py, size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		this->m_crossSections[this->index]->addSketchPoint(px, py);
	}
}


void Surface::addLine(double ax, double ay, double bx, double by, size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		this->m_crossSections[this->index]->addSketchLine(ax, ay, bx, by);
	}
}


bool Surface::isOverSketch(int crossSection, int direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		return this->m_crossSections[this->index]->isOverSketch();
	}
	
	return false;
}


bool Surface::applyOverSketch(size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		return this->m_crossSections[this->index]->applyOverSketch();
	}

	return false;
}


double Surface::distanceFromSketchToMousePointer(size_t crossSection, size_t direction, double mouse_x, double mouse_y)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		return this->m_crossSections[this->index]->distanceFromSketchToMousePointer(mouse_x, mouse_y);
	}
	
	return -1.00;
}


int Surface::getHighlightIndex(int crossSection, int direction)
{
	unsigned int response = -1;

	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		response = this->m_crossSections[this->index]->getHighlightIndex();
	}

	return response;
}


int Surface::getSelectIndex(int crossSection, int direction)
{
	unsigned int response = -1;

	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		response = this->m_crossSections[this->index]->getSelectIndex();
	}

	return response;
}


bool Surface::isSketchHighlighted(size_t crossSection, size_t direction, double mouse_x, double mouse_y)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{		
		this->resetHighlight();

		return this->m_crossSections[this->index]->highlightSketch(mouse_x, mouse_y);
	}

	return false;
}


bool Surface::isSketchHighlighted(size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		return this->m_crossSections[this->index]->isSketchHighlighted();
	}
	return false;
}


bool Surface::isSketchSelected(size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		return this->m_crossSections[this->index]->selectHighlightedSketch();
	}

	return false;
}


bool Surface::isSketchSelected(size_t crossSection, size_t direction, double mouse_x, double mouse_y)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		this->resetHighlight();

		return this->m_crossSections[this->index]->isSketchSelected(mouse_x, mouse_y);
	}

	return false;
}


void Surface::resetHighlight()
{
	for (unsigned int i = 0; i < this->m_crossSections.size(); i++)
	{
		this->m_crossSections[i]->resetHighlight();
	}
}


void Surface::resetSelected()
{
	for (unsigned int i = 0; i < this->m_crossSections.size(); i++)
	{
		this->m_crossSections[i]->resetSelect();
	}
}


void Surface::applySmooth(size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		//- Verify if the current sketch is selected
		if (this->m_crossSections[this->index]->isSketchSelected())
		{
			this->m_crossSections[this->index]->applySmooth();
		}
	}
}


void Surface::applyRotation(double angle, size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		this->m_crossSections[this->index]->applyRotation(angle);
	}
}


void Surface::applyRotation(double angle, size_t crossSection, size_t direction, double center_x, double center_y)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		this->m_crossSections[this->index]->applyRotation(angle, center_x, center_y);
	}
}


void Surface::applyScale(double ex, double ey, size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		this->m_crossSections[this->index]->applyScale(ex, ey);
	}
}


void Surface::applyScale(size_t crossSection, size_t direction, double mouse_x, double mouse_y)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		this->m_crossSections[this->index]->applyScaleMouse(mouse_x, mouse_y);
	}
}


void Surface::applyTranslation(double dx, double dy, size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		this->m_crossSections[this->index]->applyTranslation(dx, dy);
	}
}


bool Surface::isSketchModified(size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		this->m_crossSections[this->index]->isSketchModified();
	}
	return false;
}


bool Surface::updateSKetch(size_t crossSection, size_t direction)
{
	bool response = false;

	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		response = this->m_crossSections[this->index]->updateSketch();

		//- If the cross section is empty of sketch, we can erase it from the collection.
		//- This is to assure when the user click on the canvas and the system allocate memory for the new sketch
		if (!response)
		{
			this->m_crossSections.erase(this->m_crossSections.begin() + this->index);
		}
	}
	
	return response;
}


Curve2D Surface::getSketch(unsigned int csIndex, unsigned int sketchIndex)
{
	Curve2D response;

	if (csIndex < this->m_crossSections.size())
	{
		response = this->m_crossSections[csIndex]->getSketch(sketchIndex);
	}

	return response;
}


Curve2D Surface::getCurrentSketch(size_t crossSection, size_t direction)
{
	Curve2D response;

	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		response = this->m_crossSections[this->index]->getSketch();
	}

	return response;
}


Curve2D Surface::getCurrentSketch(size_t crossSection, size_t direction, unsigned int i)
{
	Curve2D response;

	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		response = this->m_crossSections[this->index]->getSketch(i);
	}

	return response;
}


Curve2D Surface::getHighlightedSketch(size_t crossSection, size_t direction)
{
	Curve2D response;

	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		response = this->m_crossSections[this->index]->getHighlightedSketch();
	}

	return response;
}


Curve2D Surface::getSelectedSketch(size_t crossSection, size_t direction)
{
	Curve2D response;

	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		response = this->m_crossSections[this->index]->getSelectedSketch();
	}

	return response;
}


Curve2D Surface::getCurrentOverSketch(size_t crossSection, size_t direction)
{
	Curve2D response;

	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		response = this->m_crossSections[this->index]->getOverSketch();
	}

	return response;
}


Curve2D Surface::getExtrudedSurfacePoints(size_t crossSection, size_t direction)
{
	Curve2D points;

	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		points = this->m_crossSections[this->index]->getExtrudedSurfacePoints();
	}

	return points;
}


std::vector<double> Surface::getSurfacePoints_Length(double box_size, int discretization)
{
	std::vector<double> points;
	points.clear();

	Curve2D sketch;

	//- For all cross sections inside the collection:
	for (size_t j = 0; j < this->m_crossSections.size(); j++)
	{
		sketch = this->m_crossSections[j]->getSketch();

		//--------------------------------------
		//- Add z_value: in this case is the cross section ID and it is placed as second coordinate because of VTK
		//- OBS: the z_value is the cross section ID mapped to the Box size.
		//--------------------------------------
		double z_value;
		z_value = static_cast<double>(((discretization - this->m_crossSections[j]->getID()) * box_size) / discretization);

		for (size_t i = 0; i < sketch.size(); i++)
		{
			points.push_back(sketch[i].x());
			points.push_back(z_value);
			points.push_back(sketch[i].y());
		}
		sketch.clear();
	}

	return points;
}


std::vector<double> Surface::getSurfacePoints_Width(double box_size, int discretization)
{
	std::vector<double> points;
	points.clear();

	Curve2D sketch;

	//- For all cross sections inside the collection:
	for (size_t j = 0; j < this->m_crossSections.size(); j++)
	{
		sketch = this->m_crossSections[j]->getSketch();

		//--------------------------------------
		//- Add z_value: in this case is the cross section ID and it is placed as second coordinate because of VTK
		//- OBS: the z_value is the cross section ID mapped to the Box size.
		//--------------------------------------
		double z_value;
		z_value = static_cast<double>(((this->m_crossSections[j]->getID()) * box_size) / discretization);

		for (size_t i = 0; i < sketch.size(); i++)
		{
			points.push_back(z_value); 
			points.push_back(sketch[i].x());			
			points.push_back(sketch[i].y());
		}
		sketch.clear();
	}

	return points;
}


std::vector<double> Surface::getSurfacePoints_Height(double box_size, int discretization)
{
	std::vector<double> points;
	points.clear();

	Curve2D sketch;

	//- For all cross sections inside the collection:
	for (unsigned int k = 0; k < this->m_crossSections.size(); k++)
	{
		//- For all sketches inside each cross section:
		for (unsigned int j = 0; j < this->m_crossSections[k]->getNumberOfSketches(); j++)
		{
			sketch = this->m_crossSections[k]->getSketch(j);

			//--------------------------------------
			//- Add h_value: in this case is the cross section ID
			//- OBS: the h_value is the cross section ID mapped to the Box size.
			//--------------------------------------
			double h_value;
			h_value = (this->m_crossSections[k]->getID() * box_size) / discretization;

			for (unsigned int i = 0; i < sketch.size(); i++)
			{
				points.push_back(sketch[i].x());
				points.push_back(sketch[i].y());
				points.push_back(h_value);
			}
			sketch.clear();
		}
	}
	
	return points;
}


bool Surface::undoSketch(size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		//- Verify if the current sketch is selected
		if (this->m_crossSections[this->index]->isSketchSelected())
		{
			return this->m_crossSections[this->index]->undoSketch();
		}
	}

	return false;
}


bool Surface::redoSketch(size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		//- Verify if the current sketch is selected
		if (this->m_crossSections[this->index]->isSketchSelected())
		{
			return this->m_crossSections[this->index]->redoSketch();
		}
	}

	return false;
}


void Surface::validateSketch(size_t crossSection, size_t direction)
{
	//- Find the index that contains the cross section id:
	if (this->updateIndex(crossSection, direction))
	{
		this->m_crossSections[this->index]->validateSketch();
	}
}


void Surface::printInfo()
{
}

//--- PRIVATE METHODS ---

bool Surface::updateIndex(size_t crossSection, size_t direction)
{
	for (size_t i = 0; i < this->m_crossSections.size(); i++)
	{
		if ((this->m_crossSections[i]->getID() == crossSection) &&
			(this->m_crossSections[i]->getDirection() == direction))
		{
			this->index = i;
			return true;
		}
	}

	return false;
}