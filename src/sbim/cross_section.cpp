
/**
* @file cross_section.cpp
* @author Sicilia Judice
* @brief File containing the class CrossSection
* @date   2021-05-12
*/

#define _USE_MATH_DEFINES
#include <cmath>
#include <QDebug>

#include "cross_section.h"


CrossSection::CrossSection()
{
	this->clear();
}


CrossSection::CrossSection(const CrossSection& newCrossSection) : CrossSection()
{
	this->m_id = newCrossSection.m_id;
	this->m_direction = newCrossSection.m_direction;
	this->m_sketches = newCrossSection.m_sketches;
	this->is_overSketch = newCrossSection.is_overSketch;
	this->is_sketchClean = newCrossSection.is_sketchClean;
	this->m_highlight = newCrossSection.m_highlight;
	this->m_select = newCrossSection.m_select;
}


CrossSection& CrossSection::operator=(const CrossSection& newCrossSection)
{
	this->m_id = newCrossSection.m_id;
	this->m_direction = newCrossSection.m_direction;
	this->m_sketches = newCrossSection.m_sketches;
	this->is_overSketch = newCrossSection.is_overSketch;
	this->is_sketchClean = newCrossSection.is_sketchClean;
	this->m_highlight = newCrossSection.m_highlight;
	this->m_select = newCrossSection.m_select;

	return *this;
}


CrossSection::~CrossSection()
{
	this->clear();
}


void CrossSection::clear()
{
	this->m_id = 0;
	this->m_direction = 0;

	//- Deallocate memory for each sketch in the collection:
	for (auto i : this->m_sketches)
		i->clear();
	this->m_sketches.clear();

	this->is_overSketch = false;
	this->is_sketchClean = false;

	this->m_highlight = -1;
	this->m_select = -1;
}


void CrossSection::setId(unsigned int id)
{
	this->m_id = id;
}


unsigned int CrossSection::getID()
{
	return this->m_id;
}


void CrossSection::setDirection(unsigned int direction)
{
	this->m_direction = direction;
}


unsigned int CrossSection::getDirection()
{
	return this->m_direction;
}


int CrossSection::getHighlightIndex()
{
	return this->m_highlight;
}


int CrossSection::getSelectIndex()
{
	return this->m_select;
}


bool CrossSection::getOverSketchFlag()
{
	return this->is_overSketch;
}


bool CrossSection::isOverSketch()
{
	//- Verify if the sketch isn't cleared
	if (this->getSketch().size())
	{
		//- Verify if there is a highlitghted sketch:
		if (this->m_highlight > -1)
			this->is_overSketch = true;
		else {
			//- Verify if there is at least one sketch in LENGTH or WIDTH direction
			if (this->m_direction == Direction::Value::LENGTH || this->m_direction == Direction::Value::WIDTH)				
				this->is_overSketch = true;
			else
				this->is_overSketch = false;
		}
	}
	else {
		this->is_overSketch = false;
	}
	
	return this->is_overSketch;
}


bool CrossSection::isSketchClean()
{
	return this->is_sketchClean;
}


void CrossSection::addNewSketch()
{
	std::shared_ptr<Sketch> newSketch = std::make_shared<Sketch>();

	newSketch->addSketch();

	this->m_sketches.push_back(newSketch);
}


void CrossSection::addNewSketch(Curve2D curve)
{
	std::shared_ptr<Sketch> newSketch = std::make_shared<Sketch>();
	
	newSketch->addSketch(curve);

	this->m_sketches.push_back(newSketch);
}


void CrossSection::addSketch(Curve2D curve)
{
	//- Verify if the direction is WE or NS
	if (this->m_direction == Direction::Value::LENGTH || this->m_direction == Direction::Value::WIDTH)
	{
		//- Verify if there is at least one sketch
		if (this->m_sketches.size())
		{
			unsigned int i = this->m_sketches.size() - 1;
			m_sketches[i]->addSketch(curve);
		}
	}
	else {
		this->addNewSketch(curve);
	}
}


bool CrossSection::copyPreviousSketch()
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_highlight > -1)
			i = this->m_highlight;

		//- If there is history, then copy the previous one on top of the stack:
		if (this->m_sketches[i]->hasHistory())
		{
			this->m_sketches[i]->copyPreviousHistory();
			return true;
		}
	}

	return false;
}


Curve2D CrossSection::getSketch(unsigned int i)
{
	Curve2D response;

	//- Verify if the index i is valid
	if (i < this->m_sketches.size())
	{
		response = this->m_sketches[i]->getSketch();
	}

	return response;
}


Curve2D CrossSection::getSketch()
{
	Curve2D response;

	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_highlight > -1)
			i = this->m_highlight;

		response = this->m_sketches[i]->getSketch();
	}

	return response;
}


Curve2D CrossSection::getHighlightedSketch()
{
	Curve2D response;

	//- Verify if there is at least one sketch
	if (this->m_sketches.size() && this->m_highlight>-1)
	{
		unsigned int i = this->m_highlight;
		response = this->m_sketches[i]->getSketch();
	}

	return response;
}


Curve2D CrossSection::getSelectedSketch()
{
	Curve2D response;

	//- Verify if there is at least one sketch
	if (this->m_sketches.size() && this->m_select > -1)
	{
		unsigned int i = this->m_select;
		response = this->m_sketches[i]->getSketch();
	}

	return response;
}


Curve2D CrossSection::getOverSketch()
{
	Curve2D response;

	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_highlight > -1)
			i = this->m_highlight;

		response = this->m_sketches[i]->getOverSketch();
	}

	return response;
}


void CrossSection::addSketchPoint(double px, double py)
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;		
		
		if (this->is_overSketch)
		{
			//- Verify if the direction is MapView
			if (this->m_direction == Direction::Value::HEIGHT && this->m_highlight > -1)
				i = this->m_highlight;
			
			this->m_sketches[i]->addCoordinate_OverSketch(px, py);
		}
		else {
			this->m_sketches[i]->addCoordinate(px, py);
			this->m_sketches[i]->addHistory();
		}
	}
}


void CrossSection::addSketchLine(double ax, double ay, double bx, double by)
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;

		for (double px = ax; px <= bx; px+=5)
		{
			this->m_sketches[i]->addCoordinate(px, ay);
		}
		//this->m_sketches[i]->addCoordinate(ax, ay);
		//this->m_sketches[i]->addCoordinate(bx, by);
		this->m_sketches[i]->addHistory();
	}
}


void CrossSection::addSketchCircle(double cx, double cy, double radiusX, double radiusY)
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;

		for (double angle = 0.0; angle <= 360; angle += 1)
		{
			double radian = (angle * M_PI) / 180.0;
			double px, py;
			px = cx + radiusX * cos(radian);
			py = cy + radiusY * sin(radian);
			this->m_sketches[i]->addCoordinate(px, py);
		}
		this->m_sketches[i]->addHistory();
	}
}


bool CrossSection::applyOverSketch()
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_highlight > -1)
			i = this->m_highlight;
		
		if (this->m_sketches[i]->overSketch())
		{
			this->validateSketch();
			return true;
		}
		else {
			this->m_sketches[i]->clearOverSketch();
		}
	}

	return false;
}


void CrossSection::validateSketch()
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;

		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_highlight > -1)
			i = this->m_highlight;

		if (this->m_sketches[i]->getSketchSize() > 2)
		{
			//- Verify if sketch was made on LENGTH or WIDTH direction:
			if (this->m_direction == Direction::Value::LENGTH || this->m_direction == Direction::Value::WIDTH)
			{
				//- If sketch is not valid, turn it into monotonic!
				if (!this->m_sketches[i]->isValid())
				{
					this->m_sketches[i]->turnIntoMonotonic();
				}
			}
			//m_sketches[i]->smooth();
			this->m_sketches[i]->addHistory();
		}
	}
}


bool CrossSection::updateSketch()
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;

		//- Verify if the sketch has only one point: it means it was a click by the user and not a sketch
		//- In this case we can erase the memory for the sketch list
		if (this->m_sketches[i]->getSketchSize() < 2)
		{
			this->m_sketches.pop_back();

			//- If, even after erasing the memory, we still have sketch, than we need to return true.
			//- This is for the case where we can have more than one sketch in the same cross section.
			if (this->m_sketches.size())
				return true;

			return false;
		}
		else {
			this->m_sketches[i]->smooth();
			this->validateSketch();
			this->m_sketches[i]->clearOverSketch();
			return true;
		}
	}
	return false;
}


void CrossSection::resetHighlight()
{
	this->m_highlight = -1;
}


void CrossSection::resetSelect()
{
	this->m_select = -1;
}


bool CrossSection::highlightSketch(double mouse_x, double mouse_y)
{
	double minimumDist = 100000.0;

	//- For every sketch inside the collection
	for (unsigned int i = 0; i < m_sketches.size(); i++)
	{
		//- Makes sure that the current sketch isn't cleared:
		if (this->m_sketches[i]->getSketch().size())
		{
			double dist;
			dist = this->m_sketches[i]->distanceTo(mouse_x, mouse_y);
			if (dist < 20 && dist < minimumDist)
			{
				minimumDist = dist;
				this->m_highlight = i;
			}
		}
	}

	if (this->m_highlight > -1)
	{
		return true;
	}

	return false;
}


bool CrossSection::isSketchHighlighted()
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		if (this->m_highlight > -1)
			return true;
	}

	return false;
}


bool CrossSection::isSketchSelected()
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		if (this->m_select > -1)
			return true;
	}

	return false;
}


bool CrossSection::isSketchSelected(double mouse_x, double mouse_y)
{
	if (this->highlightSketch(mouse_x, mouse_y))
	{
		this->m_select = this->m_highlight;
		return true;
	}

	this->m_select = -1;
	return false;
}


bool CrossSection::selectHighlightedSketch()
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		//- Verify if there is a highlighted sketch
		if (this->m_highlight > -1)
		{
			this->m_select = this->m_highlight;
			return true;
		}
	}
	this->m_select = -1;
	return false;
}


unsigned int CrossSection::getNumberOfSketches(int& crossSection, int& direction)
{
	unsigned int response = 0;

	//- For all the sketches inside the collection
	for (unsigned int i = 0; i < this->m_sketches.size(); i++)
	{
		//- Verify if there is at least one sketch
		if (this->m_sketches[i]->getSketch().size())
		{
			crossSection = this->m_id;
			direction = this->m_direction;
			response++;
		}
	}

	return response;
}


unsigned int CrossSection::getNumberOfSketches()
{
	return this->m_sketches.size();
}


Curve2D CrossSection::getExtrudedSurfacePoints()
{
	Curve2D points;

	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		points = this->m_sketches[i]->getSketch();
	}

	return points;
}


void CrossSection::clearCurrentSketch()
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_select > -1)
			i = this->m_select;
		
		//- Makes sure that the current sketch isn't cleared:
		if (this->m_sketches[i]->getSketch().size())
		{
			this->m_sketches[i]->copyPreviousHistory();
			this->m_sketches[i]->clearSketch();
			this->m_sketches[i]->addHistory();
		}
	}
}


bool CrossSection::hasSketch()
{
	for (unsigned int i = 0; i < this->m_sketches.size(); i++)
	{
		if (this->m_sketches[i]->getSketch().size())
			return true;
	}

	return false;
}


double CrossSection::distanceFromSketchToMousePointer(double mouse_x, double mouse_y)
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;

		//- Makes sure that the current sketch isn't cleared:
		if (this->m_sketches[i]->getSketch().size())
		{
			return (this->m_sketches[i]->distanceTo(mouse_x, mouse_y));
		}
	}

	return -1.00;
}


void CrossSection::applySmooth()
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_select > -1)
			i = this->m_select;

		//- Makes sure that the current sketch isn't cleared:
		if (this->m_sketches[i]->getSketch().size())
		{
			this->m_sketches[i]->copyPreviousHistory();
			this->m_sketches[i]->smooth();
			this->m_sketches[i]->addHistory();
		}
	}
}


void CrossSection::applyRotation(double angle)
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_select > -1)
			i = this->m_select;

		//- Makes sure that the current sketch isn't cleared:
		if (this->m_sketches[i]->getSketch().size())
		{
			this->m_sketches[i]->rotate(angle);
			this->m_sketches[i]->addHistory();
		}
	}
}


void CrossSection::applyRotation(double angle, double center_x, double center_y)
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_select > -1)
			i = this->m_select;

		//- Makes sure that the current sketch isn't cleared:
		if (this->m_sketches[i]->getSketch().size())
		{
			this->m_sketches[i]->rotate(angle, center_x, center_y);
			this->m_sketches[i]->addHistory();
		}
	}
}


void CrossSection::applyScale(double ex, double ey)
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_select > -1)
			i = this->m_select;

		//- Makes sure that the current sketch isn't cleared:
		if (this->m_sketches[i]->getSketch().size())
		{
			this->m_sketches[i]->scale(Point2D(ex, ey));
			this->m_sketches[i]->addHistory();
		}
	}
}


void CrossSection::applyScaleMouse(double mouse_x, double mouse_y)
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_select > -1)
			i = this->m_select;

		//- Makes sure that the current sketch isn't cleared:
		if (this->m_sketches[i]->getSketch().size())
		{
			this->m_sketches[i]->scale(mouse_x, mouse_y, 1);
			this->m_sketches[i]->addHistory();
		}
	}
}


void CrossSection::applyTranslation(double dx, double dy)
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_select > -1)
			i = this->m_select;

		//- Makes sure that the current sketch isn't cleared:
		if (this->m_sketches[i]->getSketch().size())
		{
			this->m_sketches[i]->translate(Point2D(dx, dy));
			this->m_sketches[i]->addHistory();
		}
	}
}


bool CrossSection::isSketchModified()
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_select > -1)
			i = this->m_select;

		//- Makes sure that the current sketch isn't cleared:
		if (this->m_sketches[i]->getSketch().size())
		{
			if (this->m_sketches[i]->isModified())
			{
				return true;
			}
			else {
				this->m_sketches[i]->removeHistory();
			}
		}
	}

	return false;
}


bool CrossSection::undoSketch()
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_select > -1)
			i = this->m_select;

		return this->m_sketches[i]->undo();
	}

	return false;
}


bool CrossSection::redoSketch()
{
	//- Verify if there is at least one sketch
	if (this->m_sketches.size())
	{
		unsigned int i = this->m_sketches.size() - 1;
		//- Verify if the direction is MapView
		if (this->m_direction == Direction::Value::HEIGHT && this->m_select > -1)
			i = this->m_select;

		return this->m_sketches[i]->redo();
	}

	return false;
}


void CrossSection::printInfo()
{
	if (Comments::SBIM_CROSS_SECTION())
	{
		//qDebug() << "\n CrossSection::printInfo():" << "\n\tCrossSection ID:" << this->m_id << "\n\tDirection:" << this->m_direction << "\n\tNumber of sketches: " << this->m_sketches.size();
	}
}
