/**
* @file sketch_template.h
* @author Sicilia Judice
* @brief File containing the class SketchTemplate
*/

#ifndef SKETCHTEMPLATE_H
#define SKETCHTEMPLATE_H

#include <vector>
#include "Constants.h"

class SketchTemplate {

public:

	/**
	* Constructor.
	*/
	SketchTemplate();


	/**
	* Copy constructor.
	* @param newSketchTemplate a const reference to another sketch template.
	*/
	SketchTemplate(const SketchTemplate& newSketchTemplate);


	/**
	* Assignment operator
	* @param newSketchTemplate a const reference to another sketch template.
	*/
	SketchTemplate& operator=(const SketchTemplate& newSketchTemplate);


	/**
	* Destructor.
	*/
	~SketchTemplate();


	/**
	* Method to reset the sketch template to its initial state.
	* @return void
	*/
	void reset();


	/**
	* Method to return the size of the list.
	* @return size_t
	*/
	size_t getNumberOfSketchTemplates();


	/**
	* Method to save a new sketch template on the list.
	* Return TRUE if the template is saved.
	* @param curve is the geometry of a sketch to be saved as a template.
	* @return bool
	*/
	bool saveSketchTemplate(std::vector<double> curve);

	
	/**
	* Method to load the last saved sketch template on the list.
	* @return std::vector<double>
	*/
	std::vector<double> loadSketchTemplate();


	/**
	* Method to get all the templates combined in one single vector.
	* @return std::vector<double>
	*/
	std::vector<double> getTemplates();


	/**
	* Method to get the template at position i.
	* @param i is the position in the list.
	* @return std::vector<double>
	*/
	std::vector<double> getTemplate(size_t i);


private:
	std::vector<std::vector<double>> list;	/**< Collection of sketches to be used as a template. */

	bool isDuplicated(std::vector<double> curve);
};

#endif //SKETCHTEMPLATE_H