
/**
* @file sketch_stack.h
* @author Sicilia Judice
* @brief File containing the class SketchStack
*/

#ifndef SKETCHSTACK_H
#define SKETCHSTACK_H

#include <vector>

#include "libs/polygonalcurve/CurveN.hpp"
#include "Constants.h"


/**
* SketchStack is a stack of sketches. 
* This structure is used to store every state of a sketch, being used to manage the undo/redo process.
* All the states are stored until the sketch is submitted as a curve to the surface generator.
*/

class SketchStack
{

public:

    /**
    * Constructor.
    */
    SketchStack();


    /**
    * Copy constructor.
    * @param newStack a const reference to another sketch stack.
    */
    SketchStack(const SketchStack& newStack);


    /**
    * Assignment operator
    * @param newStack a const reference to another sketch stack.
    */
    SketchStack& operator=(const SketchStack& newStack);


    /**
    * Destructor.
    */
    ~SketchStack();


    /**
    * Method that return is_empty flag.
    * @return bool
    */
    bool isEmpty();


    /**
    * Method to clear the stack.
    * If the stack is not empty, first it needs to clear every curve and then clear the stack.
    * @return void
    */
    void clear();


    /**
    * Method to return the size of the stack.
    * @return size_t
    */
    size_t getStackSize();


    /**
    * Method that returns the curve indicated by the last position in the stack.
    * If the stack is empty, it returns an empty curve.
    * @return Curve2D
    */
    Curve2D getCurve();


    /**
    * Method that returns the curve in a specific position i in the stack.
    * If the stack is empty, it returns an empty curve.
    * @param i is the index of the stack.
    * @return Curve2D
    */
    Curve2D getCurve(size_t i);

    
    /**
    * Method to add a new state (curve) of the sketching process in the stack.
    * If the stack is empty, first it needs to create a new instance of the Curve2D.
    * @param newCurve is the new curve representing the new state of the sketch.
    * @return void
    */
    void setCurve(Curve2D newCurve);


    /**
    * Method to push a new state of the sketching process in the stack.
    * @param newCurve is the new curve representing the new state of the sketch.
    * @return void
    */
    void push(Curve2D newCurve);


    /**
    * Method to push the last sketch in the stack.
    * @return void
    */
    void pushLastCurve();


    /**
    * Method to pop a curve from the stack.
    * @return void
    */
    void pop();


    /**
    * Method to pop curves until current position indicated by last variable. 
    * Used in the undo/redo process.
    * @return void
    */
    void popUntilCurrentPosition();

    
    /**
    * Method to verify if the last curve pushed is different from the previous one.
    * @return bool
    */
    bool isSketchModified();


    /**
    * Method to decrease the index of the stack.
    * @return bool
    */
    bool undo();


    /**
    * Method to increase the index of the stack.
    * @return bool
    */
    bool redo();


    /**
    * Method to print info about the stack.
    */
    void printInfo();


private:

    std::vector<Curve2D> m_stack;              /**< A collection of bidimensional curves where each curve represents a state of the sketching process. */

    size_t last = 0;                           /**< An index to store the last position of the stack. This position indicates the state that is being displayed for the user. */
        
    bool is_empty = true;                      /**< A flag to indicate when the stack is empty. */
};

#endif // SKETCHSTACK_H
