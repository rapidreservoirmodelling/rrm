/*******************************************************************
  *  @file   main.cpp
  *  @brief  Main function that calls the main GUI
  *  @author Fazilatur Rahman
  *  @date   January, 2020
  ******************************************************************/

#include "ApplicationHandler.h"

int main(int argc, char** argv)
{
    ApplicationHandler(argc, argv);
    return 0;
}


