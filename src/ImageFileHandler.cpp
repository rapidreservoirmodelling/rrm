#include "ImageFileHandler.h"

#include <QDebug>
#include <QString>
#include <QObject>
#include <QUrl>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <filesystem>
#include <qstandardpaths.h>
#include<direct.h>
#include <QtConcurrent/QtConcurrentRun>

#include <filesystem>
namespace fs = std::filesystem;


ImageFileHandler::ImageFileHandler(QString fileName)
{
    this->m_file_name = fileName;
    //this->m_json_string = imageData;
    //this->setImageFolder();
    //this->prepareJson();
    
}

void ImageFileHandler::save(QString imageData)
{
    this->m_json_string = imageData;
    this->setImageFolder();
    this->prepareJson();

    if (this->m_file_name.isEmpty())
    {
        //qDebug() << "\n ImageFileHandler::save(): \t File name required!";
        
        return;
    }

    QFile file(this->m_file_name);

    if (!file.open(QIODevice::WriteOnly))
    {
        //qDebug() << "\n ImageFileHandler::save(): \t Could not open file for writing";
        
        return;
    }

    if(this->m_json_string !="")
    {
        QTextStream out(&file);
        out << this->m_json_string;

        setImageFolder();
    }
    else {
        //qDebug() << "\n ImageFileHandler::save(): \t Json string empty!";
        
    }

    file.flush();
    file.close();
}


bool ImageFileHandler::load()
{
    if (this->m_file_name.isEmpty())
    {
        //qDebug() << "\n ImageFileHandler::load(): \t File name required!";
        
        return false;
    }

    QFile file(this->m_file_name);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        //qDebug() << "\n ImageFileHandler::load(): \t Could not open the file for reading";
        
        return false;
    }

    QTextStream in(&file);
    QString myText = in.readAll();
    this->m_json_string = myText;

    ////qDebug() << "\n ImageFileHandler::load(): \t Json String=" << this->m_json_string;

    file.close();
    return true;
}

void ImageFileHandler::prepareJson()
{
    QJsonDocument jsonResponse = QJsonDocument::fromJson(this->m_json_string.toUtf8());

    QJsonArray images_original_array = jsonResponse.array();
    
    QJsonArray images_modified_array;
    QJsonObject each_output_image_object;
    
    for (int i = 0; i < images_original_array.size(); i++)
    {
        QJsonObject each_image = (images_original_array[i]).toObject();
        
        QUrl source_url((each_image["absolute_path"]).toString());
        QString each_image_source_path = QString::fromStdString(getAbsPath(source_url));
        QString each_image_destination_path = m_image_folder_path + "/" + (each_image["file_name"]).toString();
        
        copyImageFile(each_image_source_path, m_image_folder_path);

        QUrl file_url = QUrl::fromLocalFile(each_image_destination_path);

        //Store resulting image details after saving image file
        each_output_image_object.insert(QString("direction_id"), QJsonValue(each_image["direction_id"]));
        each_output_image_object.insert(QString("cross_section_id"), QJsonValue(each_image["cross_section_id"]));
        each_output_image_object.insert(QString("absolute_path"), QJsonValue(file_url.toString()));
        each_output_image_object.insert(QString("file_name"), QJsonValue(each_image["file_name"]));

        images_modified_array.push_back(each_output_image_object);
    }

    QJsonDocument jsonDoc(images_modified_array);
    this->m_json_string = jsonDoc.toJson();

}

void ImageFileHandler::setImageFolder()
{
    QFile file(this->m_file_name);
    QFileInfo fileInfo(file);

    ////qDebug() << "Image file path: " << path.absolutePath();
    m_image_folder_path = fileInfo.absolutePath();

    ////qDebug() << "Image file path: image file name = " << (m_file_name.section('/', -1)).section('.', 0, 0);
    QString imageFolderName = (m_file_name.section('/', -1)).section('.', 0, 0) + "_images";
    QString path = QDir::cleanPath(m_image_folder_path + "//" + imageFolderName);
    ////qDebug() << "Image file path: " << path;
    ////qDebug() << "Image file path: std string  =  " << (path.toStdString()).c_str();
    
    if (!QDir(path).exists())
    {
        int response = mkdir((path.toStdString()).c_str());
        ////qDebug() << "Image file path: response =  " << response;

        if (response == -1)
        {
            //qDebug() << "Image file path: folder creation error! ";
        }
        else {
            m_image_folder_path = path;
            //qDebug() << "Image file path: folder created: ";
        }
    }
    else {
        //qDebug() << "Image file path: folder already exists! ";
        
        m_image_folder_path = path;
                
    }

}

bool ImageFileHandler::copyImageFile(const QString& sourceFile, const QString& destinationDir)
{
    //qDebug() << "ImageFileHandler::saveImageFile: source:" << sourceFile;

    QFileInfo fileInfo(sourceFile);
    ////qDebug() << "ImageFileHandler::saveImageFile: source folder:" << fileInfo.absoluteFilePath();
    QString destinationFile = destinationDir + "/" + fileInfo.fileName();
    ////qDebug() << "ImageFileHandler::saveImageFile: " << sourceFile << " to destination " << destinationFile;
    bool result = QFile::copy(sourceFile, destinationFile);
    
    try{
    fs::copy(sourceFile.toStdString(), destinationFile.toStdString()); // copy file
    }
    catch (std::filesystem::filesystem_error const& ex) {
        //qDebug() << "what():  " << ex.what() << '\n' << "code().value():    " << ex.code().value() << '\n' << "code().category(): " << ex.code().category().name() << '\n';
    }
    return result;
}

std::string ImageFileHandler::getAbsPath(const QUrl& path)
{
    //////qDebug() << "VisualizationHandler:: Saving Model: " << path;

    QUrl localPath;
    if (path.isLocalFile())
    {
        localPath = path.toLocalFile();

    }
    else localPath = path;

    QString pathString = localPath.toEncoded();
    pathString = pathString.replace("%20", "\ ");
    return pathString.toUtf8().constData();

}

QString ImageFileHandler::cleanImageFolder(QString path)
{
    if (QDir(path).entryInfoList(QDir::NoDotAndDotDot | QDir::AllEntries).count() != 0)
    {
        QDir dir(path);
        dir.setNameFilters(QStringList() << "*.*");
        dir.setFilter(QDir::Files);
        foreach(QString dirFile, dir.entryList())
        {
            dir.remove(dirFile);
        }
    }

    return path;
}

void ImageFileHandler::parseJson()
{
    /*QJsonDocument jsonResponse = QJsonDocument::fromJson(this->m_json_string.toUtf8());

    QJsonArray images_original_array = jsonResponse.array();

    for (int i = 0; i < images_original_array.size(); i++)
    {
        QJsonObject each_image = (images_original_array[i]).toObject();

        QUrl source_url((each_image["absolute_path"]).toString());
        QString each_image_source_path = QString::fromStdString(getAbsPath(source_url));
        QString each_image_destination_path = m_image_folder_path + "/" + (each_image["file_name"]).toString();

        copyImageFile(each_image_source_path, m_image_folder_path);

        //Store resulting image details after saving image file
        each_output_image_object.insert(QString("direction_id"), QJsonValue(each_image["direction_id"]));
        each_output_image_object.insert(QString("cross_section_id"), QJsonValue(each_image["cross_section_id"]));
        each_output_image_object.insert(QString("absolute_path"), QJsonValue(each_image_destination_path));
        each_output_image_object.insert(QString("file_name"), QJsonValue(each_image["file_name"]));

        images_modified_array.push_back(each_output_image_object);
    }

    QJsonDocument jsonDoc(images_modified_array);
    this->m_json_string = jsonDoc.toJson();
    */
}

QString ImageFileHandler::getJson()
{
    return m_json_string;
}