#include "ApplicationHandler.h"

#include <QApplication>
#include <QDebug>
#include <QIcon>
#include <QQmlApplicationEngine>

#include <QQmlContext>
#include <QQuickStyle>
#include <QQuickView>

#include <QtConcurrent/QtConcurrentMap>
#include <QtConcurrent/QtConcurrentRun>
#include <QFutureWatcher>

#include <QWidget>

#include "Model3dHandler.h"
#include "viz3d/CommandProcessor.h"
#include "viz3d/VtkFboItem.h"
#include "utils/screen.h"
#include "trajectory_model.h"
#include "object_tree/RrmTreeModel.h"
//#include "object_tree/TreeUpdater.h"
#include "templates/trajectory_list.h"
#include "VisualizationHandler.h"

#include "Constants.h"

#include "fd/QmlWidget.h"

#include <QtSql>

// #include "git_version.hpp"

ApplicationHandler::ApplicationHandler(int argc, char **argv)
{
	QApplication app(argc, argv);
	QQmlApplicationEngine engine;
	
	//Set app icon and style
	app.setWindowIcon(QIcon(":/images/icons/rrm-icon.ico"));
	QQuickStyle::setStyle("Material");

	//qDebug() << "QSQL Drivers: " << QSqlDatabase::drivers();

	///////////////// Start: Expose C++ classes to QML /////////////////////////////////////
	//Model3D Handling
	//Instantiate Singleton Model3DHandler and 
	//Sets context property "model3DHandler" on the global qml root context
	m_model3dHandler = Model3dHandler::getInstance(&engine);
	if (m_model3dHandler)
	{
		//connect(m_model3dHandler, &Model3dHandler::clearCompleted, this, &ApplicationHandler::clearViews);
		//The 3DRenderer allows moving the plane when zoomed-in; So, we need to capture the cross-section number
		//connect(m_model3dHandler, &Model3dHandler::crossSectionNumberChanged, m_vizHandler, &VisualizationHandler::initiateSurfacePreview);
		//qDebug() << "ApplicationHandler: Model3dHandler instance loaded successfully!";
	}
	else
	{
		qCritical() << "ApplicationHandler: Unable to get Model3dHandler instance";
		return;
	}

	//Object Tree Handling
	m_treeModel = RrmTreeModel::getInstance(&engine);

	/////// (1) Register QML types
	qmlRegisterType<TrajectoryModel>("TrajectoryModel", 1, 0, "TrajectoryModel");
	qmlRegisterUncreatableType<TrajectoryList>("TrajectoryModel", 1, 0, "TrajectoryList",
		QStringLiteral("TrajectoryList should not be created in QML"));
	qmlRegisterUncreatableType<TrajectoryList>("TrajectoryModel", 1, 0, "SketchList",
		QStringLiteral("SketchList should not be created in QML"));
	qmlRegisterUncreatableType<TrajectoryList>("TrajectoryModel", 1, 0, "ContourList",
		QStringLiteral("ContourList should not be created in QML"));

	//Register ModelViewerFboItem type to be used to display VTK window
	qmlRegisterType<VtkFboItem>("Qt3dFbo", 1, 0, "ModelViewerFboItem");
	//Register Flow diagnostic window handler
	//qmlRegisterType<FdHandler>("FdWindow", 1, 0, "FdWindow");
	qmlRegisterType<QmlWidget>("QmlWidget", 1, 0, "QmlWidget");
	
	////// (2) Use the QML engine's rootContext to attach C++ objects
	QQmlContext* ctxt = engine.rootContext();

	//VTK and overall GUI Handling
	ctxt->setContextProperty("vizHandler", this);
	
	//QML event handling
	ctxt->setContextProperty("receiver", &receiver);
	
	//CanvasHandler
	ctxt->setContextProperty("canvasHandler", &canvasHandler);
	
	//Trajectory List
	TrajectoryList trajectoryList;
	ctxt->setContextProperty("trajectoryList", &trajectoryList);

	//Sketch List
	TrajectoryList sketchList;
	ctxt->setContextProperty("sketchList", &sketchList);

	//Sketch List
	TrajectoryList contourList;
	ctxt->setContextProperty("contourList", &contourList);

	//Constants
	MainWindowConstants mwc;
	ctxt->setContextProperty("MAIN_WINDOW", &mwc);
	ButtonCanvasConstants btc;
	ctxt->setContextProperty("BT_CANVAS", &btc);
	ButtonFloatingConstants bfc;
	ctxt->setContextProperty("BT_FLOATING", &bfc);
	ButtonToolBarConstants btbc;
	ctxt->setContextProperty("BT_TOOLBAR", &btbc);
	ButtonDialogConstants bdc;
	ctxt->setContextProperty("BT_DIALOG", &bdc);
	ObjectTreeConstants otc;
	ctxt->setContextProperty("OBJECT_TREE", &otc);
	ButtonFileMenuConstants btfmc;
	ctxt->setContextProperty("BT_FILEMENU", &btfmc);
	OpenDialogConstants odc;
	ctxt->setContextProperty("OPEN_DIALOG", &odc);
	Comments com;
	ctxt->setContextProperty("COMMENTS", &com);
		
	//Constants: exposing an ENUM from C++ to QML
	//To protect the instantiation of the Constants classes from QML used the qmlRegisterUncreatableType
	qmlRegisterUncreatableType<CanvasAction>("Constants", 1, 0, "ACTION", "Not creatable as it is an enum type");
	qmlRegisterUncreatableType<Direction>("Constants", 1, 0, "DIRECTION", "Not creatable as it is an enum type");
	qmlRegisterUncreatableType<CanvasID>("Constants", 1, 0, "CANVAS_ID", "Not creatable as it is an enum type");
	qmlRegisterUncreatableType<Mouse>("Constants", 1, 0, "MOUSE", "Not creatable as it is an enum type");
	qmlRegisterUncreatableType<GeologicRules>("Constants", 1, 0, "GEOLOGICRULES", "Not creatable as it is an enum type");
	qmlRegisterUncreatableType<TreeNodeParentType>("Constants", 1, 0, "TreeNodeParentType", "Not creatable as it is an enum type");

	//Flow Diagnostic
	//ctxt->setContextProperty("fdHandler", &fdHandler);

	//Calculate Device independent pixel density to adapt to different display resolutions
	QList<QScreen*> screens = app.screens();
	Screen screenDpi;
	screenDpi.init(screens.first()); //primary screen only
	screenDpi.setBaselineDpi(160); //set baseline pixel (logical) density
	ctxt->setContextProperty("screenDpi", QVariant::fromValue(&screenDpi));	
	
	///////////////// End: Expose C++ classes to QML /////////////////////////////////////

	//Load main QML file
	engine.load(QUrl("qrc:/resources/main.qml"));

	// Get reference to the ModelViewerFboItem created in QML
	QObject* rootObject = engine.rootObjects()[0];
	m_vtkFboItem = rootObject->findChild<VtkFboItem*>("modelViewerFboItem");
	
	// Create class instance for Viz3D commandProcessor
	m_commandProcessor = std::shared_ptr<CommandProcessor>(new CommandProcessor());

	// Set command processor for the vtkFboItem
	if (m_vtkFboItem)
	{
		m_vtkFboItem->setCommandProcessor(m_commandProcessor);
		
	}
	else
	{
		qCritical() << "ApplicationHandler: Unable to get vtkFboItem instance";
		return;
	}
	
	//Visualization Handling
	//Creates the newVizHandler context property
	m_vizHandler = VisualizationHandler::getInstance(&engine, m_vtkFboItem, &canvasHandler, m_commandProcessor);
	if (m_vizHandler)
	{
		// m_vizHandler->setCurrentGitHash("sd8920fhd");
	  
		connect(m_treeModel, &RrmTreeModel::expandDomainColorMap, m_vizHandler, &VisualizationHandler::addNewColorData);
		
		//Connection for domain creation/edit
		connect(m_vizHandler, &VisualizationHandler::domainListUpdated, m_treeModel, &RrmTreeModel::updateDomainNodes);
		
		//Region Visualization
		connect(m_vizHandler, &VisualizationHandler::clearRegionCurveBox, &canvasHandler, &CanvasHandler::clearRegionCurveBox);
		//connect(m_vtkFboItem, &VtkFboItem::regionsAdded, &canvasHandler, &CanvasHandler::pullRegionCurveBox);
				
		connect(m_vizHandler, &VisualizationHandler::regionsAdded, m_treeModel, &RrmTreeModel::updateRegionNodes);
		connect(m_vtkFboItem, &VtkFboItem::regionsAdded, m_vizHandler, &VisualizationHandler::regionsDisplayed);
		connect(m_vtkFboItem, &VtkFboItem::regionsRemoved, m_vizHandler, &VisualizationHandler::clearRegionsList);
		connect(m_vtkFboItem, &VtkFboItem::regionsAdded, m_vizHandler, &VisualizationHandler::fillDomainList);

		//Connection for region creation/edit
		connect(m_vizHandler, &VisualizationHandler::regionListUpdated, m_treeModel, &RrmTreeModel::updateRegionNodes);

		//Connection for Surface submission
		connect(m_vtkFboItem, &VtkFboItem::surfacesRemoved, m_vizHandler, &VisualizationHandler::updateSurfaceList);
		connect(m_vizHandler, &VisualizationHandler::surfaceListUpdated, m_vtkFboItem, &VtkFboItem::addSurfaces);
		connect(m_vizHandler, &VisualizationHandler::surfaceListUpdated, m_treeModel, &RrmTreeModel::updateSurfaceItems);

		//Connection for Undo/Redo surfaces
		connect(m_model3dHandler, &Model3dHandler::undoCompleted, m_vizHandler, &VisualizationHandler::undo3dSurfaces);
		connect(m_model3dHandler, &Model3dHandler::redoCompleted, m_vizHandler, &VisualizationHandler::redo3dSurfaces);
		connect(m_vtkFboItem, &VtkFboItem::surfaceUpdateCompleted, m_vizHandler, &VisualizationHandler::enableUndoRedoButtons);

		//Connection for changing surface color
		connect(m_vizHandler, &VisualizationHandler::surfaceColorUpdated, m_vtkFboItem, &VtkFboItem::changeColor);
		connect(m_vizHandler, &VisualizationHandler::surfaceColorListUpdated, m_treeModel, &RrmTreeModel::updateSurfaceItems);

		//Connection for changing region color
		connect(m_vizHandler, &VisualizationHandler::regionColorUpdated, m_vtkFboItem, &VtkFboItem::changeColor);
		connect(m_vizHandler, &VisualizationHandler::regionColorListUpdated, m_treeModel, &RrmTreeModel::updateRegionNodes);
		//connect(m_vizHandler, &VisualizationHandler::regionColorListUpdated, m_vizHandler, &VisualizationHandler::adjustRegionColorIfExistInDomainList);
		
		//Connection for reset new
		connect(m_model3dHandler, &Model3dHandler::clearCompleted, m_vizHandler, &VisualizationHandler::clearViews);
		connect(m_vtkFboItem, &VtkFboItem::viewCleared, m_vizHandler, &VisualizationHandler::reset);
		connect(m_vizHandler, &VisualizationHandler::resetViz, m_treeModel, &RrmTreeModel::reset);

		//Connection for setting cross-section number change
		connect(m_vizHandler, &VisualizationHandler::crossSectionChanged, &receiver, &EventReceiver::setCrossSection);

		//Connection for setting cross-section plane orientation
		connect(m_vizHandler, &VisualizationHandler::planeOrientationChanged, &receiver, &EventReceiver::setPlaneOrientation);

		//Connections for visibility changes
		connect(m_vizHandler, &VisualizationHandler::surfaceVisibilityUpdated, m_vtkFboItem, &VtkFboItem::changeVisibility);
		connect(m_vizHandler, &VisualizationHandler::surfaceVisibilityListUpdated, m_treeModel, &RrmTreeModel::updateSurfaceItems);
		connect(m_vizHandler, &VisualizationHandler::regionVisibilityListUpdated, m_treeModel, &RrmTreeModel::updateRegionNodes);
		connect(m_vizHandler, &VisualizationHandler::domainVisibilityListUpdated, m_treeModel, &RrmTreeModel::updateDomainNodes);
		connect(m_vizHandler, &VisualizationHandler::parentGroupVisibilityUpdated, m_treeModel, &RrmTreeModel::updateParentNode);
		
		//Testing 3DSurface visibility  
		//connect(m_vizHandler, &VisualizationHandler::surfaceVisibilityListUpdated, m_vtkFboItem, &VtkFboItem::addSurfaces);

		//Connection for preview
		connect(m_vtkFboItem, &VtkFboItem::previewRemoved, m_vizHandler, &VisualizationHandler::initiateSurfacePreview);
		connect(m_vizHandler, &VisualizationHandler::surfaceListUpdated, m_vizHandler, &VisualizationHandler::checkPreviewButtonStatus);
		connect(m_vtkFboItem, &VtkFboItem::previewAdded, m_vizHandler, &VisualizationHandler::completedPreview);

		connect(m_vtkFboItem, &VtkFboItem::crossSectionUpdate, m_vizHandler, &VisualizationHandler::updateCrossSection);

		//Connection for closing FD window
		connect(m_vizHandler, &VisualizationHandler::fdWindowClosed, this, &ApplicationHandler::closeFdInterface);

		//Merge surface
		connect(m_vizHandler, &VisualizationHandler::surfaceMerged, m_treeModel, &RrmTreeModel::updateSurfaceItems);

		//VE Factor change
		connect(m_vizHandler, &VisualizationHandler::veFactorChanged, m_vtkFboItem, &VtkFboItem::changeVeFactor);

		//Setting the rrm6 model path
		connect(m_vizHandler, &VisualizationHandler::modelLoaded, this, &ApplicationHandler::setPath);

		//Flat height change signal
		connect(m_vizHandler, &VisualizationHandler::flatHeightAdjusted, m_vtkFboItem, &VtkFboItem::adjust3dViewFlatFactor);
		connect(m_vtkFboItem, &VtkFboItem::boundingBoxCleaned, m_vizHandler, &VisualizationHandler::initiateSurfaceSubmissoin);
		connect(m_vizHandler, &VisualizationHandler::surfaceToFlatSelected, &canvasHandler, &CanvasHandler::flattenTheSelectedSurface);

		// Loading Model from .rrm6 file
		connect(m_vizHandler, &VisualizationHandler::modelLoadingStarted, &canvasHandler, &CanvasHandler::modelLoaded);
		connect(&canvasHandler, &CanvasHandler::modelLoadingCompleted, m_vizHandler, &VisualizationHandler::loadMeshes);
		
		//Drop and create domain
		connect(m_vizHandler, &VisualizationHandler::domainCreatedForRegion, m_vizHandler, &VisualizationHandler::addRegionToTheCreatedDomain);
	}
	else
	{
		qCritical() << "ApplicationHandler: Unable to get VizHandler instance";
		return;
	}

	int rc = app.exec();
	
}

void ApplicationHandler::addSurfacePreview() const
{
	
}


void ApplicationHandler::toggleFdFlag()
{
	if (m_fdWindowOpenFlag !=true)
	{
		m_fdWindowOpenFlag = true;
	}
	else m_fdWindowOpenFlag = false;
		
}

bool ApplicationHandler::fdFlag()
{
	return m_fdWindowOpenFlag;
}

int ApplicationHandler::getFdWindowWidth()
{
	return m_fdWindowWidth;
}

int ApplicationHandler::getFdWindowHeight()
{
	return m_fdWindowHeight;
}

void ApplicationHandler::connectToFdInterface()
{
	fdInterface->createFlowDiagnosticsWindow();
}

void ApplicationHandler::closeFdInterface()
{
	fdInterface->closeFlowDiagnosticsWindow();
}

void ApplicationHandler::setPath(std::string path)
{
	m_modelPath = path;
	fdInterface->setProjectPath(path);
}