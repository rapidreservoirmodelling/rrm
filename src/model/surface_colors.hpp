#pragma once

#include <array>

namespace stub {

    /// Stub function to return a nice color given a surface unique id
    inline std::array<int,3> getSurfaceColor(int surface_id)
    {
        std::array<int, 3> color = {255, 0, 0};
        if (surface_id < 1)
        {
            // return red if surface_id is invalid
            return color;
        }

        // Set2 colormap from 
        // https://colorbrewer2.org/#type=qualitative&scheme=Set2&n=8
        constexpr int num_colors = 8;
        int colormap[3*num_colors] = {
            102,   194,   165,
            252,   141,    98,
            141,   160,   203,
            231,   138,   195,
            166,   216,    84,
            255,   217,    47,
            229,   196,   148,
            179,   179,   179
        };

        std::size_t i = surface_id % num_colors;
        color[0] = colormap[3*i + 0];
        color[1] = colormap[3*i + 1];
        color[2] = colormap[3*i + 2];

        return color;
    }

} // namespace stub
