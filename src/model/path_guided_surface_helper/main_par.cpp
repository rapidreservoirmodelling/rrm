#include "ode_solver_2d.hpp"
#include <chrono>
#include <Eigen/Dense>

/* bool RulesProcessor::createLengthwiseExtrudedSurface( size_t surface_id, */ 
/*         const std::vector<double> &cross_section_curve_point_data, double cross_section_depth, */ 
/*         const std::vector<double> &path_curve_point_data); */

int main()
{
    struct {
        double x, y, z;
    } origin_, length_;
    origin_ = {0.0, 0.0, 0.0};
    length_ = {500.0, 500.0, 500.0};

    std::vector<double> cross_section_curve_point_data;
    double cross_section_depth = 0.0;
    std::vector<double> path_curve_point_data;

    size_t N = 64;
    double ti = 0;
    double Lx = (length_.x - origin_.x);
    double Ly = (length_.y - origin_.y);
    double Lz = (length_.z - origin_.z);
    double dx = Lx/static_cast<double>(N-1);
    //double dy = Ly/static_cast<double>(N-1);
    double dz = Lz/static_cast<double>(N-1);
    double w = 2*boost::math::constants::pi<double>()/Lz;

    cross_section_curve_point_data.resize(2*N);
    path_curve_point_data.resize(2*N);
    cross_section_depth = 0.0;

    // Create guiding path
    for (size_t i = 0; i < N; ++i)
    {
        ti = origin_.z + static_cast<double>(i)*dz;
        path_curve_point_data[2*i + 0] = (length_.x/2 + origin_.x) + Lx*std::cos(w*ti)/8;
        path_curve_point_data[2*i + 1] = ti;
    }

    // Create cross-section
    for (size_t i = 0; i < N; ++i)
    {
        ti = origin_.z + static_cast<double>(i)*dx;
        path_curve_point_data[2*i + 0] = ti;
        path_curve_point_data[2*i + 1] = (length_.y/3 + origin_.y) + Ly*std::cos(2*w*ti)/8;
    }

    /* auto surfaceCreator = [this]( size_t s_id, const std::vector<double> &pts ) -> bool */ 
    /* { */
    /*     const double fill_distance_factor = 1.0/(10.0*std::sqrt(2)); */
    /*     auto sqr = [](double x) -> double { return x*x; }; */
    /*     double fill_distance = std::sqrt(sqr(length_.x-origin_.x) + sqr(length_.z-origin_.z))*fill_distance_factor; */

    /*     return this->modeller_.createSurface(s_id, pts, fill_distance); */
    /* }; */

    std::cout << "Trying to create path guided surface:\n"; 
    std::cout << "---> Setting solver properties\n"; 
    odeSolver2D S;

    double tol_x = 0.05 * length_.x;
    double tol_z = 0.05 * length_.z;
    S.setDomainOrigin(origin_.x-tol_x, origin_.z-tol_z);
    S.setDomainSize(length_.x+2*tol_x, length_.z+2*tol_z);

    auto curve_size = path_curve_point_data.size()/2;
    std::vector<double> path_w(curve_size), path_l(curve_size);

    std::cout << "---> Setting solver input\n"; 
    for (size_t i = 0; i < curve_size; ++i )
    {
        path_w[i] = path_curve_point_data[2*i + 0];
        path_l[i] = path_curve_point_data[2*i + 1];
        std::cout << "::: Adding point: (" << path_w[i] << ", " << path_l[i] << ")\n";
    }
    S.inputCurveTangentVectors(path_w, path_l, 256);

    std::cout << "---> Interpolating vector field\n"; 
    if ( !S.interpolateVectorField() )
    {
        std::cout << "---> ---> Could not interpolate vector field\n"; 
        return 0;
    }
    
    size_t num_samples = 80;
    std::vector<std::vector<double>> orbits(cross_section_curve_point_data.size()/2);
    for( auto& o : orbits )
    {
        o.reserve(3*num_samples);
    }

    std::vector<double> surface_points;
    double wi, li = cross_section_depth, hi;
    std::cout << "---> Sampling final surface: " << std::flush; 
    auto t0 = std::chrono::high_resolution_clock::now();
    odeSolver2D::XCoordinates orbit_w;
    odeSolver2D::YCoordinates orbit_l;
    #pragma omp parallel for shared(orbits, cross_section_curve_point_data, S) firstprivate(wi, li, hi, orbit_w, orbit_l)
    for ( size_t i = 0; i < cross_section_curve_point_data.size()/2; ++i )
    {
        wi = cross_section_curve_point_data[2*i + 0];
        hi = cross_section_curve_point_data[2*i + 1];

        std::tie(orbit_w, orbit_l) = odeSolver2D::sampleCurve(S.getOrbit(wi, li), num_samples);
        std::cout << "---> ---> iteration " << i << "; sampled points: " << orbit_l.size() << "; orbit height: " << hi << "\n"; 
        for (size_t j = 0; j < orbit_l.size(); ++j)
        {
            orbits[i].push_back(orbit_w[j]);
            orbits[i].push_back(hi);
            orbits[i].push_back(orbit_l[j]);
            /* surface_points.push_back(orbit_w[j]); */
            /* surface_points.push_back(hi); */
            /* surface_points.push_back(orbit_l[j]); */
        }
    }
    auto t1 = std::chrono::high_resolution_clock::now();
    auto dt = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();
    surface_points.reserve(orbits.size()*num_samples*3);
    for( auto i = 0; i < orbits.size(); ++i )
    {
        for ( auto j = 0; j < 3*num_samples; ++j )
        {
            surface_points[j] = orbits[i][j];
        }
    }
    std::cout << dt << " milliseconds, for " << surface_points.size()/3 << "points\n" << std::flush;

    /* std::cout << "---> Creating final surface\n"; */ 
    /* bool success = processSurfaceCreation(surfaceCreator, surface_id, surface_points); */

    return 0;
}
