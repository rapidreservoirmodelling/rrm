#include "ode_solver_2d.hpp"
#include <chrono>
#include <Eigen/Dense>

/* bool RulesProcessor::createLengthwiseExtrudedSurface( size_t surface_id, */ 
/*         const std::vector<double> &cross_section_curve_point_data, double cross_section_depth, */ 
/*         const std::vector<double> &path_curve_point_data); */

#include "path_guided_surface.hpp"

int main()
{
    //
    // Compute input data
    //
    struct {
        double width, height, length;
    } model_origin, model_size;
    model_origin = {0.0, 0.0, 0.0};
    model_size = {500.0, 500.0, 500.0};

    std::vector<double> cross_section_curve_point_data;
    /* double cross_section_depth = 0.0; */
    std::vector<double> path_curve_point_data;

    size_t N = 64;
    double ti = 0;
    double Lw = (model_size.width - model_origin.width);
    double Lh = (model_size.height - model_origin.height);
    double Ll = (model_size.length - model_origin.length);
    double dw = Lw/static_cast<double>(N-1);
    //double dy = Lh/static_cast<double>(N-1);
    double dl = Ll/static_cast<double>(N-1);
    double w = 2*boost::math::constants::pi<double>()/Ll;

    cross_section_curve_point_data.resize(2*N);
    path_curve_point_data.resize(2*N);
    /* cross_section_depth = 0.0; */

    // Create guiding path
    for (size_t i = 0; i < N; ++i)
    {
        ti = model_origin.length + static_cast<double>(i)*dl;
        cross_section_curve_point_data[2*i + 0] = (model_size.width/2 + model_origin.width) + Lw*std::cos(w*ti)/8;
        cross_section_curve_point_data[2*i + 1] = ti;
    }

    // Create cross-section
    for (size_t i = 0; i < N; ++i)
    {
        ti = model_origin.width + static_cast<double>(i)*dw;
        path_curve_point_data[2*i + 0] = ti;
        path_curve_point_data[2*i + 1] = (model_size.height/3 + model_origin.height) + Lh*std::cos(2*w*ti)/8;
    }

    // Sample actual surface
    std::cout << "Trying to create path guided surface:\n";

    PathGuidedSurface Surface;

    Surface.setOrigin(model_origin.width, model_origin.height);
    Surface.setSize(model_size.width, model_size.height);

    double model_diagonal = std::sqrt(model_size.width*model_size.width + model_size.length*model_size.length);

    size_t max_path_discretization = 256;
    double path_discretization_per_curve_length = static_cast<double>(max_path_discretization)/model_diagonal;
    Surface.addGuidingPathTangentVectors(path_curve_point_data, path_discretization_per_curve_length, max_path_discretization);

    Surface.setGuidingPaths();

    double width, length, height;
    length = 0;

    size_t max_orbit_discretization = 96;
    double orbit_discretization_per_orbit_length = static_cast<double>(max_orbit_discretization)/model_diagonal;
    for (size_t i = 0; i < cross_section_curve_point_data.size()/2; ++i )
    {
        width = cross_section_curve_point_data[2*i + 0];
        height = cross_section_curve_point_data[2*i + 0];
        Surface.addOrbitToSurfaceSamples(width, length, height, orbit_discretization_per_orbit_length, max_orbit_discretization);
    }

    auto surface_points = Surface.getSurfaceSamples();

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Second iteration
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* auto curve_size = path_curve_point_data.size()/2; */
    /* std::vector<double> path_w(curve_size), path_l(curve_size); */

    /* // */
    /* // Process input data, set vector-field */
    /* // */
    /* std::cout << "---> Setting solver input\n"; */ 
    /* auto getCurve = [] (const std::vector<double>& curve) -> odeSolver2D::Curve { */
    /*     size_t num_points = curve.size()/2; */
    /*     std::vector<double> path_x(num_points); */
    /*     std::vector<double> path_y(num_points); */

    /*     for (size_t i = 0; i < num_points; ++i ) */
    /*     { */
    /*         path_x[i] = curve[2*i + 0]; */
    /*         path_y[i] = curve[2*i + 1]; */
    /*         /1* std::cout << "::: Adding point: (" << path_w[i] << ", " << path_l[i] << ")\n"; *1/ */
    /*     } */

    /*     return std::make_tuple(path_x, path_y); */
    /* }; */
    /* std::tie(path_w, path_l) = getCurve(path_curve_point_data); */
    /* /1* std::tie(path_w, path_l) = odeSolver2D::sampleCurve(path_w, path_l, 32); *1/ */
    /* S.inputCurveTangentVectors(path_w, path_l, 256); */

    /* std::cout << "---> Interpolating vector field\n"; */ 
    /* if ( !S.interpolateVectorField() ) */
    /* { */
    /*     std::cout << "---> ---> Could not interpolate vector field\n"; */ 
    /*     return 1; */
    /* } */
    
    /* // */ 
    /* // Get orbits and re-sample them if necessary */
    /* // */
    /* auto get_curve_len = [](const odeSolver2D::XCoordinates &xcoords, const odeSolver2D::YCoordinates &ycoords) -> double { */
    /*     double len = 0.0; */
    /*     auto numel = xcoords.size(); */
    /*     if ( (numel < 2) || (xcoords.size() != ycoords.size()) ) */
    /*     { */
    /*         return len; */
    /*     } */

    /*     auto dist = [](const odeSolver2D::XCoordinates &xcoords, const odeSolver2D::YCoordinates &ycoords, size_t i0, size_t i1) -> double { */
    /*         double x = xcoords[i0] - xcoords[i1]; */
    /*         double y = ycoords[i0] - ycoords[i1]; */
    /*         double d = std::sqrt(x*x + y*y); */

    /*         return d; */
    /*     }; */

    /*     for (size_t i = 0; i < numel-1; ++i) */
    /*     { */
    /*         len += dist(xcoords, ycoords, i, i+1); */
    /*     } */

    /*     return len; */
    /* }; */

    /* std::vector<double> surface_points, cross_w, cross_h; */
    /* double wi, li = cross_section_depth, hi; */
    /* std::cout << "---> Sampling final surface: " << std::flush; */ 
    /* auto t0 = std::chrono::high_resolution_clock::now(); */
    /* odeSolver2D::XCoordinates orbit_w; */
    /* odeSolver2D::YCoordinates orbit_l; */
    /* std::tie(cross_w, cross_h) = getCurve(cross_section_curve_point_data); */
    /* double disc_x = 92, disc_z = 92; */
    /* double disc = std::sqrt(length_.x*length_.x + length_.z*length_.z)/std::sqrt(disc_x*disc_x + disc_z*disc_z); */
    /* double orbit_len; */
    /* size_t num_samples; */
    /* for ( size_t i = 0; i < cross_w.size(); ++i ) */
    /* { */
    /*     /1* wi = cross_section_curve_point_data[2*i + 0]; *1/ */
    /*     /1* hi = cross_section_curve_point_data[2*i + 1]; *1/ */
    /*     wi = cross_w[i]; */
    /*     hi = cross_h[i]; */

    /*     /1* std::tie(orbit_w, orbit_l) = odeSolver2D::sampleCurve(S.getOrbit(wi, li), 128); *1/ */
    /*     std::tie(orbit_w, orbit_l) = S.getOrbit(wi, li); */
    /*     std::cout << "---> ---> iteration " << i << "; orbit points: " << orbit_l.size() << "; orbit height: " << hi << "\n"; */ 
    /*     if (orbit_l.size() > 1) */
    /*     { */
    /*         orbit_len = get_curve_len(orbit_w, orbit_l); */
    /*         num_samples = std::ceil(orbit_len/disc); */
    /*         std::tie(orbit_w, orbit_l) = odeSolver2D::sampleCurve(orbit_w, orbit_l, num_samples); */
    /*         std::cout << "---> ---> iteration " << i << "; sampled points: " << orbit_l.size() << "; orbit height: " << hi << "\n"; */ 
    /*         for (size_t j = 0; j < orbit_l.size(); ++j) */
    /*         { */
    /*             surface_points.push_back(orbit_w[j]); */
    /*             surface_points.push_back(hi); */
    /*             surface_points.push_back(orbit_l[j]); */
    /*         } */
    /*     } */
    /* } */
    /* auto t1 = std::chrono::high_resolution_clock::now(); */
    /* auto dt = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count(); */
    /* std::cout << dt << " milliseconds, for " << surface_points.size()/3 << "points\n" << std::flush; */

    /* std::cout << "---> Creating final surface\n"; */ 
    /* bool success = processSurfaceCreation(surfaceCreator, surface_id, surface_points); */


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // First iteration
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* /1* auto surfaceCreator = [this]( size_t s_id, const std::vector<double> &pts ) -> bool *1/ */ 
    /* /1* { *1/ */
    /* /1*     const double fill_distance_factor = 1.0/(10.0*std::sqrt(2)); *1/ */
    /* /1*     auto sqr = [](double x) -> double { return x*x; }; *1/ */
    /* /1*     double fill_distance = std::sqrt(sqr(length_.x-origin_.x) + sqr(length_.z-origin_.z))*fill_distance_factor; *1/ */

    /* /1*     return this->modeller_.createSurface(s_id, pts, fill_distance); *1/ */
    /* /1* }; *1/ */

    /* std::cout << "Trying to create path guided surface:\n"; */ 
    /* std::cout << "---> Setting solver properties\n"; */ 
    /* odeSolver2D S; */

    /* double tol_x = 0.05 * length_.x; */
    /* double tol_z = 0.05 * length_.z; */
    /* S.setDomainOrigin(origin_.x-tol_x, origin_.z-tol_z); */
    /* S.setDomainSize(length_.x+2*tol_x, length_.z+2*tol_z); */

    /* auto curve_size = path_curve_point_data.size()/2; */
    /* std::vector<double> path_w(curve_size), path_l(curve_size); */

    /* std::cout << "---> Setting solver input\n"; */ 
    /* for (size_t i = 0; i < curve_size; ++i ) */
    /* { */
    /*     path_w[i] = path_curve_point_data[2*i + 0]; */
    /*     path_l[i] = path_curve_point_data[2*i + 1]; */
    /*     std::cout << "::: Adding point: (" << path_w[i] << ", " << path_l[i] << ")\n"; */
    /* } */
    /* S.inputCurveTangentVectors(path_w, path_l, 256); */

    /* std::cout << "---> Interpolating vector field\n"; */ 
    /* if ( !S.interpolateVectorField() ) */
    /* { */
    /*     std::cout << "---> ---> Could not interpolate vector field\n"; */ 
    /*     return 0; */
    /* } */
    
    /* std::vector<double> surface_points; */
    /* double wi, li = cross_section_depth, hi; */
    /* std::cout << "---> Sampling final surface: " << std::flush; */ 
    /* auto t0 = std::chrono::high_resolution_clock::now(); */
    /* odeSolver2D::XCoordinates orbit_w; */
    /* odeSolver2D::YCoordinates orbit_l; */
    /* for ( size_t i = 0; i < cross_section_curve_point_data.size()/2; ++i ) */
    /* { */
    /*     wi = cross_section_curve_point_data[2*i + 0]; */
    /*     hi = cross_section_curve_point_data[2*i + 1]; */

    /*     std::tie(orbit_w, orbit_l) = odeSolver2D::sampleCurve(S.getOrbit(wi, li), 128); */
    /*     std::cout << "---> ---> iteration " << i << "; sampled points: " << orbit_l.size() << "; orbit height: " << hi << "\n"; */ 
    /*     for (size_t j = 0; j < orbit_l.size(); ++j) */
    /*     { */
    /*         surface_points.push_back(orbit_w[j]); */
    /*         surface_points.push_back(hi); */
    /*         surface_points.push_back(orbit_l[j]); */
    /*     } */
    /* } */
    /* auto t1 = std::chrono::high_resolution_clock::now(); */
    /* auto dt = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count(); */
    /* std::cout << dt << " milliseconds, for " << surface_points.size()/3 << "points\n" << std::flush; */

    /* /1* std::cout << "---> Creating final surface\n"; *1/ */ 
    /* /1* bool success = processSurfaceCreation(surfaceCreator, surface_id, surface_points); *1/ */

    return 0;
}
