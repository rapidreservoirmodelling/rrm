#include "gsl_odeint_wrapper.hpp"

extern "C" {
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
}

#include <Eigen/Dense>

#include "planin/vector_field/div_free_vector_field_2d.hpp"

static DivFreeVectorField2D* sflux = nullptr;

struct odeintWrapper::Impl {

    const gsl_odeiv2_step_type* step_type = gsl_odeiv2_step_rk8pd;
    gsl_odeiv2_step* step;
    gsl_odeiv2_control* control;
    gsl_odeiv2_evolve* evolve;

};

odeintWrapper::odeintWrapper(System& system) : flux_(system), dim_(2), pimpl_(new Impl())
{
    sflux = &system;

    pimpl_->step = gsl_odeiv2_step_alloc(pimpl_->step_type, dim_);

    double eps_abs = 1e-6;
    double eps_rel = 1e-6;
    pimpl_->control = gsl_odeiv2_control_y_new(eps_abs, eps_rel);

    pimpl_->evolve = gsl_odeiv2_evolve_alloc(dim_);

}

odeintWrapper::~odeintWrapper()
{
    gsl_odeiv2_evolve_free(pimpl_->evolve);
    gsl_odeiv2_control_free(pimpl_->control);
    gsl_odeiv2_step_free(pimpl_->step);
}

static int flux (double /* t */, const double y[], double f[], void *params)
{
    double sign = *(double*)(params);

    auto F = sflux->operator()(State({y[0], y[1]}));
    f[0] = sign * F[0];
    f[1] = sign * F[1];

    return GSL_SUCCESS;
}

#include <cstdlib>
#include <cassert>

static int flux_error (double /* t */, const double[] /* y[] */, double[] /* f[] */, void* /* params */)
{
    return -1;
}

std::size_t odeintWrapper::map(State& state,
        double time0,
        double time1,
        double dt)
{
    std::size_t num_steps = 0;

    if (time0 > time1) {
        double sign = -1;
        gsl_odeiv2_system sys;

        assert(sflux == &flux_);
        if (sflux == &flux_)
        {
            sys = {flux, 0, dim_, &sign};
        }
        else
        {
            sys = {flux_error, 0, dim_, &sign};
        }

        double t = time1;

        while (t < time0)
        {
            int status = gsl_odeiv2_evolve_apply (pimpl_->evolve, pimpl_->control, pimpl_->step,
                    &sys,
                    &t, time0,
                    &dt, state.data());

            if (status != GSL_SUCCESS)
                break;

            num_steps++;
        }
    }
    else {
        double sign = 1;
        gsl_odeiv2_system sys;

        assert(sflux == &flux_);
        if (sflux == &flux_)
        {
            sys = {flux, 0, dim_, &sign};
        }
        else
        {
            sys = {flux_error, 0, dim_, &sign};
        }

        double t = time0;

        while (t < time1)
        {
            int status = gsl_odeiv2_evolve_apply (pimpl_->evolve, pimpl_->control, pimpl_->step,
                    &sys,
                    &t, time1,
                    &dt, state.data());

            if (status != GSL_SUCCESS)
                break;

            num_steps++;
        }
    }

    return num_steps;
}

std::tuple<odeintWrapper::ObservedStates, odeintWrapper::ObservedTimes> odeintWrapper::integrate(
        State state0,
        double time0,
        double time1,
        double dt)
{
    ObservedStates states{};
    ObservedTimes times{};

    if (time0 > time1) {
        double sign = -1;
        gsl_odeiv2_system sys;

        assert(sflux == &flux_);
        if (sflux == &flux_)
        {
            sys = {flux, 0, dim_, &sign};
        }
        else
        {
            sys = {flux_error, 0, dim_, &sign};
        }

        auto state = state0;
        double t = time1;

        states.push_back(state0);
        times.push_back(t);

        while (t < time0)
        {
            int status = gsl_odeiv2_evolve_apply (pimpl_->evolve, pimpl_->control, pimpl_->step,
                    &sys,
                    &t, time0,
                    &dt, state.data());

            if (status != GSL_SUCCESS)
                break;

            states.push_back(state);
            times.push_back(t);
        }
    }
    else {
        double sign = 1;
        gsl_odeiv2_system sys;

        assert(sflux == &flux_);
        if (sflux == &flux_)
        {
            sys = {flux, 0, dim_, &sign};
        }
        else
        {
            sys = {flux_error, 0, dim_, &sign};
        }

        auto state = state0;
        double t = time0;

        states.push_back(state0);
        times.push_back(t);

        while (t < time1)
        {
            int status = gsl_odeiv2_evolve_apply (pimpl_->evolve, pimpl_->control, pimpl_->step,
                    &sys,
                    &t, time1,
                    &dt, state.data());

            if (status != GSL_SUCCESS)
                break;

            states.push_back(state);
            times.push_back(t);
        }
    }

    return std::tie(states, times);
}
