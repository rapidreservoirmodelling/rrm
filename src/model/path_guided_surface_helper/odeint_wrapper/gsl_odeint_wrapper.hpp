//      Copyright Julio Daniel Machado Silva 2018 - 2019.
// Distributed under the Boost Software License, Version 1.0.
//      (See accompanying file LICENSE_1_0.txt or copy at
//           https://www.boost.org/LICENSE_1_0.txt)

#ifndef SIMPLE_GSL_ODEINT_WRAPPER
#define SIMPLE_GSL_ODEINT_WRAPPER

#include <array>
#include <memory>
#include <vector>
#include <tuple>

using State = std::array<double, 2>;

class DivFreeVectorField2D;
using System = DivFreeVectorField2D;

/// This class wraps the Runge-Kutta Prince-Dormand (8, 9) integragor of the gsl library.
///
/// It expects a `State` type for the physical state and a `System` type for the
/// vector-field, such that, for a `State x` and a `System F`, d/dt x(t) = F(x(t))
///
/// Reference:
/// Dormand, J. R.; Prince, P. J. (1980), "A family of embedded Runge-Kutta formulae",
/// Journal of Computational and Applied Mathematics", 6 (1): 19–26
///
class odeintWrapper {
   public:
    /// Type alias to hold a vector of states
    using ObservedStates = std::vector<State>;

    /// Type alias to hold a vector of times
    using ObservedTimes = std::vector<double>;

    /// Initialize the flux to be integrated with `system`
    odeintWrapper(System& system);

    /// Default dtor
    ~odeintWrapper();

    /// Map initial `state` at `time0` to a new `state` at `time1` with the flux;
    /// returns: (return value) number of steps; and (parameter) mapped `state`
    std::size_t map(State& state /**< In/Out: Initial state/Mapped state */,
                    double time0 = 0.0 /**< Initial time */,
                    double time1 = 1.0 /**< Final time */,
                    double dt = 1.0 /**< Initial discretization (scheme is adaptive) */);

    /// Integrate given flux from `state0` at `time0` to `time1` with `dt` increments;
    /// returns: tuple `std::tie(states, times)` computed during integration
    std::tuple<ObservedStates, ObservedTimes> integrate(
        State state0 /**< Initial state */,
        double time0 /**< Initial time */,
        double time1 /**< Final time */,
        double dt = 1.0 /**< Initial discretization (scheme is adaptive) */);
    /* {return std::tuple<std::vector<State>, std::vector<double>>();}; */

   private:
    System& flux_;
    const std::size_t dim_;

    struct Impl;
    std::unique_ptr<Impl> pimpl_;
};

#endif /* ifndef SIMPLE_BOOST_ODEINT_WRAPPER */
