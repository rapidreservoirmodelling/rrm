/** @license
 * RRM - Rapid Reservoir Modeling Project
 * Copyright (C) 2020
 * UofC - University of Calgary
 *
 * This file is part of RRM Software.
 *
 * RRM is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * RRM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with RRM.  If not, see <http://www.gnu.org/licenses/>,
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA.
 */

/**
 * @file smodeller_wrapper.hpp
 * @author Julio Daniel Machado Silva
 * @brief Wrapper for stratmod::SModeller class
 */

#ifndef RRMQMLVTK_SRC_MODEL_INCLUDE_MODEL_SMODELLER_WRAPPER_H
#define RRMQMLVTK_SRC_MODEL_INCLUDE_MODEL_SMODELLER_WRAPPER_H

#include <string>
#include <vector>
#include <mutex>

/* #include <Eigen/Dense> */

#include <stratmod/smodeller.hpp>

namespace model {

    /** 
     * @brief Creates consistent surface based models.
     *
     * This class is a singleton, meant to provide basic modelling functionality to
     * simplify the development of the new prototype.  Full modelling functionality
     * is available directly in the stratmod::Smodeller class.
     **/
    class SModellerWrapper
    {
    public:
        /** 
         * @brief Get reference to a SModellerWrapper singleton.  
         **/
        static SModellerWrapper& Instance();

        /** 
         * @brief Get reference to underlying SModeller singleton.  
         **/
        static SModeller& SModellerInstance();

        /**
         * @brief Clear model.
         **/
        void Clear();

        /**
         * @brief Load model from json file.
         *
         * @param filename Name of file.
         *
         * @return True if model was loaded correctly.
         **/
        bool LoadFile(const std::string& filename);

        /**
         * @brief Save model to json file.
         *
         * @param filename Name of file.
         *
         * @return True if model was saved correctly.
         **/
        bool SaveFile(const std::string& filename);

        /** @brief Force use of coordinate system such that x = width, y =
         * length, z = height.
         *
         * The use of the default coordinate system is automatically enforced
         * by this wrapper, clients of this API are not required to use this
         * method -- it is kept here as a record that the underlying modelling
         * library (stratmod) supports a different coordinate system (based on
         * OpenGL) as well.
         *
         * Within the default coordinate system, all methods that receive or
         * return vectors of triplets with geometry expect the list be
         * organized as follows:
         *
         * {x_0, y_0, z_0, x_1, y_1, z_1, ...}
         *
         * See the stratmod::SModeller class for more information.
         **/
        void useDefaultCoordinateSystem();

        /**
         * @brief Set model's bounding box origin.
         *
         * @param x,y,z Origin coordinates
         **/
        void SetOrigin(double origin_x, double origin_y, double origin_z);

        /**
         * @brief Set model's bounding box size.
         *
         * @param x,y,z Size coordinates
         *
         * @return True if size is set correctly, i.e., x, y, z > 0
         **/
        bool SetSize(double size_x, double size_y, double size_z);

        /**
         * @brief Get model's bounding box origin.
         *
         * @param x,y,z Origin coordinates
         **/
        void GetOrigin(double& origin_x, double& origin_y, double& origin_z);

        /**
         * @brief Get model's bounding box size.
         *
         * @param x,y,z Size coordinates
         **/
        void GetSize(double& size_x, double& size_y, double& size_z);

        /**
         * @brief Get list of surfaces indices in their insertion order
         **/
        std::vector<int> GetSurfaceIndices();

        /**
         * @brief Set model's current discretization.
         *
         * @param width_discretization[in] Discretization of widthwise cross-sections.
         * @param length_discretization[in] Discretization of lengthwise cross-sections.
         *
         * @return True if discretization is set correctly, i.e., width_discretization, length_discretization > 0
         **/
        bool SetDiscretization(int discretization_width, int discretization_length);

        /** 
         * @brief Get number of pieces in which the width/length directions are discretized.
         *
         * @param width_discretization[out] Discretization of widthwise cross-sections.
         * @param length_discretization[out] Discretization of lengthwise cross-sections.
         *
         * @return Discretization number
         **/
        bool GetDiscretization(int& discretization_width, int& discretization_length);

        /** 
         * @brief Set model to create test surfaces.
         *
         * Test surfaces do not modify past surfaces, they are meant to test whether a 
         * new surface is adequate before it is properly inserted in the model.
         *
         * To revert model to its original state call StopTestSurfaceInsertion().
         *
         * @sa StopTestSurfaceInsertion()
         **/
        void TestSurfaceInsertion();

        /** 
         * @brief Set model to stop creating test surfaces.
         *
         * @sa TestSurfaceInsertion()
         **/
        void StopTestSurfaceInsertion();

        /**
         * @brief Create a new surface from input points and add it to the model.
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param surface_id Index of surface.
         * @param point_data List of surface's sampled points (vector of triplets {width, length, height})
         * @param smoothing_parameter Set to 0 for interpolation, any value >0 creates an approximate interpolant
         *
         * @return True New surface has been added to the model.
         **/
        bool CreateSurface(int surface_id, const std::vector<double>& surface_points, double smoothing_factor = 0.0);

        /**
         * @brief Create a new surface linearly extruded along the model length.
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param surface_id Index of surface.
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         *
         * @return True New surface has been added to the model.
         **/
        [[deprecated("Call CreateLengthExtrudedSurface")]]
        bool CreateExtrudedSurface(int surface_id, const std::vector<double>& curve_points)
        {
            return CreateLengthExtrudedSurface(
                    surface_id, curve_points);
        }

        /**
         * @brief Create a new surface linearly extruded along the model length.
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param surface_id Index of surface.
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         *
         * @return True New surface has been added to the model.
         **/
        bool CreateLengthExtrudedSurface(int surface_id, const std::vector<double>& curve_points);

        /**
         * @brief Create a new surface linearly extruded along the model width.
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param surface_id Index of surface.
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         *
         * @return True New surface has been added to the model.
         **/
        bool CreateWidthExtrudedSurface(int surface_id, const std::vector<double>& curve_points);

        /**
         * @brief Create a new surface extruded along the given "lengthwise" path.
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param surface_id Index of surface.
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         * @param cross_section_position Position in model cross section lies
         * @param path_curve List of path's sampled points (vector of tuples {width, length})
         *
         * @return True New surface has been added to the model.
         **/
        [[deprecated("Call CreateLengthExtrudedSurface")]]
        bool CreateExtrudedSurface(int surface_id,
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve)
        {
            return CreateLengthExtrudedSurface(
                    surface_id, cross_section_curve, cross_section_position, path_curve);
        }

        /**
         * @brief Create a new surface extruded along the given "lengthwise" path.
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param surface_id Index of surface.
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         * @param cross_section_position Position in model cross section lies
         * @param path_curve List of path's sampled points (vector of tuples {width, length})
         *
         * @return True New surface has been added to the model.
         **/
        bool CreateLengthExtrudedSurface(int surface_id,
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve);

        /**
         * @brief Create a new surface extruded along the given "widthwise" path.
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param surface_id Index of surface.
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         * @param cross_section_position Position in model cross section lies
         * @param path_curve List of path's sampled points (vector of tuples {width, length})
         *
         * @return True New surface has been added to the model.
         **/
        bool CreateWidthExtrudedSurface(int surface_id,
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve);

        /* bool CreateStructuredSurface(int surface_id, const Eigen::MatrixXd& structured_data); */

        /**
         * @brief Sample a new surface as a "lengthwise path guided extrusion".
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         * @param cross_section_position Position in model cross section lies
         * @param path_curve List of path's sampled points (vector of tuples {width, length})
         *
         * @return True New surface has been added to the model.
         **/
        std::tuple<std::vector<double>, double> CreateLengthPathGuidedSurface(
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve);

        /**
         * @brief Sample a new surface as a "widthwise path guided extrusion".
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         * @param cross_section_position Position in model cross section lies
         * @param path_curve List of path's sampled points (vector of tuples {width, length})
         *
         * @return True New surface has been added to the model.
         **/
        std::tuple<std::vector<double>, double> CreateWidthPathGuidedSurface(
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve);

        /**
         * @brief Get cross section curve of a surface along the width direction.
         *
         * @param surface_id Index of surface.
         * @param cross_section_id Id of width cross-section curve lies.
         * @param vertex_coordinates Surface's vertex list (vector of pairs {length, height})
         * @param vertex_indices Surface's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool GetWidthCrossSectionCurve(
            int surface_id,
            int cross_section_id,
            std::vector<double>& vertex_coordinates,
            std::vector<std::size_t>& vertex_indices);

        /**
         * @brief Get cross section curve of a surface along the length direction.
         *
         * @param surface_id Index of surface.
         * @param width Id of length cross-section curve lies.
         * @param vertex_coordinates Surface's vertex list (vector of pairs {width, height})
         * @param vertex_indices Surface's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool GetLengthCrossSectionCurve(
            int surface_id,
            int cross_section_id,
            std::vector<double>& vertex_coordinates,
            std::vector<std::size_t>& vertex_indices);

        /**
         * @brief Get triangle mesh of a surface.
         *
         * @param surface_id Index of surface.
         * @param vertex_coordinates Surface's vertex list (vector of triplets {width, length, height})
         * @param vertex_list Surface's triangle list (vector of triplets of indices into the vertex list)
         *
         * @return True if mesh was retrieved.
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        bool GetSurfaceMesh(
            int surface_id,
            std::vector<double>& vertex_coordinates,
            std::vector<std::size_t>& vertex_indices);

        /**
         * @brief Returns true is there is at least one surface in the model that can be undone
         **/
        bool canUndo();

        /**
         * @brief Undo the last surface insertion
         *
         * @return True is undo() was performed
         **/
        bool undo();

        /**
         * @brief Returns true is there is at least one surface in the undo stack that can be restored to the model
         **/
        bool canRedo();

        /**
         * @brief Redo the last undoed surface
         *
         * @return True is redo() was performed
         **/
        bool redo();

        /**
         * @brief Apply RemoveAbove (RA) operator
         **/
        void removeAbove();

        /**
         * @brief Apply RemoveAboveIntersection (RAI) operator
         **/
        void removeAboveIntersection();

        /**
         * @brief Apply RemoveBelow (RB) operator
         **/
        void removeBelow();

        /**
         * @brief Apply RemoveBelowIntersection (RBI) operator
         **/
        void removeBelowIntersection();

        /**
         * @brief Request model to PreserveAbove selected curve
         *
         * @param sketch_points Points in sketch crossing curve bounding selected region.
         *
         * @return True if PreserveAbove suceeded.
         **/
        bool RequestPreserveAbove(const std::vector<double>& sketch_points);

        /**
         * @brief Request model to PreserveBelow selected curve
         *
         * @param sketch_points Points in sketch crossing curve bounding selected region.
         *
         * @return True if PreserveBelow suceeded.
         **/
        bool RequestPreserveBelow(const std::vector<double>& sketch_points);

        /**
         * @brief Request model to PreserveRegion containing given point 
         *
         * @param point Point lying in the region intended to be preserved.
         *
         * @return True if point lies in any of the model's regions.
         **/
        bool RequestPreserveRegion(const std::vector<double>& points);

        /**
         * @brief Returns true if PreserveAbove is active.
         **/
        bool PreserveAboveIsActive();

        /**
         * @brief Returns true if PreserveBelow is active.
         **/
        bool PreserveBelowIsActive();

        /**
         * @brief Returns true if PreserveRegion is active.
         **/
        bool PreserveRegionIsActive();

        /**
         * @brief Stop PreserveAbove.
         **/
        void StopPreserveAbove();

        /**
         * @brief Stop PreserveBelow.
         **/
        void StopPreserveBelow();

        /**
         * @brief Stop PreserveRegion.
         **/
        void StopPreserveRegion();

        /**
         * @brief Compute volumes of computed regions
         *
         * @param volume_list Vector with volume of region i at its i^{th} entry
         *
         * @return True if model has at least one region
         **/
        bool ComputeTetrahedralMeshVolumes( std::vector<double> &volume_list );

        /**
         * @brief Get curve that bounds region being PreservedAbove at a given lengthwise cross-section
         *
         * @param cross_section_id Index of current cross-section.
         * @param vlist Curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param elist Curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        [[deprecated("Call getLengthPreserveAboveCurveBox")]]
        bool getPreserveAboveCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist )
        {
            return getLengthPreserveAboveCurveBox(
                    cross_section_id, vlist, elist);
        }

        /**
         * @brief Get curve that bounds region being PreservedAbove at a given lengthwise cross-section
         *
         * @param cross_section_id Index of current cross-section.
         * @param vlist Curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param elist Curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getLengthPreserveAboveCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist );

        /**
         * @brief Get curve that bounds region being PreservedAbove at a given widthwise cross-section
         *
         * @param cross_section_id Index of current cross-section.
         * @param vlist Curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param elist Curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getWidthPreserveAboveCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist );

        /**
         * @brief Get curve that bounds region being PreservedBelow at a given lengthwise cross-section
         *
         * @param cross_section_id Index of current cross-section.
         * @param vlist Curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param elist Curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        [[deprecated("Call getLengthPreserveBelowCurveBox")]]
        bool getPreserveBelowCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist )
        {
            return getLengthPreserveBelowCurveBox(
                    cross_section_id, vlist, elist);
        }

        /**
         * @brief Get curve that bounds region being PreservedBelow at a given lengthwise cross-section
         *
         * @param cross_section_id Index of current cross-section.
         * @param vlist Curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param elist Curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getLengthPreserveBelowCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist );

        /**
         * @brief Get curve that bounds region being PreservedBelow at a given widthwise cross-section
         *
         * @param cross_section_id Index of current cross-section.
         * @param vlist Curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param elist Curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getWidthPreserveBelowCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist );

        /**
         * @brief Get curves that bound region 'region_id' at a given lengthwise cross-section
         *
         * Actural region is the intersection of the polygons bounded by the
         * lower and uper bounding boxes' curves
         *
         * @param cross_section_id Index of current cross-section.
         * @param lower_bound_box_vlist Lower bounding box curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param lower_bound_box_elist Lower bounding box curve's edge list (vector of pairs of indices into the vertex list)
         * @param upper_bound_box_vlist Upper bounding box curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param upper_bound_box_elist Upper bounding box curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        [[deprecated("Call getLengthRegionCurveBox")]]
        bool getRegionCurveBox( std::size_t region_id, std::size_t cross_section_id, 
                    std::vector<double> &lower_bound_box_vlist, std::vector<std::size_t> &lower_bound_box_elist,
                    std::vector<double> &upper_bound_box_vlist, std::vector<std::size_t> &upper_bound_box_elist )
        {
            return getLengthRegionCurveBox(
                    region_id, cross_section_id,
                    lower_bound_box_vlist, lower_bound_box_elist,
                    upper_bound_box_vlist, upper_bound_box_elist);
        }

        /**
         * @brief Get curves that bound region 'region_id' at a given lengthwise cross-section
         *
         * Actural region is the intersection of the polygons bounded by the
         * lower and uper bounding boxes' curves
         *
         * @param cross_section_id Index of current cross-section.
         * @param lower_bound_box_vlist Lower bounding box curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param lower_bound_box_elist Lower bounding box curve's edge list (vector of pairs of indices into the vertex list)
         * @param upper_bound_box_vlist Upper bounding box curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param upper_bound_box_elist Upper bounding box curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getLengthRegionCurveBox( std::size_t region_id, std::size_t cross_section_id, 
                    std::vector<double> &lower_bound_box_vlist, std::vector<std::size_t> &lower_bound_box_elist,
                    std::vector<double> &upper_bound_box_vlist, std::vector<std::size_t> &upper_bound_box_elist );

        /**
         * @brief Get curves that bound region 'region_id' at a given widthwise cross-section
         *
         * Actural region is the intersection of the polygons bounded by the
         * lower and uper bounding boxes' curves
         *
         * @param cross_section_id Index of current cross-section.
         * @param lower_bound_box_vlist Lower bounding box curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param lower_bound_box_elist Lower bounding box curve's edge list (vector of pairs of indices into the vertex list)
         * @param upper_bound_box_vlist Upper bounding box curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param upper_bound_box_elist Upper bounding box curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getWidthRegionCurveBox( std::size_t region_id, std::size_t cross_section_id, 
                    std::vector<double> &lower_bound_box_vlist, std::vector<std::size_t> &lower_bound_box_elist,
                    std::vector<double> &upper_bound_box_vlist, std::vector<std::size_t> &upper_bound_box_elist );

        /**
         * @brief Get tetrahedral meshes for all regions.
         *
         * This method retrieves the geometry of a model's regions.  The
         * `element_list` vector defines a map from the regions (indices of the
         * vector) into the regions' tetrahedral lists, in the following sense:
         *
         * element_list = [ Region0 tetraheral list, Region1 tetrahedral list, ..., RegionN tetrahedral list]
         *                           |
         *                           |--> [t0_index0, t0_index1, t0_index2, t0_index3, 
         *                                 t1_index0, t1_index1, t1_index2, t1_index3, ...] (and similarly for other regions)
         *
         *  All tetrahedral lists in `element_list` point to the `vertex_coordinates` vector.
         *
         * @param vertex_coordinates Regions' vertex list (vector of triplets {width, length, height}), the same for all regions
         * @param element_list Vector of vectors of tetrahedral lists (vector of quadruplets of indices into the vertex list)
         *
         * @return True if mesh was retrieved.
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        bool getRegionsTetrahedralMeshes(std::vector<double> &vertex_coordinates, std::vector< std::vector<std::size_t> > &element_list);

        /**
         * @brief Get list of surfaces indices in their stratigraphic order
         **/
        std::vector<std::size_t> getOrderedSurfacesIndices();

        /**
         * @brief Export model surfaces as '.irap' files.
         *
         * @param project_name Name of directory surfaces should be exported to.
         **/ 
        bool exportToIrapGrid(const std::string& project_name = "irap_surfaces");

        bool getBoundingSurfacesFromRegionID(std::size_t region_id,
                std::vector<std::size_t>& lower_bound, std::vector<std::size_t>& upper_bound)
        {
            return SModellerInstance().getBoundingSurfacesFromVolumeAttribute(region_id, lower_bound, upper_bound);
        }

        bool getModelInfCurveAtLength(std::vector<std::size_t> &surface_indices, std::size_t length,
                std::vector<double> &vlist, std::vector<std::size_t> &elist);

        bool getModelSupCurveAtLength(std::vector<std::size_t> &surface_indices, std::size_t length,
                std::vector<double> &vlist, std::vector<std::size_t> &elist);

        bool getModelInfCurveAtWidth(std::vector<std::size_t> &surface_indices, std::size_t width,
                std::vector<double> &vlist, std::vector<std::size_t> &elist);

        bool getModelSupCurveAtWidth(std::vector<std::size_t> &surface_indices, std::size_t width,
                std::vector<double> &vlist, std::vector<std::size_t> &elist);

        static void convertEntireCurveToBox(std::vector<double> &vlist, std::vector<size_t> &flist, double box_boundary_height);

        //////////////////////////////////////////////////////////////////////
        //
        // Deprecated methods: don't use them
        //
        //////////////////////////////////////////////////////////////////////

        /**
         * @brief Clear model.
         **/
        [[deprecated("Call SModellerWrapper::Clear() instead")]]
        void clear() { this->Clear(); }

        /** 
         * @brief Get number of pieces in which the width direction is discretized.
         *
         * @return Discretization number
         **/
        [[deprecated("Call SModellerWrapper::GetDiscretization() instead")]]
        std::size_t getWidthDiscretization() const { return this->model_.getWidthDiscretization(); }

        /** 
         * @brief Get number of pieces in which the length direction is discretized.
         *
         * @return Discretization number
         **/
        [[deprecated("Call SModellerWrapper::GetDiscretization() instead")]]
        std::size_t getLengthDiscretization() const { return this->model_.getLengthDiscretization(); }

        /**
         * @brief Stop PreserveAbove.
         **/
        [[deprecated("Call SModellerWrapper::StopPreserveAbove() instead")]]
        void stopPreserveAbove() { this->StopPreserveAbove(); }

        /**
         * @brief Stop PreserveBelow.
         **/
        [[deprecated("Call SModellerWrapper::StopPreserveBelow() instead")]]
        void stopPreserveBelow() { this->StopPreserveBelow(); }

        /**
         * @brief Get model's tetrahedral mesh.
         *
         * @param vertex_coordinates Model's vertex coordinates list (vector of triplets {width, length, height})
         * @param element_list Vector of tetrahedrals' indices list (vector of triplets of indices into the vertex list)
         * @param attribute_list Vector of regions tetrahedrals belong to
         *
         * @return Number of tetrahedrals in mesh.
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        [[deprecated("Why is this method being called?")]]
        std::size_t getTetrahedralMesh( std::vector<double> &vertex_coordinates, std::vector<std::size_t> &element_list, std::vector<long int> &attribute_list );

        [[deprecated("Typo: call method SModellerWrapper::GetLengthCrossSectionCurve() instead")]]
        bool GetLenghtCrossSectionCurve(
            int surface_id,
            int cross_section_id,
            std::vector<double>& vertex_coordinates,
            std::vector<std::size_t>& vertex_indices)
        {
            return GetLengthCrossSectionCurve(
                    surface_id, cross_section_id, vertex_coordinates, vertex_indices);
        }

        bool getVolumeAttributesFromPointList(const std::vector<double>& vcoords,
            std::vector<int>& regions) {
            return model_.getVolumeAttributesFromPointList(vcoords, regions);
        }

    private:
        SModellerWrapper();
        SModeller& model_;
        SModeller& model();
        std::mutex m;
    };

} // namesapce model

#endif
