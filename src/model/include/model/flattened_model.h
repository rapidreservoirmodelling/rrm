/** @license
 * RRM - Rapid Reservoir Modeling Project
 * Copyright (C) 2021
 * UofC - University of Calgary
 *
 * This file is part of RRM Software.
 *
 * RRM is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * RRM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with RRM.  If not, see <http://www.gnu.org/licenses/>,
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA.
 */

/**
 * @file flattened_model.hpp
 * @author Julio Daniel Machado Silva
 * @brief Wrapper for stratmod::FlattenedModelView class
 */

#ifndef RRMQMLVTK_SRC_MODEL_INCLUDE_MODEL_FLATTENED_MODEL_H
#define RRMQMLVTK_SRC_MODEL_INCLUDE_MODEL_FLATTENED_MODEL_H

#include <limits>
#include <string>
#include <vector>
#include <mutex>

#include "model/smodeller_wrapper.h"
#include "model/smodeller2_wrapper.h"

#include "stratmod/flattened_model_view.hpp"

namespace model {

    /** 
     * @brief Creates a flattened view of a stratigraphic model.
     *
     **/
    class FlattenedModel
    {
    public:
        /** 
         * @brief Get reference to a FlattenedModel singleton.  
         **/
        static FlattenedModel& Instance();

        //////////////////////////////////////////////////////////////////////
        //
        // Substitute calls to SModellerWrapper methods to those below
        //
        //////////////////////////////////////////////////////////////////////

        enum class ModelBBox { Global, PerCrossSection };

        bool SetRegionsBBox(ModelBBox choice = ModelBBox::Global);

        /**
         * @brief Create a new surface from input points and add it to the model.
         *
         * @param surface_id Index of surface.
         * @param point_data List of surface's sampled points (vector of triplets {width, length, height})
         * @param smoothing_parameter Set to 0 for interpolation, any value >0 creates an approximate interpolant
         *
         * @return True New surface has been added to the model.
         **/
        bool CreateSurface(int surface_id, const std::vector<double>& surface_points, double smoothing_factor = 0.0);

        /**
         * @brief Create a new surface linearly extruded along the model length.
         *
         * @param surface_id Index of surface.
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         *
         * @return True New surface has been added to the model.
         **/
        bool CreateLengthExtrudedSurface(int surface_id, const std::vector<double>& curve_points);

        /**
         * @brief Create a new surface linearly extruded along the model width.
         *
         * @param surface_id Index of surface.
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         *
         * @return True New surface has been added to the model.
         **/
        bool CreateWidthExtrudedSurface(int surface_id, const std::vector<double>& curve_points);

        /**
         * @brief Create a new surface extruded along the given "lengthwise" path.
         *
         * @param surface_id Index of surface.
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         * @param cross_section_position Position in model cross section lies
         * @param path_curve List of path's sampled points (vector of tuples {width, length})
         *
         * @return True New surface has been added to the model.
         **/
        bool CreateLengthExtrudedSurface(int surface_id,
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve);

        /**
         * @brief Create a new surface extruded along the given "widthwise" path.
         *
         * @param surface_id Index of surface.
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         * @param cross_section_position Position in model cross section lies
         * @param path_curve List of path's sampled points (vector of tuples {width, length})
         *
         * @return True New surface has been added to the model.
         **/
        bool CreateWidthExtrudedSurface(int surface_id,
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve);

        /**
         * @brief Sample a new surface as a "lengthwise path guided surface".
         *
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         * @param cross_section_position Position in model cross section lies
         * @param path_curve List of path's sampled points (vector of tuples {width, length})
         *
         * @return True New surface has been added to the model.
         **/
        bool CreateLengthPathGuidedSurface(int surface_id,
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve);

        /**
         * @brief Sample a new surface as a "widthwise path guided surface".
         *
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         * @param cross_section_position Position in model cross section lies
         * @param path_curve List of path's sampled points (vector of tuples {width, length})
         *
         * @return True New surface has been added to the model.
         **/
        bool CreateWidthPathGuidedSurface(int surface_id,
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve);

        /**
         * @brief Get cross section curve of a surface along the width direction.
         *
         * @param surface_id Index of surface.
         * @param cross_section_id Id of width cross-section curve lies.
         * @param vertex_coordinates Surface's vertex list (vector of pairs {length, height})
         * @param vertex_indices Surface's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool GetWidthCrossSectionCurve(
            int surface_id,
            int cross_section_id,
            std::vector<double>& vertex_coordinates,
            std::vector<std::size_t>& vertex_indices);

        /**
         * @brief Get cross section curve of a surface along the length direction.
         *
         * @param surface_id Index of surface.
         * @param width Id of length cross-section curve lies.
         * @param vertex_coordinates Surface's vertex list (vector of pairs {width, height})
         * @param vertex_indices Surface's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool GetLengthCrossSectionCurve(
            int surface_id,
            int cross_section_id,
            std::vector<double>& vertex_coordinates,
            std::vector<std::size_t>& vertex_indices);

        /**
         * @brief Get triangle mesh of a surface.
         *
         * @param surface_id Index of surface.
         * @param vertex_coordinates Surface's vertex list (vector of triplets {width, length, height})
         * @param vertex_list Surface's triangle list (vector of triplets of indices into the vertex list)
         *
         * @return True if mesh was retrieved.
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        bool GetSurfaceMesh(
            int surface_id,
            std::vector<double>& vertex_coordinates,
            std::vector<std::size_t>& vertex_indices);

        /**
         * @brief Request model to PreserveAbove selected curve
         *
         * @param sketch_points Points in sketch crossing curve bounding selected region.
         *
         * @return True if PreserveAbove suceeded.
         **/
        bool RequestPreserveAbove(const std::vector<double>& sketch_points);

        /**
         * @brief Request model to PreserveBelow selected curve
         *
         * @param sketch_points Points in sketch crossing curve bounding selected region.
         *
         * @return True if PreserveBelow suceeded.
         **/
        bool RequestPreserveBelow(const std::vector<double>& sketch_points);

        /**
         * @brief Request model to PreserveRegion containing given point 
         *
         * @param point Point lying in the region intended to be preserved.
         *
         * @return True if point lies in any of the model's regions.
         **/
        bool RequestPreserveRegion(const std::vector<double>& points);

        /**
         * @brief Get curve that bounds region being PreservedAbove at a given lengthwise cross-section
         *
         * @param cross_section_id Index of current cross-section.
         * @param vlist Curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param elist Curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getLengthPreserveAboveCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist );

        /**
         * @brief Get curve that bounds region being PreservedAbove at a given widthwise cross-section
         *
         * @param cross_section_id Index of current cross-section.
         * @param vlist Curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param elist Curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getWidthPreserveAboveCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist );

        /**
         * @brief Get curve that bounds region being PreservedBelow at a given lengthwise cross-section
         *
         * @param cross_section_id Index of current cross-section.
         * @param vlist Curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param elist Curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getLengthPreserveBelowCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist );

        /**
         * @brief Get curve that bounds region being PreservedBelow at a given widthwise cross-section
         *
         * @param cross_section_id Index of current cross-section.
         * @param vlist Curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param elist Curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getWidthPreserveBelowCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist );

        /**
         * @brief Get curves that bound region 'region_id' at a given lengthwise cross-section
         *
         * Actural region is the intersection of the polygons bounded by the
         * lower and uper bounding boxes' curves
         *
         * @param cross_section_id Index of current cross-section.
         * @param lower_bound_box_vlist Lower bounding box curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param lower_bound_box_elist Lower bounding box curve's edge list (vector of pairs of indices into the vertex list)
         * @param upper_bound_box_vlist Upper bounding box curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param upper_bound_box_elist Upper bounding box curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getLengthRegionCurveBox( std::size_t region_id, std::size_t cross_section_id, 
                    std::vector<double> &lower_bound_box_vlist, std::vector<std::size_t> &lower_bound_box_elist,
                    std::vector<double> &upper_bound_box_vlist, std::vector<std::size_t> &upper_bound_box_elist );

        /**
         * @brief Get curves that bound region 'region_id' at a given widthwise cross-section
         *
         * Actural region is the intersection of the polygons bounded by the
         * lower and uper bounding boxes' curves
         *
         * @param cross_section_id Index of current cross-section.
         * @param lower_bound_box_vlist Lower bounding box curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param lower_bound_box_elist Lower bounding box curve's edge list (vector of pairs of indices into the vertex list)
         * @param upper_bound_box_vlist Upper bounding box curve's vertex list (vector of pairs {abscissa, ordinate})
         * @param upper_bound_box_elist Upper bounding box curve's edge list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getWidthRegionCurveBox( std::size_t region_id, std::size_t cross_section_id, 
                    std::vector<double> &lower_bound_box_vlist, std::vector<std::size_t> &lower_bound_box_elist,
                    std::vector<double> &upper_bound_box_vlist, std::vector<std::size_t> &upper_bound_box_elist );

        /**
         * @brief Get tetrahedral meshes for all regions.
         *
         * This method retrieves the geometry of a model's regions.  The
         * `element_list` vector defines a map from the regions (indices of the
         * vector) into the regions' tetrahedral lists, in the following sense:
         *
         * element_list = [ Region0 tetraheral list, Region1 tetrahedral list, ..., RegionN tetrahedral list]
         *                           |
         *                           |--> [t0_index0, t0_index1, t0_index2, t0_index3, 
         *                                 t1_index0, t1_index1, t1_index2, t1_index3, ...] (and similarly for other regions)
         *
         *  All tetrahedral lists in `element_list` point to the `vertex_coordinates` vector.
         *
         * @param vertex_coordinates Regions' vertex list (vector of triplets {width, length, height}), the same for all regions
         * @param element_list Vector of vectors of tetrahedral lists (vector of quadruplets of indices into the vertex list)
         *
         * @return True if mesh was retrieved.
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        bool GetRegionsTetrahedralMeshes(std::vector<double> &vertex_coordinates, std::vector< std::vector<std::size_t> > &element_list);

        //////////////////////////////////////////////////////////////////////
        //
        // Main methods
        //
        //////////////////////////////////////////////////////////////////////

        /** 
         * @brief Compute average height of given curve vertices
         *
         * @param curve2d_points List of the curve vertices' coordinates: {x0, y0, x1, y1, ..., xN, yN}.
         *
         **/
        static double computeCurveAverageHeight(const std::vector<double>& curve2d_points)
        {
            double h = 0.;
            int num_samples = 0;
            for (std::size_t i = 0; i < curve2d_points.size(); ++i)
            {
                if ( i % 2 == 1 )
                {
                    h += curve2d_points[i];
                    num_samples++;
                }
            }
            return h/(num_samples > 0 ? static_cast<double>(num_samples) : 1.0);
        }

        /** 
         * @brief Compute min, max heights of given curve vertices
         *
         * @param curve2d_points List of the curve vertices' coordinates: {x0, y0, x1, y1, ..., xN, yN}.
         *
         **/
        static std::tuple<double, double> computeCurveMinMaxHeight(const std::vector<double>& curve2d_points)
        {
            double min = std::numeric_limits<double>::max();
            double max = std::numeric_limits<double>::min();
            for (std::size_t i = 0; i < curve2d_points.size(); ++i)
            {
                if ( i % 2 == 1 )
                {
                    double h = curve2d_points[i];
                    if (h < min)
                        min = h;
                    if (h > max)
                        max = h;
                }
            }
            return std::make_tuple(min, max);
        }

        /** 
         * @brief Flatten a given surface at specified height.
         **/
        bool FlattenSurface(int surface_id, double height);

        /**
         * @brief Stop flattening.
         **/
        void StopFlattening();

        /** 
         * @brief Returns true if flattened view is active.
         **/
        bool FlatteningIsActive();

        /** 
         * @brief Compute heights of the mapped model's bounding box
         **/
        void ComputeBBoxHeights(double& min_height, double& max_height);

        /** 
         * @brief Compute heights of the mapped model's bounding box at given lengthwise cross section.
         **/
        void ComputeBBoxHeightsAtLength(int length_cross_section_id, double& min_height, double& max_height);

        /** 
         * @brief Compute heights of the mapped model's bounding box at given widthwise cross section.
         **/
        void ComputeBBoxHeightsAtWidth(int width_cross_section_id, double& min_height, double& max_height);

        /** 
         * @brief Map points' coordinates given in flattened view to world coordinates.
         **/
        void MapFromFlattenedCoordinates(std::vector<double>& points);

        /** 
         * @brief Map points' world coordinates to flattened view coordinates.
         **/
        void MapToFlattenedCoordinates(std::vector<double>& points);

   private:
        using FlattenedModelView = stratmod::FlattenedModelView;

        FlattenedModelView fmview;
        bool global_regions_bbox = true;
        std::optional<double> min_height, max_height;

        FlattenedModel();

        void mapToFlattenedAtLength(std::size_t cross_section_id, std::vector<double>& points);

        void mapToFlattenedAtWidth(std::size_t cross_section_id, std::vector<double>& points);

        void getUnflattenedBBoxHeights(double& min_height, double& max_height);

        bool getModelInfCurveAtLength(std::vector<std::size_t> &surface_indices, std::size_t length,
                std::vector<double> &vlist, std::vector<std::size_t> &elist);

        bool getModelSupCurveAtLength(std::vector<std::size_t> &surface_indices, std::size_t length,
                std::vector<double> &vlist, std::vector<std::size_t> &elist);

        bool getModelInfCurveAtWidth(std::vector<std::size_t> &surface_indices, std::size_t width,
                std::vector<double> &vlist, std::vector<std::size_t> &elist);

        bool getModelSupCurveAtWidth(std::vector<std::size_t> &surface_indices, std::size_t width,
                std::vector<double> &vlist, std::vector<std::size_t> &elist);

        void GetBBoxHeights(double& min_height, double& max_height);
    };

} // namesapce model

#endif
