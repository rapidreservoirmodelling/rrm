/** @license
 * RRM - Rapid Reservoir Modeling Project
 * Copyright (C) 2021
 * UofC - University of Calgary
 *
 * This file is part of RRM Software.
 *
 * RRM is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * RRM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with RRM.  If not, see <http://www.gnu.org/licenses/>,
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA.
 */

/**
 * @file smodeller_wrapper.hpp
 * @author Julio Daniel Machado Silva
 * @brief Wrapper for stratmod::SModeller class
 */

#ifndef RRMQMLVTK_SRC_MODEL_INCLUDE_MODEL_SMODELLER2_WRAPPER_H
#define RRMQMLVTK_SRC_MODEL_INCLUDE_MODEL_SMODELLER2_WRAPPER_H

#include <string>
#include <vector>

#include <stratmod/smodeller2.hpp>

namespace model {

    template<typename R = double, typename T = int, typename S = int>
    auto mapVectorToEigen(std::vector<R>& points, T num_rows, S num_cols)
    {
        using EigenMap = Eigen::Map<Eigen::Matrix<R, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>;
        std::optional<EigenMap> map;

        int rows = static_cast<int>(num_rows);
        int cols = static_cast<int>(num_cols);

        if (points.size() >= static_cast<std::size_t>(rows * cols))
        {
            EigenMap M(points.data(), rows, cols);
            map = std::make_optional<EigenMap>(M);
        }

        return map;
    }

    template<typename R = double, typename T = int, typename S = int>
    auto mapVectorToEigen(const std::vector<R>& points, T num_rows, S num_cols)
    {
        using EigenMap = Eigen::Map<Eigen::Matrix<R, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>;
        std::optional<EigenMap> map;

        int rows = static_cast<int>(num_rows);
        int cols = static_cast<int>(num_cols);

        if (points.size() >= static_cast<std::size_t>(rows * cols))
        {
            // TODO: Fix this
            EigenMap M(const_cast<double*>(points.data()), rows, cols);
            map = std::make_optional<EigenMap>(M);
        }

        return map;
    }

    auto mapPoints3D(std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/3, 3));
    /* { */
        /* return *mapVectorToEigen(points, points.size()/3, 3); */
    /* } */

    auto mapPoints2D(std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/2, 2));
    /* { */
        /* return *mapVectorToEigen(points, points.size()/2, 2); */
    /* } */

    auto mapPoints3D(const std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/3, 3));
    /* { */
        /* return *mapVectorToEigen(points, points.size()/3, 3); */
    /* } */

    auto mapPoints2D(const std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/2, 2));
    /* { */
        /* return *mapVectorToEigen(points, points.size()/2, 2); */
    /* } */

    /** 
     * @brief Creates consistent surface based models.
     *
     * This class is a singleton, meant to provide basic modelling functionality to
     * simplify the development of the new prototype.  Full modelling functionality
     * is available directly in the stratmod::Smodeller class.
     **/
    class SModeller2Wrapper
    {
    public:
        using SurfaceUniqueIdentifier = stratmod::SurfaceUniqueIdentifier; // = std::size_t
        using SurfaceGroupIdentifier = stratmod::SurfaceGroupIdentifier; // = int

        /** 
         * @brief Default destructor.  
         **/
        ~SModeller2Wrapper() = default;

        /** 
         * @brief Get reference to a SModellerWrapper singleton.  
         **/
        static SModeller2Wrapper& Instance();

        /** 
         * @brief Get reference to underlying SModeller singleton.  
         **/
        static stratmod::SModeller2& model();

        bool AddStratigraphicSurface(stratmod::SurfaceUniqueIdentifier unique_id, const std::shared_ptr<stratmod::InputSurface>& isptr);

        bool AddFaultSurface(stratmod::SurfaceUniqueIdentifier unique_id, const std::shared_ptr<stratmod::InputSurface>& isptr);

        /**
         * @brief Create a new surface from input points
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param point_data List of surface's sampled points (vector of triplets {width, length, height})
         * @param smoothing_parameter Set to 0 for interpolation, any value >0 creates an approximate interpolant
         *
         * @return Pointer to new surface.
         **/
        std::shared_ptr<stratmod::InputSurface> CreateSurface(const std::vector<double>& surface_points, double smoothing_factor = 0.0);

        /**
         * @brief Create a new surface linearly extruded along the model length.
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         *
         * @return Pointer to new surface.
         **/
        std::shared_ptr<stratmod::InputSurface> CreateLengthExtrudedSurface(const std::vector<double>& curve_points);

        /**
         * @brief Create a new surface linearly extruded along the model width.
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         *
         * @return Pointer to new surface.
         **/
        std::shared_ptr<stratmod::InputSurface> CreateWidthExtrudedSurface(const std::vector<double>& curve_points);

        /**
         * @brief Create a new surface linearly extruded along the model length.
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         * @param cross_section_position Position in model cross section lies
         * @param path_curve List of path's sampled points (vector of tuples {width, length})
         *
         * @return Pointer to new surface.
         **/
        std::shared_ptr<stratmod::InputSurface> CreateLengthExtrudedSurface(
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve);

        /**
         * @brief Create a new surface linearly extruded along the model width.
         *
         * Creates an approximate interpolant as defined in paper:
         * WENDLAND H., RIEGER C., Approximate interpolation with applications 
         * to selecting smoothing parameters. Numerische Mathematik 101, 4 (2005), 
         * 729–748. 5
         *
         * See section 3 (subsection 3.2 in particular) of the paper for the criteria 
         * to pick the smoothing parameter (represented as the greek letter lambda in
         * the paper).  
         *
         * @param cross_section_curve_point_data List of surface's sampled points (vector of tuples {width, height})
         * @param cross_section_position Position in model cross section lies
         * @param path_curve List of path's sampled points (vector of tuples {width, length})
         *
         * @return Pointer to new surface.
         **/
        std::shared_ptr<stratmod::InputSurface> CreateWidthExtrudedSurface(
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve);


    private:
        SModeller2Wrapper() = default;
        stratmod::SModeller2& model_ = stratmod::SModeller2::Instance();
    };

} // namesapce model

#endif
