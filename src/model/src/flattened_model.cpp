/** @license
 * RRM - Rapid Reservoir Modeling Project
 * Copyright (C) 2021
 * UofC - University of Calgary
 *
 * This file is part of RRM Software.
 *
 * RRM is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * RRM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with RRM.  If not, see <http://www.gnu.org/licenses/>,
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA.
 */

/**
 * @file flattened_model.cpp
 * @author Julio Daniel Machado Silva
 * @brief Wrapper for stratmod::FlattenedModelView class
 */

#include "model/flattened_model.h"

namespace model {

    void FlattenedModel::mapToFlattenedAtLength(std::size_t cross_section_id, std::vector<double>& points)
    {
        if (FlatteningIsActive())
        {
            auto M = *mapPoints2D(points);

            double ox, oy, oz;
            SModellerWrapper::Instance().GetOrigin(ox, oy, oz);

            double sx, sy, sz;
            SModellerWrapper::Instance().GetSize(sx, sy, sz);

            int disc_width, disc_length;
            SModellerWrapper::Instance().GetDiscretization(disc_width, disc_length);

            double length = static_cast<double>(cross_section_id)/disc_length * sy + oy;
            Eigen::MatrixXd P(M.rows(), 3);
            P << M.col(0), length * Eigen::MatrixXd::Ones(M.rows(), 1), M.col(1);

            fmview.mapToFlattenedCoordinates(P);
            M.col(1) = P.col(2);
        }
    }

    void FlattenedModel::mapToFlattenedAtWidth(std::size_t cross_section_id, std::vector<double>& points)
    {
        if (FlatteningIsActive())
        {
            auto M = *mapPoints2D(points);

            double ox, oy, oz;
            SModellerWrapper::Instance().GetOrigin(ox, oy, oz);

            double sx, sy, sz;
            SModellerWrapper::Instance().GetSize(sx, sy, sz);

            int disc_width, disc_length;
            SModellerWrapper::Instance().GetDiscretization(disc_width, disc_length);

            double width = static_cast<double>(cross_section_id)/disc_width * sx + ox;
            Eigen::MatrixXd P(M.rows(), 3);
            P << width * Eigen::MatrixXd::Ones(M.rows(), 1), M.col(0), M.col(1);

            fmview.mapToFlattenedCoordinates(P);
            M.col(1) = P.col(2);
        }
    }

    void FlattenedModel::getUnflattenedBBoxHeights(double& min_height, double& max_height)
    {
        double ox, oy, oz;
        SModellerWrapper::Instance().GetOrigin(ox, oy, oz);
        double sx, sy, sz;
        SModellerWrapper::Instance().GetSize(sx, sy, sz);

        min_height = oz;
        max_height = oz + sz;
    }

    bool FlattenedModel::getModelInfCurveAtLength(std::vector<std::size_t> &surface_indices, std::size_t length,
            std::vector<double> &vlist, std::vector<std::size_t> &elist)
    {
        bool success = SModellerWrapper::Instance().getModelInfCurveAtLength(surface_indices, length, vlist, elist);
        mapToFlattenedAtLength(length, vlist);
        return success;
    }

    bool FlattenedModel::getModelSupCurveAtLength(std::vector<std::size_t> &surface_indices, std::size_t length,
            std::vector<double> &vlist, std::vector<std::size_t> &elist)
    {
        bool success = SModellerWrapper::Instance().getModelSupCurveAtLength(surface_indices, length, vlist, elist);
        mapToFlattenedAtLength(length, vlist);
        return success;
    }

    bool FlattenedModel::getModelInfCurveAtWidth(std::vector<std::size_t> &surface_indices, std::size_t width,
            std::vector<double> &vlist, std::vector<std::size_t> &elist)
    {
        bool success = SModellerWrapper::Instance().getModelInfCurveAtWidth(surface_indices, width, vlist, elist);
        mapToFlattenedAtWidth(width, vlist);
        return success;
    }

    bool FlattenedModel::getModelSupCurveAtWidth(std::vector<std::size_t> &surface_indices, std::size_t width,
            std::vector<double> &vlist, std::vector<std::size_t> &elist)
    {
        bool success = SModellerWrapper::Instance().getModelSupCurveAtWidth(surface_indices, width, vlist, elist);
        mapToFlattenedAtWidth(width, vlist);
        return success;
    }

    FlattenedModel::FlattenedModel() = default;

    FlattenedModel& FlattenedModel::Instance()
    {
        static FlattenedModel instance;

        return instance;
    }

    bool FlattenedModel::FlattenSurface(int surface_id, double height)
    {
        auto fmv = FlattenedModelView::Get(surface_id, height);
        if (fmv.has_value())
        {
            fmview = std::move(*fmv);

            double minh, maxh;
            ComputeBBoxHeights(minh, maxh);
            std::cout << "\nMin height: " << minh << ", max height: " << maxh << "\n\n" << std::endl;
        }

        return FlatteningIsActive();
    }

    void FlattenedModel::StopFlattening()
    {
        fmview = std::move(FlattenedModelView());
        min_height = std::optional<double>();
        max_height = std::optional<double>();
    }

    bool FlattenedModel::FlatteningIsActive()
    {
        return !fmview.expired();
    }

    void FlattenedModel::MapFromFlattenedCoordinates(std::vector<double>& points)
    {
        fmview.mapFromFlattenedCoordinates(*mapPoints3D(points));
        return;
    }

    void FlattenedModel::MapToFlattenedCoordinates(std::vector<double>& points)
    {
        fmview.mapToFlattenedCoordinates(*mapPoints3D(points));
        return;
    }

    void FlattenedModel::ComputeBBoxHeights(double& min_height, double& max_height)
    {
        getUnflattenedBBoxHeights(min_height, max_height);
        if (FlatteningIsActive())
        {
            std::vector<double> vlist;
            std::vector<std::size_t> tlist;
            SModellerWrapper::Instance().GetSurfaceMesh(*fmview.flattenedSurfaceIndex(), vlist, tlist);
            auto M = *mapPoints3D(vlist);

            M.col(2) = min_height * Eigen::MatrixXd::Ones(M.rows(), 1);
            fmview.mapToFlattenedCoordinates(M);
            min_height = M.col(2).minCoeff();

            M.col(2) = max_height * Eigen::MatrixXd::Ones(M.rows(), 1);
            fmview.mapToFlattenedCoordinates(M);
            max_height = M.col(2).maxCoeff();
        }

        return;
    }

   void FlattenedModel::ComputeBBoxHeightsAtLength(int length_cross_section_id, double& min_height, double& max_height)
    {
        int max_length, max_width;
        SModellerWrapper::Instance().GetDiscretization(max_width, max_length);

        if ((length_cross_section_id < 0) || (length_cross_section_id > max_length))
        {
            return;
        }

        getUnflattenedBBoxHeights(min_height, max_height);
        if (FlatteningIsActive())
        {
            std::vector<double> vlist;
            std::vector<std::size_t> tlist;
            SModellerWrapper::Instance().GetLenghtCrossSectionCurve(*fmview.flattenedSurfaceIndex(),
                    length_cross_section_id, vlist, tlist);
            auto M = *mapPoints2D(vlist);

            M.col(1) = min_height * Eigen::MatrixXd::Ones(M.rows(), 1);
            fmview.mapToFlattenedCoordinates(M);
            min_height = M.col(1).minCoeff();
            this->min_height = min_height;

            M.col(1) = max_height * Eigen::MatrixXd::Ones(M.rows(), 1);
            fmview.mapToFlattenedCoordinates(M);
            max_height = M.col(1).maxCoeff();
            this->max_height = max_height;
        }

        return;
    }

   void FlattenedModel::GetBBoxHeights(double& min_height, double& max_height)
   {
       /* if (FlatteningIsActive() && this->min_height.has_value() && this->max_height.has_value()) */
       /* { */
           /* min_height = *(this->min_height); */
           /* max_height = *(this->max_height); */
       /* } */
       /* else */
       {
           ComputeBBoxHeights(min_height, max_height);
       }
   }

   void FlattenedModel::ComputeBBoxHeightsAtWidth(int width_cross_section_id, double& min_height, double& max_height)
    {
        int max_length, max_width;
        SModellerWrapper::Instance().GetDiscretization(max_width, max_length);

        if ((width_cross_section_id < 0) || (width_cross_section_id > max_width))
        {
            return;
        }

        getUnflattenedBBoxHeights(min_height, max_height);
        if (FlatteningIsActive())
        {
            std::vector<double> vlist;
            std::vector<std::size_t> tlist;
            SModellerWrapper::Instance().GetWidthCrossSectionCurve(*fmview.flattenedSurfaceIndex(),
                    width_cross_section_id, vlist, tlist);
            auto M = *mapPoints2D(vlist);

            M.col(1) = min_height * Eigen::MatrixXd::Ones(M.rows(), 1);
            fmview.mapToFlattenedCoordinates(M);
            min_height = M.col(1).minCoeff();

            M.col(1) = max_height * Eigen::MatrixXd::Ones(M.rows(), 1);
            fmview.mapToFlattenedCoordinates(M);
            max_height = M.col(1).maxCoeff();
        }

        return;
    }

    bool FlattenedModel::SetRegionsBBox(ModelBBox choice)
    {
        if (choice == ModelBBox::Global)
        {
            global_regions_bbox = true;
        }
        else if (choice == ModelBBox::PerCrossSection)
        {
            global_regions_bbox = false;
        }
        else {
            return false;
        }

        return true;
    }

    bool FlattenedModel::CreateSurface(int surface_id, const std::vector<double>& surface_points, double smoothing_factor)
    {
        if (!FlatteningIsActive())
        {
            return SModellerWrapper::Instance().CreateSurface(surface_id, surface_points, smoothing_factor);
        }

        std::shared_ptr<stratmod::InputSurface> isptr = SModeller2Wrapper::Instance().CreateSurface(surface_points, smoothing_factor);
        isptr = fmview.unFlattenSurface(isptr);

        return SModeller2Wrapper::Instance().AddStratigraphicSurface(surface_id, isptr);
    }

    bool FlattenedModel::CreateLengthExtrudedSurface(int surface_id, const std::vector<double>& curve_points)
    {
        if (!FlatteningIsActive())
        {
            return SModellerWrapper::Instance().CreateLengthExtrudedSurface(surface_id, curve_points);
        }

        std::shared_ptr<stratmod::InputSurface> isptr = SModeller2Wrapper::Instance().CreateLengthExtrudedSurface(curve_points);
        isptr = fmview.unFlattenSurface(isptr);

        return SModeller2Wrapper::Instance().AddStratigraphicSurface(surface_id, isptr);
    }

    bool FlattenedModel::CreateWidthExtrudedSurface(int surface_id, const std::vector<double>& curve_points)
    {
        if (!FlatteningIsActive())
        {
            return SModellerWrapper::Instance().CreateWidthExtrudedSurface(surface_id, curve_points);
        }

        std::shared_ptr<stratmod::InputSurface> isptr = SModeller2Wrapper::Instance().CreateWidthExtrudedSurface(curve_points);
        isptr = fmview.unFlattenSurface(isptr);

        return SModeller2Wrapper::Instance().AddStratigraphicSurface(surface_id, isptr);
    }

    bool FlattenedModel::CreateLengthExtrudedSurface(int surface_id,
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve)
    {
        std::shared_ptr<stratmod::InputSurface> isptr = SModeller2Wrapper::Instance().CreateLengthExtrudedSurface(cross_section_curve, cross_section_position, path_curve);

        if (FlatteningIsActive())
        {
            isptr = fmview.unFlattenSurface(isptr);
        }

        return SModeller2Wrapper::Instance().AddStratigraphicSurface(surface_id, isptr);
    }

    bool FlattenedModel::CreateWidthExtrudedSurface(int surface_id,
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve)
    {
        std::shared_ptr<stratmod::InputSurface> isptr = SModeller2Wrapper::Instance().CreateWidthExtrudedSurface(cross_section_curve, cross_section_position, path_curve);

        if (FlatteningIsActive())
        {
            isptr = fmview.unFlattenSurface(isptr);
        }

        return SModeller2Wrapper::Instance().AddStratigraphicSurface(surface_id, isptr);
    }

    bool FlattenedModel::CreateLengthPathGuidedSurface(int surface_id,
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve)
    {
        auto [surface_points, smoothing_factor] = SModellerWrapper::Instance().CreateLengthPathGuidedSurface(
                cross_section_curve, cross_section_position, path_curve);

        std::shared_ptr<stratmod::InputSurface> isptr = SModeller2Wrapper::Instance().CreateSurface(surface_points, smoothing_factor);

        if (FlatteningIsActive())
        {
            isptr = fmview.unFlattenSurface(isptr);
        }

        return SModeller2Wrapper::Instance().AddStratigraphicSurface(surface_id, isptr);
    }

    bool FlattenedModel::CreateWidthPathGuidedSurface(int surface_id,
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve)
    {
        auto [surface_points, smoothing_factor] = SModellerWrapper::Instance().CreateWidthPathGuidedSurface(
                cross_section_curve, cross_section_position, path_curve);

        std::shared_ptr<stratmod::InputSurface> isptr = SModeller2Wrapper::Instance().CreateSurface(surface_points, smoothing_factor);

        if (FlatteningIsActive())
        {
            isptr = fmview.unFlattenSurface(isptr);
        }

        return SModeller2Wrapper::Instance().AddStratigraphicSurface(surface_id, isptr);
    }

    bool FlattenedModel::GetWidthCrossSectionCurve(
            int surface_id,
            int cross_section_id,
            std::vector<double>& vertex_coordinates,
            std::vector<std::size_t>& vertex_indices)
    {
        bool success = SModellerWrapper::Instance().GetWidthCrossSectionCurve(
                surface_id, cross_section_id, vertex_coordinates, vertex_indices);

        mapToFlattenedAtWidth(cross_section_id, vertex_coordinates);

        return success;
    }

    bool FlattenedModel::GetLengthCrossSectionCurve(
            int surface_id,
            int cross_section_id,
            std::vector<double>& vertex_coordinates,
            std::vector<std::size_t>& vertex_indices)
    {
        bool success = SModellerWrapper::Instance().GetLengthCrossSectionCurve(
                surface_id, cross_section_id, vertex_coordinates, vertex_indices);

        mapToFlattenedAtLength(cross_section_id, vertex_coordinates);

        return success;
    }

    bool FlattenedModel::GetSurfaceMesh(
            int surface_id,
            std::vector<double>& vertex_coordinates,
            std::vector<std::size_t>& vertex_indices)
    {
        bool status = SModellerWrapper::Instance().GetSurfaceMesh(surface_id, vertex_coordinates, vertex_indices);

        MapToFlattenedCoordinates(vertex_coordinates);

        return status;
    }

    bool FlattenedModel::RequestPreserveAbove(const std::vector<double>& sketch_points)
    {
        auto points = sketch_points;
        MapFromFlattenedCoordinates(points);
        return SModellerWrapper::Instance().RequestPreserveAbove(points);
    }

    bool FlattenedModel::RequestPreserveBelow(const std::vector<double>& sketch_points)
    {
        auto points = sketch_points;
        MapFromFlattenedCoordinates(points);
        return SModellerWrapper::Instance().RequestPreserveBelow(points);
    }

    bool FlattenedModel::RequestPreserveRegion(const std::vector<double>& sketch_points)
    {
        auto points = sketch_points;
        MapFromFlattenedCoordinates(points);
        return SModellerWrapper::Instance().RequestPreserveRegion(points);
    }

    bool FlattenedModel::getLengthPreserveAboveCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist )
    {
        if (!FlatteningIsActive())
        {
            return SModellerWrapper::Instance().getLengthPreserveAboveCurveBox(
                    cross_section_id, vlist, elist);
        }

        std::vector<size_t> surface_indices;
        if ( SModellerWrapper::SModellerInstance().preserveAboveIsActive(surface_indices) == false )
        {
            return false;
        }

        bool success = getModelSupCurveAtLength(surface_indices, cross_section_id, vlist, elist);
        if ( !success )
        {
            return false;
        }

        double min_height, max_height;
        if (global_regions_bbox)
        {
            GetBBoxHeights(min_height, max_height);
        }
        else
        {
            ComputeBBoxHeightsAtLength(cross_section_id, min_height, max_height);
        }

        SModellerWrapper::convertEntireCurveToBox(vlist, elist, max_height);

        return true;
    }

    bool FlattenedModel::getWidthPreserveAboveCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist )
    {
        if (!FlatteningIsActive())
        {
            return SModellerWrapper::Instance().getWidthPreserveAboveCurveBox(
                    cross_section_id, vlist, elist);
        }

        std::vector<size_t> surface_indices;
        if ( SModellerWrapper::SModellerInstance().preserveAboveIsActive(surface_indices) == false )
        {
            return false;
        }

        bool success = getModelSupCurveAtWidth(surface_indices, cross_section_id, vlist, elist);
        if ( !success )
        {
            return false;
        }

        double min_height, max_height;
        if (global_regions_bbox)
        {
            GetBBoxHeights(min_height, max_height);
        }
        else
        {
            ComputeBBoxHeightsAtWidth(cross_section_id, min_height, max_height);
        }

        SModellerWrapper::convertEntireCurveToBox(vlist, elist, max_height);

        return true;
    }

    bool FlattenedModel::getLengthPreserveBelowCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist )
    {
        if (!FlatteningIsActive())
        {
            return SModellerWrapper::Instance().getLengthPreserveBelowCurveBox(
                    cross_section_id, vlist, elist);
        }

        std::vector<size_t> surface_indices;
        if ( SModellerWrapper::SModellerInstance().preserveBelowIsActive(surface_indices) == false )
        {
            return false;
        }

        bool success = getModelInfCurveAtLength(surface_indices, cross_section_id, vlist, elist);
        if ( !success )
        {
            return false;
        }

        double min_height, max_height;
        if (global_regions_bbox)
        {
            GetBBoxHeights(min_height, max_height);
        }
        else
        {
            ComputeBBoxHeightsAtLength(cross_section_id, min_height, max_height);
        }

        SModellerWrapper::convertEntireCurveToBox(vlist, elist, min_height);

        return true;
    }

    bool FlattenedModel::getWidthPreserveBelowCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &elist )
    {
        if (!FlatteningIsActive())
        {
            return SModellerWrapper::Instance().getWidthPreserveBelowCurveBox(
                    cross_section_id, vlist, elist);
        }

        std::vector<size_t> surface_indices;
        if ( SModellerWrapper::SModellerInstance().preserveBelowIsActive(surface_indices) == false )
        {
            return false;
        }

        bool success = getModelInfCurveAtWidth(surface_indices, cross_section_id, vlist, elist);
        if ( !success )
        {
            return false;
        }

        double min_height, max_height;
        if (global_regions_bbox)
        {
            GetBBoxHeights(min_height, max_height);
        }
        else
        {
            ComputeBBoxHeightsAtWidth(cross_section_id, min_height, max_height);
        }

        SModellerWrapper::convertEntireCurveToBox(vlist, elist, min_height);

        return true;
    }

    bool FlattenedModel::getLengthRegionCurveBox( std::size_t region_id, std::size_t cross_section_id, 
            std::vector<double> &lower_bound_box_vlist, std::vector<std::size_t> &lower_bound_box_elist,
            std::vector<double> &upper_bound_box_vlist, std::vector<std::size_t> &upper_bound_box_elist )
    {
        if (!FlatteningIsActive())
        {
            return SModellerWrapper::Instance().getLengthRegionCurveBox(
                    region_id, cross_section_id,
                    lower_bound_box_vlist, lower_bound_box_elist,
                    upper_bound_box_vlist, upper_bound_box_elist);
        }

        std::vector<size_t> lower_bound, upper_bound; 
        bool success = SModellerWrapper::Instance().getBoundingSurfacesFromRegionID(region_id, lower_bound, upper_bound);
        if ( !success )
        {
            return false;
        }

        double min_height, max_height;
        if (global_regions_bbox)
        {
            GetBBoxHeights(min_height, max_height);
        }
        else
        {
            ComputeBBoxHeightsAtLength(cross_section_id, min_height, max_height);
        }

        success = getModelSupCurveAtLength(lower_bound, cross_section_id, lower_bound_box_vlist, lower_bound_box_elist);
        if ( !success )
        {
            return false;
        }

        SModellerWrapper::convertEntireCurveToBox(lower_bound_box_vlist, lower_bound_box_elist, max_height);

        success = getModelInfCurveAtLength(upper_bound, cross_section_id, upper_bound_box_vlist, upper_bound_box_elist);
        if ( !success )
        {
            return false;
        }

        SModellerWrapper::convertEntireCurveToBox(upper_bound_box_vlist, upper_bound_box_elist, min_height);

        return success;
    }

    bool FlattenedModel::getWidthRegionCurveBox( std::size_t region_id, std::size_t cross_section_id, 
            std::vector<double> &lower_bound_box_vlist, std::vector<std::size_t> &lower_bound_box_elist,
            std::vector<double> &upper_bound_box_vlist, std::vector<std::size_t> &upper_bound_box_elist )
    {
        if (!FlatteningIsActive())
        {
            return SModellerWrapper::Instance().getWidthRegionCurveBox(
                    region_id, cross_section_id,
                    lower_bound_box_vlist, lower_bound_box_elist,
                    upper_bound_box_vlist, upper_bound_box_elist);
        }

        std::vector<size_t> lower_bound, upper_bound; 

        bool success = SModellerWrapper::Instance().getBoundingSurfacesFromRegionID(region_id, lower_bound, upper_bound);
        if ( !success )
        {
            return false;
        }

        double min_height, max_height;
        if (global_regions_bbox)
        {
            GetBBoxHeights(min_height, max_height);
        }
        else
        {
            ComputeBBoxHeightsAtLength(cross_section_id, min_height, max_height);
        }

        success = getModelSupCurveAtWidth(lower_bound, cross_section_id, lower_bound_box_vlist, lower_bound_box_elist);
        if ( !success )
        {
            return false;
        }

        SModellerWrapper::convertEntireCurveToBox(lower_bound_box_vlist, lower_bound_box_elist, max_height);

        success = getModelInfCurveAtWidth(upper_bound, cross_section_id, upper_bound_box_vlist, upper_bound_box_elist);
        if ( !success )
        {
            return false;
        }

        SModellerWrapper::convertEntireCurveToBox(upper_bound_box_vlist, upper_bound_box_elist, min_height);

        return success;
    }

    bool FlattenedModel::GetRegionsTetrahedralMeshes(std::vector<double> &vertex_coordinates, std::vector< std::vector<std::size_t> > &element_list)
    {
        bool status = SModellerWrapper::Instance().getRegionsTetrahedralMeshes(vertex_coordinates, element_list);

        MapToFlattenedCoordinates(vertex_coordinates);

        return status;
    }

    //////////////////////////////////////////////////////////////////////
    //
    // Deprecated methods: don't use them
    //
    //////////////////////////////////////////////////////////////////////


} // namesapce model
