/** @license
 * RRM - Rapid Reservoir Modeling Project
 * Copyright (C) 2020
 * UofC - University of Calgary
 *
 * This file is part of RRM Software.
 *
 * RRM is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * RRM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with RRM.  If not, see <http://www.gnu.org/licenses/>,
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA.
 */

/**
 * @file smodeller_wrapper.cpp
 * @author Julio Daniel Machado Silva
 * @brief Wrapper for stratmod::SModeller class
 */

#include "model/smodeller_wrapper.h"

#include "rules_processor.hpp"
#include "stratmod/sutilities.hpp"

#if defined(MODEL_WITH_PATH_GUIDED_SURFACES)
#include "path_guided_surface.hpp"
#endif

namespace model {
    static RulesProcessor& rp = RulesProcessor::Instance();

    /////////////////////////////////////////////////////////////////
    // New code to access model
    /////////////////////////////////////////////////////////////////

    /* stratmod::SModeller& SModellerWrapper::model_ = stratmod::SModeller::Instance(); */

    SModellerWrapper::SModellerWrapper() : model_(stratmod::SModeller::Instance())
    {
        model_.clear();
        model_.useDefaultCoordinateSystem();
        //model_.changeDiscretization(32, 32);
    }

    SModellerWrapper& SModellerWrapper::Instance()
    {
        static SModellerWrapper unique_instance;
        unique_instance.SModellerInstance().useDefaultCoordinateSystem();

        return unique_instance;
    }

    SModeller& SModellerWrapper::SModellerInstance()
    {
        return stratmod::SModeller::Instance();
    }

    void SModellerWrapper::Clear()
    {
        SModellerInstance().useDefaultCoordinateSystem();
        rp.clear();
        //model_.changeDiscretization(32, 32);
    }

    bool SModellerWrapper::LoadFile(const std::string& filename)
    {
        bool success = rp.loadFile(filename);
        model_.useDefaultCoordinateSystem();
        //model_.changeDiscretization(32, 32);
        return success;
    }

    bool SModellerWrapper::SaveFile(const std::string& filename)
    {
        model_.useDefaultCoordinateSystem();
        return rp.saveFile(filename);
    }

    void SModellerWrapper::useDefaultCoordinateSystem()
    {
        model_.useDefaultCoordinateSystem();
    }

    void SModellerWrapper::SetOrigin(double origin_x, double origin_y, double origin_z)
    {
        model_.useDefaultCoordinateSystem();
        /* model_.setOrigin(origin_x, origin_y, origin_z); */
        rp.setOrigin(origin_x, origin_y, origin_z);
    }

    bool SModellerWrapper::SetSize(double size_x, double size_y, double size_z)
    {
        model_.useDefaultCoordinateSystem();
        /* return model_.setSize(size_x, size_y, size_z); */
        return rp.setLenght(size_x, size_y, size_z);
    }

    void SModellerWrapper::GetOrigin(double& origin_x, double& origin_y, double& origin_z)
    {
        model_.useDefaultCoordinateSystem();
        model_.getOrigin(origin_x, origin_y, origin_z);
    }

    void SModellerWrapper::GetSize(double& size_x, double& size_y, double& size_z)
    {
        model_.useDefaultCoordinateSystem();
        model_.getSize(size_x, size_y, size_z);
    }

    std::vector<int> SModellerWrapper::GetSurfaceIndices()
    {
        std::vector<int> ids;

        auto stored_ids = model_.getSurfacesIndices();
        ids.resize(stored_ids.size());

        for (size_t i = 0; i < stored_ids.size(); ++i)
        {
            ids[i] = static_cast<int>(stored_ids[i]);
        }

        return ids;
    }

    bool SModellerWrapper::SetDiscretization(int discretization_width, int discretization_length)
    {
        return model_.changeDiscretization(static_cast<int>(discretization_width), static_cast<int>(discretization_length));
    }

    bool SModellerWrapper::GetDiscretization(int& discretization_width, int& discretization_length)
    {
        discretization_width = static_cast<int>(model_.getWidthDiscretization());
        discretization_length = static_cast<int>(model_.getLengthDiscretization());

        return true;
    }

    void SModellerWrapper::TestSurfaceInsertion()
    {
        rp.testSurfaceInsertion();
    }

    void SModellerWrapper::StopTestSurfaceInsertion()
    {
        rp.stopTestSurfaceInsertion();
    }

    bool SModellerWrapper::CreateLengthExtrudedSurface(int surface_id, const std::vector<double>& curve_points)
    {
        model_.useDefaultCoordinateSystem();
        /* return model_.createLengthwiseExtrudedSurface(static_cast<size_t>(surface_id), curve_points); */
        return rp.createLengthwiseExtrudedSurface(static_cast<size_t>(surface_id), curve_points);
    }

    bool SModellerWrapper::CreateWidthExtrudedSurface(int surface_id, const std::vector<double>& curve_points)
    {
        model_.useDefaultCoordinateSystem();
        /* return model_.createLengthwiseExtrudedSurface(static_cast<size_t>(surface_id), curve_points); */
        return rp.createWidthwiseExtrudedSurface(static_cast<size_t>(surface_id), curve_points);
    }

    /* bool SModellerWrapper::CreateStructuredSurface(int surface_id, const Eigen::MatrixXd& structured_data) */
    /* { */
    /*     double origin_x, origin_y, origin_z; */
    /*     double size_x, size_y, size_z; */
    /*     GetOrigin(origin_x, origin_y, origin_z); */
    /*     GetSize(size_x, size_y, size_z); */

    /*     auto steps_x = static_cast<int>(structured_data.rows()); */
    /*     auto steps_y = static_cast<int>(structured_data.cols()); */

    /*     if ( (steps_x < 2) || (steps_y < 2) ) */
    /*     { */
    /*         return false; */
    /*     } */

    /*     double dx = size_x/static_cast<double>(steps_x - 1); */
    /*     Eigen::Vector2d inc_x(dx, 0.); */

    /*     double dy = size_y/static_cast<double>(steps_y - 1); */
    /*     Eigen::Vector2d inc_y(0., dy); */

    /*     Eigen::Vector2d p(origin_x, origin_y); */
    /*     std::vector<double> surface_points; */

    /*     for ( int j = 0; j < steps_y; ++j ) */
    /*     { */
    /*         for ( int i = 0; i < steps_x; ++i ) */
    /*         { */
    /*             surface_points.push_back(p[0]); */
    /*             surface_points.push_back(p[1]); */
    /*             surface_points.push_back(structured_data(i,j)); */

    /*             p += inc_x; */
    /*         } */

    /*         p += inc_y; */
    /*     } */

    /*     return CreateSurface(surface_id, surface_points, 0.); */
    /* } */

    bool SModellerWrapper::CreateSurface(int surface_id, const std::vector<double>& surface_points, double /* smoothing_factor */)
    {
        model_.useDefaultCoordinateSystem();
        /* return model_.createSurface(static_cast<size_t>(surface_id), surface_points); //, smoothing_factor); */
        return rp.createSurface(static_cast<size_t>(surface_id), surface_points); //, smoothing_factor);
    }

    bool SModellerWrapper::CreateLengthExtrudedSurface(int surface_id,
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve)
    {
        model_.useDefaultCoordinateSystem();
#if defined(MODEL_WITH_PATH_GUIDED_SURFACES)
        auto [surface_points, smoothing_factor] = CreateLengthPathGuidedSurface(cross_section_curve, cross_section_position, path_curve);
        return rp.createSurface(static_cast<size_t>(surface_id), surface_points, smoothing_factor);
#else // #if defined MODEL_WITH_PATH_GUIDED_SURFACES
        return rp.createLengthwiseExtrudedSurface(static_cast<std::size_t>(surface_id), cross_section_curve, cross_section_position, path_curve);
#endif // #if defined MODEL_WITH_PATH_GUIDED_SURFACES
    }

    bool SModellerWrapper::CreateWidthExtrudedSurface(int surface_id,
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve)
    {
        model_.useDefaultCoordinateSystem();
#if defined(MODEL_WITH_PATH_GUIDED_SURFACES)
        auto [surface_points, smoothing_factor] = CreateWidthPathGuidedSurface(cross_section_curve, cross_section_position, path_curve);
        return rp.createSurface(static_cast<size_t>(surface_id), surface_points, smoothing_factor);
#else // #if defined MODEL_WITH_PATH_GUIDED_SURFACES
        return rp.createWidthwiseExtrudedSurface(static_cast<std::size_t>(surface_id), cross_section_curve, cross_section_position, path_curve);
#endif // #if defined MODEL_WITH_PATH_GUIDED_SURFACES
    }

    std::tuple<std::vector<double>, double> SModellerWrapper::CreateLengthPathGuidedSurface(
        const std::vector<double>& cross_section_curve, double cross_section_position,
        const std::vector<double>& path_curve)
    {
#if defined(MODEL_WITH_PATH_GUIDED_SURFACES)
        model_.useDefaultCoordinateSystem();

        //
        // Use the PathGuidedSurface helper to create a surface from orbits
        // computed from a vector field defined from the `path_curve`.
        // 
        // Such orbits are used as level sets for the new surface (to be created).
        //
        PathGuidedSurface surface;

        //
        // Set boundary
        //
        // Get the origin of the coordinate system and the size of the bounding
        // box and compute a new bounding box 20% bigger than the original in
        // both dimensions.
        //
        // This is an arbitrary choice and can be changed to modify how the
        // path guided surface looks near the bounding box of the model.
        //
        double origin_x, origin_y, origin_z;
        double size_x, size_y, size_z;
        GetOrigin(origin_x, origin_y, origin_z);
        GetSize(size_x, size_y, size_z);

        // Change `m` to modify the bounding box of vector field/path guided surface helper.
        double m = 0.2;
        surface.setOrigin(origin_x - m * size_x, origin_y - m * size_y);
        surface.setSize(size_x * (1.0 + 2 * m), size_y * (1.0 + 2 * m));

        //
        // Input path
        // 
        // The input path corresponds to the curve (`path_curve`) being
        // currently used to define the vector field.
        //
        // `path_max_disc` is arbitrary, pick a number that makes sense to the
        // type of paths you want to represent.
        //
        size_t path_max_disc = 256;
        double path_disc_per_unit_of_length = static_cast<double>(path_max_disc)/std::max(size_x, size_y);
        std::vector<double> path_curve_point_data = path_curve;

        //
        // Add `path_disc_per_unit_of_length` tangent vectors (but no more than
        // `path_max_disc` samples) of `path_curve_point_data` to vector field
        //
        surface.addGuidingPathTangentVectors(path_curve_point_data, path_disc_per_unit_of_length, path_max_disc);

        //
        // You can make as many calls to `addGuidingPathTangentVectors()` as you
        // like before interpolating the vector field, but make sure that the
        // `path_curve_point_data` makes sense, i.e., has no self intersections, etc.
        //

        // Interpolate vector field.
        surface.setGuidingPaths();

        //
        // Compute orbits
        //
        // There are many parameters to control when computing orbits
        //

        // `orbit_max_disc` holds the maximum number of points allowed per orbit
        //
        size_t orbit_max_disc = 72;

        // `orbit_disc_per_unit_of_length` holds the intended discretization of the orbits per unit of length
        //
        // This is arbitrary, use more points for a better quality surface
        // (slower) and less points for a faster surface (which may not capture
        // small features of the surface)
        //
        double orbit_disc_per_unit_of_length = static_cast<double>(orbit_max_disc)/std::max(size_x, size_y);

        //
        // *******************************************************************
        // This part is only required because the model does not know what is
        // happening in the interface, if you control the sketching process you
        // probably should avoid using the code below and start passing exactly
        // the input you want to use to create a surface to the
        // addOrbitToSurfaceSamples() method.
        //

        //
        // Now the cross-section curve is interpolated to make sure that it is
        // resampled to a known number of samples
        //
        // `cross_sec_max_disc` is arbitrary, higher values may represent more
        // information from the cross-section curve and will take more time to
        // creae a new surface, lower values will be faster and possibly miss
        // features of the cross-section curve
        //
        size_t cross_sec_max_disc = 64;
        std::vector<double> path_guided_cross_section = surface.resampleCrossSectionCurve(cross_section_curve, cross_sec_max_disc);
        //
        // *******************************************************************
        //

        //
        // Create level sets with the vector field (orbits) passing through the points in the cross-section
        //
        double width, length = cross_section_position, height;
        for (size_t i = 0; i < path_guided_cross_section.size() / 2; ++i)
        {
            width = path_guided_cross_section[2 * i + 0];
            height = path_guided_cross_section[2 * i + 1];

            surface.addOrbitToSurfaceSamples(width, length, height, orbit_disc_per_unit_of_length, orbit_max_disc);
        }

        //
        // Create final surface
        //
        /* std::cout << "---> Creating final surface\n"; */
        auto surface_points = surface.getSurfaceSamples();

        //
        // These options are arbitrary, you can change them as you please
        //
        // `fill_distance_factor` = 0 means interpolation, if `fill_distance_factor` --> \infty then surface becomes smoother and smoother.
        //
        const double fill_distance_factor = 1.0 / (10.0 * std::sqrt(2));
        auto sqr = [](double x) -> double { return x * x; };
        double smoothing_factor = std::sqrt(sqr(size_x - origin_x) + sqr(size_y - origin_y)) * fill_distance_factor;

        //
        // Return surface points and smoothing factor to create a new interpolated surface
        //
        return std::tie(surface.getSurfaceSamples(), smoothing_factor);

#else // #if defined MODEL_WITH_PATH_GUIDED_SURFACES
        return std::make_tuple(std::vector<double>(), 0.);

#endif // #if defined MODEL_WITH_PATH_GUIDED_SURFACES
    }

    std::tuple<std::vector<double>, double> SModellerWrapper::CreateWidthPathGuidedSurface(
        const std::vector<double>& cross_section_curve, double cross_section_position,
        const std::vector<double>& path_curve)
    {
#if defined(MODEL_WITH_PATH_GUIDED_SURFACES)
        model_.useDefaultCoordinateSystem();

        //
        // Use the PathGuidedSurface helper to create a surface from orbits
        // computed from a vector field defined from the `path_curve`.
        // 
        // Such orbits are used as level sets for the new surface (to be created).
        //
        PathGuidedSurface surface;

        //
        // Set boundary
        //
        // Get the origin of the coordinate system and the size of the bounding
        // box and compute a new bounding box 20% bigger than the original in
        // both dimensions.
        //
        // This is an arbitrary choice and can be changed to modify how the
        // path guided surface looks near the bounding box of the model.
        //
        double origin_x, origin_y, origin_z;
        double size_x, size_y, size_z;
        GetOrigin(origin_x, origin_y, origin_z);
        GetSize(size_x, size_y, size_z);

        // Change `m` to modify the bounding box of vector field/path guided surface helper.
        double m = 0.2;
        surface.setOrigin(origin_x - m * size_x, origin_y - m * size_y);
        surface.setSize(size_x * (1.0 + 2 * m), size_y * (1.0 + 2 * m));

        //
        // Input path
        // 
        // The input path corresponds to the curve (`path_curve`) being
        // currently used to define the vector field.
        //
        // `path_max_disc` is arbitrary, pick a number that makes sense to the
        // type of paths you want to represent.
        //
        size_t path_max_disc = 256;
        double path_disc_per_unit_of_length = static_cast<double>(path_max_disc)/std::max(size_x, size_y);
        std::vector<double> path_curve_point_data = path_curve;

        //
        // Add `path_disc_per_unit_of_length` tangent vectors (but no more than
        // `path_max_disc` samples) of `path_curve_point_data` to vector field
        //
        surface.addGuidingPathTangentVectors(path_curve_point_data, path_disc_per_unit_of_length, path_max_disc);

        //
        // You can make as many calls to `addGuidingPathTangentVectors()` as you
        // like before interpolating the vector field, but make sure that the
        // `path_curve_point_data` makes sense, i.e., has no self intersections, etc.
        //

        // Interpolate vector field.
        surface.setGuidingPaths();

        //
        // Compute orbits
        //
        // There are many parameters to control when computing orbits
        //

        // `orbit_max_disc` holds the maximum number of points allowed per orbit
        //
        size_t orbit_max_disc = 72;

        // `orbit_disc_per_unit_of_length` holds the intended discretization of the orbits per unit of length
        //
        // This is arbitrary, use more points for a better quality surface
        // (slower) and less points for a faster surface (which may not capture
        // small features of the surface)
        //
        double orbit_disc_per_unit_of_length = static_cast<double>(orbit_max_disc)/std::max(size_x, size_y);

        //
        // *******************************************************************
        // This part is only required because the model does not know what is
        // happening in the interface, if you control the sketching process you
        // probably should avoid using the code below and start passing exactly
        // the input you want to use to create a surface to the
        // addOrbitToSurfaceSamples() method.
        //

        //
        // Now the cross-section curve is interpolated to make sure that it is
        // resampled to a known number of samples
        //
        // `cross_sec_max_disc` is arbitrary, higher values may represent more
        // information from the cross-section curve and will take more time to
        // creae a new surface, lower values will be faster and possibly miss
        // features of the cross-section curve
        //
        size_t cross_sec_max_disc = 64;
        std::vector<double> path_guided_cross_section = surface.resampleCrossSectionCurve(cross_section_curve, cross_sec_max_disc);
        //
        // *******************************************************************
        //

        //
        // Create level sets with the vector field (orbits) passing through the points in the cross-section
        //
        double width = cross_section_position, length, height;
        for (size_t i = 0; i < path_guided_cross_section.size() / 2; ++i)
        {
            length = path_guided_cross_section[2 * i + 0];
            height = path_guided_cross_section[2 * i + 1];

            surface.addOrbitToSurfaceSamples(width, length, height, orbit_disc_per_unit_of_length, orbit_max_disc);
        }

        //
        // Create final surface
        //
        /* std::cout << "---> Creating final surface\n"; */
        auto surface_points = surface.getSurfaceSamples();

        //
        // These options are arbitrary, you can change them as you please
        //
        // `fill_distance_factor` = 0 means interpolation, if `fill_distance_factor` --> \infty then surface becomes smoother and smoother.
        //
        const double fill_distance_factor = 1.0 / (10.0 * std::sqrt(2));
        auto sqr = [](double x) -> double { return x * x; };
        double smoothing_factor = std::sqrt(sqr(size_x - origin_x) + sqr(size_y - origin_y)) * fill_distance_factor;

        //
        // Return surface points and smoothing factor to create a new interpolated surface
        //
        return std::tie(surface.getSurfaceSamples(), smoothing_factor);

#else // #if defined MODEL_WITH_PATH_GUIDED_SURFACES
        return std::make_tuple(std::vector<double>(), 0.);

#endif // #if defined MODEL_WITH_PATH_GUIDED_SURFACES
    }


    bool SModellerWrapper::GetWidthCrossSectionCurve(
        int surface_id,
        int cross_section_position,
        std::vector<double>& vertex_coordinates,
        std::vector<std::size_t>& vertex_indices)
    {
        /* SUtilities u(model_); */
        /* bool success = u.getAlternativeWidthCrossSectionCurve( */
        bool success = model_.getWidthCrossSectionCurve(
            static_cast<std::size_t>(surface_id),
            static_cast<std::size_t>(cross_section_position),
            vertex_coordinates,
            vertex_indices);

        return success;
    }


    bool SModellerWrapper::GetLengthCrossSectionCurve(
        int surface_id,
        int cross_section_position,
        std::vector<double>& vertex_coordinates,
        std::vector<std::size_t>& vertex_indices)
    {
        /* SUtilities u(model_); */
        /* bool success = u.getAlternativeLengthCrossSectionCurve( */
        bool success = model_.getLengthCrossSectionCurve(
            static_cast<std::size_t>(surface_id),
            static_cast<std::size_t>(cross_section_position),
            vertex_coordinates,
            vertex_indices);

        return success;
    }


    bool SModellerWrapper::GetSurfaceMesh(
        int surface_id,
        std::vector<double>& vertex_coordinates,
        std::vector<std::size_t>& vertex_indices)
    {
        model_.useDefaultCoordinateSystem();
        return model_.getMesh(
            static_cast<size_t>(surface_id),
            vertex_coordinates,
            vertex_indices);
    }

    bool SModellerWrapper::RequestPreserveAbove(const std::vector<double>& sketch_points)
    {
        model_.useDefaultCoordinateSystem();
        return rp.requestPreserveAbove(sketch_points);
    }

    bool SModellerWrapper::RequestPreserveBelow(const std::vector<double>& sketch_points)
    {
        model_.useDefaultCoordinateSystem();
        return rp.requestPreserveBelow(sketch_points);
    }

    bool SModellerWrapper::RequestPreserveRegion(const std::vector<double>& p)
    {
        model_.useDefaultCoordinateSystem();
        return rp.requestPreserveRegion(p);
    }

    bool SModellerWrapper::PreserveAboveIsActive()
    {
        return rp.preserveAboveIsActive();
    }

    bool SModellerWrapper::PreserveBelowIsActive()
    {
        return rp.preserveBelowIsActive();
    }

    bool SModellerWrapper::PreserveRegionIsActive()
    {
        if (rp.preserveAboveIsActive() || rp.preserveBelowIsActive())
        {
            return true;
        }

        return false;
    }

    bool SModellerWrapper::canUndo()
    {
        return model_.canUndo();
    }

    bool SModellerWrapper::undo()
    {
        return model_.undo();
    }

    bool SModellerWrapper::canRedo()
    {
        return model_.canRedo();
    }

    bool SModellerWrapper::redo()
    {
        return model_.redo();
    }

    void SModellerWrapper::removeAbove()
    {
        model_.removeAbove();
    }

    void SModellerWrapper::removeAboveIntersection()
    {
        model_.removeAboveIntersection();
    }

    void SModellerWrapper::removeBelow()
    {
        model_.removeBelow();
    }

    void SModellerWrapper::removeBelowIntersection()
    {
        model_.removeBelowIntersection();
    }

    void SModellerWrapper::StopPreserveAbove()
    {
        rp.stopPreserveAbove();
    }

    void SModellerWrapper::StopPreserveBelow()
    {
        rp.stopPreserveBelow();
    }

    void SModellerWrapper::StopPreserveRegion()
    {
        rp.stopPreserveRegion();
    }

    bool SModellerWrapper::ComputeTetrahedralMeshVolumes( std::vector<double> &volume_list )
    {
        std::lock_guard<std::mutex> g(m);
        model_.useDefaultCoordinateSystem();
        return model_.computeTetrahedralMeshVolumes(volume_list);
    }

    bool SModellerWrapper::getLengthPreserveAboveCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &flist )
    {
        model_.useDefaultCoordinateSystem();
        return rp.getPreserveAboveCurveBoxAtLength(static_cast<std::size_t>(cross_section_id), vlist, flist);
    }

    bool SModellerWrapper::getWidthPreserveAboveCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &flist )
    {
        model_.useDefaultCoordinateSystem();
        return rp.getPreserveAboveCurveBoxAtWidth(static_cast<std::size_t>(cross_section_id), vlist, flist);
    }

    bool SModellerWrapper::getLengthPreserveBelowCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &flist )
    {
        model_.useDefaultCoordinateSystem();
        return rp.getPreserveBelowCurveBoxAtLength(static_cast<std::size_t>(cross_section_id), vlist, flist);
    }

    bool SModellerWrapper::getWidthPreserveBelowCurveBox( int cross_section_id, std::vector<double> &vlist, std::vector<size_t> &flist )
    {
        model_.useDefaultCoordinateSystem();
        return rp.getPreserveBelowCurveBoxAtWidth(static_cast<std::size_t>(cross_section_id), vlist, flist);
    }

    bool SModellerWrapper::getLengthRegionCurveBox( std::size_t region_id, std::size_t cross_section_id, 
            std::vector<double> &lower_bound_box_vlist, std::vector<std::size_t> &lower_bound_box_elist,
            std::vector<double> &upper_bound_box_vlist, std::vector<std::size_t> &upper_bound_box_elist )
    {
        model_.useDefaultCoordinateSystem();
        return rp.getRegionCurveBoxesAtLength(region_id, cross_section_id, 
                lower_bound_box_vlist, lower_bound_box_elist, 
                upper_bound_box_vlist, upper_bound_box_elist);
    }

    bool SModellerWrapper::getWidthRegionCurveBox( std::size_t region_id, std::size_t cross_section_id, 
            std::vector<double> &lower_bound_box_vlist, std::vector<std::size_t> &lower_bound_box_elist,
            std::vector<double> &upper_bound_box_vlist, std::vector<std::size_t> &upper_bound_box_elist )
    {
        model_.useDefaultCoordinateSystem();
        return rp.getRegionCurveBoxesAtWidth(region_id, cross_section_id, 
                lower_bound_box_vlist, lower_bound_box_elist, 
                upper_bound_box_vlist, upper_bound_box_elist);
    }

    bool SModellerWrapper::getRegionsTetrahedralMeshes(std::vector<double> &vertex_coordinates, std::vector< std::vector<std::size_t> > &element_list)
    {
        std::lock_guard<std::mutex> g(m);
        model_.useDefaultCoordinateSystem();
        return rp.getTetrahedralMesh(vertex_coordinates, element_list);
    }

    std::vector<std::size_t> SModellerWrapper::getOrderedSurfacesIndices()
    {
        return model_.getOrderedSurfacesIndices();
    }

    std::size_t SModellerWrapper::getTetrahedralMesh( std::vector<double> &vertex_coordinates, std::vector<std::size_t> &element_list, std::vector<long int> &attribute_list )
    {
        return model_.getTetrahedralMesh(vertex_coordinates, element_list, attribute_list);
    }

    bool SModellerWrapper::exportToIrapGrid(const std::string& project_name)
    {
        model_.useDefaultCoordinateSystem();
        rp.exportToIrapGrid(project_name);

        return true;
    }

    bool SModellerWrapper::getModelInfCurveAtLength(std::vector<std::size_t> &surface_indices, std::size_t length,
            std::vector<double> &vlist, std::vector<std::size_t> &elist)
    {
        return rp.getModelInfCurveAtLength(surface_indices, length, vlist, elist);
    }

    bool SModellerWrapper::getModelSupCurveAtLength(std::vector<std::size_t> &surface_indices, std::size_t length,
            std::vector<double> &vlist, std::vector<std::size_t> &elist)
    {
        return rp.getModelSupCurveAtLength(surface_indices, length, vlist, elist);
    }

    bool SModellerWrapper::getModelInfCurveAtWidth(std::vector<std::size_t> &surface_indices, std::size_t width,
            std::vector<double> &vlist, std::vector<std::size_t> &elist)
    {
        return rp.getModelInfCurveAtWidth(surface_indices, width, vlist, elist);
    }

    bool SModellerWrapper::getModelSupCurveAtWidth(std::vector<std::size_t> &surface_indices, std::size_t width,
            std::vector<double> &vlist, std::vector<std::size_t> &elist)
    {
        return rp.getModelSupCurveAtWidth(surface_indices, width, vlist, elist);
    }

    void SModellerWrapper::convertEntireCurveToBox(std::vector<double> &vlist, std::vector<size_t> &flist, double box_boundary_height)
    {
        rp.convertEntireCurveToBox(vlist, flist, box_boundary_height);
    }

} // namespace model
