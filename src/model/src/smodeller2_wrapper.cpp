/** @license
 * RRM - Rapid Reservoir Modeling Project
 * Copyright (C) 2021
 * UofC - University of Calgary
 *
 * This file is part of RRM Software.
 *
 * RRM is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * RRM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with RRM.  If not, see <http://www.gnu.org/licenses/>,
 * or write to the Free Software Foundation, Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA  02110-1301  USA.
 */

/**
 * @file smodeller_wrapper.cpp
 * @author Julio Daniel Machado Silva
 * @brief Wrapper for stratmod::SModeller2 class
 */

#include "model/smodeller2_wrapper.h"
#include "stratmod/surfaces/stratigraphic_surfaces.hpp"

#include "rules_processor.hpp"

namespace model {
    using namespace stratmod;

    static RulesProcessor& rp = RulesProcessor::Instance();

    auto mapPoints3D(std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/3, 3))
    {
        return mapVectorToEigen(points, points.size()/3, 3);
    }

    auto mapPoints2D(std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/2, 2))
    {
        return mapVectorToEigen(points, points.size()/2, 2);
    }

    auto mapPoints3D(const std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/3, 3))
    {
        return mapVectorToEigen(points, points.size()/3, 3);
    }

    auto mapPoints2D(const std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/2, 2))
    {
        return mapVectorToEigen(points, points.size()/2, 2);
    }

    /////////////////////////////////////////////////////////////////
    // New code to access model
    /////////////////////////////////////////////////////////////////

    SModeller2Wrapper& SModeller2Wrapper::Instance()
    {
        static SModeller2Wrapper instance;

        return instance;
    }

    stratmod::SModeller2& SModeller2Wrapper::model()
    {
        return stratmod::SModeller2::Instance();
    }

    bool SModeller2Wrapper::AddStratigraphicSurface(
            SurfaceUniqueIdentifier unique_id, const std::shared_ptr<InputSurface>& isptr)
    {
        return rp.createStratigraphicSurface(unique_id, isptr);
    }

    bool SModeller2Wrapper::AddFaultSurface(
            SurfaceUniqueIdentifier unique_id, const std::shared_ptr<InputSurface>& isptr)
    {
        return rp.createFaultSurface(unique_id, isptr);
    }

    std::shared_ptr<stratmod::InputSurface> SModeller2Wrapper::CreateSurface(const std::vector<double>& surface_points, double smoothing_factor)
    {
        auto iptr = std::make_shared<InterpolatedSurface>();
        iptr->set(*mapPoints3D(surface_points), smoothing_factor);

        return iptr;
    }

    std::shared_ptr<stratmod::InputSurface> SModeller2Wrapper::CreateLengthExtrudedSurface(const std::vector<double>& curve_points)
    {
        auto isptr = std::make_shared<ExtrudedSurface>();
        isptr->setLinearExtrusion(*mapPoints2D(curve_points));

        return isptr;
    }

    std::shared_ptr<stratmod::InputSurface> SModeller2Wrapper::CreateWidthExtrudedSurface(const std::vector<double>& curve_points)
    {
        auto isptr = std::make_shared<ExtrudedSurface>();
        isptr->setLinearExtrusion(*mapPoints2D(curve_points));
        isptr->setCrossSectionDirection({0., 1.});

        return isptr;
    }

    std::shared_ptr<stratmod::InputSurface> SModeller2Wrapper::CreateLengthExtrudedSurface(
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve)
    {
        using namespace Eigen;

        Map<Matrix<double, Dynamic, Dynamic, RowMajor>> cross_section(const_cast<double*>(cross_section_curve.data()), cross_section_curve.size()/2, 2);
        Map<Matrix<double, Dynamic, Dynamic, RowMajor>> opath(const_cast<double*>(path_curve.data()), path_curve.size()/2, 2);
        Matrix<double, Dynamic, Dynamic, RowMajor>  path(opath.rows(), opath.cols());
        path.col(0) = opath.col(1) - cross_section_position * Eigen::VectorXd::Ones(opath.rows());
        path.col(1) = opath.col(0);

        auto isptr = std::make_shared<ExtrudedSurface>();
        bool success = isptr->setPathGuidedExtrusion(cross_section, path);
        isptr->setCrossSectionOrigin({0., cross_section_position});

        return isptr;
    }

    std::shared_ptr<stratmod::InputSurface> SModeller2Wrapper::CreateWidthExtrudedSurface(
            const std::vector<double>& cross_section_curve, double cross_section_position,
            const std::vector<double>& path_curve)
    {
        using namespace Eigen;

        Map<Matrix<double, Dynamic, Dynamic, RowMajor>> cross_section(const_cast<double*>(cross_section_curve.data()), cross_section_curve.size()/2, 2);
        Map<Matrix<double, Dynamic, Dynamic, RowMajor>> opath(const_cast<double*>(path_curve.data()), path_curve.size()/2, 2);
        Matrix<double, Dynamic, Dynamic, RowMajor>  path(opath.rows(), opath.cols());
        path.col(0) = cross_section_position * Eigen::VectorXd::Ones(opath.rows()) - opath.col(0);
        path.col(1) = opath.col(1);


        auto isptr = std::make_shared<ExtrudedSurface>();
        bool success = isptr->setPathGuidedExtrusion(cross_section, path);
        isptr->setCrossSectionDirection({0., 1.});
        isptr->setCrossSectionOrigin({cross_section_position, 0.});

        return isptr;
    }
} // namespace model
