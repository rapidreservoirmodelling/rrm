/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#include "stratmod/surfaces/interpolated_triangle_surface.hpp"
#include "detail/planin/include/planin/mesh/linear_complexes.hpp"

#include <algorithm>
#include <iterator>
#include <numeric>
#include <cmath>
#include <vector>

#include <glog/logging.h>

/* #include <igl/triangle/triangulate.h> */
/* #include <igl/triangle/cdt.h> */
#include <igl/delaunay_triangulation.h>

#include <planin/predicates.hpp>

namespace stratmod 
{
    double InterpolatedTriangleSurface::evalHeight(const Eigen::Vector2d& p) const 
    {
        return isurf_.evalHeight(p);
    }

    bool InterpolatedTriangleSurface::differentiable() const 
    {
        return true;
    }

    Eigen::Vector2d InterpolatedTriangleSurface::evalDerivative(const Eigen::Vector2d& p) const 
    {
        return isurf_.evalDerivative(p);
    }

    bool InterpolatedTriangleSurface::set(const Eigen::MatrixXd& points, double smoothing_parameter)
    {
        bool success = isurf_.set(points, smoothing_parameter);

        Eigen::MatrixXd vin;
        Eigen::MatrixXi edges, holes, triangles;

        vin.resize(points.rows(), 2);
        vin.col(0) = points.col(0);
        vin.col(1) = points.col(1);

        /* std::cout << "Creating a quality Delaunay triangulation\n" << std::flush; */
        /* Eigen::MatrixXd vout; */
        /* igl::triangle::triangulate(vin, edges, holes, "q",  vout, triangles); */
        /* igl::triangle::triangulate(vin, edges, holes, "-cVV",  vout, triangles); */

        /* std::cout << "Creating a Delaunay triangulation\n" << std::flush; */
        auto orient2d = [](const double pa[2], const double pb[2], const double pc[2]) -> double {
            std::array<double, 2> pa_ = {pa[0], pa[1]};
            std::array<double, 2> pb_ = {pb[0], pb[1]};
            std::array<double, 2> pc_ = {pc[0], pc[1]};
            return RobustPredicates::Get().orient2d(pa_, pb_, pc_);
        };
        auto incircle = [](const double pa[2], const double pb[2], const double pc[2], const double pd[2]) -> double {
            std::array<double, 2> pa_ = {pa[0], pa[1]};
            std::array<double, 2> pb_ = {pb[0], pb[1]};
            std::array<double, 2> pc_ = {pc[0], pc[1]};
            std::array<double, 2> pd_ = {pd[0], pd[1]};
            return RobustPredicates::Get().incircle(pa_, pb_, pc_, pd_);
        };
        igl::delaunay_triangulation(vin, orient2d, incircle, triangles);
        auto& vout = vin;

        Eigen::MatrixXd refined_vertices;
        refined_vertices.resize(vout.rows(), 3);
        refined_vertices.col(0) = vout.col(0);
        refined_vertices.col(1) = vout.col(1);
        for (int i = 0; i < vout.rows(); ++i)
        {
            refined_vertices(i, 2) = isurf_.evalHeight(vout.row(i).transpose());
        }
        /* refined_vertices = points; */

        success &= TriangleSurface::set(refined_vertices, triangles);

        return success;
    }

    bool InterpolatedTriangleSurface::set(const Eigen::MatrixXd& vertices, const Eigen::MatrixXi& triangles, double smoothing_parameter)
    {
        std::set<int> ids{};
        std::cout << "0" << std::endl << std::flush;
        for (int i = 0; i < vertices.rows(); ++i)
        {
            for (int j = 0; j < vertices.cols(); ++j)
            {
                ids.insert(triangles(i, j));
            }
        }

        Eigen::MatrixXd points(ids.size(), 3);
        std::cout << "1" << std::endl << std::flush;
        if (!ids.empty())
        {
        std::cout << "2" << std::endl << std::flush;
            int i = 0;
            for (auto v : ids)
            {
                points.row(i++) = vertices.row(v);
            }
        }
        else
        {
        std::cout << "3" << std::endl << std::flush;
            points = vertices;
        }

        std::cout << "4" << std::endl << std::flush;
        bool success = isurf_.set(points, smoothing_parameter) && TriangleSurface::set(vertices, triangles);

        return success;
    }

    bool InterpolatedTriangleSurface::resampleTriangleSurface(const Eigen::MatrixXd& vertices, const Eigen::MatrixXi& triangles)
    {
        if (triangles.cols() != 3)
        {
            return false;
        }

        Eigen::MatrixXd refined_vertices;
        if (vertices.cols() == 2)
        {
            refined_vertices.resize(vertices.rows(), 3);
            refined_vertices.col(0) = vertices.col(0);
            refined_vertices.col(1) = vertices.col(1);
            Eigen::Vector2d v;
            for (int i = 0; i < vertices.rows(); ++i)
            {
                v << vertices(i, 0), vertices(i, 1);
                refined_vertices(i, 2) = isurf_.evalHeight(v);
            }
        }
        else if (vertices.cols() == 3)
        {
            refined_vertices = vertices;
        }
        else
        {
            return false;
        }

        input_vertices_ = refined_vertices;
        input_triangles_ = triangles;

        return true;
    }

    Eigen::MatrixXd InterpolatedTriangleSurface::getInputVertices() const
    {
        return TriangleSurface::getInputVertices();
    }

    Eigen::MatrixXi InterpolatedTriangleSurface::getInputTriangles() const
    {
        return TriangleSurface::getInputTriangles();
    }

    Eigen::MatrixXd InterpolatedTriangleSurface::getVertices() const
    {
        return TriangleSurface::getVertices();
    }

    Eigen::MatrixXi InterpolatedTriangleSurface::getTriangles() const
    {
        return TriangleSurface::getTriangles();
    }

    Eigen::MatrixXi InterpolatedTriangleSurface::getBoundary() const
    {
        return TriangleSurface::getBoundary();
    }
} // namespace stratmod
