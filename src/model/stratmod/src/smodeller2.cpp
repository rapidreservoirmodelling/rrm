/********************************************************************************/
/*                                                                              */
/* This file is part of the "Stratigraphy Modeller Library" (StratModLib)       */
/* Copyright (C) 2017, Julio Daniel Machado Silva.                              */
/*                                                                              */
/* StratModLib is free software; you can redistribute it and/or                 */
/* modify it under the terms of the GNU Lesser General Public                   */
/* License as published by the Free Software Foundation; either                 */
/* version 3 of the License, or (at your option) any later version.             */
/*                                                                              */
/* StratModLib is distributed in the hope that it will be useful,               */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU            */
/* Lesser General Public License for more details.                              */
/*                                                                              */
/* You should have received a copy of the GNU Lesser General Public             */
/* License along with StratModLib.  If not, see <http://www.gnu.org/licenses/>, */
/* or write to the Free Software Foundation, Inc., 51 Franklin Street,          */
/* Fifth Floor, Boston, MA  02110-1301  USA.                                    */
/*                                                                              */
/********************************************************************************/



#include <iostream>
#include <mutex>
#include <random>
#include <vector>
#include <map>
#include <memory>
#include <fstream>

#include "detail/planin/external/libigl/external/tetgen/tetgen.h"
#include "detail/serialization_definitions.hpp"
#include "stratmod/metadata_cereal.hpp"
#include "detail/model_interpretation_cereal.hpp"
#include "stratmod/misc/smodeller_primitives.hpp"
#include "stratmod/smodeller2.hpp"
#include "detail/smodeller_impl.hpp"

#include "detail/testing_definitions.hpp"

#include "planin/planin.hpp"
#include "planin/legacy_planar_surface.hpp"


namespace stratmod { 

/*****************************/
/* Actual Implementation */ 
/*****************************/


// Default methods 

SModeller2& SModeller2::Instance()
{
    static SModeller2 instance;

    return instance;
}

SModeller2& SModeller2::ClearInstance()
{
    auto& instance = SModeller2::Instance();
    instance.clear();

    return instance;
}

SModeller2::SModeller2() : 
    pimpl_( new SModellerImplementation() ) 
{
    pimpl_->init();
} 

SModeller2::~SModeller2() = default; 

SModellerImplementation& SModeller2::Impl()
{
    return *pimpl_;
}

const SModellerImplementation& SModeller2::Impl() const
{
    return *pimpl_;
}

std::vector<size_t> SModeller2::getSurfacesIndices()
{
    return pimpl_->inserted_surfaces_indices_; 
}

std::vector<std::size_t> SModeller2::getOrderedSurfacesIndices()
{
    return pimpl_->getOrderedSurfacesIndices();
}

/* // create[Above,Below] is DEPRECATED */
/* // */
/* // brief: */
/* // Verifies whether ir is possible to define a new 'drawing' region, */
/* // either above or below. */
/* // Argument: `eligible_surfaces` stores the indices of surfaces that can */
/* // be used as input either for an `defineAbove()` or a `defineBelow()`. */
/* // Return: true if at least one elegible surface was found. */
/* // */
/* [[deprecated]] */
/* bool requestCreateAbove( std::vector<std::size_t> &eligible_surfaces ); */

/* bool SModeller2::requestCreateAbove( std::vector<size_t> &eligible_surfaces ) */
/* { */
    /* if ( pimpl_->container_.empty() ) */
    /* { */
    /*     return false; */ 
    /* } */

    /* eligible_surfaces.clear(); */ 
    /* ControllerSurfaceIndex output_index; */ 
    /* ContainerSurfaceIndex index; */ 

    /* for ( size_t i = 0; i < pimpl_->inserted_surfaces_indices_.size(); ++i ) */
    /* { */
    /*     output_index = pimpl_->inserted_surfaces_indices_[i]; */ 
    /*     if ( pimpl_->getSurfaceIndex(output_index, index) == false ) */
    /*         continue; */ 

    /*     if ( pimpl_->container_.weakEntireSurfaceCheck(index) ) */
    /*     { */
    /*         if ( pimpl_->createBelowIsActive() ) */
    /*         { */
    /*             /1* std::cout << "Define Below is active.\n"; *1/ */ 
    /*             ContainerSurfaceIndex boundary_index; */ 

    /*             if ( pimpl_->getSurfaceIndex(pimpl_->current_.upper_boundary_, boundary_index) == false ) */
    /*             { */
    /*                 /1* std::cout << "Surface id: " << index << ", boundary id: " << boundary_index << std::endl; *1/ */ 
    /*                 /1* std::cout << "Got wrong id. \n"; *1/ */ 
    /*                 continue; */
    /*             } */

    /*             /1* std::cout << "Surface id: " << index << ", boundary id: " << boundary_index << std::endl; *1/ */ 

    /*             if ( index == boundary_index ) */
    /*             { */
    /*                 continue; */
    /*             } */

    /*             if ( pimpl_->container_[index]->weakLiesBelowOrEqualsCheck(pimpl_->container_[boundary_index]) ) */
    /*             { */
    /*                 eligible_surfaces.push_back(output_index); */ 
    /*             } */
    /*             else */
    /*             { */
    /*                 /1* std::cout << " --> Not eligible\n"; *1/ */ 
    /*             } */
    /*         } */
    /*         else */
    /*         { */
    /*             eligible_surfaces.push_back(output_index); */ 
    /*         } */

    /*     } */
    /* } */

    /* return !eligible_surfaces.empty(); */ 
/* } */

/* [[deprecated]] */
/* bool requestCreateBelow( std::vector<std::size_t> &eligible_surfaces ); */

/* bool SModeller2::requestCreateBelow( std::vector<size_t> &eligible_surfaces ) */
/* { */
    /* if ( pimpl_->container_.empty() ) */
    /* { */
    /*     return false; */ 
    /* } */

    /* eligible_surfaces.clear(); */ 
    /* ControllerSurfaceIndex output_index; */ 
    /* ContainerSurfaceIndex index; */ 

    /* for ( size_t i = 0; i < pimpl_->inserted_surfaces_indices_.size(); ++i ) */
    /* { */
    /*     output_index = pimpl_->inserted_surfaces_indices_[i]; */ 
    /*     if ( pimpl_->getSurfaceIndex(output_index, index) == false ) */
    /*         continue; */ 

    /*     if ( pimpl_->container_.weakEntireSurfaceCheck(index) ) */
    /*     { */
    /*         if ( pimpl_->createAboveIsActive() ) */
    /*         { */
    /*             /1* std::cout << "Define Below is active.\n"; *1/ */ 
    /*             ContainerSurfaceIndex boundary_index; */ 

    /*             if ( pimpl_->getSurfaceIndex(pimpl_->current_.lower_boundary_, boundary_index) == false ) */
    /*             { */
    /*                 /1* std::cout << "Surface id: " << index << ", boundary id: " << boundary_index << std::endl; *1/ */ 
    /*                 /1* std::cout << "Got wrong id. \n"; *1/ */ 
    /*                 continue; */
    /*             } */

    /*             /1* std::cout << "Surface id: " << index << ", boundary id: " << boundary_index << std::endl; *1/ */ 
    /*             if ( index == boundary_index ) */
    /*             { */
    /*                 continue; */
    /*             } */

    /*             if ( pimpl_->container_[index]->weakLiesAboveOrEqualsCheck(pimpl_->container_[boundary_index]) ) */
    /*             { */
    /*                 eligible_surfaces.push_back(output_index); */ 
    /*             } */
    /*             else */
    /*             { */
    /*                 /1* std::cout << " --> Not eligible\n"; *1/ */ 
    /*             } */
    /*         } */
    /*         else */
    /*         { */
    /*             eligible_surfaces.push_back(output_index); */ 
    /*         } */

    /*     } */
    /* } */

    /* return !eligible_surfaces.empty(); */ 
/* } */


        /* Change the model's properties */
bool SModeller2::useDefaultCoordinateSystem()
{
    /* if ( pimpl_->container_.size() > 0 ) */
    /* { */
    /*     return false; */
    /* } */

    PlaninASurface::setOutputCoordinatesOrdering( 
            PlaninASurface::Coordinate::WIDTH,
            PlaninASurface::Coordinate::HEIGHT, 
            PlaninASurface::Coordinate::DEPTH
            );

    pimpl_->default_coordinate_system_ = true;

    return true;
}

bool SModeller2::useOpenGLCoordinateSystem()
{
    /* if ( pimpl_->container_.size() > 0 ) */
    /* { */
    /*     return false; */
    /* } */

    PlaninASurface::setOutputCoordinatesOrdering( 
            PlaninASurface::Coordinate::WIDTH,
            PlaninASurface::Coordinate::DEPTH, 
            PlaninASurface::Coordinate::HEIGHT
            ); 

    pimpl_->default_coordinate_system_ = false;

    return true;
}


/**
 * \brief Change models discretization if model is empty
 *
 * \param width_discretization - discretization in the width direction
 * \param length_discretization - discretization in the length direction
 *
 * \return true if discretization was changed
 */
/* [[deprecated]] */
/* bool tryChangeDiscretization( std::size_t width_discretization = 64, std::size_t depth_discretization = 64 ); */

/* bool SModeller2::tryChangeDiscretization( size_t width, size_t length ) */
/* { */
/*     if ( pimpl_->container_.size() > 0 ) */
/*     { */
/*         return false; */
/*     } */

/*     pimpl_->discWidth_ = width; */ 
/*     pimpl_->discLenght_ = length; */ 

/*     PlaninASurface::requestChangeDiscretization(width, length); */

/*     return true; */
/* } */

bool SModeller2::changeDiscretization( size_t width, size_t length )
{
    if ( (width == 0) || (length == 0) )
    {
        return false;
    }

    // WEIRD: without the following (superfluous) test, changeDiscretization()
    // was crashing if width and length were equal to the model's discretization
    if ( (width == pimpl_->discWidth_) && (length == pimpl_->discLenght_) )
    {
        return true;
    }

    /* int counter = 0; */
    /* while ( canUndo() ) */
    /* { */
    /*     undo(); */
    /*     ++counter; */
    /*     LOG(INFO) << "Undoing surface " << counter; */
    /* } */

    /* PlaninASurface::requestChangeDiscretization(width, length); */
    pimpl_->container_.changeDiscretization(width, length);

    pimpl_->discWidth_ = width; 
    pimpl_->discLenght_ = length; 

    pimpl_->clearCSCache();
    /* pimpl_->container_.updateCache(); */

/*     while ( counter > 0 ) */
/*     { */
/*         redo(); */
/*         --counter; */
/*         LOG(INFO) << "Redoing surface " << pimpl_->container_.size() - counter; */
/*     } */

    /* pimpl_->container_.updateDiscretization(); */
    /* pimpl_->container_.updateCache(); */

    /* TODO: review use and conversion of types */
    /* /1* PlanarSurface::requestChangeDiscretization((PlanarSurface::Natural)(pimpl_->discWidth_), PlanarSurface::Natural(pimpl_->discLenght_)); *1/ */
    /* return pimpl_->container_.changeDiscretization(width, length); */

    return true;
}


size_t SModeller2::getWidthDiscretization() const
{
    return pimpl_->discWidth_;
}

size_t SModeller2::getLengthDiscretization() const
{
    return pimpl_->discLenght_;
}

void SModeller2::setOrigin( double x, double y, double z )
{
    /* if ( pimpl_->default_coordinate_system_ ) */
    /* { */
    /*     pimpl_->origin_.x = x; */ 
    /*     pimpl_->origin_.y = y; */ 
    /*     pimpl_->origin_.z = z; */ 
    /* } */
    /* else */
    /* { */
    /*     pimpl_->origin_.x = x; */ 
    /*     pimpl_->origin_.y = z; */ 
    /*     pimpl_->origin_.z = y; */ 
    /* } */

    pimpl_->origin_ = pimpl_->point3(x, y, z);
    pimpl_->container_.setOrigin(pimpl_->origin_);

    pimpl_->got_origin_ = true; 
}


bool SModeller2::setSize( double x, double y, double z )
{
    bool success = pimpl_->container_.setLength( pimpl_->point3(x, y, z) );

    if ( success )
    /* if ( ( x > 0 ) && ( y > 0 ) && ( z > 0 ) ) */ 
    { 
        /* if ( pimpl_->default_coordinate_system_ ) */
        /* { */
        /*     pimpl_->lenght_.x = x; */ 
        /*     pimpl_->lenght_.y = y; */ 
        /*     pimpl_->lenght_.z = z; */ 
        /* } */
        /* else */
        /* { */
        /*     pimpl_->lenght_.x = x; */ 
        /*     pimpl_->lenght_.y = z; */ 
        /*     pimpl_->lenght_.z = y; */ 
        /* } */

        pimpl_->lenght_ = pimpl_->point3(x, y, z);

        pimpl_->got_lenght_ = true; 
    }

    return success; 
}

void SModeller2::getOrigin( double &x, double &y, double &z )
{
    pimpl_->getOrigin(x, y, z);
}


void SModeller2::getSize( double &x, double &y, double &z )
{
    pimpl_->getLenght(x, y, z);
}

        /* Begin methods to interface with GUI */

        /* Initialization and clean up */

void SModeller2::clear()
{
    pimpl_->clear();

    /* pimpl_->container_.clear(); */ 
    /* pimpl_->dictionary_.clear(); */ 
    /* pimpl_->inserted_surfaces_indices_.clear(); */

    /* pimpl_->current_ = StateDescriptor(); */ 

    /* pimpl_->undoed_surfaces_stack_.clear(); */
    /* pimpl_->undoed_states_.clear(); */ 
    /* pimpl_->past_states_.clear(); */

    /* pimpl_->got_origin_ = false; */ 
    /* pimpl_->got_lenght_ = false; */ 

    /* pimpl_->mesh_ = nullptr; */

    /* pimpl_->init(); */
}



        /* Query or modify the automatum state */


/* // */
/* // brief: */
/* // Define new input region above surface which index is `surface_index`. */
/* // */
/* // */
/* // brief: */
/* // Define new input region above surface which index is `surface_index`. */
/* // */
/* [[deprecated]] */
/* bool createAbove( std::size_t surface_index ); */

/* bool SModeller2::createAbove( size_t surface_index ) */
/* { */
/*     return pimpl_->createAbove(surface_index); */ 
/* } */


/* // */
/* // brief: */
/* // Clear any previous `defineAbove()` call. */
/* // Safe to call anytime. */
/* // */
/* [[deprecated]] */
/* void stopCreateAbove(); */

        /* // */
        /* // brief: */
        /* // Clear any previous `createAbove()` call. */
        /* // Safe to call anytime. */
        /* // */
/* void SModeller2::stopCreateAbove() */
/* { */
    /* pimpl_->stopCreateAbove(); */
/* } */


/* // */
/* // brief: */
/* // Define new input region below surface which index is `surface_index`. */
/* // */
/* [[deprecated]] */
/* bool createBelow( std::size_t surface_index ); */

/* // */
/* // brief: */
/* // Define new input region below surface which index is `surface_index`. */
/* // */
/* bool SModeller2::createBelow( size_t surface_index ) */
/* { */
/*     return pimpl_->createBelow(surface_index); */ 
/* } */

/* // */
/* // brief: */
/* // Clear any previous `defineBelow()` call. */
/* // Safe to call anytime. */
/* // */
/* [[deprecated]] */
/* void stopCreateBelow(); */

/* // */
/* // brief: */
/* // Clear any previous `defineBelow()` call. */
/* // Safe to call anytime. */
/* // */
/* void SModeller2::stopCreateBelow() */
/* { */
/*     pimpl_->stopCreateBelow(); */
/* } */


/* [[deprecated]] */
/* bool createAboveIsActive( std::size_t &boundary_index ); */

/* bool SModeller2::createAboveIsActive( size_t &boundary_index ) */
/* { */
/*     return pimpl_->createAboveIsActive(boundary_index); */
/* } */

/* [[deprecated]] */
/* bool createBelowIsActive( std::size_t &boundary_index ); */

/* bool SModeller2::createBelowIsActive( size_t &boundary_index ) */  
/* { */
/*     return pimpl_->createBelowIsActive(boundary_index); */
/* } */

bool SModeller2::preserveAbove( const std::vector<size_t> &bounding_surfaces_list )
{
    return pimpl_->preserveAbove(bounding_surfaces_list);
}

bool SModeller2::preserveBelow( const std::vector<size_t> &bounding_surfaces_list )
{
    return pimpl_->preserveBelow(bounding_surfaces_list);
}

void SModeller2::stopPreserveAbove()
{
    return pimpl_->stopPreserveAbove();
}

void SModeller2::stopPreserveBelow()
{
    return pimpl_->stopPreserveBelow();
}

bool SModeller2::preserveAboveIsActive( std::vector<std::size_t> &bounding_surfaces_list )
{
    return pimpl_->preserveAboveIsActive(bounding_surfaces_list);
}

bool SModeller2::preserveBelowIsActive( std::vector<std::size_t> &bounding_surfaces_list )
{
    return pimpl_->preserveBelowIsActive(bounding_surfaces_list);
}

void SModeller2::disableGeologicRules()
{
    pimpl_->current_.state_ = State::SKETCHING;
    pimpl_->current_.operator_ = GeologicOperator::NoOperator;
}

void SModeller2::removeAbove()
{
    pimpl_->current_.state_ = State::RA_SKETCHING;
    pimpl_->current_.operator_ = GeologicOperator::RemoveAbove;
}

void SModeller2::removeAboveIntersection()
{
    pimpl_->current_.state_ = State::RAI_SKETCHING; 
    pimpl_->current_.operator_ = GeologicOperator::RemoveAboveIntersection;
}

        
void SModeller2::removeBelow()
{
    pimpl_->current_.state_ = State::RB_SKETCHING;
    pimpl_->current_.operator_ = GeologicOperator::RemoveBelow;
}

void SModeller2::removeBelowIntersection()
{
    pimpl_->current_.state_ = State::RBI_SKETCHING; 
    pimpl_->current_.operator_ = GeologicOperator::RemoveBelowIntersection;
} 


/* bool SModeller2::createSurface( size_t surface_id, const std::vector<double> &point_data ) */
/* { */
/*     INFO( "Got into SModeller2::createSurface(...)" ); */

/*     bool extruded_surface = false; */ 
/*     return pimpl_->insertSurface(point_data, surface_id, std::vector<size_t>(), std::vector<size_t>(), extruded_surface); */
/* } */


bool SModeller2::addStratigraphicSurface(SurfaceUniqueIdentifier surface_id, const std::shared_ptr<stratmod::InputSurface>& isptr)
{
    return pimpl_->addStratigraphicSurface(surface_id, isptr);
}

bool SModeller2::addFaultSurface(SurfaceUniqueIdentifier surface_id, const std::shared_ptr<stratmod::InputSurface>& isptr)
{
    return pimpl_->addFaultSurface(surface_id, isptr);
}

bool SModeller2::markAsStratigraphy(SurfaceUniqueIdentifier unique_id)
{
    return pimpl_->markAsStratigraphy(unique_id);
}

bool SModeller2::markAsFault(SurfaceUniqueIdentifier unique_id)
{
    return pimpl_->markAsFault(unique_id);
}

bool SModeller2::setSurfaceDescriptor(const SurfaceDescriptor& descriptor)
{
    return pimpl_->setSurfaceDescriptor(descriptor);
}

std::optional<SurfaceDescriptor> SModeller2::getSurfaceDescriptor(SurfaceUniqueIdentifier id)
{
    return pimpl_->getSurfaceDescriptor(id);
}

std::vector<SurfaceDescriptor> SModeller2::getSurfaceDescriptors(SurfaceGroupIdentifier id)
{
    return pimpl_->getSurfaceDescriptors(id);
}

std::shared_ptr<stratmod::InputSurface> SModeller2::getInputSurface(SurfaceUniqueIdentifier surface_id)
{
    return pimpl_->getInputSurface(surface_id);
}

bool SModeller2::updateSurface(SurfaceUniqueIdentifier surface_id)
{
    return pimpl_->updateSurface(surface_id);
}

bool SModeller2::updateSurface(SurfaceUniqueIdentifier surface_id, const std::shared_ptr<stratmod::InputSurface>& isptr)
{
    return pimpl_->updateSurface(surface_id, isptr);
}

/*         /////////////////////////////////////////////////////////////////////// */
/*         /1* Deprecated *1/ */
/*         /////////////////////////////////////////////////////////////////////// */

/*         bool createSurface( std::size_t surface_id, */ 
/*                 const std::vector<double> &point_data, */
/*                 double fill_distance = 0.0, */
/*                 const std::vector<std::size_t> lower_bound_ids = std::vector<std::size_t>(), */ 
/*                 const std::vector<std::size_t> upper_bound_ids = std::vector<std::size_t>() */ 
/*                 ); */

/*         bool createLengthwiseExtrudedSurface( std::size_t surface_id, */ 
/*                 const std::vector<double> &cross_section_curve_point_data, */
/*                 double fill_distance = 0.0, */
/*                 const std::vector<std::size_t> lower_bound_ids = std::vector<std::size_t>(), */
/*                 const std::vector<std::size_t> upper_bound_ids = std::vector<std::size_t>() */
/*                 ); */

/*         bool createLengthwiseExtrudedSurface( std::size_t surface_id, */ 
/*                 const std::vector<double> &cross_section_curve_point_data, double cross_section, */ 
/*                 const std::vector<double> &path_curve_point_data, */ 
/*                 double fill_distance = 0.0, */
/*                 const std::vector<std::size_t> lower_bound_ids = std::vector<std::size_t>(), */
/*                 const std::vector<std::size_t> upper_bound_ids = std::vector<std::size_t>() */
/*                 ); */

/*         bool createWidthwiseExtrudedSurface( std::size_t surface_id, */ 
/*                 const std::vector<double> &cross_section_curve_point_data, */
/*                 double fill_distance = 0.0, */
/*                 const std::vector<std::size_t> lower_bound_ids = std::vector<std::size_t>(), */
/*                 const std::vector<std::size_t> upper_bound_ids = std::vector<std::size_t>() */
/*                 ); */

/*         bool createWidthwiseExtrudedSurface( std::size_t surface_id, */ 
/*                 const std::vector<double> &cross_section_curve_point_data, double cross_section, */ 
/*                 const std::vector<double> &path_curve_point_data, */ 
/*                 double fill_distance = 0.0, */
/*                 const std::vector<std::size_t> lower_bound_ids = std::vector<std::size_t>(), */
/*                 const std::vector<std::size_t> upper_bound_ids = std::vector<std::size_t>() */
/*                 ); */

/*         bool tryCreateSurface( std::size_t surface_id, std::vector<std::size_t> &intersected_surfaces, */
/*                 const std::vector<double> &point_data, */
/*                 double fill_distance = 0.0, */
/*                 const std::vector<std::size_t> lower_bound_ids = std::vector<std::size_t>(), */ 
/*                 const std::vector<std::size_t> upper_bound_ids = std::vector<std::size_t>() */ 
/*                 ); */

/*         bool tryCreateLengthwiseExtrudedSurface( std::size_t surface_id, std::vector<std::size_t> &intersected_surfaces, */
/*                 const std::vector<double> &cross_section_curve_point_data, */
/*                 double fill_distance = 0.0, */
/*                 const std::vector<std::size_t> lower_bound_ids = std::vector<std::size_t>(), */ 
/*                 const std::vector<std::size_t> upper_bound_ids = std::vector<std::size_t>() */
/*                 ); */

/*         bool tryCreateLengthwiseExtrudedSurface( std::size_t surface_id, std::vector<std::size_t> &intersected_surfaces, */
/*                 const std::vector<double> &cross_section_curve_point_data, double cross_section, */ 
/*                 const std::vector<double> &path_curve_point_data, */ 
/*                 double fill_distance = 0.0, */
/*                 const std::vector<std::size_t> lower_bound_ids = std::vector<std::size_t>(), */
/*                 const std::vector<std::size_t> upper_bound_ids = std::vector<std::size_t>() */
/*                 ); */

/*         bool tryCreateWidthwiseExtrudedSurface( std::size_t surface_id, std::vector<std::size_t> &intersected_surfaces, */
/*                 const std::vector<double> &cross_section_curve_point_data, */
/*                 double fill_distance = 0.0, */
/*                 const std::vector<std::size_t> lower_bound_ids = std::vector<std::size_t>(), */ 
/*                 const std::vector<std::size_t> upper_bound_ids = std::vector<std::size_t>() */
/*                 ); */

/*         bool tryCreateWidthwiseExtrudedSurface( std::size_t surface_id, std::vector<std::size_t> &intersected_surfaces, */
/*                 const std::vector<double> &cross_section_curve_point_data, double cross_section, */ 
/*                 const std::vector<double> &path_curve_point_data, */ 
/*                 double fill_distance = 0.0, */
/*                 const std::vector<std::size_t> lower_bound_ids = std::vector<std::size_t>(), */
/*                 const std::vector<std::size_t> upper_bound_ids = std::vector<std::size_t>() */
/*                 ); */

 
/* bool SModeller2::createSurface( size_t surface_id, const std::vector<double> &point_data, */ 
/*                 double fill_distance, */
/*                 const std::vector<size_t> lower_bound_ids, const std::vector<size_t> upper_bound_ids ) */
/* { */
/*     bool extruded_surface = false; */ 
/*     bool orthogonally_oriented = false; */
/*     return pimpl_->insertSurface(point_data, surface_id, lower_bound_ids, upper_bound_ids, extruded_surface, orthogonally_oriented, fill_distance); */ 
/* } */

/* /1* bool SModeller2::createExtrudedSurface( size_t surface_id, const std::vector<double> &point_data ) *1/ */
/* /1* { *1/ */
/* /1*     bool extruded_surface = true; *1/ */ 
/* /1*     return pimpl_->insertSurface(point_data, surface_id, std::vector<size_t>(), std::vector<size_t>(), extruded_surface); *1/ */
/* /1* } *1/ */

/* bool SModeller2::createLengthwiseExtrudedSurface( size_t surface_id, */ 
/*         const std::vector<double> &point_data, */ 
/*         double fill_distance, */
/*         const std::vector<size_t> lower_bound_ids, */ 
/*         const std::vector<size_t> upper_bound_ids ) */
/* { */
/*     bool orthogonally_oriented = false; */ 
/*     size_t cross_section_depth = 0; */
/*     std::vector<double> empty_path = std::vector<double>(); */

/*     return pimpl_->insertExtrusionAlongPath(surface_id, */ 
/*             point_data, cross_section_depth, */ 
/*             empty_path, */ 
/*             lower_bound_ids, upper_bound_ids, orthogonally_oriented, fill_distance); */ 
/* } */


/* bool SModeller2::createLengthwiseExtrudedSurface( size_t surface_id, */ 
/*         const std::vector<double> &cross_section_curve_point_data, double cross_section_depth, */ 
/*         const std::vector<double> &path_curve_point_data, */ 
/*         double fill_distance, */
/*         const std::vector<size_t> lower_bound_ids, */
/*         const std::vector<size_t> upper_bound_ids */
/*         ) */
/* { */
/*     bool orthogonally_oriented = false; */ 
/*     return pimpl_->insertExtrusionAlongPath(surface_id, cross_section_curve_point_data, cross_section_depth, path_curve_point_data, lower_bound_ids, upper_bound_ids, orthogonally_oriented, fill_distance); */
/* } */


/* bool SModeller2::createWidthwiseExtrudedSurface( size_t surface_id, */ 
/*         const std::vector<double> &point_data, */ 
/*         double fill_distance, */
/*         const std::vector<size_t> lower_bound_ids, */ 
/*         const std::vector<size_t> upper_bound_ids ) */
/* { */
/*     bool orthogonally_oriented = true; */ 
/*     size_t cross_section_depth = 0; */
/*     std::vector<double> empty_path = std::vector<double>(); */

/*     return pimpl_->insertExtrusionAlongPath(surface_id, */ 
/*             point_data, cross_section_depth, */ 
/*             empty_path, */ 
/*             lower_bound_ids, upper_bound_ids, orthogonally_oriented, fill_distance); */ 
/* } */


/* bool SModeller2::createWidthwiseExtrudedSurface( size_t surface_id, */ 
/*         const std::vector<double> &cross_section_curve_point_data, double cross_section_depth, */ 
/*         const std::vector<double> &path_curve_point_data, */ 
/*         double fill_distance, */
/*         const std::vector<size_t> lower_bound_ids, */
/*         const std::vector<size_t> upper_bound_ids */
/*         ) */
/* { */
/*     bool orthogonally_oriented = true; */ 
/*     return pimpl_->insertExtrusionAlongPath(surface_id, cross_section_curve_point_data, cross_section_depth, path_curve_point_data, lower_bound_ids, upper_bound_ids, orthogonally_oriented, fill_distance); */
/* } */


/* bool SModeller2::tryCreateLengthwiseExtrudedSurface( size_t surface_id, std::vector<size_t> &intersected_surfaces, */
/*         const std::vector<double> &cross_section_curve_point_data, double cross_section_depth, */ 
/*         const std::vector<double> &path_curve_point_data, */ 
/*         double fill_distance, */
/*         const std::vector<size_t> lower_bound_ids, */
/*         const std::vector<size_t> upper_bound_ids */
/*         ) */
/* { */
/*     bool status = createLengthwiseExtrudedSurface(surface_id, cross_section_curve_point_data, cross_section_depth, path_curve_point_data, fill_distance, lower_bound_ids, upper_bound_ids); */

/*     if ( status == false ) */
/*     { */
/*         return false; */
/*     } */

/*     pimpl_->lastInsertedSurfaceIntersects(intersected_surfaces); */

/*     if ( ! intersected_surfaces.empty() ) */
/*     { */
/*         pimpl_->popLastSurface(); */

/*         return false; */
/*     } */

/*     return true; */
/* } */

/* bool SModeller2::tryCreateWidthwiseExtrudedSurface( size_t surface_id, std::vector<size_t> &intersected_surfaces, */
/*         const std::vector<double> &cross_section_curve_point_data, double cross_section_depth, */ 
/*         const std::vector<double> &path_curve_point_data, */ 
/*         double fill_distance, */
/*         const std::vector<size_t> lower_bound_ids, */
/*         const std::vector<size_t> upper_bound_ids */
/*         ) */
/* { */
/*     bool status = createWidthwiseExtrudedSurface(surface_id, cross_section_curve_point_data, cross_section_depth, path_curve_point_data, fill_distance, lower_bound_ids, upper_bound_ids); */

/*     if ( status == false ) */
/*     { */
/*         return false; */
/*     } */

/*     pimpl_->lastInsertedSurfaceIntersects(intersected_surfaces); */

/*     if ( ! intersected_surfaces.empty() ) */
/*     { */
/*         pimpl_->popLastSurface(); */

/*         return false; */
/*     } */

/*     return true; */
/* } */


/* /1* bool SModeller2::tryCreateSurface( size_t surface_id, const std::vector<double> &point_data, std::vector<size_t> &intersected_surfaces ) *1/ */
/* /1* { *1/ */
/* /1*     return tryCreateSurface(surface_id, point_data, std::vector<size_t>(), std::vector<size_t>(), intersected_surfaces); *1/ */
/* /1* } *1/ */

/* bool SModeller2::tryCreateSurface( size_t surface_id, std::vector<size_t> &intersected_surfaces, */ 
/*         const std::vector<double> &point_data, double fill_distance, */ 
/*         const std::vector<size_t> lower_bound_ids, const std::vector<size_t> upper_bound_ids ) */
/* { */
/*     /1* State current = pimpl_->current_.state_; *1/ */
/*     /1* pimpl_->current_.state_ = State::SKETCHING; *1/ */

/*     /1* std::cout << "Trying to create a surface...\n"; *1/ */ 
/*     bool status = createSurface(surface_id, point_data, fill_distance, lower_bound_ids, upper_bound_ids); */ 

/*     /1* pimpl_->current_.state_ = current; *1/ */ 

/*     if ( status == false ) */
/*     { */
/*         return false; */
/*     } */

/*     pimpl_->lastInsertedSurfaceIntersects(intersected_surfaces); */

/*     if ( ! intersected_surfaces.empty() ) */
/*     { */
/*         /1* std::cout << "But it intersected another surface...\n"; *1/ */
/*         pimpl_->popLastSurface(); */

/*         return false; */
/*     } */

/*     /1* std::cout << "Success!\n"; *1/ */

/*     return true; */
/* } */

/* /1* bool SModeller2::tryCreateExtrudedSurface( size_t surface_id, const std::vector<double> &point_data, std::vector<size_t> &intersected_surfaces ) *1/ */
/* /1* { *1/ */
/* /1*     return tryCreateExtrudedSurface(surface_id, point_data, std::vector<size_t>(), std::vector<size_t>(), intersected_surfaces); *1/ */
/* /1* } *1/ */

/* bool SModeller2::tryCreateLengthwiseExtrudedSurface( size_t surface_id, std::vector<size_t> &intersected_surfaces, */
/*         const std::vector<double> &point_data, */
/*         double fill_distance, */
/*         const std::vector<size_t> lower_bound_ids, */ 
/*         const std::vector<size_t> upper_bound_ids ) */
/* { */
/*     /1* State current = pimpl_->current_.state_; *1/ */
/*     /1* pimpl_->current_.state_ = State::SKETCHING; *1/ */

/*     bool status = createLengthwiseExtrudedSurface(surface_id, point_data, fill_distance, lower_bound_ids, upper_bound_ids); */ 

/*     /1* pimpl_->current_.state_ = current; *1/ */ 

/*     if ( status == false ) */
/*     { */
/*         return false; */
/*     } */

/*     pimpl_->lastInsertedSurfaceIntersects(intersected_surfaces); */

/*     if ( ! intersected_surfaces.empty() ) */
/*     { */
/*         pimpl_->popLastSurface(); */

/*         return false; */
/*     } */

/*     return true; */
/* } */

/* bool SModeller2::tryCreateWidthwiseExtrudedSurface( size_t surface_id, std::vector<size_t> &intersected_surfaces, */
/*         const std::vector<double> &point_data, */
/*         double fill_distance, */
/*         const std::vector<size_t> lower_bound_ids, */ 
/*         const std::vector<size_t> upper_bound_ids ) */
/* { */
/*     /1* State current = pimpl_->current_.state_; *1/ */
/*     /1* pimpl_->current_.state_ = State::SKETCHING; *1/ */

/*     bool status = createWidthwiseExtrudedSurface(surface_id, point_data, fill_distance, lower_bound_ids, upper_bound_ids); */ 

/*     /1* pimpl_->current_.state_ = current; *1/ */ 

/*     if ( status == false ) */
/*     { */
/*         return false; */
/*     } */

/*     pimpl_->lastInsertedSurfaceIntersects(intersected_surfaces); */

/*     if ( ! intersected_surfaces.empty() ) */
/*     { */
/*         pimpl_->popLastSurface(); */

/*         return false; */
/*     } */

/*     return true; */
/* } */

bool SModeller2::canUndo()
{
    return pimpl_->canUndo();
    /* if ( pimpl_->container_.size() > 0 ) */
    /* { */
    /*     return true; */
    /* } */

    /* return false; */
}

bool SModeller2::undo()
{
    return pimpl_->undo();
    /* if ( canUndo() == false ) */
    /* { */
    /*     return false; */ 
    /* } */

    /* PlaninASurface::Ptr last_sptr; */ 
    /* pimpl_->container_.popLastSurface(last_sptr); */

    /* size_t last_surface_index = pimpl_->inserted_surfaces_indices_.back(); */ 
    /* pimpl_->inserted_surfaces_indices_.pop_back(); */ 

    /* StateDescriptor last = pimpl_->past_states_.back(); */
    /* pimpl_->past_states_.pop_back(); */

    /* pimpl_->current_.bounded_above_ = last.bounded_above_; */
    /* pimpl_->current_.upper_boundary_list_ = last.upper_boundary_list_; */
    /* pimpl_->current_.bounded_below_ = last.bounded_below_; */
    /* pimpl_->current_.lower_boundary_list_ = last.lower_boundary_list_; */
    /* // pimpl_->current_ = last; */ 
    /* pimpl_->enforceDefineRegion(); */

    /* pimpl_->undoed_surfaces_stack_.push_back(last_sptr); */ 
    /* pimpl_->undoed_surfaces_indices_.push_back(last_surface_index); */ 

    /* pimpl_->undoed_states_.push_back(last); */

    /* auto iter = pimpl_->dictionary_.find(last_surface_index); */ 
    /* pimpl_->dictionary_.erase(iter); */ 

    /* // Cache was updated in call to SRules::popLastSurface() */
    /* /1* pimpl_->container_.updateCache(); *1/ */

    /* pimpl_->mesh_ = nullptr; */

    /* return true; */
}

/* [[deprecated]] */
/* bool popUndoStack(); */

/* bool SModeller2::popUndoStack() */
/* { */
/*     return pimpl_->popUndoStack(); */
/* } */

bool SModeller2::canRedo()
{
    return pimpl_->canRedo();
    /* if ( pimpl_->undoed_surfaces_stack_.size() > 0 ) */
    /* { */
    /*     return true; */
    /* } */

    /* return false; */
}

bool SModeller2::redo()
{
    return pimpl_->redo();
    /* if ( canRedo() == false ) */
    /* { */
    /*     return false; */
    /* } */

    /* PlaninASurface::Ptr undoed_sptr = pimpl_->undoed_surfaces_stack_.back(); */ 
    /* pimpl_->undoed_surfaces_stack_.pop_back(); */ 

    /* size_t surface_index = pimpl_->undoed_surfaces_indices_.back(); */
    /* pimpl_->undoed_surfaces_indices_.pop_back(); */

    /* /1* StateDescriptor state_before_redo_ = pimpl_->current_; *1/ */
    /* pimpl_->current_ = pimpl_->undoed_states_.back(); */
    /* pimpl_->undoed_states_.pop_back(); */
    /* pimpl_->enforceDefineRegion(); */

    /* std::vector<size_t> lbounds, ubounds; */
    /* bool status = pimpl_->parseTruncateSurfaces(lbounds, ubounds); */ 

    /* /1* pimpl_->container_.updateCache(); *1/ */

    /* if ( status == true ) */
    /* { */
    /*     status = pimpl_->commitSurface(undoed_sptr, surface_index, lbounds, ubounds); */ 
    /* } */


    /* /1* bool status = commitSurface(undoed_sptr, surface_index, std::vector<size_t>(), std::vector<size_t>()); *1/ */

    /* /1* pimpl_->current_ = state_before_redo_; *1/ */
    /* /1* pimpl_->enforceDefineRegion(); *1/ */

    /* return status; */
}

bool SModeller2::destroyLastSurface()
{
    if ( !canUndo() )
    {
        return false;
    }

    undo();
    return pimpl_->popUndoStack(); // must return true
}

/* [[deprecated]] */
/* bool getVertexList( std::size_t surface_id, std::vector<float> &vlist ); */

/* bool SModeller2::getVertexList( size_t surface_id, std::vector<float> &vlist ) */
/* { */
/*     return pimpl_->getVertexList(surface_id, vlist); */
/* } */

/* [[deprecated]] */
/* bool getVertexList( std::size_t surface_id, std::vector<double> &vlist ); */

/* bool SModeller2::getVertexList( size_t surface_id, std::vector<double> &vlist ) */
/* { */
/*     return pimpl_->getVertexList(surface_id, vlist); */
/* } */

/* [[deprecated]] */
/* bool getMesh( std::size_t surface_id, std::vector<float> &vlist, std::vector<unsigned int> &flist ); */

/* bool SModeller2::getMesh( size_t surface_id, std::vector<float> &vlist, std::vector<unsigned int> &flist ) */
/* { */
/*     return pimpl_->getMesh(surface_id, vlist, flist); */
/* } */

bool SModeller2::getCachedMesh( size_t surface_id, std::vector<double> &vlist, std::vector<size_t> &flist )
{
    return pimpl_->getMesh(surface_id, vlist, flist);
}

/* [[deprecated]] */
/* bool getWidthCrossSectionCurve( std::size_t surface_id, std::size_t width, std::vector<float> &vlist, std::vector<unsigned int> &elist ); */

/* bool SModeller2::getWidthCrossSectionCurve( size_t surface_id, size_t width, std::vector<float> &vlist, std::vector<unsigned int> &elist ) */
/* { */
/*     return pimpl_->getCrossSectionWidth(surface_id, vlist, elist, width); */
/*     /1* return pimpl_->getAdaptedCrossSectionAtConstantWidth(surface_id, vlist, elist, width); *1/ */
/* } */

bool SModeller2::adaptSurfacesMeshes(const std::vector<SurfaceUniqueIdentifier>& surfaces_indices)
{
    std::vector<std::size_t> sids;
    std::size_t index;
    for (std::size_t i = 0; i < surfaces_indices.size(); ++i)
    {
        if (pimpl_->getSurfaceIndex(surfaces_indices[i], index) == false)
        {
            return false;
        }

        pimpl_->clearCSCache(surfaces_indices[i]);
        sids.push_back(index);
    }

    return pimpl_->container_.adaptSurfacesMeshes(sids);
}

bool SModeller2::getAdaptedMesh( std::size_t surface_id, std::vector<double> &vlist, std::vector<std::size_t> &tlist )
{
    size_t index; 
    if ( pimpl_->getSurfaceIndex(surface_id, index) == false )
    {
        return false; 
    }

    auto num_triangles = pimpl_->container_.getAdaptedMesh(index, vlist, tlist);

    return (num_triangles > 0);
}

bool SModeller2::getAdaptedPartialMesh( std::size_t surface_id, std::vector<double> &vlist, std::vector<std::size_t> &tlist )
{
    size_t index; 
    if ( pimpl_->getSurfaceIndex(surface_id, index) == false )
    {
        return false; 
    }

    auto num_triangles = pimpl_->container_.getAdaptedPartialMesh(index, vlist, tlist);

    return (num_triangles > 0);
}

bool SModeller2::getCachedWidthCrossSectionCurve( size_t surface_id, size_t width, std::vector<double> &vlist, std::vector<size_t> &elist )
{
    return pimpl_->getCrossSectionWidth(surface_id, vlist, elist, width);
    /* return pimpl_->getAdaptedCrossSectionAtConstantWidth(surface_id, vlist, elist, width); */
}

bool SModeller2::getAdaptedWidthCrossSectionCurve( size_t surface_id, size_t width, std::vector<double> &vlist, std::vector<size_t> &elist )
{
    static std::mutex m_;
    std::lock_guard<std::mutex> g(m_);

    size_t index; 
    if ( pimpl_->getSurfaceIndex(surface_id, index) == false )
    {
        return false; 
    }

    auto& cs_cache = pimpl_->width_adapted_cs_;
    auto cs_iter = cs_cache.find(surface_id);
    if (cs_iter != cs_cache.end())
    {
        auto iter = cs_iter->second.find(width);
        if (iter != cs_iter->second.end())
        {
            std::tie(vlist, elist) = iter->second;
            return (elist.size() > 0);
        }
    }

    // std::array<double, 3> morigin, msize;
    // pimpl_->getOrigin(morigin[0], morigin[1], morigin[2]);
    // pimpl_->getLenght(msize[0], msize[1], msize[2]);
    auto morigin = pimpl_->origin_;
    auto msize = pimpl_->lenght_;
    Eigen::Vector3d P, T;
    P << static_cast<double>(width)/(pimpl_->discWidth_)*msize[0] + morigin[0], 0., 0.;
    T << 0.0, 1.0, 0.0;

    auto num_edges = pimpl_->container_.getAdaptedCrossSection(index, P, T, vlist, elist);
    cs_cache[surface_id][width] = std::make_tuple(vlist, elist);

    return (num_edges > 0);
}

bool SModeller2::getAdaptedPartialWidthCrossSectionCurve( size_t surface_id, size_t width, std::vector<double> &vlist, std::vector<size_t> &elist )
{
    static std::mutex m_;
    std::lock_guard<std::mutex> g(m_);

    size_t index; 
    if ( pimpl_->getSurfaceIndex(surface_id, index) == false )
    {
        return false; 
    }

    auto& cs_cache = pimpl_->width_partial_cs_;
    auto cs_iter = cs_cache.find(surface_id);
    if (cs_iter != cs_cache.end())
    {
        auto iter = cs_iter->second.find(width);
        if (iter != cs_iter->second.end())
        {
            std::tie(vlist, elist) = iter->second;
            return (elist.size() > 0);
        }
    }

    // std::array<double, 3> morigin, msize;
    // pimpl_->getOrigin(morigin[0], morigin[1], morigin[2]);
    // pimpl_->getLenght(msize[0], msize[1], msize[2]);
    auto morigin = pimpl_->origin_;
    auto msize = pimpl_->lenght_;
    Eigen::Vector3d P, T;
    P << static_cast<double>(width)/(pimpl_->discWidth_)*msize[0] + morigin[0], 0., 0.;
    T << 0.0, 1.0, 0.0;

    auto num_edges = pimpl_->container_.getAdaptedPartialCrossSection(index, P, T, vlist, elist);
    cs_cache[surface_id][width] = std::make_tuple(vlist, elist);

    return (num_edges > 0);
}

/* [[deprecated]] */
/* bool getLengthCrossSectionCurve( std::size_t surface_id, std::size_t lenght, std::vector<float> &vlist, std::vector<unsigned int> &elist ); */

/* bool SModeller2::getLengthCrossSectionCurve( size_t surface_id, size_t length, std::vector<float> &vlist, std::vector<unsigned int> &elist ) */
/* { */
/*     return pimpl_->getCrossSectionDepth(surface_id, vlist, elist, length); */
/*     /1* return pimpl_->getAdaptedCrossSectionAtConstantLength(surface_id, vlist, elist, length); *1/ */
/* } */

bool SModeller2::getCachedLengthCrossSectionCurve( size_t surface_id, size_t length, std::vector<double> &vlist, std::vector<size_t> &elist )
{
    return pimpl_->getCrossSectionDepth(surface_id, vlist, elist, length);
    /* return pimpl_->getAdaptedCrossSectionAtConstantLength(surface_id, vlist, elist, length); */
}

bool SModeller2::getAdaptedLengthCrossSectionCurve( size_t surface_id, size_t length, std::vector<double> &vlist, std::vector<size_t> &elist )
{
    static std::mutex m_;
    std::lock_guard<std::mutex> g(m_);

    size_t index; 
    if ( pimpl_->getSurfaceIndex(surface_id, index) == false )
    {
        return false; 
    }

    auto& cs_cache = pimpl_->length_adapted_cs_;
    auto cs_iter = cs_cache.find(surface_id);
    if (cs_iter != cs_cache.end())
    {
        auto iter = cs_iter->second.find(length);
        if (iter != cs_iter->second.end())
        {
            std::tie(vlist, elist) = iter->second;
            return (elist.size() > 0);
        }
    }

    // std::array<double, 3> morigin, msize;
    // pimpl_->getOrigin(morigin[0], morigin[1], morigin[2]);
    // pimpl_->getLenght(msize[0], msize[1], msize[2]);
    auto morigin = pimpl_->origin_;
    auto msize = pimpl_->lenght_;
    Eigen::Vector3d P, T;
    P << 0., static_cast<double>(length)/(pimpl_->discWidth_)*msize[1] + morigin[1], 0.;
    T << 1.0, 0.0, 0.0;

    auto num_edges = pimpl_->container_.getAdaptedCrossSection(index, P, T, vlist, elist);
    cs_cache[surface_id][length] = std::make_tuple(vlist, elist);

    return (num_edges > 0);
}

bool SModeller2::getAdaptedPartialLengthCrossSectionCurve( size_t surface_id, size_t length, std::vector<double> &vlist, std::vector<size_t> &elist )
{
    static std::mutex m_;
    std::lock_guard<std::mutex> g(m_);

    size_t index; 
    if ( pimpl_->getSurfaceIndex(surface_id, index) == false )
    {
        return false; 
    }

    auto& cs_cache = pimpl_->length_partial_cs_;
    auto cs_iter = cs_cache.find(surface_id);
    if (cs_iter != cs_cache.end())
    {
        auto iter = cs_iter->second.find(length);
        if (iter != cs_iter->second.end())
        {
            std::tie(vlist, elist) = iter->second;
            return (elist.size() > 0);
        }
    }

    // std::array<double, 3> morigin, msize;
    // pimpl_->getOrigin(morigin[0], morigin[1], morigin[2]);
    // pimpl_->getLenght(msize[0], msize[1], msize[2]);
    auto morigin = pimpl_->origin_;
    auto msize = pimpl_->lenght_;
    Eigen::Vector3d P, T;
    P << 0., static_cast<double>(length)/(pimpl_->discWidth_)*msize[1] + morigin[0], 0.;
    T << 1.0, 0.0, 0.0;

    auto num_edges = pimpl_->container_.getAdaptedPartialCrossSection(index, P, T, vlist, elist);
    cs_cache[surface_id][length] = std::make_tuple(vlist, elist);

    return (num_edges > 0);
}


/* [[deprecated]] */
/* std::size_t getTetrahedralMesh( std::vector<double> &vertex_coordinates, std::vector<std::vector<std::size_t>> &element_list ); */

/* std::size_t SModeller2::getTetrahedralMesh( std::vector<double> &vertex_coordinates, std::vector< std::vector<std::size_t> > &element_list ) */
/* { */
/*     /1* TetrahedralMeshBuilder mb(pimpl_->container_); *1/ */
/*     if ( pimpl_->buildTetrahedralMesh() == false ) */
/*     { */
/*         return 0; */
/*     } */

/*     bool status = true; */ 
/*     size_t num_elements; */

/*     status &= (pimpl_->mesh_->getVertexCoordinates(vertex_coordinates) > 0); */

/*     if ( status == false ) */
/*     { */
/*         return 0; */
/*     } */


/*     num_elements = pimpl_->mesh_->getTetrahedronList(element_list); */

/*     return num_elements; */
/* } */

bool SModeller2::buildCachedTetrahedralMesh()
{
    return pimpl_->buildFastTetrahedralMesh();
}

bool SModeller2::buildAdaptedTetrahedralMesh()
{
    return pimpl_->buildAdaptedTetrahedralMesh();
}

std::size_t SModeller2::getTetrahedralMesh( std::vector<double> &vertex_coordinates, std::vector<std::size_t> &element_list, std::vector<long int> &attribute_list )
{
    if ( pimpl_->buildTetrahedralMesh() == false )
    {
        return 0;
    }

    auto [num_vertices, num_elements] = pimpl_->mesh_->getTetrahedralMesh(vertex_coordinates, element_list, attribute_list);
    return num_elements;
}

        /* bool getNormalList( std::size_t surface_id, std::vector<double> &normals ); */

        /* bool getExtrusionPath( std::size_t surface_id, std::vector<double> &path_vertices ); */

/* bool SModeller2::getNormalList( std::size_t surface_id, std::vector<double> &normal_list ) */
/* { */
    /* size_t index; */ 
    /* if ( pimpl_->getSurfaceIndex(surface_id, index) == false ) */
    /* { */
        /* return false; */ 
    /* } */

    /* bool status = pimpl_->container_[index]->getNormalList(normal_list); */

    /* return status; */

/* } */

/* [[deprecated]] */
/* bool SModeller2::getExtrusionPath( std::size_t surface_id, std::vector<double> & ) */
/* { */
    /* size_t index; */ 
    /* if ( pimpl_->getSurfaceIndex(surface_id, index) == false ) */
    /* { */
        /* return false; */ 
    /* } */

    /* /1* return pimpl_->container_[index]->getRawPathVertexList(path_vertex_list); *1/ */
    /* return false; */
/* } */

bool SModeller2::computeTetrahedralMeshVolumes( std::vector<double> &vlist )
{
    if ( pimpl_->buildTetrahedralMesh() == false )
    {
        return false;
    }

    return pimpl_->mesh_->getRegionVolumeList(vlist);
}

bool SModeller2::getVolumeAttributesFromPointList( const std::vector<double> &vcoords, std::vector<int> &attribute_list)
{
    if ( vcoords.size() % 3 != 0 )
    {
        return false;
    }

    size_t num_points = vcoords.size()/3;

    Point3 p;
    std::vector<Point3> point_list(num_points);

    for ( size_t i = 0; i < num_points; ++i )
    {
        p = pimpl_->point3(vcoords[3*i + 0], vcoords[3*i + 1], vcoords[3*i + 2]);
        point_list[i] = p;
    }

    if ( pimpl_->buildTetrahedralMesh() == false )
    {
        return false;
    }

    bool status = pimpl_->mesh_->mapPointsToRegions(point_list, attribute_list);

    return status;
}

bool SModeller2::getBoundingSurfacesFromVolumeAttribute( std::size_t attribute_id, std::vector<size_t> &lower_bound, std::vector<size_t> &upper_bound)
{
    return pimpl_->getBoundingSurfacesFromRegionID(attribute_id, lower_bound, upper_bound);
}

std::vector<size_t> SModeller2::getSurfacesIndicesBelowPoint( double x, double y, double z )
{
    return pimpl_->getSurfacesIndicesBelowPoint(x, y, z);
}

std::vector<size_t> SModeller2::getSurfacesIndicesAbovePoint( double x, double y, double z )
{
    return pimpl_->getSurfacesIndicesAbovePoint(x, y, z);
}

ModelInterpretation SModeller2::getRegions()
{
    ModelInterpretation regions_as_domains;
    int max_num_regions = pimpl_->maxNumRegions();

    if (max_num_regions < 1)
    {
        return regions_as_domains;
    }

    for (int i = 0; i < max_num_regions; ++i)
    {
        Region r_i = Region::Get(i).value();
        Domain d_i;
        d_i.regions.insert(r_i);
        regions_as_domains.setDomain(i, d_i);
    }

    return regions_as_domains;
}

std::unordered_map<std::string, ModelInterpretation>& SModeller2::interpretations()
{
    return pimpl_->interpretations_;
}

bool SModeller2::scaleModelHeight(double scale)
{
    return pimpl_->scaleModelHeight(scale);
}

#if defined(BUILD_WITH_SERIALIZATION)
bool SModeller2::saveBinary( std::string filename )
{
        std::ofstream ofs(filename, std::ios::binary);
        /* std::ofstream ofs(filename); */

        if ( !ofs.good() )
        {
            return false; 
        }
        pimpl_->saved_model_filename_ = filename;

        cereal::PortableBinaryOutputArchive oarchive(ofs);
        /* cereal::XMLOutputArchive oarchive(ofs); */
        /* cereal::JSONOutputArchive oarchive(ofs); */

        unsigned int version = 2;

        try 
        {
            oarchive( cereal::make_nvp("SModeller2 version", version), 
                      cereal::make_nvp("SModeller2 data", *pimpl_) 
                    );
        }
        catch( const std::exception &e )
        {
            /* std::cerr << "Exception caught while trying to save file: " << e.what() << std::endl << std::flush; */

            pimpl_->saved_model_filename_ = std::make_optional<std::string>();
            return false;
        }
        catch(...)
        {
            /* std::cerr << "Unknown exception caught in method SModeller2::saveBinary(...)\n\n" << std::flush; */

            pimpl_->saved_model_filename_ = std::make_optional<std::string>();
            return false;
        }

        return true;
}

bool SModeller2::saveJSON( std::string filename )
{
        /* std::ofstream ofs(filename, std::ios::binary); */
        std::ofstream ofs(filename);

        if ( !ofs.good() )
        {
            return false; 
        }
        pimpl_->saved_model_filename_ = filename;

        /* cereal::PortableBinaryOutputArchive oarchive(ofs); */
        /* cereal::XMLOutputArchive oarchive(ofs); */
        cereal::JSONOutputArchive oarchive(ofs);

        unsigned int version = 2;

        try 
        {
            oarchive( cereal::make_nvp("SModeller2 version", version), 
                      cereal::make_nvp("SModeller2 data", *pimpl_) 
                    );
        }
        catch( const std::exception &e )
        {
            /* std::cerr << "Exception caught while trying to save file: " << e.what() << std::endl << std::flush; */

            pimpl_->saved_model_filename_ = std::make_optional<std::string>();
            return false;
        }
        catch(...)
        {
            /* std::cerr << "Unknown exception caught in method SModeller2::saveJSON(...)\n\n" << std::flush; */

            pimpl_->saved_model_filename_ = std::make_optional<std::string>();
            return false;
        }

        return true;
}

bool SModeller2::loadBinary( std::string filename )
{
        clear();

        std::ifstream ifs(filename, std::ios::binary);
        /* std::ifstream ifs(filename); */

        if ( !ifs.good() )
        {
            return false;
        }

        cereal::PortableBinaryInputArchive iarchive(ifs);
        /* cereal::XMLInputArchive iarchive(ifs); */
        /* cereal::JSONInputArchive iarchive(ifs); */

        unsigned int version;

        /* std::cout << "Trying to load binary file: " + filename + " >> "; */
        try
        {
            iarchive( version, *pimpl_ );
        }
        catch( const std::exception &e )
        {
            /* std::cout << "failure\n"; */
            /* std::cerr << "Exception caught while trying to load file: " << e.what() << std::endl << std::flush; */
            clear();

            return false;
        }
        catch(...)
        {
            /* std::cout << "failure\n"; */
            /* std::cerr << "Unknown exception caught in method SModeller2::loadBinary(...)\n\n" << std::flush; */
            clear();

            return false;
        }
        /* std::cout << "success\n" << std::flush; */
        pimpl_->saved_model_filename_ = filename;


        return true;
}

bool SModeller2::loadJSON( std::string filename )
{
        clear();

        std::ifstream ifs(filename);

        if ( !ifs.good() )
        {
            return false;
        }

        /* cereal::XMLInputArchive iarchive(ifs); */
        cereal::JSONInputArchive iarchive(ifs);

        unsigned int version;

        /* std::cout << "Trying to load JSON file: " + filename + " >> "; */
        try
        {
            iarchive( version, *pimpl_ );
        }
        catch( const std::exception &e )
        {
            std::cout << "failure\n";
            /* std::cerr << "Exception caught while trying to load file: " << e.what() << std::endl << std::flush; */
            clear();

            return false;
        }
        catch(...)
        {
            std::cout << "failure\n";
            /* std::cerr << "Unknown exception caught in method SModeller2::loadJSON(...)\n\n" << std::flush; */
            clear();

            return false;
        }
        /* std::cout << "success\n" << std::flush; */
        pimpl_->saved_model_filename_ = filename;


        return true;
}

#else //defined(BUILD_WITH_SERIALIZATION)
bool SModeller2::saveBinary( std::string )
{
    return false;
}

bool SModeller2::loadBinary( std::string )
{
    return false;
}

bool SModeller2::saveJSON( std::string )
{
    return false;
}

bool SModeller2::loadBinary( std::string )
{
    return false;
}

#endif //defined(BUILD_WITH_SERIALIZATION)

} // namespace stratmod 
