/******************************************************************************/
/*                                                                            */
/* This file is part of the "Planar Interpolation Library" (PlanInLib)        */
/* Copyright (C) 2016, Julio Daniel Machado Silva.                            */
/*                                                                            */
/* PlanInLib is free software; you can redistribute it and/or                 */
/* modify it under the terms of the GNU Lesser General Public                 */
/* License as published by the Free Software Foundation; either               */
/* version 3 of the License, or (at your option) any later version.           */
/*                                                                            */
/* PlanInLib is distributed in the hope that it will be useful,               */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          */
/* Lesser General Public License for more details.                            */
/*                                                                            */
/* You should have received a copy of the GNU Lesser General Public           */
/* License along with PlanInLib.  If not, see <http://www.gnu.org/licenses/>, */
/* or write to the Free Software Foundation, Inc., 51 Franklin Street,        */
/* Fifth Floor, Boston, MA  02110-1301  USA.                                  */
/*                                                                            */
/******************************************************************************/


#ifndef PLANIN_SRULES_HPP
#define PLANIN_SRULES_HPP

#include <cstdint>
#include <mutex>
#include <set>
#include <unordered_map>
#include <vector>

#include "core.hpp"
#include "meshing_helper.hpp"
#include "planar_surface.hpp"
#include "planin/serialization_primitives.hpp"
#include "planin/planar_surface.hpp" 
#include "planin/legacy_planar_surface.hpp" 
#include "planin/mesh/meshing_helper.hpp"
#include "planin/eigen_converters.hpp"
#include "triangle_soup_wrapper.hpp"

#include <igl/barycenter.h>
#include <igl/boundary_facets.h>
#include <igl/winding_number.h>

/** 
 * \class SRules \anchor SRules
 * 
 * \brief Simple stratigraphic rules aware surfaces container. 
 *
 * This class implements a container and a basic set of stratigraphic rules, 
 * which are applied to the container's surfaces. 
 *
 */
template<typename T>
class SRules
{
    public: 
        /** \brief The container's type. A simple vector of PlanarSurfaces's (smart) shared pointers.*/ 
        using ContainerType = std::vector<typename T::Ptr>; 

        /** \brief Type of the dictionary that translates PlanarSurface's SurfaceIds to this->container indices. */ 
        using MapType = std::unordered_map<typename T::SurfaceId, size_t>; 

        /** \brief Change surfaces' discretization. 
         *
         * \param numX: number of intervals to discretize the width of the model
         * \param numY: number of intervals to discretize the length of the model
         * \return True if the discretizations were changed. 
         */
        bool changeDiscretization(size_t numX, size_t numY);

        /** \brief Clear the container. 
         */
        void clear(); 

        /** \brief Overloading of 'operator[]'. 
         *
         * \param surface_index: index of desired surface. 
         * \return A reference to the surface's shared pointer at index 'surface_index'. 
         */
        typename T::Ptr &operator[]( std::size_t surface_index ); 

        /** \brief Container's size. 
         *
         * \return Size of the container. 
         */
        std::size_t size(); 
        
        /** \brief Query whether the container is empty. 
         *
         * \return True if the container is empty. 
         */
        bool empty(); 
        
        /** \brief Translates a PlanarSurface's SurfaceId to the surface's index in the container. 
         *
         * \param id: surface's id. 
         * \param surface_index: index of the surface in the container, output. 
         *
         * \return Whether the surface was found in the container or not. 
         */ 
        bool getSurfaceIndex( const typename T::SurfaceId id, size_t &surface_index ); 


        /** \brief Set the bounding box's origin for all PlanarSurfaces
         *
         * \param origin: bounding box's origin. 
         *
         * \return Whether the bounding box's origin was set or not. 
         */ 
        void setOrigin( Point3 origin );

        /** \brief Set the bounding box's length for all PlanarSurfaces
         *
         * \param length: bounding box's length. 
         *
         * \return Whether the bounding box's length was set or not. 
         */ 
        bool setLength( Point3 p );

        /** \brief Set the bounding box for all PlanarSurfaces
         *
         * \param origin: bounding box's origin. 
         * \param length: bounding box's length. 
         *
         * \return Whether the bounding box was set or not. 
         */ 
        /* TODO: move logic for handling the bounding box from class PlanarSurface to here. */
        bool setBoundingBox( const Point3 &origin, const Point3 &lenght ); 

        bool updateSurface(const std::shared_ptr<ISurface>& isptr, std::size_t surface_index);

        /** \brief Add a new surface to the container. 
         *
         * \param sptr: reference to a shared pointer, pointing to the surface. Warning: the surface is moved from this pointer. 
         *
         * \return Whether the surface was correctly inserted or not. The surface will not be inserted in the container if 
         * it (the surface) is not defined. 
         */
        bool addSurface( typename T::Ptr &sptr ); 

        /** \brief Add a new surface to the container. 
         *
         * \param sptr: reference to a shared pointer, pointing to the surface. Warning: the surface is moved from this pointer. 
         * \param surface_index: index of the new surface in the container. 
         *
         * \return Whether the surface was correctly inserted or not. The surface will not be inserted in the container if 
         * it (the surface) is not defined. 
         */
        bool addSurface( typename T::Ptr &sptr, std::size_t &surface_index ); 

        /** \brief Add a new surface to the container. 
         *
         * \param sptr: reference to a shared pointer, pointing to the surface. Warning: the surface is moved from this pointer. 
         * \param surface_index: index of the new surface in the container. 
         * \param remove_above_surfaces: list of surface's indices from which the inserted surface should be "removed above". 
         * \param remove_below_surfaces: list of surface's indices from which the inserted surface should be "removed below". 
         *
         * \return Whether the surface was correctly inserted or not. The surface will neither be inserted in the container if 
         * it (the surface) is not defined, nor if any of the gelological rules fail to apply.  
         */
        bool addSurface( 
                typename T::Ptr &sptr, 
                std::size_t &surface_index, 
                std::vector<size_t> remove_above_surfaces, 
                std::vector<size_t> remove_below_surfaces 
            ); 

        /** \brief Returns a list with all surfaces that intersect the last surface added to the container. 
         *
         * \param intersection_list: Vector with the indices of the intersecting surfaces. 
         *
         * \return False if the container was empty, True otherwise. 
         */
        bool lastInsertedSurfaceIntersects( std::vector<std::size_t> &intersection_list ); 

        /** \brief Returns a list with all surfaces that intersect the given surface. 
         *
         * \param sptr: Reference to shared pointer pointing to the given surface. 
         * \param intersection_list: Vector with the indices of the intersecting surfaces. 
         *
         * \return False if the container was empty, True otherwise. 
         */
        bool getIntersectionList( const typename T::Ptr &sptr, std::vector<std::size_t> &intersection_list ); 

        /** \brief Remove last added surface to the container. 
         *
         * \return False if the container was empty, True otherwise. 
         */
        bool popLastSurface(); 

        /** \brief Remove given surface from the container. 
         *
         * \param surface: shared pointer pointing to the surface to be removed from the container. 
         *
         * \return False if the container was empty, True otherwise. 
         */
        bool popLastSurface( typename T::Ptr &surface ); 


        /** \brief Disalow surfaces from being defined below the last inserted surface.  
         */
        bool defineAbove(); 
        /** \brief Stop the 'defineAbove' restriction. 
         */ 
        void stopDefineAbove();  


        /** \brief Disalow surfaces from being defined above the last inserted surface.  
         */
        bool defineBelow(); 
        /** \brief Stop the 'defineBelow' restriction. 
         */ 
        void stopDefineBelow(); 


        /** \brief Stratigraphic rule.
         */
        bool removeAbove(); 

        /** \brief Stratigraphic rule.
         */
        bool removeAboveIntersection(); 

        /** \brief Stratigraphic rule.
         */
        bool removeBelow(); 

        /** \brief Stratigraphic rule.
         */
        bool removeBelowIntersection(); 

        std::vector<size_t> getSurfacesBelowPoint( const Point3 &p );

        std::vector<size_t> getSurfacesBelowPoint( Point3 &&p );

        std::vector<size_t> getSurfacesAbovePoint( const Point3 &p );

        std::vector<size_t> getSurfacesAbovePoint( Point3 &&p );

        // return empty descriptor if outside Bounding Box
        std::vector<size_t> getActiveSurfacesBelowPoint( const Point3 &p );

        // return empty descriptor if outside Bounding Box
        std::vector<size_t> getActiveSurfacesBelowPoint( Point3 &&p );

        // return empty descriptor if outside Bounding Box
        std::vector<size_t> getActiveSurfacesAbovePoint( const Point3 &p );

        // return empty descriptor if outside Bounding Box
        std::vector<size_t> getActiveSurfacesAbovePoint( Point3 &&p );

        std::vector<size_t> getLowerBound( std::vector<size_t> surface_ids );

        std::vector<size_t> getUpperBound( std::vector<size_t> surface_ids );

        template<typename VertexList, typename FaceList, typename NormalList>
        size_t getAdaptedMesh( size_t surface_id, VertexList &vlist, FaceList &flist, NormalList &nlist );

        template<typename VertexList, typename FaceList>
        bool getLowerBoundary( const std::vector<VertexList> &vlists, const std::vector<FaceList> &flists, VertexList &boundary_vlist, FaceList &boundary_flist );

        template<typename VertexList, typename FaceList>
        bool getUpperBoundary( const std::vector<VertexList> &vlists, const std::vector<FaceList> &flists, VertexList &boundary_vlist, FaceList &boundary_flist );

        bool saveBinary( const std::string &filename );

        bool loadBinary( const std::string &filename );

        bool saveXML( const std::string &filename );

        bool loadXML( const std::string &filename );

        bool addStratigraphicSurface( typename T::Ptr &sptr, std::size_t &surface_index ); 

        bool addStructuralSurface( typename T::Ptr &sptr, std::size_t &surface_index ); 

        bool setPhysicalSurfaceId(std::size_t srules_id, std::int64_t physical_id);

        bool getPhysicalSurfaceId(std::size_t srules_id, std::int64_t& physical_id);

        void clearPhysicalSurfaceId(std::size_t srules_id);

        bool isStratigraphicSurface(std::size_t surface_index);

        bool isStructuralSurface(std::size_t surface_index);

        bool markAsStratigraphicSurface(std::size_t surface_index);

        bool markAsStructuralSurface(std::size_t surface_index);

        std::vector<std::size_t> getStratigraphicSurfacesIds();

        std::vector<std::size_t> getStructuralSurfacesIds();

        std::array<typename T::Natural, 3> getCoordinatesMap() { return T::getCoordinatesMap(); }

        bool adaptSurfacesMeshes(const std::vector<std::size_t>& surfaces_ids = {});

        bool getDomainSimplicialComplex(Eigen::MatrixXd& OV, Eigen::MatrixXi& OF);

        template<typename VertexList, typename FaceList>
        std::size_t getAdaptedMesh(std::size_t sid, VertexList& vlist, FaceList& flist);

        std::size_t getAdaptedMesh(std::size_t sid, Eigen::MatrixXd& vlist, Eigen::MatrixXi& flist);

        template<typename VertexList, typename FaceList>
        std::size_t getAdaptedPartialMesh(std::size_t sid, VertexList& vlist, FaceList& flist);

        std::size_t getAdaptedPartialMesh(std::size_t sid, Eigen::MatrixXd& vlist, Eigen::MatrixXi& flist);

        /* template<typename std::vector<double>, typename std::vector<std::size_t>> */
        std::size_t getAdaptedCrossSection(std::size_t sid, const Eigen::Vector3d& p, const Eigen::Vector3d& t,  std::vector<double>& vlist, std::vector<std::size_t>& elist);

        std::size_t getAdaptedCrossSection(std::size_t sid, const Eigen::Vector3d& p, const Eigen::Vector3d& t,  Eigen::MatrixXd& vlist, Eigen::MatrixXi& elist);

        std::size_t getAdaptedCrossSection3D(std::size_t sid, const Eigen::Vector3d& p, const Eigen::Vector3d& t,  Eigen::MatrixXd& vlist, Eigen::MatrixXi& elist);

        std::size_t getAdaptedPartialCrossSection(std::size_t sid, const Eigen::Vector3d& p, const Eigen::Vector3d& t,  std::vector<double>& vlist, std::vector<std::size_t>& elist);

        std::size_t getAdaptedPartialCrossSection(std::size_t sid, const Eigen::Vector3d& p, const Eigen::Vector3d& t,  Eigen::MatrixXd& vlist, Eigen::MatrixXi& elist);

        std::size_t getAdaptedPartialCrossSection3D(std::size_t sid, const Eigen::Vector3d& p, const Eigen::Vector3d& t,  Eigen::MatrixXd& vlist, Eigen::MatrixXi& elist);

    public:
        bool removeAbove( typename T::Ptr sptr ); 
        bool removeAboveIntersection( typename T::Ptr sptr ); 

        bool removeBelow( typename T::Ptr sptr ); 
        bool removeBelowIntersection( typename T::Ptr sptr ); 

        bool defineAbove( typename T::Ptr sptr ); 
        bool defineBelow( typename T::Ptr sptr ); 

        bool defineAbove( std::vector<typename T::Ptr> &bounding_surfaces ); 
        bool defineBelow( std::vector<typename T::Ptr> &bounding_surfaces ); 

        bool removeAbove( std::size_t surface_index ); 
        bool removeAboveIntersection( std::size_t surface_index ); 

        bool removeBelow( std::size_t surface_index ); 
        bool removeBelowIntersection( std::size_t surface_index ); 

        bool defineAbove( std::size_t surface_index ); 
        bool defineBelow( std::size_t surface_index ); 

        bool defineAbove( std::vector<size_t> surface_indices );
        bool defineBelow( std::vector<size_t> surface_indices );

        bool weakEntireSurfaceCheck( std::size_t surface_index ); 

        bool liesInsideBoundingBox( const Point3 &p );

        bool liesInsideBoundingBox( Point3 &&p );

        bool liesBetweenBoundarySurfaces( const Point3 &p );

        bool liesBetweenBoundarySurfaces( Point3 &&p );

        void updateDiscretization();

        void updateCache();

    private: 
        ContainerType container; 
        MapType dictionary; 

        std::vector<std::size_t> lower_bound_ids_ = {};
        std::vector<std::size_t> sup_lower_bound_ids_ = {};
        std::vector<typename T::WeakPtr> lower_bound_;
        bool define_above_ = false; 

        std::vector<std::size_t> upper_bound_ids_ = {};
        std::vector<std::size_t> inf_upper_bound_ids_ = {};
        std::vector<typename T::WeakPtr> upper_bound_;
        bool define_below_ = false;

        std::set<std::size_t> stratigraphic_surfaces_ids_ = {};
        std::set<std::size_t> structural_surfaces_ids_ = {};
        std::map<std::size_t, std::int64_t> physical_surface_ids_map_ = {};

        std::shared_ptr<MeshingHelper> mhptr_;
        std::mutex mhelper_mx_, am_mx_, apm_mx_, acs_mx_, apcs_mx_;
        inline MeshingHelper& meshingHelper()
        {
            std::lock_guard<std::mutex> g(mhelper_mx_);
            if (mhptr_ == nullptr)
            {
                mhptr_ = std::make_shared<MeshingHelper>();
                mhptr_->resetBaseMesh(
                        PlanarSurface::getOrigin(),
                        PlanarSurface::getLenght(),
                        PlanarSurface::getDiscretizationX(),
                        PlanarSurface::getDiscretizationY()
                        );
                /* computeDomainSimplicialComplex(); */

            }
            return *mhptr_;
        }
        inline void clearMeshingHelper()
        {
            std::lock_guard<std::mutex> g(mhelper_mx_);
            for (auto sptr : container)
            {
                sptr->clearAdaptedMeshCaches();
            }
            mhptr_ = nullptr;
        }

        bool defineAboveIsActive();
        bool defineBelowIsActive();

        bool isValidSurface( const typename T::Ptr &sptr );
        bool weakEntireSurfaceCheck( const typename T::Ptr &sptr );
        bool weakEntireSurfaceListCheck( const std::vector<typename T::Ptr> &surfaces );

        bool weakLowerBoundedEntireSurfaceListCheck( const std::vector<typename T::Ptr> &surfaces );
        bool weakUpperBoundedEntireSurfaceListCheck( const std::vector<typename T::Ptr> &surfaces );

        bool isValidSurfaceForInsertion( const typename T::Ptr &sptr, std::size_t &surface_index );

        bool boundaryAwareRemoveAbove( const typename T::Ptr &base_surface, typename T::Ptr &to_remove_surface );
        bool boundaryAwareRemoveBelow( const typename T::Ptr &base_surface, typename T::Ptr &to_remove_surface );

        bool isInUpperBoundary(const typename T::Ptr& sptr);
        bool isInLowerBoundary(const typename T::Ptr& sptr);

        bool computeDomainSimplicialComplex(const std::vector<std::size_t>& surfaces_ids = {});

        void filterEdges3D(const PlanarSurface::Ptr& sptr, const Eigen::MatrixXd& VI, const Eigen::MatrixXi& EI,
            Eigen::MatrixXi& EO, double tol = 1E-4);

        // Cereal provides an easy way to serialize objects
        friend class cereal::access;

        template<typename Archive>
        void save( Archive &ar, const std::uint32_t version ) const;

        template<typename Archive>
        void load( Archive &ar, const std::uint32_t version );

        /* template<typename Archive> */
        /* void serialize( Archive &ar, const std::uint32_t version ); */
}; 

template<typename T>
std::size_t SRules<T>::getAdaptedCrossSection(std::size_t sid, const Eigen::Vector3d& p, const Eigen::Vector3d& t, Eigen::MatrixXd& V, Eigen::MatrixXi& E)
{
    /* std::lock_guard<std::mutex> g(acs_mx_); */
    if (sid >= size())
    {
        return 0;
    }

    /* std::cout << "Preparing to compute simplicial complex" << std::endl << std::flush; */
    /* computeDomainSimplicialComplex(); */
    /* std::cout << "Computed simplicial complex" << std::endl << std::flush; */
    /* std::vector<double> vlist; */
    /* std::vector<int> flist; */
    /* std::cout << "Preparing to get mesh" << std::endl << std::flush; */
    /* container[sid]->getMesh(vlist, flist); */
    /* std::cout << "Got mesh" << std::endl << std::flush; */
    /* Eigen::MatrixXd SF = eigen_converters::mapPoints3D(vlist); */
    /* Eigen::MatrixXi ST = eigen_converyers::mapPoints3D(flist); */

    Eigen::MatrixXd SV;
    /* SV.resize(vlist.size()/3, 3); */
    /* for (int i = 0; i < SV.rows(); ++i) */
    /* { */
    /*     SV(i, 0) = vlist[3*i + 0]; */
    /*     SV(i, 1) = vlist[3*i + 1]; */
    /*     SV(i, 2) = vlist[3*i + 2]; */
    /* } */

    Eigen::MatrixXi ST;
    /* ST.resize(flist.size()/3, 3); */
    /* for (int i = 0; i < ST.rows(); ++i) */
    /* { */
    /*     ST(i, 0) = flist[3*i + 0]; */
    /*     ST(i, 1) = flist[3*i + 1]; */
    /*     ST(i, 2) = flist[3*i + 2]; */
    /* } */

    getAdaptedMesh(sid, SV, ST);

    Eigen::MatrixXd VV;
    Eigen::MatrixXi EE;
    /* std::cout << "Preparing to intersect surface with plane" << std::endl << std::flush; */
    /* mhptr_->intersectSurfaceWithPlaneCurve2D(SV, ST, p, t, VV, EE); */
    meshingHelper().intersectSurfaceWithPlaneCurve2D(SV, ST, p, t, VV, EE);
    /* std::cout << "Got curve" << std::endl << std::flush; */

    auto edge_works = [&](PlanarSurface::Ptr sptr, const Eigen::MatrixXd& V, const Eigen::RowVector2i& edge,
            const Eigen::Vector3d& O, const Eigen::Vector3d& TT, const Eigen::Vector3d& Up) -> bool
    {
        bool works = true;
        double x, y, z, h;
        Eigen::Vector3d P;

        P = V(edge(0), 0)*TT + V(edge(0), 1)*Up + O;
        x = P(0);
        y = P(1);
        z = P(2);
        sptr->getHeight({x, y}, h);
        works &= (std::abs(z - h) < 1E-4);

        P = V(edge(1), 0)*TT + V(edge(1), 1)*Up + O;
        x = P(0);
        y = P(1);
        z = P(2);
        sptr->getHeight({x, y}, h);
        works &= (std::abs(z - h) < 1E-4);

        return works;
    };

    Eigen::Vector3d Up;
    Up << 0., 0., 1.;
    auto sptr = container[sid];
    std::vector<int> el;
    for(int i = 0; i < EE.rows(); ++i)
    {
        if (edge_works(sptr, VV, EE.row(i), p, t, Up))
        {
            /* if ( (h < top) && (h > bottom) ) */
            {
                el.push_back(EE(i,0));
                el.push_back(EE(i,1));
            }
        }
    }

    E.resize(el.size()/2, 2);
    for (int i = 0; i < E.rows(); ++i)
    {
        E(i, 0) = el[2*i + 0];
        E(i, 1) = el[2*i + 1];
    }

    /* V.resize(VV.rows(), 2); */
    /* for (int i = 0; i < V.rows(); ++i) */
    /* { */
    /*     V(i, 0) = t.dot(VV.row(i).transpose() - p); */
    /*     V(i, 1) = Up.dot(VV.row(i).transpose() - p); */
    /* } */
    V = VV;
 
    auto num_edges = static_cast<std::size_t>(E.rows());

    return num_edges;
}

template<typename T>
std::size_t SRules<T>::getAdaptedCrossSection3D(std::size_t sid, const Eigen::Vector3d& p, const Eigen::Vector3d& t, Eigen::MatrixXd& V, Eigen::MatrixXi& E)
{
    /* std::lock_guard<std::mutex> g(acs_mx_); */
    if (sid >= size())
    {
        return 0;
    }

    Eigen::MatrixXd SV;
    Eigen::MatrixXi ST, EE;
    getAdaptedMesh(sid, SV, ST);

    meshingHelper().intersectSurfaceWithPlaneCurve3D(SV, ST, p, t, V, EE);

    filterEdges3D(container[sid], V, EE, E);
    auto num_edges = static_cast<std::size_t>(E.rows());

    return num_edges;
}

template<typename T>
/* template<typename std::vector<double>, typename std::vector<std::size_t>> */
std::size_t SRules<T>::getAdaptedCrossSection(std::size_t sid, const Eigen::Vector3d& p, const Eigen::Vector3d& t,  std::vector<double>& vlist, std::vector<std::size_t>& elist)
{
    Eigen::MatrixXd V;
    Eigen::MatrixXi F;
    auto num_edges = getAdaptedCrossSection(sid, p, t, V, F);

    num_edges = 0;
    for (int i = 0; i < V.rows(); ++i)
    {
        vlist.push_back(V(i, 0));
        vlist.push_back(V(i, 1));
    }

    for (int i = 0; i < F.rows(); ++i)
    {
        elist.push_back(F(i,0));
        elist.push_back(F(i,1));
        ++num_edges;
    }

    return num_edges;
}

template<typename T>
std::size_t SRules<T>::getAdaptedPartialCrossSection(std::size_t sid, const Eigen::Vector3d& p, const Eigen::Vector3d& t, Eigen::MatrixXd& V, Eigen::MatrixXi& E)
{
    /* std::lock_guard<std::mutex> g(apcs_mx_); */
    if (sid >= size())
    {
        return 0;
    }

    Eigen::MatrixXd SV, VV;
    Eigen::MatrixXi ST, EE;
    getAdaptedPartialMesh(sid, SV, ST);

    /* std::cout << "Preparing to intersect surface with plane" << std::endl << std::flush; */
    /* mhptr_->intersectSurfaceWithPlaneCurve2D(SV, ST, p, t, VV, EE); */
    meshingHelper().intersectSurfaceWithPlaneCurve2D(SV, ST, p, t, VV, EE);
    /* std::cout << "Got curve" << std::endl << std::flush; */

    auto edge_works = [&](PlanarSurface::Ptr sptr, const Eigen::MatrixXd& V, const Eigen::RowVector2i& edge,
            const Eigen::Vector3d& O, const Eigen::Vector3d& TT, const Eigen::Vector3d& Up) -> bool
    {
        bool works = true;
        double x, y, z, h;
        Eigen::Vector3d P;

        P = V(edge(0), 0)*TT + V(edge(0), 1)*Up + O;
        x = P(0);
        y = P(1);
        z = P(2);
        sptr->getHeight({x, y}, h);
        works &= (std::abs(z - h) < 1E-4);

        P = V(edge(1), 0)*TT + V(edge(1), 1)*Up + O;
        x = P(0);
        y = P(1);
        z = P(2);
        sptr->getHeight({x, y}, h);
        works &= (std::abs(z - h) < 1E-4);

        return works;
    };

    Eigen::Vector3d Up;
    Up << 0., 0., 1.;
    auto sptr = container[sid];
    std::vector<int> el;
    for(int i = 0; i < EE.rows(); ++i)
    {
        if (edge_works(sptr, VV, EE.row(i), p, t, Up))
        {
            /* if ( (h < top) && (h > bottom) ) */
            {
                el.push_back(EE(i,0));
                el.push_back(EE(i,1));
            }
        }
    }

    E.resize(el.size()/2, 2);
    for (int i = 0; i < E.rows(); ++i)
    {
        E(i, 0) = el[2*i + 0];
        E(i, 1) = el[2*i + 1];
    }

    /* V.resize(VV.rows(), 2); */
    /* for (int i = 0; i < V.rows(); ++i) */
    /* { */
    /*     V(i, 0) = t.dot(VV.row(i).transpose() - p); */
    /*     V(i, 1) = Up.dot(VV.row(i).transpose() - p); */
    /* } */
    V = VV;
 
    auto num_edges = static_cast<std::size_t>(E.rows());

    return num_edges;
}

template<typename T>
std::size_t SRules<T>::getAdaptedPartialCrossSection3D(std::size_t sid, const Eigen::Vector3d& p, const Eigen::Vector3d& t, Eigen::MatrixXd& V, Eigen::MatrixXi& E)
{
    if (sid >= size())
    {
        return 0;
    }

    Eigen::MatrixXd SV;
    Eigen::MatrixXi ST, EE;
    getAdaptedPartialMesh(sid, SV, ST);
    meshingHelper().intersectSurfaceWithPlaneCurve3D(SV, ST, p, t, V, EE);

    filterEdges3D(container[sid], V, EE, E);
    auto num_edges = static_cast<std::size_t>(E.rows());

    return num_edges;
}

template<typename T>
/* template<typename std::vector<double>, typename std::vector<std::size_t>> */
std::size_t SRules<T>::getAdaptedPartialCrossSection(std::size_t sid, const Eigen::Vector3d& p, const Eigen::Vector3d& t,  std::vector<double>& vlist, std::vector<std::size_t>& elist)
{
    Eigen::MatrixXd V;
    Eigen::MatrixXi F;
    auto num_edges = getAdaptedPartialCrossSection(sid, p, t, V, F);

    num_edges = 0;
    for (int i = 0; i < V.rows(); ++i)
    {
        vlist.push_back(V(i, 0));
        vlist.push_back(V(i, 1));
    }

    for (int i = 0; i < F.rows(); ++i)
    {
        elist.push_back(F(i,0));
        elist.push_back(F(i,1));
        ++num_edges;
    }

    return num_edges;
}

template<typename T>
void SRules<T>::filterEdges3D(const PlanarSurface::Ptr& sptr, const Eigen::MatrixXd& VI, const Eigen::MatrixXi& EI,
        Eigen::MatrixXi& EO, double tol)
{
    auto edge_works = [&](PlanarSurface::Ptr sptr, const Eigen::MatrixXd& V, const Eigen::RowVector2i& edge) -> bool
    {
        bool works = true;
        double x, y, z, h;
        Eigen::Vector3d P0, P1, BC;

        P0 << V(edge(0), 0), V(edge(0), 1), V(edge(0), 2);
        x = P0(0);
        y = P0(1);
        z = P0(2);
        sptr->getHeight({x, y}, h);
        works &= (std::abs(z - h) < tol);

        P1 << V(edge(1), 0), V(edge(1), 1), V(edge(1), 2);
        x = P1(0);
        y = P1(1);
        z = P1(2);
        sptr->getHeight({x, y}, h);
        works &= (std::abs(z - h) < tol);

        BC = P0/2. + P1/2.;
        x = BC(0);
        y = BC(1);
        z = BC(2);
        works &= sptr->getHeight({x, y}, h);

        return works;
    };

    std::vector<int> el;
    for(int i = 0; i < EI.rows(); ++i)
    {
        if (edge_works(sptr, VI, EI.row(i)))
        {
            {
                el.push_back(EI(i,0));
                el.push_back(EI(i,1));
            }
        }
    }

    EO.resize(el.size()/2, 2);
    for (int i = 0; i < EO.rows(); ++i)
    {
        EO(i, 0) = el[2*i + 0];
        EO(i, 1) = el[2*i + 1];
    }
}


template<typename T>
/* template<typename VertexList, typename FaceList> */
/* size_t SRules<T>::getAdaptedMesh( size_t surface_id, VertexList &vlist, FaceList &flist ) */
size_t SRules<T>::getAdaptedMesh( size_t surface_id, Eigen::MatrixXd &vlist, Eigen::MatrixXi &flist )
{
    /* std::lock_guard<std::mutex> g(am_mx_); */
    std::size_t face_count = 0;
    if (surface_id >= size())
    {
        return face_count;
    }

    auto sptr = container[surface_id];
    if (sptr->hasAdaptedMesh(vlist, flist))
    {
        return flist.rows();
    }

    {
        std::vector<double> vl;
        std::vector<std::size_t> fl;

        sptr->getMesh(vl, fl);
        vlist.resize(vl.size()/3, 3);
        flist.resize(fl.size()/3, 3);
        const auto& cmap = PlanarSurface::getCoordinatesMap();
        for (int i = 0; i < vlist.rows(); ++i)
        {
            vlist(i, 0) = vl[3*i + cmap[0]];
            vlist(i, 1) = vl[3*i + cmap[1]];
            vlist(i, 2) = vl[3*i + cmap[2]];
        }
        for (int i = 0; i < flist.rows(); ++i)
        {
            flist(i, 0) = fl[3*i + 0];
            flist(i, 1) = fl[3*i + 1];
            flist(i, 2) = fl[3*i + 2];
        }
    }
    if (!meshingHelper().hasSurface(surface_id))
    {
        return static_cast<std::size_t>(flist.rows());
    }

    Eigen::MatrixXi FB, J, K;
    igl::boundary_facets(flist, FB, J, K);
    Eigen::MatrixXd VB;
    VB.resize(vlist.rows(), 2);
    VB.col(0) = vlist.col(0);
    VB.col(1) = vlist.col(1);

    Eigen::MatrixXd V;
    Eigen::MatrixXi F;
    getDomainSimplicialComplex(V, F);

    auto origin = PlanarSurface::getOrigin();
    auto length = PlanarSurface::getLenght();
    double bottom = origin[2];
    double top = origin[2] + length[2];
    double tol = 1E-5;

    Eigen::MatrixXd BC;
    igl::barycenter(V, F, BC);

    lexicographicOrderOnVertices::setTolerance(tol);
    using OrderedTriangle = std::set<Point3, lexicographicOrderOnVertices>;
    struct lexicographicOrderedTriangles {
        bool operator()(const OrderedTriangle& t1, const OrderedTriangle& t2) const
        {
            auto iter1 = t1.begin();
            auto iter2 = t2.begin();
            lexicographicOrderOnVertices comparator;

            while ((iter1 != t1.end()) && (iter2 != t2.end()))
            {
                if (comparator(*iter1, *iter2))
                {
                    return true;
                }
                else if (comparator(*iter2, *iter1))
                {
                    return false;
                }

                ++iter1;
                ++iter2;
            }

            if ((iter1 == t1.end()) && (iter2 != t2.end()))
            {
                return true;
            }

            if ((iter1 != t1.end()) && (iter2 == t2.end()))
            {
                return false;
            }

            return false;
        }
    };
    std::set<OrderedTriangle, lexicographicOrderedTriangles> cached_triangles;
    auto prepare_cached_triangles = [&] ()
    {
        Point3 v0, v1, v2;
        std::size_t i0, i1, i2;
        for (int i = 0; i < flist.rows(); ++i)
        {
            OrderedTriangle ot;
            i0 = flist(i, 0);
            i1 = flist(i, 1);
            i2 = flist(i, 2);

            v0 = {vlist(i0, 0), vlist(i0, 1), 0.};
            ot.insert(v0);

            v1 = {vlist(i1, 0), vlist(i1, 1), 0.};
            ot.insert(v1);

            v2 = {vlist(i2, 0), vlist(i2, 1), 0.};
            ot.insert(v2);

            cached_triangles.insert(ot);
        }
    };
    prepare_cached_triangles();

    auto is_cached_triangle = [&](int i) -> bool
    {
        Point3 v0, v1, v2;
        // double h0, h1, h2;

        // sptr->getHeight({V(F(i, 0), 0), V(F(i, 0), 1)}, h0);
        v0 = {V(F(i, 0), 0), V(F(i, 0), 1), 0.}; // h0};

        // sptr->getHeight({V(F(i, 1), 0), V(F(i, 1), 1)}, h1);
        v1 = {V(F(i, 1), 0), V(F(i, 1), 1), 0.}; // h1};

        // sptr->getHeight({V(F(i, 2), 0), V(F(i, 2), 1)}, h2);
        v2 = {V(F(i, 2), 0), V(F(i, 2), 1), 0.}; // h2};

        OrderedTriangle ot;
        ot.insert(v0);
        ot.insert(v1);
        ot.insert(v2);

        auto iter = cached_triangles.find(ot);
        if (iter == cached_triangles.end())
        {
            return false;
        }

        return true;
    };

    std::vector<int> fl;
    Eigen::RowVector2d p;
    double wb = 1.;
    double h;
    int ncached = 0, nrefined = 0;
    for(int i = 0; i < BC.rows(); ++i)
    {
        p = BC.row(i);
        wb = igl::winding_number(VB, FB, p);
        auto& x = BC(i, 0);
        auto& y = BC(i, 1);

        if (std::abs(wb) < 0.5)
        {
            continue;
        }

        if (is_cached_triangle(i))
        {
            fl.push_back(F(i,0));
            fl.push_back(F(i,1));
            fl.push_back(F(i,2));
            ++face_count;
            ++ncached;
            /* std::cout << "Including cached triangle" << std::endl << std::flush; */
        }
        else if (sptr->getHeight({x, y}, h))
        {
            if ( (h < top) && (h > bottom) )
            {
                fl.push_back(F(i,0));
                fl.push_back(F(i,1));
                fl.push_back(F(i,2));
                ++face_count;
                ++nrefined;
            }
        }
    }
    flist.resize(fl.size()/3, 3);
    for (int i = 0; i < flist.rows(); ++i)
    {
        flist(i, 0) = fl[3*i + 0];
        flist(i, 1) = fl[3*i + 1];
        flist(i, 2) = fl[3*i + 2];
    }

    /* vlist.resize(V.rows() * V.cols()); */
    /* for(int i = 0; i < V.rows(); ++i) */
    /* { */
        /* vlist[3*i + 0] = V(i, 0); */
        /* vlist[3*i + 1] = V(i, 1); */
        /* vlist[3*i + 2] = V(i, 2); */
    /* } */
    vlist.resize(V.rows(), 3);
    vlist.col(0) = V.col(0);
    vlist.col(1) = V.col(1);
    for (int i = 0; i < vlist.rows(); ++i)
    {
        auto& x = V(i, 0);
        auto& y = V(i, 1);
        sptr->getHeight({x, y}, h);
        vlist(i, 2) = h;
    }

    sptr->setAdaptedMesh(vlist, flist);

    std::cout << "Surface " << surface_id << " included " << ncached << " cached triangles." << std::endl << std::flush;
    std::cout << "Surface " << surface_id << " included " << nrefined << " refined triangles." << std::endl << std::flush;
    /* MeshingHelper::writePLY("surface_" + std::to_string(surface_id) + ".ply", vlist, flist); */
    /* std::cout << "Face count for surface " << std::to_string(surface_id) + ": " << face_count << std::endl << std::flush; */

    return face_count;
}

template<typename T>
template<typename VertexList, typename FaceList>
std::size_t SRules<T>::getAdaptedMesh(std::size_t sid, VertexList& vlist, FaceList& flist)
{
    Eigen::MatrixXd V;
    Eigen::MatrixXi F;
    auto face_count = getAdaptedMesh(sid, V, F);

    auto cmap = PlanarSurface::getCoordinatesMap();
    vlist.resize(V.rows() * V.cols());
    for (int i = 0; i < V.rows(); ++i)
    {
        vlist[3*i + 0] = V(i, cmap[0]);
        vlist[3*i + 1] = V(i, cmap[1]);
        vlist[3*i + 2] = V(i, cmap[2]);
    }

    flist.resize(F.rows() * F.cols());
    for (int i = 0; i < F.rows(); ++i)
    {
        flist[3*i + 0] = F(i, 0);
        flist[3*i + 1] = F(i, 1);
        flist[3*i + 2] = F(i, 2);
    }

    return face_count;
}

template<typename T>
/* template<typename VertexList, typename FaceList> */
/* size_t SRules<T>::getAdaptedMesh( size_t surface_id, VertexList &vlist, FaceList &flist ) */
size_t SRules<T>::getAdaptedPartialMesh( size_t surface_id, Eigen::MatrixXd &vlist, Eigen::MatrixXi &flist )
{
    /* std::lock_guard<std::mutex> g(apm_mx_); */
    /* std::cout << "Getting adapted mesh" << std::endl << std::flush; */
    std::size_t face_count = 0;
    if (surface_id >= size())
    {
        return face_count;
    }

    auto sptr = container[surface_id];
    if (sptr->hasAdaptedPartialMesh(vlist, flist))
    {
        face_count = flist.rows();
        return face_count;
    }

    face_count = getAdaptedMesh(surface_id, vlist, flist);

    /* std::cout << "Testing if triangle surface" << std::endl << std::flush; */
    auto iptr = container[surface_id]->getISurface();
    if (iptr && !iptr->isTriangleSurface())
    {
    /* std::cout << "Not triangle surface" << std::endl << std::flush; */
        return face_count;
    }
    /* std::cout << "Triangle surface" << std::endl << std::flush; */

    /* std::vector<Eigen::MatrixXd> Vlist; */
    /* Vlist.resize(2); */
    /* Vlist[0] = vlist; */
    /* Vlist[1] = iptr->getISurfaceVertices(); */

    /* std::vector<Eigen::MatrixXi> Flist; */
    /* Flist.resize(2); */
    /* Flist[0] = flist; */
    /* Flist[1] = iptr->getISurfaceTriangles(); */
    /* std::cout << "Triangle surface has " << Vlist[1].rows() << " vertices and " << Flist[1].rows() << "triangles" << std::endl << std::flush; */

    Eigen::MatrixXd V2;
    /* MeshingHelper::intersectDomains(Vlist, Flist, V2, flist); */
    MeshingHelper::intersectDomains(vlist, flist, iptr->getISurfaceVertices(), iptr->getISurfaceTriangles(), V2, flist);

    double h;
    vlist.resize(V2.rows(), 3);
    vlist.col(0) = V2.col(0);
    vlist.col(1) = V2.col(1);
    for (int i = 0; i < vlist.rows(); ++i)
    {
        auto& x = V2(i, 0);
        auto& y = V2(i, 1);
        sptr->getHeight({x, y}, h);
        vlist(i, 2) = h;
    }

    face_count = static_cast<std::size_t>(flist.rows());
    /* std::cout << "Intersected domains, got " << face_count << " faces" << std::endl << std::flush; */

    sptr->setAdaptedPartialMesh(vlist, flist);

    return face_count;
}

template<typename T>
template<typename VertexList, typename FaceList>
std::size_t SRules<T>::getAdaptedPartialMesh(std::size_t sid, VertexList& vlist, FaceList& flist)
{
    Eigen::MatrixXd V;
    Eigen::MatrixXi F;
    auto face_count = getAdaptedPartialMesh(sid, V, F);

    auto cmap = PlanarSurface::getCoordinatesMap();
    vlist.resize(V.rows() * V.cols());
    for (int i = 0; i < V.rows(); ++i)
    {
        vlist[3*i + 0] = V(i, cmap[0]);
        vlist[3*i + 1] = V(i, cmap[1]);
        vlist[3*i + 2] = V(i, cmap[2]);
    }

    flist.resize(F.rows() * F.cols());
    for (int i = 0; i < F.rows(); ++i)
    {
        flist[3*i + 0] = F(i, 0);
        flist[3*i + 1] = F(i, 1);
        flist[3*i + 2] = F(i, 2);
    }

    return face_count;
}

template<typename T>
template<typename VertexList, typename FaceList, typename NormalList>
size_t SRules<T>::getAdaptedMesh( size_t surface_id, VertexList &vlist, FaceList &flist, NormalList &nlist )
{
    UNUSED(surface_id);
    UNUSED(vlist);
    UNUSED(flist);
    UNUSED(nlist);

    if ( surface_id >= this->size() )
    {
        return false;
    }

    size_t face_count = 0;

    container[surface_id]->getVertexList(vlist);
    flist.clear(); 

    /* std::cout << "Getting face list: \n"; */ 

    // For all triangles in discretization
    //     Consider:
    //         a) triangle is empty:
    //             return null;
    //         b) triangle is valid:
    //             return triangle;
    //         c) triangle has one invalid vertex:
    //             return case 1;
    //         d) triangle has two invalid vertices, truncated by the same surface:
    //             return case 2;
    //         e) triangle has two invalid vertices, truncated by different surfaces:
    //             return triangle; (?, try something different for visualization?)
    //
    //      If got triangles from "case 1" or case 2" update vertex list and normal list
    //
    //      function getTriangles( triangle data[in], face list[out], vertex list[out], normal list[out] ) -> num triangles(integer)
    //
    //      function computeIntersection( segment1, segment2) -> parameter(real)


    /* unsigned int face_count = 0; 0 */
    /* for ( Natural i = 0; i < nX_ - 1; i += 2 ) { */ 
    /*     for ( Natural j = 0; j < nY_ - 1; j += 2 ) { */ 
    /*     } */
    /* } */

    return face_count; 
}

template<typename T>
template<typename VertexList, typename FaceList>
bool SRules<T>::getLowerBoundary( const std::vector<VertexList> &vlists, const std::vector<FaceList> &flists, VertexList &boundary_vlist, FaceList &boundary_flist )
{
    if ( vlists.size() != flists.size() )
    {
        return false;
    }

    size_t num_elements = vlists[0].size();
    for ( size_t i = 1; i < vlists.size(); ++i )
        if ( vlists[i].size() != num_elements )
            return false;

    boundary_vlist = vlists[0];
    for ( size_t i = 1; i < vlists.size(); ++i )
    {
        for ( size_t j = 0; j < num_elements; ++j )
        {
            if ( boundary_vlist[j] > vlists[i][j] )
            {
                boundary_vlist[j] = vlists[i][j];
            }
        }
    }

    return container[0]->mergeFaceLists(flists, boundary_flist);
}

template<typename T>
template<typename VertexList, typename FaceList>
bool SRules<T>::getUpperBoundary( const std::vector<VertexList> &vlists, const std::vector<FaceList> &flists, VertexList &boundary_vlist, FaceList &boundary_flist )
{
    if ( vlists.size() != flists.size() )
    {
        return false;
    }

    size_t num_elements = vlists[0].size();
    for ( size_t i = 1; i < vlists.size(); ++i )
        if ( vlists[i].size() != num_elements )
            return false;

    boundary_vlist = vlists[0];
    for ( size_t i = 1; i < vlists.size(); ++i )
    {
        for ( size_t j = 0; j < num_elements; ++j )
        {
            if ( boundary_vlist[j] < vlists[i][j] )
            {
                boundary_vlist[j] = vlists[i][j];
            }
        }
    }

    return container[0]->mergeFaceLists(flists, boundary_flist);
}

#if defined( BUILD_WITH_SERIALIZATION )
    #include "cereal/types/vector.hpp"
    #include "cereal/types/unordered_map.hpp"

    template<typename T>
    template<typename Archive>
    void SRules<T>::save( Archive &ar, const std::uint32_t version ) const
    {
        (void)(version);
        ar(
            container, 
            dictionary,
            lower_bound_, 
            define_above_, 
            upper_bound_, 
            define_below_,
            stratigraphic_surfaces_ids_,
            structural_surfaces_ids_,
            physical_surface_ids_map_
          );
    }

    template<typename T>
    template<typename Archive>
    void SRules<T>::load( Archive &ar, const std::uint32_t version )
    {
        if ( version == 1 )
        {
            typename T::WeakPtr lbound, ubound;

            ar(
                container, 
                dictionary,
                lbound, 
                define_above_, 
                ubound, 
                define_below_
          );
                lower_bound_ = { lbound };
                upper_bound_ = { ubound };
        }
        else if (version == 2)
        {
            ar(
                container, 
                dictionary,
                lower_bound_, 
                define_above_, 
                upper_bound_, 
                define_below_
            );

            stratigraphic_surfaces_ids_ = {};
            for (size_t i = 0; i < container.size(); ++i) { stratigraphic_surfaces_ids_.insert(i); };
            structural_surfaces_ids_ = {};
            physical_surface_ids_map_ = {};
            for (size_t i = 0; i < container.size(); ++i) { physical_surface_ids_map_[i] = static_cast<int>(i); };
        }
        else
        {
            ar(
                container, 
                dictionary,
                lower_bound_, 
                define_above_, 
                upper_bound_, 
                define_below_,
                stratigraphic_surfaces_ids_,
                structural_surfaces_ids_,
                physical_surface_ids_map_
            );
        }

        updateCache();
    }

    CEREAL_CLASS_VERSION(SRules<PlanarSurface>, 3);
    CEREAL_CLASS_VERSION(SRules<LegacyPlanarSurface>, 3);

#else
    /* template<typename Archive> */
    /* void SRules::serialize( Archive &, const std::uint32_t ) {} */

    template<typename T>
    template<typename Archive>
    void SRules<T>::save( Archive &/* ar */, const std::uint32_t /* version */ ) const {}

    template<typename T>
    template<typename Archive>
    void SRules<T>::load( Archive &/* ar */, const std::uint32_t /* version */ ) {}

#endif /* BUILD_WITH_SERIALIZATION */

#include "srules.cpp"

#endif 
