/* MIT License */

/* Copyright (c) 2022 Julio Daniel Machado Silva */

/* Permission is hereby granted, free of charge, to any person obtaining a copy */
/* of this software and associated documentation files (the "Software"), to deal */
/* in the Software without restriction, including without limitation the rights */
/* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell */
/* copies of the Software, and to permit persons to whom the Software is */
/* furnished to do so, subject to the following conditions: */

/* The above copyright notice and this permission notice shall be included in all */
/* copies or substantial portions of the Software. */

/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE */
/* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, */
/* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE */
/* SOFTWARE. */


#ifndef EIGEN_CONVERTERS_HPP
#define EIGEN_CONVERTERS_HPP

#include <optional>
#include <vector>

#include <Eigen/Dense>

namespace eigen_converters {

template<typename R = double, typename T = int, typename S = int>
auto mapVectorToEigen(std::vector<R>& points, T num_rows, S num_cols)
{
    using EigenMap = Eigen::Map<Eigen::Matrix<R, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>;
    std::optional<EigenMap> map;

    int rows = static_cast<int>(num_rows);
    int cols = static_cast<int>(num_cols);

    if (points.size() >= static_cast<std::size_t>(rows * cols))
    {
        EigenMap M(points.data(), rows, cols);
        map = std::make_optional<EigenMap>(M);
    }

    return map;
}

template<typename R = double, typename T = int, typename S = int>
auto mapVectorToEigen(const std::vector<R>& points, T num_rows, S num_cols)
{
    using EigenMap = Eigen::Map<Eigen::Matrix<R, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>;
    std::optional<EigenMap> map;

    int rows = static_cast<int>(num_rows);
    int cols = static_cast<int>(num_cols);

    if (points.size() >= static_cast<std::size_t>(rows * cols))
    {
        // TODO: Fix this
        EigenMap M(const_cast<R*>(points.data()), rows, cols);
        map = std::make_optional<EigenMap>(M);
    }

    return map;
}

auto mapPoints3D(std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/3, 3));
/* { */
/* return *mapVectorToEigen(points, points.size()/3, 3); */
/* } */

auto mapPoints2D(std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/2, 2));
/* { */
/* return *mapVectorToEigen(points, points.size()/2, 2); */
/* } */

auto mapPoints3D(const std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/3, 3));
/* { */
/* return *mapVectorToEigen(points, points.size()/3, 3); */
/* } */

auto mapPoints2D(const std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/2, 2));
/* { */
/* return *mapVectorToEigen(points, points.size()/2, 2); */
/* } */

auto mapPoints3D(std::vector<int>& points) -> decltype(mapVectorToEigen(points, points.size()/3, 3));
/* { */
/* return *mapVectorToEigen(points, points.size()/3, 3); */
/* } */

auto mapPoints2D(std::vector<int>& points) -> decltype(mapVectorToEigen(points, points.size()/2, 2));
/* { */
/* return *mapVectorToEigen(points, points.size()/2, 2); */
/* } */

auto mapPoints3D(const std::vector<int>& points) -> decltype(mapVectorToEigen(points, points.size()/3, 3));
/* { */
/* return *mapVectorToEigen(points, points.size()/3, 3); */
/* } */

auto mapPoints2D(const std::vector<int>& points) -> decltype(mapVectorToEigen(points, points.size()/2, 2));
/* { */
/* return *mapVectorToEigen(points, points.size()/2, 2); */
/* } */

} // namespace eigen_converters

#endif
