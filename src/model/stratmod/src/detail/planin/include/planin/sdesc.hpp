/******************************************************************************/
/*                                                                            */
/* This file is part of the "Planar Interpolation Library" (PlanInLib)        */
/* Copyright (C) 2016, Julio Daniel Machado Silva.                            */
/*                                                                            */
/* PlanInLib is free software; you can redistribute it and/or                 */
/* modify it under the terms of the GNU Lesser General Public                 */
/* License as published by the Free Software Foundation; either               */
/* version 3 of the License, or (at your option) any later version.           */
/*                                                                            */
/* PlanInLib is distributed in the hope that it will be useful,               */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          */
/* Lesser General Public License for more details.                            */
/*                                                                            */
/* You should have received a copy of the GNU Lesser General Public           */
/* License along with PlanInLib.  If not, see <http://www.gnu.org/licenses/>, */
/* or write to the Free Software Foundation, Inc., 51 Franklin Street,        */
/* Fifth Floor, Boston, MA  02110-1301  USA.                                  */
/*                                                                            */
/******************************************************************************/


#ifndef PLANIN_SDESC_HPP
#define PLANIN_SDESC_HPP

#include <glog/logging.h>

#include "planin/serialization_primitives.hpp"

/* struct Point2 { */
/*     double data[2]; */ 
/*     double& operator[](std::size_t i) { return data[i]; }; */
/*     double operator[](std::size_t i) const { return data[i]; }; */
/* }; */

/* struct Point3 { */
/*     double data[3]; */
/*     double& operator[](std::size_t i) { return data[i]; }; */
/*     double operator[](std::size_t i) const { return data[i]; }; */
/* }; */

/* namespace cereal { */
    /* class access; */
/* } */

namespace planin {

class SDesc 
{
    public: 
        virtual ~SDesc() = default;

    private: 
        // Cereal provides an easy way to serialize objects
        friend class cereal::access;

        template<typename Archive>
        void serialize( Archive& /* ar */, const std::uint32_t /* version */ )
        {
            /* LOG(INFO) << "Call to serialize method in planin::SDesc"; */
        }

        /* template<typename Archive> */
        /* static void load_and_construct( Archive &ar, cereal::construct<InterpolatedGraph> &construct, const::uint32_t version); */
};
} // namespace planin

CEREAL_CLASS_VERSION(planin::SDesc, 0);

#endif

