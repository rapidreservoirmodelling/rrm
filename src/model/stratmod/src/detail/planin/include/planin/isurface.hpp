/******************************************************************************/
/*                                                                            */
/* This file is part of the "Planar Interpolation Library" (PlanInLib)        */
/* Copyright (C) 2016, Julio Daniel Machado Silva.                            */
/*                                                                            */
/* PlanInLib is free software; you can redistribute it and/or                 */
/* modify it under the terms of the GNU Lesser General Public                 */
/* License as published by the Free Software Foundation; either               */
/* version 3 of the License, or (at your option) any later version.           */
/*                                                                            */
/* PlanInLib is distributed in the hope that it will be useful,               */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          */
/* Lesser General Public License for more details.                            */
/*                                                                            */
/* You should have received a copy of the GNU Lesser General Public           */
/* License along with PlanInLib.  If not, see <http://www.gnu.org/licenses/>, */
/* or write to the Free Software Foundation, Inc., 51 Franklin Street,        */
/* Fifth Floor, Boston, MA  02110-1301  USA.                                  */
/*                                                                            */
/******************************************************************************/


#ifndef PLANIN_ISURFACE_HPP
#define PLANIN_ISURFACE_HPP

#include <cstdint>
#include <cstddef>

#include <glog/logging.h>

#include "planin/core.hpp"
#include "planin/serialization_primitives.hpp"

#include <Eigen/Dense>

/* struct Point2 { */
/*     double data[2]; */ 
/*     double& operator[](std::size_t i) { return data[i]; }; */
/*     double operator[](std::size_t i) const { return data[i]; }; */
/* }; */

/* struct Point3 { */
/*     double data[3]; */
/*     double& operator[](std::size_t i) { return data[i]; }; */
/*     double operator[](std::size_t i) const { return data[i]; }; */
/* }; */

/* namespace cereal { */
    /* class access; */
/* } */

namespace planin {

class ISurface 
{
    public: 
        /* using Point2 = ::Point2; */
        /* using Point3 = ::Point3; */

        virtual ~ISurface() = default;

        virtual bool surfaceIsSet() = 0; 

        virtual bool getHeight( const Point2 &p, double &height ) = 0; 

        inline double getRawHeight(const Point2& p)
        {
            double h;
            getHeight(p, h);

            return h;
        }

        virtual bool isSmooth() = 0; 

        virtual bool getNormal( const Point2 &p, Point3& normal ) = 0;

        /* virtual bool project( Point3 &p ) = 0; */ 

        inline bool liesAbove( const Point3 &p )
        {
            if (double h; getHeight({p[0], p[1]}, h))
            {
                return p[3] > h;
            }

            return false;
        }

        inline bool liesAboveRawSurface(const Point3& p)
        {
            return liesAbove(p);
        }

        inline bool liesBelow( const Point3 &p ) 
        {
            if (double h; getHeight({p[0], p[1]}, h))
            {
                return p[3] < h;
            }

            return false;
        }

        inline bool liesBelowRawSurface(const Point3& p)
        {
            return liesBelow(p);
        }

        inline bool liesOnOrAbove( const Point3 &p )
        {
            if (double h; getHeight({p[0], p[1]}, h))
            {
                return p[3] >= h;
            }

            return false;
        }

        inline bool liesOnOrBelow( const Point3 &p )
        {
            if (double h; getHeight({p[0], p[1]}, h))
            {
                return p[3] <= h;
            }

            return false;
        }

        virtual bool isTriangleSurface() = 0;

        virtual Eigen::MatrixXd getISurfaceVertices() = 0;

        virtual Eigen::MatrixXi getISurfaceTriangles() = 0;

        virtual Eigen::MatrixXi getISurfaceBoundary() = 0;

    private: 
        // Cereal provides an easy way to serialize objects
        friend class cereal::access;

        template<typename Archive>
        void serialize( Archive& /* ar */, const std::uint32_t /* version */ )
        {
            /* LOG(INFO) << "Call to serialize method in planin::ISurface"; */
        }

        /* template<typename Archive> */
        /* static void load_and_construct( Archive &ar, cereal::construct<InterpolatedGraph> &construct, const::uint32_t version); */
};
} // namespace planin

/* #if defined( BUILD_WITH_SERIALIZATION ) */
    /* #include "cereal/types/vector.hpp" */
    /* #include "cereal/types/list.hpp" */
    /* #include "cereal/types/set.hpp" */
    /* #include "cereal/types/memory.hpp" */

/* namespace planin { */
    /* template<typename Archive> */
    /* void ISurface::serialize( Archive& /1* ar *1/, const std::uint32_t /1* version *1/ ) */
    /* { */
    /* } */
/* } // namespace planin */


CEREAL_CLASS_VERSION(planin::ISurface, 0);
/* CEREAL_CLASS_VERSION(planin::ISurface, 0); */

/* #else */
/* namespace planin { */
    /* template<typename Archive> */
    /* void ISurface::serialize( Archive &, const std::uint32_t ) {} */
/* } // namespace planin */

/* #endif /1* BUILD_WITH_SERIALIZATION *1/ */

#endif

