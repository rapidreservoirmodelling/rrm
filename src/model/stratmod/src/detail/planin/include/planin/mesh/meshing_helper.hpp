/* This file is part of the plain library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The plain library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The plain library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the plain library.  If not, see <https://www.gnu.org/licenses/>. */

#ifndef PLANIN_MESHING_HELPER
#define PLANIN_MESHING_HELPER

#include <vector>
#include <set>
#include <memory>

#include <Eigen/Dense>

#include "core.hpp"
#include "planin/core.hpp"
#include "planin/structured_triangle_mesh_2d.hpp"

#include "igl/writePLY.h"

class MeshingHelper
{
    public:
        void resetBaseMesh(Point3 origin, Point3 length, std::size_t discX, std::size_t discY);

        bool hasSurface(std::size_t sid);
        bool addSurface(std::size_t sid, const std::vector<double>& vlist, const std::vector<std::size_t>& tlist);

        bool computeDomainSimplicialComplex();
        bool getDomainSimplicialComplex(Eigen::MatrixXd& OV, Eigen::MatrixXi& OF);

        void intersectSurfaceWithPlaneCurve3D(
                const Eigen::MatrixXd& surface_vertices, const Eigen::MatrixXi& surface_triangles,
                const Eigen::Vector3d& plane_origin, const Eigen::Vector3d& plane_tangent, 
                Eigen::MatrixXd& curve_vertices, Eigen::MatrixXi& curve_edges);

        void intersectSurfaceWithPlaneCurve2D(
                const Eigen::MatrixXd& surface_vertices, const Eigen::MatrixXi& surface_triangles,
                const Eigen::Vector3d& plane_origin, const Eigen::Vector3d& plane_tangent, 
                Eigen::MatrixXd& curve_vertices, Eigen::MatrixXi& curve_edges);

        static void intersectDomains(
                const std::vector<Eigen::MatrixXd>& Vlist, const std::vector<Eigen::MatrixXi>& Flist,
                Eigen::MatrixXd& VV, Eigen::MatrixXi& FF);

        static void intersectDomains(
                const Eigen::MatrixXd& DomainVlist, const Eigen::MatrixXi& DomainFlist,
                const Eigen::MatrixXd& OriginalMeshVlist, const Eigen::MatrixXi& OriginalMeshFlist,
                Eigen::MatrixXd& OV, Eigen::MatrixXi& OF);

        template <typename DerivedV,typename DerivedF>
        static inline void writePLY(
                const std::string & filename,
                const Eigen::MatrixBase<DerivedV> & V,
                const Eigen::MatrixBase<DerivedF> & F)
        {
            bool write_as_binary = false;
            igl::writePLY(filename, V, F, write_as_binary);
        }

        template <typename DerivedV,typename DerivedF, typename DerivedE>
        static inline void writePLY(
                const std::string & filename,
                const Eigen::MatrixBase<DerivedV> & V,
                const Eigen::MatrixBase<DerivedF> & F,
                const Eigen::MatrixBase<DerivedE> & E)
        {
            bool write_as_binary = false;
            igl::writePLY(filename, V, F, E, write_as_binary);
        }

    private:
        std::vector<double> vertices_;
        std::vector<int> triangles_;
        std::set<std::size_t> surface_ids_;
        StructuredTriangleMesh2D<Point2> base_mesh_;
        double top_, bottom_;

        Eigen::MatrixXd dom_simplices_vertices_;
        Eigen::MatrixXi dom_simplices_triangles_;
        bool has_dom_simplices_ = false;

        void clear()
        {
            vertices_ = {};
            triangles_ = {};
            surface_ids_ = {};
            /* base_mesh_; */
            /* top_; */
            /* bottom_; */

            Eigen::MatrixXd dom_simplices_vertices_ = Eigen::MatrixXd();
            Eigen::MatrixXi dom_simplices_triangles_ = Eigen::MatrixXi();
            has_dom_simplices_ = false;

        }

        void addTopAndBottomSurfaces();
        bool computeDomainSimplicialComplex(Eigen::MatrixXd& OV, Eigen::MatrixXi& OF);
        void addSurface(const std::vector<double>& vlist, const std::vector<std::size_t>& tlist);
        void getBaseDomainSimplicialComplex(Eigen::MatrixXd& OV, Eigen::MatrixXi& OF);

        void intersectSurfaceWithPlane();

        void filterEdgesOnPlane(const Eigen::Vector3d& p, const Eigen::Vector3d& u, const Eigen::Vector3d& v,
                const Eigen::MatrixXd& VI, const Eigen::MatrixXi& EI, Eigen::MatrixXi& EO, double tol = 1E-4);

        template<typename Kernel, typename CGAL_Segment3>
        static void mesh_to_cgal_boundary_edge_list(
                const Eigen::MatrixXd& V, const Eigen::MatrixXi& F,
                std::vector<CGAL_Segment3>& Edges);

        template<typename DerivedF, typename uE2EType>
        static void extractBranchingCurves(
                    const Eigen::MatrixBase<DerivedF>& F,
                    const std::vector<std::vector<uE2EType> >& uE2E,
                    std::vector<std::vector<size_t> >& curves);
};

#endif
