/******************************************************************************/
/*                                                                            */
/* This file is part of the "Planar Interpolation Library" (PlanInLib)        */
/* Copyright (C) 2016, Julio Daniel Machado Silva.                            */
/*                                                                            */
/* PlanInLib is free software; you can redistribute it and/or                 */
/* modify it under the terms of the GNU Lesser General Public                 */
/* License as published by the Free Software Foundation; either               */
/* version 3 of the License, or (at your option) any later version.           */
/*                                                                            */
/* PlanInLib is distributed in the hope that it will be useful,               */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU          */
/* Lesser General Public License for more details.                            */
/*                                                                            */
/* You should have received a copy of the GNU Lesser General Public           */
/* License along with PlanInLib.  If not, see <http://www.gnu.org/licenses/>, */
/* or write to the Free Software Foundation, Inc., 51 Franklin Street,        */
/* Fifth Floor, Boston, MA  02110-1301  USA.                                  */
/*                                                                            */
/******************************************************************************/


#ifndef PLANIN_MESH_BUILDER_HPP
#define PLANIN_MESH_BUILDER_HPP

#include <functional>
#include <iterator>
#include <vector>
#include <unordered_map>
#include <string>
#include <tuple>
#include <fstream>

#include "glog/logging.h"

#include "planin/planar_surface.hpp"
#include "planin/srules.hpp"
#include "planin/predicates.hpp"

#include "Eigen/Dense"
#include "polyhedra.hpp"
#include "predicates.hpp"

namespace geometric_primitives {

struct TriangleImage
{
    std::array<double, 3> vertex_height = {};
    double tolerance = 1E-9;

    TriangleImage() = default;
    ~TriangleImage() = default;
    TriangleImage(const TriangleImage&) = default;
    TriangleImage& operator=(const TriangleImage&) = default;
    TriangleImage(TriangleImage&&) = default;
    TriangleImage& operator=(TriangleImage&&) = default;

    TriangleImage& operator-()
    {
        vertex_height[0] = -vertex_height[0];
        vertex_height[1] = -vertex_height[1];
        vertex_height[2] = -vertex_height[2];

        return *this;
    }

    bool setVertex(size_t position, double vheight)
    {
        if ( position > 2 )
        {
            return false;
        }

        vertex_height[position] = vheight;

        return true;
    }

    double meanHeight()
    {
        return vertex_height[0]/3 + vertex_height[1]/3 + vertex_height[2]/3;
    }

    bool operator<(const TriangleImage &rhs) const
    {
        if ( vertex_height[0] - tolerance > rhs.vertex_height[0] )
        {
            return false;
        }

        if ( vertex_height[1] - tolerance > rhs.vertex_height[1] )
        {
            return false;
        }

        if ( vertex_height[2] - tolerance > rhs.vertex_height[2] )
        {
            return false;
        }

        return true;
    }

    bool operator==(const TriangleImage& rhs) const
    {
        bool equal = true;
        equal &= (std::abs(vertex_height[0] - rhs.vertex_height[0]) < tolerance);
        equal &= (std::abs(vertex_height[1] - rhs.vertex_height[1]) < tolerance);
        equal &= (std::abs(vertex_height[2] - rhs.vertex_height[2]) < tolerance);

        return equal;
    }

    bool operator>( const TriangleImage &rhs ) const
    {
        return rhs.operator<(*this);
    }

    bool operator>=( const TriangleImage &rhs ) const
    {
        return !( operator<(rhs) );
    }

    bool operator<=( const TriangleImage &rhs ) const
    {
        return rhs.operator>=(*this);
    }

    bool operator!=( const TriangleImage &rhs ) const
    {
        return !operator==(rhs);
    }
};

struct Prism {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    
        std::reference_wrapper<const Eigen::MatrixXd> domain_vertices;
        Eigen::RowVector3i indices{};
        Eigen::RowVector3i domain_map{};
        Eigen::Vector2d domain_centroid;
        TriangleImage top{}, base{};
        double height_centroid;
        std::vector<std::size_t> lower_bound{};
        int region_id = -1;
        double volume = 0.;

        Prism(const Eigen::MatrixXd& vertices) : domain_vertices(vertices) {}

        bool empty()
        {
            if (!got_domain_ || !got_top_base_)
            {
                return true;
            }
            return empty_;
        }

        void setDomain(const Eigen::RowVector3i& indices)
        {
            auto& vertices = this->domain_vertices.get();
            Eigen::Vector2d p0({vertices(indices[0], 0), vertices(indices[0], 1)});
            Eigen::Vector2d p1({vertices(indices[1], 0), vertices(indices[1], 1)});
            Eigen::Vector2d p2({vertices(indices[2], 0), vertices(indices[2], 1)});
            domain_centroid = p0/3 + p1/3 + p2/3;
            /* if (RobustPredicates::Get().signed_area(p0, p1, p2) < 0) */
            /* { */
                /* this->indices[2] = indices[1]; */
                /* this->indices[1] = indices[2]; */
            /* } */

            this->indices = indices;
            if ((indices[0] < indices[1]) && (indices[0] < indices[2]))
            {
                domain_map[0] = 0;
                domain_map[1] = 1;
                domain_map[2] = 2;
            }
            else if ((indices[1] < indices[0]) && (indices[1] < indices[2]))
            {
                this->indices[0] = indices[1];
                this->indices[1] = indices[2];
                this->indices[2] = indices[0];
                domain_map[0] = 1;
                domain_map[1] = 2;
                domain_map[2] = 0;
            }
            else if ((indices[2] < indices[0]) && (indices[2] < indices[1]))
            {
                this->indices[0] = indices[2];
                this->indices[1] = indices[0];
                this->indices[2] = indices[1];
                domain_map[0] = 2;
                domain_map[1] = 0;
                domain_map[2] = 1;
            }
            else
            {
                // abort
            }

            got_domain_ = true;
        }

        void setTopBase(const TriangleImage& t, const TriangleImage& b)
        {
            got_top_base_ = false;
            if (t == b)
            {
                empty_ = true;
            }
            else if (t > b)
            {
                this->top = t;
                this->base = b;
                empty_ = false;
            }
            else
            {
                this->top = b;
                this->base = t;
                empty_ = false;
            }
            height_centroid = top.meanHeight()/2 + base.meanHeight()/2;
            got_top_base_ = true;
        }

        Eigen::RowVector3d centroid()
        {
            Eigen::RowVector3d c({domain_centroid[0], domain_centroid[1], height_centroid});
            return c;
        }

        bool tetrahedralize(Eigen::MatrixXd& vertices, Eigen::MatrixXi& tetrahedra)
        {
            if (empty())
            {
                return false;
            }

            auto& vlist = domain_vertices.get();
            vertices.resize(6, 3);

            auto cmap = PlanarSurface::getCoordinatesMap();
            std::array<double, 3> point;

            // base
            point[0] = vlist(indices[0], 0);
            point[1] = vlist(indices[0], 1);
            point[2] = base.vertex_height[domain_map[0]];
            vertices.row(0) << point[cmap[0]], point[cmap[1]], point[cmap[2]];
            /* vertices.row(0) << vlist(indices[0], 0), vlist(indices[0], 1), base.vertex_height[domain_map[0]]; */

            point[0] = vlist(indices[1], 0);
            point[1] = vlist(indices[1], 1);
            point[2] = base.vertex_height[domain_map[1]];
            vertices.row(1) << point[cmap[0]], point[cmap[1]], point[cmap[2]];
            /* vertices.row(1) << vlist(indices[1], 0), vlist(indices[1], 1), base.vertex_height[domain_map[1]]; */

            point[0] = vlist(indices[2], 0);
            point[1] = vlist(indices[2], 1);
            point[2] = base.vertex_height[domain_map[2]];
            vertices.row(2) << point[cmap[0]], point[cmap[1]], point[cmap[2]];
            /* vertices.row(2) << vlist(indices[2], 0), vlist(indices[2], 1), base.vertex_height[domain_map[2]]; */

            // top
            point[0] = vlist(indices[0], 0);
            point[1] = vlist(indices[0], 1);
            point[2] = top.vertex_height[domain_map[0]];
            vertices.row(3) << point[cmap[0]], point[cmap[1]], point[cmap[2]];
            /* vertices.row(3) << vlist(indices[0], 0), vlist(indices[0], 1), top.vertex_height[domain_map[0]]; */

            point[0] = vlist(indices[1], 0);
            point[1] = vlist(indices[1], 1);
            point[2] = top.vertex_height[domain_map[1]];
            vertices.row(4) << point[cmap[0]], point[cmap[1]], point[cmap[2]];
            /* vertices.row(4) << vlist(indices[1], 0), vlist(indices[1], 1), top.vertex_height[domain_map[1]]; */

            point[0] = vlist(indices[2], 0);
            point[1] = vlist(indices[2], 1);
            point[2] = top.vertex_height[domain_map[2]];
            vertices.row(5) << point[cmap[0]], point[cmap[1]], point[cmap[2]];
            /* vertices.row(5) << vlist(indices[2], 0), vlist(indices[2], 1), top.vertex_height[domain_map[2]]; */

            volume = 0.;
            tetrahedra.resize(3, 4);
            if (indices[1] < indices[2])
            {
                tetrahedra.row(0) << 0, 1, 2, 5;
                tetrahedra.row(1) << 0, 1, 5, 4;
                tetrahedra.row(2) << 0, 4, 5, 3;
            }
            else if (indices[2] < indices[1])
            {
                tetrahedra.row(0) << 0, 1, 2, 4;
                tetrahedra.row(1) << 0, 4, 2, 5;
                tetrahedra.row(2) << 0, 4, 5, 3;
            }
            else
            {
                // abort
            }
            // tetrahedra -= Eigen::MatrixXi::Ones(3, 4);

            return true;
        }

        double computeVolume()
        {
            double vol = 0.;

            Eigen::MatrixXd vertices;
            Eigen::MatrixXi tetrahedra;
            tetrahedralize(vertices, tetrahedra);
            Eigen::RowVectorXi vertex_ids;
            
            auto abs_volume = [](const Eigen::RowVector3d& a, const Eigen::RowVector3d& b, const Eigen::RowVector3d& c, const Eigen::RowVector3d& d) -> double {
                return std::abs(RobustPredicates::Get().signed_volume(a, b, c, d))/6;
            };

            for (int i = 0; i < tetrahedra.rows(); ++i)
            {
                vertex_ids = tetrahedra.row(i);
                vol += abs_volume(// std::abs(RobustPredicates::Get().signed_volume(
                            vertices.row(vertex_ids[0]), 
                            vertices.row(vertex_ids[1]), 
                            vertices.row(vertex_ids[2]), 
                            vertices.row(vertex_ids[3])
                            //)
                        );
            }

            volume = vol;
            return vol;
        }

        /* bool getBoundarySimplicialComplex(Eigen::MatrixXd& vertices, Eigen::MatrixXi& triangles) */
        /* { */
            /* return false; */
        /* } */

        void setRegion(const std::vector<std::size_t>& lbound)
        {
            lower_bound = lbound;
            /* std::sort(lower_bound.begin(), lower_bound.end()); */
            region_id = static_cast<int>(lower_bound.size()) - 1;
        }

    private:
        bool got_domain_ = false;
        bool got_top_base_ = false;
        bool empty_ = true;
};

} // namespace geometric_primitives

template<typename T>
class MeshBuilder
{
    public:
        using RegionIndexType = int;
        using NumberOfVertices = int;
        using NumberOfElements = int;
        using TriangleImage = geometric_primitives::TriangleImage;
        /* using Tetrahedron = geometric_primitives::Tetrahedron<RegionIndexType>; */
        using Prism = geometric_primitives::Prism;

        MeshBuilder() = delete;
        ~MeshBuilder() = default;

        MeshBuilder(SRules<T>& container) : container_(container) {}

        // Methods
        bool buildPrismMesh(const Eigen::MatrixXd& vertices, const Eigen::MatrixXi& triangles);

        bool exportToTetgen(std::string) { return false; };

        bool exportToVTK(std::string);
        bool exportToVTK(std::string, const std::vector<int>& region_to_domain_map);

        bool getOrderedSurfaceIndicesList(std::vector<size_t> &);

        std::tuple<NumberOfVertices, NumberOfElements> getTetrahedralMesh(std::vector<double>& vlist, std::vector<std::size_t>& elist, std::vector<long int>& rlist);

        std::tuple<NumberOfVertices, NumberOfElements> getTetrahedralMesh(RegionIndexType region_id, std::vector<double>& vlist, std::vector<int>& elist);

        template<typename VertexList, typename ElementList>
        bool getBoundarySimplicialComplex(VertexList &, ElementList &);

        template<typename VertexList, typename ElementList, typename TriangleList>
        bool getBoundarySimplicialComplex(RegionIndexType region_id, VertexList &, ElementList &, TriangleList &);

        bool mapPointsToRegions(const std::vector<Point3>& /* points */, std::vector<int>& /* attrib_list */);

        bool mapRegionToBoundingSurfaces(RegionIndexType /* region_id */, std::vector<size_t>& /* lower_bound */, std::vector<size_t>& /* upper_bound */);

        bool getRegionVolumeList(std::vector<double>& /* volumes */);

        bool meshIsBuilt()
        {
            return !meshed_regions_.empty();
        }

        int maxNumRegions() { return static_cast<int>(container_.size()) - 1; }

    private:
        SRules<T> &container_;
        Eigen::MatrixXd domain_vertices_;

        struct RegionGeometry {
            double volume_ = 0.;
            std::vector<Prism> prisms_{};

            bool empty() { return prisms_.empty(); }
            
            bool getBoundaryMesh();

            std::tuple<int, int> getTetrahedralMesh(std::vector<double>& vlist, std::vector<int>& elist)
            {
                Eigen::MatrixXd vertices;
                Eigen::MatrixXi tetrahedra;
                vlist.clear();
                elist.clear();
                int numProcVertices = 0;
                int numProcTetrahedra = 0;
                int nrows, ncols;
                for (auto& p : prisms_)
                {
                    if(p.tetrahedralize(vertices, tetrahedra))
                    {
                        nrows = vertices.rows();
                        ncols = vertices.cols();
                        // std::cout << "\n vlist:";
                        for (int i = 0; i < nrows; ++i)
                        {
                            // for (int j = 0; j < ncols; ++j)
                            {
                                vlist.push_back(vertices(i,0));
                                // std::cout << vlist.back() << " ";
                                
                                vlist.push_back(vertices(i,1));
                                // std::cout << vlist.back() << " ";
                                
                                vlist.push_back(vertices(i,2));
                                // std::cout << vlist.back() << " ";
                            }
                        }
                        
                        nrows = tetrahedra.rows();
                        ncols = tetrahedra.cols();
                        // std::cout << "\n elist: ";
                        for (int i = 0; i < nrows; ++i)
                        {
                            for (int j = 0; j < ncols; ++j)
                            {
                                elist.push_back(tetrahedra(i,j) + numProcVertices);
                                // std::cout << elist.back() << " ";
                            }
                        }
                        
                        // std::cout << "\n" << std::flush;
                        numProcVertices += vertices.rows();
                        numProcTetrahedra += tetrahedra.rows();
                    }
                }
                return std::tie(numProcVertices, numProcTetrahedra);
            }

            bool checkConsistency()
            {
                std::vector<std::size_t> ref_lbound;
                int ref_region = -1;
                std::size_t i = 0;
                for (; i < prisms_.size(); ++i)
                {
                    if (!prisms_[i].empty())
                    {
                        ref_lbound = prisms_[i].lower_bound;
                        std::sort(ref_lbound.begin(), ref_lbound.end());
                        ref_region = prisms_[i].region_id;
                        break;
                    }

                }
                if (ref_region != (static_cast<int>(ref_lbound.size()) - 1))
                {
                    DCHECK(ref_region == (static_cast<int>(ref_lbound.size()) - 1));
                    return false;
                }
                for (; i < prisms_.size(); ++i)
                {
                    if (prisms_[i].empty())
                    {
                        continue;
                    }
                    auto lbound = prisms_[i].lower_bound;
                    std::sort(lbound.begin(), lbound.end());
                    if (lbound != ref_lbound)
                    {
                        /* std::sort(prisms_[i].lower_bound.begin(), prisms_[i].lower_bound.end()); */
                        /* std::sort(ref_lbound.begin(), ref_lbound.end()); */
                        DCHECK(lbound == ref_lbound);
                        return false;
                    }
                    if (prisms_[i].region_id != ref_region)
                    {
                        DCHECK(prisms_[i].region_id == ref_region);
                        return false;
                    }
                }
                return true;
            }

            std::vector<std::size_t> lowerBound()
            {
                std::vector<std::size_t> bound{};
                if (!prisms_.empty())
                {
                    bound = prisms_.front().lower_bound;
                }
                return bound;
            }
        };

        std::map<RegionIndexType, RegionGeometry, std::less<RegionIndexType>> meshed_regions_;
        std::vector<std::size_t> ordered_surface_indices;

        bool computeOrderedSurfaceIndicesList();

        TriangleImage getTriangleImage(std::size_t surface_id, const Eigen::MatrixXd& vlist, const Eigen::RowVector3i& triangle)
        {
            TriangleImage ti;
            double h;

            Eigen::RowVectorXd p0 = vlist.row(triangle[0]);
            container_[surface_id]->getHeight({p0[0], p0[1]}, h);
            ti.setVertex(0, h);

            Eigen::RowVectorXd p1 = vlist.row(triangle[1]);
            container_[surface_id]->getHeight({p1[0], p1[1]}, h);
            ti.setVertex(1, h);

            Eigen::RowVectorXd p2 = vlist.row(triangle[2]);
            container_[surface_id]->getHeight({p2[0], p2[1]}, h);
            ti.setVertex(2, h);

            return ti;
        }
};

#include "mesh/mesh_builder.cpp"

#endif
