#include "mesh_builder.hpp"

#include <algorithm>
#include <array>

#include <fstream>
#include <iostream>
#include <iterator>
#include <memory>
#include <unordered_map>

/* #include "mesh_builder.hpp" */

/* template<typename T> */
/* bool MeshBuilder<T>::exportToTetgen( std::string filename ) */
/* { */
/*     /1* std::vector<Prism> prism_list; *1/ */
/*     /1* std::vector<size_t> attribute_list; *1/ */

/*     /1* bool status = buildPrismMesh(prism_list); *1/ */

/*     /1* if ( status == false ) *1/ */
/*     /1* { *1/ */
/*         /1* return false; *1/ */
/*     /1* } *1/ */

/*     /1* % Create node file *1/ */
/*     std::ofstream nodeofs(filename + ".node"); */

/*     if ( !nodeofs.good() ) */
/*     { */
/*         return false; */ 
/*     } */

/*     std::vector<double> vcoords; */
/*     size_t num_vertices = getVertexCoordinates(vcoords); */

/*     /1* Header: <# of points> <dimension (3)> <# of attributes> <boundary markers (0 or 1)> *1/ */
/*     nodeofs << num_vertices << " 3 0 0\n\n"; */

/*     /1* Body: <point #> <x> <y> <z> [attributes] [boundary marker] *1/ */
/*     for ( size_t i = 0; i < vcoords.size()/3; ++i ) */
/*     { */
/*         nodeofs << i << " " */ 
/*             << vcoords[3*i + 0] << " " */ 
/*             << vcoords[3*i + 1] << " " */  
/*             << vcoords[3*i + 2] << std::endl; */
/*     } */


/*     /1* % Create ele file *1/ */
/*     std::ofstream eleofs(filename + ".ele"); */

/*     if ( !eleofs.good() ) */
/*     { */
/*         return false; */ 
/*     } */

/*     std::vector<size_t> elelist; */
/*     std::vector<size_t> attlist; */
/*     /1* size_t num_tetrahedra = getElementList(prism_list, elelist, attlist); *1/ */
/*     size_t num_tetrahedra = getTetrahedronList(elelist, attlist); */

/*     /1* Header: <# of tetrahedra> <nodes per tet. (4 or 10)> <region attribute (0 or 1)> *1/ */
/*     eleofs << num_tetrahedra << " 4 1\n\n"; */

/*     /1* Body: <tetrahedron #> <node> <node> ... <node> [attribute] *1/ */
/*     for ( size_t t = 0; t < num_tetrahedra; ++t ) */
/*     { */
/*         eleofs << t << "    " */ 
/*             << elelist[4*t + 0] << " " */ 
/*             << elelist[4*t + 1] << " " */ 
/*             << elelist[4*t + 2] << " " */ 
/*             << elelist[4*t + 3] << " " */
/*             << attlist[t] << std::endl; */
/*     } */

/*     return true; */
/* } */

template<typename T>
bool MeshBuilder<T>::exportToVTK( std::string filename )
{
    /* Create vtk file */
    std::ofstream vtkfs(filename + ".vtk");

    if ( !vtkfs.good() )
    {
        return false; 
    }


    // Create mesh
    /* std::vector<Prism> prism_list; */

    std::vector<double> vlist;
    std::vector<size_t> elist;
    std::vector<long int> rlist;

    /* bool status = buildPrismMesh(prism_list); */

    /* if ( status == false ) */
    /* { */
        /* return false; */
    /* } */

    /* size_t num_vertices = getVertexCoordinates(vlist); */
    /* size_t num_tetrahedra = getElementList(prism_list, elelist, attlist); */
    /* size_t num_tetrahedra = getTetrahedronList(elist, rlist); */

    auto [num_vertices, num_tetrahedra]= getTetrahedralMesh(vlist, elist, rlist);
    if (num_tetrahedra <= 0)
    {
        return false;
    }

    // Write file
    vtkfs << "# vtk DataFile Version 2.0\n";
    vtkfs << "Unstructured Grid\n";
    vtkfs << "ASCII\n";
    vtkfs << "DATASET UNSTRUCTURED_GRID\n\n";

    vtkfs << "POINTS " << num_vertices << " double\n";
    for ( size_t i = 0; i < num_vertices; ++i )
    {
        vtkfs << vlist[3*i + 0] << " "
            << vlist[3*i + 1] << " "
            << vlist[3*i + 2] << "\n";
    }
    vtkfs << "\n";

    vtkfs << "CELLS " << num_tetrahedra << " " << num_tetrahedra*5 << "\n";
    for ( size_t i = 0; i < num_tetrahedra; ++i )
    {
        vtkfs << "4" << " "
            << elist[4*i + 0] << " " 
            << elist[4*i + 1] << " " 
            << elist[4*i + 2] << " " 
            << elist[4*i + 3] << "\n"; 
    }
    vtkfs << "\n";

    vtkfs << "CELL_TYPES " << num_tetrahedra << "\n";
    for ( size_t i = 0; i < num_tetrahedra; ++i )
    {
        // the number "10" corresponds to "VTK_TETRA"
        vtkfs << "10" << "\n";
    }
    vtkfs << "\n";

    vtkfs << "CELL_DATA " << num_tetrahedra << "\n";
    vtkfs << "SCALARS cell_scalars int 1\n";
    vtkfs << "LOOKUP_TABLE default\n";
    for ( size_t i = 0; i < num_tetrahedra; ++i )
    {
        vtkfs << rlist[i] << "\n";
    }
    vtkfs << "\n";

    return true;
}

template<typename T>
bool MeshBuilder<T>::exportToVTK( std::string filename, const std::vector<int>& region_to_domain_map )
{
    /* Create vtk file */
    std::ofstream vtkfs(filename + ".vtk");

    if ( !vtkfs.good() )
    {
        return false; 
    }


    // Create mesh
    /* std::vector<Prism> prism_list; */

    std::vector<double> vlist;
    std::vector<size_t> elist;
    std::vector<long int> rlist;

    /* bool status = buildPrismMesh(prism_list); */

    /* if ( status == false ) */
    /* { */
        /* return false; */
    /* } */

    /* size_t num_vertices = getVertexCoordinates(vlist); */
    /* size_t num_tetrahedra = getElementList(prism_list, elelist, attlist); */
    /* size_t num_tetrahedra = getTetrahedronList(elist, rlist); */

    auto [num_vertices, num_tetrahedra]= getTetrahedralMesh(vlist, elist, rlist);
    if (num_tetrahedra <= 0)
    {
        return false;
    }

    /* auto iter = std::max_element(rlist.begin(), rlist.end()); */
    if (region_to_domain_map.size() != maxNumRegions())
    {
        return false;
    }

    // Write file
    vtkfs << "# vtk DataFile Version 2.0\n";
    vtkfs << "Unstructured Grid\n";
    vtkfs << "ASCII\n";
    vtkfs << "DATASET UNSTRUCTURED_GRID\n\n";

    vtkfs << "POINTS " << num_vertices << " double\n";
    for ( size_t i = 0; i < num_vertices; ++i )
    {
        vtkfs << vlist[3*i + 0] << " "
            << vlist[3*i + 1] << " "
            << vlist[3*i + 2] << "\n";
    }
    vtkfs << "\n";

    vtkfs << "CELLS " << num_tetrahedra << " " << num_tetrahedra*5 << "\n";
    for ( size_t i = 0; i < num_tetrahedra; ++i )
    {
        vtkfs << "4" << " "
            << elist[4*i + 0] << " " 
            << elist[4*i + 1] << " " 
            << elist[4*i + 2] << " " 
            << elist[4*i + 3] << "\n"; 
    }
    vtkfs << "\n";

    vtkfs << "CELL_TYPES " << num_tetrahedra << "\n";
    for ( size_t i = 0; i < num_tetrahedra; ++i )
    {
        // the number "10" corresponds to "VTK_TETRA"
        vtkfs << "10" << "\n";
    }
    vtkfs << "\n";

    vtkfs << "CELL_DATA " << num_tetrahedra << "\n";
    vtkfs << "SCALARS cell_scalars int 1\n";
    vtkfs << "LOOKUP_TABLE default\n";
    try
    {
        for ( size_t i = 0; i < num_tetrahedra; ++i )
        {
            vtkfs << region_to_domain_map.at(rlist[i]) << "\n";
        }
    }
    catch(const std::out_of_range& e)
    {
        std::cerr << "Failed to export vtk file -- out of range error: " << e.what() << std::endl;
        return false;
    }
    vtkfs << "\n";

    return true;
}

/* #include <algorithm> */

template<typename T>
bool MeshBuilder<T>::computeOrderedSurfaceIndicesList()
{
    /* std::vector<size_t> ordered_surface_indices = {}; */

    /* bool status = true; */
    if ( !meshIsBuilt() )
    {
        return false;
    }

    /* std::cout << "\n\n\nTrying to order the surfaces.\n\n"; */

    auto set_minus = [] ( std::vector<size_t> minuend, std::vector<size_t> subtrahend ) -> std::vector<size_t>
    {
        std::vector<size_t> result;

        std::sort(minuend.begin(), minuend.end());
        std::sort(subtrahend.begin(), subtrahend.end());

        std::set_difference(minuend.begin(), minuend.end(), subtrahend.begin(), subtrahend.end(), std::back_inserter(result));

        return result;
    };

    /* auto set_union = [] ( std::vector<size_t> lhs, std::vector<size_t> rhs ) -> std::vector<size_t> */
    /* { */
    /*     std::vector<size_t> result; */

    /*     std::sort(lhs.begin(), lhs.end()); */
    /*     std::sort(rhs.begin(), rhs.end()); */

    /*     std::set_union(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::back_inserter(result)); */

    /*     return result; */
    /* }; */

    auto cat_in_place = [] ( std::vector<size_t> &lhs, std::vector<size_t> rhs ) -> void 
    {
        if ( rhs.empty() )
        {
            return;
        }

        std::copy(rhs.begin(), rhs.end(), std::back_inserter(lhs));

        return;
    };

    std::vector<size_t> lower_bound, upper_bound, current;
    ordered_surface_indices.clear();

    for (auto& [region_id, region_geometry] : meshed_regions_)
    {
        if (region_id < 0)
        {
            continue;
        }
        if (region_geometry.empty())
        {
            continue;
        }

        lower_bound = region_geometry.lowerBound();
        /* attribute = it->second; */
        /* success = mapAttributeToBoundingSurfaces(attribute, lower_bound, upper_bound); */
        /* if ( !success ) */
        /* { */
        /*     /1* std::cout << "\nFailed to get lower and upper bounds\n" << std::flush; *1/ */
        /*     return false; */
        /* } */

        if ( false /* it == attributes_map.begin() */ )
        {
            /* std::cout << "Initial step:\n    lower_bound --> "; */
            /* for ( auto &i : lower_bound ) */
            /* { */
                /* std::cout << i << " "; */
            /* } */

            /* if ( lower_bound.size() > 1 ) */
            /* { */
                /* std::cout << "\nInitial step: lower_bound.size() =" << lower_bound.size() << "\n" << std::flush; */
                /* return false; */
            /* } */

            /* ordered_surface_indices.push_back( lower_bound.back() ); */
            cat_in_place(ordered_surface_indices, lower_bound);

            /* std::cout << "\n    ordered_surface_indices --> "; */
            /* for ( auto &i : ordered_surface_indices ) */
            /* { */
                /* std::cout << i << " "; */
            /* } */
            /* std::cout << "\n" << std::flush; */
        }
        else
        {
            current = set_minus(lower_bound, ordered_surface_indices);

            /* std::cout << "Intermediate step:\n    lower_bound --> "; */
            /* for ( auto &i : lower_bound ) */
            /* { */
                /* std::cout << i << " "; */
            /* } */
            /* std::cout << "\n    current --> "; */
            /* for ( auto &i : current ) */
            /* { */
                /* std::cout << i << " "; */
            /* } */

            /* if ( current.size() > 1 ) */
            /* { */
                /* std::cout << "\nIntermediate step: current.size() =" << current.size() << "\n" << std::flush; */
                /* return false; */
            /* } */

            /* ordered_surface_indices.push_back( current.back() ); */
            cat_in_place(ordered_surface_indices, current);

            /* std::cout << "\n    ordered_surface_indices --> "; */
            /* for ( auto &i : ordered_surface_indices ) */
            /* { */
                /* std::cout << i << " "; */
            /* } */
            /* std::cout << "\n" << std::flush; */
        }
    }

    /* std::cout << "Final step:\n    upper_bound --> "; */
    /* for ( auto &i : upper_bound ) */
    /* { */
        /* std::cout << i << " "; */
    /* } */

    /* if ( upper_bound.size() > 1 ) */
    /* { */
        /* std::cout << "\nFinal step: upper_bound.size() =" << upper_bound.size() << "\n" << std::flush; */
        /* return false; */
    /* } */
    
    /* ordered_surface_indices.push_back( upper_bound.back() ); */
    upper_bound.resize(container_.size());
    std::iota(upper_bound.begin(), upper_bound.end(), 0);
    upper_bound = set_minus(upper_bound, ordered_surface_indices);
    cat_in_place(ordered_surface_indices, upper_bound);
    
    /* std::cout << "\n    ordered_surface_indices --> "; */
    /* for ( auto &i : ordered_surface_indices ) */
    /* { */
        /* std::cout << i << " "; */
    /* } */
    /* std::cout << "\n" << std::flush; */

    //
    // O0 = empty;
    //
    // Step 1: #( L(R1) ) = 1; #( R(R1) ) > 1;
    //          O1 = L(R1)
    //
    // Step k: #( L(Rk) \ O{k-1} ) = 1; #( R(Rk) ) > 1;
    //          Sk = L(Rk) \ O{k-1}
    //          Ok = { O{k-1} < Sk } 
    //
    // Step n: n = num_regions = num_surfaces - 1; #( L(Rn) \ O{n-1} ) = 1; #( R(Rn) ) = 1;
    //          Sn = L(Rn) \ O{n-1}
    //          On = { O{n-1} < Sn < R(Rn) }
    //

    return true;
}

template<typename T>
bool MeshBuilder<T>::getOrderedSurfaceIndicesList(std::vector<size_t>& surface_ids)
{
    if (ordered_surface_indices.empty())
    {
        return false;
    }
    surface_ids = ordered_surface_indices;
    return true;
}

template<typename T>
bool MeshBuilder<T>::buildPrismMesh(const Eigen::MatrixXd& vertices, const Eigen::MatrixXi& triangles)
{
    int maxNumRegions = static_cast<int>(container_.size()) - 1;
    if ( maxNumRegions < 1 )
    {
        return false;
    }
    auto numSurfaces = container_.size();
    domain_vertices_ = vertices;
    meshed_regions_.clear();

    // meshed_regions_ should be shared along the for loop
    // meshed_regions_.reserve(maxNumRegions);

/*     // ordered_surfaces if firstprivate along the for loop */
/*     std::vector< std::pair<bool, size_t> > ordered_surfaces(numSurfaces); */

/*     // these are private along the for loop */
    std::vector<std::pair<TriangleImage, std::size_t>> list_of_triangles_images;
    list_of_triangles_images.resize(numSurfaces);
    TriangleImage triangle_image;
    struct lessThanOnSurfaceImageIndexPair {
        using Entry = std::pair<TriangleImage, std::size_t>;
        bool operator()(const Entry& lhs, const Entry& rhs)
        {
            if (lhs.first >= rhs.first)
            {
                return false;
            }
            else if (lhs.first == rhs.first)
            {
                if (lhs.second >= rhs.second)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }
    } orderOnImages;
    auto getLowerBound = [&](const std::vector<std::pair<TriangleImage, std::size_t>>& list, std::size_t pos) -> std::vector<std::size_t> {
        std::vector<std::size_t> lbound{};
        for (std::size_t i = 0; i < pos; ++i)
        {
            lbound.push_back(list[i].second);
        }
        return lbound;
    };
/*     std::vector<size_t> valid_surfaces; */
/*     std::vector<bool> cur_attribute; */
/*     std::vector<std::vector<bool>> attribute_list; */

    // Paralelize this loop?
    int numTriangles = triangles.rows();
    for (int triangle = 0; triangle < numTriangles; ++triangle)
    {
        // For every triangle triangle, we'll get all surfaces' values and 
        // their correpondent prisms.  
        //
        // This loop: 
        //     1. Gets the surfaces' heights for every triangle
        //     2. Sort surfaces accordingly to their height.
        //     3. Use the sorting in (2) to build prisms.
        //     4. Compute and store region prism belongs to.
        //

        //     1: Gets the surfaces' heights for every triangle
        for ( size_t surf_id = 0; surf_id < numSurfaces; ++surf_id )
        {
            triangle_image = getTriangleImage(surf_id, domain_vertices_, triangles.row(triangle));
            list_of_triangles_images[surf_id] = std::make_pair(triangle_image, surf_id);
        }

        //     2: Sort surfaces accordingly to their height.
        std::sort(list_of_triangles_images.begin(), list_of_triangles_images.end(), orderOnImages);

        //     3|4: Use the sorting in (2) to build prisms; compute region prism belongs to.
        for (std::size_t s = 1; s < numSurfaces; ++s)
        {
            Prism p(domain_vertices_);
            p.setDomain(triangles.row(triangle));
            p.setTopBase(list_of_triangles_images[s].first, list_of_triangles_images[s - 1].first);
            if (!p.empty())
            {
                /* Eigen::Vector3d c = p.centroid(); */
                /* p.lower_bound = container_.getSurfacesBelowPoint({c[0], c[1], c[2]}); */
                p.lower_bound = getLowerBound(list_of_triangles_images, s);
                /* p.region_id = std::min(std::max(static_cast<int>(p.lower_bound.size()) - 1, 0), maxNumRegions); */
                p.region_id = static_cast<int>(p.lower_bound.size()) - 1;
                meshed_regions_[p.region_id].volume_ += p.computeVolume();
                meshed_regions_[p.region_id].prisms_.push_back(p);
            }
        }
    }

    if ( meshed_regions_.empty() )
    {
        return false;
    }
    bool success = computeOrderedSurfaceIndicesList();

    return success;
}

template<typename T>
std::tuple<int, int> MeshBuilder<T>::getTetrahedralMesh(RegionIndexType region_id, std::vector<double>& vlist, std::vector<int>& elist)
{
    auto iter = meshed_regions_.find(region_id);
    if (iter == meshed_regions_.end())
    {
        return std::make_tuple(0, 0);
    }

    auto [_, region_geometry] = *iter;
    vlist.clear();
    elist.clear();
    auto [numVer, numTet] = region_geometry.getTetrahedralMesh(vlist, elist);

    return std::tie(numVer, numTet);
}

template<typename T>
std::tuple<int, int> MeshBuilder<T>::getTetrahedralMesh(std::vector<double>& vlist, std::vector<std::size_t>& elist, std::vector<long int>& rlist)
{
    if (meshed_regions_.empty())
    {
        return std::make_tuple(0, 0);
    }

    int numProcVertices = 0;
    int numProcTetrahedra = 0;
    vlist.clear();
    elist.clear();
    rlist.clear();
    for (auto& [region_id, region_geometry] : meshed_regions_)
    {
        DLOG_IF(ERROR, region_geometry.checkConsistency() == false) << "MeshBuilder<>::RegionGeometry::checkConsistency failure for region: " << region_id;
        /* if (region_geometry.checkConsistency() == false) */
        /* { */
            /* std::cout << "checkConsistency failure for region: " << region_id << std::endl; */
        /* } */
        std::vector<double> vertices;
        std::vector<int> tetrahedra;
        auto [numVer, numTet] = region_geometry.getTetrahedralMesh(vertices, tetrahedra);
        /* vlist.insert(vlist.end(), vertices.data(), vertices.data() + vertices.size()); */
        for (auto& coord : vertices)
        {
            vlist.push_back(coord);
        }
        for (auto& index : tetrahedra)
        {
            elist.push_back(static_cast<std::size_t>(index + numProcVertices));
        }
        std::fill_n(std::back_inserter(rlist), numTet, region_id);
        
        numProcVertices += numVer;
        numProcTetrahedra += numTet;
    }

    return std::tie(numProcVertices, numProcTetrahedra);
}

template<typename T>
bool MeshBuilder<T>::mapPointsToRegions(const std::vector<Point3>& points, std::vector<int>& regions_indices)
{
    regions_indices.clear();
    regions_indices.reserve(points.size());
    for (auto& p : points)
    {
        int rid = static_cast<int>(container_.getSurfacesBelowPoint(p).size()) - 1;
        rid = (rid <= maxNumRegions()) ? rid : -1;
        regions_indices.push_back(rid);
    }

    return true;
}

template<typename T>
bool MeshBuilder<T>::mapRegionToBoundingSurfaces(RegionIndexType region_id, std::vector<size_t>& lower_bound, std::vector<size_t>& upper_bound)
{
    if (!meshIsBuilt())
    {
        return false;
    }

    if (region_id < 0 || region_id > maxNumRegions())
    {
        return false;
    }

    std::size_t numSurfaces = static_cast<std::size_t>(region_id) + 1;
    if (numSurfaces > container_.size())
    {
        return false;
    }

    for (std::size_t i = 0; i < numSurfaces; ++i)
    {
        lower_bound.push_back(ordered_surface_indices[i]);
    }
    for (std::size_t i = numSurfaces; i < container_.size(); ++i)
    {
        upper_bound.push_back(ordered_surface_indices[i]);
    }

    /* auto print_vector = [](const std::vector<std::size_t>& vec) { */
    /*     for (auto i : vec) */
    /*     { */
    /*         std::cout << i << " "; */
    /*     } */
    /* }; */
    
    /* std::cout << "\nOrdered surfaces' indices: "; */
    /* print_vector(ordered_surface_indices); */

    /* std::cout << "\nLower bound: "; */
    /* print_vector(lower_bound); */

    /* std::cout << "\nUpper bound: "; */
    /* print_vector(upper_bound); */

    return true;
}

template<typename T>
bool MeshBuilder<T>::getRegionVolumeList(std::vector<double>& volumes)
{
    if (maxNumRegions() > 0)
    {
        volumes.resize(maxNumRegions(), 0);
    }

    for (auto& [region_id, region_geometry] : meshed_regions_)
    {
        if ((region_id > -1) && (region_id < maxNumRegions()))
        {
            volumes[region_id] = region_geometry.volume_;
        }
    }

    return true;
}

