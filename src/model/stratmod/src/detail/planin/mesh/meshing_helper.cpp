/* This file is part of the plain library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The plain library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The plain library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the plain library.  If not, see <https://www.gnu.org/licenses/>. */

#include "planin/mesh/meshing_helper.hpp"

#include <cstdarg>
#include <filesystem>
#include <iterator>

#include "glog/logging.h"

#include "igl/copyleft/cgal/remesh_self_intersections.h"
#include "igl/copyleft/cgal/RemeshSelfIntersectionsParam.h"
#include "igl/copyleft/cgal/projected_cdt.h"
#include "igl/barycenter.h"
#include "igl/bfs_orient.h"
#include "igl/boundary_facets.h"
#include "igl/oriented_facets.h"
#include "igl/unique_edge_map.h"
#include "igl/remove_unreferenced.h"
#include "igl/combine.h"
#include "igl/winding_number.h"
#include "igl/writePLY.h"

using namespace igl::copyleft::cgal;

void MeshingHelper::resetBaseMesh(Point3 origin, Point3 length, std::size_t discX, std::size_t discY)
{
    clear();

    base_mesh_.setDomainOrigin({origin[0], origin[1]});
    base_mesh_.setDomainLength({length[0], length[1]});
    base_mesh_.setDiscretization(discX, discY);

    bottom_ = origin[2];
    top_ = origin[2] + length[2];

    addTopAndBottomSurfaces();
}

void MeshingHelper::addTopAndBottomSurfaces()
{
    std::size_t num_coordinates = 3*base_mesh_.numVertices();
    std::vector<double> tvlist(num_coordinates), bvlist(num_coordinates);
    Point2 v;
    for (size_t i = 0; i < base_mesh_.numVertices(); ++i)
    {
        base_mesh_.vertex(i, v);

        tvlist[3*i + 0] = v[0];
        tvlist[3*i + 1] = v[1];
        tvlist[3*i + 2] = top_;

        bvlist[3*i + 0] = v[0];
        bvlist[3*i + 1] = v[1];
        bvlist[3*i + 2] = bottom_;
    }

    std::vector<std::size_t> flist;
    base_mesh_.triangleList(flist);

    addSurface(tvlist, flist);
    addSurface(bvlist, flist);
}

void MeshingHelper::getBaseDomainSimplicialComplex(Eigen::MatrixXd& OV, Eigen::MatrixXi& OF)
{
    std::size_t num_vertices = base_mesh_.numVertices();
    OV.resize(num_vertices, 2);
    Point2 v;
    for (size_t i = 0; i < num_vertices; ++i)
    {
        base_mesh_.vertex(i, v);

        OV(i, 0) = v[0];
        OV(i, 1) = v[1];
    }

    using StructuredTriangle = typename StructuredTriangleMesh2D<Point2>::Triangle;
    using Natural = typename StructuredTriangleMesh2D<Point2>::Natural;

    std::size_t num_triangles = base_mesh_.numTriangles();
    OF.resize(num_triangles, 3);
    StructuredTriangle t;
    for (Natural i = 0; i < num_triangles; ++i)
    {
        base_mesh_.triangle(i, t);
        OF(i, 0) = t[0];
        OF(i, 1) = t[1];
        OF(i, 2) = t[2];
    }
}

bool MeshingHelper::hasSurface(std::size_t sid)
{
    if (surface_ids_.find(sid) != surface_ids_.end())
    {
        return true;
    }

    return false;
}

bool MeshingHelper::addSurface(std::size_t sid, const std::vector<double>& vlist, const std::vector<std::size_t>& flist)
{
    if (hasSurface(sid))
    {
        return false;
    }

    surface_ids_.insert(sid);
    addSurface(vlist, flist);
    return true;
}

void MeshingHelper::addSurface(const std::vector<double>& vlist, const std::vector<std::size_t>& flist)
{
    DCHECK(vlist.size() % 3 == 0);
    DCHECK(flist.size() % 3 == 0);

    std::size_t num_stored_vertices = vertices_.size()/3;
    for (auto coord : vlist)
    {
        vertices_.push_back(coord);
    }

    auto num_vertices = vlist.size()/3;
    for (auto index : flist)
    {
        DCHECK(index < num_vertices);
        triangles_.push_back(index + num_stored_vertices);
    }

    has_dom_simplices_ = false;
}

bool MeshingHelper::computeDomainSimplicialComplex()
{
    return computeDomainSimplicialComplex(dom_simplices_vertices_, dom_simplices_triangles_);
}

bool MeshingHelper::computeDomainSimplicialComplex(Eigen::MatrixXd& OV, Eigen::MatrixXi& OF)
{
    if (surface_ids_.empty())
    {
        getBaseDomainSimplicialComplex(OV, OF);
        return true;
    }

    int num_vertices = static_cast<int>(vertices_.size()/3);
    int num_triangles = static_cast<int>(triangles_.size()/3);

    if ((num_vertices == 0) || (num_triangles == 0))
    {
        return false;
    }

    Eigen::MatrixXd V(num_vertices, 3);
    for (int i = 0; i < num_vertices; ++i)
    {
        V.row(i) << vertices_[3*i + 0], vertices_[3*i + 1], vertices_[3*i + 2];
    }
    Eigen::MatrixXi F(num_triangles, 3);
    for (int i = 0; i < num_triangles; ++i)
    {
        F.row(i) << triangles_[3*i + 0], triangles_[3*i + 1], triangles_[3*i + 2];
    }

    RemeshSelfIntersectionsParam params;
    Eigen::MatrixXd VV;
    Eigen::MatrixXi FF, IF;
    Eigen::VectorXi J, IM;
    remesh_self_intersections(V, F, params, VV, FF, IF, J, IM);
    /* writePLY("remeshed.ply", VV, FF); */

    using Kernel = CGAL::Exact_predicates_exact_constructions_kernel;
    std::vector<CGAL::Triangle_3<Kernel>> T;
    mesh_to_cgal_triangle_list(VV, FF, T);

    CGAL::Plane_3<Kernel> P(0, 0, 1, 0);
    std::vector<CGAL::Object> objects;
    for (auto& t : T)
    {
        objects.push_back(CGAL::make_object(t));
    }
    projected_cdt(objects, P, OV, OF);
    /* writePLY("domain.ply", OV, OF); */

    return true;
}

bool MeshingHelper::getDomainSimplicialComplex(Eigen::MatrixXd& OV, Eigen::MatrixXi& OF)
{
    if (has_dom_simplices_)
    {
        OV = dom_simplices_vertices_;
        OF = dom_simplices_triangles_;

        return true;
    }

    bool success = computeDomainSimplicialComplex();
    if (success)
    {
        OV = dom_simplices_vertices_;
        OF = dom_simplices_triangles_;
        has_dom_simplices_ = true;
    }

    return success;
}

void MeshingHelper::filterEdgesOnPlane(const Eigen::Vector3d& p, const Eigen::Vector3d& u, const Eigen::Vector3d& v,
        const Eigen::MatrixXd& VI, const Eigen::MatrixXi& EI, Eigen::MatrixXi& EO, double tol)
{
    auto edge_works = [&](const Eigen::Vector3d& p, const Eigen::Vector3d& u, const Eigen::Vector3d& v,
            const Eigen::MatrixXd& V, const Eigen::RowVector2i& edge) -> bool
    {
        bool works = true;
        Eigen::Vector3d P0, P1, PP0, PP1;

        P0 << V(edge(0), 0), V(edge(0), 1), V(edge(0), 2);
        PP0 = p + u.dot(P0 - p)*u + v.dot(P0 - p)*v;
        works &= (P0 - PP0).norm() < tol;

        P1 << V(edge(1), 0), V(edge(1), 1), V(edge(1), 2);
        PP1 = p + u.dot(P1 - p)*u + v.dot(P1 - p)*v;
        works &= (P1 - PP1).norm() < tol;

        return works;
    };

    std::vector<int> el;
    for(int i = 0; i < EI.rows(); ++i)
    {
        if (edge_works(p, u, v, VI, EI.row(i)))
        {
            {
                el.push_back(EI(i,0));
                el.push_back(EI(i,1));
            }
        }
    }

    EO.resize(el.size()/2, 2);
    for (int i = 0; i < EO.rows(); ++i)
    {
        EO(i, 0) = el[2*i + 0];
        EO(i, 1) = el[2*i + 1];
    }
}

void MeshingHelper::intersectSurfaceWithPlaneCurve3D(
        const Eigen::MatrixXd& SV, const Eigen::MatrixXi& ST,
        const Eigen::Vector3d& O, const Eigen::Vector3d& T, 
        Eigen::MatrixXd& curve_vertices, Eigen::MatrixXi& curve_edges)
{
    /* std::cout << "0" << std::endl << std::flush; */
    // Building plane VP, FP
    auto start = std::chrono::system_clock::now();
    Eigen::MatrixXd VP;
    auto origin = base_mesh_.origin();
    auto length = base_mesh_.length();
    /* std::cout << "I'm here." << std::endl << std::flush; */
    /* std::cout << "Origin: " << origin[0] << ", " << origin[1] << std::endl << std::flush; */
    /* std::cout << "Length: " << length[0] << ", " << length[1] << std::endl << std::flush; */
    /* std::cout << "Top/Bottom: " << top_ << ", " << bottom_ << std::endl << std::flush; */
    /* std::cout << "O: " << O(0) << ", " << O(1) << ", " << O(2) << std::endl << std::flush; */
    /* std::cout << "T: " << T(0) << ", " << T(1) << ", " << T(2) << std::endl << std::flush; */
    Eigen::Vector3d lbbox, ubbox, diag, Up;
    lbbox << origin[0], origin[1], bottom_;
    ubbox << origin[0] + length[0], origin[1] + length[1], top_;
    diag = ubbox - lbbox;
    Up << 0., 0., 1.;
    double d = diag.norm();

    /* std::cout << "1" << std::endl << std::flush; */
    VP.resize(4, 3);
    VP.row(0) = O - d*T - d*Up;
    VP.row(1) = O + d*T - d*Up;
    VP.row(2) = O + d*T + d*Up;
    VP.row(3) = O - d*T + d*Up;
    
    /* std::cout << "2" << std::endl << std::flush; */
    int num_vertices = SV.rows();
    Eigen::MatrixXi FP;
    FP.resize(2, 3);
    FP << 0, 1, 2,
       2, 3, 0;
    /* writePLY("plane_to_intersect.ply", VP, FP); */
    FP = FP + num_vertices * Eigen::MatrixXi::Ones(FP.rows(), FP.cols());

    /* std::cout << "3" << std::endl << std::flush; */
    // Merging meshes
    Eigen::MatrixXd V;
    V.resize(SV.rows() + VP.rows(), 3);
    /* V.block(0, 0, SV.rows(), 3) = SV; */
    /* V.block(SV.rows(), 0, VP.rows(), 3) = VP; //possible bug when seeting block */

    for (int i = 0; i < SV.rows(); ++i)
    {
        V.row(i) = SV.row(i);
    }

    for (int i = 0; i < VP.rows(); ++i)
    {
        V.row(i + SV.rows()) = VP.row(i);
    }


    /* std::cout << "4" << std::endl << std::flush; */
    Eigen::MatrixXi F;
    F.resize(ST.rows() + FP.rows(), 3);
    /* F.block(0, 0, ST.rows(), 3) = ST; */
    /* F.block(FP.rows(), 0, FP.rows(), 3) = FP; //possible bug when seeting block */

    for (int i = 0; i < ST.rows(); ++i)
    {
        F.row(i) = ST.row(i);
    }

    for (int i = 0; i < FP.rows(); ++i)
    {
        F.row(i + ST.rows()) = FP.row(i);
    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Time to prepare geometry: " << elapsed.count() << "\n" << std::flush;

    /* std::cout << "5" << std::endl << std::flush; */
    start = std::chrono::system_clock::now();
    RemeshSelfIntersectionsParam params;
    Eigen::MatrixXd SVV; //, VV; // use VV in place of curve_vertices
    Eigen::MatrixXi SFF, FF, IF;
    Eigen::VectorXi J, IM, UIM;
    remesh_self_intersections(V, F, params, SVV, SFF, IF, J, IM);
    // _apply_ duplicate vertex mapping IM to FF
    std::for_each(SFF.data(),SFF.data()+SFF.size(),[&IM](int & a){a=IM(a);});
    // remove any vertices now unreferenced after duplicate mapping.
    igl::remove_unreferenced(SVV,SFF,curve_vertices,FF,UIM);
    end = std::chrono::system_clock::now();
    elapsed = end - start;
    std::cout << "Time ro remesh self-intersections: " << elapsed.count() << "\n" << std::flush;

    /* writePLY("input_surface.ply", SV, ST); */
    /* writePLY("pre_intersection.ply", V, F); */
    /* writePLY("intersection_soup.ply", SVV, SFF); */
    /* writePLY("intersection_unref.ply", curve_vertices, FF); */

    start = std::chrono::system_clock::now();
    /* std::cout << "6" << std::endl << std::flush; */
    Eigen::MatrixXi E, uE, EMAP;
    std::vector<std::vector<int>> uE2E;
    igl::unique_edge_map(FF, E, uE, EMAP, uE2E);

    /* std::cout << "7" << std::endl << std::flush; */
    std::vector<std::vector<std::size_t>> curves;
    extractBranchingCurves(FF, uE2E, curves);
    end = std::chrono::system_clock::now();
    elapsed = end - start;
    std::cout << "Time to extract branching curves: " << elapsed.count() << "\n" << std::flush;


    /* std::cout << "8" << std::endl << std::flush; */
    /* DCHECK(curves.size() == 1); */
    /* if (curves.size() != 1) */
    /* { */
        /* std::cout << "NUMBER OF CURVES: " << curves.size() << std::endl << std::flush; */
    /* } */
    /* curve_edges.resize(curves.back().size()/2, 2); */
    std::vector<int> edges;
    std::vector<double> vlist;

    /* std::cout << "9" << std::endl << std::flush; */
    for (std::size_t n = 0; n < curves.size(); ++n)
    {
        auto& curve = curves[n];
        for (std::size_t i = 0; i < curve.size(); ++i)
        {
            int e0 = uE(curve[i], 0);
            int e1 = uE(curve[i], 1);

            edges.push_back(e0);
            edges.push_back(e1);
        }
    }

    Eigen::MatrixXi BE;
    BE.resize(edges.size()/2, 2);
    for (int i = 0; i < BE.rows(); ++i)
    {
        BE(i, 0) = edges[2*i + 0];
        BE(i, 1) = edges[2*i + 1];
    }

    filterEdgesOnPlane(O, T, Up, curve_vertices, BE, curve_edges);
    /* curve_edges = BE; */
    /* curve_vertices = VV; */

    /* std::cout << "E" << std::endl << std::flush; */
    /* writePLY("intersection_curve.ply", VV, FF, curve_edges); */

        /* vlist.push_back(V(e0, 0)); */
        /* vlist.push_back(V(e0, 1)); */
        /* vlist.push_back(V(e0, 2)); */

        /* vlist.push_back(V(e1, 0)); */
        /* vlist.push_back(V(e1, 1)); */
        /* vlist.push_back(V(e1, 2)); */

        /* curve_edges(i, 0) = 2*i + 0; */
        /* curve_edges(i, 1) = 2*i + 1; */
    /* } */

    /* curve_vertices.resize(vlist.size()/3, 3); */
    /* for (std::size_t i = 0; i < vlist.size()/3; ++i) */
    /* { */
        /* curve_vertices(i, 0) = vlist[3*i + 0]; */
        /* curve_vertices(i, 1) = vlist[3*i + 1]; */
        /* curve_vertices(i, 2) = vlist[3*i + 2]; */
    /* } */
}

void MeshingHelper::intersectSurfaceWithPlaneCurve2D(
        const Eigen::MatrixXd& SV, const Eigen::MatrixXi& ST,
        const Eigen::Vector3d& O, const Eigen::Vector3d& T, 
        Eigen::MatrixXd& curve_vertices, Eigen::MatrixXi& curve_edges)
{
    std::cout << "\nSTART ::: intersectSurfaceWithPlaneCurve2D(...):" << std::endl << std::flush;
    auto start = std::chrono::system_clock::now();
    /* Start */
    Eigen::MatrixXd V;
    Eigen::MatrixXi E;
    intersectSurfaceWithPlaneCurve3D(SV, ST, O, T, V, curve_edges);
    /* writePLY("intersection2.ply", V, E, curve_edges, false); */
    /* End */
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Time to intersect surface with plane and get curve 3D: " << elapsed.count() << "\n";

    start = std::chrono::system_clock::now();
    /* Start */
    Eigen::Vector3d Up;
    Up << 0., 0., 1.;

    curve_vertices.resize(V.rows(), 2);
    for (int i = 0; i < V.rows(); ++i)
    {
        curve_vertices(i, 0) = T.dot(V.row(i).transpose() - O);
        curve_vertices(i, 1) = Up.dot(V.row(i).transpose() - O);
    }
    /* End */
    end = std::chrono::system_clock::now();
    elapsed = end - start;
    std::cout << "Time to project 3D curve points into plane: " << elapsed.count() << "\n";
    std::cout << "END ::: intersectSurfaceWithPlaneCurve2D(...):" << std::endl << std::flush;
}

/* void MeshingHelper::intersectDomains( */
/*         const std::vector<Eigen::MatrixXd>& Vlist, const std::vector<Eigen::MatrixXi>& Flist, */
/*         /1* const Eigen::MatrixXd& VA, const Eigen::MatrixXi& FA, *1/ */
/*         /1* const Eigen::MatrixXd& VB, const Eigen::MatrixXi& FB, *1/ */
/*         Eigen::MatrixXd& VV, Eigen::MatrixXi& FF) */
/* { */
    /* std::cout << "\nSTART ::: intersectDomains(...):" << std::endl << std::flush; */
    /* auto start = std::chrono::system_clock::now(); */
    /* /1* Start *1/ */
    /* Eigen::MatrixXd V, OV; */
    /* Eigen::MatrixXi F, OF; */
    /* igl::combine(Vlist, Flist, V, F); */

    /* using Kernel = CGAL::Exact_predicates_exact_constructions_kernel; */
    /* std::vector<CGAL::Triangle_3<Kernel>> T; */
    /* mesh_to_cgal_triangle_list(V, F, T); */

    /* CGAL::Plane_3<Kernel> P(0, 0, 1, 0); */
    /* std::vector<CGAL::Object> objects; */
    /* for (auto& t : T) */
    /* { */
    /*     objects.push_back(CGAL::make_object(t)); */
    /* } */
    /* projected_cdt(objects, P, OV, OF); */
    /* /1* End *1/ */
    /* auto end = std::chrono::system_clock::now(); */
    /* std::chrono::duration<double> elapsed = end - start; */
    /* /1* std::cout << "Time to construct CDT: " << elapsed.count() << "\n"; *1/ */

    /* /1* std::cout << "Number of triangles in cdt " << OF.rows() << " triangles" << std::endl << std::flush; *1/ */
    /* /1* writePLY("intersected_domain.ply", OV, OF); *1/ */

    /* start = std::chrono::system_clock::now(); */
    /* /1* Start *1/ */
    /* Eigen::MatrixXd BC; */
    /* igl::barycenter(OV, OF, BC); */

    /* Eigen::MatrixXd VA, VB; */
    /* VA.resize(Vlist[0].rows(), 2); */
    /* VA.col(0) = Vlist[0].col(0); */
    /* VA.col(1) = Vlist[0].col(1); */
    /* VB.resize(Vlist[1].rows(), 2); */
    /* VB.col(0) = Vlist[1].col(0); */
    /* VB.col(1) = Vlist[1].col(1); */

    /* Eigen::MatrixXi FA, FB, J, K; */
    /* /1* igl::oriented_facets(Flist[0], FA); *1/ */
    /* /1* igl::oriented_facets(Flist[1], FB); *1/ */
    /* igl::boundary_facets(Flist[0], FA, J, K); */
    /* igl::boundary_facets(Flist[1], FB, J, K); */
    /* /1* End *1/ */
    /* end = std::chrono::system_clock::now(); */
    /* elapsed = end - start; */
    /* /1* std::cout << "Time to orient facets: " << elapsed.count() << "\n"; *1/ */

    /* /1* std::cout << "Adapted num vertices: " <<  VA.rows() << ", num triangles " << Flist[0].rows() << std::endl << std::flush; *1/ */
    /* /1* std::cout << "Actual num vertices: " <<  VB.rows() << ", num triangles " << Flist[1].rows() << std::endl << std::flush; *1/ */
    /* /1* writePLY("adapted.ply", Vlist[0], FA); *1/ */
    /* /1* writePLY("actual.ply", Vlist[1], FB); *1/ */

    /* start = std::chrono::system_clock::now(); */
    /* /1* Start *1/ */
    /* std::vector<int> fl; */
    /* Eigen::RowVector3d p; */
    /* double wa, wb; */
    /* for (int i = 0; i < BC.rows(); ++i) */
    /* { */
    /*     p = BC.row(i); */
    /*     wa = igl::winding_number(VA, FA, p); */
    /*     wb = igl::winding_number(VB, FB, p); */

    /*     /1* std::cout << "WN Adapted: " << wa << std::endl << std::flush; *1/ */
    /*     /1* std::cout << "WN Actual: " << wb << std::endl << std::flush; *1/ */
    /*     if ((std::abs(wa) > 0.5) && (std::abs(wb) > 0.5)) */
    /*     { */
    /*         fl.push_back(OF(i,0)); */
    /*         fl.push_back(OF(i,1)); */
    /*         fl.push_back(OF(i,2)); */
    /*     } */
    /* } */

    /* FF.resize(fl.size()/3, 3); */
    /* for (auto i = 0; i < FF.rows(); ++i) */
    /* { */
    /*     FF(i, 0) = fl[3*i + 0]; */
    /*     FF(i, 1) = fl[3*i + 1]; */
    /*     FF(i, 2) = fl[3*i + 2]; */
    /* } */
    /* /1* End *1/ */
    /* end = std::chrono::system_clock::now(); */
    /* elapsed = end - start; */
    /* /1* std::cout << "Time to compute winding numbers: " << elapsed.count() << "\n"; *1/ */

    /* VV = OV; */
    /* /1* std::cout << "END ::: intersectDomains(...):" << std::endl << std::flush; *1/ */
/* } */

void MeshingHelper::intersectDomains(
        const Eigen::MatrixXd& DomainVlist, const Eigen::MatrixXi& DomainFlist,
        const Eigen::MatrixXd& OriginalMeshVlist, const Eigen::MatrixXi& OriginalMeshFlist,
        Eigen::MatrixXd& OV, Eigen::MatrixXi& OF)
{
    /* std::cout << "\nSTART ::: intersectDomains(...):" << std::endl << std::flush; */
    auto start = std::chrono::system_clock::now();
    /* Start */
    Eigen::MatrixXd V, VV;
    Eigen::MatrixXi F, FF;
    /* igl::combine(Vlist, Flist, V, F); */
    Eigen::MatrixXi OriginalMeshBoundaryEdges, FA, J, K;
    igl::boundary_facets(OriginalMeshFlist, OriginalMeshBoundaryEdges, J, K);

    using Kernel = CGAL::Exact_predicates_exact_constructions_kernel;
    std::vector<CGAL::Triangle_3<Kernel>> Triangles;
    std::vector<CGAL::Segment_3<Kernel>> Edges;
    mesh_to_cgal_triangle_list(DomainVlist, DomainFlist, Triangles);
    mesh_to_cgal_boundary_edge_list<Kernel, CGAL::Segment_3<Kernel>>(OriginalMeshVlist, OriginalMeshBoundaryEdges, Edges);

    std::vector<CGAL::Object> objects;
    for (auto& t : Triangles)
    {
        objects.push_back(CGAL::make_object(t));
    }
    for (auto& e : Edges)
    {
        objects.push_back(CGAL::make_object(e));
    }
    CGAL::Plane_3<Kernel> P(0, 0, 1, 0);
    projected_cdt(objects, P, VV, FF);
    /* End */
    /* auto end = std::chrono::system_clock::now(); */
    /* std::chrono::duration<double> elapsed = end - start; */
    /* std::cout << "Time to construct CDT: " << elapsed.count() << "\n"; */

    /* std::cout << "Number of triangles in cdt " << FF.rows() << " triangles" << std::endl << std::flush; */
    /* writePLY("intersected_domain.ply", VV, FF); */

    /* start = std::chrono::system_clock::now(); */
    /* Start */
    Eigen::MatrixXd VA, VB;
    VA.resize(DomainVlist.rows(), 2);
    VA.col(0) = DomainVlist.col(0);
    VA.col(1) = DomainVlist.col(1);
    VB.resize(OriginalMeshVlist.rows(), 2);
    VB.col(0) = OriginalMeshVlist.col(0);
    VB.col(1) = OriginalMeshVlist.col(1);

    /* igl::oriented_facets(Flist[0], FA); */
    /* igl::oriented_facets(Flist[1], FB); */
    igl::boundary_facets(DomainFlist, FA, J, K);
    const Eigen::MatrixXi& FB = OriginalMeshBoundaryEdges;
    /* igl::boundary_facets(Flist[1], FB, J, K); */
    /* End */
    /* end = std::chrono::system_clock::now(); */
    /* elapsed = end - start; */
    /* std::cout << "Time to orient facets: " << elapsed.count() << "\n"; */

    /* std::cout << "Adapted num vertices: " <<  VA.rows() << ", num triangles " << Flist[0].rows() << std::endl << std::flush; */
    /* std::cout << "Actual num vertices: " <<  VB.rows() << ", num triangles " << Flist[1].rows() << std::endl << std::flush; */
    /* writePLY("adapted.ply", Vlist[0], FA); */
    /* writePLY("actual.ply", Vlist[1], FB); */

    start = std::chrono::system_clock::now();
    /* Start */
    Eigen::MatrixXd BC;
    igl::barycenter(VV, FF, BC);

    std::vector<int> fl;
    Eigen::RowVector3d p;
    double wa, wb;
    for (int i = 0; i < BC.rows(); ++i)
    {
        p = BC.row(i);
        wa = igl::winding_number(VA, FA, p);
        wb = igl::winding_number(VB, FB, p);

        /* std::cout << "WN Adapted: " << wa << std::endl << std::flush; */
        /* std::cout << "WN Actual: " << wb << std::endl << std::flush; */
        if ((std::abs(wa) > 0.5) && (std::abs(wb) > 0.5))
        {
            fl.push_back(FF(i,0));
            fl.push_back(FF(i,1));
            fl.push_back(FF(i,2));
        }
    }

    OF.resize(fl.size()/3, 3);
    for (auto i = 0; i < OF.rows(); ++i)
    {
        OF(i, 0) = fl[3*i + 0];
        OF(i, 1) = fl[3*i + 1];
        OF(i, 2) = fl[3*i + 2];
    }
    /* End */
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Time to intersectDomains(...): " << elapsed.count() << "\n";

    OV = VV;
    /* std::cout << "END ::: intersectDomains(...):" << std::endl << std::flush; */
    /* writePLY("final_domain.ply", OV, OF); */
}

template<typename Kernel, typename CGAL_Segment3>
void MeshingHelper::mesh_to_cgal_boundary_edge_list(const Eigen::MatrixXd& V, const Eigen::MatrixXi& F, std::vector<CGAL_Segment3>& Edges)
{
    using Point3 = CGAL::Point_3<Kernel>;
    using Segment3 = CGAL::Segment_3<Kernel>; 

    // **Copy?** to convert to output type (this is especially/only needed if the
    // input type DerivedV::Scalar is CGAL::Epeck
    /* Eigen::Matrix< */
    /*     typename Kernel::FT, */
    /*     DerivedV::RowsAtCompileTime, */
    /*     DerivedV::ColsAtCompileTime */
    /*     > KV(V.rows(),V.cols()); */
    /* assign(V,KV); */

    Edges.reserve(F.rows());
    // Loop over faces
    for(int f = 0;f<(int)F.rows();f++)
    {
        Edges.push_back(
                Segment3(
                    Point3(V(F(f,0),0), V(F(f,0),1), V(F(f,0),2)),
                    Point3(V(F(f,1),0), V(F(f,1),1), V(F(f,1),2))
                    )
                );
    }
    
}

// This is based on libigl code and is heavily dependent on other libigl methods.
// Trying to keep nomenclature as close as possible to libigl.
template<typename DerivedF, typename uE2EType>
void MeshingHelper::extractBranchingCurves(
        const Eigen::MatrixBase<DerivedF>& F,
        const std::vector<std::vector<uE2EType> >& uE2E,
        std::vector<std::vector<size_t> >& curves)
{
    const size_t num_faces = F.rows();
    DCHECK(F.cols() == 3);

    auto edge_index_to_face_index = [&](size_t ei)
    {
        return ei % num_faces;
    };
    auto edge_index_to_corner_index = [&](size_t ei)
    {
        return ei / num_faces;
    };
    auto get_edge_end_points = [&](size_t ei, size_t& s, size_t& d)
    {
        const size_t fi = edge_index_to_face_index(ei);
        const size_t ci = edge_index_to_corner_index(ei);
        s = F(fi, (ci+1)%3);
        d = F(fi, (ci+2)%3);
    };

    curves.clear();
    const size_t num_unique_edges = uE2E.size();
    std::unordered_multimap<size_t, size_t> vertex_edge_adjacency;
    std::vector<size_t> non_manifold_edges;

    for (size_t i=0; i<num_unique_edges; i++)
    {
        const auto& adj_edges = uE2E[i];
        if (adj_edges.size() <= 2) continue;

        const size_t ei = adj_edges[0];
        size_t s,d;
        get_edge_end_points(ei, s, d);

        vertex_edge_adjacency.insert({{s, i}, {d, i}});

        non_manifold_edges.push_back(i);

        DCHECK(vertex_edge_adjacency.count(s) > 0);
        DCHECK(vertex_edge_adjacency.count(d) > 0);
    }
    /* curves.push_back(non_manifold_edges); */
    /* return; */

    auto expand_forward = [&](std::list<size_t>& edge_curve,
            size_t& front_vertex, size_t& end_vertex)
    {
        while(vertex_edge_adjacency.count(front_vertex) == 2 && front_vertex != end_vertex)
        {
            auto adj_edges = vertex_edge_adjacency.equal_range(front_vertex);
            for (auto itr = adj_edges.first; itr!=adj_edges.second; itr++)
            {
                const size_t uei = itr->second;
                DCHECK(uE2E.at(uei).size() > 2);

                const size_t ei = uE2E[uei][0];
                if (uei == edge_curve.back()) continue;

                size_t s,d;
                get_edge_end_points(ei, s, d);
                edge_curve.push_back(uei);

                if (s == front_vertex)
                {
                    front_vertex = d;
                } else if (d == front_vertex)
                {
                    front_vertex = s;
                } else
                {
                    DLOG(FATAL) << "Invalid vertex/edge adjacency!";
                }
                break;
            }
        }
    };

    auto expand_backward = [&](std::list<size_t>& edge_curve,
            size_t& front_vertex, size_t& end_vertex)
    {
        while(vertex_edge_adjacency.count(front_vertex) == 2 && front_vertex != end_vertex) 
        {
            auto adj_edges = vertex_edge_adjacency.equal_range(front_vertex);
            for (auto itr = adj_edges.first; itr!=adj_edges.second; itr++)
            {
                const size_t uei = itr->second;
                DCHECK(uE2E.at(uei).size() > 2);

                const size_t ei = uE2E[uei][0];
                if (uei == edge_curve.front()) continue;

                size_t s,d;
                get_edge_end_points(ei, s, d);
                edge_curve.push_front(uei);

                if (s == front_vertex)
                {
                    front_vertex = d;
                } else if (d == front_vertex)
                {
                    front_vertex = s;
                } else
                {
                    DLOG(FATAL) << "Invalid vertex/edge adjacency!";
                }
                break;
            }
        }
    };

    std::vector<bool> visited(num_unique_edges, false);
    for (const size_t i : non_manifold_edges)
    {
        if (visited[i]) continue;

        std::list<size_t> edge_curve;
        edge_curve.push_back(i);

        const auto& adj_edges = uE2E[i];
        DCHECK(adj_edges.size() > 2);

        const size_t ei = adj_edges[0];
        size_t s,d;
        get_edge_end_points(ei, s, d);

        expand_forward(edge_curve, d, s);
        expand_backward(edge_curve, s, d);
        curves.emplace_back(edge_curve.begin(), edge_curve.end());

        for (auto iter = edge_curve.begin(); iter != edge_curve.end(); iter++)
        {
            visited[*iter] = true;
        }
    }
}
