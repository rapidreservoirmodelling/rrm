#define CATCH_CONFIG_RUNNER
#include "catch2/catch.hpp"

#include "glog/logging.h"

int main( int argc, char* argv[] ) 
/* { */
  /* // global setup... */

  /* int result = Catch::Session().run( argc, argv ); */

  /* // global clean-up... */

  /* return result; */
/* } */
{
    //
    // Global setup
    //

    // Initialize google::glog
    /* FLAGS_logtostderr = true; */
    /* FLAGS_colorlogtostderr = true; */
    FLAGS_log_dir = ".";
    google::InitGoogleLogging(std::string("tests_stratmod").c_str());
    google::InstallFailureSignalHandler();

    //
    // Initialize Catch2
    //

    int result = Catch::Session().run( argc, argv );

    //
    // Global clean-up
    //

    return result;
}
