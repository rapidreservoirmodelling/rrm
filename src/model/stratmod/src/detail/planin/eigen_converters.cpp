/* MIT License */

/* Copyright (c) 2022 Julio Daniel Machado Silva */

/* Permission is hereby granted, free of charge, to any person obtaining a copy */
/* of this software and associated documentation files (the "Software"), to deal */
/* in the Software without restriction, including without limitation the rights */
/* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell */
/* copies of the Software, and to permit persons to whom the Software is */
/* furnished to do so, subject to the following conditions: */

/* The above copyright notice and this permission notice shall be included in all */
/* copies or substantial portions of the Software. */

/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE */
/* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, */
/* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE */
/* SOFTWARE. */


#include "planin/eigen_converters.hpp"

namespace eigen_converters {

auto mapPoints3D(std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/3, 3))
{
    return mapVectorToEigen(points, points.size()/3, 3);
}

auto mapPoints2D(std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/2, 2))
{
    return mapVectorToEigen(points, points.size()/2, 2);
}

auto mapPoints3D(const std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/3, 3))
{
    return mapVectorToEigen(points, points.size()/3, 3);
}

auto mapPoints2D(const std::vector<double>& points) -> decltype(mapVectorToEigen(points, points.size()/2, 2))
{
    return mapVectorToEigen(points, points.size()/2, 2);
}

auto mapPoints3D(std::vector<int>& points) -> decltype(mapVectorToEigen(points, points.size()/3, 3))
{
    return mapVectorToEigen(points, points.size()/3, 3);
}

auto mapPoints2D(std::vector<int>& points) -> decltype(mapVectorToEigen(points, points.size()/2, 2))
{
    return mapVectorToEigen(points, points.size()/2, 2);
}

auto mapPoints3D(const std::vector<int>& points) -> decltype(mapVectorToEigen(points, points.size()/3, 3))
{
    return mapVectorToEigen(points, points.size()/3, 3);
}

auto mapPoints2D(const std::vector<int>& points) -> decltype(mapVectorToEigen(points, points.size()/2, 2))
{
    return mapVectorToEigen(points, points.size()/2, 2);
}

} // namespace eigen_converters
