#include <memory>

#include "cereal/types/memory.hpp"
#include "cereal/types/polymorphic.hpp"

#include "glog/logging.h"

#include "stratmod/surfaces/input_surface.hpp"
#include "stratmod/surfaces/triangle_surface.hpp"

#include "planin/isurface.hpp"

#include <Eigen/Dense>

#ifndef STRATMOD_DETAIL_ISURF_IMPL
#define STRATMOD_DETAIL_ISURF_IMPL

namespace stratmod {
    namespace detail {
        class ISurfaceImpl : public planin::ISurface {
            public:
                bool set(const std::shared_ptr<InputSurface>& isptr)
                {
                    if (isptr == nullptr)
                    {
                        return false;
                    }

                    isptr_ = isptr;
                    return true;
                }

                std::shared_ptr<InputSurface> getInputSurface()
                {
                    return isptr_;
                }

                virtual bool surfaceIsSet() override 
                {
                    if (isptr_ == nullptr)
                    {
                        return false;
                    }

                    return true;
                } 

                virtual bool getHeight( const Point2 &p, double &height ) override
                {
                    if (!surfaceIsSet())
                    {
                        return false;
                    }

                    height = isptr_->operator()({p[0], p[1]});

                    return true;
                }

                virtual bool isSmooth() override
                {
                    if (!surfaceIsSet())
                    {
                        return false;
                    }

                    return isptr_->differentiable();
                }

                virtual bool getNormal( const Point2 &p, Point3& normal ) override
                {
                    if (!surfaceIsSet())
                    {
                        return false;
                    }

                    auto odf = isptr_->derivative({p[0], p[1]});
                    if (odf.has_value())
                    {
                        Eigen::Vector3d n({-(*odf)[0], -(*odf)[1], 1.});
                        n.normalize();

                        normal[0] = n[0];
                        normal[1] = n[1];
                        normal[2] = n[2];

                        return true;
                    }

                    return false;
                }

                virtual bool isTriangleSurface() override
                {
                    if (auto ptr = std::dynamic_pointer_cast<stratmod::TriangleSurface>(isptr_))
                    {
                        return true;
                    }

                    return false;
                }

                virtual Eigen::MatrixXd getISurfaceVertices() override
                {
                    if (auto ptr = std::dynamic_pointer_cast<stratmod::TriangleSurface>(isptr_))
                    {
                        return ptr->getInputVertices();
                    }

                    return Eigen::MatrixXd();
                }

                virtual Eigen::MatrixXi getISurfaceTriangles() override
                {
                    if (auto ptr = std::dynamic_pointer_cast<stratmod::TriangleSurface>(isptr_))
                    {
                        return ptr->getInputTriangles();
                    }

                    return Eigen::MatrixXi();
                }

                virtual Eigen::MatrixXi getISurfaceBoundary() override
                {
                    if (auto ptr = std::dynamic_pointer_cast<stratmod::TriangleSurface>(isptr_))
                    {
                        return ptr->getBoundary();
                    }

                    return Eigen::MatrixXi();
                }

            private:
                std::shared_ptr<stratmod::InputSurface> isptr_;

                // Cereal provides an easy way to serialize objects
                friend class cereal::access;

                /* template<typename Archive> */
                /* void save(Archive& ar, const std::uint32_t /1* version *1/) const */
                /* { */
                /*     LOG(INFO) << "Call to serialize method in stratmod::detail::ISurfaceImpl"; */
                /*     /1* ar(cereal::base_class<planin::ISurface>(this)); *1/ */

                /*     ar(isptr_); */
                /* } */

                template<typename Archive>
                void serialize(Archive& ar, const std::uint32_t /* version */)
                {
                    ar(cereal::base_class<planin::ISurface>(this));

                    /* LOG(INFO) << "Call to serialize method in stratmod::detail::ISurfaceImpl"; */
                    ar(isptr_);
                }
        };
    } // namespace stratmod
} // namespace detail

CEREAL_CLASS_VERSION(stratmod::detail::ISurfaceImpl, 0);

CEREAL_REGISTER_TYPE(stratmod::detail::ISurfaceImpl);
CEREAL_REGISTER_POLYMORPHIC_RELATION(planin::ISurface, stratmod::detail::ISurfaceImpl);

#endif
