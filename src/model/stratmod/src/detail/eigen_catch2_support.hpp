#include <catch2/catch.hpp>
#include <Eigen/Dense>
#include <eigen3/Eigen/Dense>

template <typename DerivedA, typename DerivedB>
void inline require_eq(
        const Eigen::MatrixBase<DerivedA> & A,
        const Eigen::MatrixBase<DerivedB> & B, 
        double tolerance = 0.)
{
    CAPTURE(A);
    CAPTURE(B);

    // Sizes should match
    REQUIRE(A.rows() == B.rows());
    REQUIRE(A.cols() == B.cols());
    for(int i = 0; i < A.rows(); i++)
    {
        for(int j = 0; j < A.cols(); j++)
        {
            // Create an ijv tuple to trick catch2 into printing (i,j) so we
            // know where the disagreement is.
            std::vector<typename DerivedA::Scalar> Aijv {
                static_cast<typename DerivedA::Scalar>(i),
                static_cast<typename DerivedA::Scalar>(j),
                A(i,j)};
            std::vector<typename DerivedB::Scalar> Bijv {
                static_cast<typename DerivedB::Scalar>(i),
                static_cast<typename DerivedB::Scalar>(j),
                B(i,j)};
            REQUIRE_THAT(Aijv, Catch::Approx(Bijv).margin(tolerance));
        }
    }
}

namespace Eigen {
    template<typename Derived>
    bool operator<(const MatrixBase<Derived>& lhs, const MatrixBase<Derived>& rhs)
    {
        if (lhs.rows() != rhs.rows())
        {
            return false;
        }

        if (lhs.cols() != rhs.cols())
        {
            return false;
        }

        for (int i = 0; i < lhs.rows(); ++i)
            for (int j = 0; j < lhs.cols(); ++j)
            {
                if (lhs(i,j) >= rhs(i,j))
                {
                    return false;
                }
            }

        return true;
    }
}
