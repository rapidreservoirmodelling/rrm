
#include <iostream>
#include <chrono>
#include <vector>
#include <map>
#include <memory>

#include "planin/include/planin/sdesc.hpp"
#include "smodeller_impl.hpp"

#include "stratmod/misc/smodeller_primitives.hpp"
#include "structured_triangle_mesh_2d.hpp"
#include "testing_definitions.hpp"
#include "sdesc_impl.hpp"

#include "planin/planin.hpp"
#include "planin/legacy_planar_surface.hpp"
#include "planin/mesh/mesh_builder.hpp"
#include "planin/mesh/meshing_helper.hpp"


namespace stratmod {

bool SModellerImplementation::init()
{
    if ( got_origin_ == false ) { 
        origin_ = {{{ 0., 0., 0. }}};
    }

    if ( got_lenght_ == false ) { 
        lenght_ = {{{ 1., 1., 1. }}};
    }

    current_.state_ = State::RAI_SKETCHING;

    /* if ( max_discretization_level_ == 0 ) { */ 
    /*     setMaxDiscretizationLevel(7); */ 
    /*     requestChangeDiscretizationLevel( getMaxDiscretizationLevel(), getMaxDiscretizationLevel() ); */ 
    /* } */

    discWidth_ = 64;
    discLenght_ = 64;

    PlaninASurface::requestChangeDiscretization(
            static_cast<PlaninASurface::Natural>( discWidth_ ), 
            static_cast<PlaninASurface::Natural>( discLenght_ ) );

    mesh_ = nullptr;

    initialized_ = true; 

    return true;
}

void SModellerImplementation::clear()
{
    /* size_t discWidth_ = 64, discLenght_ = 64; */ 
    /* size_t numI_, numJ_; */ 
    /* size_t max_discretization_level_; */ 
    /* size_t level_I_, level_J_; */

    /* bool default_coordinate_system_ = true; */

    container_.clear(); 
    dictionary_.clear(); 
    inserted_surfaces_indices_.clear();

    current_ = StateDescriptor(); 

    undoed_surfaces_stack_.clear();
    undoed_surfaces_indices_.clear();
    undoed_states_.clear(); 
    past_states_.clear();

    got_origin_ = false; 
    got_lenght_ = false; 

    mesh_ = nullptr;

    interpretations_.clear();
    model_id_ = RandomId::Get();

    x_scale_ = 1.;
    y_scale_ = 1.;
    z_scale_ = 1.;

    saved_model_filename_ = std::make_optional<std::string>();

    clearCSCache();

    init();
}

Point3 SModellerImplementation::point3( double x, double y, double z )
{
    Point3 p;

    if ( default_coordinate_system_ )
    {
        p = {{{ x, y, z }}}; 
    }
    else
    {
        p = {{{ x, z, y }}}; 
    }

    return p;
}

void SModellerImplementation::getOrigin( double &x, double &y, double &z )
{
    if ( default_coordinate_system_ )
    {
        x = origin_.x;
        y = origin_.y;
        z = origin_.z;
    }
    else
    {
        x = origin_.x;
        y = origin_.z;
        z = origin_.y;
    }
}

void SModellerImplementation::getLenght( double &x, double &y, double &z )
{
    if ( default_coordinate_system_ )
    {
        x = lenght_.x;
        y = lenght_.y;
        z = lenght_.z;
    }
    else
    {
        x = lenght_.x;
        y = lenght_.z;
        z = lenght_.y;
    }
}

std::vector<size_t> SModellerImplementation::getSurfacesIndicesBelowPoint( double x, double y, double z )
{
    std::vector<size_t> surfaces_ids = {};
    Point3 p = point3(x, y, z);

    surfaces_ids = container_.getSurfacesBelowPoint(p);
    ControllerSurfaceIndex cid;
    /* std::cout << "getSurfacesIndicesBelowPoint\n"; */
    for ( auto &i : surfaces_ids )
    {
        getControllerIndex(i, cid);
        i = cid;
    }

    return surfaces_ids;
}

std::vector<size_t> SModellerImplementation::getSurfacesIndicesAbovePoint( double x, double y, double z )
{
    std::vector<size_t> surfaces_ids = {};
    Point3 p = point3(x, y, z);

    surfaces_ids = container_.getSurfacesAbovePoint(p);
    ControllerSurfaceIndex cid;
    /* std::cout << "getSurfacesIndicesAbovePoint\n"; */
    for ( size_t i = 0; i < surfaces_ids.size(); ++i )
    {
        getControllerIndex(surfaces_ids[i], cid);
        surfaces_ids[i] = cid;
    }

    return surfaces_ids;
}

std::vector<std::size_t> SModellerImplementation::getOrderedSurfacesIndices()
{
    std::vector<size_t> surfaces_ids = inserted_surfaces_indices_;

    if ( (surfaces_ids.size() <= 1) || (buildTetrahedralMesh() == false) )
    {
        return surfaces_ids;
    }

    mesh_->getOrderedSurfaceIndicesList(surfaces_ids);

    ControllerSurfaceIndex cid;
    for ( size_t i = 0; i < surfaces_ids.size(); ++i )
    {
        getControllerIndex(surfaces_ids[i], cid);
        surfaces_ids[i] = cid;
    }

    return surfaces_ids;
}

bool SModellerImplementation::getBoundingSurfacesFromRegionID( std::size_t region_id, std::vector<size_t> &lower_bound_surfaces, std::vector<size_t> &upper_bound_surfaces)
{
    /* TetrahedralMeshBuilder mb(container_); */
    if ( buildTetrahedralMesh() == false )
    {
        return false;
    }
    
    bool success = mesh_->mapRegionToBoundingSurfaces(region_id, lower_bound_surfaces, upper_bound_surfaces);
    if ( !success )
    {
        return false;
    }

    ControllerSurfaceIndex cid;
    for ( size_t i = 0; i < lower_bound_surfaces.size(); ++i )
    {
        getControllerIndex(lower_bound_surfaces[i], cid);
        lower_bound_surfaces[i] = cid;
    }

    for ( size_t i = 0; i < upper_bound_surfaces.size(); ++i )
    {
        getControllerIndex(upper_bound_surfaces[i], cid);
        upper_bound_surfaces[i] = cid;
    }

    return true;
}


#include <iostream>

bool SModellerImplementation::lastInsertedSurfaceIntersects( std::vector<ControllerSurfaceIndex> &intersected_surfaces )
{
    std::vector<ContainerSurfaceIndex> ids = {};
    bool status = container_.lastInsertedSurfaceIntersects(ids);

    if ( status == false )
    {
        return false;
    }

    intersected_surfaces.resize( ids.size() );
    ControllerSurfaceIndex cindex;

    for ( size_t i = 0; i < ids.size(); ++i )
    {
        status &= getControllerIndex(ids[i], cindex);
        intersected_surfaces[i] = cindex;
    }

    return status;
}

bool SModellerImplementation::getControllerIndex( const ContainerSurfaceIndex surface_id, ControllerSurfaceIndex &controller_id )
{
    for ( auto iter = dictionary_.begin(); iter != dictionary_.end(); ++iter )
    {
        if ( iter->second == surface_id )
        {
            controller_id = iter->first;
            /* std::cout << "Surf id: " << surface_id << " corresponds to cont id: " << controller_id << std::endl; */
            return true;
        }
    }

    return false;
}

bool SModellerImplementation::getSurfaceIndex( const ControllerSurfaceIndex controller_index, ContainerSurfaceIndex &index ) const
{
    auto iter = dictionary_.find(controller_index);  

    if ( iter == dictionary_.end() ) { 
        WARN( "getSurfaceIndex(...) could not find index in dictionary" ); 
        return false; 
    }
    else { 
        index = iter->second;
    }

    return true;
}

bool SModellerImplementation::getPlanarSurfaceId( const ControllerSurfaceIndex controller_index, PlaninASurface::SurfaceId &surface_id )
{
    size_t index; 

    if ( getSurfaceIndex(controller_index, index) == false ) {
        return false; 
    }

    /* PlaninASurface::Ptr sptr(container_[index]); */
    surface_id = container_[index]->getID();

    return false; 
}

bool SModellerImplementation::getControllerIndexFromPlanarSurfaceId( const PlaninASurface::SurfaceId surface_id, ControllerSurfaceIndex &controller_index )
{
    size_t index;
    bool success = container_.getSurfaceIndex(surface_id, index);

    success &= getControllerIndex(index, controller_index);
    if ( !success )
    {
        controller_index = std::numeric_limits<ControllerSurfaceIndex>::max();
    }

    return success;
}

bool SModellerImplementation::parseTruncateSurfaces( std::vector<size_t> &lbounds, std::vector<size_t> &ubounds )
{
    bool status = true; 
    /* std::vector<size_t> lbounds, ubounds; */

    auto & lbound_indices = current_.truncate_lower_boundary_;
    auto & ubound_indices = current_.truncate_upper_boundary_; 

    for ( auto &i : lbound_indices ) 
    {
        size_t j;
        status &= getSurfaceIndex(i, j);

        if ( status ) { 
            lbounds.push_back(j); 
        }
    }

    for ( auto &i : ubound_indices )
    {
        size_t j;
        status &= getSurfaceIndex(i, j);

        if ( status ) { 
            ubounds.push_back(j); 
        }
    }

    /* if ( status == false ) */
    /* { */
    /*     return false; */ 
    /* } */

    return status;
}

bool SModellerImplementation::isInitialized()
{
    return initialized_; 
}

bool SModellerImplementation::insertSurface( const std::vector<double> &/* point_data */, size_t /* surface_id */, 
        const std::vector<size_t> /* lower_bound_ids */, const std::vector<size_t> /* upper_bound_ids */, 
        bool /* extruded_surface */, bool /* orthogonally_oriented */, double /* fill_distance */ )
{
    /* INFO( "Got into pimpl_->insertSurface(...)" ); */

    /* if ( isInitialized() == false ) { */ 
    /*     WARN( "Modeller not initialized" ); */
    /*     return false; */ 
    /* } */

    /* if ( point_data.size() % 3 != 0 ) { */
    /*     WARN( "point_data.size() is not a multiple of 3" ); */
    /*     return false; */
    /* } */

    /* current_.truncate_lower_boundary_ = lower_bound_ids; */ 
    /* current_.truncate_upper_boundary_ = upper_bound_ids; */ 

    /* std::vector<size_t> lbounds, ubounds; */
    /* bool status = parseTruncateSurfaces(lbounds, ubounds); */ 
    /* if ( status == false ) */
    /* { */
    /*     WARN( "Failed when parsing the truncating surfaces" ); */
    /*     return false; */ 
    /* } */

    /* /1* Create a surface *1/ */
    /* PlaninASurface::Ptr sptr = std::make_shared<PlaninASurface>(extruded_surface, orthogonally_oriented); */

    /* sptr->setOrigin(origin_); */ 
    /* sptr->setLenght(lenght_); */ 

    /* const size_t dim = 3; */
    /* for ( size_t i = 0; i < point_data.size(); i += dim ) { */ 
    /*     /1* if ( default_coordinate_system_ ) *1/ */
    /*     /1* { *1/ */
    /*     /1*     sptr->addPoint( {{{ point_data[i+0], point_data[i+1], point_data[i+2] }}} ); *1/ */ 
    /*     /1* } *1/ */
    /*     /1* else *1/ */
    /*     /1* { *1/ */
    /*     /1*     sptr->addPoint( {{{ point_data[i+0], point_data[i+2], point_data[i+1] }}} ); *1/ */ 
    /*     /1* } *1/ */

    /*     sptr->addPoint( point3(point_data[i+0], point_data[i+1], point_data[i+2]) ); */
    /* } */

    /* if ( fill_distance < 0 ) */
    /* { */
    /*     sptr->setMeshFillDistance(); */
    /* } */
    /* else */
    /* { */
    /*     sptr->setFillDistance(fill_distance); */
    /* } */
    /* sptr->generateSurface(); */ 

    /* /1* Execute selected Geologic Rule *1/ */
    /* status = commitSurface(sptr, surface_id, lbounds, ubounds); */ 

    /* if ( ( status == true ) && ( undoed_surfaces_stack_.empty() == false ) ) */
    /* { */
    /*     undoed_surfaces_stack_.clear(); */
    /*     undoed_surfaces_indices_.clear(); */
    /*     undoed_states_.clear(); */ 
    /* } */

    /* return status; */

    return false;
}

bool SModellerImplementation::addStratigraphicSurface(int surface_id, const std::shared_ptr<stratmod::InputSurface>& input_sptr)
{

    if ( isInitialized() == false ) { 
        return false; 
    }

    current_.truncate_lower_boundary_ = {}; 
    current_.truncate_upper_boundary_ = {}; 

    /* std::vector<size_t> lbounds, ubounds; */
    /* bool status = parseTruncateSurfaces(lbounds, ubounds); */ 
    /* if ( status == false ) */
    /* { */
        /* WARN( "Failed when parsing the truncating surfaces" ); */
        /* return false; */ 
    /* } */

    /* Create a surface */
    std::shared_ptr<detail::ISurfaceImpl> isptr = std::make_shared<detail::ISurfaceImpl>();
    isptr->set(input_sptr);

    PlaninASurface::Ptr sptr = std::make_shared<PlaninASurface>();
    sptr->setOrigin(origin_); 
    sptr->setLenght(lenght_); 

    sptr->updateISurface(isptr);

    /* const size_t dim = 3; */
    /* for ( size_t i = 0; i < point_data.size(); i += dim ) { */ 
    /*     /1* if ( default_coordinate_system_ ) *1/ */
    /*     /1* { *1/ */
    /*     /1*     sptr->addPoint( {{{ point_data[i+0], point_data[i+1], point_data[i+2] }}} ); *1/ */ 
    /*     /1* } *1/ */
    /*     /1* else *1/ */
    /*     /1* { *1/ */
    /*     /1*     sptr->addPoint( {{{ point_data[i+0], point_data[i+2], point_data[i+1] }}} ); *1/ */ 
    /*     /1* } *1/ */

    /*     sptr->addPoint( point3(point_data[i+0], point_data[i+1], point_data[i+2]) ); */
    /* } */

    /* if ( fill_distance < 0 ) */
    /* { */
    /*     sptr->setMeshFillDistance(); */
    /* } */
    /* else */
    /* { */
    /*     sptr->setFillDistance(fill_distance); */
    /* } */
    /* sptr->generateSurface(); */ 

    /* Execute selected Geologic Rule */
    bool status = commitSurface(sptr, surface_id, SurfaceType::Stratigraphy); 
    current_.surface_type_ = SurfaceType::Stratigraphy;

    if ( ( status == true ) && ( undoed_surfaces_stack_.empty() == false ) )
    {
        undoed_surfaces_stack_.clear();
        undoed_surfaces_indices_.clear();
        undoed_states_.clear(); 
    }

    return status;

}

bool SModellerImplementation::addFaultSurface(int surface_id, const std::shared_ptr<stratmod::InputSurface>& input_sptr)
{
    if ( isInitialized() == false ) { 
        return false; 
    }

    current_.truncate_lower_boundary_ = {}; 
    current_.truncate_upper_boundary_ = {}; 

    /* Create a surface */
    std::shared_ptr<detail::ISurfaceImpl> isptr = std::make_shared<detail::ISurfaceImpl>();
    isptr->set(input_sptr);

    PlaninASurface::Ptr sptr = std::make_shared<PlaninASurface>();
    sptr->updateISurface(isptr);

    sptr->setOrigin(origin_); 
    sptr->setLenght(lenght_); 

    /* Execute selected Geologic Rule */
    bool status = commitSurface(sptr, surface_id, SurfaceType::Fault); 
    current_.surface_type_ = SurfaceType::Fault;

    if ( ( status == true ) && ( undoed_surfaces_stack_.empty() == false ) )
    {
        undoed_surfaces_stack_.clear();
        undoed_surfaces_indices_.clear();
        undoed_states_.clear(); 
    }

    return status;
}

bool SModellerImplementation::markAsStratigraphy(SurfaceUniqueIdentifier unique_id)
{
    size_t srules_id{};
    bool success = getSurfaceIndex(unique_id, srules_id);
    if (!success)
    {
        return false;
    }

    return container_.markAsStratigraphicSurface(srules_id);
}

bool SModellerImplementation::markAsFault(SurfaceUniqueIdentifier unique_id)
{
    size_t srules_id{};
    bool success = getSurfaceIndex(unique_id, srules_id);
    if (!success)
    {
        return false;
    }

    return container_.markAsStructuralSurface(srules_id);
}

bool SModellerImplementation::setSurfaceDescriptor(const SurfaceDescriptor& descriptor)
{
    size_t srules_id{};
    bool success = getSurfaceIndex(descriptor.unique_id_, srules_id);
    if (!success)
    {
        return false;
    }

    success = descriptor.groupId() && container_.setPhysicalSurfaceId(srules_id, descriptor.group_id_);
    /* success = container_.setPhysicalSurfaceId(srules_id, descriptor.group_id_); */
    if (success)
    {
        std::shared_ptr<detail::SDescImpl> sdptr = std::make_shared<detail::SDescImpl>();
        /* std::cout << "before setting surface: " << input_sptr << "\n" << std::flush; */
        DCHECK_NOTNULL(sdptr)->set(std::make_shared<SurfaceDescriptor>(descriptor));
        container_[srules_id]->updateSDesc(sdptr);
    }
    else
    {
        container_.clearPhysicalSurfaceId(srules_id);
        container_[srules_id]->updateSDesc(nullptr);
    }

    return true;
}

std::optional<SurfaceDescriptor> SModellerImplementation::getSurfaceDescriptor(SurfaceUniqueIdentifier suid)
{
    size_t srules_id{};
    bool success = getSurfaceIndex(suid, srules_id);
    if (!success)
    {
        return std::optional<SurfaceDescriptor>();
    }

    SurfaceDescriptor descriptor(suid);
    if (!container_.getPhysicalSurfaceId(srules_id, descriptor.group_id_))
    {
        return std::optional<SurfaceDescriptor>();
    }

    std::shared_ptr<planin::SDesc> sdptr = container_[srules_id]->getSDesc();
    if (sdptr)
    {
        descriptor = *dynamic_cast<detail::SDescImpl&>(*DCHECK_NOTNULL(sdptr)).get();
    }

    return std::make_optional<SurfaceDescriptor>(descriptor);
}

std::vector<SurfaceDescriptor> SModellerImplementation::getSurfaceDescriptors(SurfaceGroupIdentifier gid)
{
    std::vector<SurfaceDescriptor> descriptors;
    auto surfaces_ids = getSurfacesIndices();
    for (auto suid : surfaces_ids)
    {
        auto descriptor = getSurfaceDescriptor(suid);
        /* size_t srules_id{}; */
        /* getSurfaceIndex(suid, srules_id); */
        /* if (descriptor && container_.getPhysicalSurfaceId(srules_id, (*descriptor).group_id_)) */
        if(descriptor)
        {
            if ((*descriptor).group_id_ == gid)
            {
                descriptors.push_back(*descriptor);
            }
        }
    }

    return descriptors;
}

std::shared_ptr<stratmod::InputSurface> SModellerImplementation::getInputSurface(int controller_id)
{
    std::shared_ptr<stratmod::InputSurface> isptr;

    std::size_t surface_id;
    if (!getSurfaceIndex(controller_id, surface_id))
    {
        return isptr;
    }

    isptr = dynamic_cast<detail::ISurfaceImpl&>(*DCHECK_NOTNULL(container_[surface_id]->getISurface())).getInputSurface();

    return isptr;
}

bool SModellerImplementation::updateSurface(int controller_id)
{
    /* std::size_t surface_id; */
    /* if (!getSurfaceIndex(controller_id, surface_id)) */
    /* { */
    /*     return false; */
    /* } */

    /* auto sptr = container_[surface_id]; */
    /* DCHECK_NOTNULL(sptr)->updateRawCache(); */
    /* DCHECK_NOTNULL(sptr)->updateCache(); */

    /* return true; */

    return updateSurface(controller_id, getInputSurface(controller_id));
}

bool SModellerImplementation::updateSurface(int controller_id, const std::shared_ptr<stratmod::InputSurface>& input_sptr)
{
    std::size_t surface_id;
    if (!getSurfaceIndex(controller_id, surface_id))
    {
        return false;
    }

    std::shared_ptr<detail::ISurfaceImpl> isptr = std::make_shared<detail::ISurfaceImpl>();
    /* std::cout << "before setting surface: " << input_sptr << "\n" << std::flush; */
    DCHECK_NOTNULL(isptr)->set(input_sptr);

    /* auto sptr = container_[surface_id]; */
    /* DCHECK_NOTNULL(sptr)->updateISurface(isptr); */

    /* std::cout << "before updating surface in container\n" << std::flush; */
    container_.updateSurface(isptr, surface_id);

    /* std::cout << "before refreshing model\n" << std::flush; */
    refreshModel();

    return true;
}

bool SModellerImplementation::insertExtrusionAlongPath( size_t /* surface_id */, 
        const std::vector<double> & /* cross_section_curve */, double /* cross_section_depth */,
        const std::vector<double> & /* path_curve */,
        const std::vector<size_t> /* lower_bound_ids */, const std::vector<size_t> /* upper_bound_ids */, 
        bool /* orthogonally_oriented */, 
        double /* fill_distance */ )
{
    /* INFO( "Got into pimpl_->insertSurface(...)" ); */

    /* if ( isInitialized() == false ) { */ 
    /*     WARN( "Modeller not initialized" ); */
    /*     return false; */ 
    /* } */

    /* size_t dim = 2; */

    /* if ( cross_section_curve.size() % dim != 0 ) { */
    /*     WARN( "cross_section_curve.size() is not a multiple of 2" ); */
    /*     return false; */
    /* } */

    /* if ( path_curve.size() % dim != 0 ) { */
    /*     WARN( "path_curve.size() is not a multiple of 2" ); */
    /*     return false; */
    /* } */

    /* current_.truncate_lower_boundary_ = lower_bound_ids; */ 
    /* current_.truncate_upper_boundary_ = upper_bound_ids; */ 

    /* std::vector<size_t> lbounds, ubounds; */
    /* bool status = parseTruncateSurfaces(lbounds, ubounds); */ 
    /* if ( status == false ) */
    /* { */
    /*     WARN( "Failed when parsing the truncating surfaces" ); */
    /*     return false; */ 
    /* } */

    /* /1* Create a surface *1/ */
    /* bool extruded_surface = true; */
    /* PlaninASurface::Ptr sptr = std::make_shared<PlaninASurface>(extruded_surface, orthogonally_oriented); */

    /* sptr->setOrigin(origin_); */ 
    /* sptr->setLenght(lenght_); */ 

    /* for ( size_t i = 0; i < cross_section_curve.size(); i += dim ) { */ 
    /*     /1* if ( default_coordinate_system_ ) *1/ */
    /*     /1* { *1/ */
    /*     /1*     sptr->addPoint( {{{ cross_section_curve[i+0], cross_section_curve[i+1], cross_section_curve[i+2] }}} ); *1/ */ 
    /*     /1* } *1/ */
    /*     /1* else *1/ */
    /*     /1* { *1/ */
    /*     /1*     sptr->addPoint( {{{ cross_section_curve[i+0], cross_section_curve[i+2], cross_section_curve[i+1] }}} ); *1/ */ 
    /*     /1* } *1/ */

    /*     sptr->addPoint( Point3{ {{cross_section_curve[i+0], cross_section_depth, cross_section_curve[i+1]}} } ); */
    /* } */

    /* for ( size_t i = 0; i < path_curve.size(); i += dim ) */
    /* { */
    /*     sptr->addExtrusionPathPoint( path_curve[i+1], path_curve[i+0] ); */
    /* } */
    /* sptr->setPathOrigin( cross_section_depth, 0. ); */

    /* if ( fill_distance < 0 ) */
    /* { */
    /*     sptr->setMeshFillDistance(); */
    /* } */
    /* else */
    /* { */
    /*     sptr->setFillDistance(fill_distance); */
    /* } */
    /* sptr->generateSurface(); */ 

    /* /1* Execute selected Geologic Rule *1/ */
    /* status = commitSurface(sptr, surface_id, lbounds, ubounds); */ 

    /* if ( ( status == true ) && ( undoed_surfaces_stack_.empty() == false ) ) */
    /* { */
    /*     undoed_surfaces_stack_.clear(); */
    /*     undoed_surfaces_indices_.clear(); */
    /*     undoed_states_.clear(); */ 
    /* } */

    /* return status; */

    return false;
} 

bool SModellerImplementation::commitSurface( 
        PlaninASurface::Ptr &sptr, 
        size_t given_index, 
        SurfaceType type,
        std::vector<size_t> /* lbounds */, 
        std::vector<size_t> /* ubounds */
        )
{
    size_t index;
    bool status; 

    auto iter = dictionary_.find(given_index);
    if ( iter != dictionary_.end() )
    {
        std::cout << "Tried to insert surface with duplicated index.\n";
        return false;
    }

    if ( current_.state_ == State::SKETCHING )
    {
        // Calling SRules::addSurface() directly
        status = container_.addSurface(sptr, index);
    }
    else
    {
        if (type == SurfaceType::Stratigraphy)
        {
            status = container_.addStratigraphicSurface(sptr, index); 
        }
        else if (type == SurfaceType::Fault)
        {
            status = container_.addStructuralSurface(sptr, index); 
        }
    }

    switch ( current_.state_ ) 
    { 
        case State::SKETCHING: 
            break; 

        case State::RA_SKETCHING: 
            if ( status == true )
                status = container_.removeAbove();
            break; 

        case State::RAI_SKETCHING:
            if ( status == true )
                status = container_.removeAboveIntersection(); 
            break;

        case State::RB_SKETCHING:
            if ( status == true )
                status = container_.removeBelow(); 
            break;

        case State::RBI_SKETCHING:
            if ( status == true )
                status = container_.removeBelowIntersection(); 
            break;

        default:
            status = false; 
            break;
    }

    if ( status == true ) { 
        dictionary_[given_index] = index;
        /* container_.setPhysicalSurfaceId(index, given_index); */
        inserted_surfaces_indices_.push_back(given_index); 
        /* std::cout << "Surface " << given_index << " was commited.\n"; */

        past_states_.push_back(current_);

        mesh_ = nullptr;
    }
	else
	{
		container_.popLastSurface(sptr);
	}

    INFO( "Inside commitSurface(...) " ); 
    REQUIRE( status == true );
    return status;
}

bool SModellerImplementation::popLastSurface()
{
    if ( container_.empty() )
    {
        return false; 
    }

    PlaninASurface::Ptr last_sptr; 
    container_.popLastSurface(last_sptr);

    size_t last_surface_index = inserted_surfaces_indices_.back(); 
    inserted_surfaces_indices_.pop_back(); 

    StateDescriptor last = past_states_.back();
    past_states_.pop_back();

    // current_.bounded_above_ = last.bounded_above_;
    // // current_.upper_boundary_list_ = last.upper_boundary_list_;
    // current_.bounded_below_ = last.bounded_below_;
    // // current_.lower_boundary_list_ = last.lower_boundary_list_;
    current_ = last;
    enforceDefineRegion();

    /* pimpl_->undoed_surfaces_stack_.push_back(last_sptr); */ 
    /* pimpl_->undoed_surfaces_indices_.push_back(last_surface_index); */ 

    /* pimpl_->undoed_states_.push_back(last); */

    auto iter = dictionary_.find(last_surface_index); 
    dictionary_.erase(iter); 

    return true;
}

bool SModellerImplementation::canRedo()
{
    if ( undoed_surfaces_stack_.size() > 0 )
    {
        return true;
    }

    return false;
}

bool SModellerImplementation::redo()
{
    if ( canRedo() == false )
    {
        return false;
    }

    PlaninASurface::Ptr undoed_sptr = undoed_surfaces_stack_.back(); 
    undoed_surfaces_stack_.pop_back(); 

    size_t surface_index = undoed_surfaces_indices_.back();
    undoed_surfaces_indices_.pop_back();

    /* StateDescriptor state_before_redo_ = current_; */
    current_ = undoed_states_.back();
    undoed_states_.pop_back();
    enforceDefineRegion();

    std::vector<size_t> lbounds, ubounds;
    bool status = parseTruncateSurfaces(lbounds, ubounds); 

    /* container_.updateCache(); */

    if ( status == true )
    {
        // BUG: Add SurfaceType to state descriptor
        status = commitSurface(undoed_sptr, surface_index, current_.surface_type_, lbounds, ubounds); 
    }


    /* bool status = commitSurface(undoed_sptr, surface_index, std::vector<size_t>(), std::vector<size_t>()); */

    /* current_ = state_before_redo_; */
    /* enforceDefineRegion(); */

    return status;
}

bool SModellerImplementation::canUndo()
{
    if ( container_.size() > 0 )
    {
        return true;
    }

    return false;
}

bool SModellerImplementation::undo()
{
    if ( canUndo() == false )
    {
        return false; 
    }

    PlaninASurface::Ptr last_sptr; 
    container_.popLastSurface(last_sptr);

    size_t last_surface_index = inserted_surfaces_indices_.back(); 
    inserted_surfaces_indices_.pop_back(); 

    StateDescriptor last = past_states_.back();
    past_states_.pop_back();

    current_.bounded_above_ = last.bounded_above_;
    current_.upper_boundary_list_ = last.upper_boundary_list_;
    current_.bounded_below_ = last.bounded_below_;
    current_.lower_boundary_list_ = last.lower_boundary_list_;
    // current_ = last; 
    enforceDefineRegion();

    undoed_surfaces_stack_.push_back(last_sptr); 
    undoed_surfaces_indices_.push_back(last_surface_index); 

    undoed_states_.push_back(last);

    auto iter = dictionary_.find(last_surface_index); 
    dictionary_.erase(iter); 

    // Cache was updated in call to SRules::popLastSurface()
    /* container_.updateCache(); */

    clearCSCache(last_surface_index);

    mesh_ = nullptr;

    return true;
}

bool SModellerImplementation::scaleModel(double scaleX, double scaleY, double scaleZ)
{
    if ((scaleX < 1E-3) || (scaleY < 1E-3) || (scaleZ < 1E-3))
    {
        return false;
    }

    x_scale_ = scaleX / x_scale_;
    y_scale_ = scaleY / y_scale_;
    z_scale_ = scaleZ / z_scale_;

    origin_.x *= x_scale_;
    lenght_.x *= x_scale_;

    origin_.y *= y_scale_;
    lenght_.y *= y_scale_;

    origin_.z *= z_scale_;
    lenght_.z *= z_scale_;

    for (std::size_t i = 0; i < container_.size(); ++i)
    {
        auto& sptr = container_[i];
        sptr->setOrigin(origin_); 
        sptr->setLenght(lenght_); 
        std::shared_ptr<InputSurface> iptr = dynamic_cast<detail::ISurfaceImpl&>(*DCHECK_NOTNULL(sptr->getISurface())).getInputSurface();
        iptr->scale(Eigen::Vector3d(scaleX, scaleY, scaleZ));
    }

    for (std::size_t i = 0; i < undoed_surfaces_stack_.size(); ++i)
    {
        auto& sptr = undoed_surfaces_stack_[i];
        sptr->setOrigin(origin_); 
        sptr->setLenght(lenght_); 
        std::shared_ptr<InputSurface> iptr = dynamic_cast<detail::ISurfaceImpl&>(*DCHECK_NOTNULL(sptr->getISurface())).getInputSurface();
        iptr->scale(Eigen::Vector3d(scaleX, scaleY, scaleZ));
    }

    refreshModel();

    return true;
}

bool SModellerImplementation::scaleModelHeight(double scale)
{
    if (scale < 1E-3)
    {
        return false;
    }

    z_scale_ = scale / z_scale_;

    origin_.z *= scale;
    lenght_.z *= scale;

    for (std::size_t i = 0; i < container_.size(); ++i)
    {
        auto& sptr = container_[i];
        sptr->setOrigin(origin_); 
        sptr->setLenght(lenght_); 
        std::shared_ptr<InputSurface> iptr = dynamic_cast<detail::ISurfaceImpl&>(*DCHECK_NOTNULL(sptr->getISurface())).getInputSurface();
        iptr->scaleZ(scale);
    }

    for (std::size_t i = 0; i < undoed_surfaces_stack_.size(); ++i)
    {
        auto& sptr = undoed_surfaces_stack_[i];
        sptr->setOrigin(origin_); 
        sptr->setLenght(lenght_); 
        std::shared_ptr<InputSurface> iptr = dynamic_cast<detail::ISurfaceImpl&>(*DCHECK_NOTNULL(sptr->getISurface())).getInputSurface();
        iptr->scaleZ(scale);
    }

    refreshModel();

    return true;
}

void SModellerImplementation::refreshModel()
{
    int counter = 0;
    while ( canUndo() )
    {
        undo();
        ++counter;
    }

    while ( counter > 0 )
    {
        redo();
        --counter;
    }

    container_.updateCache();
    clearCSCache();
}

bool SModellerImplementation::popUndoStack()
{
    if ( canRedo() == false )
    {
        return false;
    }

    undoed_surfaces_stack_.pop_back(); 
    undoed_surfaces_indices_.pop_back();
    undoed_states_.pop_back();

    return true;
}

void SModellerImplementation::clearUndoStack()
{
    while (popUndoStack());
}

bool SModellerImplementation::preserveAbove( std::vector<ContainerSurfaceIndex> bounding_surfaces_list )
{
    ContainerSurfaceIndex index;
    std::vector<ContainerSurfaceIndex> surface_ids;
    /* stopPreserveAbove(); */

    std::sort(bounding_surfaces_list.begin(), bounding_surfaces_list.end());
    std::vector<ControllerSurfaceIndex> intersection_list;

    if (current_.bounded_above_)
    {
        std::set_intersection(
                bounding_surfaces_list.begin(), bounding_surfaces_list.end(),
                current_.upper_boundary_list_.begin(), current_.upper_boundary_list_.end(),
                std::back_inserter(intersection_list));

        if (!intersection_list.empty())
        {
            DLOG(WARNING) << "Trying to apply PA and PB to the same set of surfaces";
            return false;
        }
    }

    for ( auto &surface_index : bounding_surfaces_list )
    {
        if ( getSurfaceIndex(surface_index, index) == false )
        {
            return false;
        }
        surface_ids.push_back(index);
    }
    stopPreserveAbove();

    current_.bounded_below_ = container_.defineAbove(surface_ids);
    if ( current_.bounded_below_ )
    {
        current_.lower_boundary_list_ = bounding_surfaces_list;
    }
    else
    {
        current_.lower_boundary_list_ = {};
    }

    return current_.bounded_below_;
}

bool SModellerImplementation::preserveBelow( std::vector<ControllerSurfaceIndex> bounding_surfaces_list )
{
    ContainerSurfaceIndex index;
    std::vector<ContainerSurfaceIndex> surface_ids;
    /* stopPreserveBelow(); */

    std::sort(bounding_surfaces_list.begin(), bounding_surfaces_list.end());
    std::vector<ControllerSurfaceIndex> intersection_list;

    if (current_.bounded_below_)
    {
        std::set_intersection(
                bounding_surfaces_list.begin(), bounding_surfaces_list.end(),
                current_.lower_boundary_list_.begin(), current_.lower_boundary_list_.end(),
                std::back_inserter(intersection_list));

        if (!intersection_list.empty())
        {
            DLOG(WARNING) << "Trying to apply PA and PB to the same set of surfaces";
            return false;
        }
    }

    for ( auto &surface_index : bounding_surfaces_list )
    {
        if ( getSurfaceIndex(surface_index, index) == false )
        {
            return false;
        }
        surface_ids.push_back(index);
    }
    stopPreserveBelow();

    current_.bounded_above_ = container_.defineBelow(surface_ids);
    if ( current_.bounded_above_ )
    {
        current_.upper_boundary_list_ = bounding_surfaces_list;
    }
    else
    {
        current_.upper_boundary_list_ = {};
    }

    return current_.bounded_above_;
}

void SModellerImplementation::stopPreserveAbove()
{
    current_.bounded_below_ = false;
    current_.lower_boundary_list_ = {};
    container_.stopDefineAbove();
}

void SModellerImplementation::stopPreserveBelow()
{
    current_.bounded_above_ = false;
    current_.upper_boundary_list_ = {};
    container_.stopDefineBelow();
}

bool SModellerImplementation::preserveAboveIsActive( std::vector<ControllerSurfaceIndex> &bounding_surfaces_list )
{
    if ( current_.bounded_below_ == false )
    {
        return false;
    }

    bounding_surfaces_list = current_.lower_boundary_list_;

    return true;
}

bool SModellerImplementation::preserveBelowIsActive( std::vector<ControllerSurfaceIndex> &bounding_surfaces_list )
{
    if ( current_.bounded_above_ == false )
    {
        return false;
    }

    bounding_surfaces_list = current_.upper_boundary_list_;

    return true;
}

bool SModellerImplementation::createAboveIsActive()
{
    return current_.bounded_below_;
}

bool SModellerImplementation::createAboveIsActive( ControllerSurfaceIndex &boundary_index )
{
    if ( current_.bounded_below_ )
    {
        boundary_index = current_.lower_boundary_;
    }

    return current_.bounded_below_;
}

bool SModellerImplementation::createBelowIsActive()
{
    return current_.bounded_above_; 
}

bool SModellerImplementation::createBelowIsActive( ControllerSurfaceIndex &boundary_index )
{
    if ( current_.bounded_above_ )
    {
        boundary_index = current_.upper_boundary_;
    }

    return current_.bounded_above_; 
}

bool SModellerImplementation::createBelow( ControllerSurfaceIndex surface_index )
{
    ContainerSurfaceIndex index;
    if ( getSurfaceIndex(surface_index, index) == false )
    {
        return false; 
    }

    // Do not accept surface as boundary if it was alread
    // used as a lower boundary
    if ( createAboveIsActive() == true )
    {
        if ( current_.lower_boundary_ == surface_index )
        {
            return false;
        }
    }
    current_.bounded_above_ = container_.defineBelow(index);
    current_.upper_boundary_ = surface_index;

    /* std::cout << "Upper boundary id: " << surface_index << std::endl; */

    return current_.bounded_above_; 
}

//
// brief:
// Clear any previous `createBelow()` call.
// Safe to call anytime.
//
void SModellerImplementation::stopCreateBelow()
{
    current_.bounded_above_ = false;
    container_.stopDefineBelow(); 
}

bool SModellerImplementation::createAbove( ControllerSurfaceIndex surface_index )
{
    ContainerSurfaceIndex index;
    if ( getSurfaceIndex(surface_index, index) == false )
    {
        return false; 
    }

    // Do not accept surface as boundary if it was alread
    // used as an upper boundary
    if ( createBelowIsActive() == true )
    {
        if ( current_.upper_boundary_ == surface_index )
        {
            return false;
        }
    }

    current_.bounded_below_ = container_.defineAbove(index); 
    current_.lower_boundary_ = surface_index;

    TODO_UNUSED(0); // add logger for couts
    /* std::cout << "Lower boundary id: " << surface_index << std::endl; */

    return current_.bounded_below_; 
}


void SModellerImplementation::stopCreateAbove()
{
    current_.bounded_below_ = false;
    container_.stopDefineAbove(); 
}

bool SModellerImplementation::enforceDefineRegion()
{
    bool status = true; 

    if ( current_.bounded_above_ )
    {
        status &= preserveBelow(current_.upper_boundary_list_);
    }
    else
    {
        stopPreserveBelow();
    }

    if ( current_.bounded_below_)
    {
        status &= preserveAbove(current_.lower_boundary_list_);
    }
    else
    {
        stopPreserveAbove();
    }

    return status; 
}

bool SModellerImplementation::buildTetrahedralMesh()
{
    bool success = true;
    if (mesh_ == nullptr)
    {
        success = buildFastTetrahedralMesh();
    }

    return success;
}

bool SModellerImplementation::buildFastTetrahedralMesh()
{
    bool success = true;
    /* if ( mesh_ == nullptr ) */
    {
        mesh_ = std::make_shared<MeshBuilder<PlaninASurface>>(container_);
        
        using Natural = typename StructuredTriangleMesh2D<Eigen::RowVector2d>::Natural;
        /* std::array<double, 3> origin, length; */
        // auto origin = point3(origin_[0], origin_[1], origin_[2]);
        // auto length = point3(lenght_[0], lenght_[1], lenght_[2]);
        StructuredTriangleMesh2D<Eigen::RowVector2d> mesh;
        mesh.setDomainOrigin(Eigen::RowVector2d({origin_[0], origin_[1]}));
        mesh.setDomainLength(Eigen::RowVector2d({lenght_[0], lenght_[1]}));
        mesh.setDiscretization(discWidth_, discLenght_);
        
        Eigen::MatrixXd vertices;
        Eigen::MatrixXi triangles;

        vertices.resize(mesh.numVertices(), 2);
        Eigen::RowVector2d v;
        for (Natural i = 0; i < mesh.numVertices(); ++i)
        {
            mesh.vertex(i, v);
            vertices.row(i) = v;
        }

        using Triangle = typename StructuredTriangleMesh2D<Eigen::RowVector2d>::Triangle;
        triangles.resize(mesh.numTriangles(), 3);
        Triangle t;
        for (Natural i = 0; i < mesh.numTriangles(); ++i)
        {
            mesh.triangle(i, t);
            triangles.row(i) = Eigen::RowVector3i({
                    static_cast<int>(t[0]),
                    static_cast<int>(t[1]), 
                    static_cast<int>(t[2])
                        });
        }

        success = mesh_->buildPrismMesh(vertices, triangles);
    }

    /* mesh_->exportToVTK("model_fast"); */
    if (!success)
    {
        mesh_ = nullptr;
    }

    return success;
}

bool SModellerImplementation::buildAdaptedTetrahedralMesh()
{
    /* unsigned int n = std::thread::hardware_concurrency(); */
    bool success = true;
    /* if ( mesh_ == nullptr ) */
    {
        mesh_ = std::make_shared<MeshBuilder<PlaninASurface>>(container_);

        /* std::vector<double> vlist; */
        /* std::vector<std::size_t> flist; */
        /* MeshingHelper mesher; */
        /* for (std::size_t i = 0; i < container_.size(); ++i) */
        /* { */
        /*     container_[i]->getRawMesh(vlist, flist); */
        /*     mesher.addSurface(i, vlist, flist); */
        /* } */

        /* { */
        /*     /1* Add top surface *1/ */
        /*     auto num_vertices = vlist.size(); */
        /*     std::vector<double> vertices; */
        /*     vertices.resize(num_vertices, origin_[2] + lenght_[2]); */
        /*     mesher.addSurface(vertices, flist); */
        /* } */

        /* { */
        /*     /1* Add bottom surface *1/ */
        /*     auto num_vertices = vlist.size(); */
        /*     std::vector<double> vertices; */
        /*     vertices.resize(num_vertices, origin_[2]); */
        /*     mesher.addSurface(vertices, flist); */
        /* } */

        Eigen::MatrixXd V;
        Eigen::MatrixXi F;
        auto start = std::chrono::system_clock::now();
        /* mesher.getDomainSimplicialComplex(V, F); */
        container_.getDomainSimplicialComplex(V, F);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed = end - start;
        std::cout << "\nTime to remesh self intersections: " << elapsed.count() << "\n";

        start = std::chrono::system_clock::now();
        success = mesh_->buildPrismMesh(V, F);
        end = std::chrono::system_clock::now();
        elapsed = end - start;
        std::cout << "\nTime to compute tetrahedral mesh: " << elapsed.count() << "\n";
    }

    /* mesh_->exportToVTK("model_fine"); */
    if (!success)
    {
        mesh_ = nullptr;
    }

    return success;
}

} // namespace stratmod
