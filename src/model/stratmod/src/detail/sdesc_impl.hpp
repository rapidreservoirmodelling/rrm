#include <memory>

#include "cereal/types/memory.hpp"
#include "cereal/types/polymorphic.hpp"

#include <glog/logging.h>

#include "planin/sdesc.hpp"

#include "stratmod/surface_descriptor.hpp"

#ifndef STRATMOD_DETAIL_SDESC_IMPL
#define STRATMOD_DETAIL_SDESC_IMPL

namespace stratmod {
    namespace detail {
        class SDescImpl : public planin::SDesc {
            public:
                void set(const std::shared_ptr<SurfaceDescriptor>& sdptr)
                {
                    sdptr_ = sdptr;
                }

                std::shared_ptr<SurfaceDescriptor> get()
                {
                    return sdptr_;
                }

            private:
                std::shared_ptr<stratmod::SurfaceDescriptor> sdptr_{};

                // Cereal provides an easy way to serialize objects
                friend class cereal::access;

                template<typename Archive>
                void serialize(Archive& ar, const std::uint32_t /* version */)
                {
                    ar(cereal::base_class<planin::SDesc>(this));

                    /* LOG(INFO) << "Call to serialize method in stratmod::detail::SDescImpl"; */
                    ar(sdptr_);
                }
        };
    } // namespace detail
} // namespace stratmod

CEREAL_CLASS_VERSION(stratmod::detail::SDescImpl, 0);

CEREAL_REGISTER_TYPE(stratmod::detail::SDescImpl);
CEREAL_REGISTER_POLYMORPHIC_RELATION(planin::SDesc, stratmod::detail::SDescImpl);

#endif
