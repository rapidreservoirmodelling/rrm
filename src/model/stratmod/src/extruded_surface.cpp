/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#include "stratmod/surfaces/extruded_surface.hpp"

#include <cmath>

#include <glog/logging.h>
#include <optional>

#include "planin/thinplatespline22.hpp"

// This is planin's ThinPlateSpline22
static ThinPlateSpline22 kernel;

namespace stratmod 
{
    double ExtrudedSurface::CurveInterpolant::operator()(double x) const
    {
        if ( !interpolant_is_set_ )
        {
            return 0; 
        }

        double z = 0; 
        const size_t size = centers_.rows(); 

        for ( size_t i = 0; i < size; ++i ) 
        {
            z += weights_[i] * kernel(x - centers_[i], 0.); 
        }

        z += weights_[size + 0]*1.;
        z += weights_[size + 1]*x;

        return z; 
    }

    double ExtrudedSurface::CurveInterpolant::D(double x) const
    {
        if ( !interpolant_is_set_ ) { 
            return 0.;
        }

        double dx = 0; //, dy = 0; 
        const size_t size = centers_.rows(); 

        for ( size_t i = 0; i < size; ++i ) 
        {
            dx += weights_[i] * kernel.Dx(x - centers_[i], 0.); 
        }

        dx += weights_[size + 1]; 

        return dx;
    }

    bool ExtrudedSurface::CurveInterpolant::set(const Eigen::MatrixXd& points, double smoothing_parameter)
    {
        if (points.cols() != 2)
        {
            DLOG(WARNING) << "Failed to set interpolant: number of cols == " << points.cols() << "; expected 2";
            return false;
        }

        if (points.rows() < 2)
        {
            DLOG(WARNING) << "Failed to set interpolant: number of rows == " << points.rows() << "; expected >= 2";
            return false;
        }

        /* auto colinear = [](const Eigen::MatrixXd& points) -> bool { */
        /*     DCHECK_EQ(points.rows(), 3) << ": At this point number of rows must be 3"; */
        /*     DCHECK_EQ(points.cols(), 3) << ": At this point number of cols must be 3"; */

        /*     double tolerance = 1E-6; */

        /*     Eigen::RowVector3d p0 = points.row(0); */
        /*     Eigen::RowVector3d p1 = points.row(1); */
        /*     Eigen::RowVector3d p2 = points.row(2); */

        /*     if ((p1 - p0).cross(p2 - p0).norm() <= tolerance) */
        /*     { */
        /*         return true; */
        /*     } */

        /*     return false; */
        /* }; */
        /* if ((points.rows() == 3) && (colinear(points))) */
        /* { */
        /*     DLOG(WARNING) << "Falied to set interpolant: points are colinear"; */
        /*     return false; */
        /* } */

        const double eigenvalues_lower_bound = smoothing_parameter > 0. ? smoothing_parameter : 0.; 

        int poly_dim = 2;
        size_t num_points = points.rows(); 

        centers_.resize(num_points, 1);
        weights_.resize(num_points + poly_dim); 

        Eigen::MatrixXd A(num_points + poly_dim, num_points + poly_dim);
        Eigen::MatrixXd P(num_points, poly_dim); 
        Eigen::VectorXd heights(num_points + poly_dim);

        centers_ << points.col(0);

        heights << points.col(1), Eigen::VectorXd::Zero(poly_dim, 1);

        double aij = 0; 
        double pi, pj; 

        for ( size_t i = 0; i < num_points; ++i ) { 
            pi = centers_[i]; 
            P(i,0) = 1; 
            P(i,1) = pi; 

            for ( size_t j = 0; j < num_points; ++j ) { 
                pj = centers_[j]; 
                aij = kernel(pi - pj, 0.); 
                aij += ( i == j ) ? eigenvalues_lower_bound : 0.0; 

                A(i,j) = aij; 
            }
        }

        A.block(0, num_points, num_points, poly_dim) = P; 
        A.block(num_points, 0, poly_dim, num_points) = P.transpose(); 
        A.block(num_points, num_points, poly_dim, poly_dim) = Eigen::MatrixXd::Constant(poly_dim, poly_dim, 0); 

        weights_ = A.lu().solve(heights); 
        interpolant_is_set_ = true; 

        return true; 
    }

    bool ExtrudedSurface::CurveInterpolant::operator==(const CurveInterpolant& rhs) const
    {
        bool is_equal = true;
        is_equal &= (interpolant_is_set_ == rhs.interpolant_is_set_);
        is_equal &= (centers_ == rhs.centers_);
        is_equal &= (weights_ == rhs.weights_);

        return is_equal;
    }

    double ExtrudedSurface::evalHeight(const Eigen::Vector2d& p) const
    {
        double z = 0.;

        double path_origin_x = 0.;
        double path_origin_y = 0.;

        if ( path_is_set_ && cross_section_is_set_ )
        {
            double origin = path_(path_origin_x) - path_origin_y;
            z = cross_section_(p[0] - (path_(p[1]) - origin));
        }
        else if (cross_section_is_set_)
        {
            z = cross_section_(p[0]); 
        }

        return z;
    }

    Eigen::Vector2d ExtrudedSurface::evalDerivative(const Eigen::Vector2d& p) const
    {
        double dx = 0.;
        double dy = 0.;

        double path_origin_x = 0.;
        double path_origin_y = 0.;

        if ( path_is_set_ && cross_section_is_set_ )
        {
            /* std::cout << "Case: !orthogonally_oriented_ && path_is_set\n"; */
            double origin = path_(path_origin_x) - path_origin_y;
            /* height = f(p.x - (path(p.y, 0) - origin), 0); */

            dx = cross_section_.D(p[0] - (path_(p[1]) - origin));
            dy = cross_section_.D(p[0] - (path_(p[1]) - origin)) * path_.D(p[1]);

            /* std::cout << "origin = " << origin << ", dx = " << dx << ", dy = " << dy << "\n"; */
        }
        else if (cross_section_is_set_)
        {
            /* std::cout << "Case: !orthogonally_oriented_ && !path_is_set\n"; */
            dx = cross_section_.D(p[0]); 
            dy = 0; 
            /* std::cout << "dx = " << dx << ", dy = " << dy << "\n"; */
        }

        return Eigen::Vector2d({dx, dy});
    }

    bool ExtrudedSurface::differentiable() const
    {
        return true;
    }

    bool ExtrudedSurface::setLinearExtrusion(const Eigen::MatrixXd& cross_section_curve)
    {
        double smoothing_parameter = 1E-7;
        cross_section_is_set_ = cross_section_.set(cross_section_curve, smoothing_parameter);
        if(cross_section_is_set_)
        {
            cross_section_curve_ = cross_section_curve;
        }

        return cross_section_is_set_;
    }

    bool ExtrudedSurface::setPathGuidedExtrusion(const Eigen::MatrixXd& cross_section_curve, const Eigen::MatrixXd& path_curve)
    {
        double smoothing_parameter = 1E-7;
        cross_section_is_set_ = cross_section_.set(cross_section_curve, smoothing_parameter);
        if (cross_section_is_set_)
        {
            Eigen::MatrixXd path;
            // path = (transformDomain() * path_curve.transpose().colwise().homogeneous()).transpose();
            path = path_curve;
            path_is_set_ = path_.set(path, smoothing_parameter);
        }

        if (!path_is_set_ || !cross_section_is_set_)
        {
            cross_section_is_set_ = false;
            cross_section_ = CurveInterpolant();
            path_is_set_ = false;
            path_ = CurveInterpolant();

            return false;
        }

        cross_section_curve_ = cross_section_curve;
        path_curve_ = path_curve;

        return true;
    }

    void ExtrudedSurface::setCrossSectionOrigin(const Eigen::Vector2d& origin)
    {
        InputSurface::resetTranslateDomain();
        InputSurface::translate({origin[0], origin[1], 0.});

        /* if (path_is_set_) */
        /* { */
            /* setPathGuidedExtrusion(std::move(cross_section_curve_), std::move(path_curve_)); */
        /* } */
    }

    bool ExtrudedSurface::setCrossSectionDirection(const Eigen::Vector2d& direction)
    {
        if (direction.norm() <= 1E-5)
        {
            return false;
        }
        InputSurface::resetRotateDomain();

        double angle = std::atan2(direction[1], direction[0]);
        Eigen::Vector2d origin = getCrossSectionOrigin();
        InputSurface::rotate(angle, {origin[0], origin[1], 0.});
        
        /* if (path_is_set_) */
        /* { */
            /* setPathGuidedExtrusion(std::move(cross_section_curve_), std::move(path_curve_)); */
        /* } */

        return true;
    }

    Eigen::Vector2d ExtrudedSurface::getCrossSectionOrigin()
    {
        Eigen::Vector2d origin = InputSurface::getTransform().translation().head<2>();

        return origin;
    }

    Eigen::Vector2d ExtrudedSurface::getCrossSectionDirection()
    {
        Eigen::Matrix2d R = InputSurface::getTransform().rotation().topLeftCorner<2, 2>();
        Eigen::Vector2d direction = R * Eigen::Vector2d::UnitX();

        return direction.normalized();
    }

    std::optional<Eigen::MatrixXd> ExtrudedSurface::getCrossSectionCurve()
    {
        if (!cross_section_is_set_)
        {
            std::make_optional<Eigen::MatrixXd>();
        }

        return std::make_optional<Eigen::MatrixXd>(cross_section_curve_);
    }

    std::optional<Eigen::MatrixXd> ExtrudedSurface::getPathCurve()
    {
        if (!path_is_set_)
        {
            std::make_optional<Eigen::MatrixXd>();
        }

        return std::make_optional<Eigen::MatrixXd>(path_curve_);
    }

} // namespace stratmod

/* CEREAL_CLASS_VERSION(stratmod::ExtrudedSurface::CurveInterpolant, 0); */
