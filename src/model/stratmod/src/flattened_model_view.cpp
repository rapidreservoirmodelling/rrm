/* This file is part of the stratmod library */
/* Copyright (C) 2017-2021, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#include "stratmod/flattened_model_view.hpp"

#include "detail/smodeller2_access.hpp"
#include "detail/smodeller_impl.hpp"
#include "planin/planar_surface.hpp"
#include <memory>
#include <mutex>

namespace stratmod {

struct FlattenedModelView::FlattenedModelViewImpl
{
    using PlaninASurface = PlanarSurface;

    PlaninASurface::WeakPtr wptr{};
    PlaninASurface::Ptr locked{};
    PlaninASurface::SurfaceId psurface_id{};
    double base_height{};
    uintptr_t signature = reinterpret_cast<uintptr_t>(nullptr);
    std::mutex m_{};

    uintptr_t currentSignature(const PlaninASurface::Ptr& sptr) const
    {
        auto iptr = dynamic_cast<detail::ISurfaceImpl&>(*DCHECK_NOTNULL(sptr->getISurface())).getInputSurface();
        return reinterpret_cast<uintptr_t>(iptr.get());
    }

    void set(const PlaninASurface::Ptr& sptr, double height)
    {
        m_.lock();
        wptr = PlaninASurface::WeakPtr(sptr);
        locked = std::make_shared<PlaninASurface>(*DCHECK_NOTNULL(sptr));
        psurface_id = sptr->getID();
        base_height = height;
        signature = currentSignature(sptr);
        m_.unlock();
    }

    bool expired() const
    {
        if (auto sptr = wptr.lock())
        {
            if (signature == currentSignature(sptr))
            {
                return false;
            }
        }

        return true;
    }

    bool lock()
    {
        m_.lock();
        if (!expired())
        {
            return true;
        }

        m_.unlock();
        return false;
    }

    void unlock()
    {
        m_.unlock();
    }

    double deltaH(double u, double v) const {
        double h;
        locked->getHeight(Point2({u, v}), h);
        return h - base_height;
    }
};

FlattenedModelView::FlattenedModelView()
{
    pimpl_ = std::make_unique<FlattenedModelViewImpl>();
}

FlattenedModelView::~FlattenedModelView() = default;

FlattenedModelView::FlattenedModelView(FlattenedModelView&&) = default;

FlattenedModelView& FlattenedModelView::operator=(FlattenedModelView&&) = default;

std::optional<FlattenedModelView> FlattenedModelView::Get(SurfaceUniqueIdentifier surface_id, double height)
{
    std::optional<FlattenedModelView> view;
    ContainerSurfaceIndex sid;

    if (SModeller2Access::Get()->getSurfaceIndex(surface_id, sid))
    {
        FlattenedModelView fmv;
        fmv.pimpl_->set(SModeller2Access::Get()->container_[sid], height);
        view = std::make_optional<FlattenedModelView>(std::move(fmv));
    }

    return view;
}

std::optional<SurfaceUniqueIdentifier> FlattenedModelView::flattenedSurfaceIndex()
{
    std::optional<SurfaceUniqueIdentifier> surface_id;
    if (lock())
    {
        SurfaceUniqueIdentifier id;
        if (SModeller2Access::Get()->getControllerIndexFromPlanarSurfaceId(pimpl_->psurface_id, id))
        {
            surface_id = std::make_optional<SurfaceUniqueIdentifier>(id);
        }
        unlock();
    }

    return surface_id;
}

std::optional<double> FlattenedModelView::flattenedSurfaceHeight()
{
    std::optional<double> surface_height;
    if (lock())
    {
        double height = pimpl_->base_height;
        surface_height = std::optional<double>(height);
        unlock();
    }

    return surface_height;
}

bool FlattenedModelView::expired() const
{
    return pimpl_->expired();
}

Eigen::RowVector3d FlattenedModelView::fCoords(double x, double y, double z)
{
    Eigen::RowVector3d P;

    double u, v, h;
    u = x;
    v = y;
    h = z - pimpl_->deltaH(u, v);

    // Only works in default coordinates
    P << u, v, h;
    return P;
}

Eigen::RowVector3d FlattenedModelView::wCoords(double x, double y, double z)
{
    Eigen::RowVector3d P;

    double u, v, h;
    u = x;
    v = y;
    h = z + pimpl_->deltaH(u, v);

    // Only works in default coordinates
    P << u, v, h;
    return P;
}

std::shared_ptr<stratmod::TriangleSurface> FlattenedModelView::unFlattenSurface(std::shared_ptr<InputSurface> iptr)
{
    auto sptr = std::make_shared<stratmod::TriangleSurface>();

    Eigen::MatrixXd V;
    Eigen::MatrixXi T;
    InputSurface::sample(iptr, V, T);

    mapFromFlattenedCoordinates(V);
    sptr->set(V, T);

    return sptr;
}

bool FlattenedModelView::lock()
{
    return pimpl_->lock();
}

void FlattenedModelView::unlock()
{
    pimpl_->unlock();
}

} // namespace stratmod
