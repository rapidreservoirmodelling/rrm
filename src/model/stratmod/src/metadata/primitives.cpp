/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#include "stratmod/metadata/primitives.hpp"

namespace stratmod {

CommonMetadata::~CommonMetadata() = default;

bool operator==(const ScalarProperty& lhs, const ScalarProperty& rhs)
{
    bool equal = true;
    equal &= (lhs.value == rhs.value);
    equal &= (lhs.name == rhs.name);
    equal &= (lhs.unit == rhs.unit);

    return equal;
}

bool operator!=(const ScalarProperty& lhs, const ScalarProperty& rhs)
{
    return !(lhs == rhs);
}

bool operator==(const Vector3dProperty& lhs, const Vector3dProperty& rhs)
{
    bool equal = true;
    equal &= (lhs.value == rhs.value);
    equal &= (lhs.name == rhs.name);
    equal &= (lhs.unit == rhs.unit);

    return equal;
}

bool operator!=(const Vector3dProperty& lhs, const Vector3dProperty& rhs)
{
    return !(lhs == rhs);
}

bool operator==(const Matrix3dProperty& lhs, const Matrix3dProperty& rhs)
{
    bool equal = true;
    equal &= (lhs.value == rhs.value);
    equal &= (lhs.name == rhs.name);
    equal &= (lhs.unit == rhs.unit);

    return equal;
}

bool operator!=(const Matrix3dProperty& lhs, const Matrix3dProperty& rhs)
{
    return !(lhs == rhs);
}

}
