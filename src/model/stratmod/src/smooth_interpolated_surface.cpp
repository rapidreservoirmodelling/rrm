/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#include "stratmod/surfaces/smooth_interpolated_surface.hpp"

#include <cmath>

#include <glog/logging.h>

#include "planin/thinplatespline22.hpp"

// This is planin's ThinPlateSpline22
static ThinPlateSpline22 kernel;

namespace stratmod 
{
    double SmoothInterpolatedSurface::evalHeight(const Eigen::Vector2d& p) const 
    {
        if ( !interpolant_is_set_ )
        {
            return 0; 
        }

        double z = 0; 
        double x = p[0];
        double y = p[1];

        const size_t size = centers_.rows(); 

        for ( size_t i = 0; i < size; ++i ) 
        {
            z += weights_[i] * kernel(x - centers_(i,0), y - centers_(i,1)); 
        }

        z += weights_[size + 0]*1.;
        z += weights_[size + 1]*x;
        z += weights_[size + 2]*y;

        return z; 
    }

    bool SmoothInterpolatedSurface::differentiable() const 
    {
        return true;
    }

    Eigen::Vector2d SmoothInterpolatedSurface::evalDerivative(const Eigen::Vector2d& p) const 
    {
        if ( !interpolant_is_set_ ) { 
            return Eigen::Vector2d();
        }

        double dx = 0, dy = 0; 
        const size_t size = centers_.rows(); 

        for ( size_t i = 0; i < size; ++i ) 
        {
            dx += weights_[i] * kernel.Dx(p[0] - centers_(i,0), p[1] - centers_(i,1)); 
            dy += weights_[i] * kernel.Dy(p[0] - centers_(i,0), p[1] - centers_(i,1)); 
        }

        dx += weights_[size + 1]; 
        dy += weights_[size + 2]; 

        return Eigen::Vector2d({dx, dy});
    }

    bool SmoothInterpolatedSurface::set(const Eigen::MatrixXd& points, double smoothing_parameter)
    {
        if (points.cols() != 3)
        {
            DLOG(WARNING) << "Failed to set interpolant: number of cols == " << points.cols() << "; expected 3";
            return false;
        }

        if (points.rows() <= 2)
        {
            DLOG(WARNING) << "Failed to set interpolant: number of rows == " << points.rows() << "; expected >= 3";
            return false;
        }

        auto colinear = [](const Eigen::MatrixXd& points) -> bool {
            DCHECK_EQ(points.rows(), 3) << ": At this point number of rows must be 3";
            DCHECK_EQ(points.cols(), 3) << ": At this point number of cols must be 3";

            double tolerance = 1E-6;

            Eigen::RowVector3d p0 = points.row(0);
            Eigen::RowVector3d p1 = points.row(1);
            Eigen::RowVector3d p2 = points.row(2);

            if ((p1 - p0).cross(p2 - p0).norm() <= tolerance)
            {
                return true;
            }

            return false;
        };
        if ((points.rows() == 3) && (colinear(points)))
        {
            DLOG(WARNING) << "Falied to set interpolant: points are colinear";
            return false;
        }

        /* InputSurface::resetTransform(); */

        const double eigenvalues_lower_bound = smoothing_parameter > 0. ? smoothing_parameter : 0.; 

        int poly_dim = 3;
        size_t num_points = points.rows(); 

        centers_.resize(num_points, 2);
        weights_.resize(num_points + poly_dim); 

        Eigen::MatrixXd A(num_points + poly_dim, num_points + poly_dim);
        Eigen::MatrixXd P(num_points, poly_dim); 
        Eigen::VectorXd heights(num_points + poly_dim);

        centers_.col(0) = points.col(0);
        centers_.col(1) = points.col(1);

        heights << points.col(2), Eigen::VectorXd::Zero(poly_dim, 1);

        double aij = 0; 
        Eigen::Vector2d pi, pj; 

        for ( size_t i = 0; i < num_points; ++i ) { 
            pi = centers_.row(i); 
            P(i,0) = 1; 
            P(i,1) = pi[0]; 
            P(i,2) = pi[1]; 

            for ( size_t j = 0; j < num_points; ++j ) { 
                pj = centers_.row(j); 
                aij = kernel(pi[0] - pj[0], pi[1] - pj[1]); 
                aij += ( i == j ) ? eigenvalues_lower_bound : 0.0; 

                A(i,j) = aij; 
            }
        }

        A.block(0, num_points, num_points, poly_dim) = P; 
        A.block(num_points, 0, poly_dim, num_points) = P.transpose(); 
        A.block(num_points, num_points, poly_dim, poly_dim) = Eigen::MatrixXd::Constant(poly_dim, poly_dim, 0); 

        weights_ = A.lu().solve(heights); 
        interpolant_is_set_ = true; 
        points_ = points;

        return true; 
    }

    bool SmoothInterpolatedSurface::getData(Eigen::MatrixXd& data) const
    {
        if (!interpolant_is_set_)
        {
            return false;
        }

        data = centers_;
        return true;
    }

} // namespace stratmod
