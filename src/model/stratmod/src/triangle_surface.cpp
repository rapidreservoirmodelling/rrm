/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#include "stratmod/surfaces/triangle_surface.hpp"

#include <algorithm>
#include <numeric>
#include <cmath>
#include <vector>

#include <glog/logging.h>

#include <igl/AABB.h>
#include <igl/all.h>
#include <igl/barycenter.h>
#include <igl/remove_duplicate_vertices.h>
#include <igl/barycentric_coordinates.h>
#include <igl/boundary_facets.h>
/* #include <igl/triangle/triangulate.h> */
#include <igl/is_delaunay.h>
#include <igl/copyleft/cgal/submesh_aabb_tree.h>
#include <igl/copyleft/cgal/point_mesh_squared_distance.h>
#include <igl/winding_number.h>

// This is planin's ThinPlateSpline22

namespace stratmod 
{
    struct TriangleSurface::TriangleSurfaceImpl {
        using TriangleIndex = int;
        using BaricentricCoordinates = Eigen::MatrixXd;

        igl::AABB<Eigen::MatrixXd, 2> tree_;
        igl::AABB<Eigen::MatrixXd, 2> convex_hull_tree_;

        template<typename Derived>
        std::pair<TriangleIndex, BaricentricCoordinates> project(const Eigen::PlainObjectBase<Derived>& point, const Eigen::MatrixXd& vertices, const Eigen::MatrixXi& triangles) const
        {
            TriangleIndex index;
            Eigen::RowVector2d q;
            tree_.squared_distance(vertices, triangles, point, index, q);

            TriangleIndex ta = triangles(index, 0);
            const Eigen::RowVector2d& a = vertices.row(ta);

            TriangleIndex tb = triangles(index, 1);
            const Eigen::RowVector2d& b = vertices.row(tb);

            TriangleIndex tc = triangles(index, 2);
            const Eigen::RowVector2d& c = vertices.row(tc);

            BaricentricCoordinates coords;
            igl::barycentric_coordinates(q, a, b, c, coords);

            return std::make_pair(index, coords);
        }
    };

    double TriangleSurface::evalHeight(const Eigen::Vector2d& p) const 
    {
        double z = 0; 

        if (surface_is_set_) {
            int index = 0;
            Eigen::RowVector2d q;

            /* int i = 1; */
            Eigen::MatrixXd P = p.transpose();
            /* int i = static_cast<int>(igl::winding_number(vertices_, triangles_, P)); */
            DCHECK_NOTNULL(pimpl_)->tree_.squared_distance(vertices_, triangles_, p, index, q);
            /* if (i != 0) */
            /* { */
            /* } */
            /* else */
            /* { */
            /*     DCHECK_NOTNULL(pimpl_)->convex_hull_tree_.squared_distance(vertices_, convex_hull_, p, index, q); */
            /* } */

            int index_a = triangles_(index, 0);
            const Eigen::RowVector2d& a = vertices_.row(index_a);

            int index_b = triangles_(index, 1);
            const Eigen::RowVector2d& b = vertices_.row(index_b);

            int index_c = triangles_(index, 2);
            const Eigen::RowVector2d& c = vertices_.row(index_c);

            Eigen::MatrixXd coords;
            igl::barycentric_coordinates(q, a, b, c, coords);

            z = heights_[index_a]*coords.row(0)[0] + heights_[index_b]*coords.row(0)[1] + heights_[index_c]*coords.row(0)[2];
        }

        return z; 
    }

    bool TriangleSurface::differentiable() const 
    {
        return false;
    }

    Eigen::Vector2d TriangleSurface::evalDerivative(const Eigen::Vector2d& /* p */) const 
    {
        return Eigen::Vector2d::Zero();
    }

    bool TriangleSurface::set(const Eigen::MatrixXd& vertices, const Eigen::MatrixXi& triangles)
    {
        if (triangles.cols() != 3)
        {
            DLOG(WARNING) << "Failed to set surface: number of cols == " << vertices.cols() << "; expected 3";
            return false;
        }

        if (vertices.cols() != 3)
        {
            DLOG(WARNING) << "Failed to set surface: number of cols == " << vertices.cols() << "; expected 3";
            return false;
        }

        if (vertices.rows() <= 2)
        {
            DLOG(WARNING) << "Failed to set surface: number of rows == " << vertices.rows() << "; expected >= 3";
            return false;
        }

        auto colinear = [](const Eigen::MatrixXd& vertices) -> bool {
            DCHECK_EQ(vertices.rows(), 3) << ": At this point number of rows must be 3";
            DCHECK_EQ(vertices.cols(), 3) << ": At this point number of cols must be 3";

            double tolerance = 1E-6;

            Eigen::RowVector3d p0 = vertices.row(0);
            Eigen::RowVector3d p1 = vertices.row(1);
            Eigen::RowVector3d p2 = vertices.row(2);

            if ((p1 - p0).cross(p2 - p0).norm() <= tolerance)
            {
                return true;
            }

            return false;
        };
        if ((vertices.rows() == 3) && (colinear(vertices)))
        {
            DLOG(WARNING) << "Falied to set surface: vertices are colinear";
            return false;
        }

        input_vertices_ = vertices;
        input_triangles_ = triangles;

        size_t num_vertices = vertices.rows(); 

        vertices_.resize(num_vertices, 2);
        vertices_.col(0) = vertices.col(0);
        vertices_.col(1) = vertices.col(1);

        heights_ = vertices.col(2);

        triangles_ = triangles;

        Eigen::MatrixXi J, K;
        igl::boundary_facets(triangles_, boundary_, J, K);

        /* tree_ = std::make_shared<igl::AABB<Eigen::MatrixXd, 2>>(); */
        /* tree_->init(vertices_, triangles_); */
        resetImpl();

        Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic> D;
        igl::is_delaunay(vertices_, triangles_, D);
        is_delaunay_ = D.all();

        surface_is_set_ = true;

        return true; 
    }

   /* bool convexHull(); */
   /* bool TriangleSurface::convexHull() */
   /* { */
   /*     if (!surface_is_set_) */
   /*     { */
   /*         return false; */
   /*     } */

   /*     if (isDelaunay()) */
   /*     { */
   /*         return true; */
   /*     } */

   /*     Eigen::MatrixXi boundary_edges, J, K; */
   /*     igl::boundary_facets(triangles_, boundary_edges, J, K); */
   /*     boundary_ = boundary_edges; */

   /*     // Select all vertices that appear on boundary */
   /*     std::cout << "Preparing to compute index map\n" << std::flush; */
   /*     std::vector<int> index_map; */
   /*     std::map<int, std::size_t> inverse_index_map; */
   /*     for (int i = 0; i < boundary_edges.rows(); ++i) */
   /*     { */
   /*         inverse_index_map[boundary_edges(i,0)] = 0; */
   /*         inverse_index_map[boundary_edges(i,1)] = 0; */
   /*         /1* index_map.push_back(boundary_edges(i,0)); *1/ */
   /*         /1* index_map.push_back(boundary_edges(i,1)); *1/ */
   /*     } */
   /*     std::cout << "sorting index map\n" << std::flush; */
   /*     /1* std::sort(index_map.begin(), index_map.end()); *1/ */
   /*     /1* index_map.erase(std::unique(index_map.begin(), index_map.end()), index_map.end()); *1/ */
   /*     for(auto& e : inverse_index_map) */
   /*     { */
   /*         e.second = index_map.size(); */
   /*         index_map.push_back(e.first); */
   /*     } */

   /*     Eigen::MatrixXd boundary_vertices(index_map.size(), vertices_.cols()); */
   /*     std::cout << "Copying boundary vertices\n" << std::flush; */
   /*     for (int i = 0; i < boundary_vertices.rows(); ++i) */
   /*     { */
   /*         boundary_vertices.row(i) = vertices_.row(index_map[i]); */
   /*     } */
   /*     std::cout << "Mapping boundary vertices indices\n" << std::flush; */
   /*     for (int i = 0; i < boundary_edges.rows(); ++i) */
   /*     { */
   /*         boundary_edges(i,0) = inverse_index_map[boundary_edges(i,0)]; */
   /*         boundary_edges(i,1) = inverse_index_map[boundary_edges(i,1)]; */

   /*     } */
       
   /*     // Create a map from vertices to ids on vertices_ */


   /*     // Must go through all connected components and pick a point */
   /*     // inside the component */
   /*     Eigen::MatrixXd holes; */
   /*     igl::barycenter(vertices_, triangles_.row(0), holes); */

   /*     Eigen::MatrixXd vout; */
   /*     Eigen::MatrixXi convex_hull_triangles; */
   /*     std::cout << "Computing triangulation\n" << std::flush; */
   /*     igl::triangle::triangulate(boundary_vertices, boundary_edges, holes, "pcYY",  vout, convex_hull_triangles); */
   /*     DCHECK(boundary_vertices == vout); */
   /*     if ( !(boundary_vertices == vout) ) */
   /*     { */
   /*         return false; */
   /*     } */

   /*     std::cout << "Mapping convex hull triangulation back to original indices\n" << std::flush; */
   /*     for (int i = 0; i < convex_hull_triangles.rows(); ++i) */
   /*     { */
   /*         convex_hull_triangles(i,0) = index_map[convex_hull_triangles(i,0)]; */
   /*         convex_hull_triangles(i,1) = index_map[convex_hull_triangles(i,1)]; */
   /*         convex_hull_triangles(i,2) = index_map[convex_hull_triangles(i,2)]; */
   /*     } */

   /*     std::cout << "Updating triangle list\n" << std::flush; */
   /*     convex_hull_ = convex_hull_triangles; */
   /*     convex_hull_is_set_ = true; */

   /*     Eigen::MatrixXi old_triangles = triangles_; */
   /*     triangles_.resize(old_triangles.rows() + convex_hull_triangles.rows(), old_triangles.cols()); */

   /*     triangles_ << old_triangles, convex_hull_triangles; */

   /*     resetImpl(); */

   /*     return true; */
   /* } */

   bool TriangleSurface::isDelaunay()
   {
       return is_delaunay_;
   }

   Eigen::MatrixXd TriangleSurface::getInputVertices() const
   {
       return input_vertices_;
   }
   
   Eigen::MatrixXi TriangleSurface::getInputTriangles() const
   {
       return input_triangles_;
   }

   Eigen::MatrixXd TriangleSurface::getVertices() const
   {
       Eigen::MatrixXd vertices;
       vertices.resize(vertices_.rows(), 3);
       vertices.col(0) = vertices_.col(0);
       vertices.col(1) = vertices_.col(2);
       vertices.col(2) = heights_;

       return vertices;
   }

   Eigen::MatrixXi TriangleSurface::getTriangles() const
    {
        return triangles_;
    }

    /* Eigen::MatrixXi getConvexHull() const; */
    /* Eigen::MatrixXi TriangleSurface::getConvexHull() const */
    /* { */
    /*     return convex_hull_; */
    /* } */

    Eigen::MatrixXi TriangleSurface::getBoundary() const
    {
        return boundary_;
    }

    void TriangleSurface::resetImpl()
    {
        pimpl_ = std::make_shared<TriangleSurfaceImpl>();
        pimpl_->tree_.init(vertices_, triangles_);

        if (convex_hull_is_set_)
        {
            pimpl_->convex_hull_tree_.init(vertices_, convex_hull_);
        }
    }

} // namespace stratmod
