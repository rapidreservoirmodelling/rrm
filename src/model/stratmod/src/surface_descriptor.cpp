/* #include "stratmod/misc/smodeller_primitives.hpp" */
#include "stratmod/surface_descriptor.hpp"

namespace stratmod {

    std::int64_t invalidGroupId() { return std::numeric_limits<std::int64_t>::min(); }

    SurfaceDescriptor::SurfaceDescriptor(SurfaceUniqueIdentifier id)
    { 
        unique_id_ = id;
        group_id_ = invalidGroupId();
    }

    SurfaceDescriptor::SurfaceDescriptor(SurfaceUniqueIdentifier id, SurfaceGroupIdentifier gid)
    {
        unique_id_ = id;
        group_id_ = static_cast<std::int64_t>(gid);
    }

        /* SurfaceDescriptor() = default; */

        /* SurfaceUniqueIdentifier unique_id() const; */
        /* SurfaceGroupIdentifier group_id() const; */

    /* SurfaceUniqueIdentifier SurfaceDescriptor::unique_id() const */
    /* { */
    /*     return group_id_; */
    /* } */

    /* SurfaceGroupIdentifier SurfaceDescriptor::group_id() const */
    /* { */
    /*     return unique_id_; */
    /* } */

    SurfaceUniqueIdentifier SurfaceDescriptor::uniqueId() const
    {
        return unique_id_;
    }

    std::optional<SurfaceGroupIdentifier> SurfaceDescriptor::groupId() const
    {
        if (group_id_ == invalidGroupId())
        {
            return std::optional<SurfaceGroupIdentifier>();
        }

        return std::optional(static_cast<SurfaceGroupIdentifier>(group_id_));
    }
} // stratmod
