/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#include <glog/logging.h>

#include "stratmod/surfaces/input_surface.hpp"
#include "detail/smodeller2_access.hpp"
#include "planin/planar_surface.hpp"

namespace stratmod {

    void InputSurface::sample(std::shared_ptr<InputSurface> iptr, std::vector<double>& vlist, std::vector<std::size_t>& tlist)
    {
        std::shared_ptr<detail::ISurfaceImpl> isptr = std::make_shared<detail::ISurfaceImpl>();
        isptr->set(iptr);

        auto& origin = SModeller2Access::Get()->origin_;
        auto& length = SModeller2Access::Get()->lenght_;

        PlanarSurface::Ptr sptr = std::make_shared<PlanarSurface>();
        sptr->setOrigin(origin); 
        sptr->setLenght(length); 

        sptr->updateISurface(isptr);
        sptr->getRawMesh(vlist, tlist);
    }

    void InputSurface::sample(std::shared_ptr<InputSurface> iptr, Eigen::MatrixXd& V, Eigen::MatrixXi& T)
    {
        std::vector<double> vlist;
        std::vector<std::size_t> tlist;

        InputSurface::sample(iptr, vlist, tlist);

        int num_vertices = vlist.size()/3;
        V = Eigen::MatrixXd(num_vertices, 3);
        for (int i = 0; i < num_vertices; ++i)
        {
            V(i, 0) = vlist[3*i + 0];
            V(i, 1) = vlist[3*i + 1];
            V(i, 2) = vlist[3*i + 2];
        }

        int num_triangles = tlist.size()/3;
        T = Eigen::MatrixXi(num_triangles, 3);
        for (int i = 0; i < num_triangles; ++i)
        {
            T(i, 0) = tlist[3*i + 0];
            T(i, 1) = tlist[3*i + 1];
            T(i, 2) = tlist[3*i + 2];
        }
    }

    InputSurface::~InputSurface() = default;

    void InputSurface::translate(const Eigen::Vector3d& displacement)
    {
        translateDomain({displacement[0], displacement[1]});
        translateZ(displacement[2]);
    }

    /* void InputSurface::rotate(double angle_radians, const Eigen::Vector2d& center) */
    /* { */
    /*     translateDomain(-center); */
    /*     transform_dom_.rotate(-angle_radians); */
    /*     translateDomain(center); */
    /* } */

    void InputSurface::rotate(double angle_radians, const Eigen::Vector3d& center)
    {
        translate(-center);
        transform_dom_.rotate(-angle_radians);
        translate(center);
    }


    bool InputSurface::scale(const Eigen::Vector3d& scale, const Eigen::Vector3d& center)
    {
        if ((scale[0] < min_scale_) || (scale[1] < min_scale_) || (scale[2] < min_scale_))
        {
            return false;
        }

        translate(-center);
        scaleDomain({scale[0], scale[1]});
        scaleZ(scale[2]);
        translate(center);

        return true;
    }

    void InputSurface::resetTransform()
    {
        transform_dom_ = Eigen::Affine2d::Identity();
        translateZ_ = 0.;
        scaleZ_ = 1.;
    }

    Eigen::Affine3d InputSurface::getTransform() const
    {
        Eigen::Affine3d transform = Eigen::Affine3d::Identity();

        Eigen::Vector2d t2 = transform_dom_ * Eigen::Vector2d::Zero();
        Eigen::Vector3d t3({-t2[0], -t2[1], translateZ_});

        Eigen::Matrix2d m2 = transform_dom_.linear().inverse();

        transform.matrix().topLeftCorner<2, 2>() = m2;
        transform.matrix()(2, 2) = scaleZ_;
        transform.translate(t3);

        return transform;
    }

    bool InputSurface::operator==(const InputSurface& rhs) const
    {
        bool is_equal = true;

        is_equal &= (transform_dom_.matrix() == rhs.transform_dom_.matrix());
        is_equal &= (translateZ_ == rhs.translateZ_);
        is_equal &= (scaleZ_ == rhs.scaleZ_);
        is_equal &= (min_scale_ == rhs.min_scale_);

        return is_equal;
    }

    bool InputSurface::isEqual(const InputSurface& rhs) const
    {
        bool is_equal = true;
        is_equal &= (transform_dom_.matrix() == rhs.transform_dom_.matrix());
        is_equal &= (translateZ_ == rhs.translateZ_);
        is_equal &= (scaleZ_ == rhs.scaleZ_);
        is_equal &= (min_scale_ == rhs.min_scale_);

        return is_equal;
    }

    Eigen::Vector2d InputSurface::evalDerivative(const Eigen::Vector2d& /* p */) const 
    { 
        return Eigen::Vector2d::Zero();
    }

    Eigen::Affine2d InputSurface::transformDomain() const &&
    {
        return transform_dom_;
    }

    Eigen::Affine2d& InputSurface::transformDomain() &
    {
        return transform_dom_;
    }

    void InputSurface::resetRotateDomain()
    {
        Eigen::Matrix2d R, S;
        transformDomain().computeRotationScaling(&R, &S);
        transformDomain().linear() = S;
    }

    void InputSurface::translateDomain(const Eigen::Vector2d& displacement)
    {
        transform_dom_.translate(-displacement);
    }

    void InputSurface::resetTranslateDomain()
    {
        translateDomain(transformDomain().translation());
    }

    double InputSurface::translateZ() const &&
    {
        return translateZ_;
    }

    double& InputSurface::translateZ(double displacement) &
    {
        if ((-min_scale_ < scaleZ_) && (scaleZ_ < min_scale_))
        {
            translateZ_ += displacement/min_scale_;
        }
        else
        {
            translateZ_ += displacement/scaleZ_;
        }

        return translateZ_;
    }

    bool InputSurface::scaleDomain(double isotropic_scale)
    {
        return scaleDomain(Eigen::Vector2d({isotropic_scale, isotropic_scale}));
    }

    bool InputSurface::scaleDomain(const Eigen::Vector2d& scale)
    {
        if ((scale[0] < min_scale_) || (scale[1] < min_scale_))
        {
            return false;
        }

        transform_dom_.scale(scale.cwiseInverse());
        return true;
    }

    void InputSurface::resetScaleDomain()
    {
        Eigen::Matrix2d R, S;
        transformDomain().computeRotationScaling(&R, &S);
        transformDomain().linear() = R;
    }

    double InputSurface::scaleZ() const &&
    {
        return scaleZ_;
    }

    double& InputSurface::scaleZ(double scale) &
    {
        if (scale < min_scale_)
        {
            scale = min_scale_;
        }
        scaleZ_ *= scale;

        return scaleZ_;
    }

} // namespace stratmod
