#include "catch2/catch.hpp"
#include "glog/logging.h"

#include "detail/type_name.hpp"
#include "stratmod/smodeller.hpp"
#include "stratmod/smodeller2.hpp"
#include "stratmod/surfaces/stratigraphic_surfaces.hpp"

// For Approx() and its literal _a
using namespace Catch::literals;
using namespace stratmod;

static auto class_name = std::string( type_name<decltype(stratmod::SModeller2::Instance())>() );
static auto tag_name = "[" + class_name + "::{saveJSON,loadJSON, saveBinary, loadBinary}]";

SCENARIO( "Testing " + class_name + " save/load of a SmoothInterpolatedSurface", tag_name)
{
    GIVEN( "A model with a SmoothInterpolatedSurface") {

        stratmod::SModeller2& model = stratmod::SModeller2::ClearInstance();

        Eigen::RowVector3d p0({0., 0., 0.});
        Eigen::RowVector3d p1({1., 0., 1.});
        Eigen::RowVector3d p2({0., 1., 2.});

        Eigen::MatrixXd points(3,3); 
        points << p0, p1, p2;

        std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

        // Create surface
        REQUIRE(sptr->set(points, 0.) == true); 

        // Add surface to model
        int surface_id = 0;
        REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);

        std::string filename = "test_smodeller";

        WHEN( "Trying to save model as a JSON file" ) {
            THEN( "The model is saved" ) 
            {
                REQUIRE(model.saveJSON(filename + ".json") == true);
            }
        }
 
        WHEN( "Trying to read model from previously saved JSON file" ) {
            THEN( "The model is loaded" ) 
            {
                REQUIRE(model.loadJSON(filename + ".json") == true);
            }
        }

        WHEN( "Trying to save model as a binary file" ) {
            THEN( "The model is saved" ) 
            {
                REQUIRE(model.saveBinary(filename + ".bin") == true);
            }
        }
 
        WHEN( "Trying to read model from previously saved binary file" ) {
            THEN( "The model is loaded" ) 
            {
                REQUIRE(model.loadBinary(filename + ".bin") == true);
            }
        }
    }

    GIVEN( "A model with an ExtrudedSurface") {

        stratmod::SModeller2& model = SModeller2::ClearInstance();

        Eigen::RowVector2d p0({0., 0.});
        Eigen::RowVector2d p1({1., 0.});

        Eigen::MatrixXd points(2,2); 
        points << p0, p1;

        auto sptr = std::make_shared<stratmod::ExtrudedSurface>();

        // Create surface
        REQUIRE(sptr->setLinearExtrusion(points) == true); 

        // Add surface to model
        int surface_id = 0;
        REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);

        std::string filename = "test_smodeller";

        WHEN( "Trying to save model as a JSON file" ) {
            THEN( "The model is saved" ) 
            {
                REQUIRE(model.saveJSON(filename + ".json") == true);
            }
        }
 
        WHEN( "Trying to read model from previously saved JSON file" ) {
            THEN( "The model is loaded" ) 
            {
                REQUIRE(model.loadJSON(filename + ".json") == true);
            }
        }

        WHEN( "Trying to save model as a binary file" ) {
            THEN( "The model is saved" ) 
            {
                REQUIRE(model.saveBinary(filename + ".bin") == true);
            }
        }
 
        WHEN( "Trying to read model from previously saved binary file" ) {
            THEN( "The model is loaded" ) 
            {
                REQUIRE(model.loadBinary(filename + ".bin") == true);
            }
        }
    }
}
