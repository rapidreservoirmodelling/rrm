/* // For the automatic definition of operators !=, >, <=, >= from ==. < */
/* #include <utility> */
/* using namespace std::rel_ops; */

#include "catch2/catch.hpp"
#include "glog/logging.h"

#include "detail/type_name.hpp"
#include "stratmod/smodeller2.hpp"
#include "stratmod/surfaces/smooth_interpolated_surface.hpp"

// For Approx() and its literal _a
using namespace Catch::literals;
using namespace stratmod;

static auto class_name = std::string( type_name<decltype(stratmod::SModeller2::Instance())>() );
static auto tag_name = "[" + class_name + "::updateSurface]";

SCENARIO( "Testing " + class_name + " update surface", tag_name)
{
    GIVEN( "A model with a surface at index 0") {

        stratmod::SModeller2& model = SModeller2::ClearInstance();
        Eigen::RowVector3d p0({0., 0., 0.});
        Eigen::RowVector3d p1({1., 0., 1.});
        Eigen::RowVector3d p2({0., 1., 2.});

        Eigen::MatrixXd points(3,3); 
        points << p0, p1, p2;

        std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

        // Create surface
        REQUIRE(sptr->set(points, 0.) == true); 

        // Create a copy of original surface
        auto copy_sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>(*sptr);
        REQUIRE(*copy_sptr == *sptr);

        // Add surface to model
        int surface_id = 0;
        REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);

        WHEN( "Trying to get the surface at index 0" ) {
            THEN( "The surface is obtained" ) 
            {
                auto isptr = model.getInputSurface(surface_id);
                REQUIRE(isptr != nullptr);
                REQUIRE(dynamic_cast<stratmod::SmoothInterpolatedSurface&>(*isptr) == *copy_sptr);
            }
        }

        WHEN( "Updating the surface outside of the model" ) {
            THEN( "model must be updated manually" ) 
            {
                sptr->translate({1., 1., 1.});
                model.updateSurface(surface_id);

                auto isptr = model.getInputSurface(surface_id);
                REQUIRE(isptr != nullptr);
                REQUIRE(dynamic_cast<stratmod::SmoothInterpolatedSurface&>(*isptr) == *sptr);
                REQUIRE(dynamic_cast<stratmod::SmoothInterpolatedSurface&>(*isptr) != *copy_sptr);
            }
        }

        WHEN( "Updating the surface in the model" ) {
            THEN( "model is updated automatically" ) 
            {
                copy_sptr->rotate(1.);
                model.updateSurface(surface_id, copy_sptr);

                auto isptr = model.getInputSurface(surface_id);
                REQUIRE(isptr != nullptr);
                REQUIRE(dynamic_cast<stratmod::SmoothInterpolatedSurface&>(*isptr) != *sptr);
                REQUIRE(dynamic_cast<stratmod::SmoothInterpolatedSurface&>(*isptr) == *copy_sptr);
            }
        }
    }
}
