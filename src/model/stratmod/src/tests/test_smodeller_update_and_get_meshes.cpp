#include "catch2/catch.hpp"
#include "glog/logging.h"

#include "detail/type_name.hpp"
#include "stratmod/smodeller2.hpp"
#include "stratmod/surfaces/stratigraphic_surfaces.hpp"

// For Approx() and its literal _a
using namespace Catch::literals;
using namespace stratmod;

static auto class_name = std::string( type_name<decltype(stratmod::SModeller2::Instance())>() );
static auto tag_name = "[" + class_name + "::bunch of stuff]";

/* SCENARIO( "Testing " + class_name + " with pre loaded model", tag_name) */
/* { */
    /* GIVEN( "A model") { */

/*         LOG(WARNING) << "BEGIN TEST"; */
/*         stratmod::SModeller2 model; */
/*         REQUIRE(model.loadJSON("test_run.smod") == true); */

 
/*         WHEN( "Trying to get surfaces from the model" ) { */
/*             THEN( "Something happens" ) */ 
/*             { */
/*                 LOG(WARNING) << "Loading surfaces"; */
/*                 std::vector<double> vlist; */
/*                 std::vector<std::size_t> flist; */
/*                 auto sids = model.getSurfacesIndices(); */
/*                 for (auto i : sids) */
/*                 { */
/*                     model.getCachedMesh(i, vlist, flist); */
/*                 } */
/*             } */
/*         } */

/*         WHEN( "Trying to get surfaces after resizing model" ) { */
/*             THEN( "What happens?" ) */ 
/*             { */
/*                 LOG(WARNING) << "Changing discretization"; */
/*                 model.changeDiscretization(128, 128); */
/*                 std::vector<double> vlist; */
/*                 std::vector<std::size_t> flist; */
/*                 LOG(WARNING) << "Loading modified surfaces"; */
/*                 auto sids = model.getSurfacesIndices(); */
/*                 for (auto i : sids) */
/*                 { */
/*                     model.getCachedMesh(i, vlist, flist); */
/*                 } */
/*             } */
/*         } */
    /* } */
/* } */

SCENARIO( "Testing " + class_name + " PA/PB", tag_name)
{
    GIVEN( "A model") {

        LOG(WARNING) << "BEGIN TEST";
        stratmod::SModeller2& model = SModeller2::Instance();

 
        WHEN( "Trying add surfaces to the model" ) {
            THEN( "Nothing odd should happen" ) 
            {
                LOG(WARNING) << "Loading surfaces";
                Eigen::MatrixXd s1(3,3), s2(3,3), s3(3,3), s4(3,3), s5(3,3);
                
                s1 << 0, 0, 1/4,
                   0, 1, 1/4,
                   1, 0, 1/4;
                
                s2 << 0, 0, 1/2,
                   0, 1, 1/2,
                   1, 0, 1/2;

                s3 << 0, 0, 0,
                   0, 1, 1,
                   1, 0, 1;

                s4 << 0, 0, 1/2,
                   1, 0, 1/2,
                   0, 1, 1/2;

                s4 << 0, 0, 3/4,
                   1, 0, 3/4,
                   0, 1, 3/4;

                model.removeAbove();
                auto sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                sptr->set(s1);
                model.addStratigraphicSurface(1, sptr);

                sptr->set(s2);
                model.addStratigraphicSurface(2, sptr);

                sptr->set(s3);
                model.addStratigraphicSurface(3, sptr);

                model.preserveAbove({3});

                sptr->set(s4);
                model.addStratigraphicSurface(4, sptr);

                sptr->set(s5);
                model.addStratigraphicSurface(5, sptr);


                std::vector<double> vlist;
                std::vector<std::size_t> flist;
                auto sids = model.getSurfacesIndices();
                for (auto i : sids)
                {
                    model.getCachedMesh(i, vlist, flist);
                }
            }
        }

        /* WHEN( "Trying to get surfaces after resizing model" ) { */
        /*     THEN( "What happens?" ) */ 
        /*     { */
        /*         LOG(WARNING) << "Changing discretization"; */
        /*         model.changeDiscretization(128, 128); */
        /*         std::vector<double> vlist; */
        /*         std::vector<std::size_t> flist; */
        /*         LOG(WARNING) << "Loading modified surfaces"; */
        /*         auto sids = model.getSurfacesIndices(); */
        /*         for (auto i : sids) */
        /*         { */
        /*             model.getCachedMesh(i, vlist, flist); */
        /*         } */
        /*     } */
        /* } */
    }
}
