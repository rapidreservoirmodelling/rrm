#include "catch2/catch.hpp"
#include "glog/logging.h"

#include "detail/type_name.hpp"
#include "detail/eigen_catch2_support.hpp"
#include "stratmod/surfaces/smooth_interpolated_surface.hpp"

// For Approx() and its literal _a
using namespace Catch::literals;

static auto class_name = std::string( type_name<decltype(stratmod::SmoothInterpolatedSurface())>() );
static auto tag_name = "[" + class_name + "]";

SCENARIO( "Testing " + class_name + " surface creation", tag_name)
{
    GIVEN( "An empty surface") {

        stratmod::SmoothInterpolatedSurface surface;

        WHEN( "Trying to create a surface with less than 3 points" ) {
            THEN( "The surface won't be created" )
            {
                Eigen::MatrixXd one_point = Eigen::MatrixXd::Zero(1,3);
                REQUIRE(surface.set(one_point) == false);

                Eigen::MatrixXd two_points = Eigen::MatrixXd::Zero(2,3);
                REQUIRE(surface.set(two_points) == false);
            }
        }

        WHEN( "Trying to create a surface from a list of points with the wrong dimension" ) {
            THEN( "The surface won't be created" )
            {
                Eigen::MatrixXd points(3,4);
                points <<
                    0., 1., 2., 3.,
                    0., 0., 1., 0.,
                    0., 0., 0., 1.;

                REQUIRE(surface.set(points) == false);
            }
        }

        WHEN( "Inserting 3 colinear points" ) {
            THEN( "The surface won't be created" )
            {
                Eigen::MatrixXd points(3,3);
                points <<
                    0., 0., 0.,
                    1., 0., 0.,
                    2., 0., 0.;

                REQUIRE(surface.set(points) == false);
            }
        }

        WHEN( "Inserting 3 coplanar points" ) {
            THEN( "A plane containing the 3 points is created" ) 
            {
                Eigen::RowVector3d p0({0., 0., 0.});
                Eigen::RowVector3d p1({1., 0., 1.});
                Eigen::RowVector3d p2({0., 1., 2.});

                Eigen::MatrixXd points(3,3); 
                points << p0, p1, p2;

                // Create surface
                REQUIRE(surface.set(points, 0.) == true); 

                // Check if p0, p1, p2 lie in surface
                CHECK(surface({p0[0], p0[1]}) == Approx(p0[2]));
                CHECK(surface({p1[0], p1[1]}) == Approx(p1[2]));
                CHECK(surface({p2[0], p2[1]}) == Approx(p2[2]));

                // Check if p0, p1, p2 triangle's baricenter lies in surface
                Eigen::RowVector3d baricenter = (p0 + p1 + p2)/3;
                CHECK(surface({baricenter[0], baricenter[1]}) == Approx(baricenter[2]));
            }
        }
    }
}

SCENARIO( "Testing " + class_name + " height computation", tag_name)
{
    GIVEN( "A surface that interpolates a plane") {

        stratmod::SmoothInterpolatedSurface surface;

        Eigen::RowVector3d p0({0., 0., 0.});
        Eigen::RowVector3d p1({1., 0., 1.});
        Eigen::RowVector3d p2({0., 1., 2.});

        Eigen::MatrixXd points(3,3); 
        points << p0, p1, p2;

        // Create surface
        REQUIRE(surface.set(points) == true); 

        WHEN( "Computing heights after translating surface S(x, y) by vector `t`" ) {
            THEN( "Surface heights change according to `S(x - t[0], y - t[1]) + t[2]`" )
            {
                Eigen::Vector3d t({1., 1./2., -1.});
                auto transformed_surface = surface;
                transformed_surface.translate(t);

                Eigen::Vector2d q0({p0[0], p0[1]});
                Eigen::Vector2d q1({p1[0], p1[1]});
                Eigen::Vector2d q2({p2[0], p2[1]});
                Eigen::Vector2d td({t[0], t[1]});

                REQUIRE(transformed_surface(q0) == surface(q0 - td) + t[2]);
                REQUIRE(transformed_surface(q1) == surface(q1 - td) + t[2]);
                REQUIRE(transformed_surface(q2) == surface(q2 - td) + t[2]);
            }
        }

        WHEN( "Computing heights after rotating surface S(x, y) by angle `-a` (radians)" ) {
            THEN( "Surface heights change according to S(x*cos(a) + y*sin(a), -x*sin(a) + y*cos(a)" )
            {
                constexpr auto kPI = 3.14159265358979323846;
                double a = kPI/2;
                auto transformed_surface = surface;
                transformed_surface.rotate(a);

                Eigen::Vector2d q0({p0[0], p0[1]});
                Eigen::Vector2d q1({p1[0], p1[1]});
                Eigen::Vector2d q2({p2[0], p2[1]});

                double cosa = std::cos(a);
                double sina = std::sin(a);

                REQUIRE(transformed_surface(q0) == surface({q0[0]*cosa + q0[1]*sina, -q0[0]*sina + q0[1]*cosa}));
                REQUIRE(transformed_surface(q1) == surface({q1[0]*cosa + q1[1]*sina, -q1[0]*sina + q1[1]*cosa}));
                REQUIRE(transformed_surface(q2) == surface({q2[0]*cosa + q2[1]*sina, -q2[0]*sina + q2[1]*cosa}));
            }
        }

        WHEN( "Computing heights after scaling surface S(x, y) by scale vector `s`" ) {
            THEN( "Surface heights change according to s[2]*S(x/s[0], y/s[1])" )
            {
                Eigen::Vector3d s({2., 1./2., 3});
                auto transformed_surface = surface;
                transformed_surface.scale(s);

                Eigen::Vector2d q0({p0[0], p0[1]});
                Eigen::Vector2d q1({p1[0], p1[1]});
                Eigen::Vector2d q2({p2[0], p2[1]});

                REQUIRE(transformed_surface(q0) == s[2]*surface({1./s[0]*q0[0], 1./s[1]*q0[1]}));
                REQUIRE(transformed_surface(q1) == s[2]*surface({1./s[0]*q1[0], 1./s[1]*q1[1]}));
                REQUIRE(transformed_surface(q2) == s[2]*surface({1./s[0]*q2[0], 1./s[1]*q2[1]}));
            }
        }
    }
}

SCENARIO( "Testing " + class_name + " derivative computation", tag_name)
{
    GIVEN( "A surface that interpolates a plane") {

        stratmod::SmoothInterpolatedSurface surface;

        Eigen::RowVector3d p0({0., 0., 0.});
        Eigen::RowVector3d p1({1., 0., 1.});
        Eigen::RowVector3d p2({0., 1., 2.});

        Eigen::MatrixXd points(3,3); 
        points << p0, p1, p2;

        // Create surface
        REQUIRE(surface.set(points) == true); 

        WHEN( "Querying whether the surface is differentiable" ) {
            THEN( "The surface is differentiable" )
            {
                REQUIRE(surface.differentiable() == true);
            }
        }

        WHEN( "Computing derivatives" ) {
            THEN( "The derivatives are colinear to the plane's normal" ) 
            {
                Eigen::Vector3d plane_normal = (p1 - p0).cross(p2 - p0).transpose().normalized();
                Eigen::Vector3d surface_normal;

                auto derivative = surface.derivative({p0[0], p0[1]});
                surface_normal = Eigen::Vector3d({-(*derivative)[0], -(*derivative)[1], 1}).normalized();
                REQUIRE( plane_normal.cross(surface_normal).norm() == 0.0_a );

                derivative = surface.derivative({p1[0], p1[1]});
                surface_normal = Eigen::Vector3d({-(*derivative)[0], -(*derivative)[1], 1}).normalized();
                REQUIRE( plane_normal.cross(surface_normal).norm() == 0.0_a );

                derivative = surface.derivative({p2[0], p2[1]});
                surface_normal = Eigen::Vector3d({-(*derivative)[0], -(*derivative)[1], 1}).normalized();
                REQUIRE( plane_normal.cross(surface_normal).norm() == 0.0_a );
            }
        }

        WHEN( "Computing derivatives after translating S(x, y) by vector `t`" ) {
            THEN( "D(SoT)(T(x,y)) changes according to DS(T(x,y)), T(x,y) = (x - t[0], y - t[1])" )
            {
                Eigen::Vector3d t({1., 1./2., -1.});
                auto transformed_surface = surface;
                transformed_surface.translate(t);

                Eigen::Vector2d q0({p0[0], p0[1]});
                Eigen::Vector2d q1({p1[0], p1[1]});
                Eigen::Vector2d q2({p2[0], p2[1]});
                Eigen::Vector2d td({t[0], t[1]});

                REQUIRE(transformed_surface.derivative(q0).value() == surface.derivative(q0 - td).value());
                REQUIRE(transformed_surface.derivative(q1).value() == surface.derivative(q1 - td).value());
                REQUIRE(transformed_surface.derivative(q2).value() == surface.derivative(q2 - td).value());
            }
        }

        WHEN( "Computing derivatives after rotating S(x, y) by angle `-a` (radians)" ) {
            THEN( "D(SoT)(T(x,y)) changes according to T^t * DS(T(x,y)), T(x,y) = (x*cos(a) + y*sin(a), -x*sin(a) + y*cos(a)" )
            {
                constexpr auto kPI = 3.14159265358979323846;
                double a = kPI/2;
                auto transformed_surface = surface;
                transformed_surface.rotate(a);

                Eigen::Vector2d q0({p0[0], p0[1]});
                Eigen::Vector2d q1({p1[0], p1[1]});
                Eigen::Vector2d q2({p2[0], p2[1]});

                double cosa = std::cos(a);
                double sina = std::sin(a);

                Eigen::Matrix2d T;
                T << cosa, sina, 
                    -sina, cosa;

                REQUIRE(transformed_surface.derivative(q0).value() == T.transpose() * surface.derivative(T * q0).value());
                REQUIRE(transformed_surface.derivative(q1).value() == T.transpose() * surface.derivative(T * q1).value());
                REQUIRE(transformed_surface.derivative(q2).value() == T.transpose() * surface.derivative(T * q2).value());
            }
        }

        WHEN( "Computing derivatives after scaling S(x, y) by scale vector `s`" ) {
            THEN( "D(SoT)(T(x,y)) changes according to T^t * s[2]*S(T(x, y)), T(x,y) = (x/s[0], y/s[1])" )
            {
                Eigen::Vector3d scale({2., 1./2., 3.});
                surface.scale(scale);
                auto transformed_surface = surface;
                transformed_surface.scale(scale);

                Eigen::Vector2d q0({p0[0], p0[1]});
                Eigen::Vector2d q1({p1[0], p1[1]});
                Eigen::Vector2d q2({p2[0], p2[1]});

                Eigen::Matrix2d T;
                T << 1./scale[0], 0., 
                     0.         , 1./scale[1];

                REQUIRE(transformed_surface.derivative(q0).value() == T.transpose() * scale[2]*surface.derivative(T * q0).value());
                REQUIRE(transformed_surface.derivative(q1).value() == T.transpose() * scale[2]*surface.derivative(T * q1).value());
                REQUIRE(transformed_surface.derivative(q2).value() == T.transpose() * scale[2]*surface.derivative(T * q2).value());
            }
        }
    }
}

SCENARIO( "Testing " + class_name + " transforms", tag_name)
{
    GIVEN( "A point `p` in a surface") {

        // Tolerance
        double tolerance = 1E-6;
        Eigen::Matrix4d tol = tolerance * Eigen::Matrix4d::Ones();

        // Define 3 coplanar points
        Eigen::RowVector3d p0({0., 0., 0.});
        Eigen::RowVector3d p1({1., 0., 1.});
        Eigen::RowVector3d p2({0., 1., 2.});

        Eigen::MatrixXd points(3,3); 
        points << p0, p1, p2;

        Eigen::Vector3d p = (p0 + p1 + p2).transpose()/3.;

        // Create surface
        stratmod::SmoothInterpolatedSurface surface;
        REQUIRE(surface.set(points) == true); 

        // Point `p` lies in surface
        REQUIRE(surface({p[0], p[1]}) == Approx(p[2]).margin(tolerance));

        // Eigen affine transform
        Eigen::Affine3d T = Eigen::Affine3d::Identity();

        WHEN( "Composing translation, rotation and scalling" ) {
            THEN( "The surface transforms accordingly to an Eigen::Affine3d transform operating on `p`" )
            {
                REQUIRE(surface.getTransform().matrix() == T.matrix());

                // Translation vectors
                Eigen::Vector3d t1({+1., +1./2., -1.});
                Eigen::Vector3d t2({-1., -1./2., +1.});

                // Angles
                constexpr auto kPI = 3.14159265358979323846;
                double a1 = +kPI/2;
                double a2 = -kPI/2;

                // Scale vectors
                Eigen::Vector3d s1({2., 1./3., 5.});
                Eigen::Vector3d s2({1./2., 3., 1./5.});

                surface.translate(t1);
                T = Eigen::Translation3d(t1[0], t1[1], t1[2]) * T;
                require_eq(surface.getTransform().matrix(), T.matrix(), tolerance);
                {
                    Eigen::Vector3d Tp = T*p;
                    CAPTURE(surface.getTransform().matrix());
                    CAPTURE(T.matrix());
                    REQUIRE(surface({Tp[0], Tp[1]}) == Approx(Tp[2]).margin(tolerance));
                }

                surface.rotate(a1);
                T = Eigen::AngleAxis<double>(a1, Eigen::Vector3d::UnitZ()) * T;
                require_eq(surface.getTransform().matrix(), T.matrix(), tolerance);
                {
                    Eigen::Vector3d Tp = T*p;
                    CAPTURE(surface.getTransform().matrix());
                    CAPTURE(T.matrix());
                    REQUIRE(surface({Tp[0], Tp[1]}) == Approx(Tp[2]).margin(tolerance));
                }

                surface.scale(s1);
                T = Eigen::Scaling(s1[0], s1[1], s1[2]) * T;
                require_eq(surface.getTransform().matrix(), T.matrix(), tolerance);
                {
                    Eigen::Vector3d Tp = T*p;
                    CAPTURE(surface.getTransform().matrix());
                    CAPTURE(T.matrix());
                    REQUIRE(surface({Tp[0], Tp[1]}) == Approx(Tp[2]).margin(tolerance));
                }

                surface.translate(t2);
                T.pretranslate(t2);
                require_eq(surface.getTransform().matrix(), T.matrix(), tolerance);
                {
                    Eigen::Vector3d Tp = T*p;
                    CAPTURE(surface.getTransform().matrix());
                    CAPTURE(T.matrix());
                    REQUIRE( surface({Tp[0], Tp[1]}) == Approx(Tp[2]).margin(tolerance) );
                }

                surface.rotate(a2);
                T.prerotate(Eigen::AngleAxisd(a2, Eigen::Vector3d::UnitZ()));
                require_eq(surface.getTransform().matrix(), T.matrix(), tolerance);
                {
                    Eigen::Vector3d Tp = T*p;
                    CAPTURE(surface.getTransform().matrix());
                    CAPTURE(T.matrix());
                    REQUIRE( surface({Tp[0], Tp[1]}) == Approx(Tp[2]).margin(tolerance) );
                }

                surface.scale(s2);
                T.prescale(s2);
                require_eq(surface.getTransform().matrix(), T.matrix(), tolerance);
                {
                    Eigen::Vector3d Tp = T*p;
                    CAPTURE(surface.getTransform().matrix());
                    CAPTURE(T.matrix());
                    REQUIRE( surface({Tp[0], Tp[1]}) == Approx(Tp[2]).margin(tolerance) );
                }

            }
        }
    }
}
