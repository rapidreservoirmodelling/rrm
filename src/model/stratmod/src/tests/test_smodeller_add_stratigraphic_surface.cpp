#include "catch2/catch.hpp"
#include "glog/logging.h"

#include "detail/type_name.hpp"
#include "stratmod/smodeller2.hpp"
#include "stratmod/surfaces/smooth_interpolated_surface.hpp"

// For Approx() and its literal _a
using namespace Catch::literals;

static auto class_name = std::string( type_name<decltype(stratmod::SModeller2::Instance())>() );
static auto tag_name = "[" + class_name + "::addStratigraphicSurface]";

SCENARIO( "Testing " + class_name + " stratigraphic surface creation", tag_name)
{
    GIVEN( "An empty model") {

        stratmod::SModeller2& model = stratmod::SModeller2::ClearInstance();

        WHEN( "Creating a surface with 3 coplanar points" ) {
            THEN( "A plane containing the 3 points is created" ) 
            {
                Eigen::RowVector3d p0({0., 0., 0.});
                Eigen::RowVector3d p1({1., 0., 1.});
                Eigen::RowVector3d p2({0., 1., 2.});

                Eigen::MatrixXd points(3,3); 
                points << p0, p1, p2;

                std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                // Create surface
                REQUIRE(sptr->set(points, 0.) == true); 

                // Add surface to model
                int surface_id = 0;
                REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);
            }
        }

        WHEN( "Creating 3 surfaces with Remove-Above" ) {

            model.removeAbove();
            THEN( "The 3 surfaces are correctly added to the model" ) 
            {
                {
                    Eigen::RowVector3d p0({0., 0., 0.});
                    Eigen::RowVector3d p1({1., 0., 1.});
                    Eigen::RowVector3d p2({0., 1., 2.});

                    Eigen::MatrixXd points(3,3); 
                    points << p0, p1, p2;

                    std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                    // Create surface
                    REQUIRE(sptr->set(points, 0.) == true); 

                    // Add surface to model
                    int surface_id = 0;
                    REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);
                }
                {
                    Eigen::RowVector3d p0({0., 0., 1./2.});
                    Eigen::RowVector3d p1({1., 0., 1./2.});
                    Eigen::RowVector3d p2({0., 1., 1./2.});

                    Eigen::MatrixXd points(3,3); 
                    points << p0, p1, p2;

                    std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                    // Create surface
                    REQUIRE(sptr->set(points, 0.) == true); 

                    // Add surface to model
                    int surface_id = 1;
                    REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);
                }
                {
                    Eigen::RowVector3d p0({0., 0., 1.});
                    Eigen::RowVector3d p1({1., 0., 1.});
                    Eigen::RowVector3d p2({0., 1., 1.});

                    Eigen::MatrixXd points(3,3); 
                    points << p0, p1, p2;

                    std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                    // Create surface
                    REQUIRE(sptr->set(points, 0.) == true); 

                    // Add surface to model
                    int surface_id = 2;
                    REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);
                }
            }
        }

        WHEN( "Creating 3 surfaces with Remove-Above-Intersection" ) {

            model.removeAboveIntersection();
            THEN( "The 3 surfaces are correctly added to the model" ) 
            {
                {
                    Eigen::RowVector3d p0({0., 0., 0.});
                    Eigen::RowVector3d p1({1., 0., 1.});
                    Eigen::RowVector3d p2({0., 1., 2.});

                    Eigen::MatrixXd points(3,3); 
                    points << p0, p1, p2;

                    std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                    // Create surface
                    REQUIRE(sptr->set(points, 0.) == true); 

                    // Add surface to model
                    int surface_id = 0;
                    REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);
                }
                {
                    Eigen::RowVector3d p0({0., 0., 1./2.});
                    Eigen::RowVector3d p1({1., 0., 1./2.});
                    Eigen::RowVector3d p2({0., 1., 1./2.});

                    Eigen::MatrixXd points(3,3); 
                    points << p0, p1, p2;

                    std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                    // Create surface
                    REQUIRE(sptr->set(points, 0.) == true); 

                    // Add surface to model
                    int surface_id = 1;
                    REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);
                }
                {
                    Eigen::RowVector3d p0({0., 0., 1.});
                    Eigen::RowVector3d p1({1., 0., 1.});
                    Eigen::RowVector3d p2({0., 1., 1.});

                    Eigen::MatrixXd points(3,3); 
                    points << p0, p1, p2;

                    std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                    // Create surface
                    REQUIRE(sptr->set(points, 0.) == true); 

                    // Add surface to model
                    int surface_id = 2;
                    REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);
                }
            }
        }

        WHEN( "Creating 3 surfaces with Remove-Below" ) {

            model.removeBelow();
            THEN( "The 3 surfaces are correctly added to the model" ) 
            {
                {
                    Eigen::RowVector3d p0({0., 0., 0.});
                    Eigen::RowVector3d p1({1., 0., 1.});
                    Eigen::RowVector3d p2({0., 1., 2.});

                    Eigen::MatrixXd points(3,3); 
                    points << p0, p1, p2;

                    std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                    // Create surface
                    REQUIRE(sptr->set(points, 0.) == true); 

                    // Add surface to model
                    int surface_id = 0;
                    REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);
                }
                {
                    Eigen::RowVector3d p0({0., 0., 1./2.});
                    Eigen::RowVector3d p1({1., 0., 1./2.});
                    Eigen::RowVector3d p2({0., 1., 1./2.});

                    Eigen::MatrixXd points(3,3); 
                    points << p0, p1, p2;

                    std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                    // Create surface
                    REQUIRE(sptr->set(points, 0.) == true); 

                    // Add surface to model
                    int surface_id = 1;
                    REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);
                }
                {
                    Eigen::RowVector3d p0({0., 0., 1.});
                    Eigen::RowVector3d p1({1., 0., 1.});
                    Eigen::RowVector3d p2({0., 1., 1.});

                    Eigen::MatrixXd points(3,3); 
                    points << p0, p1, p2;

                    std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                    // Create surface
                    REQUIRE(sptr->set(points, 0.) == true); 

                    // Add surface to model
                    int surface_id = 2;
                    REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);
                }
            }
        }

        WHEN( "Creating 3 surfaces with Remove-Below-Intersection" ) {

            model.removeBelowIntersection();
            THEN( "The 3 surfaces are correctly added to the model" ) 
            {
                {
                    Eigen::RowVector3d p0({0., 0., 0.});
                    Eigen::RowVector3d p1({1., 0., 1.});
                    Eigen::RowVector3d p2({0., 1., 2.});

                    Eigen::MatrixXd points(3,3); 
                    points << p0, p1, p2;

                    std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                    // Create surface
                    REQUIRE(sptr->set(points, 0.) == true); 

                    // Add surface to model
                    int surface_id = 0;
                    REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);
                }
                {
                    Eigen::RowVector3d p0({0., 0., 1./2.});
                    Eigen::RowVector3d p1({1., 0., 1./2.});
                    Eigen::RowVector3d p2({0., 1., 1./2.});

                    Eigen::MatrixXd points(3,3); 
                    points << p0, p1, p2;

                    std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                    // Create surface
                    REQUIRE(sptr->set(points, 0.) == true); 

                    // Add surface to model
                    int surface_id = 1;
                    REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);
                }
                {
                    Eigen::RowVector3d p0({0., 0., 1.});
                    Eigen::RowVector3d p1({1., 0., 1.});
                    Eigen::RowVector3d p2({0., 1., 1.});

                    Eigen::MatrixXd points(3,3); 
                    points << p0, p1, p2;

                    std::shared_ptr<stratmod::SmoothInterpolatedSurface> sptr = std::make_shared<stratmod::SmoothInterpolatedSurface>();

                    // Create surface
                    REQUIRE(sptr->set(points, 0.) == true); 

                    // Add surface to model
                    int surface_id = 2;
                    REQUIRE(model.addStratigraphicSurface(surface_id, sptr) == true);
                }
            }
        }
 
    }
}
