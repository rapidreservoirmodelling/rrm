#include "catch2/catch.hpp"
#include "glog/logging.h"

#include "detail/type_name.hpp"
#include "detail/eigen_catch2_support.hpp"
#include "stratmod/surfaces/triangle_surface.hpp"

// For Approx() and its literal _a
using namespace Catch::literals;

static auto class_name = std::string( type_name<decltype(stratmod::TriangleSurface())>() );
static auto tag_name = "[" + class_name + "]";

SCENARIO( "Testing " + class_name + " surface creation", tag_name)
{
    GIVEN( "An empty surface and a tolerance margin") {

        stratmod::TriangleSurface surface;
        double tolerance = 1E-6;

        /* WHEN( "Trying to create a surface with less than 2 points" ) { */
        /*     THEN( "The surface won't be created" ) */
        /*     { */
        /*         Eigen::MatrixXd one_point = Eigen::MatrixXd::Zero(1,2); */
        /*         REQUIRE(surface.setLinearExtrusion(one_point) == false); */
        /*     } */
        /* } */

        /* WHEN( "Trying to create a surface from a list of points with the wrong dimension" ) { */
        /*     THEN( "The surface won't be created" ) */
        /*     { */
        /*         Eigen::MatrixXd points(2,4); */
        /*         points << */
        /*             0., 1., 2., 3., */
        /*             0., 0., 1., 2.; */

        /*         REQUIRE(surface.setLinearExtrusion(points) == false); */
        /*     } */
        /* } */

        /* WHEN( "Inserting 3 colinear points" ) { */
        /*     THEN( "The surface won't be created" ) */
        /*     { */
        /*         Eigen::MatrixXd points(3,3); */
        /*         points << */
        /*             0., 0., 0., */
        /*             1., 0., 0., */
        /*             2., 0., 0.; */

        /*         REQUIRE(surface.set(points) == false); */
        /*     } */
        /* } */
        WHEN( "Creating a surface composed of one triangle" ) {
            THEN( "The surface is created and heights are evaluated properly" )
            {
                Eigen::RowVector3d p0({0., 0., 0.});
                Eigen::RowVector3d p1({1., 0., 1.});
                Eigen::RowVector3d p2({0., 1., 2.});

                Eigen::MatrixXd points(3,3); 
                points << p0, p1, p2;

                Eigen::MatrixXi triangle(1,3);
                triangle << 0, 1, 2;

                // Create surface
                REQUIRE(surface.set(points, triangle) == true); 

                // Check if p0, p1, p2 lie in surface
                CHECK(surface({p0[0], p0[1]}) == Approx(p0[2]));
                CHECK(surface({p1[0], p1[1]}) == Approx(p1[2]));
                CHECK(surface({p2[0], p2[1]}) == Approx(p2[2]));

                // Check if p0, p1, p2 triangle's baricenter lies in surface
                Eigen::RowVector3d baricenter = (p0 + p1 + p2)/3;
                CHECK(surface({baricenter[0], baricenter[1]}) == Approx(baricenter[2]));
            }
        }
    }
}

/* SCENARIO( "Testing " + class_name + " height computation", tag_name) */
/* { */
/*     GIVEN( "A surface that interpolates a plane") { */

/*         stratmod::SmoothInterpolatedSurface surface; */

/*         Eigen::RowVector3d p0({0., 0., 0.}); */
/*         Eigen::RowVector3d p1({1., 0., 1.}); */
/*         Eigen::RowVector3d p2({0., 1., 2.}); */

/*         Eigen::MatrixXd points(3,3); */ 
/*         points << p0, p1, p2; */

/*         // Create surface */
/*         REQUIRE(surface.set(points) == true); */ 

/*         WHEN( "Computing heights after translating surface S(x, y) by vector `t`" ) { */
/*         } */

/*         WHEN( "Computing heights after rotating surface S(x, y) by angle `-a` (radians)" ) { */
/*         } */

/*         WHEN( "Computing heights after scaling surface S(x, y) by scale vector `s`" ) { */
/*         } */
/*     } */
/* } */

/* SCENARIO( "Testing " + class_name + " derivative computation", tag_name) */
/* { */
/*     GIVEN( "A surface that interpolates a plane") { */

/*         stratmod::ExtrudedSurface surface; */

/*         Eigen::RowVector3d p0({0., 0., 0.}); */
/*         Eigen::RowVector3d p1({1., 0., 1.}); */
/*         Eigen::RowVector3d p2({0., 1., 2.}); */

/*         Eigen::MatrixXd points(3,3); */ 
/*         points << p0, p1, p2; */

/*         // Create surface */
/*         REQUIRE(surface.set(points) == true); */ 

/*         WHEN( "Querying whether the surface is differentiable" ) { */
/*             THEN( "The surface is differentiable" ) */
/*             { */
/*                 REQUIRE(surface.differentiable() == true); */
/*             } */
/*         } */

/*         WHEN( "Computing derivatives" ) { */
/*             THEN( "The derivatives are colinear to the plane's normal" ) */ 
/*             { */
/*                 Eigen::Vector3d plane_normal = (p1 - p0).cross(p2 - p0).transpose().normalized(); */
/*                 Eigen::Vector3d surface_normal; */

/*                 auto derivative = surface.derivative({p0[0], p0[1]}); */
/*                 surface_normal = Eigen::Vector3d({-(*derivative)[0], -(*derivative)[1], 1}).normalized(); */
/*                 REQUIRE( plane_normal.cross(surface_normal).norm() == 0.0_a ); */

/*                 derivative = surface.derivative({p1[0], p1[1]}); */
/*                 surface_normal = Eigen::Vector3d({-(*derivative)[0], -(*derivative)[1], 1}).normalized(); */
/*                 REQUIRE( plane_normal.cross(surface_normal).norm() == 0.0_a ); */

/*                 derivative = surface.derivative({p2[0], p2[1]}); */
/*                 surface_normal = Eigen::Vector3d({-(*derivative)[0], -(*derivative)[1], 1}).normalized(); */
/*                 REQUIRE( plane_normal.cross(surface_normal).norm() == 0.0_a ); */
/*             } */
/*         } */
/*     } */
/* } */
