#include "catch2/catch.hpp"
#include "glog/logging.h"

#include "detail/type_name.hpp"
#include "detail/eigen_catch2_support.hpp"
#include "stratmod/surfaces/extruded_surface.hpp"

// For Approx() and its literal _a
using namespace Catch::literals;

static auto class_name = std::string( type_name<decltype(stratmod::ExtrudedSurface())>() );
static auto tag_name = "[" + class_name + "]";

SCENARIO( "Testing " + class_name + " surface creation", tag_name)
{
    GIVEN( "An empty surface and a tolerance margin") {

        stratmod::ExtrudedSurface surface;
        double tolerance = 1E-6;

        WHEN( "Trying to create a surface with less than 2 points" ) {
            THEN( "The surface won't be created" )
            {
                Eigen::MatrixXd one_point = Eigen::MatrixXd::Zero(1,2);
                REQUIRE(surface.setLinearExtrusion(one_point) == false);
            }
        }

        WHEN( "Trying to create a surface from a list of points with the wrong dimension" ) {
            THEN( "The surface won't be created" )
            {
                Eigen::MatrixXd points(2,4);
                points <<
                    0., 1., 2., 3.,
                    0., 0., 1., 2.;

                REQUIRE(surface.setLinearExtrusion(points) == false);
            }
        }

        /* WHEN( "Inserting 3 colinear points" ) { */
        /*     THEN( "The surface won't be created" ) */
        /*     { */
        /*         Eigen::MatrixXd points(3,3); */
        /*         points << */
        /*             0., 0., 0., */
        /*             1., 0., 0., */
        /*             2., 0., 0.; */

        /*         REQUIRE(surface.set(points) == false); */
        /*     } */
        /* } */

        WHEN( "Inserting 2 colinear points" ) {
            THEN( "A plane linearly extruded from the line containing the 2 points is created" ) 
            {
                Eigen::RowVector2d p0({0., 0.});
                Eigen::RowVector2d p1({1., 0.});

                Eigen::MatrixXd points(2,2); 
                points << p0, p1;

                // Create surface
                REQUIRE(surface.setLinearExtrusion(points) == true); 

                // Check if p0, p1 lie in surface
                CHECK(surface({p0[0], 0.}) == Approx(p0[1]));
                CHECK(surface({p1[0], 0.}) == Approx(p1[1]));

                // Check if p0, p1 segment's baricenter lies in surface
                Eigen::RowVector2d baricenter = (p0 + p1)/2;
                CHECK(surface({baricenter[0], 0.}) == Approx(baricenter[1]));
            }
        }

        WHEN( "Setting the cross-section origin" ) {
            THEN( "It is properly set with the tolerance margin" )
            {
                // Set origin
                Eigen::Vector2d origin({1./2., 0.});
                surface.setCrossSectionOrigin(origin);

                // Get origin and compare
                Eigen::Vector2d sorigin = surface.getCrossSectionOrigin();
                require_eq(sorigin, origin, tolerance);
            }
        }

        WHEN( "Setting the cross-section origin multiple times" ) {
            THEN( "The last value is set withinh the tolerance margin" )
            {
                // Set origin
                Eigen::Vector2d origin0({1./2., 0.});
                surface.setCrossSectionOrigin(origin0);

                // Get origin0 and compare
                Eigen::Vector2d sorigin0 = surface.getCrossSectionOrigin();
                require_eq(sorigin0, origin0, tolerance);

                // Set origin
                Eigen::Vector2d origin1({0., 1./2.});
                surface.setCrossSectionOrigin(origin1);

                // Get origin1 and compare
                Eigen::Vector2d sorigin1 = surface.getCrossSectionOrigin();
                require_eq(sorigin1, origin1, tolerance);

                // Set origin
                Eigen::Vector2d origin2({1., 1.});
                surface.setCrossSectionOrigin(origin2);

                // Get origin2 and compare
                Eigen::Vector2d sorigin2 = surface.getCrossSectionOrigin();
                require_eq(sorigin2, origin2, tolerance);
            }
        }

        WHEN( "Setting the cross-section direction multiple times" ) {
            THEN( "The last value is set withinh the tolerance margin" )
            {
                // Set origin
                Eigen::Vector2d direction0 = Eigen::Vector2d({1., 0.}).normalized();
                surface.setCrossSectionOrigin(direction0);

                // Get direction0 and compare
                Eigen::Vector2d sdirection0 = surface.getCrossSectionOrigin();
                require_eq(sdirection0, direction0, tolerance);

                // Set origin
                Eigen::Vector2d direction1 = Eigen::Vector2d({0., 1.}).normalized();
                surface.setCrossSectionOrigin(direction1);

                // Get direction1 and compare
                Eigen::Vector2d sdirection1 = surface.getCrossSectionOrigin();
                require_eq(sdirection1, direction1, tolerance);

                // Set origin
                Eigen::Vector2d direction2 = Eigen::Vector2d({-1., 0.}).normalized();
                surface.setCrossSectionOrigin(direction2);

                // Get direction2 and compare
                Eigen::Vector2d sdirection2 = surface.getCrossSectionOrigin();
                require_eq(sdirection2, direction2, tolerance);
            }
        }

        WHEN( "Setting the cross-section direction" ) {
            THEN( "It is properly set within the tolerance margin" )
            {
                // Set direction
                Eigen::Vector2d direction = Eigen::Vector2d({1., 1.}).normalized();
                REQUIRE(surface.setCrossSectionDirection(direction) == true);

                // Get direction and compare
                Eigen::Vector2d sdirection = surface.getCrossSectionDirection();
                require_eq(sdirection, direction, tolerance);
            }
        }

        WHEN( "Setting the cross-section's: 1) origin, and then 2) direction" ) {
            THEN( "The cross-section is properly set regardless of the order of operations" )
            {
                Eigen::Vector2d origin({1./2., 0.});
                Eigen::Vector2d direction = Eigen::Vector2d({1., 1.}).normalized();

                surface.setCrossSectionOrigin(origin);
                REQUIRE(surface.setCrossSectionDirection(direction) == true);

                Eigen::Vector2d sorigin = surface.getCrossSectionOrigin();
                require_eq(sorigin, origin, tolerance);

                Eigen::Vector2d sdirection = surface.getCrossSectionDirection();
                require_eq(sdirection, direction, tolerance);
            }
        }

        WHEN( "Setting the cross-section's: 1) direction, and then 2) origin" ) {
            THEN( "The cross-section is properly set regardless of the order of operations" )
            {
                Eigen::Vector2d origin({1./2., 0.});
                Eigen::Vector2d direction = Eigen::Vector2d({1., 1.}).normalized();

                REQUIRE(surface.setCrossSectionDirection(direction) == true);
                surface.setCrossSectionOrigin(origin);

                Eigen::Vector2d sorigin = surface.getCrossSectionOrigin();
                require_eq(sorigin, origin, tolerance);

                Eigen::Vector2d sdirection = surface.getCrossSectionDirection();
                require_eq(sdirection, direction, tolerance);
            }
        }
    }
}

/* SCENARIO( "Testing " + class_name + " height computation", tag_name) */
/* { */
/*     GIVEN( "A surface that interpolates a plane") { */

/*         stratmod::SmoothInterpolatedSurface surface; */

/*         Eigen::RowVector3d p0({0., 0., 0.}); */
/*         Eigen::RowVector3d p1({1., 0., 1.}); */
/*         Eigen::RowVector3d p2({0., 1., 2.}); */

/*         Eigen::MatrixXd points(3,3); */ 
/*         points << p0, p1, p2; */

/*         // Create surface */
/*         REQUIRE(surface.set(points) == true); */ 

/*         WHEN( "Computing heights after translating surface S(x, y) by vector `t`" ) { */
/*         } */

/*         WHEN( "Computing heights after rotating surface S(x, y) by angle `-a` (radians)" ) { */
/*         } */

/*         WHEN( "Computing heights after scaling surface S(x, y) by scale vector `s`" ) { */
/*         } */
/*     } */
/* } */

/* SCENARIO( "Testing " + class_name + " derivative computation", tag_name) */
/* { */
/*     GIVEN( "A surface that interpolates a plane") { */

/*         stratmod::ExtrudedSurface surface; */

/*         Eigen::RowVector3d p0({0., 0., 0.}); */
/*         Eigen::RowVector3d p1({1., 0., 1.}); */
/*         Eigen::RowVector3d p2({0., 1., 2.}); */

/*         Eigen::MatrixXd points(3,3); */ 
/*         points << p0, p1, p2; */

/*         // Create surface */
/*         REQUIRE(surface.set(points) == true); */ 

/*         WHEN( "Querying whether the surface is differentiable" ) { */
/*             THEN( "The surface is differentiable" ) */
/*             { */
/*                 REQUIRE(surface.differentiable() == true); */
/*             } */
/*         } */

/*         WHEN( "Computing derivatives" ) { */
/*             THEN( "The derivatives are colinear to the plane's normal" ) */ 
/*             { */
/*                 Eigen::Vector3d plane_normal = (p1 - p0).cross(p2 - p0).transpose().normalized(); */
/*                 Eigen::Vector3d surface_normal; */

/*                 auto derivative = surface.derivative({p0[0], p0[1]}); */
/*                 surface_normal = Eigen::Vector3d({-(*derivative)[0], -(*derivative)[1], 1}).normalized(); */
/*                 REQUIRE( plane_normal.cross(surface_normal).norm() == 0.0_a ); */

/*                 derivative = surface.derivative({p1[0], p1[1]}); */
/*                 surface_normal = Eigen::Vector3d({-(*derivative)[0], -(*derivative)[1], 1}).normalized(); */
/*                 REQUIRE( plane_normal.cross(surface_normal).norm() == 0.0_a ); */

/*                 derivative = surface.derivative({p2[0], p2[1]}); */
/*                 surface_normal = Eigen::Vector3d({-(*derivative)[0], -(*derivative)[1], 1}).normalized(); */
/*                 REQUIRE( plane_normal.cross(surface_normal).norm() == 0.0_a ); */
/*             } */
/*         } */
/*     } */
/* } */
