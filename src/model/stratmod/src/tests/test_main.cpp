#define CATCH_CONFIG_RUNNER
#include "catch2/catch.hpp"

#include "glog/logging.h"

int main( int argc, char* argv[] ) 
{
    //
    // Global setup
    //

    // Initialize google::glog
    FLAGS_alsologtostderr = true;
    FLAGS_colorlogtostderr = true;
    FLAGS_log_dir = ".";
    google::InitGoogleLogging(argv[0]);
    google::InstallFailureSignalHandler();

    //
    // Initialize Catch2
    //

    int result = Catch::Session().run( argc, argv );

    //
    // Global clean-up
    //

    return result;
}
