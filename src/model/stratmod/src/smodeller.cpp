/********************************************************************************/
/*                                                                              */
/* This file is part of the "Stratigraphy Modeller Library" (StratModLib)       */
/* Copyright (C) 2017, Julio Daniel Machado Silva.                              */
/*                                                                              */
/* StratModLib is free software; you can redistribute it and/or                 */
/* modify it under the terms of the GNU Lesser General Public                   */
/* License as published by the Free Software Foundation; either                 */
/* version 3 of the License, or (at your option) any later version.             */
/*                                                                              */
/* StratModLib is distributed in the hope that it will be useful,               */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU            */
/* Lesser General Public License for more details.                              */
/*                                                                              */
/* You should have received a copy of the GNU Lesser General Public             */
/* License along with StratModLib.  If not, see <http://www.gnu.org/licenses/>, */
/* or write to the Free Software Foundation, Inc., 51 Franklin Street,          */
/* Fifth Floor, Boston, MA  02110-1301  USA.                                    */
/*                                                                              */
/********************************************************************************/

#include <glog/logging.h>

#include <iostream>
#include <type_traits>
#include <vector>
#include <map>
#include <memory>
#include <fstream>

#include "stratmod/metadata_cereal.hpp"
#include "detail/model_interpretation_cereal.hpp"
#include "stratmod/smodeller.hpp"
#include "stratmod/smodeller2.hpp"
#include "detail/smodeller_impl.hpp"

#include "detail/testing_definitions.hpp"

#include "planin/planin.hpp"
#include "stratmod/surfaces/extruded_surface.hpp"
#include "stratmod/surfaces/smooth_interpolated_surface.hpp"
#include "stratmod/surfaces/interpolated_triangle_surface.hpp"


/*****************************/
/* Actual Implementation */ 
/*****************************/

namespace stratmod {

// Default methods 

SModeller& SModeller::Instance()
{
    static SModeller instance;

    return instance;
}

[[deprecated("The SModeller class is a singleton, use SModeller::Instance() instead.")]]
SModeller::SModeller() : model_(SModeller2::Instance())
{
}
/* SModeller::SModeller() : */ 
/*     model_.model_.pimpl_( new stratmod::SModellerImplementation() ) */
/* { */
/*     model_.pimpl_->init(); */
/* } */ 

SModeller::~SModeller() = default; 

/* stratmod::SModellerImplementation& SModeller::Impl() */
/* { */
/*     return *model_.pimpl_; */
/* } */

/* const stratmod::SModellerImplementation& SModeller::Impl() const */
/* { */
/*     return *model_.pimpl_; */
/* } */

std::vector<size_t> SModeller::getSurfacesIndices()
{
    return model_.pimpl_->inserted_surfaces_indices_; 
}

std::vector<std::size_t> SModeller::getOrderedSurfacesIndices()
{
    return model_.pimpl_->getOrderedSurfacesIndices();
}

/* // create[Above,Below] is DEPRECATED */
/* // */
/* // brief: */
/* // Verifies whether ir is possible to define a new 'drawing' region, */
/* // either above or below. */
/* // Argument: `eligible_surfaces` stores the indices of surfaces that can */
/* // be used as input either for an `defineAbove()` or a `defineBelow()`. */
/* // Return: true if at least one elegible surface was found. */
/* // */
/* [[deprecated]] */
/* bool requestCreateAbove( std::vector<std::size_t> &eligible_surfaces ); */

/* bool SModeller::requestCreateAbove( std::vector<size_t> &eligible_surfaces ) */
/* { */
    /* if ( model_.pimpl_->container_.empty() ) */
    /* { */
    /*     return false; */ 
    /* } */

    /* eligible_surfaces.clear(); */ 
    /* ControllerSurfaceIndex output_index; */ 
    /* ContainerSurfaceIndex index; */ 

    /* for ( size_t i = 0; i < model_.pimpl_->inserted_surfaces_indices_.size(); ++i ) */
    /* { */
    /*     output_index = model_.pimpl_->inserted_surfaces_indices_[i]; */ 
    /*     if ( model_.pimpl_->getSurfaceIndex(output_index, index) == false ) */
    /*         continue; */ 

    /*     if ( model_.pimpl_->container_.weakEntireSurfaceCheck(index) ) */
    /*     { */
    /*         if ( model_.pimpl_->createBelowIsActive() ) */
    /*         { */
    /*             /1* std::cout << "Define Below is active.\n"; *1/ */ 
    /*             ContainerSurfaceIndex boundary_index; */ 

    /*             if ( model_.pimpl_->getSurfaceIndex(model_.pimpl_->current_.upper_boundary_, boundary_index) == false ) */
    /*             { */
    /*                 /1* std::cout << "Surface id: " << index << ", boundary id: " << boundary_index << std::endl; *1/ */ 
    /*                 /1* std::cout << "Got wrong id. \n"; *1/ */ 
    /*                 continue; */
    /*             } */

    /*             /1* std::cout << "Surface id: " << index << ", boundary id: " << boundary_index << std::endl; *1/ */ 

    /*             if ( index == boundary_index ) */
    /*             { */
    /*                 continue; */
    /*             } */

    /*             if ( model_.pimpl_->container_[index]->weakLiesBelowOrEqualsCheck(model_.pimpl_->container_[boundary_index]) ) */
    /*             { */
    /*                 eligible_surfaces.push_back(output_index); */ 
    /*             } */
    /*             else */
    /*             { */
    /*                 /1* std::cout << " --> Not eligible\n"; *1/ */ 
    /*             } */
    /*         } */
    /*         else */
    /*         { */
    /*             eligible_surfaces.push_back(output_index); */ 
    /*         } */

    /*     } */
    /* } */

    /* return !eligible_surfaces.empty(); */ 
/* } */

/* [[deprecated]] */
/* bool requestCreateBelow( std::vector<std::size_t> &eligible_surfaces ); */

/* bool SModeller::requestCreateBelow( std::vector<size_t> &eligible_surfaces ) */
/* { */
    /* if ( model_.pimpl_->container_.empty() ) */
    /* { */
    /*     return false; */ 
    /* } */

    /* eligible_surfaces.clear(); */ 
    /* ControllerSurfaceIndex output_index; */ 
    /* ContainerSurfaceIndex index; */ 

    /* for ( size_t i = 0; i < model_.pimpl_->inserted_surfaces_indices_.size(); ++i ) */
    /* { */
    /*     output_index = model_.pimpl_->inserted_surfaces_indices_[i]; */ 
    /*     if ( model_.pimpl_->getSurfaceIndex(output_index, index) == false ) */
    /*         continue; */ 

    /*     if ( model_.pimpl_->container_.weakEntireSurfaceCheck(index) ) */
    /*     { */
    /*         if ( model_.pimpl_->createAboveIsActive() ) */
    /*         { */
    /*             /1* std::cout << "Define Below is active.\n"; *1/ */ 
    /*             ContainerSurfaceIndex boundary_index; */ 

    /*             if ( model_.pimpl_->getSurfaceIndex(model_.pimpl_->current_.lower_boundary_, boundary_index) == false ) */
    /*             { */
    /*                 /1* std::cout << "Surface id: " << index << ", boundary id: " << boundary_index << std::endl; *1/ */ 
    /*                 /1* std::cout << "Got wrong id. \n"; *1/ */ 
    /*                 continue; */
    /*             } */

    /*             /1* std::cout << "Surface id: " << index << ", boundary id: " << boundary_index << std::endl; *1/ */ 
    /*             if ( index == boundary_index ) */
    /*             { */
    /*                 continue; */
    /*             } */

    /*             if ( model_.pimpl_->container_[index]->weakLiesAboveOrEqualsCheck(model_.pimpl_->container_[boundary_index]) ) */
    /*             { */
    /*                 eligible_surfaces.push_back(output_index); */ 
    /*             } */
    /*             else */
    /*             { */
    /*                 /1* std::cout << " --> Not eligible\n"; *1/ */ 
    /*             } */
    /*         } */
    /*         else */
    /*         { */
    /*             eligible_surfaces.push_back(output_index); */ 
    /*         } */

    /*     } */
    /* } */

    /* return !eligible_surfaces.empty(); */ 
/* } */


        /* Change the model's properties */
bool SModeller::useDefaultCoordinateSystem()
{
    /* if ( model_.pimpl_->container_.size() > 0 ) */
    /* { */
    /*     return false; */
    /* } */

    PlanarSurface::setOutputCoordinatesOrdering( 
            PlanarSurface::Coordinate::WIDTH,
            PlanarSurface::Coordinate::HEIGHT, 
            PlanarSurface::Coordinate::DEPTH
            );

    model_.pimpl_->default_coordinate_system_ = true;

    return true;
}

bool SModeller::useOpenGLCoordinateSystem()
{
    /* if ( model_.pimpl_->container_.size() > 0 ) */
    /* { */
    /*     return false; */
    /* } */

    PlanarSurface::setOutputCoordinatesOrdering( 
            PlanarSurface::Coordinate::WIDTH,
            PlanarSurface::Coordinate::DEPTH, 
            PlanarSurface::Coordinate::HEIGHT
            ); 

    model_.pimpl_->default_coordinate_system_ = false;

    return true;
}

bool SModeller::isUsingDefaultCoordinateSystem()
{
    return model_.pimpl_->default_coordinate_system_;
}

/**
 * \brief Change models discretization if model is empty
 *
 * \param width_discretization - discretization in the width direction
 * \param length_discretization - discretization in the length direction
 *
 * \return true if discretization was changed
 */
/* [[deprecated]] */
/* bool tryChangeDiscretization( std::size_t width_discretization = 64, std::size_t depth_discretization = 64 ); */

/* bool SModeller::tryChangeDiscretization( size_t width, size_t length ) */
/* { */
/*     if ( model_.pimpl_->container_.size() > 0 ) */
/*     { */
/*         return false; */
/*     } */

/*     model_.pimpl_->discWidth_ = width; */ 
/*     model_.pimpl_->discLenght_ = length; */ 

/*     PlanarSurface::requestChangeDiscretization(width, length); */

/*     return true; */
/* } */

bool SModeller::changeDiscretization( size_t width, size_t length )
{
    return model_.changeDiscretization(width, length);

    if ( (width == 0) || (length == 0) )
    {
        return false;
    }

    // WEIRD: without the following (superfluous) test, changeDiscretization()
    // was crashing if width and length were equal to the model's discretization
    if ( (width == model_.pimpl_->discWidth_) && (length == model_.pimpl_->discLenght_) )
    {
        return true;
    }

    int counter = 0;
    while ( canUndo() )
    {
        undo();
        ++counter;
    }

    PlanarSurface::requestChangeDiscretization(width, length);

    model_.pimpl_->discWidth_ = width; 
    model_.pimpl_->discLenght_ = length; 
    /* model_.pimpl_->container_.updateCache(); */

    while ( counter > 0 )
    {
        redo();
        --counter;
    }

    /* TODO: review use and conversion of types */
    /* /1* PlanarSurface::requestChangeDiscretization((PlanarSurface::Natural)(model_.pimpl_->discWidth_), PlanarSurface::Natural(model_.pimpl_->discLenght_)); *1/ */
    /* return model_.pimpl_->container_.changeDiscretization(width, length); */

    return true;
}


size_t SModeller::getWidthDiscretization() const
{
    return model_.pimpl_->discWidth_;
}

size_t SModeller::getLengthDiscretization() const
{
    return model_.pimpl_->discLenght_;
}

void SModeller::setOrigin( double x, double y, double z )
{
    /* if ( model_.pimpl_->default_coordinate_system_ ) */
    /* { */
    /*     model_.pimpl_->origin_.x = x; */ 
    /*     model_.pimpl_->origin_.y = y; */ 
    /*     model_.pimpl_->origin_.z = z; */ 
    /* } */
    /* else */
    /* { */
    /*     model_.pimpl_->origin_.x = x; */ 
    /*     model_.pimpl_->origin_.y = z; */ 
    /*     model_.pimpl_->origin_.z = y; */ 
    /* } */

    model_.pimpl_->origin_ = model_.pimpl_->point3(x, y, z);
    model_.pimpl_->container_.setOrigin(model_.pimpl_->origin_);

    model_.pimpl_->got_origin_ = true; 
}


bool SModeller::setSize( double x, double y, double z )
{
    bool success = model_.pimpl_->container_.setLength( model_.pimpl_->point3(x, y, z) );

    if ( success )
    /* if ( ( x > 0 ) && ( y > 0 ) && ( z > 0 ) ) */ 
    { 
        /* if ( model_.pimpl_->default_coordinate_system_ ) */
        /* { */
        /*     model_.pimpl_->lenght_.x = x; */ 
        /*     model_.pimpl_->lenght_.y = y; */ 
        /*     model_.pimpl_->lenght_.z = z; */ 
        /* } */
        /* else */
        /* { */
        /*     model_.pimpl_->lenght_.x = x; */ 
        /*     model_.pimpl_->lenght_.y = z; */ 
        /*     model_.pimpl_->lenght_.z = y; */ 
        /* } */

        model_.pimpl_->lenght_ = model_.pimpl_->point3(x, y, z);

        model_.pimpl_->got_lenght_ = true; 
    }

    return success; 
}

void SModeller::getOrigin( double &x, double &y, double &z )
{
    model_.pimpl_->getOrigin(x, y, z);
}


void SModeller::getSize( double &x, double &y, double &z )
{
    model_.pimpl_->getLenght(x, y, z);
}

        /* Begin methods to interface with GUI */

        /* Initialization and clean up */

void SModeller::clear()
{
    model_.clear();

    /* model_.pimpl_->container_.clear(); */ 
    /* model_.pimpl_->dictionary_.clear(); */ 
    /* model_.pimpl_->inserted_surfaces_indices_.clear(); */

    /* model_.pimpl_->current_ = StateDescriptor(); */ 

    /* model_.pimpl_->undoed_surfaces_stack_.clear(); */
    /* model_.pimpl_->undoed_states_.clear(); */ 
    /* model_.pimpl_->past_states_.clear(); */

    /* model_.pimpl_->got_origin_ = false; */ 
    /* model_.pimpl_->got_lenght_ = false; */ 

    /* model_.pimpl_->mesh_ = nullptr; */

    /* model_.pimpl_->init(); */
}



        /* Query or modify the automatum state */


/* // */
/* // brief: */
/* // Define new input region above surface which index is `surface_index`. */
/* // */
/* // */
/* // brief: */
/* // Define new input region above surface which index is `surface_index`. */
/* // */
/* [[deprecated]] */
/* bool createAbove( std::size_t surface_index ); */

/* bool SModeller::createAbove( size_t surface_index ) */
/* { */
/*     return model_.pimpl_->createAbove(surface_index); */ 
/* } */


/* // */
/* // brief: */
/* // Clear any previous `defineAbove()` call. */
/* // Safe to call anytime. */
/* // */
/* [[deprecated]] */
/* void stopCreateAbove(); */

        /* // */
        /* // brief: */
        /* // Clear any previous `createAbove()` call. */
        /* // Safe to call anytime. */
        /* // */
/* void SModeller::stopCreateAbove() */
/* { */
    /* model_.pimpl_->stopCreateAbove(); */
/* } */


/* // */
/* // brief: */
/* // Define new input region below surface which index is `surface_index`. */
/* // */
/* [[deprecated]] */
/* bool createBelow( std::size_t surface_index ); */

/* // */
/* // brief: */
/* // Define new input region below surface which index is `surface_index`. */
/* // */
/* bool SModeller::createBelow( size_t surface_index ) */
/* { */
/*     return model_.pimpl_->createBelow(surface_index); */ 
/* } */

/* // */
/* // brief: */
/* // Clear any previous `defineBelow()` call. */
/* // Safe to call anytime. */
/* // */
/* [[deprecated]] */
/* void stopCreateBelow(); */

/* // */
/* // brief: */
/* // Clear any previous `defineBelow()` call. */
/* // Safe to call anytime. */
/* // */
/* void SModeller::stopCreateBelow() */
/* { */
/*     model_.pimpl_->stopCreateBelow(); */
/* } */


/* [[deprecated]] */
/* bool createAboveIsActive( std::size_t &boundary_index ); */

/* bool SModeller::createAboveIsActive( size_t &boundary_index ) */
/* { */
/*     return model_.pimpl_->createAboveIsActive(boundary_index); */
/* } */

/* [[deprecated]] */
/* bool createBelowIsActive( std::size_t &boundary_index ); */

/* bool SModeller::createBelowIsActive( size_t &boundary_index ) */  
/* { */
/*     return model_.pimpl_->createBelowIsActive(boundary_index); */
/* } */

bool SModeller::preserveAbove( const std::vector<size_t> &bounding_surfaces_list )
{
    return model_.pimpl_->preserveAbove(bounding_surfaces_list);
}

bool SModeller::preserveBelow( const std::vector<size_t> &bounding_surfaces_list )
{
    return model_.pimpl_->preserveBelow(bounding_surfaces_list);
}

void SModeller::stopPreserveAbove()
{
    return model_.pimpl_->stopPreserveAbove();
}

void SModeller::stopPreserveBelow()
{
    return model_.pimpl_->stopPreserveBelow();
}

bool SModeller::preserveAboveIsActive( std::vector<std::size_t> &bounding_surfaces_list )
{
    return model_.pimpl_->preserveAboveIsActive(bounding_surfaces_list);
}

bool SModeller::preserveBelowIsActive( std::vector<std::size_t> &bounding_surfaces_list )
{
    return model_.pimpl_->preserveBelowIsActive(bounding_surfaces_list);
}

void SModeller::disableGeologicRules()
{
    model_.pimpl_->current_.state_ = State::SKETCHING;
}

void SModeller::removeAbove()
{
    model_.pimpl_->current_.state_ = State::RA_SKETCHING;
}

void SModeller::removeAboveIntersection()
{
    model_.pimpl_->current_.state_ = State::RAI_SKETCHING; 
}

        
void SModeller::removeBelow()
{
    model_.pimpl_->current_.state_ = State::RB_SKETCHING;
}

void SModeller::removeBelowIntersection()
{
    model_.pimpl_->current_.state_ = State::RBI_SKETCHING; 
} 


/* bool SModeller::createSurface( size_t surface_id, const std::vector<double> &point_data ) */
/* { */
/*     INFO( "Got into SModeller::createSurface(...)" ); */

/*     bool extruded_surface = false; */ 
/*     return model_.pimpl_->insertSurface(point_data, surface_id, std::vector<size_t>(), std::vector<size_t>(), extruded_surface); */
/* } */

bool SModeller::createSurface( size_t surface_id, const std::vector<double> &point_data, 
                double fill_distance,
                const std::vector<size_t>, const std::vector<size_t>)
{
    /* bool extruded_surface = false; */ 
    /* bool orthogonally_oriented = false; */
    /* return model_.pimpl_->insertSurface(point_data, surface_id, lower_bound_ids, upper_bound_ids, extruded_surface, orthogonally_oriented, fill_distance); */ 


    using namespace Eigen;

    std::shared_ptr<SmoothInterpolatedSurface> isptr = std::make_shared<SmoothInterpolatedSurface>();

    Map<Eigen::Matrix<double, Dynamic, Dynamic, RowMajor>> points(const_cast<double*>(point_data.data()), point_data.size()/3, 3);
    bool success;

    if (model_.pimpl_->default_coordinate_system_)
    {
        LOG(INFO) << "Trying to create SmoothInterpolatedSurface in default coordinate system";

        /* success = isptr->set(points); */
        success = isptr->set(points, std::max(fill_distance, 1E-3));
    }
    else
    {
        LOG(INFO) << "Trying to create SmoothInterpolatedSurface in opengl coordinate system";

        MatrixXd points2(points.rows(), points.cols());
        points2.col(0) = points.col(0);
        points2.col(1) = points.col(2);
        points2.col(2) = points.col(1);

        /* success = isptr->set(points2); */
        success = isptr->set(points2, std::max(fill_distance, 1E-3));
    }
    auto print = [](bool success) -> std::string { return (success ? "success" : "failure"); };
    LOG(INFO) << "Result of creating SmoothInterpolatedSurface is " << print(success);

    success &= model_.addStratigraphicSurface(static_cast<int>(surface_id), isptr);
    LOG(INFO) << "Result of inserting surface " << surface_id << " into model is " << print(success);

    return success;
}

/* bool SModeller::createExtrudedSurface( size_t surface_id, const std::vector<double> &point_data ) */
/* { */
/*     bool extruded_surface = true; */ 
/*     return model_.pimpl_->insertSurface(point_data, surface_id, std::vector<size_t>(), std::vector<size_t>(), extruded_surface); */
/* } */

bool SModeller::createLengthwiseExtrudedSurface( size_t surface_id, 
        const std::vector<double> &point_data, 
        double /* fill_distance */,
        const std::vector<size_t>, 
        const std::vector<size_t>)
{
    /* bool orthogonally_oriented = false; */ 
    /* size_t cross_section_depth = 0; */
    /* std::vector<double> empty_path = std::vector<double>(); */

    /* return model_.pimpl_->insertExtrusionAlongPath(surface_id, */ 
            /* point_data, cross_section_depth, */ 
            /* empty_path, */ 
            /* lower_bound_ids, upper_bound_ids, orthogonally_oriented, fill_distance); */ 

    using namespace Eigen;

    Map<Matrix<double, Dynamic, Dynamic, RowMajor>> points(const_cast<double*>(point_data.data()), point_data.size()/2, 2);
    auto isptr = std::make_shared<ExtrudedSurface>();
    bool success = isptr->setLinearExtrusion(points);

    auto print = [](bool success) -> std::string { return (success ? "success" : "failure"); };
    DLOG(INFO) << "Result of creating Lengthwise ExtrudedSurface is " << print(success);

    success &= model_.addStratigraphicSurface(static_cast<int>(surface_id), isptr);;
    DLOG(INFO) << "Result of inserting surface " << surface_id << " into model is " << print(success);

    return success;
}


bool SModeller::createLengthwiseExtrudedSurface( size_t surface_id, 
        const std::vector<double> &cross_section_curve_point_data, double cross_section_depth, 
        const std::vector<double> &path_curve_point_data, 
        double /* fill_distance */,
        const std::vector<size_t> /* lower_bound_ids */,
        const std::vector<size_t> /* upper_bound_ids */
        )
{
    /* bool orthogonally_oriented = false; */ 
    /* return model_.pimpl_->insertExtrusionAlongPath(surface_id, cross_section_curve_point_data, cross_section_depth, path_curve_point_data, lower_bound_ids, upper_bound_ids, orthogonally_oriented, fill_distance); */
    using namespace Eigen;

    Map<Matrix<double, Dynamic, Dynamic, RowMajor>> cross_section(const_cast<double*>(cross_section_curve_point_data.data()), cross_section_curve_point_data.size()/2, 2);
    Map<Matrix<double, Dynamic, Dynamic, RowMajor>> opath(const_cast<double*>(path_curve_point_data.data()), path_curve_point_data.size()/2, 2);
    Matrix<double, Dynamic, Dynamic, RowMajor>  path(opath.rows(), opath.cols());
    path.col(0) = opath.col(1) - cross_section_depth * Eigen::VectorXd::Ones(opath.rows());
    path.col(1) = opath.col(0);

    auto isptr = std::make_shared<ExtrudedSurface>();
    bool success = isptr->setPathGuidedExtrusion(cross_section, path);
    isptr->setCrossSectionOrigin({0., cross_section_depth});

    auto print = [](bool success) -> std::string { return (success ? "success" : "failure"); };
    DLOG(INFO) << "Result of creating Lengthwise ExtrudedSurface is " << print(success);

    success &= model_.addStratigraphicSurface(static_cast<int>(surface_id), isptr);;
    DLOG(INFO) << "Result of inserting surface " << surface_id << " into model is " << print(success);

    return success;
}


bool SModeller::createWidthwiseExtrudedSurface( size_t surface_id, 
        const std::vector<double> &point_data, 
        double /* fill_distance */,
        const std::vector<size_t> /* lower_bound_ids */, 
        const std::vector<size_t> /* upper_bound_ids */ )
{
    /* bool orthogonally_oriented = true; */ 
    /* size_t cross_section_depth = 0; */
    /* std::vector<double> empty_path = std::vector<double>(); */

    /* return model_.pimpl_->insertExtrusionAlongPath(surface_id, */ 
            /* point_data, cross_section_depth, */ 
            /* empty_path, */ 
            /* lower_bound_ids, upper_bound_ids, orthogonally_oriented, fill_distance); */ 

    using namespace Eigen;

    Map<Matrix<double, Dynamic, Dynamic, RowMajor>> points(const_cast<double*>(point_data.data()), point_data.size()/2, 2);
    auto isptr = std::make_shared<ExtrudedSurface>();
    bool success = isptr->setLinearExtrusion(points);
    isptr->setCrossSectionDirection({0., 1.});

    auto print = [](bool success) -> std::string { return (success ? "success" : "failure"); };
    DLOG(INFO) << "Result of creating Widthwise ExtrudedSurface is " << print(success);

    success &= model_.addStratigraphicSurface(static_cast<int>(surface_id), isptr);;
    DLOG(INFO) << "Result of inserting Widthwise ExtrudedSurface into model is " << print(success);

    return success;
}


bool SModeller::createWidthwiseExtrudedSurface( size_t surface_id, 
        const std::vector<double> &cross_section_curve_point_data, double cross_section_depth, 
        const std::vector<double> &path_curve_point_data, 
        double /* fill_distance */,
        const std::vector<size_t> /* lower_bound_ids */,
        const std::vector<size_t> /* upper_bound_ids */
        )
{
    /* bool orthogonally_oriented = true; */ 
    /* return model_.pimpl_->insertExtrusionAlongPath(surface_id, cross_section_curve_point_data, cross_section_depth, path_curve_point_data, lower_bound_ids, upper_bound_ids, orthogonally_oriented, fill_distance); */

    using namespace Eigen;

    Map<Matrix<double, Dynamic, Dynamic, RowMajor>> cross_section(const_cast<double*>(cross_section_curve_point_data.data()), cross_section_curve_point_data.size()/2, 2);
    Map<Matrix<double, Dynamic, Dynamic, RowMajor>> opath(const_cast<double*>(path_curve_point_data.data()), path_curve_point_data.size()/2, 2);
    Matrix<double, Dynamic, Dynamic, RowMajor>  path(opath.rows(), opath.cols());
    path.col(0) = cross_section_depth * Eigen::VectorXd::Ones(opath.rows()) - opath.col(1);
    path.col(1) = opath.col(0);


    auto isptr = std::make_shared<ExtrudedSurface>();
    bool success = isptr->setPathGuidedExtrusion(cross_section, path);
    isptr->setCrossSectionDirection({0., 1.});
    isptr->setCrossSectionOrigin({cross_section_depth, 0.});

    auto print = [](bool success) -> std::string { return (success ? "success" : "failure"); };
    DLOG(INFO) << "Result of creating Widthwise ExtrudedSurface is " << print(success);

    success &= model_.addStratigraphicSurface(static_cast<int>(surface_id), isptr);;
    DLOG(INFO) << "Result of inserting Widthwise ExtrudedSurface into model is " << print(success);

    return success;
}


bool SModeller::tryCreateLengthwiseExtrudedSurface( size_t surface_id, std::vector<size_t> &intersected_surfaces,
        const std::vector<double> &cross_section_curve_point_data, double cross_section_depth, 
        const std::vector<double> &path_curve_point_data, 
        double fill_distance,
        const std::vector<size_t> lower_bound_ids,
        const std::vector<size_t> upper_bound_ids
        )
{
    bool status = createLengthwiseExtrudedSurface(surface_id, cross_section_curve_point_data, cross_section_depth, path_curve_point_data, fill_distance, lower_bound_ids, upper_bound_ids);

    if ( status == false )
    {
        return false;
    }

    model_.pimpl_->lastInsertedSurfaceIntersects(intersected_surfaces);

    if ( ! intersected_surfaces.empty() )
    {
        model_.pimpl_->popLastSurface();

        return false;
    }

    return true;
}

bool SModeller::tryCreateWidthwiseExtrudedSurface( size_t surface_id, std::vector<size_t> &intersected_surfaces,
        const std::vector<double> &cross_section_curve_point_data, double cross_section_depth, 
        const std::vector<double> &path_curve_point_data, 
        double fill_distance,
        const std::vector<size_t> lower_bound_ids,
        const std::vector<size_t> upper_bound_ids
        )
{
    bool status = createWidthwiseExtrudedSurface(surface_id, cross_section_curve_point_data, cross_section_depth, path_curve_point_data, fill_distance, lower_bound_ids, upper_bound_ids);

    if ( status == false )
    {
        return false;
    }

    model_.pimpl_->lastInsertedSurfaceIntersects(intersected_surfaces);

    if ( ! intersected_surfaces.empty() )
    {
        model_.pimpl_->popLastSurface();

        return false;
    }

    return true;
}


/* bool SModeller::tryCreateSurface( size_t surface_id, const std::vector<double> &point_data, std::vector<size_t> &intersected_surfaces ) */
/* { */
/*     return tryCreateSurface(surface_id, point_data, std::vector<size_t>(), std::vector<size_t>(), intersected_surfaces); */
/* } */

bool SModeller::tryCreateSurface( size_t surface_id, std::vector<size_t> &intersected_surfaces, 
        const std::vector<double> &point_data, double fill_distance, 
        const std::vector<size_t> lower_bound_ids, const std::vector<size_t> upper_bound_ids )
{
    /* State current = model_.pimpl_->current_.state_; */
    /* model_.pimpl_->current_.state_ = State::SKETCHING; */

    /* std::cout << "Trying to create a surface...\n"; */ 
    bool status = createSurface(surface_id, point_data, fill_distance, lower_bound_ids, upper_bound_ids); 

    /* model_.pimpl_->current_.state_ = current; */ 

    if ( status == false )
    {
        return false;
    }

    model_.pimpl_->lastInsertedSurfaceIntersects(intersected_surfaces);

    if ( ! intersected_surfaces.empty() )
    {
        /* std::cout << "But it intersected another surface...\n"; */
        model_.pimpl_->popLastSurface();

        return false;
    }

    /* std::cout << "Success!\n"; */

    return true;
}

/* bool SModeller::tryCreateExtrudedSurface( size_t surface_id, const std::vector<double> &point_data, std::vector<size_t> &intersected_surfaces ) */
/* { */
/*     return tryCreateExtrudedSurface(surface_id, point_data, std::vector<size_t>(), std::vector<size_t>(), intersected_surfaces); */
/* } */

bool SModeller::tryCreateLengthwiseExtrudedSurface( size_t surface_id, std::vector<size_t> &intersected_surfaces,
        const std::vector<double> &point_data,
        double fill_distance,
        const std::vector<size_t> lower_bound_ids, 
        const std::vector<size_t> upper_bound_ids )
{
    /* State current = model_.pimpl_->current_.state_; */
    /* model_.pimpl_->current_.state_ = State::SKETCHING; */

    bool status = createLengthwiseExtrudedSurface(surface_id, point_data, fill_distance, lower_bound_ids, upper_bound_ids); 

    /* model_.pimpl_->current_.state_ = current; */ 

    if ( status == false )
    {
        return false;
    }

    model_.pimpl_->lastInsertedSurfaceIntersects(intersected_surfaces);

    if ( ! intersected_surfaces.empty() )
    {
        model_.pimpl_->popLastSurface();

        return false;
    }

    return true;
}

bool SModeller::tryCreateWidthwiseExtrudedSurface( size_t surface_id, std::vector<size_t> &intersected_surfaces,
        const std::vector<double> &point_data,
        double fill_distance,
        const std::vector<size_t> lower_bound_ids, 
        const std::vector<size_t> upper_bound_ids )
{
    /* State current = model_.pimpl_->current_.state_; */
    /* model_.pimpl_->current_.state_ = State::SKETCHING; */

    bool status = createWidthwiseExtrudedSurface(surface_id, point_data, fill_distance, lower_bound_ids, upper_bound_ids); 

    /* model_.pimpl_->current_.state_ = current; */ 

    if ( status == false )
    {
        return false;
    }

    model_.pimpl_->lastInsertedSurfaceIntersects(intersected_surfaces);

    if ( ! intersected_surfaces.empty() )
    {
        model_.pimpl_->popLastSurface();

        return false;
    }

    return true;
}

bool SModeller::canUndo()
{
    return model_.canUndo();
}

bool SModeller::undo()
{
    return model_.undo();
}

/* [[deprecated]] */
/* bool popUndoStack(); */

/* bool SModeller::popUndoStack() */
/* { */
/*     return model_.pimpl_->popUndoStack(); */
/* } */

bool SModeller::canRedo()
{
    return model_.canRedo();
}

bool SModeller::redo()
{
    return model_.redo();
}

bool SModeller::destroyLastSurface()
{
    return model_.destroyLastSurface();
}

/* [[deprecated]] */
/* bool getVertexList( std::size_t surface_id, std::vector<float> &vlist ); */

/* bool SModeller::getVertexList( size_t surface_id, std::vector<float> &vlist ) */
/* { */
/*     return model_.pimpl_->getVertexList(surface_id, vlist); */
/* } */

/* [[deprecated]] */
/* bool getVertexList( std::size_t surface_id, std::vector<double> &vlist ); */

/* bool SModeller::getVertexList( size_t surface_id, std::vector<double> &vlist ) */
/* { */
/*     return model_.pimpl_->getVertexList(surface_id, vlist); */
/* } */

/* [[deprecated]] */
/* bool getMesh( std::size_t surface_id, std::vector<float> &vlist, std::vector<unsigned int> &flist ); */

/* bool SModeller::getMesh( size_t surface_id, std::vector<float> &vlist, std::vector<unsigned int> &flist ) */
/* { */
/*     return model_.pimpl_->getMesh(surface_id, vlist, flist); */
/* } */

bool SModeller::getMesh( size_t surface_id, std::vector<double> &vlist, std::vector<size_t> &flist )
{
    return model_.pimpl_->getMesh(surface_id, vlist, flist);
}

/* [[deprecated]] */
/* bool getWidthCrossSectionCurve( std::size_t surface_id, std::size_t width, std::vector<float> &vlist, std::vector<unsigned int> &elist ); */

/* bool SModeller::getWidthCrossSectionCurve( size_t surface_id, size_t width, std::vector<float> &vlist, std::vector<unsigned int> &elist ) */
/* { */
/*     return model_.pimpl_->getCrossSectionWidth(surface_id, vlist, elist, width); */
/*     /1* return model_.pimpl_->getAdaptedCrossSectionAtConstantWidth(surface_id, vlist, elist, width); *1/ */
/* } */

bool SModeller::getWidthCrossSectionCurve( size_t surface_id, size_t width, std::vector<double> &vlist, std::vector<size_t> &elist )
{
    return model_.pimpl_->getCrossSectionWidth(surface_id, vlist, elist, width);
    /* return model_.pimpl_->getAdaptedCrossSectionAtConstantWidth(surface_id, vlist, elist, width); */
}

/* [[deprecated]] */
/* bool getLengthCrossSectionCurve( std::size_t surface_id, std::size_t lenght, std::vector<float> &vlist, std::vector<unsigned int> &elist ); */

/* bool SModeller::getLengthCrossSectionCurve( size_t surface_id, size_t length, std::vector<float> &vlist, std::vector<unsigned int> &elist ) */
/* { */
/*     return model_.pimpl_->getCrossSectionDepth(surface_id, vlist, elist, length); */
/*     /1* return model_.pimpl_->getAdaptedCrossSectionAtConstantLength(surface_id, vlist, elist, length); *1/ */
/* } */

bool SModeller::getLengthCrossSectionCurve( size_t surface_id, size_t length, std::vector<double> &vlist, std::vector<size_t> &elist )
{
    return model_.pimpl_->getCrossSectionDepth(surface_id, vlist, elist, length);
    /* return model_.pimpl_->getAdaptedCrossSectionAtConstantLength(surface_id, vlist, elist, length); */
}


/* [[deprecated]] */
/* std::size_t getTetrahedralMesh( std::vector<double> &vertex_coordinates, std::vector<std::vector<std::size_t>> &element_list ); */

/* std::size_t SModeller::getTetrahedralMesh( std::vector<double> &vertex_coordinates, std::vector< std::vector<std::size_t> > &element_list ) */
/* { */
/*     /1* TetrahedralMeshBuilder mb(model_.pimpl_->container_); *1/ */
/*     if ( model_.pimpl_->buildTetrahedralMesh() == false ) */
/*     { */
/*         return 0; */
/*     } */

/*     bool status = true; */ 
/*     size_t num_elements; */

/*     status &= (model_.pimpl_->mesh_->getVertexCoordinates(vertex_coordinates) > 0); */

/*     if ( status == false ) */
/*     { */
/*         return 0; */
/*     } */


/*     num_elements = model_.pimpl_->mesh_->getTetrahedronList(element_list); */

/*     return num_elements; */
/* } */

std::size_t SModeller::getTetrahedralMesh( std::vector<double> &vertex_coordinates, std::vector<std::size_t> &element_list, std::vector<long int> &attribute_list )
{
    return model_.getTetrahedralMesh(vertex_coordinates, element_list, attribute_list);
}

bool SModeller::getNormalList( std::size_t surface_id, std::vector<double> &normal_list )
{
    size_t index; 
    if ( model_.pimpl_->getSurfaceIndex(surface_id, index) == false )
    {
        return false; 
    }

    bool status = model_.pimpl_->container_[index]->getNormalList(normal_list);

    return status;

}

[[deprecated]]
bool SModeller::getExtrusionPath( std::size_t surface_id, std::vector<double> &path_vertex_list )
{

    auto isptrd = model_.getInputSurface(surface_id);
    std::shared_ptr<ExtrudedSurface> isptr = std::dynamic_pointer_cast<ExtrudedSurface>(isptrd);
    if (isptr == nullptr)
    {
        return false;
    }

    auto path = isptr->getPathCurve();
    if (path.has_value() == false)
    {
        return false;
    }

    for (int i = 0; i < path.value().rows(); ++i)
    {
        /* if (std::abs(isptr->getCrossSectionDirection().dot(Eigen::Vector2d({1., 0}))) < 1E-5) */
        /* { */
        /*     path_vertex_list.push_back(path.value()(i,0)); */
        /*     path_vertex_list.push_back(path.value()(i,1)); */
        /* } */
        /* else */
        {
            path_vertex_list.push_back(path.value()(i,1));
            path_vertex_list.push_back(path.value()(i,0));
        }
    }

    /* TODO: update new implementation */
    /* return model_.pimpl_->container_[index]->getRawPathVertexList(path_vertex_list); */
    return true;
}

bool SModeller::computeTetrahedralMeshVolumes( std::vector<double> &vlist )
{
    return model_.computeTetrahedralMeshVolumes(vlist);
}

bool SModeller::getVolumeAttributesFromPointList( const std::vector<double> &vcoords, std::vector<int> &attribute_list)
{
    /* if ( vcoords.size() % 3 != 0 ) */
    /* { */
    /*     return false; */
    /* } */

    /* size_t num_points = vcoords.size()/3; */

    /* Point3 p; */
    /* std::vector<Point3> point_list(num_points); */

    /* for ( size_t i = 0; i < num_points; ++i ) */
    /* { */
    /*     p = model_.pimpl_->point3(vcoords[3*i + 0], vcoords[3*i + 1], vcoords[3*i + 2]); */
    /*     point_list[i] = p; */
    /* } */

    /* if ( model_.pimpl_->buildTetrahedralMesh() == false ) */
    /* { */
    /*     return false; */
    /* } */

    /* bool status = model_.pimpl_->mesh_->mapPointsToRegions(point_list, attribute_list); */

    return model_.getVolumeAttributesFromPointList(vcoords, attribute_list);
}

bool SModeller::getBoundingSurfacesFromVolumeAttribute( std::size_t attribute_id, std::vector<size_t> &lower_bound, std::vector<size_t> &upper_bound)
{
    return model_.pimpl_->getBoundingSurfacesFromRegionID(attribute_id, lower_bound, upper_bound);
}

std::vector<size_t> SModeller::getSurfacesIndicesBelowPoint( double x, double y, double z )
{
    return model_.pimpl_->getSurfacesIndicesBelowPoint(x, y, z);
}

std::vector<size_t> SModeller::getSurfacesIndicesAbovePoint( double x, double y, double z )
{
    return model_.pimpl_->getSurfacesIndicesAbovePoint(x, y, z);
}

bool SModeller::setSurfaceMetadata( std::size_t surface_id, const SurfaceMetadata& metadata )
{
    auto iptr = model_.getInputSurface(surface_id);
    if (iptr)
    {
        iptr->metadata() = metadata;
        return true;
    }

    return false;
}

bool SModeller::getSurfaceMetadata( std::size_t surface_id, SurfaceMetadata& metadata )
{
    auto iptr = model_.getInputSurface(surface_id);
    if (iptr)
    {
        metadata =  iptr->metadata();
        return true;
    }

    return false;
}

std::optional<Region> SModeller::getRegion(int region_id)
{
    return Region::Get(region_id);
}

ModelInterpretation SModeller::getRegions()
{
    return model_.getRegions();
}

ModelInterpretation SModeller::newInterpretation()
{
    return ModelInterpretation();
}

std::unordered_map<std::string, ModelInterpretation>& SModeller::interpretations()
{
    return model_.interpretations();
}

#if defined(BUILD_WITH_SERIALIZATION)
bool SModeller::saveBinary( std::string filename )
{
    return model_.saveBinary(filename);
}

bool SModeller::saveJSON( std::string filename )
{
    return model_.saveJSON(filename);
}

bool SModeller::loadBinary( std::string filename )
{
    return model_.loadBinary(filename);
}

bool SModeller::loadJSON( std::string filename )
{
    return model_.loadJSON(filename);
}

#else //defined(BUILD_WITH_SERIALIZATION)
bool SModeller::saveBinary( std::string )
{
    return false;
}

bool SModeller::loadBinary( std::string )
{
    return false;
}

bool SModeller::saveJSON( std::string )
{
    return false;
}

bool SModeller::loadBinary( std::string )
{
    return false;
}

#endif //defined(BUILD_WITH_SERIALIZATION)

} // namespace stratmod
