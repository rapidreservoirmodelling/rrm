Stratmod: _correct_ geology for everyone
----------------------------------------

*v0.1*

Stratmod stands for the _stratigraphy modeller library_, which intends to
provide a simple and stable interface to model planar stratigraphy.  

For now, I'm sure you would like to have a look at the 
[issue tracker](https://bitbucket.org/jdms/stratmod/issues).  

Documentation will follow shortly.  


