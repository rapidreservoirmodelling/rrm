/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#ifndef STRATMOD_SURFACES_INPUT_SURFACE_HPP
#define STRATMOD_SURFACES_INPUT_SURFACE_HPP


#include <memory>
#include <optional>

#include "stratmod/surfaces/abstract_surface.hpp"
#include "cereal/access.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/types/polymorphic.hpp"
#include "stratmod/misc/serialization_archives.hpp"
#include "stratmod/misc/eigen_cereal_support.hpp"
#include "stratmod/misc/smodeller_primitives.hpp"

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include "stratmod/surfaces/graph_of_function.hpp"

namespace stratmod {

class InputSurface : public GraphOfFunction
{
    public:
        InputSurface() = default;
        virtual ~InputSurface() = 0;

        InputSurface(const InputSurface&) = default;
        InputSurface& operator=(const InputSurface&) = default;

        InputSurface(InputSurface&&) = default;
        InputSurface& operator=(InputSurface&&) = default;

        virtual bool differentiable() const override
        { 
            return false;
        }

        virtual double operator()(const Eigen::Vector2d& p) const final
        {
            return scaleZ_ * (evalHeight(transform_dom_ * p) + translateZ_);
        }

        std::optional<Eigen::Vector2d> derivative(const Eigen::Vector2d& p) const
        {
            if (!differentiable())
            {
                return std::make_optional<Eigen::Vector2d>();
            }

            Eigen::Matrix2d DT = transform_dom_.linear().transpose();
            return std::make_optional(static_cast<Eigen::Vector2d>(DT * scaleZ_*evalDerivative(transform_dom_ * p)));
        }

        std::unique_ptr<stratmod::InputSurface> clone() const
        {
            std::unique_ptr<stratmod::InputSurface> bptr(static_cast<stratmod::InputSurface*>(this->doClone()));

            if ((bptr != nullptr) && (AbstractSurface::metadataPtr() != nullptr))
            {
                bptr->metadataPtr() = AbstractSurface::metadataPtr()->clone();
            }

            return bptr;
        }

        void translate(const Eigen::Vector3d& displacement);

        void rotate(double angle_radians, const Eigen::Vector3d& center = Eigen::Vector3d::Zero());

        bool scale(const Eigen::Vector3d& scale, const Eigen::Vector3d& center = Eigen::Vector3d::Zero());

        double scaleZ() const &&;

        double& scaleZ(double scale = 1.) &;

        virtual void resetTransform();

        Eigen::Affine3d getTransform() const;

        static void sample(std::shared_ptr<InputSurface> iptr, Eigen::MatrixXd& Vertices, Eigen::MatrixXi& Triangles);

        static void sample(std::shared_ptr<InputSurface> iptr, std::vector<double>& vertex_list, std::vector<std::size_t>& triangle_list);

    protected:
        virtual double evalHeight(const Eigen::Vector2d& /* p */) const = 0;

        virtual Eigen::Vector2d evalDerivative(const Eigen::Vector2d& /* p */) const;

        virtual stratmod::InputSurface* doClone() const = 0;

        bool operator==(const InputSurface& rhs) const;

        bool isEqual(const InputSurface& rhs) const;

        Eigen::Affine2d transformDomain() const &&;

        Eigen::Affine2d& transformDomain() &;

        void resetRotateDomain();

        void translateDomain(const Eigen::Vector2d& displacement);

        void resetTranslateDomain();

        double translateZ() const &&;

        double& translateZ(double displacement = 0.) &;

        bool scaleDomain(double isotropic_scale);

        bool scaleDomain(const Eigen::Vector2d& scale);

        void resetScaleDomain();

        /* double scaleZ() const &&; */

        /* double& scaleZ(double scale = 1.) &; */


    private:
        Eigen::Affine2d transform_dom_ = Eigen::Affine2d::Identity();
        double translateZ_ = 0.;
        double scaleZ_ = 1.;
        static constexpr double min_scale_ = 1E-12;

        // Cereal provides an easy way to serialize objects
        friend class cereal::access;

        template<typename Archive>
        void save(Archive & ar, const std::uint32_t /* version */) const
        {
            /* LOG(INFO) << "Call save method in stratmod::InputSurface"; */
            ar(cereal::base_class<stratmod::GraphOfFunction>(this));
            ar(transform_dom_.matrix());
            ar(translateZ_);
            ar(scaleZ_);
        }

        template<typename Archive>
        void load(Archive & ar, const std::uint32_t /* version */) 
        {
            /* LOG(INFO) << "Call load method in stratmod::InputSurface"; */
            ar(cereal::base_class<stratmod::GraphOfFunction>(this));
            Eigen::Matrix3d m;
            ar(m);
            transform_dom_.matrix() = m;
            ar(translateZ_);
            ar(scaleZ_);
        }
};

template<typename T, typename std::enable_if<std::is_base_of<stratmod::InputSurface, T>::value>::type* = nullptr>
static std::unique_ptr<InputSurface> CloneInputSurface(const T& derived)
{
    std::unique_ptr<InputSurface> bptr(static_cast<InputSurface*>(new T(derived)));
    return bptr;
}

template<typename T, typename std::enable_if<std::is_base_of<stratmod::InputSurface, T>::value>::type* = nullptr>
bool operator!=(const T& lhs, const T& rhs)
{
    return !(lhs == rhs);
}

        /* bool operator==(const InputSurface& lhs, const InputSurface& rhs) */
        /* { */
        /*     bool are_equal = true; */
        /*     are_equal &= (lhs.transform_dom_.matrix() == rhs.transform_dom_.matrix()); */
        /*     are_equal &= (lhs.translateZ_ == rhs.translateZ_); */
        /*     are_equal &= (lhs.scaleZ_ == rhs.scaleZ_); */
        /*     are_equal &= (lhs.min_scale_ == rhs.min_scale_); */

        /*     return are_equal; */
        /* } */


} // namespace stratmod

CEREAL_CLASS_VERSION(stratmod::InputSurface, 0);

CEREAL_REGISTER_TYPE(stratmod::InputSurface);
CEREAL_REGISTER_POLYMORPHIC_RELATION(stratmod::InputSurface, stratmod::GraphOfFunction);

#endif
