/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#ifndef STRATMOD_EXTRUDED_SURFACE_HPP
#define STRATMOD_EXTRUDED_SURFACE_HPP

#include <memory>
#include <optional>

#include "stratmod/surfaces/input_surface.hpp"

namespace stratmod {

class ExtrudedSurface : public InputSurface 
{
    public:
        virtual bool differentiable() const override;

        bool setLinearExtrusion(const Eigen::MatrixXd& cross_section_curve);
        bool setPathGuidedExtrusion(const Eigen::MatrixXd& cross_section_curve, const Eigen::MatrixXd& path_curve);

        void setCrossSectionOrigin(const Eigen::Vector2d& origin);
        bool setCrossSectionDirection(const Eigen::Vector2d& direction);

        bool getData() const;

        Eigen::Vector2d getCrossSectionOrigin();
        Eigen::Vector2d getCrossSectionDirection();

        std::optional<Eigen::MatrixXd> getCrossSectionCurve();
        std::optional<Eigen::MatrixXd> getPathCurve();

        bool operator==(const ExtrudedSurface& rhs) const
        {
            bool equal = InputSurface::operator==(rhs);

            equal &= (cross_section_is_set_ == rhs.cross_section_is_set_);
            equal &= (cross_section_ == rhs.cross_section_);
            equal &= (cross_section_curve_ == rhs.cross_section_curve_);

            equal &= (path_is_set_ == rhs.path_is_set_);
            equal &= (path_ == rhs.path_);
            equal &= (path_curve_ == rhs.path_curve_);

            /* equal &= (origin_ == rhs.origin_); */
            /* equal &= (direction_ == rhs.direction_); */

            return equal;
        }

    protected:
        virtual double evalHeight(const Eigen::Vector2d& /* p */) const override;

        virtual Eigen::Vector2d evalDerivative(const Eigen::Vector2d& /* p */) const override;

        virtual stratmod::ExtrudedSurface* doClone() const override { return new ExtrudedSurface(*this); }

    private:
        class CurveInterpolant
        {
            public:
                double operator()(double x) const;
                double D(double x) const;
                bool set(const Eigen::MatrixXd& points, double smoothing_parameter);
                bool operator==(const CurveInterpolant& rhs) const;

            private:
                bool interpolant_is_set_ = false;
                Eigen::VectorXd centers_;
                Eigen::VectorXd weights_;

                // Cereal provides an easy way to serialize objects
                friend class cereal::access;

                template<typename Archive>
                void serialize(Archive& ar, const std::uint32_t /* version */)
                {
                    ar(interpolant_is_set_);
                    ar(centers_);
                    ar(weights_);
                }

        };

        bool cross_section_is_set_ = false;
        CurveInterpolant cross_section_;
        Eigen::MatrixXd cross_section_curve_;

        bool path_is_set_ = false;
        CurveInterpolant path_;
        Eigen::MatrixXd path_curve_;

        double smoothing_parameter_ = 1E-7;

        /* Eigen::Vector2d origin_ = Eigen::Vector2d::Zero(); */
        /* Eigen::Vector2d direction_ = Eigen::Vector2d::UnitX(); */

        // Cereal provides an easy way to serialize objects
        friend class cereal::access;

        template<typename Archive>
        void save(Archive& ar, const std::uint32_t /* version */) const
        {
            ar(cereal::base_class<stratmod::InputSurface>(this));

            ar(cross_section_is_set_);
            ar(cross_section_);
            ar(cross_section_curve_);

            ar(path_is_set_);
            ar(path_);
            ar(path_curve_);

            ar(smoothing_parameter_);

            /* ar(origin_); */
            /* ar(direction_); */
        }

        template<typename Archive>
        void load(Archive& ar, const std::uint32_t /* version */)
        {
            ar(cereal::base_class<stratmod::InputSurface>(this));

            ar(cross_section_is_set_);
            ar(cross_section_);
            ar(cross_section_curve_);

            ar(path_is_set_);
            ar(path_);
            ar(path_curve_);

            ar(smoothing_parameter_);

            /* ar(origin_); */
            /* ar(direction_); */
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
};

} // namespace stratmod

CEREAL_CLASS_VERSION(stratmod::ExtrudedSurface, 0);

CEREAL_REGISTER_TYPE(stratmod::ExtrudedSurface);
CEREAL_REGISTER_POLYMORPHIC_RELATION(stratmod::InputSurface, stratmod::ExtrudedSurface);

#endif
