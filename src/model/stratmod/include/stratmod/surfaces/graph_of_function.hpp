/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#ifndef STRATMOD_GRAPH_OF_FUNCTION_SURFACE_HPP
#define STRATMOD_GRAPH_OF_FUNCTION_SURFACE_HPP


#include <optional>

#include "cereal/access.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/types/polymorphic.hpp"
#include "stratmod/misc/serialization_archives.hpp"
#include "stratmod/misc/eigen_cereal_support.hpp"

#include "stratmod/surfaces/abstract_surface.hpp"


namespace stratmod {

class GraphOfFunction : public AbstractSurface
{
    public:
        virtual ~GraphOfFunction() = 0;

        virtual bool differentiable() const override
        {
            return false;
        }

        virtual double operator()(const Eigen::Vector2d& p) const = 0;

    private:
        // Cereal provides an easy way to serialize objects
        friend class cereal::access;

        template<typename Archive>
        void save(Archive & ar, const std::uint32_t /* version */) const
        {
            ar(cereal::base_class<stratmod::AbstractSurface>(this));
        }

        template<typename Archive>
        void load(Archive & ar, const std::uint32_t /* version */) 
        {
            ar(cereal::base_class<stratmod::AbstractSurface>(this));
        }
};

} // namespace stratmod

CEREAL_CLASS_VERSION(stratmod::GraphOfFunction, 0);

CEREAL_REGISTER_TYPE(stratmod::GraphOfFunction);
CEREAL_REGISTER_POLYMORPHIC_RELATION(stratmod::AbstractSurface, stratmod::GraphOfFunction);

#endif
