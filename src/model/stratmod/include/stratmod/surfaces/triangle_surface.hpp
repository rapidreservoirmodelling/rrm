/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#ifndef STRATMOD_TRIANGLE_SURFACE_HPP
#define STRATMOD_TRIANGLE_SURFACE_HPP

#include "stratmod/surfaces/input_surface.hpp"

namespace igl {
    template<typename VertexMatrixType, int dim>
    class AABB;
}

namespace stratmod {

class TriangleSurface : public InputSurface 
{
    public:
        TriangleSurface() = default;
        virtual ~TriangleSurface() = default;

        TriangleSurface(const TriangleSurface&) = default;
        TriangleSurface& operator=(const TriangleSurface&) = default;

        TriangleSurface(TriangleSurface&&) = default;
        TriangleSurface& operator=(TriangleSurface&&) = default;

        virtual bool differentiable() const override;

        bool set(const Eigen::MatrixXd& vertices, const Eigen::MatrixXi& triangles);

        bool isDelaunay();

        bool getData(Eigen::MatrixXd& vertices) const;

        bool getData(Eigen::MatrixXd& vertices, Eigen::MatrixXd& triangles) const;

        Eigen::MatrixXd getInputVertices() const;
        Eigen::MatrixXi getInputTriangles() const;

        Eigen::MatrixXd getVertices() const;
        Eigen::MatrixXi getTriangles() const;

        Eigen::MatrixXi getBoundary() const;

    protected:
        virtual double evalHeight(const Eigen::Vector2d& /* p */) const override;

        virtual Eigen::Vector2d evalDerivative(const Eigen::Vector2d& /* p */) const override;

        virtual stratmod::TriangleSurface* doClone() const override { return new TriangleSurface(*this); }

        bool surface_is_set_ = false;
        Eigen::MatrixXd input_vertices_;
        Eigen::MatrixXi input_triangles_;
        bool is_delaunay_ = false;
        Eigen::MatrixXd vertices_;
        Eigen::VectorXd heights_;
        Eigen::MatrixXi triangles_;
        bool convex_hull_is_set_ = false;
        Eigen::MatrixXi convex_hull_;
        Eigen::MatrixXi boundary_;

        struct TriangleSurfaceImpl;
        std::shared_ptr<TriangleSurfaceImpl> pimpl_;

        void resetImpl();

    private:
        // Cereal provides an easy way to serialize objects
        friend class cereal::access;

        template<typename Archive>
        void save(Archive& ar, const std::uint32_t /* version */) const
        {
            ar(cereal::base_class<stratmod::InputSurface>(this));

            ar(surface_is_set_);
            ar(input_vertices_);
            ar(input_triangles_);
            ar(is_delaunay_);
            ar(vertices_);
            ar(heights_);
            ar(triangles_);
            ar(boundary_);
            ar(convex_hull_is_set_);
            ar(convex_hull_);

            /* resetImpl(); */
        }

        template<typename Archive>
        void load(Archive& ar, const std::uint32_t /* version */)
        {
            ar(cereal::base_class<stratmod::InputSurface>(this));

            ar(surface_is_set_);
            ar(input_vertices_);
            ar(input_triangles_);
            ar(is_delaunay_);
            ar(vertices_);
            ar(heights_);
            ar(triangles_);
            ar(boundary_);
            ar(convex_hull_is_set_);
            ar(convex_hull_);

            resetImpl();
        }
};

} // namespace stratmod

CEREAL_CLASS_VERSION(stratmod::TriangleSurface, 0);

CEREAL_REGISTER_TYPE(stratmod::TriangleSurface);
CEREAL_REGISTER_POLYMORPHIC_RELATION(stratmod::InputSurface, stratmod::TriangleSurface);

#endif
