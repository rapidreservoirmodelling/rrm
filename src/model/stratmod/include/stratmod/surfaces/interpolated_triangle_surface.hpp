/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#ifndef STRATMOD_INTERPOLATED_TRIANGLE_SURFACE_HPP
#define STRATMOD_INTERPOLATED_TRIANGLE_SURFACE_HPP

#include "stratmod/surfaces/triangle_surface.hpp"
#include "stratmod/surfaces/smooth_interpolated_surface.hpp"

namespace stratmod {

class InterpolatedTriangleSurface : public TriangleSurface 
{
    public:
        InterpolatedTriangleSurface() = default;
        ~InterpolatedTriangleSurface() = default;

        InterpolatedTriangleSurface(const InterpolatedTriangleSurface&) = default;
        InterpolatedTriangleSurface& operator=(const InterpolatedTriangleSurface&) = default;

        InterpolatedTriangleSurface(InterpolatedTriangleSurface&&) = default;
        InterpolatedTriangleSurface& operator=(InterpolatedTriangleSurface&&) = default;

        virtual bool differentiable() const override;

        bool set(const Eigen::MatrixXd& points, double smoothing_parameter = 1E-7);
        bool set(const Eigen::MatrixXd& vertices, const Eigen::MatrixXi& triangles, double smoothing_parameter = 1E-7);

        bool resampleTriangleSurface(const Eigen::MatrixXd& vertices, const Eigen::MatrixXi& triangles);

        Eigen::MatrixXd getInputVertices() const;
        Eigen::MatrixXi getInputTriangles() const;

        Eigen::MatrixXd getVertices() const;
        Eigen::MatrixXi getTriangles() const;
        Eigen::MatrixXi getBoundary() const;

    protected:
        virtual double evalHeight(const Eigen::Vector2d& /* p */) const override;

        virtual Eigen::Vector2d evalDerivative(const Eigen::Vector2d& /* p */) const override;

        virtual stratmod::InterpolatedTriangleSurface* doClone() const override { return new InterpolatedTriangleSurface(*this); }

    private:
        SmoothInterpolatedSurface isurf_;

        // Cereal provides an easy way to serialize objects
        friend class cereal::access;

        template<typename Archive>
        void save(Archive& ar, const std::uint32_t /* version */) const
        {
            ar(cereal::base_class<stratmod::TriangleSurface>(this));

            ar(isurf_);
        }

        template<typename Archive>
        void load(Archive& ar, const std::uint32_t /* version */)
        {
            ar(cereal::base_class<stratmod::TriangleSurface>(this));

            ar(isurf_);
        }
};

} // namespace stratmod

CEREAL_CLASS_VERSION(stratmod::InterpolatedTriangleSurface, 0);

CEREAL_REGISTER_TYPE(stratmod::InterpolatedTriangleSurface);
CEREAL_REGISTER_POLYMORPHIC_RELATION(stratmod::TriangleSurface, stratmod::InterpolatedTriangleSurface);

#endif
