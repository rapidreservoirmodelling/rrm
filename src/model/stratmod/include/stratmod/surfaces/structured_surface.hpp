/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#ifndef STRATMOD_STRUCTURED_SURFACE_HPP
#define STRATMOD_STRUCTURED_SURFACE_HPP

#include "stratmod/surfaces/input_surface.hpp"

namespace stratmod {

class StructuredSurface : public InputSurface 
{
    public:
        virtual bool differentiable() const override;

        bool setHeights(const Eigen::MatrixXd& heights);
        void setDomainOrigin(const Eigen::Vector2d& origin);
        void setDomainCenter(const Eigen::Vector2d& center);
        bool setDomainSize(const Eigen::Vector2d& size);
        void setDomainAngle(double angle_radians);

        bool getData(Eigen::MatrixXd& data) const;

    protected:
        virtual double evalHeight(const Eigen::Vector2d& /* p */) const override;

        virtual Eigen::Vector2d evalDerivative(const Eigen::Vector2d& /* p */) const override;

        virtual stratmod::StructuredSurface* doClone() const override { return new StructuredSurface(*this); }

    private:
        bool surface_is_set_ = false;
        Eigen::VectorXd origin_ = Eigen::Vector2d::Zero();
        Eigen::VectorXd size_ = Eigen::Vector2d::Ones();
        double angle_ = 0.;

        // Cereal provides an easy way to serialize objects
        friend class cereal::access;

        template<typename Archive>
        void serialize(Archive& ar, const std::uint32_t /* version */)
        {
            ar(cereal::base_class<stratmod::InputSurface>(this));

            ar(surface_is_set_);
            ar(origin_);
            ar(size_);
        }
};

} // namespace stratmod

CEREAL_CLASS_VERSION(stratmod::StructuredSurface, 0);

/* CEREAL_REGISTER_TYPE(stratmod::StructuredSurface); */
/* CEREAL_REGISTER_POLYMORPHIC_RELATION(stratmod::InputSurface, stratmod::StructuredSurface); */

#endif
