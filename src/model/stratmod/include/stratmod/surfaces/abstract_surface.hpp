/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#ifndef STRATMOD_ABSTRACT_SURFACE_HPP
#define STRATMOD_ABSTRACT_SURFACE_HPP

#include <memory>

#include "cereal/access.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/types/polymorphic.hpp"
#include "stratmod/misc/serialization_archives.hpp"

#include "stratmod/metadata.hpp"
#include "stratmod/metadata_cereal.hpp"

namespace stratmod {

class AbstractSurface 
{
    public:
        virtual ~AbstractSurface() = 0;

        virtual bool differentiable() const = 0;

        stratmod::SurfaceMetadata& metadata() &;

        std::shared_ptr<stratmod::SurfaceMetadata>& metadataPtr() &;

        std::shared_ptr<stratmod::SurfaceMetadata> metadataPtr() const &;

        std::shared_ptr<stratmod::SurfaceMetadata> metadataPtr() &&;

    private:
        std::shared_ptr<stratmod::SurfaceMetadata> pmetadata_;

        // Cereal provides an easy way to serialize objects
        friend class cereal::access;

        template<typename Archive>
        void save(Archive & ar, const std::uint32_t /* version */) const 
        {
            ar(pmetadata_);
        }

        template<typename Archive>
        void load(Archive & ar, const std::uint32_t /* version */) 
        {
            ar(pmetadata_);
        }
};

}

CEREAL_CLASS_VERSION(stratmod::AbstractSurface, 0);
/* CEREAL_REGISTER_TYPE(stratmod::AbstractSurface); */

#endif
