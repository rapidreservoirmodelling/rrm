#ifndef STRATMOD_INCLUDE_STRATIGRAPHIC_SURFACES_HPP
#define STRATMOD_INCLUDE_STRATIGRAPHIC_SURFACES_HPP

#include "stratmod/surfaces/smooth_interpolated_surface.hpp"
#include "stratmod/surfaces/extruded_surface.hpp"
#include "stratmod/surfaces/triangle_surface.hpp"
/* #include "stratmod/surfaces/structured_surface.hpp" */

#endif
