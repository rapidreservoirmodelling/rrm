/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#ifndef STRATMOD_THIN_PLATE_SPLINE_SURFACE_HPP
#define STRATMOD_THIN_PLATE_SPLINE_SURFACE_HPP

#include "stratmod/surfaces/input_surface.hpp"

namespace stratmod {

class SmoothInterpolatedSurface : public InputSurface 
{
    public:
        virtual bool differentiable() const override;

        bool set(const Eigen::MatrixXd& points, double smoothing_parameter = 1E-7);

        bool getData(Eigen::MatrixXd& data) const;

        bool operator==(const SmoothInterpolatedSurface& rhs) const
        {
            bool equal = InputSurface::isEqual(rhs);

            equal &= (interpolant_is_set_ == rhs.interpolant_is_set_);
            equal &= (points_ == rhs.points_);
            equal &= (smoothing_parameter_ == rhs.smoothing_parameter_);
            equal &= (centers_ == rhs.centers_);
            equal &= (weights_ == rhs.weights_);

            return equal;
        }

    protected:
        virtual double evalHeight(const Eigen::Vector2d& /* p */) const override;

        virtual Eigen::Vector2d evalDerivative(const Eigen::Vector2d& /* p */) const override;

        virtual stratmod::SmoothInterpolatedSurface* doClone() const override { return new SmoothInterpolatedSurface(*this); }

    private:
        bool interpolant_is_set_ = false;
        Eigen::MatrixXd points_;
        double smoothing_parameter_ = 1E-7;
        Eigen::MatrixXd centers_;
        Eigen::VectorXd weights_;

        friend class stratmod::InterpolatedTriangleSurface;

        // Cereal provides an easy way to serialize objects
        friend class cereal::access;

        template<typename Archive>
        void save(Archive& ar, const std::uint32_t /* version */) const
        {
            ar(cereal::base_class<stratmod::InputSurface>(this));

            /* LOG(INFO) << "Call save method in stratmod::SmoothInterpolatedSurface"; */
            ar(interpolant_is_set_);
            ar(points_);
            ar(smoothing_parameter_);
            ar(centers_);
            ar(weights_);
        }

        template<typename Archive>
        void load(Archive& ar, const std::uint32_t /* version */)
        {
            ar(cereal::base_class<stratmod::InputSurface>(this));

            /* LOG(INFO) << "Call load method in stratmod::SmoothInterpolatedSurface"; */
            ar(interpolant_is_set_);
            ar(points_);
            ar(smoothing_parameter_);
            ar(centers_);
            ar(weights_);
        }
};

using InterpolatedSurface = SmoothInterpolatedSurface;

} // namespace stratmod

CEREAL_CLASS_VERSION(stratmod::SmoothInterpolatedSurface, 0);

CEREAL_REGISTER_TYPE(stratmod::SmoothInterpolatedSurface);
CEREAL_REGISTER_POLYMORPHIC_RELATION(stratmod::InputSurface, stratmod::SmoothInterpolatedSurface);

#endif
