/* This file is part of the stratmod library */
/* Copyright (C) 2017-2020, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#ifndef STRATMOD_SURFACE_DESCRIPTOR
#define STRATMOD_SURFACE_DESCRIPTOR

#include <optional>

#include "cereal/access.hpp"
#include "stratmod/misc/serialization_archives.hpp"

#include "stratmod/misc/stratmod_primitives.hpp"
#include "stratmod/metadata.hpp"
#include "stratmod/metadata_cereal.hpp"

namespace stratmod {

using SurfaceUniqueIdentifier = std::size_t;
using SurfaceGroupIdentifier = std::int32_t;

class STRATMODLIB_DLL_HANDLER SurfaceDescriptor {
    public:
        SurfaceMetadata metadata{};

        SurfaceDescriptor(SurfaceUniqueIdentifier id);

        SurfaceDescriptor(SurfaceUniqueIdentifier id, SurfaceGroupIdentifier gid);

        ~SurfaceDescriptor() = default;

        SurfaceDescriptor(const SurfaceDescriptor&) = default;
        SurfaceDescriptor& operator=(const SurfaceDescriptor&) = default;

        SurfaceDescriptor(SurfaceDescriptor&&) = default;
        SurfaceDescriptor& operator=(SurfaceDescriptor&&) = default;

        SurfaceUniqueIdentifier uniqueId() const;
        std::optional<SurfaceGroupIdentifier> groupId() const;

    private:
        SurfaceUniqueIdentifier unique_id_{};
        std::int64_t group_id_{};

        SurfaceDescriptor() = default;

        friend class cereal::access;
        template<typename T, typename B> friend struct cereal::detail::Version;

        template<typename Archive>
        void serialize(Archive& ar, const std::uint32_t /* version */)
        {
            ar(unique_id_);
            ar(group_id_);
            ar(metadata);
        }

        friend struct SModellerImplementation;
};

} // namespace stratmod

CEREAL_CLASS_VERSION(stratmod::SurfaceDescriptor, 0);

#endif
