/********************************************************************************/
/*                                                                              */
/* This file is part of the "Stratigraphy Modeller Library" (stratmod)          */
/* Copyright (C) 2017, Julio Daniel Machado Silva.                              */
/*                                                                              */
/* StratModLib is free software; you can redistribute it and/or                 */
/* modify it under the terms of the GNU Lesser General Public                   */
/* License as published by the Free Software Foundation; either                 */
/* version 3 of the License, or (at your option) any later version.             */
/*                                                                              */
/* StratModLib is distributed in the hope that it will be useful,               */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU            */
/* Lesser General Public License for more details.                              */
/*                                                                              */
/* You should have received a copy of the GNU Lesser General Public             */
/* License along with StratModLib.  If not, see <http://www.gnu.org/licenses/>, */
/* or write to the Free Software Foundation, Inc., 51 Franklin Street,          */
/* Fifth Floor, Boston, MA  02110-1301  USA.                                    */
/*                                                                              */
/********************************************************************************/



#ifndef STRATMOD_SMODELLER2_HPP
#define STRATMOD_SMODELLER2_HPP

#include <vector>
#include <memory>
#include <optional>

#include "stratmod/misc/smodeller_primitives.hpp"
#include "stratmod/surface_descriptor.hpp"
#include "stratmod/surfaces/stratigraphic_surfaces.hpp"

#include "stratmod/model_interpretation.hpp"


namespace stratmod {

/** 
 * @brief Creates consistent surface based models.
 *
 * This class provides the user API of the stratmod2 library.
 **/
class STRATMODLIB_DLL_HANDLER SModeller2
{
    public:
        ///////////////////////////////////////////////////////////////////////
        //
        // Constructors, maintenance, etc.
        //
        ///////////////////////////////////////////////////////////////////////

        /** @brief Get reference to unique SModeller instance.
         **/
        static SModeller2& Instance();

        /** @brief Get reference to unique SModeller instance and clear it.
         **/
        static SModeller2& ClearInstance();

        /**
         * @brief Default destructor.
         **/
        virtual ~SModeller2(); 

        /** 
         * @brief Deleted copy constructor.  
         **/
        SModeller2( const SModeller2 &m ) = delete;

        /** 
         * @brief Deleted move constructor.  
         **/
        SModeller2( SModeller2 &&m ) = delete; 

        /** 
         * @brief Deleted copy assignment constructor.  
         **/
        SModeller2& operator=( const SModeller2 &m ) = delete;

        /** 
         * @brief Deleted move assignment constructor.  
         **/
        SModeller2& operator=( SModeller2 &&m ) = delete; 

        /**
         * @brief Clear model.
         **/
        void clear();

        /**
         * @brief Save model to binary file..
         *
         * @param filename Name of file.
         *
         * @return True if model was saved correctly.
         **/
        bool saveBinary( std::string filename );

        /**
         * @brief Load model from binary file..
         *
         * @param filename Name of file.
         *
         * @return True if model was loaded correctly.
         **/
        bool loadBinary( std::string filename );

        /**
         * @brief Save model to json file..
         *
         * @param filename Name of file.
         *
         * @return True if model was saved correctly.
         **/
        bool saveJSON( std::string filename );

        /**
         * @brief Load model from json file..
         *
         * @param filename Name of file.
         *
         * @return True if model was loaded correctly.
         **/
        bool loadJSON( std::string filename );


        ///////////////////////////////////////////////////////////////////////
        //
        // Model geometry and discretization
        //
        ///////////////////////////////////////////////////////////////////////

        /**
         * @brief Set coordinate system such that x = width, y = length, z = height.
         *
         * Within the default coordinate system, all methods that receive or return 
         * vectors of triplets with geometry expect the list be organized as follows:
         *
         * {x_0, y_0, z_0, x_1, y_1, z_1, ...}
         **/
        [[deprecated("SModeller2 ASSUMES the 'Default Coordinate System' is used")]]
        bool useDefaultCoordinateSystem();

        /**
         * @brief Set coordinate system such that x = width, y = height, z = length.
         *
         * Within the OpenGL coordinate system, all methods that receive or return 
         * vectors of triplets with geometry expect the list be organized as follows:
         *
         * {x_0, z_0, y_0, x_1, z_1, y_1, ...}
         **/
        [[deprecated("SModeller2 ASSUMES the 'Default Coordinate System' is used, users MUST KNOW how the stratmod2 library internally works to use the 'OpenGL Coordinate System'")]]
        bool useOpenGLCoordinateSystem();

        /** 
         * @brief Get number of pieces in which the width direction is discretized.
         *
         * @return Discretization number
         **/
        std::size_t getWidthDiscretization() const;

        /** 
         * @brief Get number of pieces in which the length direction is discretized.
         *
         * @return Discretization number
         **/
        std::size_t getLengthDiscretization() const;

        /**
         * @brief Set model's bounding box origin.
         *
         * @param x,y,z Origin coordinates
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        void setOrigin( double x, double y, double z );

        /**
         * @brief Set model's bounding box size.
         *
         * @param x,y,z Size coordinates
         *
         * @return True if size is set correctly, i.e., x, y, z > 0
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        bool setSize( double x, double y, double z );

        /**
         * @brief Get model's bounding box origin.
         *
         * @param x,y,z Origin coordinates
         **/
        void getOrigin( double &x, double &y, double &z );

        /**
         * @brief Get model's bounding box size.
         *
         * @param x,y,z Size coordinates
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        void getSize( double &x, double &y, double &z );

        /**
         * @brief Change model's current discretization.
         *
         * @param width_discretization Discretization of widthwise cross-sections.
         * @param length_discretization Discretization of lengthwise cross-sections.
         *
         * @return True if discretization is set correctly, i.e., width_discretization, length_discretization > 0
         **/
        bool changeDiscretization( std::size_t width_discretization = 64, std::size_t length_discretization = 64 );

        /**
         * @brief Change model z-scale.
         *
         * Change model z-scale by a scale factor, if scale > 0.
         *
         * @param scale Parameter, must be greater than 0.
         *
         * @return True if scale > 0.
         **/
        bool scaleModelHeight(double scale = 1.0);


        ///////////////////////////////////////////////////////////////////////
        //
        // Geologic Operators:
        //
        // The following methods change the state of the modelling library.
        //
        ///////////////////////////////////////////////////////////////////////

        //
        // Operators that change newly inserted surfaces
        //

        /**
         * @brief PreserveAbove.
         *
         * Select the surfaces given by list bounding_surfaces_list 
         * as a lower boundary for surface creation, i.e., all parts of new surfaces
         * that lie below bounding_surfaces_list will be removed.
         *
         * @param bounding_surfaces_list Lower boundary for creation of new surfaces.
         *
         * @return True if bounding_surfaces_list is a valid lower boundary for PreserveAbove.
         **/
        bool preserveAbove( const std::vector<size_t> &bounding_surfaces_list );

        /**
         * @brief PreserveBelow.
         *
         * Select the surfaces given by list bounding_surfaces_list 
         * as an upper boundary for surface creation, i.e., all parts of new surfaces
         * that lie above bounding_surfaces_list will be removed.
         *
         * @param bounding_surfaces_list Upper boundary for creation of new surfaces.
         *
         * @return True if bounding_surfaces_list is a valid upper boundary for PreserveBelow.
         **/
        bool preserveBelow( const std::vector<size_t> &bounding_surfaces_list );

        /**
         * @brief Stop PreserveAbove.
         **/
        void stopPreserveAbove();

        /**
         * @brief Stop PreserveBelow.
         **/
        void stopPreserveBelow();

        /**
         * @brief Tells whether PreserveAbove is currently active.
         *
         * @param[out] bounding_surfaces_list List of surfaces indices of lower boundary.
         *
         * @return True PreserveAbove is active.
         **/
        bool preserveAboveIsActive( std::vector<std::size_t> &bounding_surfaces_list );

        /**
         * @brief Tells whether PreserveBelow is currently active.
         *
         * @param[out] bounding_surfaces_list List of surfaces indices of upper boundary.
         *
         * @return True PreserveBelow is active.
         **/
        bool preserveBelowIsActive( std::vector<std::size_t> &bounding_surfaces_list );

        //
        // Operators that change old, previously inserted surfaces
        //

        /**
         * @brief Apply RemoveAbove (RA) operator
         **/
        void removeAbove();

        /**
         * @brief Apply RemoveAboveIntersection (RAI) operator
         **/
        void removeAboveIntersection();

        /**
         * @brief Apply RemoveBelow (RB) operator
         **/
        void removeBelow();

        /**
         * @brief Apply RemoveBelowIntersection (RBI) operator
         **/
        void removeBelowIntersection();

        /**
         * @brief Disable RA, RB, RAI, RBI
         **/
        void disableGeologicRules();

        //
        // Change model by removing or restoring surfaces
        //

        /**
         * @brief Returns true is there is at least one surface in the model that can be undone
         **/
        bool canUndo();

        /**
         * @brief Undo the last surface insertion
         *
         * @return True is undo() was performed
         **/
        bool undo();

        /**
         * @brief Returns true is there is at least one surface in the undo stack that can be restored to the model
         **/
        bool canRedo();

        /**
         * @brief Redo the last undoed surface
         *
         * @return True is redo() was performed
         **/
        bool redo();

        /**
         * @brief Destroy last surface inserted in model (this operation can't be undone).
         **/
        bool destroyLastSurface();


        ///////////////////////////////////////////////////////////////////////
        //
        // Insert surfaces in model, query ids of surfaces already inserted
        // in model.
        //
        ///////////////////////////////////////////////////////////////////////

        /**
         * @brief Get list of surfaces indices in their insertion order
         **/
        std::vector<std::size_t> getSurfacesIndices();

        /**
         * @brief Get list of surfaces indices in their stratigraphic order
         **/
        std::vector<std::size_t> getOrderedSurfacesIndices();
        
        /**
         * @brief Get list of stratigraphic surfaces indices in their insertion order
         **/
        std::vector<SurfaceUniqueIdentifier> getStratigraphicSurfacesIndices();

        /**
         * @brief Get list of fault surfaces indices in their insertion order
         **/
        std::vector<SurfaceUniqueIdentifier> getFaultSurfacesIndices();

        /**
         * @brief Add a stratigraphic InputSurface in the model.
         *
         * @param surface_id Index of surface.
         * @param isptr Pointer to InputSurface that is meant to be inserted in model.
         *
         * @return True Input surface has been added to the model.
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        bool addStratigraphicSurface(
                SurfaceUniqueIdentifier unique_id,
                const std::shared_ptr<stratmod::InputSurface>& isptr
                );

        /**
         * @brief Add a fault InputSurface in the model.
         *
         * @param surface_id Index of surface.
         * @param isptr Pointer to InputSurface that is meant to be inserted in model.
         *
         * @return True Input surface has been added to the model.
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        bool addFaultSurface(
                SurfaceUniqueIdentifier unique_id,
                const std::shared_ptr<stratmod::InputSurface>& isptr
                );

        /**
         * @brief Mark surface in model as stratigraphic surface.
         *
         * If method returns true, it means that surface of unique_id
         * is now a stratigraphic surface.
         *
         * @param surface_id Index of surface.
         *
         * @return True if unique_id corresponds to surface in the model.
         **/
        bool markAsStratigraphy(SurfaceUniqueIdentifier unique_id);

        /**
         * @brief Mark surface in model as fault surface.
         *
         * If method returns true, it means that surface of unique_id
         * is now a fault surface.
         *
         * @param surface_id Index of surface.
         *
         * @return True if unique_id corresponds to surface in the model.
         **/
        bool markAsFault(SurfaceUniqueIdentifier unique_id);

        /**
         * @brief Set a surface's descriptor.
         *
         * 
         * If group id is not defined, this method clears any previously saved
         * descriptor for the same unique surface id.
         *
         * @param descriptor SurfaceDescriptor meant to be set.
         *
         * @return True if descriptor.uniqueId() corresponds to surface in the model.
         **/
        bool setSurfaceDescriptor(const SurfaceDescriptor& descriptor);

        /**
         * @brief Get a surface's descriptor.
         *
         * @param unique_id Unique identifier of surface.
         *
         * @return Non-empty optional if unique_id corresponds to surface with
         * a descriptor saved in the model, empty optional otherwise.
         **/
        std::optional<SurfaceDescriptor> getSurfaceDescriptor(SurfaceUniqueIdentifier unique_id);

        /**
         * @brief Get all surfaces' descriptors matching a given group id.
         *
         * @param group_id Group identifier of surfaces.
         *
         * @return Non-empty vector if at least one surface belonging to group
         * group_id is found in model, empty vector otherwise.
         **/
        std::vector<SurfaceDescriptor> getSurfaceDescriptors(SurfaceGroupIdentifier group_id);

        /**
         * @brief Get pointer to input surface.
         *
         * This method returns a pointer to an input surface in the model, so
         * that it can be modified after being inserted in the model.
         *
         * Modifications are reflected in the model after the model is updated,
         * e.g. with a call to updateSurface().
         *
         * @param unique_id Surface's unique identifier.
         *
         * @return Pointer to input surface if model contains a surface of id unique_id.
         *
         * @sa updateSurface()
         **/
        std::shared_ptr<stratmod::InputSurface> getInputSurface(SurfaceUniqueIdentifier unique_id);

        /**
         * @brief Update model surface.
         *
         * This method updates a surface that may have been modified after it
         * was inserted in the model.
         *
         * @param surface_id Surface's unique identifier.
         *
         * @return True if model contains a surface with id surface_id.
         *
         * @sa getInputSurface()
         **/
        bool updateSurface(SurfaceUniqueIdentifier surface_id);

        /**
         * @brief Update model surface.
         *
         * This method replaces the input surface of the surface indexed by
         * surface_id and updates the model.
         *
         * @param surface_id Surface's unique identifier.
         *
         * @param isptr New input surface.
         *
         * @return True if model contains a surface with id surface_id.
         *
         * @sa getInputSurface()
         **/
        bool updateSurface(
                SurfaceUniqueIdentifier surface_id,
                const std::shared_ptr<stratmod::InputSurface>& isptr
                );


        ///////////////////////////////////////////////////////////////////////
        //
        // Get geometry and model properties for visualization and numerics.
        //
        ///////////////////////////////////////////////////////////////////////

        /**
         * @brief Build data structure that allows generation of high quality geometry.
         *
         * @param surfaces_indices Unique ids of all surfaces meant to be refined.
         *
         * @return True all entries in surfaces_indices correspond to surfaces in the model.
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        bool adaptSurfacesMeshes(const std::vector<SurfaceUniqueIdentifier>& surfaces_indices);

        /**
         * @brief Get cached (fast) triangle mesh of a surface.
         *
         * @param surface_id Index of surface.
         * @param vlist Surface's vertex list (vector of triplets {width, length, height})
         * @param tlist Surface's triangle list (vector of triplets of indices into the vertex list)
         *
         * @return True if mesh was retrieved.
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        bool getCachedMesh( std::size_t surface_id, std::vector<double> &vlist, std::vector<std::size_t> &tlist );

        /**
         * @brief Get high-quality triangle mesh of a surface.
         *
         * @param surface_id Index of surface.
         * @param vlist Surface's vertex list (vector of triplets {width, length, height})
         * @param tlist Surface's triangle list (vector of triplets of indices into the vertex list)
         *
         * @return True if mesh was retrieved.
         *
         * @sa adaptSurfacesMeshes(), useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        bool getAdaptedMesh( std::size_t surface_id, std::vector<double> &vlist, std::vector<std::size_t> &tlist );

        /**
         * @brief Get high-quality partial triangle mesh of a surface.
         *
         * If the input surface corresponding to "surface_id" inherits from
         * class TriangleSurface, them the partial mesh respects the input mesh
         * used to create the surface.
         *
         * @param surface_id Index of surface.
         * @param vlist Surface's vertex list (vector of triplets {width, length, height})
         * @param tlist Surface's triangle list (vector of triplets of indices into the vertex list)
         *
         * @return True if mesh was retrieved.
         *
         * @sa adaptSurfacesMeshes(), useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        bool getAdaptedPartialMesh( std::size_t surface_id, std::vector<double> &vlist, std::vector<std::size_t> &tlist );

        /**
         * @brief Get cached (fast) cross section curve of a surface along the width direction.
         *
         * @param surface_id Index of surface.
         * @param length Id of length cross-section curve lies.
         * @param vlist Surface's vertex list (vector of pairs {width, height})
         * @param flist Surface's triangle list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getCachedWidthCrossSectionCurve( std::size_t surface_id, std::size_t width, std::vector<double> &vlist, std::vector<std::size_t> &elist );

        /**
         * @brief Get high-quality cross section curve of a surface along the width direction.
         *
         * @param surface_id Index of surface.
         * @param width Id of width cross-section curve lies.
         * @param vlist Surface's vertex list (vector of pairs {length, height})
         * @param flist Surface's triangle list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         *
         * @sa adaptSurfacesMeshes()
         **/
        bool getAdaptedWidthCrossSectionCurve( std::size_t surface_id, std::size_t width, std::vector<double> &vlist, std::vector<std::size_t> &elist );

        /**
         * @brief Get high-quality, partial cross section curve of a surface along the width direction.
         *
         * If the input surface corresponding to "surface_id" inherits from
         * class TriangleSurface, them the partial curve respects the input mesh
         * used to create the surface.
         *
         *
         * @param surface_id Index of surface.
         * @param width Id of width cross-section curve lies.
         * @param vlist Surface's vertex list (vector of pairs {length, height})
         * @param flist Surface's triangle list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         *
         * @sa adaptSurfacesMeshes()
         **/
        bool getAdaptedPartialWidthCrossSectionCurve( std::size_t surface_id, std::size_t width, std::vector<double> &vlist, std::vector<std::size_t> &elist );

        /**
         * @brief Get cached (fast) cross section curve of a surface along the length direction.
         *
         * @param surface_id Index of surface.
         * @param length Id of length cross-section curve lies.
         * @param vlist Surface's vertex list (vector of pairs {width, height})
         * @param flist Surface's triangle list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getCachedLengthCrossSectionCurve( std::size_t surface_id, std::size_t lenght, std::vector<double> &vlist, std::vector<std::size_t> &elist );

        /**
         * @brief Get high-quality cross section curve of a surface along the length direction.
         *
         * @param surface_id Index of surface.
         * @param length Id of length cross-section curve lies.
         * @param vlist Surface's vertex list (vector of pairs {width, height})
         * @param flist Surface's triangle list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         **/
        bool getAdaptedLengthCrossSectionCurve( std::size_t surface_id, std::size_t lenght, std::vector<double> &vlist, std::vector<std::size_t> &elist );

        /**
         * @brief Get high-quality, partial cross section curve of a surface along the length direction.
         *
         * If the input surface corresponding to "surface_id" inherits from
         * class TriangleSurface, them the partial curve respects the input mesh
         * used to create the surface.
         *
         *
         * @param surface_id Index of surface.
         * @param length Id of width cross-section curve lies.
         * @param vlist Surface's vertex list (vector of pairs {length, height})
         * @param flist Surface's triangle list (vector of pairs of indices into the vertex list)
         *
         * @return True if curve was retrieved.
         *
         * @sa adaptSurfacesMeshes()
         **/
        bool getAdaptedPartialLengthCrossSectionCurve( std::size_t surface_id, std::size_t lenght, std::vector<double> &vlist, std::vector<std::size_t> &elist );

        /**
         * @brief Build a cached (fast) tetrahedral mesh adapted to the model from the
         * surfaces' cache.
         *
         * @return True and builds mesh if model has more than one surface.
         **/
        bool buildCachedTetrahedralMesh();

        /**
         * @brief Build a high-quality tetrahedral mesh adapted to the model.
         *
         * @return True and builds mesh if model has more than one surface.
         *
         * @sa adaptSurfacesMeshes()
         **/
        bool buildAdaptedTetrahedralMesh();

        /**
         * @brief Get model's tetrahedral mesh.
         *
         * This method will get whatever tetrahedral mesh was previous computed
         * with either buildTetrahedralMesh() or buildAdaptedTetrahedralMesh().
         * If a tetrahedral mesh was not previously computed, then this
         * method will attempt to build a fast tetrahedral mesh.
         *
         * @param vertex_coordinates Model's vertex coordinates list (vector of triplets {width, length, height})
         * @param element_list Vector of tetrahedrals' indices list (vector of triplets of indices into the vertex list)
         * @param attribute_list Vector of regions tetrahedrals belong to
         *
         * @return Number of tetrahedrals in mesh.
         *
         * @sa buildFastTetrahedralMesh(), buildAdaptedTetrahedralMesh()
         **/
        std::size_t getTetrahedralMesh( std::vector<double> &vertex_coordinates, std::vector<std::size_t> &element_list, std::vector<long int> &attribute_list );

        /**
         * @brief Compute volumes of regions.
         *
         * @param vlist List of volumes of regions.
         *
         * @return True if model has non-empty regions.
         **/
        bool computeTetrahedralMeshVolumes( std::vector<double> &vlist );

        /**
         * @brief Compute region ids from list of points.
         *
         * @param vcoords[in] Points' coordinates list (vector of triplets {width, length, height})
         * @param regions[out] Regions' ids corresponding to list of points.
         *
         * @return True if model has non-empty regions.
         **/
        bool getVolumeAttributesFromPointList( const std::vector<double> &vcoords, std::vector<int> &regions);

        /**
         * @brief Compute region ids from list of points.
         *
         * @param vcoords[in] Points' coordinates list (vector of triplets {width, length, height})
         * @param regions[out] Regions' ids corresponding to list of points.
         *
         * @return True if model has non-empty regions.
         **/
        /* bool getRegionsFromPointList( const std::vector<double> &vcoords, std::vector<int> &regions); */

        /**
         * @brief Compute list of bounding surfaces given a region id.
         *
         * @param attribute_id Region id.
         * @param lower_bound[out] Ids of surfaces below the given region.
         * @param upper_bound[out] Ids of surfaces above the given region.
         *
         * @return True if model has non-empty regions.
         **/
        bool getBoundingSurfacesFromVolumeAttribute( std::size_t attribute_id, std::vector<size_t> &lower_bound, std::vector<size_t> &upper_bound);

        /**
         * @brief Get ids of surfaces that lie below a given point.
         *
         * @param x,y,z Query point.
         *
         * @return List of surfaces' ids.
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        std::vector<size_t> getSurfacesIndicesBelowPoint( double x, double y, double z );

        /**
         * @brief Get ids of surfaces that lie above a given point.
         *
         * @param x,y,z Query point.
         *
         * @return List of surfaces' ids.
         *
         * @sa useDefaultCoordinateSystem(), useOpenGLCoordinateSystem()
         **/
        std::vector<size_t> getSurfacesIndicesAbovePoint( double x, double y, double z );


        ///////////////////////////////////////////////////////////////////////
        //
        // Manipulate volumetric model(s).
        //
        ///////////////////////////////////////////////////////////////////////

        /**
         * @brief Compute regions and return then as domains to be used in a
         * new ModelInterpretation (i.e., computed regions, assigned domains
         * and curves, and their associated metadata).
         *
         * @return ModelInterpretation with regions set as domains.
         *
         **/
        ModelInterpretation getRegions();

        /**
         * @brief Store model interpretations, i.e., computed regions, assigned
         * domains and curves, and their associated metadata.
         * 
         * This method is useful to save model interpratations alongside the
         * surface based model.
         *
         * @return Reference to a dictionary of ModelInterpretation(s).
         *
         **/
        std::unordered_map<std::string, ModelInterpretation>& interpretations();


        ///////////////////////////////////////////////////////////////////////
        /* From SUtilities (DON'T USE)*/
        ///////////////////////////////////////////////////////////////////////

        std::vector<std::vector<int>> computeRegionToDomainMap(std::string filename);


    private:
        SModeller2(); 

        std::unique_ptr<SModellerImplementation> pimpl_;

        SModellerImplementation& Impl();
        const SModellerImplementation& Impl() const;

        friend class SModeller;
        friend class SUtilities;

        friend class stratmod::detail::SModeller2Access;
};

} // namespace stratmod

#endif
