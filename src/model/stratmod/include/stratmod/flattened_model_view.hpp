/* This file is part of the stratmod library */
/* Copyright (C) 2017-2021, Julio Daniel Machado Silva. */

/* The stratmod library is free software: you can redistribute it and/or modify it */
/* under the terms of the GNU General Public License as published by the Free */
/* Software Foundation, either version 3 of the License, or (at your option) any */
/* later version. */

/* The stratmod library is distributed in the hope that it will be useful, but */
/* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more */
/* details. */

/* You should have received a copy of the GNU General Public License */
/* along with the stratmod library.  If not, see <https://www.gnu.org/licenses/>. */

#ifndef STRATMOD_FLATTENED_MODEL_VIEW
#define STRATMOD_FLATTENED_MODEL_VIEW

#include <memory>
#include <optional>
#include <vector>

#include "stratmod/smodeller2.hpp"

#include "Eigen/Dense"

namespace stratmod {

class STRATMODLIB_DLL_HANDLER FlattenedModelView {
    public:
        FlattenedModelView();
        virtual ~FlattenedModelView();
        FlattenedModelView(const FlattenedModelView&) = delete;
        FlattenedModelView& operator=(const FlattenedModelView&) = delete;
        FlattenedModelView(FlattenedModelView&&);
        FlattenedModelView& operator=(FlattenedModelView&&);

        static std::optional<FlattenedModelView> Get(SurfaceUniqueIdentifier surface_id, double height);

        std::optional<SurfaceUniqueIdentifier> flattenedSurfaceIndex();

        std::optional<double> flattenedSurfaceHeight();

        bool expired() const;

        template<typename Derived>
        void mapToFlattenedCoordinates(Eigen::DenseBase<Derived>& Points);

        template<typename Derived>
        void mapFromFlattenedCoordinates(Eigen::DenseBase<Derived>& Points);

        std::shared_ptr<stratmod::TriangleSurface> unFlattenSurface(std::shared_ptr<InputSurface> sptr);

    private:
        struct FlattenedModelViewImpl;
        std::unique_ptr<FlattenedModelViewImpl> pimpl_{};

        bool lock();
        void unlock();

        inline Eigen::RowVector3d fCoords(double x, double y, double z);
        inline Eigen::RowVector3d wCoords(double x, double y, double z);
};

template<typename Derived>
void FlattenedModelView::mapToFlattenedCoordinates(Eigen::DenseBase<Derived>& P)
{
    if (!expired() && (P.cols() == 3))
    {
        if (lock())
        {
            try {
                for (int i = 0; i < P.rows(); ++i)
                {
                    P.row(i) = fCoords(P(i, 0), P(i, 1), P(i, 2));
                }
            }
            catch(...) {}
            unlock();
        }
    }
}

template<typename Derived>
void FlattenedModelView::mapFromFlattenedCoordinates(Eigen::DenseBase<Derived>& P)
{
    if (!expired() && (P.cols() == 3))
    {
        if (lock())
        {
            try {
                for (int i = 0; i < P.rows(); ++i)
                {
                    P.row(i) = wCoords(P(i, 0), P(i, 1), P(i, 2));
                }
            }
            catch(...) {}
            unlock();
        }
    }
}

} // namespace stratmod

#endif
