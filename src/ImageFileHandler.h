/***************************************************************************

* This class handles storing and loading of Image Data and files.

* Copyright (C) 2022, Fazilatur Rahman.

* It converts the Image data to JSON string and saves it to the disk.
* Plus, when the model gets opened from .rrm file it loads corresponding Image files.

*****************************************************************************/

#ifndef IMAGEFILEHANDLER_H
#define IMAGEFILEHANDLER_H

#include <QObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include "Constants.h"

#define MAIN_LABEL          "IMAGE DATA"

class ImageFileHandler : public QObject
{
    Q_OBJECT

public:
    
    /**
    * Constructor.
    */
    ImageFileHandler(QString fileName);

    /**
    * Method that saves the IMAGE data to disk.
    * @return void
    */
    Q_INVOKABLE void save(QString imageData);
    
    /**
    * Method that laods the IMAGE data from disk.
    * @return bool
    */
    Q_INVOKABLE bool load();

    /**
    * Method that returns IMAGE data from disk.
    * @return QString
    */
    QString getJson();

private:
    void prepareJson();

    void setImageFolder();

    bool copyImageFile(const QString& sourceFile, const QString& destinationDir);

    std::string getAbsPath(const QUrl& path);

    QString cleanImageFolder(QString path);

    void parseJson();
    
    QString m_json_string;

    QString m_file_name;

    QString m_image_folder_path;

};


#endif // IMAGEFILEHANDLER_H
