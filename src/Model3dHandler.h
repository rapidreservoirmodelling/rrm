/*******************************************************************
  *  @file   Model3dHandler.h
  *  @brief  Pointer implementation of Model3dHandler (singleton)
  *  @author Fazilatur Rahman
  *  @date   2020-02-18
  ******************************************************************/

#ifndef MODEL3DHANDLER_H
#define MODEL3DHANDLER_H

#include <vector>
#include <QObject>
#include <QtQml>

//#include "model/smodeller_wrapper.h"
#include "model/flattened_model.h"

struct surfaceMetadata {
	int id;
	QString name = "";
	QString description = "";
	QString color;
	QString category = -1;
};

class Model3dHandler:public QObject
{
	Q_OBJECT
	Q_PROPERTY(double size_x MEMBER m_size_x NOTIFY size_x_Changed)
	Q_PROPERTY(double size_y MEMBER m_size_y NOTIFY size_y_Changed)
	Q_PROPERTY(double size_z MEMBER m_size_z NOTIFY size_z_Changed)
	Q_PROPERTY(int width_Discretization MEMBER m_width_Discretization NOTIFY width_Discretization_Changed)
	Q_PROPERTY(int length_Discretization MEMBER m_length_Discretization NOTIFY length_Discretization_Changed)
	Q_PROPERTY(double origin_x MEMBER m_origin_x NOTIFY origin_x_Changed)
	Q_PROPERTY(double origin_y MEMBER m_origin_y NOTIFY origin_y_Changed)
	Q_PROPERTY(double origin_z MEMBER m_origin_z NOTIFY origin_z_Changed)
	
public:

	/**
	* Method that generates the single instance
	*/
	static Model3dHandler* getInstance(QQmlEngine*);

	/**
	* Destructor.
	*/
	~Model3dHandler();

	/**
	* Method to reset properties to initial state.
	*/
	Q_INVOKABLE void reset();

	/**
	* Method that stores the selected cross-section's parameters.
	* @param crossSectionId is the position number
	* @param planeId denotes cross-section plane orientation id
	* @return void
	*/
	void saveSelectedCrossSection(int crossSectionId, int planeId);

	/**
	* Method that returns the dimension of the model.
	* @return void
	*/
	void getModelDimension(double& size_x, double& size_y, double& size_z);

	/**
	* Method that store the model dimension in private members.
	* @return void
	*/
	Q_INVOKABLE void setModelDimension();

	/**
	* Method that returns the discretization of the model.
	* @return void
	*/
	void getModelDiscretization(int& width, int& length);

	/**
	* Method that store the model discretization in private members.
	* @return void
	*/
	Q_INVOKABLE void setModelDiscretization();
	Q_INVOKABLE void setModelDiscretization(int discretization);

	/**
	* Method that returns the crossSectionId is the position number.
	* @return int
	*/
	int getCrossSectionId();
	
	/**
	* Method that returns the cross-section plane' orientation id.
	* @return int
	*/
	int getPlaneId();

	/**
	* Method that loads sketch and the 3D model data from a .rrm file
	* @param filename
	* @return bool
	*/
	bool loadFile(const QString& filename);

	/**
	* Method that saves the sketch and 3D model data to a .rrm file
	* This is an invokable method that may be called from QML
	* @param filename
	* @return bool
	*/
	Q_INVOKABLE bool saveFile(const QString& filename);

	/**
	* Method that sets the origin of the 3D bounding box
	* @param origin_x is the x-coordinate of the origin
	* @param origin_y is the y-coordinate of the origin 
	* @param origin_z is the z-coordinate of the origin
	* @return void
	*/
	Q_INVOKABLE void setOrigin(double origin_x, double origin_y, double origin_z);
	
	/**
	* Method that sets the size of the 3D bounding box
	* @param size_x is the size on the x direction
	* @param size_y is the size on the y direction
	* @param size_z is the size on the z direction
	* @return void
	*/
	Q_INVOKABLE bool setSize(double size_x, double size_y, double size_z);

	/**
	* Getter method that returns the origin of the 3D bounding box
	* @param origin_x is the x-coordinate of the origin
	* @param origin_y is the y-coordinate of the origin
	* @param origin_z is the z-coordinate of the origin
	* @return void
	*/
	void getOrigin(double& origin_x, double& origin_y, double& origin_z);
	
	/**
	* Getter method that returns the size of the 3D bounding box
	* @param size_x is the size on the x direction
	* @param size_y is the size on the y direction
	* @param size_z is the size on the z direction
	* @return void
	*/
	void getSize(double& size_x, double& size_y, double& size_z);

	/**
	* Getter method that returns the number of surfaces in the 3D data model 
	* @return std::vector<int>
	*/
	std::vector<int> getSurfaceIndices();

	/**
	* Update the local surface indice list using data from the model.
	*/
	void setSurfaceIndices();

	/**
	* Setter method that sets discretization for the 3D data model
	* @param discretization_width
	* @param discretization_length
	* @return bool
	*/
	bool setDiscretization(int discretization_width, int discretization_length);

	/**
	* Getter method that returns discretization for the 3D data model
	* @param discretization_width
	* @param discretization_length
	* @return bool
	*/
	bool getDiscretization(int& discretization_width, int& discretization_length);

	/**
	* Getter method that returns width discretization for the 3D data model
	* @return int
	*/
	Q_INVOKABLE int getWidthDiscretization();

	/**
	* Getter method that returns length discretization for the 3D data model
	* @return int
	*/
	Q_INVOKABLE int getLengthDiscretization();

	/**
	* Method that creates extruded surface
	* @param surface_id
	* @param curve
	* @return bool
	*/
	bool createExtrudedSurface(int surface_id, const std::vector<double>& curve);

	/**
	* Method that creates extruded surface in LENGTH direction
	* @param surface_id
	* @param curve
	* @return bool
	*/
	bool createLengthExtrudedSurface(int surface_id, const std::vector<double>& curve);
	
	/**
	* Method that creates extruded surface in WIDTH direction
	* @param surface_id
	* @param curve
	* @return bool
	*/
	bool createWidthExtrudedSurface(int surface_id, const std::vector<double>& curve);

	/**
	* Getter method that returns width cross-section curve
	* @param surface_id
	* @param cross_section_id
	* @param curve
	* @return bool
	*/
	bool getWidthCrossSectionCurve(
		int surface_id,
		int cross_section_id,
		std::vector<double>& curve);

	bool getWidthCrossSectionCurve(
		int surface_id,
		int cross_section_id,
		std::vector<double>& curve,
		std::vector<std::size_t>& indices);

	/**
	* Getter method that returns length cross-section curve
	* @param surface_id
	* @param cross_section_id
	* @param curve
	* @return bool
	*/
	bool getLenghtCrossSectionCurve(
		int surface_id,
		int cross_section_id,
		std::vector<double>& curve);

	bool getLenghtCrossSectionCurve(
		int surface_id,
		int cross_section_id,
		std::vector<double>& curve,
		std::vector<std::size_t>& indices);

	/**
	* Getter method that returns surface mesh
	* @param surface_id
	* @param vertex_coordinates
	* @param vertex_indices
	* @return bool
	*/
	bool getSurfaceMesh(
		int surface_id,
		std::vector<double>& vertex_coordinates,
		std::vector<std::size_t>& vertex_indices);

	/**
	* Getter method that returns surface mesh when the flattening mode is on
	* @param surface_id
	* @param vertex_coordinates
	* @param vertex_indices
	* @return bool
	*/
	bool getFlattenedSurfaceMesh(
		int surface_id,
		std::vector<double>& vertex_coordinates,
		std::vector<std::size_t>& vertex_indices);

	/**
	* Getter method that returns surface mesh when the flattening mode is on
	* @param vertex_coordinates
	* @param element_list
	* @return bool
	*/
	bool getFlattenedRegionsMeshes(std::vector<double>& vertex_coordinates, std::vector< std::vector<std::size_t> >& element_list);

	/**
	 * Get curves that bound region 'region_id' at a given lengthwise cross-section
	 * @return True if curve was retrieved.
	 **/
	bool getFlattenedLengthRegionCurveBox(std::size_t region_id, std::size_t cross_section_id,
		std::vector<double>& lower_bound_box_vlist, std::vector<std::size_t>& lower_bound_box_elist,
		std::vector<double>& upper_bound_box_vlist, std::vector<std::size_t>& upper_bound_box_elist);

	/**
	 * Get curves that bound region 'region_id' at a given widthwise cross-section
	 * @return True if curve was retrieved.
	 **/
	bool getFlattenedWidthRegionCurveBox(std::size_t region_id, std::size_t cross_section_id,
		std::vector<double>& lower_bound_box_vlist, std::vector<std::size_t>& lower_bound_box_elist,
		std::vector<double>& upper_bound_box_vlist, std::vector<std::size_t>& upper_bound_box_elist);

	/**
	 * Get cross section curve of a surface along the width direction.
	 * @return True if curve was retrieved.
	 **/
	bool getFlattenedWidthCrossSectionCurve(int surface_id, int cross_section_id,
		std::vector<double>& vertex_coordinates, std::vector<std::size_t>& vertex_indices);

	/**
	 * Get cross section curve of a surface along the length direction.
	 * @return True if curve was retrieved.
	 **/
	bool getFlattenedLengthCrossSectionCurve(int surface_id, int cross_section_id,
		std::vector<double>& vertex_coordinates, std::vector<std::size_t>& vertex_indices);

	/**
	 * Flatten a given surface at specified height.
	 **/
	bool FlattenSurface(int surface_id, double height);

	/**
	 * Stop flattening.
	 **/
	void StopFlattening();

	/**
	 * Returns true if flattened view is active.
	**/
	bool isFlatteningActive();

	/**
	 * Compute heights of the mapped model's bounding box
	 **/
	void ComputeBBoxHeights(double& min_height, double& max_height);

	/**
	* Compute average height of given curve vertices
	**/
	double computeCurveAverageHeight(const std::vector<double>& curve2d_points);

	/**
	 * Create a new surface from input points and add it to the model.
	 * @return True New surface has been added to the model.
	 **/
	bool CreateFlattenedSurface(int surface_id, const std::vector<double>& surface_points, double smoothing_factor = 0.0);

	/**
	 * Sample a new surface as a "lengthwise path guided surface".
	 * @return True New surface has been added to the model.
	 **/
	bool CreateFlattenedLengthPathGuidedSurface(int surface_id, const std::vector<double>& cross_section_curve, 
		double cross_section_position, const std::vector<double>& path_curve);

	/**
	 * Sample a new surface as a "widthwise path guided surface".
	 * @return True New surface has been added to the model.
	 **/
	bool CreateFlattenedWidthPathGuidedSurface(int surface_id, const std::vector<double>& cross_section_curve, 
		double cross_section_position, const std::vector<double>& path_curve);


	/**
	 * Create a new surface extruded along the given "lengthwise" path.
	 * @return True New surface has been added to the model.
	 **/
	bool CreateFlattenedLengthExtrudedSurface(int surface_id, const std::vector<double>& cross_section_curve, 
		double cross_section_position, const std::vector<double>& path_curve);

	/**
	 * Create a new surface extruded along the given "widthwise" path.
	 * @return True New surface has been added to the model.
	 **/
	bool CreateFlattenedWidthExtrudedSurface(int surface_id, const std::vector<double>& cross_section_curve, 
		double cross_section_position, const std::vector<double>& path_curve);

	/**
	 * Create a new surface linearly extruded along the model length.
	 * @return True New surface has been added to the model.
	 **/
	bool CreateFlattenedLengthExtrudedSurface(int surface_id, const std::vector<double>& curve_points);

	/**
	 * Create a new surface linearly extruded along the model width.
	 * @return True New surface has been added to the model.
	 **/
	bool CreateFlattenedWidthExtrudedSurface(int surface_id, const std::vector<double>& curve_points);

	/**
	 * Request model to PreserveRegion containing given point
	 * @return True if point lies in any of the model's regions.
	 **/
	bool RequestFlattenedPreserveRegion(const std::vector<double>& points);

	/**
	 * Request model to PreserveAbove selected curve
	 * @return True if PreserveAbove suceeded.
	 **/
	bool RequestFlattenedPreserveAbove(const std::vector<double>& sketch_points);

	/**
	 * Request model to PreserveBelow selected curve
	 * @return True if PreserveBelow suceeded.
	 **/
	bool RequestFlattenedPreserveBelow(const std::vector<double>& sketch_points);

	/**
	 * Get curve that bounds region being PreservedAbove at a given lengthwise cross-section
	 * @return True if curve was retrieved.
	 **/
	bool getFlattenedLengthPreserveAboveCurveBox(int cross_section_id, std::vector<double>& vlist, std::vector<size_t>& elist);

	/**
	 * Get curve that bounds region being PreservedAbove at a given widthwise cross-section
	 * @return True if curve was retrieved.
	 **/
	bool getFlattenedWidthPreserveAboveCurveBox(int cross_section_id, std::vector<double>& vlist, std::vector<size_t>& elist);

	/**
	 * Get curve that bounds region being PreservedBelow at a given lengthwise cross-section
	 * @return True if curve was retrieved.
	 **/
	bool getFlattenedLengthPreserveBelowCurveBox(int cross_section_id, std::vector<double>& vlist, std::vector<size_t>& elist);

	/**
	 * Get curve that bounds region being PreservedBelow at a given widthwise cross-section
	 * @return True if curve was retrieved.
	 **/
	bool getFlattenedWidthPreserveBelowCurveBox(int cross_section_id, std::vector<double>& vlist, std::vector<size_t>& elist);

	/**
	* Method that returns the SModellerWrapper instance
	* @return SModellerWrapper&
	*/
	model::SModellerWrapper& model();

	/**
	* Getter method that returns the width of the 3D bounding box
	* @return int
	*/
	Q_INVOKABLE int getWidth();
	
	/**
	* Getter method that returns the length of the 3D bounding box
	* @return int
	*/
	Q_INVOKABLE int getLength();

	/**
	* Getter method that returns the height of the 3D bounding box
	* @return int
	*/
	Q_INVOKABLE int getHeight();

	/**
	* Method that turns on the RA rule
	* This is an invokable method that may be called from QML
	* @return void
	*/
	Q_INVOKABLE void removeAbove();
	
	/**
	* Method that turns on the RB rule
	* This is an invokable method that may be called from QML
	* @return void
	*/
	Q_INVOKABLE void removeBelow();

	/**
	* Method that turns on the RAI rule
	* This is an invokable method that may be called from QML
	* @return void
	*/
	Q_INVOKABLE void removeAboveIntersection();

	/**
	* Method that turns on the RBI rule
	* This is an invokable method that may be called from QML
	* @return void
	*/
	Q_INVOKABLE void removeBelowIntersection();
	
	/**
	* Method that enables rules
	* @return void
	*/
	void enableRule();
	
	/**
	* Method that disables rules
	* @return void
	*/
	void disableRule();

	/**
	* Method that checks if rule is enabled
	* @return bool
	*/
	bool isRuleEnabled();

	/**
	* Method that returns the rule id
	* This is an invokable method that may be called from QML
	* @return int
	*/
	Q_INVOKABLE int getRemoveRuleId();

	/**
	* Method that sets the rule id
	* @return void
	*/
	void setRemoveRuleId(int ruleId);
	
	/**
	* Method that checks if PR is active
	* This is an invokable method that may be called from QML
	* @return bool
	*/
	Q_INVOKABLE bool isPreserveRegionActive();

	/**
	* Method that checks if PR is active
	* This is an invokable method that may be called from QML
	* @return bool
	*/
	Q_INVOKABLE bool isPreserveAboveActive();

	/**
	* Method that checks if PR is active
	* This is an invokable method that may be called from QML
	* @return bool
	*/
	Q_INVOKABLE bool isPreserveBelowActive();

	/**
	* Method that de-activates the PR
	* This is an invokable method that may be called from QML
	* @return void
	*/
	Q_INVOKABLE void stopPreserveRegion();

	/**
	* Method that de-activates the PA
	* This is an invokable method that may be called from QML
	* @return void
	*/
	Q_INVOKABLE void stopPreserveAbove();

	/**
	* Method that de-activates the PB
	* This is an invokable method that may be called from QML
	* @return void
	*/
	Q_INVOKABLE void stopPreserveBelow();

	/**
	* Method that clears the 3D data model when Reset New is activated
	* This is an invokable method that may be called from QML
	* @return void
	*/
	Q_INVOKABLE void resetNew();

	/**
	* Getter method for ordered surface indices
	* @return std::vector<std::size_t>
	*/
	std::vector<std::size_t> getOrderedSurfaceIndices();

	/**
	* Getter method for tetrahedral meshes
	* @return std::size_t
	*/
	std::size_t getTetraHedralMesh(
		std::vector<double> vertex_coordinates,
		std::vector<std::size_t> element_list,
		std::vector<long int> attribute_list
		);
	/**
		 * @brief Undo the last surface insertion
		 *
		 * @return True is undo() was performed
		 **/
	/**
	* Method that Undo the last surface insertion
	* This is an invokable method that may be called from QML
	* @return bool
	*/
	Q_INVOKABLE bool undo();

	/**
	* Method that Redo the last undoed surface
	* This is an invokable method that may be called from QML
	* @return void
	*/
	Q_INVOKABLE bool redo();

	/**
	* Method that exports to iRAP Grid
	* @return void
	*/
	Q_INVOKABLE bool exportToIrapGrid(const std::string& project_name);

	/**
	* Method that sets the region list
	* @return void
	* @param list is the list of regions containing volumes of each
	*/
	void setRegionsList(std::vector<double> list);

	/**
	* Method that returns region list size
	* @return std::vector<double>
	*/
	std::vector<double> getRegionList();

	bool surfaceIsStratigraphy(int id);
	bool surfaceIsStructure(int id);
	std::vector <int> getSurfaceType();
	int getSurfaceTypeById(int id);
	//bool getSurfaceData(std::vector<int>& idList, std::vector<std::string>& nameList, std::vector<std::string>& descriotionList, std::vector<std::optional<QColor>>& colorList, std::vector<int>& typeList);
	bool getSurfaceData(std::vector<int>& idList, std::vector<std::string>& nameList, std::vector<std::string>& descriptionList, std::vector<QColor>& colorList, std::vector<int>& typeList, std::vector<int>&  groupIdList);
	
	void clearDomains();
	void enforceDomainConsistency();
	void enforceRegionConsistency();
	bool eraseDomain(int domain_id);
	void setDomain(int domain_id, const QString& name, const QString& description);
	void setRegion(int region_id, const QString& name, const QString& description);
	bool setDomainColor(int domain_id, QColor c);
	bool setRegionColor(int domain_id, QColor c);
	bool addRegionIdToDomain(int domain_id, int region_id);
	bool eraseRegionIdFromDomain(int domain_id, int region_id);
	int getDomainIdByRegion(int region_id);
	std::vector<int> getRegionIdsByDomain(int domain_id);
	std::vector<int> getDomainList();
	//void getDomainMetadataById(int domain_id, QString* name, QString* description, QString* color);
	void getDomainMetadataById(int domain_id, QString& name, QString& description, QString& color);
	void getRegionMetadataById(int region_id, QString& name, QString& description, QString& color);
	void resetRegions();	

	// Surface Grouping
	void setSurfaceGroup(int surfaceId, int groupId);
	int getSurfaceGroup(int surfaceId);
	std::vector<int> getChildSurfacesOfAGroup(int groupId);

	bool setSurfaceMetadataById(int surfaceId, QString name, QString description, QString color);
	surfaceMetadata getSurfaceMetadataById(int surfaceId);

	void getGroupMetadataById(int groupId, QString& name, QString& description, QString& color);
	void setGroupMetadataById(int groupId, QString name, QString description, QString color);

	void setSurfaceData(int id, QString name, QString type, QString color);
	void getSurfaceData(int& id, QString& name, QString& type, QString& color);

	bool getVolumeAttributesFromPointList(const std::vector<double>& vcoords, std::vector<int>& regions);

	//- ADD by Sicilia to handle RegionMenu
	Q_INVOKABLE double getModelVolume();
	Q_INVOKABLE std::vector<double> getRegionListProportion();
	Q_INVOKABLE int getNumberOfRegions();
	Q_INVOKABLE double getRegionVolumeProportion(int regionID);
	Q_INVOKABLE void setDomainList(std::vector<double> list);
	Q_INVOKABLE std::vector<double> getDomainListProportion();
	Q_INVOKABLE int getNumberOfDomains();
	Q_INVOKABLE double getDomainVolumeProportion(int domainID);
	Q_INVOKABLE QString getDomainName(int domainID);

public slots:
	void setDimension(double w, double l, double h);
	
signals:
	void size_x_Changed();
	void size_y_Changed();
	void size_z_Changed();
	void width_Discretization_Changed();
	void length_Discretization_Changed();
	void origin_x_Changed();
	void origin_y_Changed();
	void origin_z_Changed();
	void dimensionChanged();
	void undoCompleted();
	void redoCompleted();
	void clearCompleted();

private:

	//- Singleton object handling of this class
	static bool instanceFlag;
	static Model3dHandler* single;
	
	//- Model Configuration
	double m_size_x = 500.0;
	double m_size_y = 500.0;
	double m_size_z = 500.0;
	int m_width_Discretization = 64; 
	int m_length_Discretization = 64;
	double m_origin_x = 0.0; 
	double m_origin_y = 0.0; 
	double m_origin_z = 0.0;

	//- Cross-section position number 
	int m_crossSectionId = 64;//1;

	//- Cross-section plane orientation marker; 0-front; 1-top
	int m_planeId = 2;//0; 

	//- Private Constructor
	Model3dHandler(QObject* parent = nullptr);

	//- SModellerWrapper instance
	model::SModellerWrapper& m_model = model::SModellerWrapper::Instance();

	//- FlattenedModel instance
	model::FlattenedModel& m_flat_model = model::FlattenedModel::Instance();


	//- Rule flag 
	bool m_ruleEnabled = false;

	//- Remove rule id
	int m_removeRuleId = 0; // 51-RA, 52-RB, 53-RAI, 54-RBI

	//- Regions list 
	std::vector<double> m_region_list;
	double m_total_volume;
	std::vector<double> m_region_list_proportion;

	//- Domains list
	std::vector<double> m_domain_list;
	std::vector<double> m_domain_list_proportion;

	//- Surface Indice list
	std::vector<int> m_surface_indices;

};

#endif  // MODEL3DHANDLER_H

