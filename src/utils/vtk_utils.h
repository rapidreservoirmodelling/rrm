#ifndef RRMQMLVTK_SRC_UTILS_VTK_UTILS_H
#define RRMQMLVTK_SRC_UTILS_VTK_UTILS_H

#include <string>
#include <QString.h>

#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkNew.h>

namespace vtk_utils 
{
	/**
	 * @brief  Converts triangleMesh to vtkUnstructuredGrid
	 * @return vtkSmartPointer<vtkUnstructuredGrid>
	 **/
	vtkSmartPointer<vtkUnstructuredGrid> triangleMesh2vtkUnstructuredGrid(
			const std::vector<double>& vertex_coordinates,
			const std::vector<size_t>& vertex_indices);

	/**
	 * @brief  Converts tetrahedralMesh to vtkUnstructuredGrid
	 * @return vtkSmartPointer<vtkUnstructuredGrid>
	 **/
	vtkSmartPointer<vtkUnstructuredGrid> tetrahedralMesh2vtkUnstructuredGrid(
			const std::vector<double>& vertex_coordinates,
			const std::vector<size_t>& vertex_indices);

	/**
	 * @brief  Converts vtkUnstructuredGrid to vtkPolyData
	 * @return vtkSmartPointer<vtkPolyData>
	 **/
	vtkSmartPointer<vtkPolyData> vtkUnstructuredGrid2vtkPolyData(
			const vtkSmartPointer<vtkUnstructuredGrid>&);

	/**
	 * @brief  Converts triangleMesh to vtkPolyData
	 * @return vtkSmartPointer<vtkPolyData>
	 **/
	vtkSmartPointer<vtkPolyData> triangleMesh2vtkPolyData(
		const std::vector<double>& vertex_coordinates,
		const std::vector<size_t>& vertex_indices);

	/**
	 * @brief  Stores vtkUnstructuredGrid to XML file
	 * @return bool
	 **/
    bool vtkUnstructuredGrid_To_XMLFile(
            vtkSmartPointer<vtkUnstructuredGrid> pgrid,
            const std::string &filename);

	/**
	 * @brief  Stores vtkPolyData tp XML file
	 * @return bool
	 **/
	bool vtkPolyData_To_XMLFile(
		vtkSmartPointer<vtkPolyData> pgrid,
		const std::string& filename);

	/**
	 * @brief  Converts tetrahedralMesh to vtkPolyData
	 * @return vtkSmartPointer<vtkPolyData>
	 **/
	vtkSmartPointer<vtkPolyData> tetrahedralMesh2vtkPolyData(
		const std::vector<double>& vertex_coordinates,
		const std::vector<size_t>& vertex_indices);

	/**
	 *@brief performs deep copy of the vtkunstructured grid
	 *@return vtkUnstructuredGrid
	**/
	vtkSmartPointer<vtkUnstructuredGrid> getUnstructuredGridWithDeepCopy(vtkSmartPointer<vtkUnstructuredGrid> data);
	
	//- VTK surface data definiton
	typedef struct vtkSurfaceData {
		vtkSmartPointer<vtkPolyData> polyData;
		int surfaceId;
		QString color;
		int category;
		bool visibility;
	};

	/**
	 * @brief  Converts vertex_indices to vtkCellArray
	 * @return vtkSmartPointer<vtkCellArray>
	 **/
	vtkSmartPointer<vtkCellArray> getTriangles(const std::vector<size_t>& vertex_indices);

	/**
	 * @brief  Converts vertex_coordinates to vtkPoints
	 * @return vtkSmartPointer<vtkPoints>
	 **/
	vtkSmartPointer<vtkPoints> getPoints(const std::vector<double>& vertex_coordinates);

	/**
	 * @brief  Converts triangleMesh to vtkSurfaceData
	 * @return vtkSurfaceData
	 **/
	vtkSurfaceData triangleMesh2vtkSurfaceData(
		int surface_id,
		QString surface_color,
		int surface_category,
		const std::vector<double>& vertex_coordinates,
		const std::vector<size_t>& vertex_indices);


} // namespace vtk_utils

#endif
