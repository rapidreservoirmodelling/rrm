
/***************************************************************************

* This class handles the global GUI events.

* Copyright (C) 2020, Fazilatur Rahman.

* It provides the communication support between QML and C++ backend code to
* keep track of button actions and corresponding events.

*****************************************************************************/

#ifndef EVENTRECEIVER_H
#define EVENTRECEIVER_H

#include <QObject>
#include <QString>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>

#include "Constants.h"

class EventReceiver:public QObject
{
    Q_OBJECT

    Q_PROPERTY(int lastCanvasAction MEMBER m_lastCanvasAction NOTIFY lastCanvasActionChanged)
    Q_PROPERTY(bool buttonSketch MEMBER m_buttonSketch NOTIFY updateHighlight)
    Q_PROPERTY(bool buttonSketchLine MEMBER m_buttonSketchLine NOTIFY updateHighlight)
    Q_PROPERTY(bool buttonSketchCircle MEMBER m_buttonSketchCircle NOTIFY updateHighlight)
    Q_PROPERTY(bool buttonSketchEllipse MEMBER m_buttonSketchEllipse NOTIFY updateHighlight)
    Q_PROPERTY(bool buttonTranslate MEMBER m_buttonTranslate NOTIFY updateHighlight)
    Q_PROPERTY(bool buttonRotate MEMBER m_buttonRotate NOTIFY updateHighlight)
    Q_PROPERTY(bool buttonScale MEMBER m_buttonScale NOTIFY updateHighlight)
    Q_PROPERTY(bool buttonGuide MEMBER m_buttonGuide NOTIFY updateHighlight)
    Q_PROPERTY(bool buttonFixGuide MEMBER m_buttonFixGuide NOTIFY updateHighlight)
    Q_PROPERTY(bool buttonZoom MEMBER m_buttonZoom NOTIFY updateHighlight)
        
public:
    explicit EventReceiver(QObject *parent = 0);
public:
    Q_INVOKABLE void reset();
    int getEvent();
    
signals:
    void sendToQml(int response);
    void updateCanvasAction(int action);
    void updateCanvasVisualizationFV(int action);
    void updateCanvasVisualizationMV(int action);
    
    void rulesButtonDisabled();
    void removeRuleEnabled(int removeRuleId);
    void templateSaved(int num);
    void receiveMouseFromFrontView(QString response);
    void receiveMouseFromMapView(QString response);
    void sendFixFrontViewGuidelines();
    void sendFixMapViewGuidelines();
    void surfaceCreated(int canvasID);
    void sendTrajectoryTemplateIndex(int index);
    void sendSketchTemplateIndex(int index);
    void sendContourTemplateIndex(int index);
    void preserveButtonDisabled();
    void lastCanvasActionChanged();
    void updateHighlight();
    
    void crossSectionChanged(int id);
    void planeOrientationChanged(int id);
    void previewChanged(bool flag);
    void updatePreviewVisualization();
    void getSketchRegionOpacity(double alpha);

public slots:
    void setEvent(int id);
    void setCanvasAction(int id);
    void setCanvasVisualizationFV(int id);
    void setCanvasVisualizationMV(int id);
    void setLastCanvasAction();
    
    void setRulesButtonOff();
    void setRemoveRule(int id);
    void setNumTemplates(int n);
    void setMouseFromFrontView(int x, int y, int w, int h, int ox, int oy);
    void setMouseFromMapView(int x, int y, int w, int h, int ox, int oy);
    void fixFrontViewGuidelines();
    void fixMapViewGuidelines();
    void sendSurfaceCreated(int canvasID);
    void setTrajectoryTemplateIndex(int index);
    void setSketchTemplateIndex(int index);
    void setContourTemplateIndex(int index);
    void setPreserveButtonOff();
    void resetFloatingButtons();

    void setCrossSection(double id);
    void setPlaneOrientation(int id);
    void setPreview(bool flag);
    void updatePreview();
        
    void setSketchRegionOpacity(double alpha);

private:
    int m_event_id;
    int m_remove_rule_id;
    int m_num_templates;

    //- Flags to control the highlight of the floating buttons
    bool m_buttonSketch = true;
    bool m_buttonSketchLine = false;
    bool m_buttonSketchCircle = false;
    bool m_buttonSketchEllipse = false;
    bool m_buttonTranslate = false;
    bool m_buttonRotate = false;
    bool m_buttonScale = false;
    bool m_buttonGuide = false;
    bool m_buttonFixGuide = false;
    bool m_buttonZoom = false;

    int m_lastCanvasAction = CanvasAction::Value::FB_SKETCH;

    //- Cross-section# and Plane Orientation
    int m_cross_section_id;
    int m_cross_plane_orientation_id;

};

#endif // EVENTRECEIVER_H
