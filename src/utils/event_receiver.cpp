#include "event_receiver.h"

#include <QDebug>

EventReceiver::EventReceiver(QObject *parent)
{
    this->reset();
}


void EventReceiver::reset()
{
    m_event_id = 0;
    m_cross_section_id = 0;

    m_buttonSketch = true;
    m_buttonSketchLine = false;
    m_buttonSketchCircle = false;
    m_buttonSketchEllipse = false;
    m_buttonTranslate = false;
    m_buttonRotate = false;
    m_buttonScale = false;
    m_buttonGuide = false;
    m_buttonFixGuide = false;
    m_buttonZoom = false;

    m_lastCanvasAction = CanvasAction::Value::FB_SKETCH;
}


int EventReceiver::getEvent()
{
    return m_event_id;
}

void EventReceiver::setEvent(int id){
    ////qDebug() << "C++ Event: " << id;
    m_event_id = id;
    emit(sendToQml(id));
}

void EventReceiver::setCanvasAction(int id)
{
    emit(updateCanvasAction(id));
}

void EventReceiver::setCanvasVisualizationFV(int id)
{
    emit(updateCanvasVisualizationFV(id));
}

void EventReceiver::setCanvasVisualizationMV(int id)
{
    emit(updateCanvasVisualizationMV(id));
}

void EventReceiver::setLastCanvasAction()
{
    emit(updateCanvasAction(m_lastCanvasAction));
}

void EventReceiver::setCrossSection(double id) {
    ////qDebug() << "EventReceiver::setCrossSection: " << id;
    m_cross_section_id = (int) id;
    emit(crossSectionChanged(m_cross_section_id));
}

void EventReceiver::setPlaneOrientation(int id) {
    ////qDebug() << "EventReceiver::setPlaneOrientation: " << id;
    m_cross_plane_orientation_id = (int)id;
    emit(planeOrientationChanged(m_cross_section_id));
}

void EventReceiver::setPreview(bool flag)
{
    emit previewChanged(flag);
}

void EventReceiver::updatePreview()
{
    emit updatePreviewVisualization();
}

void EventReceiver::setSketchRegionOpacity(double alpha)
{
    emit getSketchRegionOpacity(alpha);
}

void EventReceiver::setRulesButtonOff() {
    emit(rulesButtonDisabled());
}

void EventReceiver::setPreserveButtonOff() {
    emit(preserveButtonDisabled());
}

void EventReceiver::setRemoveRule(int id) {
    m_remove_rule_id = id;
    emit(removeRuleEnabled(id));
}

void EventReceiver::setNumTemplates(int n) {
    m_num_templates = n;
    emit(templateSaved(n));
}

void EventReceiver::setMouseFromFrontView(int x, int y, int w, int h, int ox, int oy) {

    QString response;
    QJsonArray plot_array;

    QString x_str("x");
    QString y_str("y");

    QJsonObject pointObject;
    pointObject.insert(x_str, QJsonValue(x));
    pointObject.insert(y_str, QJsonValue(y));
    plot_array.push_back(QJsonValue(pointObject));
    pointObject.insert(x_str, QJsonValue(w));
    pointObject.insert(y_str, QJsonValue(h));
    plot_array.push_back(QJsonValue(pointObject));
    pointObject.insert(x_str, QJsonValue(ox));
    pointObject.insert(y_str, QJsonValue(oy));
    plot_array.push_back(QJsonValue(pointObject));

    QJsonObject final_object;
    final_object.insert(QString("plottingData"), QJsonValue(plot_array));
    QJsonDocument jsonDoc(final_object);

    response = jsonDoc.toJson();

    emit(receiveMouseFromFrontView(response));
}


void EventReceiver::setMouseFromMapView(int x, int y, int w, int h, int ox, int oy) {

    QString response;
    QJsonArray plot_array;

    QString x_str("x");
    QString y_str("y");

    QJsonObject pointObject;
    pointObject.insert(x_str, QJsonValue(x));
    pointObject.insert(y_str, QJsonValue(y));
    plot_array.push_back(QJsonValue(pointObject));
    pointObject.insert(x_str, QJsonValue(w));
    pointObject.insert(y_str, QJsonValue(h));
    plot_array.push_back(QJsonValue(pointObject));
    pointObject.insert(x_str, QJsonValue(ox));
    pointObject.insert(y_str, QJsonValue(oy));
    plot_array.push_back(QJsonValue(pointObject));

    QJsonObject final_object;
    final_object.insert(QString("plottingData"), QJsonValue(plot_array));
    QJsonDocument jsonDoc(final_object);

    response = jsonDoc.toJson();

    emit(receiveMouseFromMapView(response));
}

void EventReceiver::fixFrontViewGuidelines()
{
    emit(sendFixFrontViewGuidelines());
}

void EventReceiver::fixMapViewGuidelines()
{
    emit(sendFixMapViewGuidelines());
}


void EventReceiver::sendSurfaceCreated(int canvasID)
{
    emit(surfaceCreated(canvasID));
}


void EventReceiver::setTrajectoryTemplateIndex(int index)
{
    emit(sendTrajectoryTemplateIndex(index));
}


void EventReceiver::setSketchTemplateIndex(int index)
{
    emit(sendSketchTemplateIndex(index));
}


void EventReceiver::setContourTemplateIndex(int index)
{
    emit(sendContourTemplateIndex(index));
}


void EventReceiver::resetFloatingButtons()
{
    m_buttonSketch = false;
    m_buttonSketchLine = false;
    m_buttonSketchCircle = false;
    m_buttonSketchEllipse = false;
    m_buttonTranslate = false;
    m_buttonRotate = false;
    m_buttonScale = false;
    m_buttonZoom = false;
}
