#include <array>

#include "surface_colors.h"

namespace stub {

    /// Stub function to return a nice color given a surface unique id
    std::array<int,3> getSurfaceColor(int surface_id)
    {
        std::array<int, 3> color = {255, 0, 0};
        if (surface_id < 1)
        {
            // return red if surface_id is invalid
            return color;
        }

        // Set2 colormap from 
        // https://colorbrewer2.org/#type=qualitative&scheme=Set2&n=8
        constexpr int num_colors = 8;
        int colormap[3*num_colors] = {
            102,   194,   165,
            252,   141,    98,
            141,   160,   203,
            231,   138,   195,
            166,   216,    84,
            255,   217,    47,
            229,   196,   148,
            179,   179,   179
        };

        std::size_t i = surface_id % num_colors;
        color[0] = colormap[3*i + 0];
        color[1] = colormap[3*i + 1];
        color[2] = colormap[3*i + 2];

        return color;
    }

    std::string getSurfaceColorHex(int surface_id)
    {
        std::string color = "#FF0000";
        if (surface_id < 0)
        {
            // return red if surface_id is invalid
            return color;
        }
        
        // Set2 colormap from 
        // https://colorbrewer2.org/#type=qualitative&scheme=Set2&n=12
        constexpr int num_colors = 12;
        std::string colormap[num_colors] = {
                "#a6cee3",
                "#1f78b4",
                "#b2df8a",
                "#33a02c",
                "#fb9a99",
                "#e31a1c",
                "#fdbf6f",
                "#ff7f00",
                "#cab2d6",
                "#6a3d9a",
                "#ffff99",
                "#b15928"
        };

        std::size_t i = surface_id % num_colors;
        color = colormap[i];
        
        return color;
    }

    QColor getSurfaceQColor(int surface_id)
    {
        std::string color = "#FF0000";
        if (surface_id < 0)
        {
            // return red if surface_id is invalid
            return QColor{ QString::fromUtf8(color.c_str()) };
        }

        // Set2 colormap from 
        // https://colorbrewer2.org/#type=qualitative&scheme=Set2&n=12
        constexpr int num_colors = 12;
        std::string colormap[num_colors] = {
                "#a6cee3",
                "#1f78b4",
                "#b2df8a",
                "#33a02c",
                "#fb9a99",
                "#e31a1c",
                "#fdbf6f",
                "#ff7f00",
                "#cab2d6",
                "#6a3d9a",
                "#ffff99",
                "#b15928"
        };

        std::size_t i = surface_id % num_colors;
        color = colormap[i];

        return QColor{ QString::fromUtf8(color.c_str()) };
        
    }
    QString getSurfaceQStringColor(int surface_id)
    {
        std::string color = "#FF0000";
        if (surface_id < 0)
        {
            // return red if surface_id is invalid
            return QString::fromUtf8(color.c_str()) ;
        }

        // Set2 colormap from 
        // https://colorbrewer2.org/#type=qualitative&scheme=Set2&n=12
        constexpr int num_colors = 12;
        std::string colormap[num_colors] = {
                "#a6cee3",
                "#1f78b4",
                "#b2df8a",
                "#33a02c",
                "#fb9a99",
                "#e31a1c",
                "#fdbf6f",
                "#ff7f00",
                "#cab2d6",
                "#6a3d9a",
                "#ffff99",
                "#b15928"
        };

        std::size_t i = surface_id % num_colors;
        color = colormap[i];

        return QString::fromUtf8(color.c_str());

    }

} // namespace stub
