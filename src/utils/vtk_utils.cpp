#include "vtk_utils.h"

#include <vtkCellArray.h>
#include <vtkCellType.h>
#include <vtkDataSetMapper.h>
#include <vtkGeometryFilter.h>
#include <vtkPoints.h>
#include <vtkTriangle.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkXMLPolyDataWriter.h>
#include <QString>

#include <QFutureWatcher>
#include <QtConcurrent>


namespace vtk_utils 
{
	/*typedef struct vtkSurfaceData {
		vtkSmartPointer<vtkPolyData> polyData;
		int surfaceId;
		QString color;
	};*/

	vtkSmartPointer<vtkUnstructuredGrid> triangleMesh2vtkUnstructuredGrid(
			const std::vector<double>& vertex_coordinates,
			const std::vector<size_t>& vertex_indices)
	{
		vtkSmartPointer<vtkUnstructuredGrid> output_grid = vtkSmartPointer<vtkUnstructuredGrid>::New();

		vtkSmartPointer<vtkPoints> points =
			vtkSmartPointer<vtkPoints>::New();
		auto num_points = vertex_coordinates.size() / 3;
		float coords[3];
		
		for (size_t i=0; i < num_points; i++) 
		{
			coords[0] = vertex_coordinates[3 * i + 0];
			coords[1] = vertex_coordinates[3 * i + 1];
			coords[2] = vertex_coordinates[3 * i + 2];

			points->InsertPoint(i, coords);
		}

		auto num_triangles = vertex_indices.size()/3;
		// vtkIdType triangle[12] = { 0, 0, 0, 0, 0, 0, 0, 0 }; // if it crashes...
		vtkIdType triangle[3];

		for (size_t i = 0; i < num_triangles; ++i )
		{
			triangle[0] = vertex_indices[3 * i + 0]; 
			triangle[1] = vertex_indices[3 * i + 1]; 
			triangle[2] = vertex_indices[3 * i + 2];

			output_grid->InsertNextCell(VTK_TRIANGLE, 3, triangle);
		}

		output_grid->SetPoints(points);

		return output_grid;
	}

	vtkSmartPointer<vtkUnstructuredGrid> tetrahedralMesh2vtkUnstructuredGrid(
			const std::vector<double>& vertex_coordinates,
			const std::vector<size_t>& vertex_indices)
	{
		vtkSmartPointer<vtkUnstructuredGrid> output_grid = vtkSmartPointer<vtkUnstructuredGrid>::New();

		vtkSmartPointer<vtkPoints> points =
			vtkSmartPointer<vtkPoints>::New();
		auto num_points = vertex_coordinates.size() / 3;
		float coords[3];
		
		for (size_t i=0; i < num_points; i++) 
		{
			coords[0] = vertex_coordinates[3 * i + 0];
			coords[1] = vertex_coordinates[3 * i + 1];
			coords[2] = vertex_coordinates[3 * i + 2];

			points->InsertPoint(i, coords);
		}

		auto num_tetrahedra = vertex_indices.size()/4;
		// vtkIdType triangle[12] = { 0, 0, 0, 0, 0, 0, 0, 0 }; // if it crashes...
		vtkIdType tetrahedra[4];

		for (size_t i = 0; i < num_tetrahedra; ++i )
		{
			tetrahedra[0] = vertex_indices[4 * i + 0]; 
			tetrahedra[1] = vertex_indices[4 * i + 1]; 
			tetrahedra[2] = vertex_indices[4 * i + 2];
			tetrahedra[3] = vertex_indices[4 * i + 3];

			output_grid->InsertNextCell(VTK_TETRA, 4, tetrahedra);
		}

		output_grid->SetPoints(points);

		return output_grid;
	}

	vtkSmartPointer<vtkPolyData> vtkUnstructuredGrid2vtkPolyData(
			const vtkSmartPointer<vtkUnstructuredGrid>& ugrid)
    {
        vtkSmartPointer<vtkGeometryFilter> gfilter = vtkSmartPointer<vtkGeometryFilter>::New();
        gfilter->SetInputData(ugrid);
        gfilter->Update();

        vtkSmartPointer<vtkPolyData> polydata = gfilter->GetOutput();

        return polydata;
    }


	vtkSmartPointer<vtkPolyData> triangleMesh2vtkPolyData(
		const std::vector<double>& vertex_coordinates,
		const std::vector<size_t>& vertex_indices)
	{
		vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
		auto num_points = vertex_coordinates.size() / 3;
		float coords[3];

		for (size_t i = 0; i < num_points; i++)
		{
			coords[0] = vertex_coordinates[3 * i + 0];
			coords[1] = vertex_coordinates[3 * i + 1];
			coords[2] = vertex_coordinates[3 * i + 2];

			points->InsertPoint(i, coords);
		}

		vtkSmartPointer<vtkCellArray> triangles = vtkSmartPointer<vtkCellArray>::New();
		auto num_triangles = vertex_indices.size() / 3;
		
		for (size_t i = 0; i < num_triangles; ++i)
		{
			vtkSmartPointer<vtkTriangle> triangle =	vtkSmartPointer<vtkTriangle>::New();
						
			triangle->GetPointIds()->SetId(0, vertex_indices[3 * i + 0]);
			triangle->GetPointIds()->SetId(1, vertex_indices[3 * i + 1]);
			triangle->GetPointIds()->SetId(2, vertex_indices[3 * i + 2]);

			triangles->InsertNextCell(triangle);
			
		}
		
		// Create a polydata object
		vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();

		// Add the geometry and topology to the polydata
		polyData->SetPoints(points);
		polyData->SetPolys(triangles);

		return polyData;
	}

    bool vtkUnstructuredGrid_To_XMLFile(vtkSmartPointer<vtkUnstructuredGrid> pgrid, const std::string &filename)
    {
        if (pgrid == nullptr)
        {
            return false;
        }

        vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer =
            vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
        writer->SetFileName(filename.c_str());
#if VTK_MAJOR_VERSION <= 5
        writer->SetInput(pgrid);
#else
        writer->SetInputData(pgrid);
#endif
        writer->Write();

        return true;
    }

	bool vtkPolyData_To_XMLFile(vtkSmartPointer<vtkPolyData> polyData, const std::string& filename)
	{
		if (polyData == nullptr)
		{
			return false;
		}

		vtkSmartPointer<vtkXMLPolyDataWriter> writer =
			vtkSmartPointer<vtkXMLPolyDataWriter>::New();
		writer->SetFileName(filename.c_str());
#if VTK_MAJOR_VERSION <= 5
		writer->SetInput(pgrid);
#else
		writer->SetInputData(polyData);
#endif
		writer->Write();

		return true;

	}

	vtkSmartPointer<vtkPolyData> tetrahedralMesh2vtkPolyData(
		const std::vector<double>& vertex_coordinates,
		const std::vector<size_t>& vertex_indices)
	{
		vtkSmartPointer<vtkUnstructuredGrid> output_grid = vtkSmartPointer<vtkUnstructuredGrid>::New();

		vtkSmartPointer<vtkPoints> points =
			vtkSmartPointer<vtkPoints>::New();
		auto num_points = vertex_coordinates.size() / 3;
		float coords[3];

		for (size_t i = 0; i < num_points; i++)
		{
			coords[0] = vertex_coordinates[3 * i + 0];
			coords[1] = vertex_coordinates[3 * i + 1];
			coords[2] = vertex_coordinates[3 * i + 2];

			points->InsertPoint(i, coords);
		}

		auto num_tetrahedra = vertex_indices.size() / 4;
		// vtkIdType triangle[12] = { 0, 0, 0, 0, 0, 0, 0, 0 }; // if it crashes...
		vtkIdType tetrahedra[4];

		for (size_t i = 0; i < num_tetrahedra; ++i)
		{
			tetrahedra[0] = vertex_indices[4 * i + 0];
			tetrahedra[1] = vertex_indices[4 * i + 1];
			tetrahedra[2] = vertex_indices[4 * i + 2];
			tetrahedra[3] = vertex_indices[4 * i + 3];

			output_grid->InsertNextCell(VTK_TETRA, 4, tetrahedra);
		}

		output_grid->SetPoints(points);

		//return output_grid;

		vtkSmartPointer<vtkGeometryFilter> gfilter = vtkSmartPointer<vtkGeometryFilter>::New();
		gfilter->SetInputData(output_grid);
		gfilter->Update();

		vtkSmartPointer<vtkPolyData> polydata = gfilter->GetOutput();

		return polydata;
	}

	vtkSmartPointer<vtkUnstructuredGrid> getUnstructuredGridWithDeepCopy(vtkSmartPointer<vtkUnstructuredGrid> data)
	{
		vtkSmartPointer<vtkUnstructuredGrid> outputGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();

		outputGrid->DeepCopy(data);

		return outputGrid;
	}

	vtkSmartPointer<vtkCellArray> getTriangles(const std::vector<size_t>& vertex_indices)
	{
		vtkSmartPointer<vtkCellArray> triangles = vtkSmartPointer<vtkCellArray>::New();
		auto num_triangles = vertex_indices.size() / 3;

		
		for (size_t i = 0; i < num_triangles; ++i)
		{
			vtkSmartPointer<vtkTriangle> triangle = vtkSmartPointer<vtkTriangle>::New();

			triangle->GetPointIds()->SetId(0, vertex_indices[3 * i + 0]);
			triangle->GetPointIds()->SetId(1, vertex_indices[3 * i + 1]);
			triangle->GetPointIds()->SetId(2, vertex_indices[3 * i + 2]);

			triangles->InsertNextCell(triangle);

		}

		return triangles;
	}

	vtkSmartPointer<vtkPoints> getPoints(const std::vector<double>& vertex_coordinates)
	{
		vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
		auto num_points = vertex_coordinates.size() / 3;
		float coords[3];

		for (size_t i = 0; i < num_points; i++)
		{
			coords[0] = vertex_coordinates[3 * i + 0];
			coords[1] = vertex_coordinates[3 * i + 1];
			coords[2] = vertex_coordinates[3 * i + 2];

			points->InsertPoint(i, coords);
		}
		return points;
	}
	/*
	vtkSurfaceData triangleMesh2vtkSurfaceData(
		int surface_id,
		QString surface_color,
		int surface_category,
		const std::vector<double>& vertex_coordinates,
		const std::vector<size_t>& vertex_indices)
	{
		// Start Concurrent Calculation of points and triangles
		QFuture<vtkSmartPointer<vtkPoints> > futurePoints = QtConcurrent::run(&vtk_utils::getPoints, vertex_coordinates);
		QFuture<vtkSmartPointer<vtkCellArray> > futureTriangles = QtConcurrent::run(&vtk_utils::getTriangles, vertex_indices);
		
		futurePoints.waitForFinished();
		futureTriangles.waitForFinished();

		// Create a polydata object
		vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();

		// Add the geometry and topology to the polydata
		polyData->SetPoints(futurePoints.result());//polyData->SetPoints(points);
		polyData->SetPolys(futureTriangles.result());//polyData->SetTrianlges(triangles);

		//Prepare surface mesh data
		vtkSurfaceData output;
		output.surfaceId = surface_id;
		output.color = surface_color;
		output.category = surface_category;
		output.polyData = polyData;

		return output;
		
	}
	*/
	
	vtkSurfaceData triangleMesh2vtkSurfaceData(
		int surface_id,
		QString surface_color,
		int surface_category,
		const std::vector<double>& vertex_coordinates,
		const std::vector<size_t>& vertex_indices)
	{
		vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
		auto num_points = vertex_coordinates.size() / 3;
		float coords[3];

		for (size_t i = 0; i < num_points; i++)
		{
			coords[0] = vertex_coordinates[3 * i + 0];
			coords[1] = vertex_coordinates[3 * i + 1];
			coords[2] = vertex_coordinates[3 * i + 2];

			points->InsertPoint(i, coords);
		}

		vtkSmartPointer<vtkCellArray> triangles = vtkSmartPointer<vtkCellArray>::New();
		auto num_triangles = vertex_indices.size() / 3;

		for (size_t i = 0; i < num_triangles; ++i)
		{
			vtkSmartPointer<vtkTriangle> triangle = vtkSmartPointer<vtkTriangle>::New();

			triangle->GetPointIds()->SetId(0, vertex_indices[3 * i + 0]);
			triangle->GetPointIds()->SetId(1, vertex_indices[3 * i + 1]);
			triangle->GetPointIds()->SetId(2, vertex_indices[3 * i + 2]);

			triangles->InsertNextCell(triangle);

		}

		// Create a polydata object
		vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();

		// Add the geometry and topology to the polydata
		polyData->SetPoints(points);
		polyData->SetPolys(triangles);

		//Prepare surface mesh data
		vtkSurfaceData output;
		output.surfaceId = surface_id;
		output.color = surface_color;
		output.category = surface_category;
		output.polyData = polyData;

		return output;
		
	}
	
} // namespace vtk_utils
