#include "screen.h"

#include <QDebug>

Screen::Screen(QObject *parent)
{
}

void Screen::init(QScreen *screen)
{
    //for single display (primary) device
    QString name = screen->name();
    double pixelRatio = screen->devicePixelRatio();
    m_size = screen->size();
    double logicalDpi = screen->logicalDotsPerInch();
    double physicalDpi = screen->physicalDotsPerInch();
    ////qDebug() << QString("Primary Display: %1 x %2 (%3: Pixel Ratio:%4) LogicalDpi: %5 PhysicalDpi: %6").arg(m_size.width()).arg(m_size.height()).arg(name).arg(pixelRatio).arg(logicalDpi).arg(physicalDpi);

    #ifdef Q_OS_WIN
        this->m_screenDpi = logicalDpi * pixelRatio;
    #endif
    #ifdef Q_OS_ANDROID
        this->m_screenDpi = physicalDpi * pixelRatio;
    #endif

    this->m_scaleFactor = this->m_screenDpi/this->m_baseline_dpi;
    
}
void Screen::setBaselineDpi(double value)
{
    this->m_baseline_dpi = value;
}
double Screen::getScreenDpi()
{
    return this->m_screenDpi;
}

double Screen::getScaleFactor()
{
    return this->m_scaleFactor;
}

int Screen::getDiP(int value)
{
    return value * this->m_scaleFactor;
}

QSize Screen::getScreenSize()
{
    return m_size;
}


