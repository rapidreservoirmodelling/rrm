#ifndef STUB_SURFACE_COLORS_UTILS_H
#define STUB_SURFACE_COLORS_UTILS_H

#include <QColor>

namespace stub {
    /**
     * @brief  Stub function to return a nice color given a surface unique id
     * @return std::array<int, 3>
     **/
    std::array<int, 3> getSurfaceColor(int surface_id);
    
    /**
     * @brief  Stub function to return a nice color given a surface unique id
     * @return std::string
     **/
    std::string getSurfaceColorHex(int surface_id);

    /**
     * @brief  Stub function to return a nice color given a surface unique id
     * @return QColor
     **/
    QColor getSurfaceQColor(int surface_id);

    /**
     * @brief  Stub function to return a nice color given a surface unique id
     * @return QString
     **/
    QString getSurfaceQStringColor(int surface_id);

}

#endif