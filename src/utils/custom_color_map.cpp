#include "custom_color_map.h"
#include "surface_colors.h"

CustomColorMap::CustomColorMap()
{
         
}

QString CustomColorMap::getDefaultColor(int position)
{
    return stub::getSurfaceQStringColor(position + 1);
        
}

QString CustomColorMap::getColor(int position)
{
    if (position >= m_color_list.size())
        m_color_list.push_back(getDefaultColor(position));
    return m_color_list.at(position);

}

void CustomColorMap::setColor(int position, QString customColor)
{
    if (position < m_color_list.size())
        m_color_list[position] = customColor;
    else m_color_list.push_back(customColor);
}

std::vector<QString> CustomColorMap::getAll()
{
    return m_color_list;
}

void CustomColorMap::addColor(int position)
{
    if (position >= m_color_list.size())
        m_color_list.push_back(getDefaultColor(position));
    //return m_color_list.at(position);

}

void CustomColorMap::clear()
{
    if(m_color_list.size() != 0)
        m_color_list.clear();
}

void CustomColorMap::pop_back()
{
    if (m_color_list.size() != 0)
        m_color_list.pop_back();
}

int CustomColorMap::size()
{
    return m_color_list.size();
        
}

QString CustomColorMap::getCustomColorByIndex(int index)
{
    std::string color = "#FF0000";
    if (index < 0)
    {
        // return red if index is invalid
        return QString::fromUtf8(color.c_str());
    }

    // Set2 colormap from 
    // https://colorbrewer2.org/#type=qualitative&scheme=Set2&n=12
    constexpr int num_colors = 12;
    std::string colormap[num_colors] = {
            "#a6cee3",
            "#1f78b4",
            "#b2df8a",
            "#33a02c",
            "#fb9a99",
            "#e31a1c",
            "#fdbf6f",
            "#ff7f00",
            "#cab2d6",
            "#6a3d9a",
            "#ffff99",
            "#b15928"
    };

    std::size_t i = index % num_colors;
    color = colormap[i];

    return QString::fromUtf8(color.c_str());

}