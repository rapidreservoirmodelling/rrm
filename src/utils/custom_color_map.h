#ifndef CUSTOMCOLORMAP_H
#define CUSTOMCOLORMAP_H

#include <vector>
#include <QColor>

class CustomColorMap
{
public:
    CustomColorMap();
    QString getColor(int position);
    void setColor(int position, QString customColor);
    std::vector<QString> getAll();
    void addColor(int position);
    void clear();
    void pop_back();
    int size();
    static QString getCustomColorByIndex(int index);
private:
    QString getDefaultColor(int position);
    
    std::vector<QString> m_color_list;
};

#endif // CUSTOMCOLORMAP_H

