/***************************************************************************

* This is the Screen utility class.

* Copyright (C) 2020, Fazilatur Rahman.

* The purpose of this class is to allow using device independent pixels while 
* designing the user interface.

*****************************************************************************/


#ifndef SCREEN_H
#define SCREEN_H

#include <QObject>
#include <QtGui/qscreen.h>
#include <QSize>
#include <QtGlobal>

class Screen:public QObject
{
 Q_OBJECT
public:
    /**
    * Constructor.
    */
    explicit Screen(QObject *parent = 0);

    /**
    * Method that initializes primary (single) display screen parameters 
    * @return void
    */
    void init(QScreen *screen);

public:

    /**
    * Method that returns the screen DPI
    * This is an invokable method that may be called from QML
    * @return double
    */
    Q_INVOKABLE double getScreenDpi();
    
    /**
    * Method that returns the scale factor
    * This is an invokable method that may be called from QML
    * @return double
    */
    Q_INVOKABLE double getScaleFactor();

    /**
    * Method that returns the Dip (Device Independent Pixel)
    * This is an invokable method that may be called from QML
    * @param value of pixels   
    * @return int
    */
    Q_INVOKABLE int getDiP(int value);

    /**
    * Method that returns the screen size
    * This is an invokable method that may be called from QML
    * @return QSize
    */
    Q_INVOKABLE QSize getScreenSize();

    /**
    * Method that sets the baseline DPI
    * @return void
    */
    void setBaselineDpi(double value);

private:
    //- Screen dot per inch (DPI)
    double m_screenDpi;
    
    //- Scale factor
    double m_scaleFactor;

    //- Base line DPI
    double m_baseline_dpi=160;

    //- Screen size
    QSize m_size;

};


#endif // SCREEN_H
