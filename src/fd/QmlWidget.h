#include <QQuickPaintedItem>
#include <QPushButton>
#include <QTextEdit>
#include <QWidget>

class QmlWidget : public QQuickPaintedItem
{
    Q_OBJECT

public:
    //explicit QmlWidget(QQuickItem* parent = 0);
    explicit QmlWidget(QWidget* parent = 0);
    void paint(QPainter* painter);

public slots:
    void handleButton();
private:
    QWidget* fdWidgetWindow;
    QPushButton* button1;

};