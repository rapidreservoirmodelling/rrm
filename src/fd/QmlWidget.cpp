#include "qmlwidget.h"
#include <QVBoxLayout>

//QmlWidget::QmlWidget(QQuickItem* parent) : QQuickPaintedItem(parent)
QmlWidget::QmlWidget(QWidget* parent) : QQuickPaintedItem()
{
    //fdWidgetWindow = new QWidget(parent);
    fdWidgetWindow = new QWidget();
    //fdWidgetWindow->setMinimumSize(1024, 700);
    fdWidgetWindow->setVisible(true);
    fdWidgetWindow->activateWindow();
        
    button1 = new QPushButton("Button One", fdWidgetWindow);
    button1->setFlat(false);
    button1->setGeometry(QRect(QPoint(150, 50), QSize(75, 23)));
    
    QPushButton* button2 = new QPushButton("Button Two");
    QPushButton* button3 = new QPushButton("Button Three");
    QPushButton* button4 = new QPushButton("Button Four");
    QPushButton* button5 = new QPushButton("Button Five");

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(button1);
    layout->addWidget(button2);
    layout->addWidget(button3);
    layout->addWidget(button4);
    layout->addWidget(button5);

    // Connect button1 signal to appropriate slot
    connect(button1, &QPushButton::released, this, &QmlWidget::handleButton);

    fdWidgetWindow->setLayout(layout);

}

void QmlWidget::paint(QPainter *painter) {
    fdWidgetWindow->render(painter);
    
}

void QmlWidget::handleButton()
{
    // change the text
    button1->setText("I'm a Widget Button");
    // resize button
    button1->resize(100, 100);
}