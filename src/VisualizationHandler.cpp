#include "VisualizationHandler.h"
#include "utils/custom_color_map.h"
#include "viz3d/ActionConstants.hpp"
#include "sbim/SbimFileHandler.h"
#include "ImageFileHandler.h"

#include <map>
#include <QFutureWatcher>
#include <QtConcurrent>

bool VisualizationHandler::instanceFlag = false;
VisualizationHandler* VisualizationHandler::single = NULL;

VisualizationHandler::VisualizationHandler(VtkFboItem* vtkFboItem, CanvasHandler* canvasHandler, std::shared_ptr<CommandProcessor> commandProcessor)
{
	m_model3dHandler = Model3dHandler::getInstance(nullptr);
    m_vtkFboItem = vtkFboItem;
    m_canvasHandler = canvasHandler;
    
    m_commandProcessor = commandProcessor;

    togglePreviewButtonStaus();
    if (!getPreviewFlag())
    {
        setPreviewFlag();
    }
}

VisualizationHandler* VisualizationHandler::getInstance(QQmlEngine* qmlEngine, VtkFboItem* vtkFboItem, CanvasHandler* canvasHandler, std::shared_ptr<CommandProcessor> commandProcessor)
{
    if (!instanceFlag)
    {
        single = new VisualizationHandler(vtkFboItem, canvasHandler, commandProcessor);
        instanceFlag = true;
        //set this instance to QML root context
        QQmlContext* rootContext = qmlEngine->rootContext();
        rootContext->setContextProperty("newVizHandler", QVariant::fromValue(single));
        return single;
    }
    else
    {
        return single;
    }
}

VisualizationHandler::~VisualizationHandler()
{
    instanceFlag = false;
}

void VisualizationHandler::initData()
{

}

std::vector < std::shared_ptr<SurfaceItem>> VisualizationHandler::getSurfaceList()
{
	return surfaceList;
}

std::vector< std::shared_ptr<Region> > VisualizationHandler::getRegionList()
{
	return regionList;
}

std::vector< std::shared_ptr<Domain> > VisualizationHandler::getDomainList()
{
    return domainList;
}

void VisualizationHandler::changeObjectVisibility(int index, int parentTypeId, int checkState)
{
	bool flag = true;
	//////qDebug() << "VisualizationHandler::changeObjectVisibility: " << index << ", checkState:" << checkState << " parent-id: " << parentTypeId;

	switch (checkState)
	{
	case 0:
		flag = false;
		break;
	case 1:
		flag = true;
		break;
	case 2:
		flag = true;
		break;
	default:
		flag = true;
		break;
	}

    switch (parentTypeId)
    {
        case ObjectCategories::Name::STRATIGRAPHY:
            m_visibilityMap[index] = flag;
            m_vtkFboItem->changeVisibility(index, parentTypeId, flag);
            changeSingleSurfaceVisibility(index, parentTypeId, flag);
            emit(surfaceVisibilityListUpdated(surfaceList));
                        
            emit(surfaceMapUpdated(getSurfaceColorMap(), getSurfaceVisibilityMap()));
            break;
        
        case ObjectCategories::Name::STRUCTURE:
            m_visibilityMap[index] = flag;
            m_vtkFboItem->changeVisibility(index, parentTypeId, flag);
            changeSingleSurfaceVisibility(index, parentTypeId, flag);
            emit(surfaceVisibilityListUpdated(surfaceList));
            //emit(surfaceMapUpdated(getSurfaceColorList(), m_visibilityMap));
            emit(surfaceMapUpdated(getSurfaceColorMap(), getSurfaceVisibilityMap()));
            break;

        case ObjectCategories::Name::REGION:
            m_region_visibilityMap[index] = flag;
            m_vtkFboItem->changeVisibility(index, parentTypeId, flag);
            //emit(regionVisibilityListUpdated(regionList));
            changeSingleRegionVisibility(index, flag);
            emit(regionMapUpdated(getRegionColorList(), m_region_visibilityMap));
            break;

        case ObjectCategories::Name::DOMAIN_VOLUME:
            //////qDebug() << "VisualizationHandler::changeDomainVisibility1: " << index << ", " << parentTypeId;
            m_domain_visibilityMap[index] = flag;
            //////qDebug() << "VisualizationHandler::changeDomainVisibility2: " << index << ", " << parentTypeId;
            changeSingleDomainVisibility(index, flag);
            m_vtkFboItem->changeVisibility(index, parentTypeId, flag);
            emit(domainMapUpdated( prepareDomainMap() ) );
            break;

        case ObjectCategories::Name::REGION_IN_DOMAIN:
            m_region_visibilityMap[index] = flag;
            m_vtkFboItem->changeVisibility(index, parentTypeId, flag);
            //emit(regionVisibilityListUpdated(regionList));
            changeSingleRegionVisibility(index, flag);
            emit(regionMapUpdated(getRegionColorList(), m_region_visibilityMap));
            break;

        default:
        
            break;
    }//end: switch (parentTypeId)
    //////qDebug() << "VisualizationHandler::changeSurfaceVisibility: " << index << ", " << flag;
}

QString VisualizationHandler::getDomainColorbyId(int domainId)
{
    for (const std::shared_ptr<Domain>& domain : domainList)
    {
        if (domain->getId() == domainId)
            return domain->getColor();
    }

    return "";
}

QString VisualizationHandler::prepareDomainMap()
{
    QJsonArray domain_array;

    QString domain_id_str("domain_id");
    QString region_array_str("region_array");
    QString visibility_str("visibility");
    QString region_list_str("region_list");
    QString region_id_str("id");
    QString region_color_str("color");
    QString domain_color_str("domain_color");

    if (m_domain_region_ids.size() != 0)
        m_domain_region_ids.clear();

    std::vector <int> idList = {};
    std::vector<double> domainVolumeList;

    for (const std::shared_ptr<Domain>& domain : domainList)
    {
        domainVolumeList.push_back(domain->getVolume());

        QJsonObject domainObject;

        domainObject.insert(domain_id_str, QJsonValue(domain->getId()));
        domainObject.insert(domain_color_str, QJsonValue(domain->getColor()));

        QJsonArray regionArray;

        std::vector <std::shared_ptr<Region>> regionPointerList = domain->getRegionPointerList();
        for (const std::shared_ptr<Region>& region : regionPointerList)
        {
                QJsonObject regionObject;

                regionObject.insert(region_id_str, QJsonValue(region->getId()));
                regionObject.insert(region_color_str, QJsonValue(region->getColor()));

                regionArray.push_back(QJsonValue(regionObject));
        }
        domainObject.insert(region_list_str, QJsonValue(regionArray));
        domainObject.insert(visibility_str, QJsonValue(domain->getVisibility()));

        domain_array.push_back(QJsonValue(domainObject));
    }

    m_model3dHandler->setDomainList(domainVolumeList);

    QJsonObject final_object;
    final_object.insert(QString("domain_map"), QJsonValue(domain_array));
    QJsonDocument jsonDoc(final_object);

    QString json_string = jsonDoc.toJson();

    return json_string;
}

void VisualizationHandler::setItemVisibilityAll(int parentTypeId, int checkState)
{
    bool flag = true;
    ////qDebug() << "VisualizationHandler::setItemVisibilityAll: " <<" checkState:" << checkState << " parent-id: " << parentTypeId;

    switch (checkState)
    {
    case 0:
        flag = false;
        break;
    case 1:
        flag = true;
        break;
    case 2:
        flag = true;
        break;
    default:
        flag = true;
        break;
    }

   switch (parentTypeId)
    {
       case ObjectCategories::Name::STRATIGRAPHY:
            if (surfaceList.size() > 0)
            {
                changeAllSurfaceVisibility(ObjectCategories::Name::STRATIGRAPHY, flag);//changeAllSurfaceVisibility(1, flag);
                m_stratigraphy_visibility = flag;
                emit(parentGroupVisibilityUpdated(ObjectCategories::Name::STRATIGRAPHY, flag));
                emit(surfaceVisibilityListUpdated(surfaceList));

                //emit(surfaceMapUpdated(getSurfaceColorList(), m_visibilityMap));
                emit(surfaceMapUpdated(getSurfaceColorMap(), getSurfaceVisibilityMap()));
            }
            break;
       case ObjectCategories::Name::STRUCTURE:
            if (surfaceList.size() > 0)
            {
                changeAllSurfaceVisibility(ObjectCategories::Name::STRUCTURE, flag);//changeAllSurfaceVisibility(2, flag);
                m_structural_visibility = flag;
                emit(parentGroupVisibilityUpdated(ObjectCategories::Name::STRUCTURE, flag));
                emit(surfaceVisibilityListUpdated(surfaceList));

                //emit(surfaceMapUpdated(getSurfaceColorList(), m_visibilityMap));
                emit(surfaceMapUpdated(getSurfaceColorMap(), getSurfaceVisibilityMap()));
            }
            break;
       case ObjectCategories::Name::REGION:
            if (getVizFlag() && regionList.size() > 0)
            {
                changeAllRegionVisibility(flag);
                m_region_visibility = flag;
                //- This is to assure that if region is visible, domain is not
                changeAllDomainVisibility(!flag);
                m_domain_visibility = !flag;

                emit(parentGroupVisibilityUpdated(ObjectCategories::Name::REGION, flag));//emit(parentGroupVisibilityUpdated(3, flag));
                emit(regionVisibilityListUpdated(regionList));
                emit(regionMapUpdated(getRegionColorList(), m_region_visibilityMap));
                emit(domainMapUpdated(prepareDomainMap()));
            }
            break;

       case ObjectCategories::Name::DOMAIN_VOLUME:
            if (getVizFlag() && domainList.size() > 0)
            {
                changeAllDomainVisibility(flag);
                m_domain_visibility = flag;
                //- This is to assure that if domain is visible, region is not
                changeAllRegionVisibility(!flag);
                m_region_visibility = !flag;
                
                //qDebug() << "VisualizationHandler::setItemVisibilityAll: flag:" <<  flag << " parent-id: " << parentTypeId;
                emit(parentGroupVisibilityUpdated(ObjectCategories::Name::DOMAIN_VOLUME, flag));//emit(parentGroupVisibilityUpdated(4, flag));
                emit(domainVisibilityListUpdated(domainList));
                emit(domainMapUpdated( prepareDomainMap()) );
                emit(regionMapUpdated(getRegionColorList(), m_region_visibilityMap));
            }
            break;
    
        default:

            break;
    }
    //////qDebug() << "VisualizationHandler::changeSurfaceVisibility: " << index << ", " << flag;
    
}
void VisualizationHandler::setItemColor(int item_id, int parent_type_id, QString color)
{
    //////qDebug() << "VisualizationHandler::setItemColor: item_id=" << item_id << " parent_type_id: " << parent_type_id;
    switch (parent_type_id)
    {
        case ObjectCategories::Name::STRATIGRAPHY:
            //m_surfaceColorMap.setColor(item_id, color);
            changeSingleSurfaceColor(item_id, parent_type_id, color);
            
            break;

        case ObjectCategories::Name::STRUCTURE:
            //m_surfaceColorMap.setColor(item_id, color);
            changeSingleSurfaceColor(item_id, parent_type_id, color);
        
            break;

        case ObjectCategories::Name::REGION:
            changeSingleRegionColor(item_id, parent_type_id, color);
            adjustRegionColorIfExistInDomainList(item_id, color);
            m_model3dHandler->setRegionColor(item_id, color);

            break;

        case ObjectCategories::Name::DOMAIN_VOLUME:
            //Commenting this out
            //Since the legacy does not allow changing domain color
            changeSingleDomainColor(item_id, parent_type_id, color);
            m_model3dHandler->setDomainColor(item_id, color);
            changeAllDomainVisibility(true);

            emit(domainListUpdated(domainList));
            emit(domainMapUpdated(prepareDomainMap()));

            break;

        case ObjectCategories::Name::REGION_IN_DOMAIN:
            changeSingleRegionColor(item_id, parent_type_id, color);
            
            m_model3dHandler->setRegionColor(item_id, color);

            emit(domainListUpdated(domainList));
            break;
    }
    
}

void VisualizationHandler::changeSingleSurfaceColor(int itemDataId, int type_id, QString newColor)
{
    int i = 0;
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        if (surface->getId() == itemDataId)
        {
            surface->setColor(newColor);
            //Saving surface color to the model
            QString desc = "";
            if (type_id == ObjectCategories::Name::STRATIGRAPHY)
                desc = "stratigraphy";
            else if (type_id == ObjectCategories::Name::STRUCTURE)
                desc = "structure";
            m_model3dHandler->setSurfaceData(itemDataId, surface->getName(), desc, newColor);

            std::vector<std::shared_ptr<SurfaceItem>> childList = surface->getSurfaceChildList();
            if (childList.size() > 0)
            {
                for (const std::shared_ptr<SurfaceItem>& child : childList)
                {
                    child->setColor(newColor);
                    m_model3dHandler->setSurfaceData(child->getId(), child->getName(), desc, newColor);

                    emit(surfaceColorUpdated(child->getId(), type_id, newColor));
                }
            }
            emit(surfaceColorUpdated(surface->getId(), type_id, newColor));

            break;
        }
        i++;
    }
    emit(surfaceColorListUpdated(surfaceList));
    //emit(surfaceMapUpdated(getSurfaceColorList(), m_visibilityMap));
    emit(surfaceMapUpdated(getSurfaceColorMap(), getSurfaceVisibilityMap()));
}
// Original 
/*void VisualizationHandler::changeSingleSurfaceColor(int itemDataId, int type_id, QString newColor)
{
    int i = 0;
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        if (surface->getId() == itemDataId)
        {
            surface->setColor(newColor);
            
            emit(surfaceColorUpdated(surface->getId(), type_id, newColor));
            
            break;
        }
        i++;
    }
    emit(surfaceColorListUpdated(surfaceList));
    emit(surfaceMapUpdated(getSurfaceColorList(), m_visibilityMap));
}*/

void VisualizationHandler::changeSingleRegionColor(int itemDataId, int type_id, QString newColor)
{
    int i = 0;
    for (const std::shared_ptr<Region>& region : regionList)
    {
        if (region->getId() == itemDataId)
        {
            region->setColor(newColor);

            emit(regionColorUpdated(region->getId(), type_id, newColor));

            break;
        }
        i++;
    }
    emit(regionColorListUpdated(regionList));
    emit(regionMapUpdated(getRegionColorList(), m_region_visibilityMap));
    emit(domainMapUpdated(prepareDomainMap()));
}

void VisualizationHandler::changeSingleDomainColor(int itemDataId, int type_id, QString newColor)
{
    int i = 0;
    for (const std::shared_ptr<Domain>& domain : domainList)
    {
        if (domain->getId() == itemDataId)
        {
            domain->setColor(newColor);

            //emit(regionColorUpdated(region->getId(), type_id, newColor));
            m_vtkFboItem->changeColor(domain->getId(), type_id, newColor);
            break;
        }
        i++;
    }
    //emit(domainListUpdated(domainList));
    //emit(domainMapUpdated(prepareDomainMap()));
}

void VisualizationHandler::changeSingleSurfaceVisibility(int itemDataId, int typeId, bool visiblilty)
{
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        if (surface->getId() == itemDataId)
        {
            ////qDebug() << "VisualizationHandler::changeSingleSurfaceVisibility: itemDataId=" << itemDataId;
            surface->setVisibility(visiblilty);

            std::vector<std::shared_ptr<SurfaceItem>> childList = surface->getSurfaceChildList();
            if (childList.size() > 0)
            {
                /*for (const std::shared_ptr<SurfaceItem>& child : childList)
                {
                    //qDebug() << "VisualizationHandler::changeSingleSurfaceVisibility: child Id=" << child->getId();
                    child->setVisibility(visiblilty);
                    //emit(surfaceVisibilityUpdated(child->getId(), typeId, visiblilty));
                    
                }
                */
                changeChildSurfaceVisibility(surface, typeId, visiblilty);
            }
            
            break;
        }

    }
    
}

/*void VisualizationHandler::changeSingleSurfaceVisibility(int itemDataId, bool visiblilty)
{
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        if (surface->getId() == itemDataId)
        {
            ////qDebug() << "VisualizationHandler::changeSingleSurfaceVisibility: itemDataId=" << itemDataId;
            surface->setVisibility(visiblilty);

            break;
        }
    
    }
    
}*/

void VisualizationHandler::changeSingleDomainVisibility(int itemDataId, bool visiblilty)
{
    for (const std::shared_ptr<Domain>& domain : domainList)
    {
        if (domain->getId() == itemDataId)
        {
            domain->setVisibility(visiblilty);

            break;
        }
    
    }

}

void VisualizationHandler::changeAllSurfaceVisibility(int surfaceCategory, bool visibility)
{
    //////qDebug() << "VisualizationHandler::changeAllSurfaceVisibility1: " << surfaceCategory << ", visibility:" << visibility;

    m_vtkFboItem->changeVisibilityBySurfaceCategory(surfaceCategory, visibility);

    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        //////qDebug() << "VisualizationHandler::changeAllSurfaceVisibility: Bingo: " << surface->getCategory();
        if (surface->getCategory() == surfaceCategory)
        {
            //////qDebug() << "VisualizationHandler::changeAllSurfaceVisibility2: match" << surfaceCategory << ", visibility:" << visibility;
            surface->setVisibility(visibility);
            m_visibilityMap[surface->getId()] = visibility;

            std::vector<std::shared_ptr<SurfaceItem>> childList = surface->getSurfaceChildList();
            if (childList.size() > 0)
            {
                /*for (const std::shared_ptr<SurfaceItem>& child : childList)
                {
                    //qDebug() << "VisualizationHandler::changeSingleSurfaceVisibility: child Id=" << child->getId();
                    child->setVisibility(visiblilty);
                    //emit(surfaceVisibilityUpdated(child->getId(), typeId, visiblilty));

                }
                */
                changeChildSurfaceVisibility(surface, surfaceCategory, visibility);
            }
        }

    }

}

/*void VisualizationHandler::changeAllSurfaceVisibility(int surfaceCategory, bool visibility)
{
    //////qDebug() << "VisualizationHandler::changeAllSurfaceVisibility1: " << surfaceCategory << ", visibility:" << visibility;

    m_vtkFboItem->changeVisibilityBySurfaceCategory(surfaceCategory, visibility);

    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        //////qDebug() << "VisualizationHandler::changeAllSurfaceVisibility: Bingo: " << surface->getCategory();
        if (surface->getCategory() == surfaceCategory)
        {
            //////qDebug() << "VisualizationHandler::changeAllSurfaceVisibility2: match" << surfaceCategory << ", visibility:" << visibility;
            surface->setVisibility(visibility);
            m_visibilityMap[surface->getId()] = visibility;

        }

    }

}*/

void VisualizationHandler::changeAllRegionVisibility(bool visibility)
{
    for (const std::shared_ptr<Region>& region : regionList)
    {
        region->setVisibility(visibility);
        m_vtkFboItem->changeVisibility(region->getId(), 3, visibility);
        m_region_visibilityMap[region->getId()] = visibility;
    }

}

void VisualizationHandler::changeAllDomainVisibility(bool visibility)
{
    for (const std::shared_ptr<Domain>& domain : domainList)
    {
        domain->setVisibility(visibility);
        m_vtkFboItem->changeVisibility(domain->getId(), 4, visibility);
    }

}
void VisualizationHandler::changeSingleRegionVisibility(int itemDataId, bool visiblilty)
{
    int i = 0;
    for (const std::shared_ptr<Region>& region : regionList)
    {
        if (region->getId() == itemDataId)
        {
            region->setVisibility(visiblilty);

            break;
        }
        i++;
    }

}
std::vector<QString> VisualizationHandler::getColorList()
{
    return m_customColorMap.getAll();
}

std::vector<QString> VisualizationHandler::getSurfaceColorList()
{
    std::vector<QString> surfaceColorList;
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        surfaceColorList.push_back(surface->getColor());

        std::vector<std::shared_ptr<SurfaceItem>> childList = surface->getSurfaceChildList();
        if (childList.size() > 0)
        {
            for (const std::shared_ptr<SurfaceItem>& child : childList)
            {
                surfaceColorList.push_back(child->getColor());

            }
        }
                
    }

    return surfaceColorList;
}

std::vector<QString> VisualizationHandler::getRegionColorList()
{
    //return m_regionColorMap.getAll();

    std::vector<QString> colorList;
    for (const std::shared_ptr<Region>& region : regionList)
    {
        colorList.push_back(region->getColor());
    }

    return colorList;
}

std::vector<bool> VisualizationHandler::getVisibilityList()
{
    std::vector<bool> surfaceVisibilityList;

    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        surfaceVisibilityList.push_back(true);
    }

    return surfaceVisibilityList;
}

void VisualizationHandler::reset()
{
    m_customColorMap.clear();
    m_visibilityMap.clear();
    surfaceList.clear();
    regionList.clear();
    domainList.clear();

    //m_model3dHandler->clearDomains();
    
}

void VisualizationHandler::addNewColorData()
{
    m_customColorMap.addColor(m_customColorMap.size());

    emit newDomainColorReady(m_customColorMap.getAll());

}

void VisualizationHandler::createNewDomain(QString name, QString description)
{
    if (isItemNameUnique(name, 4))
    {
        //std::shared_ptr<Domain> newDomain = std::make_shared<Domain>(domainList.size(), name, description);
        std::shared_ptr<Domain> newDomain = std::make_shared<Domain>(determineNewDomainId(), name, description);
        newDomain->setVisibility(false);
        newDomain->setColor(m_domainColorMap.getCustomColorByIndex(domainList.size()));

        domainList.push_back(newDomain);
        m_domain_visibilityMap.push_back(false);

        m_model3dHandler->setDomain(newDomain->getId(), name, description);
        m_model3dHandler->setDomainColor(newDomain->getId(), newDomain->getColor());

        emit(domainListUpdated(domainList));

    }
    else emit(duplicateItemNameDetected("Domain"));
}
void VisualizationHandler::addToDomainList(int id, QString name, QString description)
{
    
    ////qDebug() << "VisualizationHandler::addToDomainList: " << name ;
    if (id == -1) {
        id = domainList.size();
    }
    std::shared_ptr<Domain> newDomain = std::make_shared<Domain>(id, name, description);
    newDomain->setVisibility(m_domain_visibility);
    newDomain->setColor(m_domainColorMap.getCustomColorByIndex(domainList.size()));

    domainList.push_back(newDomain);
    m_domain_visibilityMap.push_back(m_domain_visibility);

    m_model3dHandler->setDomain(newDomain->getId(), name, description);
    m_model3dHandler->setDomainColor(newDomain->getId(), newDomain->getColor());

    // DEBUG CODE
    /*//qDebug() << "VisualizationHandler::addToDomainList: size_of_domain_list = " << domainList.size();
    for (const std::shared_ptr<Domain>& domain : domainList)
    {
        //qDebug() << " - domain id = " << domain->getId() << ", name=" << domain->getName();
    }*/
    emit(domainListUpdated(domainList));
    emit(domainMapUpdated(prepareDomainMap()));
}

void VisualizationHandler::removeSingleObject(int parentTypeId, int dataId, int parentId)
{
    //////qDebug() << "VisualizationHandler::removeSingleObject: type=" << parentTypeId << ", dataId=" << dataId << ", parentId=" << parentId;

    int index = 0;

    switch (parentTypeId)
    {
        case ObjectCategories::Name::DOMAIN_VOLUME:
            try {
                for (const std::shared_ptr<Domain>& domain : domainList)
                {
                    if (domain->getId() == dataId)
                    {
                        ////qDebug() << "VisualizationHandler::removeSingleObject: domainId=" << domain->getId() << ", index=" << index;
                        domain->clearRegionPointerList();
                        domainList.erase(domainList.begin() + index);
                        
                        emit(domainListUpdated(domainList));
                        emit(domainMapUpdated(prepareDomainMap()));

                        m_vtkFboItem->removeSingleDomain(dataId);
                        m_model3dHandler->eraseDomain(dataId);
                        
                        break;
                    }
                    index++;
                }

            }
            catch (int err) {
                ////qDebug() << "VisualizationHandler::removeSingleObject - domain";
            }
            break;

        case ObjectCategories::Name::REGION_IN_DOMAIN:
            std::shared_ptr<Domain> domain = getDomainById(parentId);
            //qDebug() << "VisualizationHandler::removeSingleObject: REgion in domain remove: parent-id=" <<parentId<< ", domain_id:" << domain->getId()<< ", domain_name:" << domain->getName();
            if (domain != nullptr)
            {
                std::vector <std::shared_ptr<Region>> regionPointerList = domain->getRegionPointerList();
               qDebug() << "VisualizationHandler::removeRegionFromDomain: regionPointerListSize=" << regionPointerList.size();
                try {
                    index = 0;
                    for (const std::shared_ptr<Region>& region : regionPointerList)
                    {
                        if (region->getId() == dataId)
                        {
                            if (regionPointerList.size() > 1)
                            {
                                regionPointerList.erase(regionPointerList.begin() + index);
                                domain->setRegionPointerList(regionPointerList);
                                domain->setVolume(0.0 - region->getVolume());

                                m_vtkFboItem->removeSingleRegionFromDomain(dataId, parentId);
                                m_model3dHandler->eraseRegionIdFromDomain(domain->getId(), dataId);
                                emit(domainListUpdated(domainList));
                                emit(domainMapUpdated(prepareDomainMap()));
                                
                            }
                            else {
                                // The domain contains only one region. 
                                // Since there should be no empty domain in the domainList
                                // erase domain from the domain list
                                int domainId = domain->getId();
                                int counter = 0;
                                for (const std::shared_ptr<Domain>& eachDomain : domainList)
                                {
                                    if (eachDomain->getId() == domainId)
                                    {
                                        domainList.erase(domainList.begin() + counter);

                                        
                                        emit(domainListUpdated(domainList));
                                        emit(domainMapUpdated(prepareDomainMap()));
                                        
                                        m_vtkFboItem->removeSingleDomain(domainId);
                                        m_model3dHandler->eraseDomain(domainId);

                                        break;
                                    }
                                    counter++;
                                }
                            }
                            
                            break;
                        }
                        index++;
                    }

                }
                catch (int err) {
                    ////qDebug() << "VisualizationHandler::removeSingleObject - domain child";
                }
            }
            break;
    }//end: switch
}

QString VisualizationHandler::getSurfaceName(int dataId)
{
    try{
        ////qDebug() << "VisualizationHandler::getSurfaceName 1: id=" << dataId << ", surfaceList size= " << surfaceList.size();
        for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
        {
            ////qDebug() << "VisualizationHandler::getSurfaceName 1.1: id=" << dataId << ", surface_id="<< surface->getId();
            if (surface->getId() == dataId)
            {
                ////qDebug() << "VisualizationHandler::getSurfaceName 2";
                return surface->getName();
            }

        }
        ////qDebug() << "VisualizationHandler::getSurfaceName 3";
    }
    catch (int err) {
        ////qDebug() << "VisualizationHandler::getSurfaceName 4";
    }
    ////qDebug() << "VisualizationHandler::getSurfaceName 5";

    return "";
}

QString VisualizationHandler::getItemName(int parentTypeId, int itemDataId, int parentId)
{
    ////qDebug() << "VisualizationHandler::getItemName: " << parentTypeId << "," << itemDataId;
    //////qDebug() << "VisualizationHandler::getItemDescription: " << parentTypeId << "," << itemDataId;
    QString result = "";

    switch (parentTypeId)
    {
        case ObjectCategories::Name::STRATIGRAPHY:
            result = getSurfaceName(itemDataId);
            break;

        case ObjectCategories::Name::STRUCTURE:
            result = getSurfaceName(itemDataId);
            break;
        
        case ObjectCategories::Name::REGION:
            for (const std::shared_ptr<Region>& region : regionList)
            {
                if (region->getId() == itemDataId)
                    result = region->getName();
            
            }
            break;

        case ObjectCategories::Name::DOMAIN_VOLUME:
            for (const std::shared_ptr<Domain>& domain : domainList)
            {
                if (domain->getId() == itemDataId)
                    result = domain->getName();
            
            }
            break;

        case ObjectCategories::Name::REGION_IN_DOMAIN:
            for (const std::shared_ptr<Region>& region : regionList)
            {
                if (region->getId() == itemDataId)
                    result = region->getName();

            }
            break;
        case ObjectCategories::Name::STRATIGRAPHIC_CHILD:
            ////qDebug() << "VisualizationHandler::getItemName: BINGO 1";
            std::shared_ptr childSurface = findChildSurface(itemDataId, parentId);
            ////qDebug() << "VisualizationHandler::getItemName: BINGO 2";
            result = childSurface->getName();
            ////qDebug() << "VisualizationHandler::getItemName: BINGO 3";
            break;
        }
        //////qDebug() << "VisualizationHandler::getItemDescription: result=" << result;
        return result;
}

QString VisualizationHandler::getSurfaceDescription(int dataId)
{
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        if (surface->getId() == dataId)
            return surface->getDescription();

    }
    return "";
}

QString VisualizationHandler::getSurfaceColor(int dataId)
{
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        if (surface->getId() == dataId)
            return surface->getColor();

    }
    return "";
}

QString VisualizationHandler::getChildSurfaceColor(int dataId)
{
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        if (surface->getId() == dataId)
            return surface->getColor();
        else {
            std::vector <std::shared_ptr<SurfaceItem>> childList = surface->getSurfaceChildList();
            for (const std::shared_ptr<SurfaceItem>& child : childList)
            {
                if (child->getId() == dataId)
                    return child->getColor();
            }
        }

    }
    return "";
}

std::shared_ptr<SurfaceItem> VisualizationHandler::findChildSurface(int dataId, int parentId)
{
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        if (surface->getId() == parentId)
        {
            std::vector <std::shared_ptr<SurfaceItem>> childList = surface->getSurfaceChildList();
            if (childList.size() > 0)
            {
                for (const std::shared_ptr<SurfaceItem>& surfaceChild : childList)
                {
                    if (surfaceChild->getId() == dataId)
                        return surfaceChild;

                }
            }
        }

    }

    return NULL;
}

QString VisualizationHandler::getItemDescription(int parentTypeId, int itemDataId)
{
    ////qDebug() << "VisualizationHandler::getItemDescription: " << parentTypeId << "," << itemDataId;
    QString result = "";

    switch (parentTypeId)
    {
        case ObjectCategories::Name::STRATIGRAPHY:
            result = getSurfaceDescription(itemDataId);
            break;

        case ObjectCategories::Name::STRUCTURE:
            result = getSurfaceDescription(itemDataId);
            break;

        case ObjectCategories::Name::REGION:
            for (const std::shared_ptr<Region>& region : regionList)
            {
                if (region->getId() == itemDataId)
                    result = region->getDescription();
                
            }
            break;

        case ObjectCategories::Name::DOMAIN_VOLUME:
            //std::shared_ptr<Domain>&domain = domainList.at(itemDataId);
            //result = domain->getDescription();
            for (const std::shared_ptr<Domain>& domain : domainList)
            {
                if (domain->getId() == itemDataId)
                    result = domain->getDescription();
                
            }
            break;

        case ObjectCategories::Name::REGION_IN_DOMAIN:
            for (const std::shared_ptr<Region>& region : regionList)
            {
                if (region->getId() == itemDataId)
                    result = region->getDescription();

            }
            break;
        case ObjectCategories::Name::STRATIGRAPHIC_CHILD:
            result = getSurfaceDescription(itemDataId);
            break;
    }
    //////qDebug() << "VisualizationHandler::getItemDescription: result=" << result;
    return result;
}

QString VisualizationHandler::getItemColor(int parentTypeId, int itemDataId)
{
    ////qDebug() << "VisualizationHandler::getItemColor: " << parentTypeId << "," << itemDataId;
    QString result = "";

    switch (parentTypeId)
    {
    case ObjectCategories::Name::STRATIGRAPHY:
        result = getSurfaceColor(itemDataId);
        break;

    case ObjectCategories::Name::STRATIGRAPHIC_CHILD:
        result = getChildSurfaceColor(itemDataId);
        break;

    case ObjectCategories::Name::STRUCTURE:
        result = getSurfaceColor(itemDataId);
        break;

    case ObjectCategories::Name::REGION:
        for (const std::shared_ptr<Region>& region : regionList)
        {
            if (region->getId() == itemDataId)
                result = region->getColor();

        }
        break;

    case ObjectCategories::Name::DOMAIN_VOLUME:
        //std::shared_ptr<Domain>&domain = domainList.at(itemDataId);
        //result = domain->getDescription();
        for (const std::shared_ptr<Domain>& domain : domainList)
        {
            if (domain->getId() == itemDataId)
                result = domain->getColor();

        }
        break;

    case ObjectCategories::Name::REGION_IN_DOMAIN:
        for (const std::shared_ptr<Region>& region : regionList)
        {
            if (region->getId() == itemDataId)
                result = region->getColor();

        }
        break;

    }
    
    return result;
}

QString VisualizationHandler::getItemType(int parentTypeId, int itemDataId)
{
    ////qDebug() << "VisualizationHandler::getItemType: " << parentTypeId << "," << itemDataId;
    QString result = "";

    switch (parentTypeId)
    {
    case ObjectCategories::Name::STRATIGRAPHY:
        result = "STRATIGRAPHY";
        break;

    case ObjectCategories::Name::STRUCTURE:
        result = "STRUCTURE";
        break;

    case ObjectCategories::Name::REGION:
        result = "REGION";
        break;

    case ObjectCategories::Name::DOMAIN_VOLUME:
        result = "DOMAIN";
        break;

    case ObjectCategories::Name::REGION_IN_DOMAIN:
        result = "REGION INSIDE DOMAIN ";
        break;

    }

    return result;
}
/*void VisualizationHandler::saveSurfaceData(int parentTypeId, int itemDataId, QString itemNameText, QString itemDescription)
{
    try {
        for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
        {
            if (surface->getId() == itemDataId)
            {
                if (surface->getDescription() != itemDescription)
                {
                    surface->setDescription(itemDescription);
                    emit(itemDescriptionUpdated("Surface"));
                }
                if (surface->getName() != itemNameText)
                {
                    if (isItemNameUnique(itemNameText, parentTypeId))
                    {
                        surface->reName(itemNameText);

                        //Saving surface color to the model
                        QString desc = "";
                        if (surface->getCategory() == ObjectCategories::Name::STRATIGRAPHY)
                            desc = "stratigraphy";
                        else if (surface->getCategory() == ObjectCategories::Name::STRUCTURE)
                            desc = "structure";
                        //qDebug() << "VisualizationHandler::saveSurfaceData: id=" << itemDataId << ", name=" << surface->getName() << ", type=" << desc << ", color=" << surface->getColor();
                        m_model3dHandler->setSurfaceData(itemDataId, surface->getName(), desc, surface->getColor());

                        emit(surfaceListUpdated(surfaceList));

                    }
                    else emit(duplicateItemNameDetected("Surface"));
                }
                break;
            }

        }

    }
    catch (int err) {
        //qDebug() << "VisualizationHandler::saveSurfaceData: saveSurfaceData";
    }
}
*/
void VisualizationHandler::updateSingleSurfaceData(const std::shared_ptr<SurfaceItem> surface, int parentTypeId, int itemDataId, QString itemNameText, QString itemDescription)
{
    //if (surface->getName() != itemNameText)
    if (surface->getId() == itemDataId)
    {
        if (isItemNameUnique(itemNameText, parentTypeId))
        {
            surface->reName(itemNameText);

            //Saving surface color to the model
            QString desc = "";
            if (surface->getCategory() == ObjectCategories::Name::STRATIGRAPHY)
                desc = "stratigraphy";
            else if (surface->getCategory() == ObjectCategories::Name::STRUCTURE)
                desc = "structure";
            ////qDebug() << "VisualizationHandler::saveSurfaceData: id=" << itemDataId << ", name=" << surface->getName() << ", type=" << desc << ", color=" << surface->getColor();
            m_model3dHandler->setSurfaceData(itemDataId, surface->getName(), desc, surface->getColor());

            emit(surfaceListUpdated(surfaceList));

        }
        else emit(duplicateItemNameDetected("Surface"));
    }
}
void VisualizationHandler::saveSurfaceData(int parentTypeId, int itemDataId, QString itemNameText, QString itemDescription)
{
    try {
        for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
        {
            if (surface->getId() == itemDataId)
            {
                    updateSingleSurfaceData(surface, parentTypeId, itemDataId, itemNameText, itemDescription);
                    break;
            }            
        }

    }
    catch (int err) {
        //qDebug() << "VisualizationHandler::saveSurfaceData: saveSurfaceData";
    }
}

void VisualizationHandler::saveChildSurfaceData(int parentTypeId, int itemDataId, QString itemNameText, QString itemDescription)
{
    try {
        for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
        {
            std::vector<std::shared_ptr<SurfaceItem>> childList = surface->getSurfaceChildList();
            if (childList.size() > 0)
            {
                for (const std::shared_ptr<SurfaceItem>& child : childList)
                {
                    if (child->getId() == itemDataId)
                    {
                        updateSingleSurfaceData(child, parentTypeId, itemDataId, itemNameText, itemDescription);
                        break;
                    }
                }
            }
            
        }

    }
    catch (int err) {
        //qDebug() << "VisualizationHandler::saveChildSurfaceData: saveChildSurfaceData";
    }
}
void VisualizationHandler::saveItemFormData(int parentTypeId, int itemDataId, QString itemNameText, QString itemDescription)
{
    ////qDebug() << "VisualizationHandler::saveItemFormData: " << parentTypeId << "," << itemDataId << ", " << itemNameText << ", " << itemDescription;
    
    switch (parentTypeId)
    {
        case ObjectCategories::Name::STRATIGRAPHY:
            saveSurfaceData(parentTypeId, itemDataId, itemNameText, itemDescription);
            break;
            
        case ObjectCategories::Name::STRATIGRAPHIC_CHILD:
            saveChildSurfaceData(parentTypeId, itemDataId, itemNameText, itemDescription);
            break;
        case ObjectCategories::Name::STRUCTURE:
            saveSurfaceData(parentTypeId, itemDataId, itemNameText, itemDescription);
            break;

        case ObjectCategories::Name::REGION:
            try {
                for (const std::shared_ptr<Region>& region : regionList)
                {
                    if (region->getId() == itemDataId)
                    {
                        if (region->getDescription() != itemDescription)
                        {
                            region->setDescription(itemDescription);
                            emit(itemDescriptionUpdated("Region"));
                        }
                        if (region->getName() != itemNameText)
                        {
                            if (isItemNameUnique(itemNameText, parentTypeId))
                            {
                                region->reName(itemNameText);
                                m_model3dHandler->setRegion(region->getId(), itemNameText, itemDescription);
                                emit(regionListUpdated(regionList));
                                emit(domainListUpdated(domainList));
                            }
                            else emit(duplicateItemNameDetected("Region")); 
                        }
                        break;
                    }

                }

            }
            catch (int err) {
                ////qDebug() << "VisualizationHandler::saveItemFormData: saveRegionData";
            }
            break;
        case ObjectCategories::Name::DOMAIN_VOLUME:
            try {
                for (const std::shared_ptr<Domain>& domain : domainList)
                {
                    if (domain->getId() == itemDataId)
                    {
                        if (domain->getDescription() != itemDescription)
                        {
                            domain->setDescription(itemDescription);
                            emit(itemDescriptionUpdated("Domain"));
                        }
                        if (domain->getName() != itemNameText)
                        {
                            if (isItemNameUnique(itemNameText, parentTypeId))
                            {
                                domain->reName(itemNameText);
                                m_model3dHandler->setDomain(domain->getId(), itemNameText, itemDescription);
                                emit(domainListUpdated(domainList));
                            }
                            else  emit(duplicateItemNameDetected("Domain"));
                        }
                        break;
                    }

                }

            }
            catch (int err) {
                ////qDebug() << "VisualizationHandler::saveItemFormData: saveDomainData";
            }

            break;

        case ObjectCategories::Name::REGION_IN_DOMAIN:
            try {
                for (const std::shared_ptr<Region>& region : regionList)
                {
                    if (region->getId() == itemDataId)
                    {
                        if (region->getDescription() != itemDescription)
                        {
                            region->setDescription(itemDescription);
                            emit(itemDescriptionUpdated("Region"));
                        }
                        if (region->getName() != itemNameText)
                        {
                            if (isItemNameUnique(itemNameText, parentTypeId))
                            {
                                region->reName(itemNameText);
                                m_model3dHandler->setRegion(region->getId(), itemNameText, itemDescription);
                                emit(regionListUpdated(regionList));
                                emit(domainListUpdated(domainList));
                            }
                            else emit(duplicateItemNameDetected("Region"));
                        }
                        break;
                    }

                }

            }
            catch (int err) {
                ////qDebug() << "VisualizationHandler::saveItemFormData: saveRegionInDomainData";
            }
            break;

        
        }//end:switch
}

bool VisualizationHandler::isSurfaceNameUnique(QString newItemName)
{
    try {
        for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
        {
            if (surface->getName() == newItemName)
            {
                return false;
                
            }

        }

    }
    catch (int err) {
        ////qDebug() << "VisualizationHandler::isSurfaceNameUnique";
    }
    return true;
}
bool VisualizationHandler::isItemNameUnique(QString newItemName, int parentTypeId)
{
    bool result = true;
    
    switch (parentTypeId)
    {
        case ObjectCategories::Name::STRATIGRAPHY://1:
            result = isSurfaceNameUnique(newItemName);
            break;

        case ObjectCategories::Name::STRUCTURE://2:
            result = isSurfaceNameUnique(newItemName);
            break;

        case ObjectCategories::Name::REGION://3:
            try {
                for (const std::shared_ptr<Region>& region : regionList)
                {
                    if (region->getName() == newItemName)
                    {
                        result = false;
                        break;
                    }

                }
                
            }
            catch (int err) {
                ////qDebug() << "VisualizationHandler::isRegionNameUnique";
            }
            break;
        case ObjectCategories::Name::DOMAIN_VOLUME://4:
            try {
                for (const std::shared_ptr<Domain>& domain : domainList)
                {
                    if (domain->getName() == newItemName)
                    //if (QString::compare(domain->getName(), newItemName, Qt::CaseInsensitive))
                    {
                        ////qDebug() << "VisualizationHandler::isItemNameUnique: BP2: domain id=" << domain->getName();
                        result = false;
                        break;
                    }

                }
                
            }
            catch (int err) {
                ////qDebug() << "VisualizationHandler::isDomainNameUnique";
            }
            break;

    
    }//end switch

    return result;
}

void VisualizationHandler::setVizFlag(bool value)
{
    m_viz = value;
    //emit(vizFlagChanged(m_viz));

    emit(fdWindowClosed());
    if (m_viz)
    {
        /*fillRegionsList();
        m_vtkFboItem->addRegions();
        */
        fillRegionsList();
        m_region_visibility = true;
        m_domain_visibility = false;

        QFuture<void> futureA = QtConcurrent::run(m_vtkFboItem, &VtkFboItem::addRegions);
        QFutureWatcher<void> watcherA;
        //connect(&watcher, SIGNAL(finished()), this, SIGNAL(modelLoadingCompleted()));
        watcherA.setFuture(futureA);

        watcherA.waitForFinished();

        if (watcherA.isCanceled())
        {
            
        }
        else
        {
            takeCareOFRegionVizFlagUpdate();

        }

    }
    else
    {
        /*removeRegionPointerListFromDomains();
        m_vtkFboItem->removePreviousRegions();
        m_vtkFboItem->removePreviousDomains();
        emit(clearRegionCurveBox());
        */

        removeRegionPointerListFromDomains();
        QFuture<void> future = QtConcurrent::run(m_vtkFboItem, &VtkFboItem::removePreviousRegions);
        QFutureWatcher<void> watcher;
        //connect(&watcher, SIGNAL(finished()), this, SIGNAL(modelLoadingCompleted()));
        watcher.setFuture(future);

        watcher.waitForFinished();

        if (watcher.isCanceled())
        {
            ////qDebug() << "VisualizationHandler::setVizFlag() OFF: canceled!!!";

        }
        else
        {
            m_vtkFboItem->removePreviousDomains();
            regionList.clear();
            emit(regionListUpdated(regionList));
            domainList.clear();
            emit(domainListUpdated(domainList));

            //qDebug() << "VisualizationHandler::setVizFlag() OFF: finished!!!";

            takeCareOFRegionVizFlagUpdate();
                        
            emit(clearRegionCurveBox());
            
        }
                
    }

    
}

void VisualizationHandler::takeCareOFRegionVizFlagUpdate()
{
    //qDebug() << "VisualizationHandler::takeCareOFRegionVizFlagUpdate()" << "RegionViz on the FrontView start";
    emit(vizFlagChanged(m_viz));
}

bool VisualizationHandler::getVizFlag()
{
    return m_viz;
}

/*void VisualizationHandler::fillRegionsList() 
{
    //std::vector<double> volume_list = m_model3dHandler->getRegionList();
    //Compute TetrahedralMesh Volumes
    std::vector<double> region_list;
    m_model3dHandler->model().ComputeTetrahedralMeshVolumes(region_list);
    //Save region_list size
    m_model3dHandler->setRegionsList(region_list);

    if (region_list.size() > 0)
    {
        for (int i = 0; i < region_list.size(); i++)
        {
            if (region_list[i] > 0.0)
            {
                QString name = "Region" + QString::number(i);
                QString desc = "";
                QString color = m_regionColorMap.getCustomColorByIndex(i+1);
                //QString color = m_regionColorMap.getColor(i);
            
                //std::shared_ptr eachRegion = std::make_shared<Region>(regionList.size(), name, desc, color, region_list[i], true);
                std::shared_ptr eachRegion = std::make_shared<Region>(i, name, desc, color, region_list[i], true);
                regionList.push_back(eachRegion);

                m_region_visibilityMap.push_back(true);
            }
        }
        
    }
    emit(regionsAdded(regionList));
    emit(regionMapUpdated(getRegionColorList(), m_region_visibilityMap));
}
*/
void VisualizationHandler::fillRegionsList()
{
    m_model3dHandler->enforceRegionConsistency();

    //std::vector<double> volume_list = m_model3dHandler->getRegionList();
    //Compute TetrahedralMesh Volumes
    std::vector<double> region_list;
    m_model3dHandler->model().ComputeTetrahedralMeshVolumes(region_list);
    //Save region_list size
    m_model3dHandler->setRegionsList(region_list);
    
    //qDebug() << "Region List: A";
    printVector(region_list);
    QString name;
    QString description;
    QString color;

    if (region_list.size() > 0)
    {
        for (int i = 0; i < region_list.size(); i++)
        {
            //if (region_list[i] > 0.0)
            {
                m_model3dHandler->getRegionMetadataById(i, name, description, color);
                
                if (name == "")
                {
                    name = "Region" + QString::number(i);
                    //m_model3dHandler->setRegion(i, name, description);
                }
                
                if (color == "")
                {
                    color = m_regionColorMap.getCustomColorByIndex(i + 1);
                    //m_model3dHandler->setRegionColor(i, color);
                }
                
                std::shared_ptr eachRegion = std::make_shared<Region>(i, name, description, color, region_list[i], true);
                regionList.push_back(eachRegion);

                m_region_visibilityMap.push_back(true);
            }
        }

    }
    //qDebug() << "Region List: B size=" << regionList.size();
    emit(regionsAdded(regionList));
    emit(regionMapUpdated(getRegionColorList(), m_region_visibilityMap));
}
void VisualizationHandler::clearRegionsList()
{
    if (regionList.size() != 0)
        regionList.clear();
    emit(regionsRemoved());
}

void VisualizationHandler::initiateSurfaceSubmissoin()
{
    m_vtkFboItem->removePreviousSurfaces();
    //m_treeModel->update(currentSurfaceList);
    //updateSurfaceList();
}

void VisualizationHandler::loadMeshes()
{
    //m_vtkFboItem->removePreviousSurfaces();
    //emit(modelLoadingCompleted());

    QFuture<void> future = QtConcurrent::run(m_vtkFboItem, &VtkFboItem::removePreviousSurfaces);    
    QFutureWatcher<void> watcher;
    connect(&watcher, SIGNAL(finished()), this, SIGNAL(modelLoadingCompleted()));
    watcher.setFuture(future);

    watcher.waitForFinished();

    if (watcher.isCanceled())
    {
        ////qDebug() << "VisualizationHandler::loadMeshes(): canceled!!!";
       
    }
    else
    {
        ////qDebug() << "VisualizationHandler::loadMeshes(): finished!!!";
        emit(modelLoadingCompleted());
    }

}
void VisualizationHandler::addSubmittedSurfaces()
{
    m_vtkFboItem->addSurfaces();

}

void VisualizationHandler::updateSurfaceList()
{
    //check if there were color assigned before
    std::vector<QString> surfaceColorList = getSurfaceColorList();
    std::vector<bool> surfaceVisibilityList = m_visibilityMap;

    m_surfaceColorQmap = getSurfaceColorQmap();
    m_surfaceVisibilityQmap = getSurfaceVisibilityQmap();

    //Update VizHandler data
    if (surfaceList.size() != 0)
    {
        surfaceList.clear();
        m_visibilityMap.clear();
    }


    //std::vector <int> canvas_surface_list = m_model3dHandler->getSurfaceType();
    std::vector <int> canvas_surface_list = m_model3dHandler->getSurfaceIndices();
    ////qDebug() << "BINGO: " << canvas_surface_list;


    if (canvas_surface_list.size() > 0)
    {
        for (int i = 0; i < canvas_surface_list.size(); i++)
        {
            //int group_id = m_model3dHandler->getSurfaceGroup(i+1);
            int group_id = m_model3dHandler->getSurfaceGroup(canvas_surface_list[i]);
            ////qDebug() << "VisualizationHandler:: updateSurfaceList: group-id = " << group_id << ", of surface =" << canvas_surface_list[i];

            //createSurfaceItem(int category, int surfaceId, std::vector<QString> surfaceColorList, std::vector<bool> surfaceVisibilityList)

            //std::shared_ptr<SurfaceItem> eachSurface = createSurfaceItem(canvas_surface_list[i], i, surfaceColorList, surfaceVisibilityList);
            std::shared_ptr<SurfaceItem> eachSurface = createSurfaceItem(m_model3dHandler->getSurfaceTypeById(canvas_surface_list[i]), canvas_surface_list[i], surfaceColorList, surfaceVisibilityList);

            if (group_id != -1)
            {
                if ((group_id != eachSurface->getId()))
                {
                    std::shared_ptr<SurfaceItem> groupSurfaceItem = getSurfaceById(group_id);

                    if (groupSurfaceItem != NULL)
                    {
                        groupSurfaceItem->addToChildList(eachSurface);
                    }
                    else
                    {
                        //Create the group surface item first
                        //std::shared_ptr<SurfaceItem> groupSurfaceItem = createSurfaceItem(canvas_surface_list[i], group_id, surfaceColorList, surfaceVisibilityList);
                        std::shared_ptr<SurfaceItem> groupSurfaceItem = createSurfaceItem(m_model3dHandler->getSurfaceTypeById(canvas_surface_list[i]), group_id, surfaceColorList, surfaceVisibilityList);
                        groupSurfaceItem->addToChildList(eachSurface);
                        surfaceList.push_back(groupSurfaceItem);
                    }
                }
                else if ((group_id == eachSurface->getId()))
                {
                    std::shared_ptr<SurfaceItem> groupSurfaceItem = getSurfaceById(group_id);

                    if (groupSurfaceItem != NULL)
                    {
                        groupSurfaceItem->addToChildList(eachSurface);
                    }
                    else
                    {
                        std::shared_ptr<SurfaceItem> each_surface_copy = std::make_shared<SurfaceItem>(*eachSurface); //Using copy constructor

                        //Add the copy of the surface to the parent
                        eachSurface->addToChildList(each_surface_copy);
                        surfaceList.push_back(eachSurface);
                    }
                }

            }
            else surfaceList.push_back(eachSurface);

            bool visibility = true;
            if (i < surfaceVisibilityList.size())
            {
                visibility = surfaceVisibilityList.at(i);
            }
            m_visibilityMap.push_back(visibility);
        }//end: for
    }
    emit(surfaceListUpdated(surfaceList));

    //emit(surfaceMapUpdated(getSurfaceColorList(), m_visibilityMap));
    emit(surfaceMapUpdated(getSurfaceColorMap(), getSurfaceVisibilityMap()));
}

QString VisualizationHandler::getQString(std::string str)
{
    return QString::fromUtf8(str.data(), int(str.size()));
}

void VisualizationHandler::undo3dSurfaces()
{
    m_visibilityMap.clear();

    m_canvasHandler->undo3d();
    m_model3dHandler->setSurfaceIndices();
    m_vtkFboItem->removePreviousSurfaces();

    //emit(refreshSubmittedCurvesAfterUndoRedo(m_surfaceColorMap.getAll(), m_visibilityMap));
}
void VisualizationHandler::redo3dSurfaces()
{
    m_visibilityMap.clear();
    m_model3dHandler->setSurfaceIndices();
    m_canvasHandler->redo3d();
    m_vtkFboItem->removePreviousSurfaces();
    
    //emit(refreshSubmittedCurvesAfterUndoRedo(m_surfaceColorMap.getAll(), m_visibilityMap));
}

QUrl VisualizationHandler::getLocalPath(const QUrl& path)
{
    //////qDebug() << "VisualizationHandler:: Saving Model: " << path;

    if (path.isLocalFile())
    {
        return path.toLocalFile();

    }
    return path;
}

std::string VisualizationHandler::getAbsPath(const QUrl& path)
{
    //////qDebug() << "VisualizationHandler:: Saving Model: " << path;

    QUrl localPath;
    if (path.isLocalFile())
    {
        localPath = path.toLocalFile();

    }
    else localPath = path;

    QString pathString = localPath.toEncoded();
    pathString = pathString.replace("%20", "\ ");
    return pathString.toUtf8().constData();

}

void VisualizationHandler::loadModel(const QUrl& path)
{
    ////qDebug() << "VisualizationHandler:: Open Model ";

    QFuture<void> futureA = QtConcurrent::run(this, &VisualizationHandler::prepareToLoadModel);
    QFutureWatcher<void> watcherA;
    watcherA.setFuture(futureA);

    watcherA.waitForFinished();

    if (watcherA.isCanceled())
    {
        ////qDebug() << "VisualizationHandler::loadModel(): canceled!!!";

    }
    else
    {
        //Start Loading model from via the SModeller
        if (m_model3dHandler->model().LoadFile(getAbsPath(path)))
        {
            m_model3dHandler->setModelDimension();
            m_model3dHandler->setModelDiscretization();
            m_model3dHandler->setSurfaceIndices();

            changeDimension();
     
            emit(modelLoadingStarted(path));
        }
    }
    
}

void VisualizationHandler::setBoundingBoxDimension(double size_x, double size_y, double size_z)
{
    m_boxDimensionX = size_x;
    m_boxDimensionY = size_y;
    m_boxDimensionZ = size_z;
}

void VisualizationHandler::changeDimension()
{
    double size_x;
    double size_y;
    double size_z;

    m_model3dHandler->getModelDimension(size_x, size_y, size_z);
    m_boxDimensionX = size_x;
    m_boxDimensionY = size_y;
    m_boxDimensionZ = size_z;
    
    m_vtkFboItem->changeDimension();
    setBoundingBoxDimension(m_boxDimensionX, m_boxDimensionY, m_boxDimensionZ);
    
}

bool VisualizationHandler::exportToIRAPgrid()
{
    ////qDebug() << "VisualizationHandler:: exporting.. ";

    //return  m_model3dHandler->exportToIrapGrid("test_irap");
    return  m_model3dHandler->exportToIrapGrid("Exported gridded surfaces");
}

void VisualizationHandler::addSurfacesFromSBIM() const
{
    m_vtkFboItem->removePreviousSurfaces();
}

void VisualizationHandler::clearViews()
{
    ////qDebug() << "VisualizationHandler:: reset all views.. ";

    //Re-initialize Model3dHandler has been commented out since we will let the user to reset set the dimension if necessary 
    //m_model3dHandler->model().SetSize(m_boxDimensionX, m_boxDimensionY, m_boxDimensionZ);
    //m_model3dHandler->setOrigin(0, 0, 0);

    //Reset CanvasHandler
    m_canvasHandler->reset();

    //Reset Object tree model
    //m_treeModel->reset();

    //Clear any items inside the bounding box 
    m_vtkFboItem->resetView();

    //Reset the Bounding box so that the user may choose to set a new dimension
    m_vtkFboItem->resetBoundingBox();

    //Reset object VizButton
    m_viz = false;

    emit(resetViz());
    emit(domainVisibilityListUpdated(domainList));
    emit(openDimensionDialog());
}

void VisualizationHandler::updateMainWindowSize(bool shrink, int position)
{
    emit(resizeMainWindow(shrink));
}

void VisualizationHandler::setCrossSection(const double crossSectionNumber)
{
    m_crossSectionNumber = crossSectionNumber;
    m_vtkFboItem->setCrossSection(m_crossSectionNumber);
    m_vtkFboItem->setPlaneOrientation(m_currentPlaneOrientationId);
    emit(crossSectionChanged(m_crossSectionNumber));
}

void VisualizationHandler::setPlaneOrientation(const int planeOrientation, int csID)
{
    m_currentPlaneOrientationId = planeOrientation;
    m_vtkFboItem->setCrossSection(csID);
    m_vtkFboItem->setPlaneOrientation(m_currentPlaneOrientationId);
    emit(planeOrientationChanged(m_currentPlaneOrientationId));
}

void VisualizationHandler::setSourceRegion(int sourceRegion, int sourceParentTypeId)
{
    ////qDebug() << "VisualizationHandler:: setSourceRegion: id = " << sourceRegion << ", parentTypeId: " << sourceParentTypeId;
    if (sourceParentTypeId == 3)
    {
        m_source_region = sourceRegion;
        m_source_region_parent_type_id = sourceParentTypeId;
    }
}

void VisualizationHandler::removeRegionFromDomainWithoutUpdatingView(int regionId, int domainId)
{
   qDebug() << "VisualizationHandler::removeRegionFromDomainWithoutUpdatingView: region_id = " << regionId << ", domainId=" << domainId;

    int index = 0;

    std::shared_ptr<Domain> domain = getDomainById(domainId);
    if (domain != nullptr)
    {
            std::vector <std::shared_ptr<Region>> regionPointerList = domain->getRegionPointerList();
            ////qDebug() << "VisualizationHandler::removeRegionFromDomain: regionPointerListSize=" << regionPointerList.size();
            try {
                if (regionPointerList.size() > 1)
                {
                    index = 0;
                    for (const std::shared_ptr<Region>& region : regionPointerList)
                    {
                        if (region->getId() == regionId)
                        {
                            regionPointerList.erase(regionPointerList.begin() + index);
                            domain->setRegionPointerList(regionPointerList);
                            domain->setVolume(0.0 - region->getVolume());

                            m_vtkFboItem->removeSingleRegionFromDomain(regionId, domainId);
                            m_model3dHandler->eraseRegionIdFromDomain(domain->getId(), regionId);

                            //qDebug() << "Removing region " << regionId<< " from domain ";

                            break;
                        }
                        index++;
                    }
                }
                else
                {
                    //If there is only one region in the domain
                    // Remove the domain
                    int counter = 0;
                    for (const std::shared_ptr<Domain>& eachDomain : domainList)
                    {
                        if (eachDomain->getId() == domainId)
                        {
                            domainList.erase(domainList.begin() + counter);

                            m_model3dHandler->eraseDomain(domainId);
                            break;
                        }
                        counter++;
                    }

                }

            }
            catch (int err) {
                ////qDebug() << "VisualizationHandler::removeRegionFromDomain - domain child";
            }
    }
        
}

void VisualizationHandler::setRegionForRemovalFromDomain(int sourceDomain, int sourceParentTypeId, int regionId)
{
    qDebug() << "VisualizationHandler:: setRegionForRemovalFromDomain: domainId = " << sourceDomain << ", region_id= " << regionId;
    if (sourceParentTypeId == 5)
    {
        m_source_region = regionId;
        m_source_region_parent_type_id = sourceParentTypeId; //ObjectCategories::REGION;
        m_source_domain = sourceDomain;
        //removeRegionFromDomainWithoutUpdatingView(regionId, sourceDomain);
    }
}

void VisualizationHandler::setDestinationDomain(int destinationDomain)
{
    ////qDebug() << "VisualizationHandler:: setDestinationDomain: id = " << destinationDomain;

    m_destination_domain = destinationDomain;

    if (m_source_region_parent_type_id == ObjectCategories::REGION_IN_DOMAIN)
    {
        //qDebug() << "VisualizationHandler:: setDestinationDomain: A";
        removeRegionFromDomainWithoutUpdatingView(m_source_region, m_source_domain);
    }

    emit(dropToDomainReady());
}

int VisualizationHandler::determineNewDomainId()
{
    int newDomainId = 0;
    for (const std::shared_ptr<Domain>& domain : domainList)
    {
        if (domain->getId() >= newDomainId)
            newDomainId = domain->getId() + 1;
     
    }

    return newDomainId;
}

void VisualizationHandler::createAndsetDestinationDomain()
{
    if (m_source_region_parent_type_id == 3)
    {
        m_source_region_parent_type_id = -1;
        m_destination_domain = determineNewDomainId();

        if ((m_source_region == -1) || (m_destination_domain == -1))
        {
            ////qDebug() << "VisualizationHandler:: dropToDomain Bingo 2";
            return;
        }
        
        std::shared_ptr<Domain> domain;
        QString domainColor = "";
        ////qDebug() << "VisualizationHandler:: dropToDomain Bingo 1: source_region id =" << m_source_region;
        if (getDomainById(m_destination_domain) == nullptr)
        {
            if (isItemNameUnique("domain" + QString::number(m_destination_domain), ObjectCategories::Name::DOMAIN_VOLUME))
            {
                QString parentDomainName = getRegionsParentDomainIfExists(m_source_region);
                if (parentDomainName == "")
                {
                    addToDomainList(m_destination_domain, "domain" + QString::number(m_destination_domain), "");
                    emit(domainCreatedForRegion(m_destination_domain, m_source_region));
                }
                else {
                    emit(duplicateRegionDropDetected(parentDomainName, getRegionsParentDomainIfExists(m_source_region)));
                }
            }
            else
            {
                emit(duplicateItemNameDetected("Domain"));
                return;
            }

            ////qDebug() << "VisualizationHandler:: dropToDomain Bingo 3";
        }
        
    }

    else emit(dropToDomainNotAllowed());
}

void VisualizationHandler::addRegionToTheCreatedDomain(int domain_id, int region_id)
{
    std::shared_ptr<Region> region = getRegionById(region_id);
    std::shared_ptr<Domain> domain = getDomainById(domain_id);

    QString domainColor = "";
    if (domainColor == "")
    {
        ////qDebug() << "VisualizationHandler:: dropToDomain Bingo 5";
        domainColor = m_domainColorMap.getCustomColorByIndex(m_destination_domain);
        ////qDebug() << "VisualizationHandler:: dropToDomain Bingo 6: " << domainColor;
    }

    if (region != nullptr)//if ((region != nullptr) &&(domain != nullptr))
    {
        ////qDebug() << "VisualizationHandler:: dropToDomain Bingo 6: region is there: " << region->getId();
        if (domain != nullptr)
        {
            ////qDebug() << "VisualizationHandler:: dropToDomain Bingo 7: domain: " << domain->getId();
            if (!doesTheRegionExistInsideAnyOtherDomain(m_source_region))
            {
                ////qDebug() << "VisualizationHandler:: dropToDomain Bingo 8: region was not added to any domain";
                domain->addToRegionPointerList(region);
                domain->setColor(domainColor);

                domain->setVolume(region->getVolume());

                m_commandProcessor->addToDomainList(domain->getId(), region->getId(), domain->getColor());

                m_model3dHandler->addRegionIdToDomain(domain->getId(), region->getId());
                
                //Added to update the 3D visualization as soon as a region gets added
                setItemVisibilityAll(ObjectCategories::Name::DOMAIN_VOLUME, m_domain_visibility);

                emit(domainListUpdated(domainList));
                emit(domainMapUpdated(prepareDomainMap()));
            }
            else
            {
                ////qDebug() << "VisualizationHandler:: dropToDomain Bingo 9";
                emit(duplicateRegionDropDetected(region->getName(), getRegionsParentDomainIfExists(region->getId())));
            }

        }
        //else //qDebug() << "VisualizationHandler:: dropToDomain Bingo 10";
    }
    else
    {
        ////qDebug() << "VisualizationHandler:: dropToDomain Bingo 11";
    }
}

void VisualizationHandler::dropToDomain()
{
    qDebug() << "VisualizationHandler:: dropToDomain -START" ;
    
    //m_destination_domain = destinationDomain;

    if (m_source_region_parent_type_id == -1)
    {
        ////qDebug() << "VisualizationHandler:: dropToDomain Bingo 1";
        emit(dropToDomainNotAllowed());
        return;
    }
    if ((m_source_region == -1) || (m_destination_domain == -1))
    {
        ////qDebug() << "VisualizationHandler:: dropToDomain Bingo 2";
        return;
    }

    
    std::shared_ptr<Region> region = getRegionById(m_source_region);
    //std::shared_ptr<Domain> domain = getDomainById(m_destination_domain);
    
    std::shared_ptr<Domain> domain;
    QString domainColor = "";
    //qDebug() << "VisualizationHandler:: dropToDomain Bingo 1";
    if (getDomainById(m_destination_domain) == nullptr)
    {
        //qDebug() << "VisualizationHandler:: dropToDomain Bingo 2";
        if (isItemNameUnique("domain" + QString::number(m_destination_domain), ObjectCategories::Name::DOMAIN_VOLUME))
        {
            //qDebug() << "VisualizationHandler:: dropToDomain Bingo 2.1";
            addToDomainList(m_destination_domain, "domain" + QString::number(m_destination_domain), "");
        }
        else
        {
            //qDebug() << "VisualizationHandler:: dropToDomain Bingo 2.2:  " << "domain" + QString::number(m_destination_domain);
            emit(duplicateItemNameDetected("Domain"));
            return;
        }

       //qDebug() << "VisualizationHandler:: dropToDomain Bingo 3";
    }
    domain = getDomainById(m_destination_domain);
    //qDebug() << "VisualizationHandler:: dropToDomain Bingo 4";

    if (domainColor == "")
    {
        //qDebug() << "VisualizationHandler:: dropToDomain Bingo 5";
        domainColor = domain->getColor();//m_domainColorMap.getCustomColorByIndex(m_destination_domain);
        //qDebug() << "VisualizationHandler:: dropToDomain Bingo 6";
    }
    if ((region != nullptr) && (domain != nullptr))
    {
        qDebug() << "VisualizationHandler:: dropToDomain -START - A";
        //qDebug() << "VisualizationHandler:: dropToDomain Bingo 7: domain: " << domain->getId();
        //Check if the region was added before
        //if (!doesTheRegionExistInsideTheDomain(m_source_region, domain))
        
        if (!doesTheRegionExistInsideAnyOtherDomain(m_source_region))
        {
            qDebug() << "VisualizationHandler:: dropToDomain -START - B";
            //qDebug() << "VisualizationHandler:: dropToDomain Bingo 8: region was not added to any domain";

            domain->addToRegionPointerList(region);
            //domain->setColor(m_domainColorMap.getCustomColorByIndex(domain->getId()));
            domain->setColor(domainColor);

            domain->setVolume(region->getVolume());

            m_commandProcessor->addToDomainList(domain->getId(), region->getId(), domain->getColor());
            
            m_model3dHandler->addRegionIdToDomain(domain->getId(), region->getId());

            //Added to update the 3D visualization as soon as a region gets added
            setItemVisibilityAll(ObjectCategories::Name::DOMAIN_VOLUME, m_domain_visibility);
            
            emit(domainListUpdated(domainList));
            emit(domainMapUpdated(prepareDomainMap()));
        }
        else
        {
            qDebug() << "VisualizationHandler:: dropToDomain -START - C";
            //qDebug() << "VisualizationHandler:: dropToDomain Bingo 9";
            emit(duplicateRegionDropDetected(region->getName(), getRegionsParentDomainIfExists(region->getId()) ) );
        }
        
    }
}

std::shared_ptr<Region> VisualizationHandler::getRegionById(int id)
{
    for (const std::shared_ptr<Region>& region : regionList)
    {
        if (region->getId() == id)
            return region;

    }
    return nullptr;
}

std::shared_ptr<Domain> VisualizationHandler::getDomainById(int id)
{
    for (const std::shared_ptr<Domain>& domain : domainList)
    {
        if (domain->getId() == id)
            return domain;

    }
    return nullptr;
}

bool VisualizationHandler::doesTheRegionExistInsideAnyOtherDomain(int region_id)
{
    for (const std::shared_ptr<Domain>& domain : domainList)
    {
        
        std::vector<std::shared_ptr<Region>> domainChildRegionList = domain->getRegionPointerList();
        for (const std::shared_ptr<Region>& region : domainChildRegionList)
        {
            if (region->getId() == region_id)
            {
                    ////qDebug() << "VisualizationHandler::doesTheRegionExistInsideAnyOtherDomain: region " << region_id << " exists in domain " << domain->getId();
                    return true;
            }

        }
        
    }
    
    return false;
}

QString VisualizationHandler::getRegionsParentDomainIfExists(int region_id)
{
    for (const std::shared_ptr<Domain>& domain : domainList)
    {
        //if (domain->getId() != domain_id)
        //{
        std::vector<std::shared_ptr<Region>> domainChildRegionList = domain->getRegionPointerList();
        for (const std::shared_ptr<Region>& region : domainChildRegionList)
        {
            if (region->getId() == region_id)
                return domain->getName();

        }
        //}
    }

    return "";
}

void VisualizationHandler::removeRegionPointerListFromDomains()
{
    for (std::shared_ptr<Domain>& domain: domainList)
    {
        domain->clearRegionPointerList();
        domain->setVolume(0.0 - domain->getVolume());
        domain->setColor("");
    }
    m_domain_region_ids.clear();
    emit(domainListUpdated(domainList));
}

void VisualizationHandler::setStratigraphicVisibility(bool visibility)
{
    m_stratigraphy_visibility = visibility;
}

void VisualizationHandler::setStructuralVisibility(bool visibility)
{
    m_structural_visibility = visibility;
}

void VisualizationHandler::setRegionVisibility(bool visibility)
{
    m_region_visibility = visibility;
}

void VisualizationHandler::setDomainVisibility(bool visibility)
{
    m_domain_visibility = visibility;
}

void VisualizationHandler::setPreviewFlag()
{
    m_preview_flag = true;

    m_rule_operator_id = m_model3dHandler->getRemoveRuleId();
    m_model3dHandler->model().TestSurfaceInsertion();

    emit(previewTurnedOn());
}

void VisualizationHandler::resetPreviewFlag()
{
    m_preview_flag = false;

    m_model3dHandler->model().StopTestSurfaceInsertion();
    m_model3dHandler->setRemoveRuleId(m_rule_operator_id);

    emit(previewTurnedOff());
}

bool VisualizationHandler::getPreviewFlag()
{
    return m_preview_flag;
}

void VisualizationHandler::initiateSurfacePreview()
{
    ////qDebug() << "Going to show 3d preview";
    m_vtkFboItem->addSurfacePreview();
    //m_vtkFboItem->removeSurfacePreview();
}

void VisualizationHandler::removeSurfacePreview()
{
    m_vtkFboItem->removeSurfacePreview();
}

void VisualizationHandler::clearSurfacePreview()
{
    m_vtkFboItem->clearSurfacePreview();
}

void VisualizationHandler::addSurfacePreview()
{
    m_vtkFboItem->addSurfacePreview();
}

void VisualizationHandler::setCurrentRuleOperator(int id)
{
    m_rule_operator_id = id;
}

void VisualizationHandler::checkPreviewButtonStatus()
{
    if (m_preview_button_status)
        setPreviewFlag();
    else resetPreviewFlag();
}

void VisualizationHandler::togglePreviewButtonStaus()
{
    if (m_preview_button_status)
        m_preview_button_status = false;
    else m_preview_button_status = true;
}

void VisualizationHandler::updateCrossSection()
{
    //////qDebug() << "VisualizationHandler::updateCrossSection: ";
    m_vtkFboItem->updateCrossSection();
    
}

/* Example Method for Sicilia*/
void VisualizationHandler::saveSbimData(QUrl fileUrl)
{
    //////qDebug() << "VisualizationHandler::saveSbimData: " << getAbsPath(fileUrl).c_str();

    QString absPath = QString::fromStdString(getAbsPath(fileUrl));
    SbimFileHandler sbimFileHandler(absPath);
    sbimFileHandler.save();

}

/* Example Method for Sicilia*/
void VisualizationHandler::loadSbimData(QUrl fileUrl)
{
    ////qDebug() << "VisualizationHandler::loadSbimData: " << getAbsPath(fileUrl).c_str();
    QString absPath = QString::fromStdString(getAbsPath(fileUrl));
    SbimFileHandler sbimFileHandler(absPath);
    sbimFileHandler.load();
}

void VisualizationHandler::fillDomainList()
{
    m_model3dHandler->enforceDomainConsistency();
    std::vector<int> region_list_of_a_domain;

    updateDomainListFromModel();

    //if (m_domain_visibilityMap.size() > 0)
      //  m_domain_visibilityMap.clear();

    if (domainList.size() > 0)
    {
        for (const std::shared_ptr<Domain>& domain : domainList)
        {
            region_list_of_a_domain = m_model3dHandler->getRegionIdsByDomain(domain->getId());
            //qDebug() << "================TEST domain id=" << domain->getId() <<": region list: ==================";
            //printVector(region_list_of_a_domain);
            
            m_domain_visibilityMap.push_back(true);

            double volume = 0.0;
            for (int r_id : region_list_of_a_domain)
            {
                //////qDebug() << "TEST domain " << domain->getId() << " - region id: " << r_id;

                std::shared_ptr<Region> region = getRegionById(r_id);
                domain->addToRegionPointerList(region);
                
                m_commandProcessor->addToDomainList(domain->getId(), region->getId(), domain->getColor());

                volume += region->getVolume();
                
            }
            domain->setVolume(volume);
            region_list_of_a_domain.clear();
        }
        emit(domainListUpdated(domainList));
        emit(domainVisibilityListUpdated(domainList));
        emit(domainMapUpdated(prepareDomainMap()));
    }

}

void VisualizationHandler::updateDomainListFromModel()
{
    if (domainList.size() > 0)
    {
        domainList.clear();
        m_commandProcessor->clearDomainList();
    }
    
    std::vector<int> list_of_domain = m_model3dHandler->getDomainList();

    QString name;
    QString description;
    QString color;

    for (int i: list_of_domain)
    {
        m_model3dHandler->getDomainMetadataById(i, name, description, color);
        std::shared_ptr<Domain> newDomain = std::make_shared<Domain>(i, name, description);
        newDomain->setVisibility(false);
        newDomain->setColor(color);

        domainList.push_back(newDomain);
    }
}


void VisualizationHandler::printVector(std::vector<int> const& input)
{
    for (int i = 0; i < input.size(); i++) {
        ////qDebug() << input.at(i) << ' ';
    }
}

void VisualizationHandler::printVector(std::vector<double> const& input)
{
    for (int i = 0; i < input.size(); i++) {
        qDebug() << input.at(i) << ' ';
    }
}

void VisualizationHandler::adjustRegionColorIfExistInDomainList(int region_id, QString newColor)
{
    //////qDebug() << "VisualizationHandler::adjustRegionColorIfExistInDomainList: region_id=" << region_id;

    for (const std::shared_ptr<Domain>& domain : domainList)
    {
        std::vector<std::shared_ptr<Region>> domainChildRegionList = domain->getRegionPointerList();
        for (const std::shared_ptr<Region>& region : domainChildRegionList)
        {
            if (region->getId() == region_id)
            {
                region->setColor(newColor);
                emit(domainListUpdated(domainList));
                return;
            }


        }

    }
    
}

void VisualizationHandler::removeCrossSectionImage(int directionId, int crossSectionId)
{
    ////qDebug() << "VisualizationHandler::removeCrossSectionImage: " << directionId << "," << crossSectionId ;
    emit(crossSectionImageRemoved(directionId, crossSectionId));
}

void VisualizationHandler::loadCrossSectionImage(int directionId, int crossSectionId, QString imagePath)
{
    ////qDebug() << "VisualizationHandler::loadCrossSectionImage: " << directionId << "," << crossSectionId << "," << imagePath;
    emit(crossSectionImageLoaded(directionId, crossSectionId, imagePath));

}

void VisualizationHandler::enableImageRemoveButton(int direction)
{
    ////qDebug() << "VisualizationHandler::enableImageRemoveButton: " ;
    emit(imageRemoveButtonEnabled(direction));
}

void VisualizationHandler::disableImageRemoveButton(int direction)
{
    ////qDebug() << "VisualizationHandler::disableImageRemoveButton: ";
    emit(imageRemoveButtonDisabled(direction));
}

void VisualizationHandler::completeImageRemoval(int directionId, int crossSectionId)
{
    ////qDebug() << "VisualizationHandler::completeImageRemoval: ";
    
    emit(imageRemovalCompleted());
}

///////////////////////////
void VisualizationHandler::setSourceSurface(int parentId, int surfaceId, int surfaceParentTypeId)
{
    m_source_surface_id = surfaceId;
    m_source_surface_parent_type_id = surfaceParentTypeId;
    m_parent_id_of_source_surface = parentId;
}

void VisualizationHandler::setDestinationSurface(int surfaceId, int surfaceParentTypeId)
{
    m_desination_surface_id = surfaceId;
    m_destination_surface_parent_type_id = surfaceParentTypeId;

    //Check if source surface is a group
    bool is_source_a_group = false;
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        if (surface->getId() == m_source_surface_id)
        {
            std::vector <std::shared_ptr<SurfaceItem>> childList = surface->getSurfaceChildList();
            if (childList.size() > 0)
                is_source_a_group = true;
            break;
        }
    }

    if (is_source_a_group != true)
    {

        if ((m_source_surface_parent_type_id == ObjectCategories::Name::STRATIGRAPHY) && (m_destination_surface_parent_type_id == ObjectCategories::Name::STRATIGRAPHY))

        {
            surfaceMergeAction();
            emit(surfaceMerged(surfaceList));
            
        }

        else if ((m_source_surface_parent_type_id == ObjectCategories::Name::STRATIGRAPHIC_CHILD) && (m_destination_surface_parent_type_id == ObjectCategories::Name::STRATIGRAPHY))
        {
            ////qDebug() << "VisualizationHandler::setDestinationSurface: Dropping from child to a new destination";

            if (m_source_surface_id != m_desination_surface_id)
            {
                //Find the current parent of the source
                std::shared_ptr<SurfaceItem> parent_of_source_surface = getSurfaceById(m_parent_id_of_source_surface);
                if (parent_of_source_surface != NULL)
                {
                    // Remove source from its current parent
                    std::vector<std::shared_ptr<SurfaceItem>> childList_of_the_parent = parent_of_source_surface->getSurfaceChildList();

                    int index = 0;
                    for (const std::shared_ptr<SurfaceItem> eachChild : childList_of_the_parent)
                    {
                        if ((eachChild->getId() == m_source_surface_id) && (m_source_surface_id != m_parent_id_of_source_surface))
                            //if (eachChild->getId() == m_source_surface_id)
                        {
                            //Add Child surface to the main surface list
                            eachChild->setParentId(ObjectCategories::Name::STRATIGRAPHY);
                            addSurfaceIntoSurfaceList(eachChild);
                            //Remove child surface from the child list
                            childList_of_the_parent.erase(childList_of_the_parent.begin() + index);
                            //Set the revised child list to the parent surface
                            parent_of_source_surface->setChildList(childList_of_the_parent);
                            //Set the group id of the child surface to -1
                            m_model3dHandler->setSurfaceGroup(eachChild->getId(), -1);
                            surfaceMergeAction();
                            break;
                        }
                        index++;
                    }
                    //Merge Action for source to destination
                    //surfaceMergeAction();
                    emit(surfaceMerged(surfaceList));
                }

            }
        }
    }
}

std::shared_ptr<SurfaceItem> VisualizationHandler::getSurfaceById(int id)
{
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        if (surface->getId() == id)
            return surface;

    }
    return nullptr;
}


void VisualizationHandler::removeSurfaceFromSurfaceList(int surfaceId)
{
    int index = 0;
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        if (surface->getId() == surfaceId)
        {
            surfaceList.erase(surfaceList.begin() + index);

            break;
        }
        index++;
    }
}

void VisualizationHandler::unmergeSurfaceFromGroup(int parentTypeId, int surfaceId, int parentId)
{
    ////qDebug() << "VisualizationHandler::unmergeSurfaceFromGroup: " << surfaceId << ",parent-id:" << parentId;

    if (parentTypeId == ObjectCategories::Name::STRATIGRAPHIC_CHILD)
    {
        int group_id = m_model3dHandler->getSurfaceGroup(surfaceId);
        ////qDebug() << "VisualizationHandler::unmergeSurfaceFromGroup: Going to unmerge - " << surfaceId << ",parent-id:" << parentId <<", group-id:" << group_id;
        if (group_id == surfaceId)
            return;
        //Find the parent Object in the surface list
        std::shared_ptr<SurfaceItem> parent_surface = getSurfaceById(parentId);
        //Retrieve the child list
        std::vector<std::shared_ptr<SurfaceItem> > childList = parent_surface->getSurfaceChildList();
        
        //Check the ChildList size of the parent
        if (childList.size() <= 2)
        {
            ////qDebug() << "VisualizationHandler::unmergeSurfaceFromGroup: Going to unmerge - child list size <=2";
            //Do the following for both surfaces
            for (const std::shared_ptr<SurfaceItem> eachChild : childList)
            {
                //Copy surface from the child list
                std::shared_ptr <SurfaceItem> childSurfaceCopy = eachChild;
                
                //Add surface to the surface List according its id
                eachChild->setParentId(ObjectCategories::Name::STRATIGRAPHY);
                //Set the default group_id to -1
                eachChild->setGroupId(-1);

                addSurfaceIntoSurfaceList(eachChild);

                // Setting child surfaces group-id to default value i.e. -1
                m_model3dHandler->setSurfaceGroup(eachChild->getId(), -1);
                
            }
            //Empty the childlist of the parent_surface
            parent_surface->clearChildList();
            //Remove parent_surface from the surfaceList
            removeSurfaceFromSurfaceList(parent_surface->getId());
            //Setting group id to default i.e. -1
            m_model3dHandler->setSurfaceGroup(parent_surface->getId(), -1);
            //add parent_surface to the surfaceList
            addSurfaceIntoSurfaceList(parent_surface);
        }
        else
        {
            ////qDebug() << "VisualizationHandler::unmergeSurfaceFromGroup: Going to unmerge - child list size > 2";
            
            //Find the surface in the child list and erase 
            int index = 0;
            for (const std::shared_ptr<SurfaceItem> eachChild : childList)
            {
                //if(eachChild->getId() == surfaceId)
                if ( (eachChild->getId() == surfaceId) && (eachChild->getGroupId() != surfaceId))
                {
                    // Setting child surfaces group-id to default value i.e. -1
                    m_model3dHandler->setSurfaceGroup(eachChild->getId(), -1);

                    //Set the default group_id to -1
                    eachChild->setGroupId(-1);

                    //Add Child surface to the main surface list
                    addSurfaceIntoSurfaceList(eachChild);
                    //Remove child surface from the child list
                    childList.erase(childList.begin() + index);
                    //Set the revised child list to the parent surface
                    parent_surface->setChildList(childList);
                    break;
                }
                index++;
            }
            
        }
        emit(surfaceMerged(surfaceList));
    }
}

void VisualizationHandler::addSurfaceIntoSurfaceList(std::shared_ptr<SurfaceItem> surfaceObj)
{
    bool flag = false;
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        if ((surface->getId() ) == surfaceObj->getId())
        {
            flag = true;
            break;
        }
    
    }

    if(!flag) 
        surfaceList.push_back(surfaceObj);
    
}

void VisualizationHandler::saveChildSurfaceData(int parentId, int parentTypeId, int itemDataId, QString itemNameText, QString itemDescription)
{
    std::shared_ptr<SurfaceItem> childSurface = findChildSurface(itemDataId, parentId);

    if (childSurface != NULL)
    {
        if (childSurface->getDescription() != itemDescription)
            childSurface->setDescription(itemDescription);
        if (childSurface->getName() != itemNameText)
        {
            if (isItemNameUnique(itemNameText, parentTypeId))
            {
                childSurface->reName(itemNameText);
                emit(surfaceListUpdated(surfaceList));
            }
            else emit(duplicateItemNameDetected("Surface"));
        }
    }

}

void VisualizationHandler::surfaceMergeAction()
{
   // //qDebug() << "VisualizationHandler::surfaceMergeAction";

    ////qDebug() << "VisualizationHandler::setDestinationSurface: 1";
    //Find the destination surface in the surfaceList
    std::shared_ptr<SurfaceItem> source_surface = getSurfaceById(m_source_surface_id);
    std::shared_ptr<SurfaceItem> destination_surface = getSurfaceById(m_desination_surface_id);

    ////qDebug() << "VisualizationHandler::setDestinationSurface: 2";

    std::vector<std::shared_ptr<SurfaceItem>> destination_childList = destination_surface->getSurfaceChildList();

    ////qDebug() << "VisualizationHandler::setDestinationSurface: 3";

    if (destination_childList.size() == 0) // When the destination surface's child list size =0
    {
        ////qDebug() << "VisualizationHandler::setDestinationSurface: 4";
        //Make a copy of the destination by setting its parent-type id to TreeNodeParentType::Value::STRATIGRAPHIC_CHILD
        std::shared_ptr<SurfaceItem> destination_copy = std::make_shared<SurfaceItem>(*destination_surface); //Using copy constructor
        //std::shared_ptr<SurfaceItem> destination_copy = destination_surface; //Assignment operator

        ////qDebug() << "VisualizationHandler::setDestinationSurface: 5";
        //Add this copy of the destination as child of the original destination node
        destination_copy->setGroupId(destination_surface->getId());
        destination_surface->addToChildList(destination_copy);
        
        //Save Group-id of the destination surface in the model
        m_model3dHandler->setSurfaceGroup(destination_surface->getId(), destination_surface->getId());
        
        ////qDebug() << "VisualizationHandler::setDestinationSurface: 6";

        //Change the parent type id of the child as TreeNodeParentType::Value::STRATIGRAPHIC_CHILD
        source_surface->setParentId(ObjectCategories::Name::STRATIGRAPHIC_CHILD);
        //Set the group id of the source
        source_surface->setGroupId(destination_surface->getId());
        //Add the source as a child to the destination
        destination_surface->addToChildList(source_surface);
        //Remove source from its original parent
        removeSurfaceFromSurfaceList(source_surface->getId());
        ////qDebug() << "VisualizationHandler::setDestinationSurface: 7";

        //Set source_surface color as of the destination color
        //source_surface->setColor(destination_surface->getColor());

    }
    else if (destination_childList.size() > 0) //When the destination surface's child list size > 0
    {
        ////qDebug() << "VisualizationHandler::setDestinationSurface: 8";
        //Change the parent type id of the child as TreeNodeParentType::Value::STRATIGRAPHIC_CHILD
        //Add the source as a child to the destination
        destination_surface->addToChildList(source_surface);
        //Remove source from its original parent
        removeSurfaceFromSurfaceList(source_surface->getId());
        ////qDebug() << "VisualizationHandler::setDestinationSurface: 9";

        //Set source_surface color as of the destination color
        //source_surface->setColor(destination_surface->getColor());
    }
    //Save Group-id of the source surface in the model
    m_model3dHandler->setSurfaceGroup(source_surface->getId(), destination_surface->getId());
    m_model3dHandler->setGroupMetadataById(destination_surface->getId(), destination_surface->getName(), destination_surface->getDescription(), destination_surface->getColor());
    
    emit(surfaceMapUpdated(getSurfaceColorMap(), getSurfaceVisibilityMap()));
}

void VisualizationHandler::changeChildrenSurfaceColorAndVisibility(int surfaceId, int type_id, QString parentColor, bool parentVisibility)
{
    int i = 0;
    std::vector<QString> childColorList;
    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        if (surface->getId() == surfaceId)
        {
            std::vector < std::shared_ptr<SurfaceItem> > childList = surface->getSurfaceChildList();
            for (const std::shared_ptr<SurfaceItem>& child : childList)
            {
                childColorList.push_back(parentColor);
            }
            surface->setColor(parentColor);

            emit(surfaceColorUpdated(surface->getId(), type_id, parentColor));

            break;
        }
        i++;
    }
    //emit(surfaceColorListUpdated(surfaceList));

    //emit(surfaceMapUpdated(getSurfaceColorList(), m_visibilityMap));
    emit(surfaceMapUpdated(getSurfaceColorMap(), getSurfaceVisibilityMap()));
}
/*std::shared_ptr<SurfaceItem> VisualizationHandler::createSurfaceItem(int category, int surfaceId, std::vector<QString> surfaceColorList, std::vector<bool> surfaceVisibilityList)
{
    QString name = "";
    QString desc = "";
    QString color = "";
    int surface_category = category + 1;
    //Start
    int model_surface_id = surfaceId + 1;
    m_model3dHandler->getSurfaceData(model_surface_id, name, desc, color);
    //qDebug() << "VisualizationHandler::createSurfaceItem: Metadata: id=" << surfaceId << ", name=" << name << ", type=" << desc <<", category=" << category << ", color=" << color;
    
    if (name == "")
    {
        if (surface_category == 1)
            name = "Horizon" + QString::number(surfaceId + 1);
        if (surface_category == 2)
            name = "Surface" + QString::number(surfaceId + 1);
    }
    if (color == "")
    {
        if (m_surfaceColorQmap.count() > 0)
        {
            if ((surfaceId + 1) >= m_surfaceColorQmap.count())
                color = m_surfaceColorMap.getCustomColorByIndex(surfaceId + 1);
            else color = m_surfaceColorQmap[surfaceId + 1];

        }
        else color = m_surfaceColorMap.getCustomColorByIndex(surfaceId + 1);
    }

    bool visibility = true;
    if (m_surfaceVisibilityQmap.count() > 0)
    {
        if ((surfaceId + 1) <= m_surfaceColorQmap.count())
            visibility = m_surfaceVisibilityQmap[surfaceId + 1];
    }
    std::shared_ptr<SurfaceItem> surface = std::make_shared<SurfaceItem>(surfaceId + 1, name, desc, color, surface_category, visibility);
    if (category == 0)
        desc = "stratigraphy";
    else if (category == 1)
        desc = "structure";
    m_model3dHandler->setSurfaceData(model_surface_id, name, desc, color);

    if ((m_flat_surface_id !=-1) && (surface->getId() == m_flat_surface_id))
        surface->flatten(true);
    else surface->flatten(false);

    return surface;
}
*/

std::shared_ptr<SurfaceItem> VisualizationHandler::createSurfaceItem(int category, int surfaceId, std::vector<QString> surfaceColorList, std::vector<bool> surfaceVisibilityList)
{
    QString name = "";
    QString desc = "";
    QString color = "";

    int surface_category = category;//int surface_category = category + 1;
    
    //Start
    int model_surface_id = surfaceId;//int model_surface_id = surfaceId + 1;
    m_model3dHandler->getSurfaceData(model_surface_id, name, desc, color);
    //qDebug() << "VisualizationHandler::createSurfaceItem: Metadata: surface_id=" << surfaceId << ", name=" << name << ", type=" << desc << ", category=" << category << ", color=" << color;

    if (name == "")
    {
        if (surface_category == 1)
            name = "Horizon" + QString::number(surfaceId);//name = "Horizon" + QString::number(surfaceId + 1);
        if (surface_category == 2)
            name = "Surface" + QString::number(surfaceId);//name = "Surface" + QString::number(surfaceId + 1);

    }
    if (color == "")
    {
        /*if (m_surfaceColorQmap.count() > 0)
        {
            if ((surfaceId + 1) >= m_surfaceColorQmap.count())
                color = m_surfaceColorMap.getCustomColorByIndex(surfaceId + 1);
            else color = m_surfaceColorQmap[surfaceId + 1];

        }
        else color = m_surfaceColorMap.getCustomColorByIndex(surfaceId + 1);
        */
        if (m_surfaceColorQmap.count() > 0)
        {
            if (surfaceId >= m_surfaceColorQmap.count())
                color = m_surfaceColorMap.getCustomColorByIndex(surfaceId);
            else color = m_surfaceColorQmap[surfaceId];

        }
        else color = m_surfaceColorMap.getCustomColorByIndex(surfaceId);
    }

    bool visibility = true;
    /*if (m_surfaceVisibilityQmap.count() > 0)
    {
        if ((surfaceId + 1) <= m_surfaceColorQmap.count())
            visibility = m_surfaceVisibilityQmap[surfaceId + 1];
    }*/
    if (m_surfaceVisibilityQmap.count() > 0)
    {
        if (surfaceId  <= m_surfaceColorQmap.count())
            visibility = m_surfaceVisibilityQmap[surfaceId];
    }
    std::shared_ptr<SurfaceItem> surface = std::make_shared<SurfaceItem>(surfaceId, name, desc, color, surface_category, visibility);
    if (category == 1)
        desc = "stratigraphy";
    else if (category == 2)
        desc = "structure";
    m_model3dHandler->setSurfaceData(model_surface_id, name, desc, color);

    if ((m_flat_surface_id != -1) && (surface->getId() == m_flat_surface_id))
        surface->flatten(true);
    else surface->flatten(false);

    return surface;
}


std::vector<QString> VisualizationHandler::getSurfaceColorMap()
{
    QMap<int, QString> surfaceColorMap;
    surfaceColorMap = getSurfaceColorQmap();
    /*for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        surfaceColorMap.insert(surface->getId(), surface->getColor() );

        std::vector<std::shared_ptr<SurfaceItem>> childList = surface->getSurfaceChildList();
        if (childList.size() > 0)
        {
            for (const std::shared_ptr<SurfaceItem>& child : childList)
            {
                surfaceColorMap.insert(child->getId(), child->getColor() );
            }
        }

    }*/

    std::vector<QString> colormap{};
    QMapIterator<int, QString> it(surfaceColorMap);
    while (it.hasNext()) {
        it.next();
        //vmap.insert(QString::number(it.key()), it.value());
        colormap.push_back(it.value());
    }

    return colormap;
}

std::vector<bool> VisualizationHandler::getSurfaceVisibilityMap()
{
    QMap<int, bool>  surfaceVisibilityMap;
    surfaceVisibilityMap = getSurfaceVisibilityQmap();

    /*for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        surfaceVisibilityMap.insert(surface->getId(), surface->getVisibility() );

        std::vector<std::shared_ptr<SurfaceItem>> childList = surface->getSurfaceChildList();
        if (childList.size() > 0)
        {
            for (const std::shared_ptr<SurfaceItem>& child : childList)
            {
                surfaceVisibilityMap.insert(child->getId(), child->getVisibility() );
            }
        }

    }*/
    
    std::vector<bool> visibilityMap{};
    QMapIterator<int, bool> it(surfaceVisibilityMap);
    while (it.hasNext()) {
        it.next();
        //vmap.insert(QString::number(it.key()), it.value());
        visibilityMap.push_back(it.value());
    }
    return visibilityMap;
}

QMap<int, QString> VisualizationHandler::getSurfaceColorQmap()
{
    m_surfaceColorQmap.clear();

    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        m_surfaceColorQmap.insert(surface->getId(), surface->getColor());

        std::vector<std::shared_ptr<SurfaceItem>> childList = surface->getSurfaceChildList();
        if (childList.size() > 0)
        {
            for (const std::shared_ptr<SurfaceItem>& child : childList)
            {
                m_surfaceColorQmap.insert(child->getId(), child->getColor());
            }
        }

    }

    return m_surfaceColorQmap;
}

QMap<int, bool> VisualizationHandler::getSurfaceVisibilityQmap()
{
    m_surfaceVisibilityQmap.clear();

    for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
    {
        m_surfaceVisibilityQmap.insert(surface->getId(), surface->getVisibility());

        std::vector<std::shared_ptr<SurfaceItem>> childList = surface->getSurfaceChildList();
        if (childList.size() > 0)
        {
            for (const std::shared_ptr<SurfaceItem>& child : childList)
            {
                m_surfaceVisibilityQmap.insert(child->getId(), child->getVisibility());
            }
        }

    }
    return m_surfaceVisibilityQmap;
}

void VisualizationHandler::changeChildSurfaceVisibility(const std::shared_ptr<SurfaceItem>& parent_surface, int typeId, bool visiblilty)
{
    std::vector<int> idList{};
    std::vector<std::shared_ptr<SurfaceItem> > childList = parent_surface->getSurfaceChildList();
    for (const std::shared_ptr<SurfaceItem>& child : childList)
    {
        ////qDebug() << "VisualizationHandler::changeChildSurfaceVisibility: child Id=" << child->getId();
        child->setVisibility(visiblilty);
        idList.push_back(child->getId());
    }
    
   // //qDebug() << "VisualizationHandler::changeChildSurfaceVisibility: Id list=" << idList << " of paren_surface: " << parent_surface->getId();

    if (idList.size() > 0)
    {
        m_vtkFboItem->changeSurfaceVisibilityList(idList, parent_surface->getCategory(), visiblilty);
        //emit(surfaceMapUpdated(getSurfaceColorMap(), getSurfaceVisibilityMap()));
    }
}

void VisualizationHandler::saveImageDb(QUrl fileUrl, QString image_data_json)
{
    ////qDebug() << "VisualizationHandler::saveImageDb:" << fileUrl;

    //- Prepare the file name with .images.JSON extension
    QString absPath = QString::fromStdString(getAbsPath(fileUrl));
    ////qDebug() << "VisualizationHandler::saveImageDb: " << absPath;
    QString croped_fileName = absPath.section(".", 0, -1) + ".images.json";
   // //qDebug() << "VisualizationHandler::saveImageDb: " << croped_fileName << ", image data: " << image_data_json;

    ImageFileHandler imgFileHandler(croped_fileName);
    imgFileHandler.save(image_data_json);
}

void VisualizationHandler::loadImageDb(QUrl fileUrl)
{
    ////qDebug() << "VisualizationHandler::loadImageDb:" << fileUrl;
    //- Prepare the file name with .JSON extension
    QString absPath = QString::fromStdString(getAbsPath(fileUrl));
    QString croped_fileName = absPath.section(".", 0, -1) + ".images.json";
    
    ImageFileHandler imgFileHandler(croped_fileName);
    if(imgFileHandler.load())
        emit(imageDataLoaded(imgFileHandler.getJson()));
}

bool VisualizationHandler::isFlattening()
{
    return m_flat_mode;
}

void VisualizationHandler::startFlattening(int surface_id, double flat_height)
{
    m_flat_surface_id = surface_id;
    m_flat_height = flat_height;

    emit(flatHeightAdjusted(m_flat_mode, m_flat_height));
}

void VisualizationHandler::stopFlattening()
{
    m_flat_surface_id = -1;
    m_flat_height = 0.0;

    emit(flatHeightAdjusted(false, m_flat_height));
}

void VisualizationHandler::setFlattenedSurfaceId(int id) {
    m_flat_surface_id = id;

   // //qDebug() << "VisualizationHandler::setFlattenedSurfaceId:" << id;
    emit(surfaceToFlatSelected(m_flat_surface_id)); // Signal to the CanvasHandler
}

int VisualizationHandler::getSurfaceCount()
{
    return surfaceList.size();
}

void VisualizationHandler::process3DMesh(bool previewFlag) {
   // //qDebug() << "VisualizationHandler::process3DMesh()" ;

    if (previewFlag)
    {
        //removeSurfacePreview();
        QFuture<void> future = QtConcurrent::run(m_vtkFboItem, &VtkFboItem::removeSurfacePreview);
        QFutureWatcher<void> watcher;
        watcher.setFuture(future);

        watcher.waitForFinished();

        if (watcher.isCanceled())
        {
            ////qDebug() << "VisualizationHandler::process3DMesh(): canceled!!!";

        }
        else
        {
            ////qDebug() << "VisualizationHandler::process3DMesh(): finished!!!";
            emit(meshProcessingCompleted());
        }

    }
    else {
        //loadMeshes();
        QFuture<void> future2 = QtConcurrent::run(m_vtkFboItem, &VtkFboItem::removePreviousSurfaces);
        QFutureWatcher<void> watcher2;
        watcher2.setFuture(future2);

        watcher2.waitForFinished();

        if (watcher2.isCanceled())
        {
            qDebug() << "VisualizationHandler::process3DMesh() watcher2: canceled!!!";

        }
        else
        {
            //qDebug() << "VisualizationHandler::process3DMesh() watcher2: finished!!!";
            emit(meshProcessingCompleted());
        }
        
    }
}

void VisualizationHandler::prepareToLoadModel()
{
    ////qDebug() << "VisualizationHandler::prepareToLoadModel() ";

    //Reset VisualizationHandler
    this->reset();

    //Reset Model3dHandler
    m_model3dHandler->reset();

    //Clear any items inside the bounding box 
    m_vtkFboItem->resetView();

    //Reset the Bounding box so that the user may choose to set a new dimension
    m_vtkFboItem->resetBoundingBox();

    //Reset object VizButton
    m_viz = false;
       
    emit(resetViz());

    emit(domainVisibilityListUpdated(domainList));
}