#ifndef CANVASHANDLER_H
#define CANVASHANDLER_H

#include <QtQml>
#include <QObject>
#include <QString>
#include <QPolygonF>

#include <memory>
#include <vector>

#include <sbim/sketch_map_view.h>
#include <sbim/sketch_handler.h>
#include <sbim/sketch_template.h>

#include "Constants.h"
#include "Model3dHandler.h"

/**
* It is an interface between the QML and C++.
* The input data will be handled here. 
* All the coordinate system mappings will be handled here.
* The visualization update will receive data from here.
*/

#define NS_REVERSED true

class CanvasHandler : public QObject
{
	Q_OBJECT

	Q_PROPERTY(int canvasAction MEMBER m_canvasAction NOTIFY canvasActionChanged)

	Q_PROPERTY(int crossSection READ crossSection WRITE setCrossSection NOTIFY crossSectionChanged)
	Q_PROPERTY(int direction MEMBER m_direction NOTIFY directionChanged)
	Q_PROPERTY(bool isLength MEMBER m_isLength NOTIFY isLengthChanged)
	Q_PROPERTY(bool isWidth MEMBER m_isWidth NOTIFY isWidthChanged)
	Q_PROPERTY(bool isHeight MEMBER m_isHeight NOTIFY isHeightChanged)

	Q_PROPERTY(double csWE_Scale MEMBER m_csWE_Scale NOTIFY csWE_ScaleChanged)
	Q_PROPERTY(double csNS_Scale MEMBER m_csNS_Scale NOTIFY csNS_ScaleChanged)
	Q_PROPERTY(double csMap_Scale MEMBER m_csMap_Scale NOTIFY csMap_ScaleChanged)

	Q_PROPERTY(double csWE_Origin_x MEMBER m_csWE_Origin_x NOTIFY csWE_Origin_xChanged)
	Q_PROPERTY(double csWE_Origin_y MEMBER m_csWE_Origin_y NOTIFY csWE_Origin_yChanged)
	Q_PROPERTY(double csNS_Origin_x MEMBER m_csNS_Origin_x NOTIFY csNS_Origin_xChanged)
	Q_PROPERTY(double csNS_Origin_y MEMBER m_csNS_Origin_y NOTIFY csNS_Origin_yChanged)
	Q_PROPERTY(double csMap_Origin_x MEMBER m_csMap_Origin_x NOTIFY csMap_Origin_xChanged)
	Q_PROPERTY(double csMap_Origin_y MEMBER m_csMap_Origin_y NOTIFY csMap_Origin_yChanged)

	Q_PROPERTY(bool isSketchRegion MEMBER m_isSketchRegion NOTIFY isSketchRegionChanged)

	Q_PROPERTY(bool isPreview MEMBER m_isPreview NOTIFY isPreviewFlagChanged)
	Q_PROPERTY(bool isPathGuided MEMBER m_isPathGuided NOTIFY isPathGuidedChanged)
	Q_PROPERTY(bool isSubmitAllowed MEMBER m_isSubmitAllowed NOTIFY isSubmitAllowedChanged)
	Q_PROPERTY(bool pgeIsActive MEMBER m_pgeIsActive NOTIFY pgeIsActiveChanged)

	Q_PROPERTY(int selectIndex MEMBER m_selectIndex NOTIFY selectIndexChanged)

	Q_PROPERTY(int highlight_FV MEMBER m_highlight_FV NOTIFY highlightFVChanged)
	Q_PROPERTY(int highlight_MV MEMBER m_highlight_MV NOTIFY highlightMVChanged)
	Q_PROPERTY(int highlight_TR MEMBER m_highlight_TR NOTIFY highlightTRChanged)

	Q_PROPERTY(double veFactor MEMBER m_VE_factor NOTIFY veFactorChanged)
	Q_PROPERTY(double veAngle MEMBER m_VE_angle NOTIFY veAngleChanged)
	Q_PROPERTY(bool veAngleVisible MEMBER m_VE_angle_visible NOTIFY veAngleVisibleChanged)
	Q_PROPERTY(bool veBkgImage MEMBER m_VE_bkg_image NOTIFY veBkgImageChanged)
	Q_PROPERTY(double flatHeight MEMBER m_Flat_Height NOTIFY flatHeightChanged)

public:

	/**
	* Constructor.
	*/
	explicit CanvasHandler(QObject* parent = 0);

	
	/**
	* Method that reset CanvasHandler properties to its initial state.
	* @return void
	*/
	void reset();


	//--- FAZILA'S PUBLIC METHODS -----------------------------------------------	
	/**
	* Method to load surfaces ID from a loaded model inside the data structure
	* return void
	*/
	void setSurfacesID();


	/**
	* Method responsible to submit the sketch to the 3D Model to create the 3D surface.
	* It connects with SketchHandler to receive data of the sketches to be submitted.
	* It connects with the Model3DHandler in order to create the 3D surface.
	* @return bool
	*/
	Q_INVOKABLE bool submitPreview();

	
	/**
	* Method to set the type of the surface being created.
	* @param type 0 is stratigraphy, 1 is fault.
	* @return void
	*/
	Q_INVOKABLE void setSurfaceType(int type);


	/**
	* Method to get the surface type vector
	* @return std::vector<int>
	*/
	Q_INVOKABLE std::vector<int> getSurfaceTypeList();
	Q_INVOKABLE QString getRegionPolygon();
	//--- END OF FAZILA'S PUBLIC METHODS ----------------------------------------


	//--- SICILIA'S PUBLIC METHODS ----------------------------------------------

	/**
	* Method that returns the current cross section.
	* @return int
	*/
	int crossSection() const;

	
	/**
	* Method that sets the current cross section.
	* @param cross_section is the numerical identification of the cross section.
	* @return void
	*/
	void setCrossSection(int cross_section);


	/**
	* Method to get the previous cross section ID used.
	* @param min is the minimum value for a cross section.
	* @return int
	*/
	[[deprecated("Call getPreviousUsedCrossSection")]]
	Q_INVOKABLE int getPreviousSketchCrossSection(int min);
	

	/**
	* Method to get the next cross section ID used.
	* @param max is the maximum value for a cross section.
	* @return int
	*/
	[[deprecated("Call getNextUsedCrossSection")]]
	Q_INVOKABLE int getNextSketchCrossSection(int max);


	/**
	* Method to get the next cross section ID used.
	* @return int
	*/
	Q_INVOKABLE int getPreviousUsedCrossSection();


	/**
	* Method to get the previous cross section ID used.
	* @return int
	*/
	Q_INVOKABLE int getNextUsedCrossSection();


	/**
	* Method to add a cross section ID to the collection of cross sections used.
	* @param csID is the ID of the used cross section.
	* @return void
	*/
	Q_INVOKABLE void addCrossSectionUsed(int csID, int direction);


	/**
	* Method to add a cross section ID to the collection of cross sections used.
	* @param csID is the ID of the used cross section.
	* @return void
	*/
	Q_INVOKABLE void loadCrossSectionUsed(QJsonArray file);


	/**
	* Method to reset the highlight and select flags for FrontView.
	* @return void
	*/
	Q_INVOKABLE void resetHighlightSelection_FV();
	
	
	/**
	* Method to reset the highlight and select flags for MapView.
	* @return void
	*/
	Q_INVOKABLE void resetHighlightSelection_MV();


	/**
	* Method that receives from Front View canvas the user interaction and delegates to the responsible handler.
	* @param buttonID identifies which button from the mouse is in use
	* @param buttonAction identifies which action from the mouse is in use
	* @param mouse_x is the X coordinate of the mouse pointer (Screen Coordinate System)
	* @param mouse_y is the Y coordinate of the mouse pointer (Screen Coordinate System)
	* @return void
	*/
	Q_INVOKABLE void delegateFrontViewAction(int buttonID, int buttonAction, double mouse_x, double mouse_y);
	
	
	/**
	* Method that receives from Map View canvas the user interaction and delegates to the responsible handler.
	* @param buttonID identifies which button from the mouse is in use
	* @param buttonAction identifies which action from the mouse is in use
	* @param mouse_x is the X coordinate of the mouse pointer (Screen Coordinate System)
	* @param mouse_y is the Y coordinate of the mouse pointer (Screen Coordinate System)
	* @return void
	*/
	Q_INVOKABLE void delegateMapViewAction(int buttonID, int buttonAction, double mouse_x, double mouse_y, double radiusX, double radiusY);
	
	
	/**
	* Method that receives actions related to the buttons on the bottom of Front View.
	* @param action is the action related to the buttons.
	* @return void
	*/
	Q_INVOKABLE void delegateFrontViewVisualization(int action);


	/**
	* Method that receives actions related to the buttons on the bottom of Map View.
	* @param action is the action related to the buttons.
	* @return void
	*/
	Q_INVOKABLE void delegateMapViewVisualization(int action);


	/**
	* Method that receives actions related to Sketch Region process.
	* @param buttonID identifies which button from the mouse is in use
	* @param buttonAction identifies which action from the mouse is in use
	* @param model_x is the X coordinate of the mouse pointer converted into Model Coordinate System
	* @param model_y is the Y coordinate of the mouse pointer converted into Model Coordinate System
	* @return void
	*/
	void delegateSketchRegionAction(int buttonID, int buttonAction, double model_x, double model_y);

	
	/**
	* Method that receives actions related to Region Visualization.
	* @param canvasID identifies from which canvas this method was called
	* @param buttonID identifies which button from the mouse is in use
	* @param buttonAction identifies which action from the mouse is in use
	* @param mouse_x is the X coordinate of the mouse pointer in Screen Coordinate System
	* @param mouse_y is the Y coordinate of the mouse pointer in Screen Coordinate System
	* @return void
	*/
	Q_INVOKABLE void delegateRegionAction(int canvasID, int buttonID, int buttonAction, double mouse_x, double mouse_y);

	
	/**
	* Method that verifies if a click was made inside of the Model domain.
	* @param model_x is the X coordinate of the mouse pointer in Model Coordinate System
	* @param model_y is the Y coordinate of the mouse pointer in Model Coordinate System
	* @param dimX is the dimension related to X coordinate
	* @param dimY is the dimension related to Y coordinate
	* @return bool
	*/
	bool isClickInsideDomain(double model_x, double model_y, double dimX, double dimY);


	/**
	* Method that receives actions related to Flattening process.
	* @param buttonID identifies which button from the mouse is in use
	* @param buttonAction identifies which action from the mouse is in use
	* @param model_x is the X coordinate of the mouse pointer converted into Model Coordinate System
	* @param model_y is the Y coordinate of the mouse pointer converted into Model Coordinate System
	* @return void
	*/
	void delegateFlatteningAction(int buttonID, int buttonAction, double model_x, double model_y);

	/**
	* Method that finds the nearest curve to the mouse pointer to flatt
	* @param model_x is the X coordinate of the mouse pointer converted into Model Coordinate System
	* @param model_y is the Y coordinate of the mouse pointer converted into Model Coordinate System
	* @return int
	*/
	int findSurfaceIDtoFlatt(double model_x, double model_y);


	/**
	* Method to update the sketch data structure every time the user changes the direction in the GUI.
	* @return void
	*/
	Q_INVOKABLE void updateSketchDataStructure();


	/**
	* Method to set a QFuture to watch for the submitSketch process
	* @return void
	*/
	Q_INVOKABLE void futureSubmitPreview();
	
	/**
	* Method to set a QFuture to watch for the submitSketch process
	* @return void
	*/
	Q_INVOKABLE void futureSubmitSketch();

	/**
	* Method responsible to submit the sketch to the 3D Model to create the 3D surface.
	* It connects with SketchHandler to receive data of the sketches to be submitted.
	* It connects with the Model3DHandler in order to create the 3D surface.
	* @return bool
	*/
	Q_INVOKABLE bool submitSketch();
	Q_INVOKABLE bool submitSketch(bool preview);


	/**
	* Method responsible to return to QML the preview curves (return in Screen Coordinate System).
	* It receive from the Model3D the intersection of the preview surface with the current cross section.
	* @param canvasID is the identification of the canvas
	* @return QString
	*/
	Q_INVOKABLE QString returnPreview(int canvasID);


	/**
	* Method responsible to return to QML the submitted curves (return in Screen Coordinate System).
	* It receive from the Model3D all the intersection of all surfaces with the current cross section.
	* @param canvasID is an identification from which canvas this method is called.
	* @return QString
	*/
	Q_INVOKABLE QString getSubmittedCurves(int canvasID);

	
	/**
	* Method responsible to return the submitted curve (return in Screen Coordinate System).
	* @param surfaceID is an identification of the surface.
	* @return std::vector<double>
	*/
	std::vector<double> getCurve(int surfaceID);


	/**
	* Method responsible to return to QML the submitted trajectories (return in Screen Coordinate System).
	* @return QString
	*/
	Q_INVOKABLE QString returnSubmittedTrajectories();


	Q_INVOKABLE QString returnCurrentSketches();		/**< Update the visualization of all the current sketches in Mapview*/

	/**
	* Method responsible to return to QML the submitted contours (in Screen Coordinate System).
	* @return QString
	*/
	Q_INVOKABLE QString returnSubmittedContours();


	/**
	* Method responsible to return to QML the current contours (in Screen Coordinate System).
	* @return QString
	*/
	Q_INVOKABLE QString returnCurrentContours();


	/**
	* Method that will update the collection of contours
	* @return void
	*/
	Q_INVOKABLE void updateContours();


	/**
	* Method that will update the collection of current contours
	* @return void
	*/
	Q_INVOKABLE void updateCurrentContours();


	/**
	* Method to calculate the preserve region boundary.
	* @param x coordinate of the point clicked by the user to select the region.
	* @param y coordinate of the point clicked by the user to select the region.
	* @return void
	*/
	Q_INVOKABLE void preserveRegionBoundary(double x, double y);


	/**
	* Method to update the region according to the cross section change.
	* @return void
	*/
	Q_INVOKABLE void updateRegionBoundary();


	/**
	* Method to calculate the preserve boundary.
	* This method will use the sketch created by the user to identify the upper or lower boundary of the region.
	* If the user sketched from top to bottom he intends to preserve below.
	* If the user sketched from bottom to top he intends to preserve above.
	* @return void
	*/
	Q_INVOKABLE void preserveRegionBoundary();


	/**
	* Method to return to QML the preserve region boundary.
	* @return QString
	*/
	Q_INVOKABLE QString returnRegion();


	/**
	* Method to save the current sketch as a sketch template
	* @return void
	*/
	Q_INVOKABLE void saveSketchTemplate();


	/**
	* Method to save the current sketch contour as a sketch template
	* @return void
	*/
	Q_INVOKABLE void saveSketchContourTemplate();


	/**
	* Method to load the saved sketch template in the current cross section.
	* @param index is the position in the template collection where the selected sketch is.
	* @return void
	*/
	Q_INVOKABLE void loadSketchTemplate(int index);


	/**
	* Method to load the saved contour template in the current cross section.
	* @param index is the position in the template collection where the selected sketch is.
	* @return void
	*/
	Q_INVOKABLE void loadSketchContourTemplate(int index);


	/**
	* Method to load the trajectory template selected by the user.
	* @param index is the position in the template collection where the selected trajectory is.
	* @return void
	*/
	Q_INVOKABLE void loadTrajectoryTemplate(int index);

	
	/**
	* Method that saves SketchData to disk
	* @param fileUrl
	* @return void
	*/
	Q_INVOKABLE void saveSbimData(QUrl fileUrl);

	
	/**
	* Method that loads SbimData from disk
	* @param fileUrl
	* @return void
	*/
	Q_INVOKABLE void loadSbimData(QUrl fileUrl);

	
	/**
	* Method that saves SBIM data objects
	* This is an invokable method that may be called from QML
	* @param const QUrl& path
	* @return std::string
	*/
	Q_INVOKABLE std::string getAbsPath(const QUrl& path);


	/**
	* Method to update the flag isSurfaceCreated
	* @return void
	*/
	Q_INVOKABLE void updateIsSurfaceCreated();


	/**
	* Method to store the click of the mouse when fixing guidelines in FrontView
	* @param mouseX is the X coordinate of the mouse click
	* @param mouseY is the Y coordinate of the mouse click
	* @return void
	*/
	Q_INVOKABLE void fixGuidelineFrontView(double mouseX, double mouseY);


	/**
	* Method to store the click of the mouse when fixing guidelines in MapView
	* @param mouseX is the X coordinate of the mouse click
	* @param mouseY is the Y coordinate of the mouse click
	* @return void
	*/
	Q_INVOKABLE void fixGuidelineMapView(double mouseX, double mouseY);


	/**
	* Methods to get the stored click of the mouse when fixing guidelines in FrontView
	* @return double
	*/
	Q_INVOKABLE double getFV_gl_mouseX();
	Q_INVOKABLE double getFV_gl_mouseY();
	Q_INVOKABLE double getMV_gl_mouseX();
	Q_INVOKABLE double getMV_gl_mouseY();


	//--- END OF SICILIA'S PUBLIC METHODS ---------------------------------------


signals:
	//--- FAZILA'S SIGNALS ------------------------------------------------------	
	void updateSurfaceList(std::vector<int> surfaceList);
	void surfaceTypeChanged(int surfaceType);
	void resetNew();
	void pullRegionCurveBox();
	void refreshSubmittedCurves();
	void clearRegionCurveBox();
	void flatteningStarted(int surface_id_to_flat, double flat_height);
	void flatteningStopped();

	//--- END OF FAZILA'S SIGNALS -----------------------------------------------	
	
	//--- SICILIAS'S SIGNALS ----------------------------------------------------	
	void canvasActionChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/
	void crossSectionChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/
	void directionChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/
	void isLengthChanged();				/**< Q_PROPERTY: signal related to NOTIFY.*/
	void isWidthChanged();				/**< Q_PROPERTY: signal related to NOTIFY.*/
	void isHeightChanged();				/**< Q_PROPERTY: signal related to NOTIFY.*/
	void csWE_ScaleChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/
	void csNS_ScaleChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/
	void csMap_ScaleChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/
	void csWE_Origin_xChanged();		/**< Q_PROPERTY: signal related to NOTIFY.*/
	void csWE_Origin_yChanged();		/**< Q_PROPERTY: signal related to NOTIFY.*/
	void csNS_Origin_xChanged();		/**< Q_PROPERTY: signal related to NOTIFY.*/
	void csNS_Origin_yChanged();		/**< Q_PROPERTY: signal related to NOTIFY.*/
	void csMap_Origin_xChanged();		/**< Q_PROPERTY: signal related to NOTIFY.*/
	void csMap_Origin_yChanged();		/**< Q_PROPERTY: signal related to NOTIFY.*/
	void isSketchRegionChanged();		/**< Q_PROPERTY: signal related to NOTIFY.*/
	void isPreviewFlagChanged();		/**< Q_PROPERTY: signal related to NOTIFY.*/
	void isPathGuidedChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/
	void isSubmitAllowedChanged();		/**< Q_PROPERTY: signal related to NOTIFY.*/
	void pgeIsActiveChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/
	void selectIndexChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/
	void highlightFVChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/
	void highlightMVChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/
	void highlightTRChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/
	void veFactorChanged();				/**< Q_PROPERTY: signal related to NOTIFY.*/
	void veAngleChanged();				/**< Q_PROPERTY: signal related to NOTIFY.*/
	void veAngleVisibleChanged();		/**< Q_PROPERTY: signal related to NOTIFY.*/
	void veBkgImageChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/
	void flatHeightChanged();			/**< Q_PROPERTY: signal related to NOTIFY.*/

	void getUpdatedPolygonCorners(QString response);	/**< Sketch Region: update the visualization fo the region.*/

	void selectTrajectory(bool response);
	void selectSketchFV(bool response);
	void selectSketchMV(bool response);
	void highlightTrajectory(bool response);
	void highlightSketch(bool response);
	void getUpdatedSketch(QString response);
	void getUpdatedSketches(QString response);
	void getUpdatedOverSketch(QString response);
	void getUpdatedTrajectory(QString response);
	void getUpdatedTrajectoryOverSketch(QString response);
	void updateTrajectoryTemplatesList();
	void updateSketchTemplatesList();
	void updateContourTemplatesList();	
	void getUpdatedSketchRegion(QString response);
	void updateLoadedData();

	void updateDirection();
	void enableDirection();
	void surfaceCreated(bool response);
	void highlightSurfaceCreated(int index);
	void updateBBoxFlattening(double height);

	void modelLoadingCompleted();

	void regionClicked(int _region, int _regionSize, QString _regionName, QString _regionVolume, QString _regionColor,
		int _domain, QString _domainName, QString _domainVolume, QString _domainColor);

	void sketchSubmitted();

	void previewSubmitted();
	//--- END OF SICILIAS'S SIGNALS ---------------------------------------------


public slots:
	//--- FAZILA'S PUBLIC SLOTS -------------------------------------------------	
	void undo3d();
	void redo3d();

	void flattenTheSelectedSurface(int surface_id);
	//--- END OF FAZILA'S PUBLIC SLOTS ------------------------------------------	

	//--- SICILIA'S PUBLIC SLOTS ------------------------------------------------	
	void returnSketch();		/**< Update the visualization of the current sketch in the QML side*/	
	void returnTrajectory();	/**< Update the visualization of the current trajectory in the QML side.*/
	void returnOverSketch(std::vector<double> curve);
	void returnTrajectoryOverSketch(std::vector<double> curve);

	//- Related to Regions
	void updateSketchRegionStatus();
	void returnSketchRegion();
	void onSketchRegionAddPoint(double mouse_x, double mouse_y);
	void startNewSketchRegion();
	void resetSketchRegion();
	bool calculateRegionPolygon();

	//- File Handle
	void modelLoaded(const QUrl& path);

	//--- END OF SICILIA'S PUBLIC SLOTS -----------------------------------------	

		
private:

	//--- FAZILA'S PRIVATE MEMBERS ----------------------------------------------	
	//- Data Structure to save the state of the GUI
	int gui_surfaceType = 0;					/**< 0 is stratigraphy, 1 is fault.*/
	std::vector<int> gui_surfaceID;				/**< Keep track of the surface type and ID.*/
	//--- END OF FAZILA'S PRIVATE MEMBERS ---------------------------------------	


	//--- SICILIA'S PRIVATE MEMBERS ---------------------------------------------		
	int m_canvasAction;					/**< Action: keeps track of the current action inside the canvas.*/

	//- Cross Section 
	int m_crossSection;					/**< Cross Section: numerical identification of the current cross section. */
	int m_direction;					/**< Cross Section: numerical identification of the current direction of the cross section.*/
	bool m_isLength;					/**< Cross Section: flag to keep track of which direction among LENGTH, WIDTH and HEIGHT was selected last.*/
	bool m_isWidth;						/**< Cross Section: flag to keep track of which direction among LENGTH, WIDTH and HEIGHT was selected last.*/
	bool m_isHeight;					/**< Cross Section: flag to keep track of which direction among LENGTH, WIDTH and HEIGHT was selected last.*/

	double m_cs_Position_W;				/**< Cross Section: the position of the current cross section inside the model in WIDTH discreztization. */
	double m_cs_Position_L;				/**< Cross Section: the position of the current cross section inside the model in LENGTH discreztization. */
	double m_cs_Position_H;				/**< Cross Section: the position of the current cross section inside the model in HEIGHT discreztization. */

	double m_csWE_Scale;				/**< Cross Section: scale factor to fit the Cross Section inside the Front View Canvas in WE direction.*/
	double m_csNS_Scale;				/**< Cross Section: scale factor to fit the Cross Section inside the Front View Canvas in NS direction.*/
	double m_csMap_Scale;				/**< Cross Section: scale factor to fit the Cross Section inside the Map View.*/

	double m_csWE_Origin_x;				/**< Cross Section: X coordinate of the origin of the Cross Section inside the Front View Canvas in WE direction (Screen Coordinate System)*/
	double m_csWE_Origin_y;				/**< Cross Section: Y coordinate of the origin of the Cross Section inside the Front View Canvas in WE direction (Screen Coordinate System)*/
	double m_csNS_Origin_x;				/**< Cross Section: X coordinate of the origin of the Cross Section inside the Front View Canvas in NS direction (Screen Coordinate System)*/
	double m_csNS_Origin_y;				/**< Cross Section: Y coordinate of the origin of the Cross Section inside the Front View Canvas in NS direction (Screen Coordinate System)*/
	double m_csMap_Origin_x;			/**< Cross Section: X coordinate of the origin of the Cross Section inside the Map View (Screen Coordinate System)*/
	double m_csMap_Origin_y;			/**< Cross Section: Y coordinate of the origin of the Cross Section inside the Map View (Screen Coordinate System)*/

	double m_Flat_Height;				/**< Bounding Box: new height of the box when Flattening is on.*/
	double m_Flat_delta;

	double m_VE_factor;					/**< Vertical Exxageration scale factor*/
	double m_VE_angle;					/**< Vertical Exxageration angle for the guidelines*/
	bool m_VE_angle_visible;			/**< Flag for visibility of the Vertical Exxageration angle in the guidelines*/
	bool m_VE_bkg_image;				/**< Flag to keep track if VE will be applied to background image or not.*/

	//- Sketch Regions
	std::vector<double> sketchRegion;	/**< Sketch Region: Vector to store the sketch made by the user to select regions above or below. Screen Coordinate System.*/
	bool m_isSketchRegion;				/**< Sketch Region: Sketch Region: flag to keep track if the Sketch Region was selected.*/

	//- Regions
	std::vector<double> pr_boundary;	/**< Region: Vector with the points of the boundary of the region.*/
	std::vector<double> pa_boundary;	/**< Region: Vector with the points of the ABOVE boundary of the region.*/
	std::vector<double> pb_boundary;	/**< Region: Vector with the points of the BELOW boundary of the region.*/
	bool   regionIsValid;				/**< Region: Flag to indicate that the region selected is valid.*/

	//- 2D Point 
	double m_point_x;		/**< The X coordinate of a 2D point received from GUI (Screen Coordinate System). */
	double m_point_y;		/**< The Y coordinate of a 2D point received from GUI (Screen Coordinate System). */

	bool m_isPreview;		/**< Flag to keep track if the current surface is a preview or not.*/
	bool m_isPathGuided;	/**< Flag to keep track if there is a trajectory to be calculated.*/
	bool m_isSubmitAllowed; /**< Flag to keep track if there is at least one sketch to be submitted.*/
	bool m_pgeIsActive;		/**< Flag to keep track the extrusion method: TRUE is Path Guided Extrusion, FALSE is Linear Extrusion*/


	//- Sketch
	SketchHandler* sketchHandler = nullptr;		/**< Instance of the SketchHandler to receive data for SBIM classes.*/
	int m_selectIndex;							/**< To keep track of which sketch is selected in case there is more than one sketch in the same cross section.*/

	int m_highlight_FV;							/**< FrontView: To keep track of which sketch is highlighted in case there is more than one sketch in the same cross section.*/
	int m_highlight_MV;							/**< MapView: To keep track of which sketch is highlighted in case there is more than one sketch in the same cross section.*/
	int m_highlight_TR;							/**< Trajectory: To keep track of which sketch is highlighted in case there is more than one sketch in the same cross section.*/

	//- Templates
	std::shared_ptr<SketchTemplate> sketchTemplates = nullptr;		/**< Instance of the SketchTemplate to store the sketch as templates. */
	std::shared_ptr<SketchTemplate> trajectoryTemplates = nullptr;	/**< Instance of the SketchTemplate to store the trajectories as templates. */
	std::shared_ptr<SketchTemplate> contourTemplates = nullptr;		/**< Instance of the SketchTemplate to store the sketch contours as templates. */

	//- Sketch Map View
	std::shared_ptr<SketchMapView> sketchContours = nullptr;		/**< Collection of the sketch contours.*/
	std::shared_ptr<SketchMapView> sketchTrajectories = nullptr;	/**< Collection of submitted trajectories.*/

	//- Previous and Next Cross Sections
	std::vector<int> m_usedCrossSections_NS;	/**< Collection of all cross sections used during sketching process in NS direction.*/
	std::vector<int> m_usedCrossSections_WE;	/**< Collection of all cross sections used during sketching process in WE direction.*/
	std::vector<int> m_usedCrossSections_MAP;	/**< Collection of all cross sections used during sketching process in MAP direction.*/

	//- Control Fixed Guidelines
	double gl_FV_mouseX;		/**< X coordinate of the mouse click on Front View to fix the guideline*/
	double gl_FV_mouseY;		/**< Y coordinate of the mouse click on Front View to fix the guideline*/
	double gl_MV_mouseX;		/**< X coordinate of the mouse click on Map View to fix the guideline*/
	double gl_MV_mouseY;		/**< Y coordinate of the mouse click on Map View to fix the guideline*/

	//--- END OF SICILIA'S PRIVATE MEMBERS --------------------------------------	


	//--- FAZILA'S PRIVATE METHODS ----------------------------------------------	
	//--- END OF FAZILA'S PRIVATE METHODS ---------------------------------------	

	//--- SICILIA'S PRIVATE METHODS ---------------------------------------------	
	/**
	* Method that maps a point (x,y) from the screen to the model coordinate system.
	* Where screen here is specifically the Front View canvas with WE cross section.
	* @param screen_x is the x coordinate of the point from the screen.
	* @param screen_y is the y coordinate of the point from the screen.
	* @param model_x is the mapped x in the model coordinate system.
	* @param model_y is the mapped y in the model coordinate system.
	* @return void
	*/
	void fromFrontViewWEToModel(double screen_x, double screen_y, double& model_x, double& model_y);


	/**
	* Method that maps a point (x,y) from the screen to the model coordinate system.
	* Where screen here is specifically the Front View canvas with NS cross section.
	* @param screen_x is the x coordinate of the point from the screen.
	* @param screen_y is the y coordinate of the point from the screen.
	* @param model_x is the mapped x in the model coordinate system.
	* @param model_y is the mapped y in the model coordinate system.
	* @return void
	*/
	void fromFrontViewNSToModel(double screen_x, double screen_y, double& model_x, double& model_y);


	/**
	* Method that maps a point (x,y) from the screen to the model coordinate system.
	* Where screen here is specifically the Map View canvas.
	* @param screen_x is the x coordinate of the point from the screen.
	* @param screen_y is the y coordinate of the point from the screen.
	* @param model_x is the mapped x in the model coordinate system.
	* @param model_y is the mapped y in the model coordinate system.
	* @return void
	*/
	void fromMapViewToModel(double screen_x, double screen_y, double& model_x, double& model_y);


	/**
	* Method that maps an array of points (x,y) from the model to the screen coordinate system.
	* Where screen here is specifically the Front View canvas with WE cross section.
	* @param old is the array of points (x,y) in the model coordinate system.
	* @return std::vector<double>
	*/
	std::vector<double> fromModelToFrontViewWE(std::vector<double> old);


	/**
	* Method that maps an array of points (x,y) from the model to the screen coordinate system.
	* Where screen here is specifically the Front View canvas with NS cross section.
	* @param old is the array of points (x,y) in the model coordinate system.
	* @return std::vector<double>
	*/
	std::vector<double> fromModelToFrontViewNS(std::vector<double> old);


	/**
	* Method that maps an array of points (x,y) from the model to the screen coordinate system.
	* Where screen here is specifically the Map View.
	* @param old is the array of points (x,y) in the model coordinate system.
	* @return std::vector<double>
	*/
	std::vector<double> fromModelToMapView(std::vector<double> old);


	/**
	* Method that transforms vector double to Qt data structure QPolygonF.
	* @param curve_coordinates is the vector of double.
	* @return QPolygonF
	*/
	QPolygonF fromVectorDoubleToQt(std::vector<double> curve_coordinates);
	
	
	/**
	* Method that transforms Qt data structure QPolygonF into vector double.
	* @param pol_ is the Qt data structure QPolygonF.
	* @return std::vector<double>
	*/
	std::vector<double> fromQtToVectorDouble(const QPolygonF& pol_);


	/**
	* Method that prepares a vector double into a Json Object.
	* @param curve is the vector double.
	* @return QString
	*/
	QString prepareJsonObject(std::vector<double> curve);
	QString implode2json(std::vector<std::vector<double>> array);


	/**
	* Method that preserves above.
	* @return std::vector<double>
	*/
	std::vector<double> preserveAbove();
	
	
	/**
	* Method that preserves below.
	* @return std::vector<double>
	*/
	std::vector<double> preserveBelow();
	//--- END OF SICILIA'S PRIVATE METHODS --------------------------------------	

};

#endif // CANVASHANDLER