#include "CanvasHandler.h"

#include <QDebug>
#include <QString>
#include <QObject>
#include <cmath>

#include <QFutureWatcher>
#include <QtConcurrent>

#include "Model3dHandler.h"
#include "sbim/SbimFileHandler.h"

CanvasHandler::CanvasHandler(QObject* parent)
{
    this->sketchHandler = new SketchHandler();
    this->sketchTemplates = std::make_shared<SketchTemplate>();
    this->trajectoryTemplates = std::make_shared<SketchTemplate>();
    this->contourTemplates = std::make_shared<SketchTemplate>();
    this->sketchContours = std::make_shared<SketchMapView>();
    this->sketchTrajectories = std::make_shared<SketchMapView>();

    this->reset();

    if (this->sketchHandler)
    {
        connect(sketchHandler, &SketchHandler::updatePreviewFlag, [=](bool flag){
            m_isPreview = flag;
        });

        connect(this->sketchHandler, &SketchHandler::updateSelectSketch, [=](bool response) 
            {
                if (m_direction == Direction::Value::HEIGHT) {
                    m_selectIndex = sketchHandler->getSelectIndex(m_crossSection, m_direction);
                    emit(selectSketchMV(response));
                }
                else
                    emit(selectSketchFV(response));
            });
        
        connect(this->sketchHandler, &SketchHandler::updateHighlightSketch, [=](bool response)
            {
                if (m_direction == Direction::Value::HEIGHT) {
                    m_highlight_MV = sketchHandler->getHighlightIndex(m_crossSection, m_direction);
                }
                else {
                    m_highlight_FV = sketchHandler->getHighlightIndex(m_crossSection, m_direction);
                }
                
                emit(highlightSketch(response));
            });

        connect(this->sketchHandler, &SketchHandler::updateHighlightTrajectory, [=](bool response)
            {
                if(response)
                    m_highlight_TR = 0;
                else
                    m_highlight_TR = -1;
                emit(highlightTrajectory(response));
            });

        connect(this->sketchHandler, &SketchHandler::enableDirectionButtons, [=]() 
            {
                m_isSubmitAllowed = false;
                emit(enableDirection()); 
            });
        
        connect(this->sketchHandler, &SketchHandler::updateSelectTrajectory, this, &CanvasHandler::selectTrajectory);         
        connect(this->sketchHandler, &SketchHandler::updateSketch, this, &CanvasHandler::returnSketch);
        connect(this->sketchHandler, &SketchHandler::updateOverSketch, this, &CanvasHandler::returnOverSketch);
        connect(this->sketchHandler, &SketchHandler::updateTrajectory, this, &CanvasHandler::returnTrajectory);
        connect(this->sketchHandler, &SketchHandler::updateTrajectoryOverSketch, this, &CanvasHandler::returnTrajectoryOverSketch);
        connect(this->sketchHandler, &SketchHandler::updateDirectionButtons, [=]() {emit(updateDirection()); });
        connect(this->sketchHandler, &SketchHandler::updateUsedCrossSections, this, &CanvasHandler::addCrossSectionUsed);
    }
}


void CanvasHandler::reset()
{
    //qDebug() << "\n\n-----------------------------> CanvasHandler::reset() ";
    this->m_canvasAction = CanvasAction::Value::FB_SKETCH;

    //- Cross Section
    this->m_crossSection = 64;
    this->m_direction = Direction::Value::LENGTH;
    this->m_isLength = true;
    this->m_isWidth = false;
    this->m_isHeight = false;

    this->m_cs_Position_W = 0.0;
    this->m_cs_Position_L = 0.0;
    this->m_cs_Position_H = 0.0;

    this->m_csWE_Scale = 1.0;
    this->m_csNS_Scale = 1.0;
    this->m_csMap_Scale = 1.0;

    this->m_csWE_Origin_x = 0.0;
    this->m_csWE_Origin_y = 0.0;
    this->m_csNS_Origin_x = 0.0;
    this->m_csNS_Origin_y = 0.0;
    this->m_csMap_Origin_x = 0.0;
    this->m_csMap_Origin_y = 0.0;

    this->m_Flat_Height = 0.0;
    this->m_Flat_delta = 0.0;

    this->m_VE_factor = 1.0;
    this->m_VE_angle = 0.0;
    this->m_VE_angle_visible = false;
    this->m_VE_bkg_image = false;

    //- Sketch Region
    this->sketchRegion.clear();
    this->m_isSketchRegion = false;


    //- 2D Point
    this->m_point_x = 0.0;
    this->m_point_y = 0.0;

    this->m_isPreview = false;
    this->m_isPathGuided = false;
    this->m_isSubmitAllowed = false;
    this->m_pgeIsActive = false;

    //- Region
    this->regionIsValid = false;
    this->pr_boundary.clear();

    //- Sketch
    this->sketchHandler->reset();
    this->m_selectIndex = -1;
    this->m_highlight_FV = -1;
    this->m_highlight_MV = -1;
    this->m_highlight_TR = -1;

    this->sketchTemplates->reset();
    this->trajectoryTemplates->reset();
    this->contourTemplates->reset();
    
    //- Sketch MapView
    this->sketchContours->reset();
    this->sketchTrajectories->reset();

    this->m_usedCrossSections_NS.clear();
    this->m_usedCrossSections_WE.clear();
    this->m_usedCrossSections_MAP.clear();
    
    this->gui_surfaceID.clear();

    //- Fix Guidelines
    this->gl_FV_mouseX = 0.0;
    this->gl_FV_mouseY = 0.0;
    this->gl_MV_mouseX = 0.0;
    this->gl_MV_mouseY = 0.0;

    emit(resetNew());
}


//--- FAZILA'S PUBLIC METHODS -----------------------------------------------

void CanvasHandler::setSurfacesID()
{
    //- Get the surface indices from the Model3D
    std::vector<int> index =  Model3dHandler::getInstance(nullptr)->getSurfaceIndices();

    for (size_t i = 0; i < index.size(); i++)
    {
        this->gui_surfaceID.push_back(0);
    }
}


void CanvasHandler::setSurfaceType(int type)
{
    this->gui_surfaceType = type;
    emit(surfaceTypeChanged(this->gui_surfaceType));
}


std::vector<int> CanvasHandler::getSurfaceTypeList()
{
    return this->gui_surfaceID;
}


QString CanvasHandler::getRegionPolygon()
{
    //- Get instance of the 3D Model
    int width_Discretization, length_Discretization;
    Model3dHandler::getInstance(nullptr)->getModelDiscretization(width_Discretization, length_Discretization);
    std::vector<double> region_list;
    region_list = Model3dHandler::getInstance(nullptr)->getRegionList();//Model3dHandler::getInstance(nullptr)->model().ComputeTetrahedralMeshVolumes(region_list);
    //qDebug() << "CanvasHandler::getRegionPolygon(): Region list size = " << region_list.size();

    //- Calculate the region polygon
    std::vector<std::vector<double>> region_curve_box{};

    for (size_t i = 0; i < region_list.size(); i++)
    {
        //if (region_list[i] > 0.0)
        {
            std::vector<double> lower_bound_box_vlist{};
            std::vector<std::size_t> lower_bound_box_elist{};
            std::vector<double> upper_bound_box_vlist{};
            std::vector<std::size_t> upper_bound_box_elist{};
            std::vector<double> each_region_curve_box{};

            if (this->m_isLength)
            {
                int crossSection_ID = length_Discretization - this->m_crossSection;
                Model3dHandler::getInstance(nullptr)->getFlattenedLengthRegionCurveBox(i, crossSection_ID,
                    lower_bound_box_vlist, lower_bound_box_elist, upper_bound_box_vlist, upper_bound_box_elist);

                QPolygonF polygon_lower = this->fromVectorDoubleToQt(lower_bound_box_vlist);
                QPolygonF polygon_upper = this->fromVectorDoubleToQt(upper_bound_box_vlist);

                QPolygonF polygon_intersected = polygon_upper.intersected(polygon_lower);
                each_region_curve_box = this->fromQtToVectorDouble(polygon_intersected);

                region_curve_box.push_back(this->fromModelToFrontViewWE(each_region_curve_box));
            }
            else if (this->m_isWidth)
            {

                int crossSection_ID = this->m_crossSection;
                Model3dHandler::getInstance(nullptr)->getFlattenedWidthRegionCurveBox(i, crossSection_ID,
                    lower_bound_box_vlist, lower_bound_box_elist, upper_bound_box_vlist, upper_bound_box_elist);

                QPolygonF polygon_lower = this->fromVectorDoubleToQt(lower_bound_box_vlist);
                QPolygonF polygon_upper = this->fromVectorDoubleToQt(upper_bound_box_vlist);

                QPolygonF polygon_intersected = polygon_upper.intersected(polygon_lower);
                each_region_curve_box = this->fromQtToVectorDouble(polygon_intersected);

                region_curve_box.push_back(this->fromModelToFrontViewNS(each_region_curve_box));
            }
        }
    }
    //qDebug() << "CanvasHandler::getRegionPolygon(): Region curve_box = " << this->implode2json(region_curve_box);
    return this->implode2json(region_curve_box);
}
//--- END OF FAZILA'S PUBLIC METHODS ----------------------------------------


//--- SICILIA'S PUBLIC METHODS ----------------------------------------------
int CanvasHandler::crossSection() const {
    return this->m_crossSection;
}


void CanvasHandler::setCrossSection(int cross_section)
{
    this->m_crossSection = cross_section;
    this->sketchHandler->resetSelect();

    //- Get instance of the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);
    int width_Discretization, length_Discretization;
    Model3dHandler::getInstance(nullptr)->getModelDiscretization(width_Discretization, length_Discretization);

    //- Update current position of the cross section in the model related to the current direction
    int crossSection_ID;
    if (this->m_direction == Direction::Value::LENGTH)
    {
        crossSection_ID = length_Discretization - this->m_crossSection;
        this->m_cs_Position_L = static_cast<double>(((crossSection_ID)*box_l) / length_Discretization);
        ////qDebug() << "Set Cross Section: " << m_crossSection << ", " << m_cs_Position_L;
    }
    else if (this->m_direction == Direction::Value::WIDTH)
    {
        crossSection_ID = this->m_crossSection;
        this->m_cs_Position_W = static_cast<double>(((crossSection_ID)*box_w) / width_Discretization);
    }
    else if (this->m_direction == Direction::Value::HEIGHT)
    {
        this->m_cs_Position_H = 0.0; //TO BE IMPLEMENTED
    }
}

int CanvasHandler::getPreviousSketchCrossSection(int min)
{
    if (this->m_crossSection > min)
    {
        int response;
        response = this->sketchHandler->getPreviousSketchCrossSection(this->m_crossSection);
        if (response > -1)
            return response;
    }

    return this->m_crossSection;
}


int CanvasHandler::getNextSketchCrossSection(int max)
{
    if (this->m_crossSection < max)
    {
        int response;
        response = this->sketchHandler->getNextSketchCrossSection(this->m_crossSection);
        if (response > -1)
            return response;
    }
      
    return this->m_crossSection;
}


int CanvasHandler::getPreviousUsedCrossSection()
{
    int response = this->m_crossSection;

    if (this->m_direction == Direction::Value::LENGTH)
    {
        for (int i = 0; i < this->m_usedCrossSections_WE.size(); i++)
        {
            if (this->m_usedCrossSections_WE[i] < this->m_crossSection)
            {
                response = this->m_usedCrossSections_WE[i];
            }
        }
    }
    else if (this->m_direction == Direction::Value::WIDTH)
    {
        for (int i = 0; i < this->m_usedCrossSections_NS.size(); i++)
        {
            if (this->m_usedCrossSections_NS[i] < this->m_crossSection)
            {
                response = this->m_usedCrossSections_NS[i];
            }
        }
    }
    else if (this->m_direction == Direction::Value::HEIGHT)
    {
        for (int i = 0; i < this->m_usedCrossSections_MAP.size(); i++)
        {
            if (this->m_usedCrossSections_MAP[i] < this->m_crossSection)
            {
                response = this->m_usedCrossSections_MAP[i];
            }
        }
    }

    return response;
}


int CanvasHandler::getNextUsedCrossSection()
{
    int response = this->m_crossSection;

    if (this->m_direction == Direction::Value::LENGTH)
    {
        for (int i = this->m_usedCrossSections_WE.size() - 1; i >= 0; i--)
        {
            if (this->m_usedCrossSections_WE[i] > this->m_crossSection)
            {
                response = this->m_usedCrossSections_WE[i];
            }
        }
    }
    else if (this->m_direction == Direction::Value::WIDTH)
    {
        for (int i = this->m_usedCrossSections_NS.size() - 1; i >= 0; i--)
        {
            if (this->m_usedCrossSections_NS[i] > this->m_crossSection)
            {
                response = this->m_usedCrossSections_NS[i];
            }
        }
    }
    else if (this->m_direction == Direction::Value::HEIGHT)
    {
        for (int i = this->m_usedCrossSections_MAP.size() - 1; i >= 0; i--)
        {
            if (this->m_usedCrossSections_MAP[i] > this->m_crossSection)
            {
                response = this->m_usedCrossSections_MAP[i];
            }
        }
    }

    return response;
}


void CanvasHandler::addCrossSectionUsed(int csID, int direction)
{
    if (direction == Direction::Value::LENGTH)
    {
        //- Add the ID to the collection
        this->m_usedCrossSections_WE.push_back(csID);

        //- Sort the collection in ascending order
        std::sort(this->m_usedCrossSections_WE.begin(), this->m_usedCrossSections_WE.end());

        //- Erase duplicate values
        std::vector<int>::iterator it;
        it = std::unique(this->m_usedCrossSections_WE.begin(), this->m_usedCrossSections_WE.end());
        this->m_usedCrossSections_WE.resize(std::distance(this->m_usedCrossSections_WE.begin(), it));
    }
    else if (direction == Direction::Value::WIDTH)
    {
        //- Add the ID to the collection
        this->m_usedCrossSections_NS.push_back(csID);

        //- Sort the collection in ascending order
        std::sort(this->m_usedCrossSections_NS.begin(), this->m_usedCrossSections_NS.end());

        //- Erase duplicate values
        std::vector<int>::iterator it;
        it = std::unique(this->m_usedCrossSections_NS.begin(), this->m_usedCrossSections_NS.end());
        this->m_usedCrossSections_NS.resize(std::distance(this->m_usedCrossSections_NS.begin(), it));
    }
    else if (direction == Direction::Value::HEIGHT)
    {
        //- Add the ID to the collection
        this->m_usedCrossSections_MAP.push_back(csID);

        //- Sort the collection in ascending order
        std::sort(this->m_usedCrossSections_MAP.begin(), this->m_usedCrossSections_MAP.end());

        //- Erase duplicate values
        std::vector<int>::iterator it;
        it = std::unique(this->m_usedCrossSections_MAP.begin(), this->m_usedCrossSections_MAP.end());
        this->m_usedCrossSections_MAP.resize(std::distance(this->m_usedCrossSections_MAP.begin(), it));
    }    
}

void CanvasHandler::loadCrossSectionUsed(QJsonArray file)
{
    for(int i=0; i<file.size(); i++) {
        QJsonObject jsonObj = file.at(i).toObject();
        double csID, direction;
        csID = jsonObj.value("cross_section_id").toDouble();
        direction = jsonObj.value("direction_id").toDouble();
        this->addCrossSectionUsed(csID, direction);
    }
}

void CanvasHandler::resetHighlightSelection_FV()
{
    this->m_highlight_FV = -1;
}


void CanvasHandler::resetHighlightSelection_MV()
{
    this->m_highlight_MV = -1;
    this->m_highlight_TR = -1;
}


void CanvasHandler::delegateFrontViewAction(int buttonID, int buttonAction, double mouse_x, double mouse_y)
{    
    double model_x = 0.0;
    double model_y = 0.0;

    std::vector<double> sketch;
    std::vector<double> screenCurve;

    int surfaceID = 1;

    //- Get instance of the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);
    std::vector<int> index = Model3dHandler::getInstance(nullptr)->getSurfaceIndices();

    if ((this->m_canvasAction == CanvasAction::Value::FB_SKETCH && buttonAction == Mouse::Action::PRESS)
        || this->m_canvasAction == CanvasAction::Value::FB_SKETCH_LINE
        || this->m_canvasAction == CanvasAction::Value::FB_SKETCH_CIRCLE
        || this->m_canvasAction == CanvasAction::Value::FB_SKETCH_ELLIPSE)
    {
        if (index.size())
            surfaceID = index[index.size() - 1] + 1;
    }
    
    //- Sketch inside Front View WE
    if (this->m_direction == Direction::Value::LENGTH)
    {
        this->fromFrontViewWEToModel(mouse_x, mouse_y, model_x, model_y);

        //- Sketch Region
        if (this->m_canvasAction == CanvasAction::Value::TB_START_SKETCH_REGION || 
            this->m_canvasAction == CanvasAction::Value::TB_STOP_SKETCH_REGION)
        {
            this->delegateSketchRegionAction(buttonID, buttonAction, model_x, model_y);
        }
        //- Flattening
        else if (this->m_canvasAction == CanvasAction::Value::TB_START_FLATTENING ||
            this->m_canvasAction == CanvasAction::Value::TB_STOP_FLATTENING)
        {
            this->delegateFlatteningAction(buttonID, buttonAction, model_x, model_y);
        }
        //- Sketch Surface
        else {
            this->sketchHandler->setSketchedPoint(model_x, model_y);
            this->sketchHandler->setSurfaceID(surfaceID);
            this->sketchHandler->setSketchLineLength(box_w);
            this->sketchHandler->delegateSketchAction(buttonID, buttonAction, 
                this->m_canvasAction, this->m_crossSection, this->m_direction);
        }        
    }
    //- Sketch inside Front View NS
    else if (this->m_direction == Direction::Value::WIDTH) {
        this->fromFrontViewNSToModel(mouse_x, mouse_y, model_x, model_y); 

        //- Reverse NS coordinates
        if(NS_REVERSED) model_x = box_l - model_x;

        //- Sketch Region
        if (this->m_canvasAction == CanvasAction::Value::TB_START_SKETCH_REGION ||
            this->m_canvasAction == CanvasAction::Value::TB_STOP_SKETCH_REGION)
        {
            this->delegateSketchRegionAction(buttonID, buttonAction, model_x, model_y);
        }
        //- Flattening
        else if (this->m_canvasAction == CanvasAction::Value::TB_START_FLATTENING ||
                 this->m_canvasAction == CanvasAction::Value::TB_STOP_FLATTENING)
        {
            this->delegateFlatteningAction(buttonID, buttonAction, model_x, model_y);
        }
        //- Sketch Surface
        else {
            this->sketchHandler->setSketchedPoint(model_x, model_y);
            this->sketchHandler->setSurfaceID(surfaceID);
            this->sketchHandler->setSketchLineLength(box_l);
            this->sketchHandler->delegateSketchAction(buttonID, buttonAction,
            this->m_canvasAction, this->m_crossSection, this->m_direction);
        }
    }
    //- Sketch inside Map View MAP
    else if (this->m_direction == Direction::Value::HEIGHT)
    {
        //- Sketch Region
        if (this->m_canvasAction == CanvasAction::Value::TB_STOP_SKETCH_REGION)
        {
            this->delegateSketchRegionAction(buttonID, buttonAction, 0.0, 0.0);
        }
    }
}


void CanvasHandler::delegateMapViewAction(int buttonID, int buttonAction, double mouse_x, double mouse_y, double radiusX, double radiusY)
{
    double model_x = 0.0;
    double model_y = 0.0;

    int surfaceID = 1;

    //- Get instance of the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);

    if ((this->m_canvasAction == CanvasAction::Value::FB_SKETCH && buttonAction == Mouse::Action::PRESS)
        || this->m_canvasAction == CanvasAction::Value::FB_SKETCH_LINE
        || this->m_canvasAction == CanvasAction::Value::FB_SKETCH_CIRCLE
        || this->m_canvasAction == CanvasAction::Value::FB_SKETCH_ELLIPSE)
    {
        //- Get the surface indices from the Model3D
        std::vector<int> index = Model3dHandler::getInstance(nullptr)->getSurfaceIndices();

        if (index.size())
        {
            if (this->m_direction == Direction::Value::HEIGHT)
                surfaceID = index[index.size() - 1] + 1;
            else
                surfaceID = index[index.size() - 1];
        }
    }

    //- Sketch inside Map View
    if (this->m_direction == Direction::Value::HEIGHT)
    {
        this->fromMapViewToModel(mouse_x, mouse_y, model_x, model_y);
        this->sketchHandler->setSketchedPoint(model_x, model_y);
        this->sketchHandler->setSketchLineLength(box_w);
        this->sketchHandler->setSketchCircleRadiusX(radiusX);
        this->sketchHandler->setSketchCircleRadiusY(radiusY);
        this->sketchHandler->setSurfaceID(surfaceID);
        this->sketchHandler->delegateSketchAction(buttonID, buttonAction, 
            this->m_canvasAction, this->m_crossSection, this->m_direction);
    }
    //- Trajectory inside Map View
    else {
        this->fromMapViewToModel(mouse_x, mouse_y, model_x, model_y);
        this->sketchHandler->setSketchedPoint(model_x, model_y);
        this->sketchHandler->setSurfaceID(surfaceID);
        this->sketchHandler->delegateTrajectoryAction(buttonID, buttonAction, 
            this->m_canvasAction, this->m_crossSection, this->m_direction);
    }
}


void CanvasHandler::delegateFrontViewVisualization(int action)
{
    this->sketchHandler->delegateSketchAction(action, this->m_crossSection, this->m_direction);
}


void CanvasHandler::delegateMapViewVisualization(int action)
{
    //- Sketch inside Map View
    if (this->m_direction == Direction::Value::HEIGHT) {
        this->sketchHandler->delegateSketchAction(action, this->m_crossSection, this->m_direction);
    }
    //- Trajectory inside Map View
    else {
        this->sketchHandler->delegateTrajectoryAction(action, this->m_crossSection, this->m_direction);
    }
}


void CanvasHandler::delegateSketchRegionAction(int buttonID, int buttonAction, double model_x, double model_y)
{
    if(this->m_canvasAction == CanvasAction::Value::TB_START_SKETCH_REGION)
    {
        if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::PRESS)
        {
            if (this->m_isSketchRegion)
            {
                this->resetSketchRegion();
                this->m_isSketchRegion = false;
            }
            this->sketchRegion.clear();
            this->sketchRegion.push_back(model_x);
            this->sketchRegion.push_back(model_y);
        }

        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::MOVE)
        {
            this->sketchRegion.push_back(model_x);
            this->sketchRegion.push_back(model_y);
        }

        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::RELEASE)
        {
            if (!this->m_isSketchRegion)
            {
                this->preserveRegionBoundary();
            }
        }

        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::PRESS_AND_HOLD)
        {
            Model3dHandler::getInstance(nullptr)->stopPreserveRegion();
            this->m_isSketchRegion = true;
            this->resetSketchRegion();
            this->preserveRegionBoundary(model_x, model_y);
        }

        else if (buttonID == Mouse::Button::RIGHT && buttonAction == Mouse::Action::DOUBLE_CLICK)
        {
            //this->resetSketchRegion();
            this->sketchRegion.clear();
            //this->m_isSketchRegion = false;
        }
    }

    else if (this->m_canvasAction == CanvasAction::Value::TB_STOP_SKETCH_REGION)
    {
        Model3dHandler::getInstance(nullptr)->stopPreserveRegion();
        this->resetSketchRegion();
        this->sketchRegion.clear();
        this->m_isSketchRegion = false;
    }

    //- Update the visualization of the Sketch in the QML side
    std::vector<double> screenCurve;

    if (this->m_direction == Direction::Value::LENGTH)
    {
        screenCurve = this->fromModelToFrontViewWE(this->sketchRegion);
        if (buttonID == Mouse::Button::LEFT && (buttonAction == Mouse::Action::RELEASE || buttonAction == Mouse::Action::PRESS_AND_HOLD))
        {
            QString screenRegion = this->prepareJsonObject(this->fromModelToFrontViewWE(this->pr_boundary));
            emit(getUpdatedPolygonCorners(screenRegion));
        }
    }
    else if (this->m_direction == Direction::Value::WIDTH)
    {
        screenCurve = this->fromModelToFrontViewNS(this->sketchRegion);
        if (buttonID == Mouse::Button::LEFT && (buttonAction == Mouse::Action::RELEASE || buttonAction == Mouse::Action::PRESS_AND_HOLD))
        {
            QString screenRegion = this->prepareJsonObject(this->fromModelToFrontViewNS(this->pr_boundary));
            emit(getUpdatedPolygonCorners(screenRegion));
        }
    }

    QString response = this->prepareJsonObject(screenCurve);
    emit(getUpdatedSketchRegion(response));
}

void CanvasHandler::delegateRegionAction(int canvasID, int buttonID, int buttonAction, double mouse_x, double mouse_y)
{
    double model_x = 0.0;
    double model_y = 0.0;

    //- Get size info from the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler* model3dHandler = Model3dHandler::getInstance(nullptr);
    model3dHandler->model().GetSize(box_w, box_l, box_h);

    std::vector<double> regionList = model3dHandler->getRegionList();
    int regionSize = model3dHandler->getNumberOfRegions();

    QString regionName;
    QString regionDescription;
    QString regionColor;
    QString domainName;
    QString domainDescription;
    QString domainColor;

    //- Method called from FrontView
    if (canvasID == CanvasID::Value::FRONT_VIEW)
    {
        //- Click inside Front View WE
        if (this->m_direction == Direction::Value::LENGTH)
        {
            this->fromFrontViewWEToModel(mouse_x, mouse_y, model_x, model_y);

            //- Prepare a 3D coordinate to check if the preserve region is possible
            std::vector<double> p;
            p.push_back(model_x);
            p.push_back(this->m_cs_Position_L);
            p.push_back(model_y);

            if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK)
            {
                if (this->isClickInsideDomain(model_x, model_y, box_w, box_h))
                {
                    std::vector<int> regions;
                    if (model3dHandler->getVolumeAttributesFromPointList(p, regions))
                    {
                        model3dHandler->getRegionMetadataById(regions[0], regionName, regionDescription, regionColor);
                        int domain_id = model3dHandler->getDomainIdByRegion(regions[0]);
                        if (domain_id > -1) {
                            model3dHandler->getDomainMetadataById(domain_id, domainName, domainDescription, domainColor);
                        }
                        QString regionVolume = QString::number(model3dHandler->getRegionVolumeProportion(regions[0]), 'G', 3);
                        QString domainVolume = QString::number(model3dHandler->getDomainVolumeProportion(domain_id), 'G', 3);
                        emit regionClicked(regions[0], regionSize, regionName, regionVolume, regionColor,
                            domain_id, domainName, domainVolume, domainColor);
                    }
                }
            }
        }
        //- Click inside Front View NS
        else if (this->m_direction == Direction::Value::WIDTH)
        {
            this->fromFrontViewNSToModel(mouse_x, mouse_y, model_x, model_y);

            //- Prepare a 3D coordinate to check if the preserve region is possible
            std::vector<double> p;
            p.push_back(this->m_cs_Position_W);
            p.push_back(model_x);
            p.push_back(model_y);

            if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK)
            {                
                if (this->isClickInsideDomain(model_x, model_y, box_l, box_h))
                {
                    std::vector<int> regions;
                    if (model3dHandler->getVolumeAttributesFromPointList(p, regions))
                    {
                        model3dHandler->getRegionMetadataById(regions[0], regionName, regionDescription, regionColor);
                        int domain_id = model3dHandler->getDomainIdByRegion(regions[0]);
                        if (domain_id > -1) {
                            model3dHandler->getDomainMetadataById(domain_id, domainName, domainDescription, domainColor);
                        }
                        QString regionVolume = QString::number(model3dHandler->getRegionVolumeProportion(regions[0]), 'G', 3);
                        QString domainVolume = QString::number(model3dHandler->getDomainVolumeProportion(domain_id), 'G', 3);
                        emit regionClicked(regions[0], regionSize, regionName, regionVolume, regionColor,
                            domain_id, domainName, domainVolume, domainColor);
                    }
                }
            }
        }
    }    
}

bool CanvasHandler::isClickInsideDomain(double model_x, double model_y, double dimX, double dimY)
{
    //- Verify X dimension
    if (model_x >= 0.0 && model_x <= dimX)
    {
        //- Verify Y dimension
        if (model_y >= 0.0 && model_y <= dimY)
        {
            return true;
        }
    }

    return false;
}


void CanvasHandler::delegateFlatteningAction(int buttonID, int buttonAction, double model_x, double model_y)
{
    if (this->m_canvasAction == CanvasAction::Value::TB_START_FLATTENING)
    {
        if (buttonID == Mouse::Button::NO_BUTTON && buttonAction == Mouse::Action::MOVE)
        {
            int i = findSurfaceIDtoFlatt(model_x, model_y);
            emit(highlightSurfaceCreated(i));
        }
        else if (buttonID == Mouse::Button::LEFT && buttonAction == Mouse::Action::CLICK) {
            int i = findSurfaceIDtoFlatt(model_x, model_y);
            Model3dHandler::getInstance(nullptr)->FlattenSurface(i, model_y);
            
            double minH, maxH;
            Model3dHandler::getInstance(nullptr)->ComputeBBoxHeights(minH, maxH);
            this->m_Flat_Height = maxH - minH;
            this->m_Flat_delta = minH;
            emit(updateBBoxFlattening(this->m_Flat_Height));
            emit(refreshSubmittedCurves());  

            // To let Object tree know which surface is being flattened
            emit(flatteningStarted(i, this->m_Flat_Height));
        }
        else if (buttonID == Mouse::Button::RIGHT && buttonAction == Mouse::Action::DOUBLE_CLICK) {
            //this->m_Flat_delta = 0.0;
        }
    }
    else if (this->m_canvasAction == CanvasAction::Value::TB_STOP_FLATTENING) 
    {
        Model3dHandler::getInstance(nullptr)->StopFlattening();
        this->m_Flat_Height = 0.0;
        emit(updateBBoxFlattening(0.0));
        emit(refreshSubmittedCurves());

        // To let Object tree know that flattening has stopped
        emit(flatteningStopped());
    }
}

int CanvasHandler::findSurfaceIDtoFlatt(double model_x, double model_y)
{
    //- Get instance of the 3D Model
    int width_Discretization, length_Discretization;
    Model3dHandler::getInstance(nullptr)->getModelDiscretization(width_Discretization, length_Discretization);

    //- Get the surface indices from the Model3D
    std::vector<int> index = Model3dHandler::getInstance(nullptr)->getSurfaceIndices();

    double minimumDist = 100000.0;
    Curve2D submittedCurve;
    int response = -1;

    for (unsigned int i = 0; i < index.size(); i++)
    {
        submittedCurve.clear();

        std::vector<double> curve;
        std::vector<double> eachCurve;
        std::vector<std::size_t> indices;
        std::vector<std::size_t> segments;

        int crossSection_ID;
        if (this->m_isLength)
        {
            crossSection_ID = length_Discretization - this->m_crossSection;
            Model3dHandler::getInstance(nullptr)->getFlattenedLengthCrossSectionCurve(index[i], crossSection_ID, curve, indices);
        }
        else {
            crossSection_ID = this->m_crossSection;
            Model3dHandler::getInstance(nullptr)->getFlattenedWidthCrossSectionCurve(index[i], crossSection_ID, curve, indices);
        }
        
        for (int p = 0; p < curve.size(); p+=2)
        {
            Point2D point(curve[p], curve[p + 1]);
            submittedCurve.add(point);
        }

        double dist;
        dist = submittedCurve.distanceTo(Point2D(model_x, model_y));
        if (dist < 20 && dist < minimumDist)
        {
            minimumDist = dist;
            response = index[i];
        }
    }
    return response;
}


void CanvasHandler::updateSketchDataStructure()
{
    this->sketchHandler->updateSketchDataStructure();
}

void CanvasHandler::futureSubmitPreview()
{
    QFuture<void> future = QtConcurrent::run(this, &CanvasHandler::submitPreview);
    QFutureWatcher<void> watcher;
    //connect(&watcher, SIGNAL(finished()), this, SIGNAL(modelLoadingCompleted()));
    watcher.setFuture(future);
    watcher.waitForFinished();

    if (watcher.isCanceled())
    {
        //qDebug() << "CanvasHandler::submitPreview(): canceled!!!";
    }
    else
    {
        //qDebug() << "CanvasHandler::submitPreview(): finished!!!";
        emit(previewSubmitted());
    }
}

void CanvasHandler::futureSubmitSketch()
{
    QFuture<void> future = QtConcurrent::run(this, &CanvasHandler::submitSketch);
    QFutureWatcher<void> watcher;
    //connect(&watcher, SIGNAL(finished()), this, SIGNAL(modelLoadingCompleted()));
    watcher.setFuture(future);
    watcher.waitForFinished();

    if (watcher.isCanceled())
    {
        //qDebug() << "CanvasHandler::submitSketch(): canceled!!!";
    }
    else
    {
        //qDebug() << "CanvasHandler::submitSketch(): finished!!!";
        emit(sketchSubmitted());
    }
}

bool CanvasHandler::submitSketch()
{
    QElapsedTimer timer;
    timer.start();

    bool submit = false;
    bool isExtruded = false;
    std::vector<double> points;

    std::vector<double> path;
    int surfaceID = 0;
    int csID = -1;

    //- Get instance of the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);
    int width_Discretization, length_Discretization;
    Model3dHandler::getInstance(nullptr)->getModelDiscretization(width_Discretization, length_Discretization);

    //- For the FrontView WE:
    if (m_direction == Direction::Value::LENGTH)
    {
        submit = sketchHandler->submitSketch(isExtruded, points, path, box_l, length_Discretization, surfaceID, csID, m_direction);
        
        if (submit)
        {
            //- Create the surface in the model
            //- Verify if there is only one sketch to create the surface:
            if (isExtruded)
            {
                //- Verify if there is a trajectory to be used:
                if (path.size())
                {
                    int crossSection_ID = length_Discretization - csID;
                    this->m_cs_Position_L = static_cast<double>(((crossSection_ID)*box_l) / length_Discretization);
                    //- Verify if Path Guided Extrusion is Active
                    if (this->m_pgeIsActive)
                    {
                        Model3dHandler::getInstance(nullptr)->CreateFlattenedLengthPathGuidedSurface(surfaceID, points, this->m_cs_Position_L, path);                       
                    }
                    else {
                        Model3dHandler::getInstance(nullptr)->CreateFlattenedLengthExtrudedSurface(surfaceID, points, this->m_cs_Position_L, path);                        
                    }
                    this->sketchTrajectories->cleanData();
                    this->sketchTrajectories->saveCurve(path, csID, surfaceID);
                    if (trajectoryTemplates->saveSketchTemplate(path)) {
                        emit(updateTrajectoryTemplatesList());
                    }
                }
                else {
                    Model3dHandler::getInstance(nullptr)->CreateFlattenedLengthExtrudedSurface(surfaceID, points);                  
                }                
            }
            else
            {
                Model3dHandler::getInstance(nullptr)->CreateFlattenedSurface(surfaceID, points);                
            }            
        }
    }
    //- For the FrontView NS:
    else if (m_direction == Direction::Value::WIDTH)
    {
        submit = sketchHandler->submitSketch(isExtruded, points, path, box_w, width_Discretization, surfaceID, csID, m_direction);

        if (submit)
        {
            //- Create the surface in the model
            //- Verify if there is only one sketch to create the surface:
            if (isExtruded)
            {
                //- Verify if there is a trajectory to be used:
                if (path.size())
                {
                    int crossSection_ID = csID;
                    this->m_cs_Position_W = static_cast<double>(((crossSection_ID)*box_w) / width_Discretization);
                    //- Verify if Path Guided Extrusion is Active
                    if (this->m_pgeIsActive)
                    {
                        Model3dHandler::getInstance(nullptr)->CreateFlattenedWidthPathGuidedSurface(surfaceID, points, this->m_cs_Position_W, path);
                    }
                    else {
                        Model3dHandler::getInstance(nullptr)->CreateFlattenedWidthExtrudedSurface(surfaceID, points, this->m_cs_Position_W, path);
                    }
                    this->sketchTrajectories->cleanData();
                    this->sketchTrajectories->saveCurve(path, csID, surfaceID);
                    if (trajectoryTemplates->saveSketchTemplate(path)) {
                        emit(updateTrajectoryTemplatesList());
                    }
                }
                else {
                    Model3dHandler::getInstance(nullptr)->CreateFlattenedWidthExtrudedSurface(surfaceID, points);
                }
            }
            else
            {
                Model3dHandler::getInstance(nullptr)->CreateFlattenedSurface(surfaceID, points);
            }
        }
    }
    //- For the MapView
    else if (this->m_direction == Direction::Value::HEIGHT)
    {
        submit = this->sketchHandler->submitSketchContour(points, box_h, length_Discretization, surfaceID);        

        if (submit)
        {
            //- Update the selected flag
            emit(selectSketchMV(false));

            //- Update contours
            this->updateContours();

            //- Create the surface in the model
            Model3dHandler::getInstance(nullptr)->CreateFlattenedSurface(surfaceID, points);
        }
    }

    if (submit)
    {
        if (gui_surfaceType == 1)
            Model3dHandler::getInstance(nullptr)->surfaceIsStructure(surfaceID);
        else Model3dHandler::getInstance(nullptr)->surfaceIsStratigraphy(surfaceID);

        //- Updating the local surface indice list inside Model3dHandler
        Model3dHandler::getInstance(nullptr)->setSurfaceIndices();
        
        //Setting Default Group Id
        Model3dHandler::getInstance(nullptr)->setSurfaceGroup(surfaceID, -1);        
    }

    this->m_isSubmitAllowed = false;
    emit(enableDirection());
    this->updateIsSurfaceCreated();
    
    return submit;
}

bool CanvasHandler::submitSketch(bool preview)
{
    bool submit = false;
    bool isExtruded = false;
    std::vector<double> points;

    std::vector<double> path;
    int surfaceID = 0;
    int csID = -1;

    //- Get instance of the 3D Model    
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);
    int width_Discretization, length_Discretization;
    Model3dHandler::getInstance(nullptr)->getModelDiscretization(width_Discretization, length_Discretization);

    ////qDebug() << "\n Submit: CS " << m_crossSection << ", Pos: " << length_Discretization << " - " << m_crossSection << " = " << length_Discretization - m_crossSection;

    //- For the FrontView WE:
    if (m_direction == Direction::Value::LENGTH)
    {
        //- Verify if there is sketch to be submitted:
        if (preview)
        {
            submit = sketchHandler->submitPreview(isExtruded, points, path, box_l, length_Discretization, surfaceID, csID, m_direction);
        }
        else
        {
            submit = sketchHandler->submitSketch(isExtruded, points, path, box_l, length_Discretization, surfaceID, csID, m_direction);
            emit(enableDirection());
        }

        if (submit)
        {
            //- Create the surface in the model
            //- Verify if there is only one sketch to create the surface:
            if (isExtruded)
            {
                //- Verify if there is a trajectory to be used:
                if (path.size())
                {
                    //- Verify if Path Guided Extrusion is Active
                    if (this->m_pgeIsActive)
                    {
                        Model3dHandler::getInstance(nullptr)->CreateFlattenedLengthPathGuidedSurface(surfaceID, points, m_cs_Position_L, path);
                    }
                    else {
                        Model3dHandler::getInstance(nullptr)->CreateFlattenedLengthExtrudedSurface(surfaceID, points, m_cs_Position_L, path);
                    }
                    if(!preview)
                    {
                        this->sketchTrajectories->cleanData();
                        this->sketchTrajectories->saveCurve(path, this->m_crossSection, surfaceID);
                        if (trajectoryTemplates->saveSketchTemplate(path)) {
                            emit(updateTrajectoryTemplatesList());
                        }
                    }
                }
                else {
                    Model3dHandler::getInstance(nullptr)->CreateFlattenedLengthExtrudedSurface(surfaceID, points);
                }
            }
            else
            {
                Model3dHandler::getInstance(nullptr)->CreateFlattenedSurface(surfaceID, points);
            }
        }
    }
    //- For the FrontView NS:
    else if (m_direction == Direction::Value::WIDTH)
    {
        //- Verify if there is sketch to be submitted:
        if (preview)
        {
            submit = sketchHandler->submitPreview(isExtruded, points, path, box_w, width_Discretization, surfaceID, csID, m_direction);
        }
        else
        {
            submit = sketchHandler->submitSketch(isExtruded, points, path, box_w, width_Discretization, surfaceID, csID, m_direction);
            emit(enableDirection());
        }

        if (submit)
        {
            //- Create the surface in the model
            //- Verify if there is only one sketch to create the surface:
            if (isExtruded)
            {
                //- Verify if there is a trajectory to be used:
                if (path.size())
                {
                    //- Verify if Path Guided Extrusion is Active
                    if (this->m_pgeIsActive)
                    {
                        Model3dHandler::getInstance(nullptr)->CreateFlattenedWidthPathGuidedSurface(surfaceID, points, m_cs_Position_W, path);
                    }
                    else {
                        Model3dHandler::getInstance(nullptr)->CreateFlattenedWidthExtrudedSurface(surfaceID, points, m_cs_Position_W, path);
                    }
                    if (!preview)
                    {
                        this->sketchTrajectories->cleanData();
                        this->sketchTrajectories->saveCurve(path, this->m_crossSection, surfaceID);
                        if (trajectoryTemplates->saveSketchTemplate(path)) {
                            emit(updateTrajectoryTemplatesList());
                        }
                    }
                }
                else {
                    Model3dHandler::getInstance(nullptr)->CreateFlattenedWidthExtrudedSurface(surfaceID, points);
                }
            }
            else
            {
                Model3dHandler::getInstance(nullptr)->CreateFlattenedSurface(surfaceID, points);
            }
        }
    }
    //- For the MapView
    else if (this->m_direction == Direction::Value::HEIGHT)
    {
        submit = this->sketchHandler->submitSketchContour(points, box_h, length_Discretization, surfaceID);
        emit(enableDirection());

        if (submit)
        {
            //- Update the selected flag
            emit(selectSketchMV(false));

            //- Create the surface in the model
            Model3dHandler::getInstance(nullptr)->CreateFlattenedSurface(surfaceID, points);
        }        
    }

    if (submit)
    {
        if (gui_surfaceType == 1)
            Model3dHandler::getInstance(nullptr)->surfaceIsStructure(surfaceID);
        else Model3dHandler::getInstance(nullptr)->surfaceIsStratigraphy(surfaceID);

        //- Updating the local surface indice list inside Model3dHandler
        Model3dHandler::getInstance(nullptr)->setSurfaceIndices();
    }    

    this->m_isSubmitAllowed = false;
    this->updateIsSurfaceCreated();
    return submit;
}


bool CanvasHandler::submitPreview()
{
    bool preview = true;
    bool submit = false;
    bool isExtruded = false;
    std::vector<double> points;

    std::vector<double> path;
    int surfaceID = 0;
    int csID = -1;

    //- Get instance of the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);
    int width_Discretization, length_Discretization;
    Model3dHandler::getInstance(nullptr)->getModelDiscretization(width_Discretization, length_Discretization);

    //- For the FrontView WE:
    if (m_direction == Direction::Value::LENGTH)
    {
        if (preview)
        {
            submit = sketchHandler->submitPreview(isExtruded, points, path, box_l, length_Discretization, surfaceID, csID, m_direction);

            if (submit)
            {
                //- Create the surface in the model
                //- Verify if there is only one sketch to create the surface:
                if (isExtruded)
                {
                    //- Verify if there is a trajectory to be used:
                    if (path.size())
                    {
                        int crossSection_ID = length_Discretization - csID;
                        this->m_cs_Position_L = static_cast<double>(((crossSection_ID)*box_l) / length_Discretization);
                        //- Verify if Path Guided Extrusion is Active
                        if (this->m_pgeIsActive)
                        {
                            Model3dHandler::getInstance(nullptr)->CreateFlattenedLengthPathGuidedSurface(surfaceID, points, m_cs_Position_L, path);
                        }
                        else {
                            Model3dHandler::getInstance(nullptr)->CreateFlattenedLengthExtrudedSurface(surfaceID, points, m_cs_Position_L, path);
                        }
                    }
                    else {
                        Model3dHandler::getInstance(nullptr)->CreateFlattenedLengthExtrudedSurface(surfaceID, points);
                    }
                }
                else
                {
                    Model3dHandler::getInstance(nullptr)->CreateFlattenedSurface(surfaceID, points);
                }
            }
        }
    }
    //- For the FrontView NS:
    else if (m_direction == Direction::Value::WIDTH)
    {
        if (preview)
        {
            submit = sketchHandler->submitPreview(isExtruded, points, path, box_w, width_Discretization, surfaceID, csID, m_direction);

            if (submit)
            {
                //- Create the surface in the model
                //- Verify if there is only one sketch to create the surface:
                if (isExtruded)
                {
                    //- Verify if there is a trajectory to be used:
                    if (path.size())
                    {
                        int crossSection_ID = csID;
                        this->m_cs_Position_W = static_cast<double>(((crossSection_ID)*box_w) / width_Discretization);
                        //- Verify if Path Guided Extrusion is Active
                        if (this->m_pgeIsActive)
                        {
                            Model3dHandler::getInstance(nullptr)->CreateFlattenedWidthPathGuidedSurface(surfaceID, points, m_cs_Position_W, path);
                        }
                        else {
                            Model3dHandler::getInstance(nullptr)->CreateFlattenedWidthExtrudedSurface(surfaceID, points, m_cs_Position_W, path);
                        }
                    }
                    else {
                        Model3dHandler::getInstance(nullptr)->CreateFlattenedWidthExtrudedSurface(surfaceID, points);
                    }
                }
                else
                {
                    Model3dHandler::getInstance(nullptr)->CreateFlattenedSurface(surfaceID, points);
                }
            }
        }
    }
    //- For the MapView
    else if (this->m_direction == Direction::Value::HEIGHT)
    {
        if (preview)
        {
            submit = this->sketchHandler->submitSketchContourPreview(points, box_h, length_Discretization, surfaceID);
            if (submit)
            {
                //- Create the surface in the model
                Model3dHandler::getInstance(nullptr)->CreateFlattenedSurface(surfaceID, points);
            }
        }
    }

    if (submit)
    {
        if (gui_surfaceType == 1)
            Model3dHandler::getInstance(nullptr)->surfaceIsStructure(surfaceID);
        else Model3dHandler::getInstance(nullptr)->surfaceIsStratigraphy(surfaceID);

        //- Updating the local surface indice list inside Model3dHandler
        Model3dHandler::getInstance(nullptr)->setSurfaceIndices();
    }
    
    return submit;
}


QString CanvasHandler::returnPreview(int canvasID)
{
    QJsonArray curves_array;

    //- Get instance of the 3D Model
    int width_Discretization, length_Discretization;
    Model3dHandler::getInstance(nullptr)->getModelDiscretization(width_Discretization, length_Discretization);

    //- Get the surface indices from the Model3D
    std::vector<int> index = Model3dHandler::getInstance(nullptr)->getSurfaceIndices();

    std::vector<double> eachCurve;

    //- For the FrontView WE:
    if (canvasID == CanvasID::Value::FRONT_VIEW && m_direction == Direction::Value::LENGTH)
    {
        if (this->m_isPreview)
        {
            //- Preview is the last surface inside the model
            if (index.size())
            {
                unsigned int i = index.size() - 1;

                std::vector<double> curve;
                std::vector<std::size_t> indices;
                std::vector<std::size_t> segments;

                int crossSection_ID = length_Discretization - m_crossSection;
                Model3dHandler::getInstance(nullptr)->getFlattenedLengthCrossSectionCurve(index[i], crossSection_ID, curve, indices);
                eachCurve = fromModelToFrontViewWE(curve);
            }
        }
    }
    //- For the FrontView NS:
    else if (canvasID == CanvasID::Value::FRONT_VIEW && m_direction == Direction::Value::WIDTH)
    {
        if (this->m_isPreview)
        {
            //- Preview is the last surface inside the model
            if (index.size())
            {
                unsigned int i = index.size() - 1;

                std::vector<double> curve;
                std::vector<std::size_t> indices;
                std::vector<std::size_t> segments;

                int crossSection_ID = m_crossSection;
                Model3dHandler::getInstance(nullptr)->getFlattenedWidthCrossSectionCurve(index[i], crossSection_ID, curve, indices);
                eachCurve = fromModelToFrontViewNS(curve);
            }
        }
    }//endif

    QString response = prepareJsonObject(eachCurve);
    return response;
}


QString CanvasHandler::getSubmittedCurves(int canvasID)
{
    QJsonArray curves_array;

    //- Get instance of the 3D Model
    int width_Discretization, length_Discretization;
    Model3dHandler::getInstance(nullptr)->getModelDiscretization(width_Discretization, length_Discretization);

    //- Get the surface indices from the Model3D
    std::vector<int> index = Model3dHandler::getInstance(nullptr)->getSurfaceIndices();

    //- For the FrontView:
    if (canvasID == CanvasID::Value::FRONT_VIEW)
    {
        for (unsigned int i = 0; i < index.size(); i++)
        {
            std::vector<double> curve;
            std::vector<double> eachCurve;
            std::vector<std::size_t> indices;
            std::vector<std::size_t> segments;

            int crossSection_ID;
            if (this->m_isLength)
            {
                crossSection_ID = length_Discretization - this->m_crossSection;
                Model3dHandler::getInstance(nullptr)->getFlattenedLengthCrossSectionCurve(index[i], crossSection_ID, curve, indices);
                eachCurve = this->fromModelToFrontViewWE(curve);
            }
            else {
                crossSection_ID = this->m_crossSection;
                Model3dHandler::getInstance(nullptr)->getFlattenedWidthCrossSectionCurve(index[i], crossSection_ID, curve, indices);
                eachCurve = this->fromModelToFrontViewNS(curve);
            }

            //- Prepare the segments array            
            if (indices.size())
            {
                //- Store first indice
                segments.push_back(indices[0]);
                for (int ind = 0; ind < indices.size() - 2; ind += 2)
                {
                    if (indices[ind + 1] != indices[ind + 2])
                    {
                        segments.push_back(indices[ind + 1]);
                        segments.push_back(indices[ind + 2]);
                    }
                }
                //- Store the last indice
                segments.push_back(indices[indices.size() - 1]);
            }

            //- Prepare the Json data
            QJsonArray segment_array;
            QJsonObject segment_object;
            for (int seg = 0; seg < segments.size(); seg += 2)
            {
                
                QJsonArray plot_array;

                QString x_str("x");
                QString y_str("y");

                for (int j = segments[seg]; j <= segments[seg+1]; j++)
                {
                    QJsonObject pointObject;

                    pointObject.insert(x_str, QJsonValue(eachCurve[2*j]));
                    pointObject.insert(y_str, QJsonValue(eachCurve[2*j + 1]));

                    plot_array.push_back(QJsonValue(pointObject));
                }
                segment_array.push_back(plot_array);

                
            }//end: for seg

            segment_object.insert(QString("index"), QString::number(i));
            segment_object.insert(QString("segments"), QJsonValue(segment_array));

            curves_array.push_back(segment_object);

            curve.clear();
            eachCurve.clear();
            segments.clear();
            indices.clear();
        }//end: for i
    }//endif

    QJsonObject final_object;
    final_object.insert(QString("curves_array"), QJsonValue(curves_array));
    QJsonDocument jsonDoc(final_object);

    QString json_string = jsonDoc.toJson();
    return json_string;            
}

std::vector<double> CanvasHandler::getCurve(int surfaceID)
{
    //- Get instance of the 3D Model
    int width_Discretization, length_Discretization;
    Model3dHandler::getInstance(nullptr)->getModelDiscretization(width_Discretization, length_Discretization);

    std::vector<double> curve;
    std::vector<std::size_t> indices;

    //- For the FrontView:
    int crossSection_ID;
    if (this->m_isLength)
    {
        crossSection_ID = length_Discretization - this->m_crossSection;
        Model3dHandler::getInstance(nullptr)->getFlattenedLengthCrossSectionCurve(surfaceID, crossSection_ID, curve, indices);
    } else {
        crossSection_ID = this->m_crossSection;
        Model3dHandler::getInstance(nullptr)->getFlattenedWidthCrossSectionCurve(surfaceID, crossSection_ID, curve, indices);
    }    
    return curve;
}


QString CanvasHandler::returnSubmittedTrajectories()
{
    QJsonArray curves_array;

    //- Get instance of the 3D Model
    std::vector<int> index = Model3dHandler::getInstance(nullptr)->getSurfaceIndices();

    unsigned int listSize = this->sketchTrajectories->getNumberOfCurves();
    std::vector<int> surfaceIDList = this->sketchTrajectories->getSurfaceIDList();

    unsigned int i = 0;
    //- For all submitted surfaces
    for (unsigned int a = 0; a < index.size(); a++)
    {
        std::vector<double> curve;
        std::vector<double> eachCurve;

        QJsonArray segment_array;
        QJsonObject segment_object;

        if (i < listSize)
        {
            if (surfaceIDList[i] == index[a])
            {
                do
                {
                    curve = this->sketchTrajectories->getCurve(i);
                    eachCurve = this->fromModelToMapView(curve);

                    QJsonArray plot_array;

                    QString x_str("x");
                    QString y_str("y");

                    for (unsigned int j = 0; j < eachCurve.size(); j += 2)
                    {
                        QJsonObject pointObject;

                        pointObject.insert(x_str, QJsonValue(eachCurve[j]));
                        pointObject.insert(y_str, QJsonValue(eachCurve[j + 1]));

                        plot_array.push_back(QJsonValue(pointObject));
                    }
                    segment_array.push_back(plot_array);
                    i++;
                } while (i < listSize && surfaceIDList[i] == surfaceIDList[i - 1]);
            }
        }

        segment_object.insert(QString("index"), QString::number(a));
        segment_object.insert(QString("segments"), QJsonValue(segment_array));

        curves_array.push_back(segment_object);
        curve.clear();
        eachCurve.clear();
    }

    QJsonObject final_object;
    final_object.insert(QString("curves_array"), QJsonValue(curves_array));
    QJsonDocument jsonDoc(final_object);

    QString json_string = jsonDoc.toJson();
    return json_string;
}


QString CanvasHandler::returnCurrentSketches()
{
    QJsonArray curves_array;

    unsigned int listSize = this->sketchHandler->getNumberOfSketches(this->m_crossSection, this->m_direction);    

    for (unsigned int i = 0; i < listSize; i++)
    {
        std::vector<double> curve;
        std::vector<double> eachCurve;

        //- Receive the current sketch from the SketchHandler
        curve = this->sketchHandler->returnSketch(this->m_crossSection, this->m_direction, i);
        eachCurve = this->fromModelToMapView(curve);

        QJsonArray plot_array;
        QString x_str("x");
        QString y_str("y");

        for (unsigned int j = 0; j < eachCurve.size(); j += 2)
        {
            QJsonObject pointObject;

            pointObject.insert(x_str, QJsonValue(eachCurve[j]));
            pointObject.insert(y_str, QJsonValue(eachCurve[j + 1]));

            plot_array.push_back(QJsonValue(pointObject));
        }
        curves_array.push_back(plot_array);        

        curve.clear();
        eachCurve.clear();
    }

    QJsonObject final_object;
    final_object.insert(QString("curves_array"), QJsonValue(curves_array));
    QJsonDocument jsonDoc(final_object);

    QString json_string = jsonDoc.toJson();
    return json_string;
}


QString CanvasHandler::returnSubmittedContours()
{
    ////qDebug() << "\n ===> RETURN Submitted Contours:";
    QJsonArray curves_array;   

    //- Get instance of the 3D Model
    std::vector<int> index = Model3dHandler::getInstance(nullptr)->getSurfaceIndices();

    unsigned int listSize = this->sketchContours->getNumberOfCurves();
    std::vector<int> surfaceIDList = this->sketchContours->getSurfaceIDList();

    unsigned int i = 0;
    //- For all submitted surfaces
    for (unsigned int a = 0; a < index.size(); a++)
    {
        std::vector<double> curve;
        std::vector<double> eachCurve;

        QJsonArray segment_array;
        QJsonObject segment_object;

        if(i < listSize)
        {
            if (surfaceIDList[i] == index[a])
            {
                do
                {                    
                    curve = this->sketchContours->getCurve(i);
                    eachCurve = this->fromModelToMapView(curve);

                    QJsonArray plot_array;

                    QString x_str("x");
                    QString y_str("y");

                    for (unsigned int j = 0; j < eachCurve.size(); j += 2)
                    {
                        QJsonObject pointObject;

                        pointObject.insert(x_str, QJsonValue(eachCurve[j]));
                        pointObject.insert(y_str, QJsonValue(eachCurve[j + 1]));

                        plot_array.push_back(QJsonValue(pointObject));
                    }
                    segment_array.push_back(plot_array);
                    i++;
                } while (i < listSize && surfaceIDList[i] == surfaceIDList[i - 1]);                
            }
        }

        segment_object.insert(QString("index"), QString::number(a));
        segment_object.insert(QString("segments"), QJsonValue(segment_array));

        curves_array.push_back(segment_object);
        curve.clear();
        eachCurve.clear();
    }

    ////qDebug() << "\n Submitted contours: " << curves_array.size();
    QJsonObject final_object;
    final_object.insert(QString("curves_array"), QJsonValue(curves_array));
    QJsonDocument jsonDoc(final_object);

    QString json_string = jsonDoc.toJson();
    return json_string;
}


QString CanvasHandler::returnCurrentContours()
{
    QJsonArray curves_array;
    QJsonArray segment_array;
    QJsonObject segment_object;

    unsigned int listSize = this->sketchContours->getNumberOfCurrentCurves();
    std::vector<int> surfaceIDList = this->sketchContours->getCurrentSurfaceIDList();

    for (unsigned int i = 0; i < listSize;)
    {
        std::vector<double> curve;
        std::vector<double> eachCurve;

        do
        {
            curve = this->sketchContours->getCurrentCurve(i);
            eachCurve = this->fromModelToMapView(curve);

            QJsonArray plot_array;

            QString x_str("x");
            QString y_str("y");

            for (unsigned int j = 0; j < eachCurve.size(); j += 2)
            {
                QJsonObject pointObject;

                pointObject.insert(x_str, QJsonValue(eachCurve[j]));
                pointObject.insert(y_str, QJsonValue(eachCurve[j + 1]));

                plot_array.push_back(QJsonValue(pointObject));
            }
            segment_array.push_back(plot_array);
            i++;
        } while (i < listSize && surfaceIDList[i] == surfaceIDList[i - 1]);

        segment_object.insert(QString("index"), QString::number(surfaceIDList[i - 1]));
        segment_object.insert(QString("segments"), QJsonValue(segment_array));

        curves_array.push_back(segment_object);

        curve.clear();
        eachCurve.clear();
    }

    QJsonObject final_object;
    final_object.insert(QString("curves_array"), QJsonValue(curves_array));
    QJsonDocument jsonDoc(final_object);

    QString json_string = jsonDoc.toJson();
    return json_string;
}

void CanvasHandler::updateContours()
{
    //- Verify if the action is coming from MapView
    if (this->m_direction == Direction::Value::HEIGHT)
    {
        int surfaceID = 0;
        this->sketchContours->cleanData();
        ////qDebug() << "\n Update Contours: ";
        //- Get number of cross sections that have sketch on it
        unsigned int numOfCrossSections = sketchHandler->getNumberOfCrossSectionsSubmitted();
        ////qDebug() << "------------> num of CS: " << numOfCrossSections;
        //- For each cross section
        for (unsigned int j = 0; j < numOfCrossSections; j++)
        {
            int csID = this->sketchHandler->getCrossSectionIDSubmitted(j);
            ////qDebug() << "-----------------> CS ID: " << csID;
            //- Save sketch from cross sections
            std::vector<double> sketch;
            unsigned int numOfSketches = this->sketchHandler->getNumberOfSketchesSubmitted(j);
            ////qDebug() << "-----------------> num of sketches: " << numOfSketches;
            for (unsigned int i = 0; i < numOfSketches; i++)
            {
                sketch = this->sketchHandler->returnSketchContourSubmitted(j, i, surfaceID);
                this->sketchContours->saveCurve(sketch, csID, surfaceID);
                sketch.clear();
            }
        }
    }
}


void CanvasHandler::updateCurrentContours()
{   
    //- Verify if the action is coming from MapView
    if (this->m_direction == Direction::Value::HEIGHT)
    {
        int surfaceID = 0;

        //- Get number of cross sections that have sketch on it
        unsigned int numOfCrossSections = this->sketchHandler->getNumberOfCrossSections();
        this->sketchContours->resetCurrentCurves();

        //- For each cross section
        for (unsigned int j = 0; j < numOfCrossSections; j++)
        {
            int csID = this->sketchHandler->getCrossSectionID(j);

            //- Save sketch from cross sections different from the current one
            if (csID != this->m_crossSection)
            {
                std::vector<double> sketch;
                unsigned int numOfSketches = this->sketchHandler->getNumberOfSketches(j);

                for (unsigned int i = 0; i < numOfSketches; i++)
                {
                    sketch = this->sketchHandler->returnSketchContour(j, i, surfaceID);
                    this->sketchContours->saveCurrentCurve(sketch, csID, surfaceID);
                    sketch.clear();
                }
            }
        }
    }
}


void CanvasHandler::preserveRegionBoundary(double x, double y)
{
    if (this->m_direction == Direction::Value::LENGTH)
    {
        this->regionIsValid = false;

        //- Prepare a 3D coordinate to check if the preserve region is possible
        std::vector<double> p;
        p.push_back(x);
        p.push_back(this->m_cs_Position_L);
        p.push_back(y);

        //- Verify if Preserve Region is valid
        this->regionIsValid = Model3dHandler::getInstance(nullptr)->RequestFlattenedPreserveRegion(p);
        this->updateRegionBoundary();
    }
    else if (this->m_direction == Direction::Value::WIDTH)
    {
        this->regionIsValid = false;

        //- Prepare a 3D coordinate to check if the preserve region is possible
        std::vector<double> p;
        p.push_back(this->m_cs_Position_W);
        p.push_back(x);        
        p.push_back(y);

        //- Verify if Preserve Region is valid
        this->regionIsValid = Model3dHandler::getInstance(nullptr)->RequestFlattenedPreserveRegion(p);
        this->updateRegionBoundary();
    }
}


void CanvasHandler::updateRegionBoundary()
{
    if (this->regionIsValid)
    {
        this->pa_boundary = this->preserveAbove();
        this->pb_boundary = this->preserveBelow();

        this->calculateRegionPolygon();
    }
}

void CanvasHandler::preserveRegionBoundary()
{
    this->regionIsValid = false;

    //- Get instance of the 3D model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);
    int width_Discretization, length_Discretization;
    Model3dHandler::getInstance(nullptr)->getModelDiscretization(width_Discretization, length_Discretization);

    //- Prepare a 3D coordinate to check if the preserve region is possible
    //- OBS: the z_value is the cross section ID mapped to the Box size.
    double z_value;
    std::vector<double> p_curve;

    if (this->m_direction == Direction::Value::LENGTH)
    {
        int crossSection_ID = length_Discretization - this->m_crossSection;
        z_value = ((crossSection_ID) * box_l) / length_Discretization;
        
        for (int i = 0; i < this->sketchRegion.size(); i += 2)
        {
            p_curve.push_back(this->sketchRegion[i]);
            p_curve.push_back(z_value);
            p_curve.push_back(this->sketchRegion[i + 1]);
        }
    }
    else if (this->m_direction == Direction::Value::WIDTH)
    {
        int crossSection_ID = this->m_crossSection;
        z_value = ((crossSection_ID)*box_w) / width_Discretization;

        for (int i = 0; i < this->sketchRegion.size(); i += 2)
        {
            p_curve.push_back(z_value); 
            p_curve.push_back(this->sketchRegion[i]);            
            p_curve.push_back(this->sketchRegion[i + 1]);
        }
    }
    

    //- Verify if it will be preserve above or preserve below:
    double firstY, lastY;
    if (this->sketchRegion.size())
    {
        firstY = this->sketchRegion[1];
        lastY = this->sketchRegion[this->sketchRegion.size() - 1];

        //- Preserve above:
        if (firstY < lastY)
        {
            this->regionIsValid = Model3dHandler::getInstance(nullptr)->RequestFlattenedPreserveAbove(p_curve);
            if (this->regionIsValid)
            {
                this->pa_boundary = this->preserveAbove();
                if (!this->calculateRegionPolygon())
                {
                    this->pb_boundary.clear();
                    this->pa_boundary = this->preserveAbove();
                    this->calculateRegionPolygon();
                }
            }
        }
        //- Preserve below:
        else {
            this->regionIsValid = Model3dHandler::getInstance(nullptr)->RequestFlattenedPreserveBelow(p_curve);
            if (this->regionIsValid)
            {
                this->pb_boundary = this->preserveBelow();
                if (!this->calculateRegionPolygon())
                {
                    this->pa_boundary.clear();
                    this->pb_boundary = this->preserveBelow();
                    this->calculateRegionPolygon();
                }
            }
        }
    }
}


QString CanvasHandler::returnRegion()
{
    QString region;

    //if (this->m_direction == Direction::Value::LENGTH)
    if(this->m_isLength)
    {
        //qDebug() << "size: " << this->pr_boundary.size();
        region = this->prepareJsonObject(this->fromModelToFrontViewWE(this->pr_boundary));
    }
    //else if (this->m_direction == Direction::Value::WIDTH)
    else if(this->m_isWidth)
    {
        region = this->prepareJsonObject(this->fromModelToFrontViewNS(this->pr_boundary));
    }

    ////qDebug() << region;

    return region;
}


void CanvasHandler::saveSketchTemplate()
{
    std::vector<double> sketch;
    
    //- Receive the current sketch from the SketchHandler
    sketch = this->sketchHandler->returnSketch(this->m_crossSection, this->m_direction);

    if (sketch.size())
    {
        if (this->sketchTemplates->saveSketchTemplate(sketch))
        {
            emit(updateSketchTemplatesList());
        }
        ////qDebug() << "\n ===> Sketch Templates: " << sketchTemplates->getNumberOfSketchTemplates();
    }
}


void CanvasHandler::saveSketchContourTemplate()
{
    std::vector<double> sketch;

    //- Receive the current selected sketch from the SketchHandler
    sketch = this->sketchHandler->returnSelectedSketch(this->m_crossSection, this->m_direction);

    if (sketch.size())
    {
        if (this->contourTemplates->saveSketchTemplate(sketch))
        {
            emit(updateContourTemplatesList());
        }
    }
}


void CanvasHandler::loadSketchTemplate(int index)
{
    std::vector<double> sketch;
    std::vector<double> newSketch;
    std::vector<double> screenCurve;

    //- Copy the last saved sketch template
    newSketch = this->sketchTemplates->getTemplate(index);

    //- Call the method from SketchHandler
    sketch = this->sketchHandler->loadSketchTemplate(this->m_crossSection, this->m_direction, newSketch);    

    //- Map the received sketch to Screen Coordinate System
    if (this->m_direction == Direction::Value::LENGTH)
    {
        screenCurve = this->fromModelToFrontViewWE(sketch);
    }
    else if (this->m_direction == Direction::Value::WIDTH)
    {
        screenCurve = this->fromModelToFrontViewNS(sketch);
    }

    //- Update the visualization of the Sketch in the QML side
    if (screenCurve.size())
    {
        QString response = this->prepareJsonObject(screenCurve);
        emit(getUpdatedSketch(response));
    }
}


void CanvasHandler::loadSketchContourTemplate(int index)
{
    std::vector<double> sketch;
    std::vector<double> newSketch;
    std::vector<double> screenCurve;

    //- Copy the last saved sketch template
    newSketch = this->contourTemplates->getTemplate(index);

    //- Call the method from SketchHandler
    sketch = this->sketchHandler->loadSketchTemplate(this->m_crossSection, this->m_direction, newSketch);

    //- Map the received sketch to Screen Coordinate System
    if (this->m_direction == Direction::Value::HEIGHT)
    {
        screenCurve = this->fromModelToMapView(sketch);
    }

    //- Update the visualization of the Sketch in the QML side
    if (screenCurve.size())
    {
        QString response = this->prepareJsonObject(screenCurve);
        emit(getUpdatedSketch(response));
    }
}


void CanvasHandler::loadTrajectoryTemplate(int index)
{
    std::vector<double> newPath;
    std::vector<double> screenCurve;

    //- Copy the selected trajectory template
    newPath = this->trajectoryTemplates->getTemplate(index);

    //- Call the method from SketchHandler
    this->sketchHandler->copyTrajectoryTemplate(newPath, this->m_crossSection, this->m_direction);

    //- Map the received sketch to Screen Coordinate System
    screenCurve = this->fromModelToMapView(newPath);

    //- Update the visualization of the Trajectory in the QML side
    if (screenCurve.size())
    {
        QString response = this->prepareJsonObject(screenCurve);
        emit(getUpdatedTrajectory(response));
    }
}


void CanvasHandler::saveSbimData(QUrl fileUrl)
{
    //- Prepare the file name with .JSON extension
    QString absPath = QString::fromStdString(getAbsPath(fileUrl));
    ////qDebug() << "CanvasHandler SAVE: " << absPath;
    QString croped_fileName = absPath.section(".", 0, -1) + ".json";
    ////qDebug() << "CanvasHandler SAVE: " << croped_fileName;

    //- Create instance of the SbimFileHandler
    SbimFileHandler sbimFileHandler(croped_fileName);
    
    //- Reset data
    sbimFileHandler.resetDataStructure();

    //- Set all SBIM data
    SketchData contourData;
    contourData.curves = this->sketchContours->getCurveList();
    contourData.cross_section_id = this->sketchContours->getCrossSectionIDList();
    contourData.surface_id = this->sketchContours->getSurfaceIDList();
    sbimFileHandler.setContourData(contourData);

    SketchData trajectoryData;
    trajectoryData.curves = this->sketchTrajectories->getCurveList();
    trajectoryData.cross_section_id = this->sketchTrajectories->getCrossSectionIDList();
    trajectoryData.surface_id = this->sketchTrajectories->getSurfaceIDList();
    sbimFileHandler.setTrajectoryData(trajectoryData);
    
    //- Save file
    sbimFileHandler.save();
}


void CanvasHandler::loadSbimData(QUrl fileUrl)
{
    //- Prepare the file name with .JSON extension
    QString absPath = QString::fromStdString(getAbsPath(fileUrl));
    QString croped_fileName = absPath.section(".", 0, -1) + ".json";
    ////qDebug() << "CanvasHandler LOAD: " << croped_fileName;

    //- Create instance of the SbimFileHandler
    SbimFileHandler sbimFileHandler(croped_fileName);

    //- Load file
    sbimFileHandler.load();

    //- Get all SBIM data    
    SketchData contourData = sbimFileHandler.getContourData();
    this->sketchContours->reset();
    this->sketchContours->setCurveList(contourData.curves);
    this->sketchContours->setCrossSectionIDList(contourData.cross_section_id);
    this->sketchContours->setSurfaceIDList(contourData.surface_id);

    SketchData trajectoryData = sbimFileHandler.getTrajectoryData();
    this->sketchTrajectories->reset();
    this->sketchTrajectories->setCurveList(trajectoryData.curves);
    this->sketchTrajectories->setCrossSectionIDList(trajectoryData.cross_section_id);
    this->sketchTrajectories->setSurfaceIDList(trajectoryData.surface_id);
    for (int i = 0; i < trajectoryData.curves.size(); i++) {
        this->trajectoryTemplates->saveSketchTemplate(trajectoryData.curves[i]);
        emit(updateTrajectoryTemplatesList());
    }    
    
    emit(updateLoadedData());
}


std::string CanvasHandler::getAbsPath(const QUrl& path)
{
    ////qDebug() << "CanvasHandler:: Saving Model: " << path;

    QUrl localPath;
    if (path.isLocalFile())
    {
        localPath = path.toLocalFile();

    }
    else localPath = path;

    QString pathString = localPath.toEncoded();
    pathString = pathString.replace("%20", "\ ");
    return pathString.toUtf8().constData();
}


void CanvasHandler::updateIsSurfaceCreated()
{
    bool response = false;

    if (this->m_direction == Direction::Value::LENGTH || this->m_direction == Direction::Value::WIDTH)
    {
        //- Get instance of the 3D Model
        std::vector<int> index = Model3dHandler::getInstance(nullptr)->getSurfaceIndices();

        if (index.size())
            response = true;
    }

    emit(surfaceCreated(response));
}


void CanvasHandler::fixGuidelineFrontView(double mouseX, double mouseY)
{
    //- Get instance of the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);

    double modelX, modelY;

    if(this->m_isLength)
    {
        this->fromFrontViewWEToModel(mouseX, mouseY, modelX, modelY);
    }
    else if(this->m_isWidth)
    {
        this->fromFrontViewNSToModel(mouseX, mouseY, modelX, modelY);

        //- Reverse NS coordinates
        if (NS_REVERSED) modelX = box_l - modelX;
    }

    this->gl_FV_mouseX = modelX;
    this->gl_FV_mouseY = modelY;
}

void CanvasHandler::fixGuidelineMapView(double mouseX, double mouseY)
{
    //- Get instance of the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);

    double modelX, modelY;

    this->fromMapViewToModel(mouseX, mouseY, modelX, modelY);

    this->gl_MV_mouseX = modelX;
    this->gl_MV_mouseY = modelY;
}

double CanvasHandler::getFV_gl_mouseX()
{
    std::vector<double> response;
    std::vector<double> click;
    click.push_back(this->gl_FV_mouseX);
    click.push_back(this->gl_FV_mouseY);

    if (this->m_isLength)        
    {
        response = this->fromModelToFrontViewWE(click);
    }
    else if(this->m_isWidth)
    {
        response = this->fromModelToFrontViewNS(click);
    }

    return response[0];
}

double CanvasHandler::getFV_gl_mouseY()
{
    std::vector<double> response;
    std::vector<double> click;
    response.clear();
    click.clear();
    click.push_back(this->gl_FV_mouseX);
    click.push_back(this->gl_FV_mouseY);

    if (this->m_isLength)
    {
        response = this->fromModelToFrontViewWE(click);
    }
    else if (this->m_isWidth)
    {
        response = this->fromModelToFrontViewNS(click);
    }

    return response[1];
}

double CanvasHandler::getMV_gl_mouseX()
{
    std::vector<double> response;
    std::vector<double> click;
    response.clear();
    click.clear();
    click.push_back(this->gl_MV_mouseX);
    click.push_back(this->gl_MV_mouseY);

    response = this->fromModelToMapView(click);
    return response[0];
}

double CanvasHandler::getMV_gl_mouseY()
{
    std::vector<double> response;
    std::vector<double> click;
    response.clear();
    click.clear();
    click.push_back(this->gl_MV_mouseX);
    click.push_back(this->gl_MV_mouseY);

    response = this->fromModelToMapView(click);
    return response[1];
}

//--- END OF SICILIA'S PUBLIC METHODS ---------------------------------------


//--- FAZILA'S PUBLIC SLOTS -------------------------------------------------	
void CanvasHandler::undo3d()
{
    this->updateSketchRegionStatus();

    //- Get instance of the 3D Model
    //std::vector<size_t> index = SModeller::Instance().getSurfacesIndices();//Model3dHandler::getInstance(nullptr)->getSurfaceIndices();
    std::vector<int> index = Model3dHandler::getInstance(nullptr)->getSurfaceIndices();
    int surfaceID = -1;
    /*
    if (index.size()) {
        surfaceID = index[index.size() - 1] + 1; //CORRECT
    }
    */
    surfaceID = index.back();

    this->sketchContours->undoSurface(surfaceID);
    this->sketchTrajectories->undoSurface(surfaceID);
    this->updateIsSurfaceCreated();
    emit(refreshSubmittedCurves());
}


void CanvasHandler::redo3d()
{
    this->updateSketchRegionStatus();

    //- Get instance of the 3D Model
    std::vector<int> index = Model3dHandler::getInstance(nullptr)->getSurfaceIndices();
    int surfaceID = -1;
    /*
    if (index.size()) {
        surfaceID = index[index.size() - 1];
    }
    */
    surfaceID = index.back();

    this->sketchContours->redoSurface(surfaceID);
    this->sketchTrajectories->redoSurface(surfaceID);
    this->updateIsSurfaceCreated();
    emit(refreshSubmittedCurves());
}

void CanvasHandler::flattenTheSelectedSurface(int surface_id)
{
    std::vector<double> screenCurve = this->getCurve(surface_id);
    double avgHeight = Model3dHandler::getInstance(nullptr)->computeCurveAverageHeight(screenCurve);
    Model3dHandler::getInstance(nullptr)->FlattenSurface(surface_id, avgHeight);

    double minH, maxH;
    Model3dHandler::getInstance(nullptr)->ComputeBBoxHeights(minH, maxH);
    this->m_Flat_Height = maxH - minH;
    this->m_Flat_delta = minH;
    emit(updateBBoxFlattening(this->m_Flat_Height));
    emit(refreshSubmittedCurves());

    // To let Object tree know which surface is being flattened
    emit(flatteningStarted(surface_id, this->m_Flat_Height));

}

//--- END OF FAZILA'S PUBLIC SLOTS ------------------------------------------	


//--- SICILIA'S PUBLIC SLOTS ------------------------------------------------	
void CanvasHandler::returnSketch()
{
    std::vector<double> sketch;
    std::vector<double> screenCurve;

    //- Receive the current sketch from the SketchHandler
    sketch = this->sketchHandler->returnSketch(this->m_crossSection, this->m_direction);

    //- Map the received sketch to Screen Coordinate System
    if (this->m_direction == Direction::Value::LENGTH) {
        screenCurve = this->fromModelToFrontViewWE(sketch);
    }
    else if (this->m_direction == Direction::Value::WIDTH) {
        screenCurve = this->fromModelToFrontViewNS(sketch);
    }
    else if (this->m_direction == Direction::Value::HEIGHT) {
        screenCurve = this->fromModelToMapView(sketch);
    }

    if (screenCurve.size())
        this->m_isSubmitAllowed = true;

    //- Update the visualization of the Sketch in the QML side
    QString response = this->prepareJsonObject(screenCurve);
    emit(getUpdatedSketch(response));
}


void CanvasHandler::returnTrajectory()
{
    std::vector<double> sketch;
    std::vector<double> screenCurve;

    //- Receive the current sketch of the trajectory from the SketchHandler
    sketch = this->sketchHandler->returnTrajectory();

    //- Map the received sketch to Screen Coordinate System
    screenCurve = this->fromModelToMapView(sketch);

    if (screenCurve.size())
        this->m_isPathGuided = true;
    else
        this->m_isPathGuided = false;

    //- Update the visualization of the Sketch in the QML side
    QString response = this->prepareJsonObject(screenCurve);
    emit(getUpdatedTrajectory(response));
}


void CanvasHandler::returnOverSketch(std::vector<double> curve)
{
    std::vector<double> screenCurve;

    //- Map the received sketch to Screen Coordinate System
    if (this->m_direction == Direction::Value::LENGTH) {
        screenCurve = this->fromModelToFrontViewWE(curve);
    }
    else if (this->m_direction == Direction::Value::WIDTH) {
        screenCurve = this->fromModelToFrontViewNS(curve);
    }
    else if (this->m_direction == Direction::Value::HEIGHT) {
        screenCurve = this->fromModelToMapView(curve);
    }

    //- Update the visualization of the Sketch in the QML side
    QString response = this->prepareJsonObject(screenCurve);
    emit(getUpdatedOverSketch(response));
}


void CanvasHandler::returnTrajectoryOverSketch(std::vector<double> curve)
{
    std::vector<double> screenCurve;

    //- Map the received sketch to Screen Coordinate System
    screenCurve = this->fromModelToMapView(curve);

    if (screenCurve.size())
        this->m_isPathGuided = true;
    else
        this->m_isPathGuided = false;

    //- Update the visualization of the Sketch in the QML side
    QString response = this->prepareJsonObject(screenCurve);
    emit(getUpdatedTrajectoryOverSketch(response));
}


void CanvasHandler::updateSketchRegionStatus()
{
    this->resetSketchRegion();

    if (Model3dHandler::getInstance(nullptr)->isPreserveAboveActive())
    {
        //updateRegionBoundary();
        this->pa_boundary = this->preserveAbove();
    }
    if (Model3dHandler::getInstance(nullptr)->isPreserveBelowActive())
    {
        this->pb_boundary = this->preserveBelow();
    }
    this->calculateRegionPolygon();

    emit(getUpdatedPolygonCorners(this->returnRegion()));
}


void CanvasHandler::returnSketchRegion()
{
    //- Map the received sketch to Screen Coordinate System
    std::vector<double> screenCurve;

    if (this->m_direction == Direction::Value::LENGTH)
    {
        screenCurve = this->fromModelToFrontViewWE(this->sketchRegion);
    }
    else if (this->m_direction == Direction::Value::WIDTH)
    {
        screenCurve = this->fromModelToFrontViewNS(this->sketchRegion);
    }

    //- Update the visualization of the Sketch in the QML side
    QString response = this->prepareJsonObject(screenCurve);
    emit(getUpdatedSketchRegion(response));
}


void CanvasHandler::onSketchRegionAddPoint(double mouse_x, double mouse_y)
{
    double x, y;

    if (this->m_direction == Direction::Value::LENGTH)
    {
        this->fromFrontViewWEToModel(mouse_x, mouse_y, x, y);

        this->sketchRegion.push_back(x);
        this->sketchRegion.push_back(y);

        //- Map the received sketch to Screen Coordinate System
        std::vector<double> screenCurve;
        screenCurve = this->fromModelToFrontViewWE(this->sketchRegion);

        //- Update the visualization of the Sketch in the QML side
        QString response = this->prepareJsonObject(screenCurve);
        emit(getUpdatedSketchRegion(response));
    }
    else if (this->m_direction == Direction::Value::WIDTH)
    {
        this->fromFrontViewNSToModel(mouse_x, mouse_y, x, y);

        this->sketchRegion.push_back(x);
        this->sketchRegion.push_back(y);

        //- Map the received sketch to Screen Coordinate System
        std::vector<double> screenCurve;
        screenCurve = this->fromModelToFrontViewNS(this->sketchRegion);

        //- Update the visualization of the Sketch in the QML side
        QString response = this->prepareJsonObject(screenCurve);
        emit(getUpdatedSketchRegion(response));
    }
}


void CanvasHandler::startNewSketchRegion()
{
    this->sketchRegion.clear();
    //- Update the visualization of the Sketch in the QML side
    QString response = this->prepareJsonObject(this->sketchRegion);
    emit(getUpdatedSketchRegion(response));
}


void CanvasHandler::resetSketchRegion()
{
    this->pa_boundary.clear();
    this->pb_boundary.clear();
    this->pr_boundary.clear();
}


bool CanvasHandler::calculateRegionPolygon()
{
    if (this->regionIsValid)
    {
        //- Calculation of the Polygon
        if ((this->pb_boundary.empty() == false) && (this->pa_boundary.empty() == false))
        {
            QPolygonF polygon_lower = this->fromVectorDoubleToQt(this->pa_boundary);
            QPolygonF polygon_upper = this->fromVectorDoubleToQt(this->pb_boundary);

            QPolygonF polygon_intersected = polygon_upper.intersected(polygon_lower);
            this->pr_boundary = this->fromQtToVectorDouble(polygon_intersected);

            if (this->pr_boundary.size())
            {
                return true;
            }
            else {
                return false;
            }

        }
        else if (this->pb_boundary.empty() == false)
        {
            this->pr_boundary = this->pb_boundary;
            return true;
        }
        else if (this->pa_boundary.empty() == false)
        {
            this->pr_boundary = this->pa_boundary;
            return true;
        }
        else {
            this->pr_boundary.clear();
            return false;
        }
    }
}

void CanvasHandler::modelLoaded(const QUrl& path)
{
    this->reset();
    this->loadSbimData(path);
    emit(modelLoadingCompleted());
}
//--- END OF SICILIA'S PUBLIC SLOTS -----------------------------------------	


//--- FAZILA'S PRIVATE METHODS ----------------------------------------------	
//--- END OF FAZILA'S PRIVATE METHODS ---------------------------------------	


//--- SICILIA'S PRIVATE METHODS ---------------------------------------------	
void CanvasHandler::fromFrontViewWEToModel(double screen_x, double screen_y, double& model_x, double& model_y)
{
    //- Get instance of the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);
    double box_origin_x, box_origin_y, box_origin_z;
    Model3dHandler::getInstance(nullptr)->getOrigin(box_origin_x, box_origin_y, box_origin_z);
    
    //- Verify if Flattening is on
    if (Model3dHandler::getInstance(nullptr)->isFlatteningActive())
    {
        box_h = this->m_Flat_Height;
        box_origin_y = this->m_Flat_delta;
    }

    //- Map from Front View WE to Model Coordinate System
    double screen_W, screen_H;
    screen_W = box_w * this->m_csWE_Scale;
    screen_H = box_h * this->m_csWE_Scale * this->m_VE_factor;

    model_x = ((screen_x - this->m_csWE_Origin_x) / this->m_csWE_Scale) + box_origin_x;
    model_y = ((screen_H - (screen_y - this->m_csWE_Origin_y)) / this->m_csWE_Scale / this->m_VE_factor) + box_origin_y;
}


void CanvasHandler::fromFrontViewNSToModel(double screen_x, double screen_y, double& model_x, double& model_y)
{
    //- Get instance of the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);
    double box_origin_x, box_origin_y, box_origin_z;
    Model3dHandler::getInstance(nullptr)->getOrigin(box_origin_x, box_origin_y, box_origin_z);
        
    //- Verify if Flattening is on
    if (Model3dHandler::getInstance(nullptr)->isFlatteningActive())
    {
        box_h = this->m_Flat_Height;
        box_origin_y = this->m_Flat_delta;
    }
    
    //- Map from Front View NS to Model Coordinate System
    double screen_W, screen_H;
    screen_W = box_l * this->m_csNS_Scale;
    screen_H = box_h * this->m_csNS_Scale * this->m_VE_factor;

    model_x = ((screen_x - this->m_csNS_Origin_x) / this->m_csNS_Scale) + box_origin_z;
    model_y = ((screen_H - (screen_y - this->m_csNS_Origin_y)) / this->m_csNS_Scale / this->m_VE_factor) + box_origin_y;
}


void CanvasHandler::fromMapViewToModel(double screen_x, double screen_y, double& model_x, double& model_y)
{
    //- Get instance of the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);
    double box_origin_x, box_origin_y, box_origin_z;
    Model3dHandler::getInstance(nullptr)->getOrigin(box_origin_x, box_origin_y, box_origin_z);

    //- Map from Map View to Model Coordinate System
    double screen_W, screen_L;
    screen_W = box_w * this->m_csMap_Scale;
    screen_L = box_l * this->m_csMap_Scale;

    model_x = ((screen_x - this->m_csMap_Origin_x) / this->m_csMap_Scale) + box_origin_x;
    model_y = ((screen_L - (screen_y - this->m_csMap_Origin_y)) / this->m_csMap_Scale) + box_origin_y;
}


std::vector<double> CanvasHandler::fromModelToFrontViewWE(std::vector<double> old)
{
    //- Get instance of the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);
    double box_origin_x, box_origin_y, box_origin_z;
    Model3dHandler::getInstance(nullptr)->getOrigin(box_origin_x, box_origin_y, box_origin_z);
    
    //- Verify if Flattening is on
    if (Model3dHandler::getInstance(nullptr)->isFlatteningActive())
    {
        box_h = this->m_Flat_Height;
        box_origin_y = this->m_Flat_delta;
    }
    
    //- Map from Map View to Model Coordinate System
    std::vector<double> response;

    double value;
    for (size_t i = 0; i < old.size(); i += 2)
    {
        value = ((old[i] - box_origin_x) * this->m_csWE_Scale) + this->m_csWE_Origin_x;
        response.push_back(value);

        value = ((box_h - (old[i + 1] - box_origin_y)) * this->m_csWE_Scale * this->m_VE_factor) + this->m_csWE_Origin_y;
        response.push_back(value);
    }
    old.clear();

    return response;
}


std::vector<double> CanvasHandler::fromModelToFrontViewNS(std::vector<double> old)
{
    //- Get instance of the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);
    double box_origin_x, box_origin_y, box_origin_z;
    Model3dHandler::getInstance(nullptr)->getOrigin(box_origin_x, box_origin_y, box_origin_z);
    
    //- Verify if Flattening is on
    if (Model3dHandler::getInstance(nullptr)->isFlatteningActive())
    {
        box_h = this->m_Flat_Height;
        box_origin_y = this->m_Flat_delta;
    }
    
    //- Map from Map View to Model Coordinate System
    std::vector<double> response;

    double value;
    for (size_t i = 0; i < old.size(); i += 2)
    {
        //- Reverse
        if (NS_REVERSED) old[i] = box_l - old[i];

        value = ((old[i] - box_origin_x) * this->m_csNS_Scale) + this->m_csNS_Origin_x;
        response.push_back(value);

        value = ((box_h - (old[i + 1] - box_origin_y)) * this->m_csNS_Scale * this->m_VE_factor) + this->m_csNS_Origin_y;
        response.push_back(value);
    }
    old.clear();
    return response;
}


std::vector<double> CanvasHandler::fromModelToMapView(std::vector<double> old)
{
    //- Get instance of the 3D Model
    double box_w, box_l, box_h;
    Model3dHandler::getInstance(nullptr)->getModelDimension(box_w, box_l, box_h);
    double box_origin_x, box_origin_y, box_origin_z;
    Model3dHandler::getInstance(nullptr)->getOrigin(box_origin_x, box_origin_y, box_origin_z);

    //- Map from Map View to Model Coordinate System
    std::vector<double> response;

    double value;
    for (size_t i = 0; i < old.size(); i += 2)
    {
        value = ((old[i] - box_origin_x) * this->m_csMap_Scale) + this->m_csMap_Origin_x;
        response.push_back(value);

        value = ((box_l - (old[i + 1] - box_origin_z)) * this->m_csMap_Scale) + this->m_csMap_Origin_y;
        response.push_back(value);
    }
    old.clear();

    return response;
}


QPolygonF CanvasHandler::fromVectorDoubleToQt(std::vector<double> curve_coordinates)
{
    QPolygonF pol_;

    for (size_t i = 0; i < curve_coordinates.size(); i += 2)
    {
        pol_ << QPointF(curve_coordinates[i], curve_coordinates[i + 1]);
    }
    return pol_;
}


std::vector<double> CanvasHandler::fromQtToVectorDouble(const QPolygonF& pol_)
{
    std::vector<double> curve_coordinates;

    for (int i = 0; i < pol_.size(); ++i)
    {
        QPointF p = pol_[i];

        curve_coordinates.push_back(p.x());
        curve_coordinates.push_back(p.y());
    }

    return curve_coordinates;
}


QString CanvasHandler::prepareJsonObject(std::vector<double> curve)
{
    QJsonArray plot_array;

    QString x_str("x");
    QString y_str("y");

    //- Iterate over elements of the curve and prepare jSonObject:
    for (size_t i = 0; i < curve.size(); i += 2)
    {
        QJsonObject pointObject;

        pointObject.insert(x_str, QJsonValue(curve[i]));
        pointObject.insert(y_str, QJsonValue(curve[i + 1]));

        plot_array.push_back(QJsonValue(pointObject));
    }

    QJsonObject final_object;
    final_object.insert(QString("plottingData"), QJsonValue(plot_array));
    QJsonDocument jsonDoc(final_object);

    QString json_string = jsonDoc.toJson();
    return json_string;
}


QString CanvasHandler::implode2json(std::vector<std::vector<double>> array)
{
    QJsonArray output_array;

    QString x_str("x");
    QString y_str("y");

    ////qDebug() << "Vector size: " << array.size();
    //- Iterate over elements of the array and prepare jSonObject:
    for (size_t k = 0; k < array.size(); k++)
    {
        QJsonObject indexObject;
        QJsonArray plot_array;

        for (size_t i = 0; i < array[k].size(); i += 2)
        {
            QJsonObject pointObject;

            pointObject.insert(x_str, QJsonValue(array[k][i]));
            pointObject.insert(y_str, QJsonValue(array[k][i + 1]));

            plot_array.push_back(pointObject);
        }
        indexObject.insert(QString::number(k), QJsonValue(plot_array));
        output_array.push_back(QJsonValue(indexObject));
    }

    QJsonObject final_object;
    final_object.insert(QString("regionListData"), QJsonValue(output_array));
    QJsonDocument jsonDoc(final_object);

    QString json_string = jsonDoc.toJson();

    return json_string;
}


std::vector<double> CanvasHandler::preserveAbove()
{
    //- Get information of Boundary Box discretization
    int width_Discretization, length_Discretization;
    Model3dHandler::getInstance(nullptr)->getModelDiscretization(width_Discretization, length_Discretization);

    std::vector<double> PA_region_vertices;
    std::vector<size_t> PA_region_segments;

    //if (this->m_direction == Direction::Value::LENGTH)
    if (this->m_isLength)
    {
        int crossSection_ID = length_Discretization - this->m_crossSection;
        Model3dHandler::getInstance(nullptr)->getFlattenedLengthPreserveAboveCurveBox(crossSection_ID, PA_region_vertices, PA_region_segments);
    }
    //else if (this->m_direction == Direction::Value::WIDTH)
    else if (this->m_isWidth)
    {
        int crossSection_ID = this->m_crossSection;
        Model3dHandler::getInstance(nullptr)->getFlattenedWidthPreserveAboveCurveBox(crossSection_ID, PA_region_vertices, PA_region_segments);
    }

    return PA_region_vertices;
}


std::vector<double> CanvasHandler::preserveBelow()
{
    //- Get information of Boundary Box discretization
    int width_Discretization, length_Discretization;
    Model3dHandler::getInstance(nullptr)->getModelDiscretization(width_Discretization, length_Discretization);

    std::vector<double> PB_region_vertices;
    std::vector<size_t> PB_region_segments;

    //if (this->m_direction == Direction::Value::LENGTH)
    if (this->m_isLength)
    {
        int crossSection_ID = length_Discretization - this->m_crossSection;
        Model3dHandler::getInstance(nullptr)->getFlattenedLengthPreserveBelowCurveBox(crossSection_ID, PB_region_vertices, PB_region_segments);
    }
    //else if (this->m_direction == Direction::Value::WIDTH)
    else if (this->m_isWidth)
    {
        int crossSection_ID = this->m_crossSection;
        Model3dHandler::getInstance(nullptr)->getFlattenedWidthPreserveBelowCurveBox(crossSection_ID, PB_region_vertices, PB_region_segments);
    }

    return PB_region_vertices;

}
//--- END OF SICILIA'S PRIVATE METHODS --------------------------------------	