#include "Model3dHandler.h"

#include <qDebug.h>
#include <filesystem>

#include "Constants.h"

#include "model/metadata_access.h"

#include "stratmod/smodeller2.hpp"

bool Model3dHandler::instanceFlag = false;
Model3dHandler* Model3dHandler::single = NULL;

Model3dHandler::Model3dHandler(QObject* parent) : QObject(parent)
{
	m_removeRuleId = GeologicRules::Operator::RAI;
}

Model3dHandler::~Model3dHandler()
{
	instanceFlag = false;
}

Model3dHandler* Model3dHandler::getInstance(QQmlEngine* qmlEngine)
{
	if (!instanceFlag)
	{
		single = new Model3dHandler();
		instanceFlag = true;
		//set this instance to QML root context
		QQmlContext* rootContext = qmlEngine->rootContext();
		rootContext->setContextProperty("model3DHandler", QVariant::fromValue(single));
		return single;
	}
	else
	{
		return single;
	}
}

void Model3dHandler::getModelDimension(double& size_x, double& size_y, double& size_z)
{
	size_x = m_size_x;
	size_y = m_size_y;
	size_z = m_size_z;
}

void Model3dHandler::setModelDimension()
{
	double size_x, size_y, size_z;
	m_model.GetSize(size_x, size_y, size_z);
	m_size_x = size_x;
	m_size_y = size_y;
	m_size_z = size_z;
}

void Model3dHandler::getModelDiscretization(int& width, int &length)
{
	width = m_width_Discretization;
	length = m_length_Discretization;
}

void Model3dHandler::setModelDiscretization()
{
	int width, length;
	m_model.GetDiscretization(width, length);
	m_width_Discretization = width;
	m_length_Discretization = length;
}

void Model3dHandler::setModelDiscretization(int discretization)
{
	m_width_Discretization = discretization;
	m_length_Discretization = discretization;
	m_model.SetDiscretization(m_width_Discretization, m_length_Discretization);
}

void Model3dHandler::reset()
{
	m_size_x = 500.0;
	m_size_y = 500.0;
	m_size_z = 500.0;
	m_width_Discretization = 64;
	m_length_Discretization = 64;
	m_origin_x = 0.0;
	m_origin_y = 0.0;
	m_origin_z = 0.0;

	m_crossSectionId = 64;

	m_planeId = 2;

	m_ruleEnabled = false;

	m_removeRuleId = 0;

	m_region_list.clear();
	m_total_volume = 0.0;
	m_region_list_proportion.clear();
	m_domain_list.clear();
	m_domain_list_proportion.clear();

	m_surface_indices.clear();

	m_model.clear();
}

void Model3dHandler::saveSelectedCrossSection(int crossSectionId, int planeId)
{
	if (m_crossSectionId != crossSectionId)
		m_crossSectionId = crossSectionId;

	if (m_planeId != planeId)
		m_planeId = planeId;

}

int Model3dHandler::getCrossSectionId()
{
	return m_crossSectionId;
}

int Model3dHandler::getPlaneId()
{
	return m_planeId;
}

void Model3dHandler::setDimension(double w, double l, double h) 
{
	emit(dimensionChanged());
}

int Model3dHandler::getWidth()
{
	double size_x;
	double size_y;
	double size_z;

	m_model.GetSize(size_x, size_y, size_z);

	return (int)size_x;
}

int Model3dHandler::getLength()
{
	double size_x;
	double size_y;
	double size_z;

	m_model.GetSize(size_x, size_y, size_z);

	return (int)size_y;
}
int Model3dHandler::getHeight()
{
	double size_x;
	double size_y;
	double size_z;

	m_model.GetSize(size_x, size_y, size_z);

	return (int)size_z;
}

/////////////////////////////////////////////////////////////////
// New code to access model
/////////////////////////////////////////////////////////////////

bool Model3dHandler::loadFile(const QString& filename)
{
	return m_model.LoadFile(filename.toStdString());
}

bool Model3dHandler::saveFile(const QString& filename)
{
	std::filesystem::path full_path = (std::filesystem::current_path());
	//QString s = QString::fromUtf8((full_path.string()).c_str());
	////qDebug() << "Saving file here: " << s;
	return m_model.SaveFile((full_path / filename.toStdString()).string());	
}

void Model3dHandler::setOrigin(double origin_x, double origin_y, double origin_z)
{
	m_origin_x = origin_x;
	m_origin_y = origin_y;
	m_origin_z = origin_z;

	m_model.useDefaultCoordinateSystem();
	m_model.SetOrigin(origin_x, origin_y, origin_z);
}

bool Model3dHandler::setSize(double size_x, double size_y, double size_z)
{
	m_size_x = size_x;
	m_size_y = size_y;
	m_size_z = size_z;

	return m_model.SetSize(size_x, size_y, size_z);
}

void Model3dHandler::getOrigin(double& origin_x, double& origin_y, double& origin_z)
{
	origin_x = m_origin_x;
	origin_y = m_origin_y;
	origin_z = m_origin_z;
	//m_model.GetOrigin(origin_x, origin_y, origin_z);
}

void Model3dHandler::getSize(double& size_x, double& size_y, double& size_z)
{
	size_x = m_size_x;
	size_y = m_size_y;
	size_z = m_size_z;
	//m_model.GetSize(size_x, size_y, size_z);
}

std::vector<int> Model3dHandler::getSurfaceIndices()
{
	return m_surface_indices;
}

void Model3dHandler::setSurfaceIndices()
{
	m_surface_indices = m_model.GetSurfaceIndices();
}

bool Model3dHandler::setDiscretization(int discretization_width, int discretization_length)
{
	return m_model.SetDiscretization(static_cast<int>(discretization_width), static_cast<int>(discretization_length));
}

bool Model3dHandler::getDiscretization(int& discretization_width, int& discretization_length)
{
	/* discretization_width = static_cast<int>(m_model.getWidthDiscretization()); */
	/* discretization_length = static_cast<int>(m_model.getLengthDiscretization()); */

	return m_model.GetDiscretization(discretization_width, discretization_length);
}

int Model3dHandler::getWidthDiscretization()
{
	return static_cast<int>(m_model.getWidthDiscretization());
}

int Model3dHandler::getLengthDiscretization()
{
	return static_cast<int>(m_model.getLengthDiscretization());
}

bool Model3dHandler::createExtrudedSurface(int surface_id, const std::vector<double>& curve)
{
	return m_model.CreateExtrudedSurface(static_cast<size_t>(surface_id), curve);
}

bool Model3dHandler::createLengthExtrudedSurface(int surface_id, const std::vector<double>& curve)
{
	return m_flat_model.CreateLengthExtrudedSurface(static_cast<size_t>(surface_id), curve);
}

bool Model3dHandler::createWidthExtrudedSurface(int surface_id, const std::vector<double>& curve)
{
	return m_flat_model.CreateWidthExtrudedSurface(static_cast<size_t>(surface_id), curve);
}

bool Model3dHandler::getWidthCrossSectionCurve(
	int surface_id,
	int cross_section_id,
	std::vector<double>& curve)
{
	std::vector<double> vertex_coordinates;
	std::vector<std::size_t> vertex_indices;

	bool success = m_flat_model.GetWidthCrossSectionCurve(
		surface_id,
		cross_section_id,
		vertex_coordinates,
		vertex_indices);

	if (!success)
	{
		return false;
	}

	size_t curve_size = vertex_coordinates.size() / 2;
	for (size_t i = 0; i < curve_size; ++i)
	{
		curve.push_back(vertex_coordinates[2 * i + 0]);
		curve.push_back(vertex_coordinates[2 * i + 1]);
	}

	return true;
}

bool Model3dHandler::getWidthCrossSectionCurve(
	int surface_id,
	int cross_section_id,
	std::vector<double>& curve,
	std::vector<std::size_t>& indices)
{

	bool success = m_model.GetWidthCrossSectionCurve(
		surface_id,
		cross_section_id,
		curve,
		indices);

	if (!success)
	{
		return false;
	}

	return true;
}


bool Model3dHandler::getLenghtCrossSectionCurve(
	int surface_id,
	int cross_section_id,
	std::vector<double>& curve)
{
	std::vector<double> vertex_coordinates;
	std::vector<std::size_t> vertex_indices;

	bool success = m_flat_model.GetLengthCrossSectionCurve(
		surface_id,
		cross_section_id,
		vertex_coordinates,
		vertex_indices);

	if (!success)
	{
		return false;
	}

	size_t curve_size = vertex_coordinates.size() / 2;
	for (size_t i = 0; i < curve_size; ++i)
	{
		curve.push_back(vertex_coordinates[2 * i + 0]);
		curve.push_back(vertex_coordinates[2 * i + 1]);
	}

	////qDebug() << "Surface id: " << surface_id << ", indices size: " << vertex_indices.size();

	return true;
}

bool Model3dHandler::getLenghtCrossSectionCurve(
	int surface_id,
	int cross_section_id,
	std::vector<double>& curve,
	std::vector<std::size_t>& indices)
{

	bool success = m_model.GetLenghtCrossSectionCurve(
		surface_id,
		cross_section_id,
		curve,
		indices);

	if (!success)
	{
		return false;
	}

	return true;
}

bool Model3dHandler::getSurfaceMesh(
	int surface_id,
	std::vector<double>& vertex_coordinates,
	std::vector<std::size_t>& vertex_indices)
{
	return m_model.GetSurfaceMesh(
		surface_id,
		vertex_coordinates,
		vertex_indices);
}

bool Model3dHandler::getFlattenedSurfaceMesh(
	int surface_id,
	std::vector<double>& vertex_coordinates,
	std::vector<std::size_t>& vertex_indices)
{
	//return m_model.GetSurfaceMesh(
	return m_flat_model.GetSurfaceMesh(
		surface_id,
		vertex_coordinates,
		vertex_indices);
}

bool Model3dHandler::getFlattenedRegionsMeshes(std::vector<double>& vertex_coordinates, std::vector< std::vector<std::size_t> >& element_list)
{
	//return m_model.GetSurfaceMesh(
	return m_flat_model.GetRegionsTetrahedralMeshes(
		vertex_coordinates,
		element_list);
}

bool Model3dHandler::getFlattenedLengthRegionCurveBox(std::size_t region_id, std::size_t cross_section_id,
	std::vector<double>& lower_bound_box_vlist, std::vector<std::size_t>& lower_bound_box_elist,
	std::vector<double>& upper_bound_box_vlist, std::vector<std::size_t>& upper_bound_box_elist)
{
	return m_flat_model.getLengthRegionCurveBox(region_id, cross_section_id, lower_bound_box_vlist, lower_bound_box_elist, upper_bound_box_vlist, upper_bound_box_elist);
}

bool Model3dHandler::getFlattenedWidthRegionCurveBox(std::size_t region_id, std::size_t cross_section_id,
	std::vector<double>& lower_bound_box_vlist, std::vector<std::size_t>& lower_bound_box_elist,
	std::vector<double>& upper_bound_box_vlist, std::vector<std::size_t>& upper_bound_box_elist)
{
	return m_flat_model.getWidthRegionCurveBox(region_id, cross_section_id, lower_bound_box_vlist, lower_bound_box_elist, upper_bound_box_vlist, upper_bound_box_elist);
}

bool Model3dHandler::getFlattenedWidthCrossSectionCurve(int surface_id, int cross_section_id,
	std::vector<double>& vertex_coordinates, std::vector<std::size_t>& vertex_indices)
{
	return m_flat_model.GetWidthCrossSectionCurve(surface_id, cross_section_id, vertex_coordinates, vertex_indices);
}

bool Model3dHandler::getFlattenedLengthCrossSectionCurve(int surface_id, int cross_section_id,
	std::vector<double>& vertex_coordinates, std::vector<std::size_t>& vertex_indices)
{
	return m_flat_model.GetLengthCrossSectionCurve(surface_id, cross_section_id, vertex_coordinates, vertex_indices);
}

bool Model3dHandler::FlattenSurface(int surface_id, double height)
{
	return m_flat_model.FlattenSurface(surface_id, height);
}

void Model3dHandler::StopFlattening()
{
	m_flat_model.StopFlattening();
}

bool Model3dHandler::isFlatteningActive()
{
	return m_flat_model.FlatteningIsActive();
}

void Model3dHandler::ComputeBBoxHeights(double& min_height, double& max_height)
{
	m_flat_model.ComputeBBoxHeights(min_height, max_height);
}

double Model3dHandler::computeCurveAverageHeight(const std::vector<double>& curve2d_points)
{
	return m_flat_model.computeCurveAverageHeight(curve2d_points);
}

bool Model3dHandler::CreateFlattenedSurface(int surface_id, const std::vector<double>& surface_points, double smoothing_factor)
{
	return m_flat_model.CreateSurface(surface_id, surface_points, smoothing_factor);
}

bool Model3dHandler::CreateFlattenedLengthPathGuidedSurface(int surface_id, const std::vector<double>& cross_section_curve,
	double cross_section_position, const std::vector<double>& path_curve)
{
	return m_flat_model.CreateLengthPathGuidedSurface(surface_id, cross_section_curve, cross_section_position, path_curve);
}

bool Model3dHandler::CreateFlattenedWidthPathGuidedSurface(int surface_id, const std::vector<double>& cross_section_curve,
	double cross_section_position, const std::vector<double>& path_curve)
{
	return m_flat_model.CreateWidthPathGuidedSurface(surface_id, cross_section_curve, cross_section_position, path_curve);
}

bool Model3dHandler::CreateFlattenedLengthExtrudedSurface(int surface_id, const std::vector<double>& cross_section_curve,
	double cross_section_position, const std::vector<double>& path_curve)
{
	return m_flat_model.CreateLengthExtrudedSurface(surface_id, cross_section_curve, cross_section_position, path_curve);
}

bool Model3dHandler::CreateFlattenedWidthExtrudedSurface(int surface_id, const std::vector<double>& cross_section_curve,
	double cross_section_position, const std::vector<double>& path_curve)
{
	return m_flat_model.CreateWidthExtrudedSurface(surface_id, cross_section_curve, cross_section_position, path_curve);
}

bool Model3dHandler::CreateFlattenedLengthExtrudedSurface(int surface_id, const std::vector<double>& curve_points)
{
	return m_flat_model.CreateLengthExtrudedSurface(surface_id, curve_points);
}

bool Model3dHandler::CreateFlattenedWidthExtrudedSurface(int surface_id, const std::vector<double>& curve_points)
{
	return m_flat_model.CreateWidthExtrudedSurface(surface_id, curve_points);
}

bool Model3dHandler::RequestFlattenedPreserveRegion(const std::vector<double>& points)
{
	return m_flat_model.RequestPreserveRegion(points);
}

bool Model3dHandler::RequestFlattenedPreserveAbove(const std::vector<double>& sketch_points)
{
	return m_flat_model.RequestPreserveAbove(sketch_points);
}

bool Model3dHandler::RequestFlattenedPreserveBelow(const std::vector<double>& sketch_points)
{
	return m_flat_model.RequestPreserveBelow(sketch_points);
}

bool Model3dHandler::getFlattenedLengthPreserveAboveCurveBox(int cross_section_id, std::vector<double>& vlist, std::vector<size_t>& elist)
{
	return m_flat_model.getLengthPreserveAboveCurveBox(cross_section_id, vlist, elist);
}

bool Model3dHandler::getFlattenedWidthPreserveAboveCurveBox(int cross_section_id, std::vector<double>& vlist, std::vector<size_t>& elist)
{
	return m_flat_model.getWidthPreserveAboveCurveBox(cross_section_id, vlist, elist);
}

bool Model3dHandler::getFlattenedLengthPreserveBelowCurveBox(int cross_section_id, std::vector<double>& vlist, std::vector<size_t>& elist)
{
	return m_flat_model.getLengthPreserveBelowCurveBox(cross_section_id, vlist, elist);
}

bool Model3dHandler::getFlattenedWidthPreserveBelowCurveBox(int cross_section_id, std::vector<double>& vlist, std::vector<size_t>& elist)
{
	return m_flat_model.getWidthPreserveBelowCurveBox(cross_section_id, vlist, elist);
}



model::SModellerWrapper& Model3dHandler::model()
{ 
	return m_model; 
}

void Model3dHandler::removeAbove()
{
	////qDebug() << "Model3dHandler::removeAbove() - setting enabled!";
	enableRule();
	m_removeRuleId = GeologicRules::Operator::RA;//51;
	m_model.removeAbove();
}

void Model3dHandler::removeBelow()
{
	////qDebug() << "Model3dHandler::removeBelow() - setting enabled!";
	enableRule();
	m_removeRuleId = GeologicRules::Operator::RB;//52;
	m_model.removeBelow();
}

void Model3dHandler::removeAboveIntersection()
{
	////qDebug() << "Model3dHandler::removeAboveIntersection() - setting enabled!";
	enableRule();
	m_removeRuleId = GeologicRules::Operator::RAI;//53;
	m_model.removeAboveIntersection();
}

void Model3dHandler::removeBelowIntersection()
{
	////qDebug() << "Model3dHandler::removeBelowIntersection() - setting enabled!";
	enableRule();
	m_removeRuleId = GeologicRules::Operator::RBI;//54;
	m_model.removeBelowIntersection();
}

void Model3dHandler::enableRule()
{
	m_ruleEnabled = true;
}

void Model3dHandler::disableRule()
{
	m_ruleEnabled = false;
}

bool Model3dHandler::isRuleEnabled()
{
	return m_ruleEnabled;
}

int Model3dHandler::getRemoveRuleId()
{
	return m_removeRuleId;
}

void Model3dHandler::setRemoveRuleId(int ruleId)
{
	switch (ruleId)
	{
	case GeologicRules::Operator::RA:
		removeAbove();
		break;
	case GeologicRules::Operator::RB:
		removeBelow();
		break;
	case GeologicRules::Operator::RAI:
		removeAboveIntersection();
		break;
	case GeologicRules::Operator::RBI:
		removeBelowIntersection();
		break;
	}
}

bool Model3dHandler::isPreserveRegionActive()
{
	return m_model.PreserveRegionIsActive();
}

void Model3dHandler::stopPreserveRegion()
{
	m_model.StopPreserveRegion();
}

void Model3dHandler::stopPreserveAbove()
{
	m_model.stopPreserveAbove();
}

void Model3dHandler::stopPreserveBelow()
{
	m_model.stopPreserveBelow();
}

bool Model3dHandler::isPreserveAboveActive()
{
	return m_model.PreserveAboveIsActive();
	
}

bool Model3dHandler::isPreserveBelowActive()
{
	return m_model.PreserveBelowIsActive();

}

void Model3dHandler::resetNew()
{
	this->reset();
	emit(clearCompleted());
}

std::vector<std::size_t> Model3dHandler::getOrderedSurfaceIndices()
{
	return  m_model.getOrderedSurfacesIndices();
}

std::size_t Model3dHandler::getTetraHedralMesh(
									std::vector<double> vertex_coordinates,
									std::vector<std::size_t> element_list,
									std::vector<long int> attribute_list)
{
	return m_model.getTetrahedralMesh(vertex_coordinates, element_list, attribute_list);
}

bool Model3dHandler::undo()
{
	if (m_model.canUndo())
	{
		if (m_model.undo())
		{
			emit(undoCompleted());
			return true;
		}
	}
	return false;
}

bool Model3dHandler::redo()
{
	if (m_model.canRedo())
	{
		if (m_model.redo())
		{
			emit(redoCompleted());
			return true;
		}

	}

	return false;
}

bool Model3dHandler::exportToIrapGrid(const std::string& project_name)
{
	if (model::SModellerWrapper::Instance().exportToIrapGrid(project_name))
	{
		//qDebug() << "Model3dHandler:: export success!! ";
		return true;
	}
	else //qDebug() << "Model3dHandler:: export error! ";
	return false;
}

void Model3dHandler::setRegionsList(std::vector<double> list)
{
	m_region_list = list;

	//- Update the total volume of the model
	double volume = 0.0;
	for (int i = 0; i < m_region_list.size(); i++)
	{
		volume = volume + m_region_list[i];		
	}
	m_total_volume = volume;

	//- Update the region list that stores the volume in proportion
	m_region_list_proportion.clear();
	for (int i = 0; i < m_region_list.size(); i++)
	{
		m_region_list_proportion.push_back((m_region_list[i] / m_total_volume) * 100);
	}
}

std::vector<double> Model3dHandler::getRegionList()
{
	return m_region_list;
}

bool Model3dHandler::surfaceIsStratigraphy(int id)
{
	std::string name;
	std::string description = "stratigraphy";
	std::optional<QColor> qcolor;

	return model::MetadataAccess::setSurfaceMetadata(id, name, description, qcolor);
}
bool Model3dHandler::surfaceIsStructure(int id)
{
	std::string name;
	std::string description = "structure";
	std::optional<QColor> qcolor;

	return model::MetadataAccess::setSurfaceMetadata(id, name, description, qcolor);
}
std::vector <int> Model3dHandler::getSurfaceType()
{
	auto sufaceIndices = m_model.GetSurfaceIndices();
	
	std::vector <int> surfaceTypes;
	for(auto i: sufaceIndices)
	{
		std::string name;
		std::string description;
		std::optional<QColor> qcolor;

		bool success = model::MetadataAccess::getSurfaceMetadata(i, name, description, qcolor);
		//qDebug() << "TESTMODEL: " << success << ", desc:" << description.c_str();

		
		if (description == "stratigraphy")
			surfaceTypes.push_back(0);

		else if (description == "structure")
			surfaceTypes.push_back(1);

		else surfaceTypes.push_back(-1);
		
	}
	return surfaceTypes;
}

int Model3dHandler::getSurfaceTypeById(int id)
{
	auto sufaceIndices = m_model.GetSurfaceIndices();

	std::vector <int> surfaceTypes;
	for (auto i : sufaceIndices)
	{
		if (i == id)
		{
			std::string name;
			std::string description;
			std::optional<QColor> qcolor;

			bool success = model::MetadataAccess::getSurfaceMetadata(i, name, description, qcolor);
			////qDebug() << "TESTMODEL: " << success << ", desc:" << description.c_str();


			if (description == "stratigraphy")
				return 1;

			else if (description == "structure")
				return 2;

		}

	}
	return -1;
}

void Model3dHandler::clearDomains()
{
	//MMA::clearIntrepetation();
	model::MetadataAccess::clearIntrepetation();

}

void Model3dHandler::enforceDomainConsistency()
{
	//MMA::clearIntrepetation();
	model::MetadataAccess::enforceDomainsConsistency();
}

void Model3dHandler::enforceRegionConsistency()
{
	//MMA::clearIntrepetation();
	model::MetadataAccess::enforceRegionsConsistency();
}

bool Model3dHandler::eraseDomain(int domain_id)
{
	//return MMA::interpretation().eraseDomain(domain_id);
	return model::MetadataAccess::interpretation().eraseDomain(domain_id);
}

void Model3dHandler::setDomain(int domain_id, const QString& name, const QString& description)
{
	//auto& domain = MMA::domains()[domain_id];
	auto& domain = model::MetadataAccess::domains()[domain_id];
	domain.metadata.name = name.toStdString();
	domain.metadata.description = description.toStdString();
}

void Model3dHandler::setRegion(int region_id, const QString& name, const QString& description)
{
	//auto& domain = MMA::domains()[domain_id];
	auto& region = model::MetadataAccess::regions()[region_id];
	region.metadata.name = name.toStdString();
	region.metadata.description = description.toStdString();
}

// Notice that here we use the 'find()' method instead of 'operator[]'
// (as we did in 'setDomain()'), because the later would create a 
// domain if it could not find any with 'domain_id' in the model.
bool Model3dHandler::setDomainColor(int domain_id, QColor c)
{
	//auto iter = MMA::domains().find(domain_id);
	auto iter = model::MetadataAccess::domains().find(domain_id);
	if (iter != model::MetadataAccess::domains().end())
	{
		auto& [id, domain] = *iter;
		model::MetadataAccess::setColor(domain, c);
		return true;
	}
	return false;
}

bool Model3dHandler::setRegionColor(int region_id, QColor c)
{
	auto iter = model::MetadataAccess::regions().find(region_id);
	if (iter != model::MetadataAccess::regions().end())
	{
		auto& [id, region] = *iter;
		model::MetadataAccess::setColor(region, c);
		return true;
	}
	return false;
}

bool Model3dHandler::addRegionIdToDomain(int domain_id, int region_id)
{
	auto iter = model::MetadataAccess::domains().find(domain_id);
	if (iter != model::MetadataAccess::domains().end())
	{
		auto& [id, domain] = *iter;
		auto region = stratmod::Region::Get(region_id);
		if (region)
		{
			domain.regions.insert(region_id);
			return true;
		}
	}
	return false;
}

bool Model3dHandler::eraseRegionIdFromDomain(int domain_id, int region_id)
{
	auto iter = model::MetadataAccess::domains().find(domain_id);
	if (iter != model::MetadataAccess::domains().end())
	{
		auto& [id, domain] = *iter;
		auto region = stratmod::Region::Get(region_id);
		if (region)
		{
			domain.regions.erase(region_id);
			return true;
		}
	}
	return false;
}

int Model3dHandler::getDomainIdByRegion(int region_id)
{
	for (auto& [i, domain_i] : model::MetadataAccess::domains())
	{
		auto regions_domain_i = domain_i.regions;
		std::set<stratmod::Region> ::iterator setIterator;

		for (setIterator = regions_domain_i.begin(); setIterator != regions_domain_i.end(); setIterator++)
		{
			if (region_id == setIterator->id())
			{
				return i;
			}
		}
	}

	return -1;
}

std::vector<int> Model3dHandler::getRegionIdsByDomain(int domain_id)
{
	std::vector<int> regionIdList;
	

	auto iter = model::MetadataAccess::domains().find(domain_id);
	if (iter != model::MetadataAccess::domains().end())
	{
		auto& [id, domain] = *iter;
		std::set <stratmod::Region> regionSet;
		regionSet = domain.regions;
		std::set<stratmod::Region> ::iterator setIterator;
		
		for (setIterator = regionSet.begin(); setIterator != regionSet.end(); setIterator++)
		{
			//Retrieve regin id list
			regionIdList.push_back(setIterator->id());
		}

		return regionIdList;
	}

	return {};
}

std::vector<int> Model3dHandler::getDomainList()
{
	//return model::MetadataAccess::interpretation().getConsistentDomainsIndices();
	return model::MetadataAccess::interpretation().getDomainsIndices();
}

void Model3dHandler::getDomainMetadataById(int domain_id, QString& name, QString& description, QString& color)
{
	auto iter = model::MetadataAccess::domains().find(domain_id);
	if (iter != model::MetadataAccess::domains().end())
	{
		auto& [id, domain] = *iter;
			
		QColor c;
		model::MetadataAccess::getColor(domain, c);
		
		name = QString::fromUtf8((domain.metadata.name).c_str());
		description = QString::fromUtf8((domain.metadata.description).c_str());
		
		color = c.name();

	}

	
}

void Model3dHandler::getRegionMetadataById(int region_id, QString& name, QString& description, QString& color)
{
	auto iter = model::MetadataAccess::regions().find(region_id);
	if (iter != model::MetadataAccess::regions().end())
	{
		auto& [id, region] = *iter;

		name = QString::fromUtf8((region.metadata.name).c_str());
		description = QString::fromUtf8((region.metadata.description).c_str());

		QColor c;
		if (model::MetadataAccess::getColor(region, c))
			color = c.name();
		else color = "";
	}
}

void Model3dHandler::resetRegions()
{
	model::MetadataAccess::resetRegions();

}

bool Model3dHandler::getSurfaceData(std::vector<int>& idList, std::vector<std::string>& nameList, std::vector<std::string>& descriptionList, std::vector<QColor>& colorList, std::vector<int>& typeList, std::vector<int>& groupIdList)
{
	auto sufaceIndices = m_model.GetSurfaceIndices();
	std::vector<int> surfaceIndices = m_model.GetSurfaceIndices();

	int j = 0;
	for (int i = 0; i < sufaceIndices.size(); i++)
	{
		idList[j] = sufaceIndices[i];

		std::string name;
		std::string description;
		std::optional<QColor> qcolor;

		bool success = model::MetadataAccess::getSurfaceMetadata(i, name, description, qcolor);
		////qDebug() << "TESTMODEL: " << success << ", desc:" << description.c_str();
		nameList[j] = name;
		descriptionList[j] = description;
		colorList[j] = qcolor.value();

		if (description == "stratigraphy")
		{
			typeList[j] = 0;
		}
		else if (description == "structure")
		{
			typeList[j] = 1;
		}

		else {
			typeList[j] = -1;
		}

		j++;
	}
	return true;
}

void Model3dHandler::setSurfaceGroup(int surfaceId, int groupId)
{
	stratmod::SurfaceUniqueIdentifier unique_identifier = surfaceId;
	stratmod::SurfaceGroupIdentifier group_identifier = groupId;

	stratmod::SurfaceDescriptor descriptor(unique_identifier, group_identifier);

	stratmod::SModeller2::Instance().setSurfaceDescriptor(descriptor);
}

int Model3dHandler::getSurfaceGroup(int surfaceId)
{
	stratmod::SurfaceUniqueIdentifier surface_unique_identifier = surfaceId;

	std::optional<stratmod::SurfaceDescriptor> descriptor = stratmod::SModeller2::Instance().getSurfaceDescriptor(surface_unique_identifier);

	if (descriptor.has_value() && descriptor.value().groupId().has_value())
		return descriptor.value().groupId().value();
	return -1;

}
///
void Model3dHandler::getGroupMetadataById(int groupId, QString& name, QString& description, QString& color)
{
	//Find Group identifier
	stratmod::SurfaceGroupIdentifier group_identifier = groupId;
	std::optional<stratmod::SurfaceDescriptor> group_descriptor = stratmod::SModeller2::Instance().getSurfaceDescriptor(group_identifier);

	if (group_descriptor.has_value() && group_descriptor.value().groupId().has_value() )
	{
		//return group_descriptor.value().groupId().value();
		stratmod::SurfaceMetadata groupMetadata = group_descriptor.value().metadata;
	
		std::string name_string;
		std::string description_string;
		std::optional<QColor> qcolor;
		model::MetadataAccess::getSurfaceMetadata(groupMetadata, name_string, description_string, qcolor);
		
		name = QString::fromUtf8(name_string.c_str());
		description = QString::fromUtf8(description_string.c_str());
		
		if (qcolor.has_value())
			color = (qcolor.value()).rgba();
		else color = "";
		
	}

}

void Model3dHandler::setGroupMetadataById(int groupId, QString name, QString description, QString color)
{
	stratmod::SurfaceGroupIdentifier group_identifier = groupId;
	std::optional<stratmod::SurfaceDescriptor> group_descriptor = stratmod::SModeller2::Instance().getSurfaceDescriptor(group_identifier);
	
	if (group_descriptor.has_value() && group_descriptor.value().groupId().has_value())
	{
		stratmod::SurfaceMetadata groupMetadata = group_descriptor.value().metadata;

		std::string name_string = name.toLocal8Bit().constData();
		std::string description_string = description.toLocal8Bit().constData();
		const std::optional<QColor> qColor = color;

		model::MetadataAccess::setSurfaceMetadata(groupMetadata, name_string, description_string, qColor);
	}
}

std::vector<int> Model3dHandler::getChildSurfacesOfAGroup(int groupId)
{
	stratmod::SurfaceGroupIdentifier groupIdentifier = groupId;
	
	std::vector<stratmod::SurfaceDescriptor> surface_descriptor_list = stratmod::SModeller2::Instance().getSurfaceDescriptors(groupIdentifier);

	std::vector<int> childSurfaceList;
	for (const stratmod::SurfaceDescriptor& eachDescriptor : surface_descriptor_list)
	{
        //
        // The following line does not make sense to me, the group id of each
        // descriptor is exactly the same as the argument of this method
        // (groupId).  I think you mean to use the surfaces' unique ids.
        //
		/* childSurfaceList.push_back(eachDescriptor.group_id); */
		childSurfaceList.push_back(eachDescriptor.uniqueId());
	}
	return childSurfaceList;
}

bool Model3dHandler::setSurfaceMetadataById(int surfaceId, QString name, QString description, QString color )
{
	std::string name_string = name.toLocal8Bit().constData();
	std::string description_string = description.toLocal8Bit().constData();
	QColor color_string = color;

	return model::MetadataAccess::setSurfaceMetadata(surfaceId, name_string, description_string, color_string);
}

surfaceMetadata Model3dHandler::getSurfaceMetadataById(int surfaceId)
{
	std::string name;
	std::string description;
	std::optional<QColor> qcolor;
	int category;

	bool success = model::MetadataAccess::getSurfaceMetadata(surfaceId, name, description, qcolor);

	surfaceMetadata data;
	if (success)
	{
		if (description == "stratigraphy")
			category = 0;
		else if (description == "structure")
			category = 1;
		else
			category = -1;

		data.id = surfaceId;
		data.name = QString::fromUtf8(name.c_str());

		data.category = category;

		if (qcolor.has_value())
		{
			data.color = (qcolor.value()).rgba();
		}
		
	}
	else {
		data.id = surfaceId;
		data.name = "";
		data.color = "";
	}
	return data;
}

void Model3dHandler::setSurfaceData(int id, QString name, QString type, QString color)
{
	std::string name_string = name.toLocal8Bit().constData();
	std::string description_string = type.toLocal8Bit().constData();
	std::optional<QColor> qcolor = color;

	bool response = model::MetadataAccess::setSurfaceMetadata(id, name_string, description_string, qcolor);

	/*if (response)
	{
		//qDebug() << "Model3dHandler::setSurfaceData: Metadata saving successful for id: " << id;

	}
	else //qDebug() << "Model3dHandler::setSurfaceData: Metadata saving error for id: " << id;
	*/
}

void Model3dHandler::getSurfaceData(int& id, QString& name, QString& type, QString& color)
{
	std::string name_string;
	std::string description_string;
	std::optional<QColor> qcolor;

	bool response = model::MetadataAccess::getSurfaceMetadata(id, name_string, description_string, qcolor);
	/*if (response)
	{
		////qDebug() << "Model3dHandler::getSurfaceData: Metadata: id=" << id << ", name=" << name_string.c_str() << ", type=" << description_string.c_str() << ", color=" << (qcolor.value()).rgba();
		//qDebug() << "Model3dHandler::getSurfaceData: Metadata access successful for id=" << id;
	}
	else //qDebug() << "Model3dHandler::getSurfaceData: Metadata access error for id=" <<id;
	*/
	name = QString::fromUtf8(name_string.c_str());
	type = QString::fromUtf8(description_string.c_str());

	if (qcolor.has_value())
		//color = (qcolor.value()).rgba();
		color = (qcolor.value()).name();
	else color = "";
}

bool Model3dHandler::getVolumeAttributesFromPointList(const std::vector<double>& vcoords, std::vector<int>& regions)
{
	return m_model.getVolumeAttributesFromPointList(vcoords, regions);
}

double Model3dHandler::getModelVolume()
{
	return m_total_volume;
}

std::vector<double> Model3dHandler::getRegionListProportion()
{
	return m_region_list_proportion;
}

int Model3dHandler::getNumberOfRegions()
{
	return m_region_list.size();
}

double Model3dHandler::getRegionVolumeProportion(int regionID)
{
	if (regionID < m_region_list_proportion.size())
	{
		return m_region_list_proportion[regionID];
	}
	
	return 0.0;
}

void Model3dHandler::setDomainList(std::vector<double> list)
{
	m_domain_list = list;
	//- Update the domain list that stores the volume in proportion
	m_domain_list_proportion.clear();
	for (int i = 0; i < m_domain_list.size(); i++)
	{
		m_domain_list_proportion.push_back((m_domain_list[i] / m_total_volume) * 100);
	}
}

std::vector<double> Model3dHandler::getDomainListProportion()
{
	return m_domain_list_proportion;
}

int Model3dHandler::getNumberOfDomains()
{
	return m_domain_list.size();
}

double Model3dHandler::getDomainVolumeProportion(int domainID)
{
	if (domainID < m_domain_list_proportion.size())
	{
		return m_domain_list_proportion[domainID];
	}

	return 0.0;
}

QString Model3dHandler::getDomainName(int domainID)
{
	auto iter = model::MetadataAccess::domains().find(domainID);
	if (iter != model::MetadataAccess::domains().end())
	{
		auto& [id, domain] = *iter;

		return QString::fromUtf8((domain.metadata.name).c_str());
	}
}