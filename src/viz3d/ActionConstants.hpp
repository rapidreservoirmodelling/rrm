#ifndef ACTIONCONSTANTS_H
#define ACTIONCONSTANTS_H

#include <QObject>

/**
* Class ThreeDViewActions
* It contains the actions from the 3DView window.
*/

class ThreeDViewActions : public QObject
{
	Q_OBJECT
public:
	enum Action { NO_ACTION, 
		SURFACES_ADD, 
		SURFACES_REMOVE, 
		PREVIEW_ADD, 
		PREVIEW_REMOVE, 
		REGIONS_ADD, 
		REGIONS_REMOVE, 
		DOMAINS_REMOVE_SINGLE_REGION,
		CLEAR,
		CHANGE_COLOR,
		CHANGE_VISIBILITY,
		CHANGE_VISIBILITY_SURFACE_TYPE,
		DOMAINS_REMOVE_ALL,
		CHANGE_SURFACE_VISIBILITY_LIST,
		DOMAINS_REMOVE_SINGLE_DOMAIN
	};
	Q_ENUM(Action)

private:
	//- Constructor private to prevent instantiation
	explicit ThreeDViewActions();
};

/**
* Class ObjectCategories
* It contains object categories handled inside the 3DView window.
*/

class ObjectCategories : public QObject
{
	Q_OBJECT
public:
	enum Name { NONE, 
		STRATIGRAPHY, STRUCTURE, 
		REGION, DOMAIN_VOLUME, REGION_IN_DOMAIN,
		STRATIGRAPHIC_CHILD, STRUCTURAL_CHILD
	};
	Q_ENUM(Name)

private:
	//- Constructor private to prevent instantiation
	explicit ObjectCategories();
};

#endif
