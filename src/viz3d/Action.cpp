#include "Action.h"

#include "Surface3d.h"
#include "Region3d.h"

#include "CommandProcessor.h"
#include "Surface3dRenderer.h"
#include "ActionConstants.hpp"

#include <QMutex>

Action::Action(int actionId, std::shared_ptr<CommandProcessor> commandProcessor, Surface3dRenderer* vtkFboRenderer)
{
	m_action_id = actionId;
	m_commandProcessor = commandProcessor;
	m_vtkFboRenderer = vtkFboRenderer;
}

void Action::setup(QThread* cthread)
{
	m_thread = cthread;
	connect(m_thread, SIGNAL(started()), this, SLOT(run()));
	connect(m_thread, SIGNAL(finished()), this, SLOT(execute()));
}

void Action::SetColorChangeParameter(int index, int type, QString color)
{
	object_index = index;
	object_type_id = type;
	object_color = color;
	
}

void Action::SetVisibilityChangeParameter(int index, int type, bool visibility)
{
	object_index = index;
	object_type_id = type;
	object_visibility_flag = visibility;
}

void Action::SetSurfaceVisibilityChangeParameter(std::vector<int> indices, int type, bool visibility)
{
	object_indices = indices;
	object_type_id = type;
	object_visibility_flag = visibility;
}

void Action::SetVisibilityChangeParameter(int type, bool visibility)
{
	object_type_id = type;
	object_visibility_flag = visibility;
}

void Action::SetParameters4RemoveRegionFromDomain(int region_id, int domain_id)
{
	m_region_id = region_id;
	m_domain_id = domain_id;
}

void Action::SetParameters4RemoveDomain(int domain_id)
{
	m_domain_id = domain_id;
}

void Action::run()
{
	switch(m_action_id)
	{
		case ThreeDViewActions::Action::SURFACES_ADD :
			m_surfaces = m_commandProcessor->addSurfaces();
			
			break;
		
		case ThreeDViewActions::Action::SURFACES_REMOVE :
			m_surfaces = m_commandProcessor->getAllSurfaces();
			m_previewedSurfaces = m_commandProcessor->getAllPreviewedSurfaces();

			m_commandProcessor->removeSurfaces();
			m_commandProcessor->removeSurfacePreview();
			
			break;
		
		case ThreeDViewActions::Action::REGIONS_ADD :
			m_regions = m_commandProcessor->addRegions();
			break;
			
		case ThreeDViewActions::Action::REGIONS_REMOVE :
			m_regions = m_commandProcessor->getAllRegions();

			m_commandProcessor->removeRegions();
	
			break;
			
		case ThreeDViewActions::Action::PREVIEW_ADD :
			m_previewedSurfaces = m_commandProcessor->addSurfacePreview();
			
			break;
		
		case ThreeDViewActions::Action::PREVIEW_REMOVE :
			m_previewedSurfaces = m_commandProcessor->getAllPreviewedSurfaces();
			
			m_commandProcessor->removeSurfacePreview();
	
			break;
		
		case ThreeDViewActions::Action::DOMAINS_REMOVE_SINGLE_REGION:
			m_domain_3d_List = m_commandProcessor->getAllDomainRegions();

			m_commandProcessor->removeRegionFromDomainList(m_region_id, m_domain_id);

			break;
		
		case ThreeDViewActions::Action::DOMAINS_REMOVE_SINGLE_DOMAIN:
			m_domain_3d_List = m_commandProcessor->getAllDomainRegionsByDomainId(m_domain_id);
			
			m_commandProcessor->removeRegionFromDomainList(m_region_id, m_domain_id);
			
			break;

		case ThreeDViewActions::Action::CLEAR:
			//Clear surfaces
			m_surfaces = m_commandProcessor->getAllSurfaces();
			////qDebug() << "CommandClear::run2 = " << m_surfaces.size();
			m_commandProcessor->removeSurfaces();

			//Clear regions
			m_regions = m_commandProcessor->getAllRegions();
			m_commandProcessor->removeRegions();

			//Clear Previewed surface if there is any
			m_previewedSurfaces = m_commandProcessor->getAllPreviewedSurfaces();
			m_commandProcessor->removeSurfacePreview();

			//Clear domains
			m_domain_regions = m_commandProcessor->getAllDomainRegions();
			m_commandProcessor->clearDomainList();

			break;
		case ThreeDViewActions::Action::CHANGE_COLOR:
			switch (object_type_id)
			{
				case ObjectCategories::Name::STRATIGRAPHY://case 1:
					m_update_status = m_commandProcessor->updateSurfaceColor(object_index, object_color);
					m_surfaces = m_commandProcessor->getAllSurfaces();

					break;
				
				case ObjectCategories::Name::STRUCTURE://case 2:
					m_update_status = m_commandProcessor->updateSurfaceColor(object_index, object_color);
					m_surfaces = m_commandProcessor->getAllSurfaces();

					break;
				
				case ObjectCategories::Name::REGION://case 3:
					m_update_status = m_commandProcessor->updateRegionColor(object_index, object_color);
					m_regions = m_commandProcessor->getAllRegions();

					break;

				case ObjectCategories::Name::DOMAIN_VOLUME://case 4:
					m_update_status = m_commandProcessor->updateDomainColor(object_index, object_color);
					m_domain_regions = m_commandProcessor->getAllDomainRegions();

					break;

				case ObjectCategories::Name::REGION_IN_DOMAIN://case 5:
					m_update_status = m_commandProcessor->updateRegionColor(object_index, object_color);
					m_regions = m_commandProcessor->getAllRegions();

					//m_domain_region_update_status = m_commandProcessor->updateDomainColor(object_index, object_color);
					//m_domain_regions = m_commandProcessor->getAllDomainRegions();
					
					break;
				default:
					break;
			}
			break;
		case ThreeDViewActions::Action::CHANGE_VISIBILITY:
			switch (object_type_id)
			{
				case ObjectCategories::Name::STRATIGRAPHY://case 1:
					////qDebug() << "Action::case 1: index=" << object_index << ", object type_id: " << object_type_id;
					m_surfaces = m_commandProcessor->getAllSurfaces();
					m_commandProcessor->updateSurfaceVisibility(object_index, object_visibility_flag);
					break;

				case ObjectCategories::Name::STRUCTURE://case 2:
					////qDebug() << "Action::case 2: index=" << object_index << ", object type_id: " << object_type_id;
					m_surfaces = m_commandProcessor->getAllSurfaces();
					m_commandProcessor->updateSurfaceVisibility(object_index, object_visibility_flag);
					break;

				case ObjectCategories::Name::REGION://case 3:
					m_regions = m_commandProcessor->getAllRegions();
					m_commandProcessor->updateRegionVisibility(object_index, object_visibility_flag);
					break;

				case ObjectCategories::Name::DOMAIN_VOLUME://case 4:
					//m_domain_regions = m_commandProcessor->getAllDomainRegions();
					////qDebug() << "Action::case 4: index=" << object_index << ", object type_id: " << object_type_id;
					m_domain_regions = m_commandProcessor->getAllDomainRegionsByDomainId(object_index);
					m_commandProcessor->updateDomainVisibility(object_index, object_visibility_flag);

					break;
				case ObjectCategories::Name::REGION_IN_DOMAIN://case 5:
					m_regions = m_commandProcessor->getAllRegions();
					m_commandProcessor->updateRegionVisibility(object_index, object_visibility_flag);

					break;
				default:
					break;
			}// end: switch (object_type_id)
			break;
		case ThreeDViewActions::Action::CHANGE_VISIBILITY_SURFACE_TYPE:
			m_surfaces = m_commandProcessor->getAllSurfaces();
			m_commandProcessor->updateSurfaceVisibilityByTypeId(object_type_id, object_visibility_flag);

			break;

		case ThreeDViewActions::Action::DOMAINS_REMOVE_ALL:
			m_domain_3d_List = m_commandProcessor->getAllDomainRegions();
			m_commandProcessor->clearDomainList();

			break;

		case ThreeDViewActions::Action::CHANGE_SURFACE_VISIBILITY_LIST:
			m_surfaces = m_commandProcessor->getAllSurfaces();
			m_commandProcessor->updateSurfaceVisibilityByIdList(object_indices, object_type_id, object_visibility_flag);

			break;

	}//end: switch(action_id)
	
	m_ready = true;
	emit ready();
}


//bool Action::isReady() const
bool Action::isReady() 
{
	return m_ready;
}

void Action::execute()
{
	switch(m_action_id)
	{
		case ThreeDViewActions::Action::SURFACES_ADD :
			for each (m_surface in m_surfaces)
			{
				m_vtkFboRenderer->addActor(m_surface);
			}
			
			break;
		
		case ThreeDViewActions::Action::SURFACES_REMOVE :
			for each (m_surface in m_surfaces)
			{
				m_vtkFboRenderer->removeActor(m_surface);
			}
			
			for each (m_surface in m_previewedSurfaces)
			{
				m_vtkFboRenderer->removeActor(m_surface);
			}
			
			break;
		
		case ThreeDViewActions::Action::REGIONS_ADD :
			for each (m_region in m_regions)
			{
				//qDebug() << "ThreeDViewActions::Action::REGIONS_ADD: " << m_region->getIndex();
				m_vtkFboRenderer->addActor(m_region);
			}
			break;
			
		case ThreeDViewActions::Action::REGIONS_REMOVE :
			for each (m_region in m_regions)
			{
				m_vtkFboRenderer->removeActor(m_region);
			}
			break;
			
		case ThreeDViewActions::Action::PREVIEW_ADD :
			for each (m_surface in m_previewedSurfaces)
			{
				m_vtkFboRenderer->addActor(m_previewedSurfaces.at(m_previewedSurfaces.size()-1));
			}
			
			break;
		
		case ThreeDViewActions::Action::PREVIEW_REMOVE :
			if (m_previewedSurfaces.size() != 0)
			{
				for each (m_surface in m_previewedSurfaces)
				{
					m_vtkFboRenderer->removeActor(m_surface);
				}
			}
	
			break;
		
		case ThreeDViewActions::Action::DOMAINS_REMOVE_SINGLE_REGION:
			for each (m_region in m_domain_3d_List)
			{
				if ((m_region->getIndex() == m_region_id) && (m_region->getParentIndex() == m_domain_id))
				{
					m_vtkFboRenderer->removeActor(m_region);
				}
			}

			break;
		
		case ThreeDViewActions::Action::DOMAINS_REMOVE_SINGLE_DOMAIN:
			for each (m_region in m_domain_3d_List)
			{
				if (m_region->getParentIndex() == m_domain_id)
				{
					m_vtkFboRenderer->removeActor(m_region);
				}
			}

			break;
		case ThreeDViewActions::Action::DOMAINS_REMOVE_ALL:
			for each (m_region in m_domain_3d_List)
			{
				//if ((m_region->getIndex() == m_region_id) && (m_region->getParentIndex() == m_domain_id))
				//{
					m_vtkFboRenderer->removeActor(m_region);
				//}
			}
			break;

		case ThreeDViewActions::Action::CLEAR:
			//Remove surfaces
			for each (m_surface in m_surfaces)
			{
				m_vtkFboRenderer->removeActor(m_surface);
			}
	
			//Remove regions
			for each (m_region in m_regions)
			{
				m_vtkFboRenderer->removeActor(m_region);
			}

			//Remove previewed surfaces
			for each (m_surface in m_previewedSurfaces)
			{
				m_vtkFboRenderer->removeActor(m_surface);
			}

			//Remove domains
			for each (m_region in m_domain_regions)
			{
				m_vtkFboRenderer->removeActor(m_region);
			}
			break;

		case ThreeDViewActions::Action::CHANGE_COLOR:
			switch (object_type_id)
			{
				case ObjectCategories::Name::STRATIGRAPHY://case 1:
					for (const std::shared_ptr<Surface3d>& m_surface : m_surfaces)
					{
						if (m_surface->getIndex() == object_index)
						{
							if (m_update_status)
							{
								m_vtkFboRenderer->removeActor(m_surface);
								m_vtkFboRenderer->addActor(m_surface);
							}
							break;
						}

					}
					break;

				case ObjectCategories::Name::STRUCTURE://case 2:
					for (const std::shared_ptr<Surface3d>& m_surface : m_surfaces)
					{
						if (m_surface->getIndex() == object_index)
						{
							if (m_update_status)
							{
								m_vtkFboRenderer->removeActor(m_surface);
								m_vtkFboRenderer->addActor(m_surface);
							}
							break;
						}

					}
					break;

				case ObjectCategories::Name::REGION://case 3:
					for (const std::shared_ptr<Region3d>& m_region : m_regions)
					{
						if (m_region->getIndex() == object_index)
						{
							if (m_update_status)
							{
								m_vtkFboRenderer->removeActor(m_region);
								m_vtkFboRenderer->addActor(m_region);
							}
							break;
						}

					}
					break;

				case ObjectCategories::Name::DOMAIN_VOLUME://case 4:
					for (const std::shared_ptr<Region3d>& m_region : m_domain_regions)
					{
						if (m_region->getParentIndex() == object_index)
						{
							if (m_update_status)
							{
								m_vtkFboRenderer->removeActor(m_region);
								m_vtkFboRenderer->addActor(m_region);
							}
							break;
						}

					}
					break;
				
				case ObjectCategories::Name::REGION_IN_DOMAIN://case 5:
					for (const std::shared_ptr<Region3d>& m_region : m_regions)
					{
						if (m_region->getIndex() == object_index)
						{
							if (m_update_status)
							{
								m_vtkFboRenderer->removeActor(m_region);
								m_vtkFboRenderer->addActor(m_region);
							}
							break;
						}

					}

					/*for (const std::shared_ptr<Region3d>& m_region : m_domain_regions)
					{
						if (m_region->getParentIndex() == object_index)
						{
							if (m_domain_region_update_status)
							{
								m_vtkFboRenderer->removeActor(m_region);
								m_vtkFboRenderer->addActor(m_region);
							}
							break;
						}

					}*/
					break;

				default:
					break;
			}
			break;
		case ThreeDViewActions::Action::CHANGE_VISIBILITY_SURFACE_TYPE:
			for (const std::shared_ptr<Surface3d>& m_surface : m_surfaces)
			{
				if (m_surface->getCategory() == object_type_id)
				{
					if (object_visibility_flag)
					{
						m_vtkFboRenderer->addActor(m_surface);
					}
					else m_vtkFboRenderer->removeActor(m_surface);

				}

			}
			break;
		case ThreeDViewActions::Action::CHANGE_SURFACE_VISIBILITY_LIST:
			
			for (const std::shared_ptr<Surface3d>& m_surface : m_surfaces)
			{
				if (std::find(object_indices.begin(), object_indices.end(), m_surface->getIndex()) != object_indices.end())
				{
					if (m_surface->getCategory() == object_type_id)
					{
						if (object_visibility_flag)
						{
							m_vtkFboRenderer->addActor(m_surface);
						}
						else m_vtkFboRenderer->removeActor(m_surface);

					}
				}

			}
			break;
		case ThreeDViewActions::Action::CHANGE_VISIBILITY:
			switch (object_type_id)
			{
				case ObjectCategories::Name::STRATIGRAPHY://case 1:
					for (const std::shared_ptr<Surface3d>& m_surface : m_surfaces)
					{
						if (m_surface->getIndex() == object_index)
						{
							if (object_visibility_flag)
							{
								m_vtkFboRenderer->addActor(m_surface);
							}
							else m_vtkFboRenderer->removeActor(m_surface);

							break;
						}

					}
					break;

				case ObjectCategories::Name::STRUCTURE://case 2:
					for (const std::shared_ptr<Surface3d>& m_surface : m_surfaces)
					{
						if (m_surface->getIndex() == object_index)
						{
							if (object_visibility_flag)
							{
								m_vtkFboRenderer->addActor(m_surface);
							}
							else m_vtkFboRenderer->removeActor(m_surface);

							break;
						}

					}
					break;

				case ObjectCategories::Name::REGION://case 3:
					for (const std::shared_ptr<Region3d>& m_region : m_regions)
					{
						if (m_region->getIndex() == object_index)
						{
							if (object_visibility_flag)
							{
								m_vtkFboRenderer->addActor(m_region);
							}
							else m_vtkFboRenderer->removeActor(m_region);

							break;
						}

					}

					break;

				case ObjectCategories::Name::DOMAIN_VOLUME://case 4:
					for (const std::shared_ptr<Region3d>& m_region : m_domain_regions)
					{
						if (m_region->getParentIndex() == object_index)
						{
							if (object_visibility_flag)
							{
								m_vtkFboRenderer->addActor(m_region);
							}
							else m_vtkFboRenderer->removeActor(m_region);

						}

					}
					break;
				case ObjectCategories::Name::REGION_IN_DOMAIN://case 5:
					for (const std::shared_ptr<Region3d>& m_region : m_regions)
					{
						if (m_region->getIndex() == object_index)
						{
							if (object_visibility_flag)
							{
								m_vtkFboRenderer->addActor(m_region);
							}
							else m_vtkFboRenderer->removeActor(m_region);

							break;
						}

					}
					break;
				default:
					break;
			} //end: switch (object_type_id)
		
			break;
	}//end: switch(m_action_id)

	emit done();
}
