/***************************************************************************

* This class is part of the 3D Visualization module of Geologic surfaces.

* Copyright (C) 2020, Fazilatur Rahman.

* It handles synchronization of geologic surfaces based on commands received 
* from QtQuick and renders changes to 3D representation of surfces by 
* coordinating with the command processor.

*****************************************************************************/

#ifndef SURFACE3DRENDERER_H
#define SURFACE3DRENDERER_H

#include <vtkActor.h>
#include <vtkCellPicker.h>
#include <vtkCubeSource.h>
#include <vtkOutlineFilter.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkGenericRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkPlane.h>
#include <vtkImplicitPlaneWidget2.h>

#include "Renderer.h"

class Surface3d;
class Region3d;
class VtkFboItem;
class CommandProcessor;
class Model3dHandler;

class Surface3dRenderer : public QObject, public Renderer
{
	Q_OBJECT

public:
	/**
	* Constructor.
	*/
	Surface3dRenderer();
	
	/**
	* Method that registers the command processor.
	* @return void
	*/
	void setCommandProcessor(const std::shared_ptr<CommandProcessor> commandProcessor);
	
	/**
	* This function is called as a result of QQuickFramebufferObject::update()
	* It is used to update the renderer with changes that have occurred in the QQuick item
	* @param item is the QQuick FrameBuffer item that contains this renderer
	* @return void
	*/
	virtual void synchronize(QQuickFramebufferObject* item);

	/**
	* This function is called when the FBO should be rendered into.
	* @return void
	*/
	virtual void render();
	
	/**
	* Method that adds the corresponding surface' actor to the renderer 
	* @param surface is the Surface3d of the target actor 
	* @return void
	*/
	void addActor(const std::shared_ptr<Surface3d> surface);

	/**
	* Method that adds the corresponding region' actor to the renderer
	* @param surface is the Region3d of the target actor
	* @return void
	*/
	void addActor(const std::shared_ptr<Region3d> region);

	/**
	* Method that removes the corresponding surface' actor from the renderer
	* @param surface is the Surface3d of the target actor
	* @return void
	*/
	void removeActor(const std::shared_ptr<Surface3d> surface);

	/**
	* Method that removes the corresponding region' actor from the renderer
	* @param region is the Region3d of the target actor
	* @return void
	*/
	void removeActor(const std::shared_ptr<Region3d> region);
	
	/**
	* Method that resets the initial camera position
	* @return void
	*/
	void resetCamera();
	
	/**
	* Method that changes the dimension of the 3D bounding box based on user input
	* @return void
	*/
	void changeDimension();

signals:
	void planeOrientationAdjusted();

private:
	/**
	* Method that initializes the background of the 3D window
	* @return void
	*/
	void InitBackground();

	/**
	* Method that initializes axes orientation marker
	* @return void
	*/
	void InitOrientationMarker();
	
	/**
	* Method that initializes 3D bounding box
	* @return void
	*/
	void InitImplicitPlaneWidget();

	/**
	* Method that adjusts the cross section plane orientation inside the 3D bounding box
	* @return void
	*/
	void adjustPlane();

	/**
	* Method that adjusts the cross section selector inside the 3D bounding box
	* @return void
	*/
	void adjustCrossSection();

	/**
	* Method that adjusts the camera position based on user input of box dimensions
	* @return void
	*/
	void adjustCamera();

	/**
	* Method that executes actions from the queue
	* @return void
	*/
	void executeActions();

	/**
	* Method that defines which vtk commands are accepted on the 3D renderer
	* @return void
	*/
	void acceptMouseEvents();

	/**
	* Method that handles mouse move action
	* @return void
	*/
	void handleMouseMovement();

	/**
	* Method that handles mouse wheel action
	* @return void
	*/
	void handleMouseWheelDelta();

	/**
	* Method that move and rotate the outline box
	* @return void
	*/
	void moveTheOutlineBox();
	
	/**
	* Method that reset the scene to look down to the X, Y axis
	* @return void
	*/
	void adjustCameraViewUp();

	//- Command processor 
	std::shared_ptr<CommandProcessor> m_commandProcessor;

	//- FrameBufferObject item that contains the VTK renderer 
	VtkFboItem *m_vtkFboItem = nullptr;

	//- VTK Render Window 
	vtkSmartPointer<vtkGenericOpenGLRenderWindow> m_vtkRenderWindow;

	//- VTK Renderer 
	vtkSmartPointer<vtkRenderer> m_renderer;

	//- Preview Renderer 
	vtkSmartPointer<vtkRenderer> m_preivew_renderer;

	//- VTK render window interactor 
	vtkSmartPointer<vtkGenericRenderWindowInteractor> m_vtkRenderWindowInteractor;
	
	//- Orienter marker widget 
	vtkSmartPointer<vtkOrientationMarkerWidget> m_orientation_marker;
	
	//- Plane widget's implicit data source 
	vtkSmartPointer<vtkCubeSource> cubeSource;

	//- Cross-section plane 
	vtkSmartPointer<vtkPlane> m_plane;

	//- Cross-section plane actor
	vtkSmartPointer<vtkActor> m_plane_actor;

	//- VTK second generation implicit plane widget 
	vtkSmartPointer<vtkImplicitPlaneWidget2> m_planeWidget;

	//- Cross-section plane representation
	vtkSmartPointer<vtkImplicitPlaneRepresentation> rep;
		
	//- Picker
	vtkSmartPointer<vtkCellPicker> m_picker;

	//- Mouse events
	std::shared_ptr<QMouseEvent> m_mouseLeftButton = nullptr;
	std::shared_ptr<QMouseEvent> m_mouseEvent = nullptr;
	std::shared_ptr<QMouseEvent> m_moveEvent = nullptr;
	std::shared_ptr<QWheelEvent> m_wheelEvent = nullptr;

	//- Camera position
	double m_camPositionX = -237.885;
	double m_camPositionY = -392.348;
	double m_camPositionZ = 369.477;

	//- Render flag
	bool m_firstRender = true;

	//- Cross-section position number
	double m_currentCrossSectionNumber = 64.0;

	//- Cross-section plane orientation marker
	int m_currentPlaneOrientationId = 2; // 1-WIDTH, 2-LENGTH, 3-HEIGHT

	//- Model3dHandler instance
	Model3dHandler* m_model3dHandler;

	//- Dimensions of the 3D bounding box
	double m_boxDimensionX = 0.0;
	double m_boxDimensionY = 0.0;
	double m_boxDimensionZ = 0.0;
	
	//- Direction parameter to push the plane
	double m_directionParameter = 0.0;

	//- Flag to keep track if dimension of the bounding box needs to be adjusted
	bool m_dimension_change_flag = false;

	//- Flag to keep track of plane orientation change 
	bool m_plane_orientation_change_flag = false;

	//- Vertical Exxageration scale factor
	double m_VE_factor = 1.0;					

	//- Flat Height factor
	double m_Flat_factor = 1.0;
};

#endif //SURFACE3DRENDERER_H
