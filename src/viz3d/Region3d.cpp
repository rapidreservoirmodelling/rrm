#include "Region3d.h"
#include "./utils/vtk_utils.h"

#include <QDebug>

#include <vtkTransformFilter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkProperty.h>
#include <vtkTransform.h>

Region3d::Region3d(vtkSmartPointer<vtkUnstructuredGrid> meshData, QColor colorHex, int index)
{
	m_index = index;
	m_meshData = meshData;
	m_Color = colorHex;

	//Translate the mesh data to (0.0, 0.0, 0.0)
	vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	translation->Translate(0.0, 0.0, 0.0);
	m_translateFilter = vtkSmartPointer<vtkTransformFilter>::New();
	m_translateFilter->SetInputData(m_meshData);
	m_translateFilter->SetTransform(translation);
	m_translateFilter->Update();

	// Region3d Mapper
	m_mapper = vtkSmartPointer<vtkDataSetMapper>::New();
	m_mapper->SetInputConnection(m_translateFilter->GetOutputPort());
	m_mapper->ScalarVisibilityOff();

	// Region3d Actor
	m_actor = vtkSmartPointer<vtkActor>::New();
	m_actor->SetMapper(m_mapper);
	m_actor->GetProperty()->SetInterpolationToFlat();

	m_actor->GetProperty()->SetAmbient(0.5);
	m_actor->GetProperty()->SetDiffuse(0.8);
	m_actor->GetProperty()->SetSpecular(0.0);

	//Position the actor to (0.0, 0.0, 0.0)
	m_actor->SetPosition(0.0, 0.0, 0.0);
}

Region3d::Region3d(const Region3d& obj)
{
	////qDebug() << "Copying Region3d";
	m_index = obj.m_index;
	//m_meshData = obj.m_meshData; //this does a shallow copy
	m_meshData = vtk_utils::getUnstructuredGridWithDeepCopy(obj.m_meshData); //this allows deep copy
	m_Color = obj.m_Color;

	//Translate the mesh data to (0.0, 0.0, 0.0)
	vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	translation->Translate(0.0, 0.0, 0.0);
	m_translateFilter = vtkSmartPointer<vtkTransformFilter>::New();
	m_translateFilter->SetInputData(m_meshData);
	m_translateFilter->SetTransform(translation);
	m_translateFilter->Update();

	// Region3d Mapper
	m_mapper = vtkSmartPointer<vtkDataSetMapper>::New();
	m_mapper->SetInputConnection(m_translateFilter->GetOutputPort());
	m_mapper->ScalarVisibilityOff();

	// Region3d Actor
	m_actor = vtkSmartPointer<vtkActor>::New();
	m_actor->SetMapper(m_mapper);
	m_actor->GetProperty()->SetInterpolationToFlat();

	m_actor->GetProperty()->SetAmbient(0.5);
	m_actor->GetProperty()->SetDiffuse(0.8);
	m_actor->GetProperty()->SetSpecular(0.0);

	//Position the actor to (0.0, 0.0, 0.0)
	m_actor->SetPosition(0.0, 0.0, 0.0);
}

Region3d& Region3d::Region3d::operator=(const Region3d& obj)
{
	////qDebug() << "Region3d: Assignment Operator";
	m_index = obj.m_index;
	//m_meshData = newRegion.m_meshData;
	m_meshData = vtk_utils::getUnstructuredGridWithDeepCopy(obj.m_meshData);
	m_Color = obj.m_Color;

	return *this;
}

const vtkSmartPointer<vtkActor>& Region3d::getActor() const
{
	return m_actor;
}

const QColor Region3d::getColor() const
{
	return m_Color;
}

void Region3d::updateSurfaceColor()
{
	if (this->getColor() != nullptr)
	{
		this->setColor(this->getColor());
	}
	else
	{
		this->setColor(m_defaultColor);
	}
}

void Region3d::setColor(const QColor& color)
{
	m_actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
}

void Region3d::setVisibility(bool flag)
{
	m_visible = flag;
}

const bool Region3d::getVisibility() const
{
	return m_visible;
}

void Region3d::updateVisibility()
{
	if (this->getVisibility() != true)
	{
		this->setVisibility(this->getVisibility());
	}
	else
	{
		this->setVisibility(m_visible);
	}
}

const int Region3d::getIndex() const
{
	return m_index;
}

void Region3d::setCustomColor(const QColor& color)
{
	m_Color = color;
	this->setColor(color);
}

void Region3d::setParentIndex(int domainId)
{
	m_parent_index = domainId;
}

int Region3d::getParentIndex()
{
	return m_parent_index;
}