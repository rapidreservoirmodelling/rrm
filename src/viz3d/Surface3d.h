/***************************************************************************

* This class is part of the 3D Visualization module of Geologic surfaces.

* Copyright (C) 2020, Fazilatur Rahman.

* It represents the 3D representation of geologic surfaces and associated 
* properties needed for rendering via VTK

*****************************************************************************/

#ifndef SURFACE3D_H
#define SURFACE3D_H

#include <QColor>

#include <vtkActor.h>
#include <vtkPolyDataMapper.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkSmartPointer.h>

class Surface3d 
{
public:

	/**
	* Constructor.
	* @param meshData of the surface represented by vtkPolyData.
	* @param colorHex of the surface.
	* @param index of the surface.
	*/
	Surface3d(vtkSmartPointer<vtkPolyData> meshData, QColor colorHex, int index);

	/**
	* Method that returns the corresponding actor of a Surface3D
	* @return vtkSmartPointer<vtkActor>
	*/
	const vtkSmartPointer<vtkActor>& getActor() const;

	/**
	* Returns the color of a Surface3D
	* @return QColor
	*/
	const QColor getColor() const;

	/**
	* Updates the color of a Surface3D
	* @return void
	*/
	void updateSurfaceColor();

	/**
	* Sets the visibility of a Surface3D
	* @return void
	*/
	void setVisibility(bool flag);

	/**
	* Returns the visibility of a Surface3D
	* @return bool
	*/
	const bool getVisibility() const;

	/**
	* Updates the visibility of a Surface3D
	* @return void
	*/
	void updateVisibility();

	/**
	* Returns the index of a Surface3D
	* @return int
	*/
	const int getIndex() const;

	/**
	* Sets the color of a Surface3D
	* @return void
	*/
	void setCustomColor(const QColor& color);

	/**
	* Sets the opacity of a Surface3D
	* @return void
	*/
	void setOpacity(double opacity);

	/**
	* Sets the category of a Surface3D - (STRUCTURAL(2) or STRATIGRAPHY (1) )
	* @return void
	*/
	void setCategory(int category);

	/**
	* Method that returns the category of a Surface3D - (STRUCTURAL(2) or STRATIGRAPHY (1) )
	* @return int
	*/
	int getCategory();

private:
	
	/**
	* Sets the color of a Surface3D
	* @return void
	*/
	void setColor(const QColor& color);

	//- Surface index
	int m_index;

	//- Default color of a surface if no color was assigned during surface creation
	QColor m_defaultColor = QColor{ "#0281ca" };

	//- Transform filter 
	vtkSmartPointer<vtkTransformPolyDataFilter> m_translateFilter;

	//- Mesh data of the surface 
	vtkSmartPointer<vtkPolyData> m_meshData;

	//- Surface mapper
	vtkSmartPointer<vtkPolyDataMapper> m_mapper;

	//- Surface actor 
	vtkSmartPointer<vtkActor> m_actor;

	//- Surface color 
	QColor m_Color;

	//- Surface Visibility
	bool m_visible = true;

	//- Surface Opacity
	double m_opacity = 1.0;

	//- Surface category
	double m_category;
};

#endif //SURFACE3D_H
