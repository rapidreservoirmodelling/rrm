#include "Surface3dRenderer.h"

#include <QQuickWindow>

#include <vtkOrientationMarkerWidget.h>
#include <vtkImplicitPlaneRepresentation.h>
#include <vtkCaptionActor2D.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkCamera.h>
#include <vtkAxesActor.h>
#include <vtkPlane.h>
#include <vtkTransform.h>
#include <vtkInteractorStyleTrackball.h>
#include <vtkInteractorStyleTrackballCamera.h>

#include "Renderer.h"
#include "Action.h"
#include "Surface3d.h"
#include "Region3d.h"
#include "CommandProcessor.h"
#include "VtkFboItem.h"
#include "PlaneWidgetCallback.hpp"
#include "./Model3dHandler.h"
#include "CustomMouseInteractorStyle.hpp"
#include <vtkCallbackCommand.h>

namespace {
	void CameraModifiedCallback(vtkObject* caller,
		long unsigned int vtkNotUsed(eventId),
		void* vtkNotUsed(clientData),
		void* vtkNotUsed(callData))
	{
		////qDebug() << caller->GetClassName() << " modified";

		vtkCamera* camera = static_cast<vtkCamera*>(caller);
		// print the interesting stuff
		////qDebug() << "\tPosition: " << camera->GetPosition()[0] << ", " << camera->GetPosition()[1] << ", " << camera->GetPosition()[2];
		////qDebug() << "\tFocal point: " << camera->GetFocalPoint()[0] << ", " << camera->GetFocalPoint()[1] << ", " << camera->GetFocalPoint()[2];
		////qDebug() << "\tView angle: " << camera->GetViewAngle();
		////qDebug() << "\tView-up: " << camera->GetViewUp()[0] << ", " << camera->GetViewUp()[1] << ", " << camera->GetViewUp()[2];
	}
} // namespace

Surface3dRenderer::Surface3dRenderer(): Renderer()
{
	m_vtkRenderWindow = this->getVtkRenderWindow();
	m_renderer = this->getVtkRenderer();
	
	m_vtkRenderWindowInteractor = this->getVtkRenderWindowInteractor();
	m_picker = this->getPicker();

	//Model3dHandler instance
	m_model3dHandler = Model3dHandler::getInstance(NULL);

	update();
}

void Surface3dRenderer::setCommandProcessor(const std::shared_ptr<CommandProcessor> commandProcessor)
{
	m_commandProcessor = commandProcessor;
}

void Surface3dRenderer::synchronize(QQuickFramebufferObject *item)
{
	// First time synchronization
	if (!m_vtkFboItem)
	{
		m_vtkFboItem = static_cast<VtkFboItem *>(item);
	}

	if (!m_vtkFboItem->isInitialized())
	{
		m_vtkFboItem->setVtkFboRenderer(this);
		emit m_vtkFboItem->rendererInitialized();
	}
	
	//Once initialized
	int *rendererSize = m_vtkRenderWindow->GetSize();

	if (m_vtkFboItem->width() != rendererSize[0] || m_vtkFboItem->height() != rendererSize[1])
	{
		m_vtkRenderWindow->SetSize(m_vtkFboItem->width(), m_vtkFboItem->height());
	}

	// Mouse events from QQuickWindow item
	if (!m_vtkFboItem->getLastMouseLeftButton()->isAccepted())
	{
		m_mouseLeftButton = std::make_shared<QMouseEvent>(*m_vtkFboItem->getLastMouseLeftButton());
		m_vtkFboItem->getLastMouseLeftButton()->accept();
	}

	if (!m_vtkFboItem->getLastMouseButton()->isAccepted())
	{
		m_mouseEvent = std::make_shared<QMouseEvent>(*m_vtkFboItem->getLastMouseButton());
		m_vtkFboItem->getLastMouseButton()->accept();
	}

	if (!m_vtkFboItem->getLastMoveEvent()->isAccepted())
	{
		m_moveEvent = std::make_shared<QMouseEvent>(*m_vtkFboItem->getLastMoveEvent());
		m_vtkFboItem->getLastMoveEvent()->accept();
	}

	if (!m_vtkFboItem->getLastWheelEvent()->isAccepted())
	{
		m_wheelEvent = std::make_shared<QWheelEvent>(*m_vtkFboItem->getLastWheelEvent());
		m_vtkFboItem->getLastWheelEvent()->accept();
	}

	//If plane orientation has already been adjusted establish connection for further actions needed
	if (!m_plane_orientation_change_flag)
	{
		connect(this, &Surface3dRenderer::planeOrientationAdjusted, m_vtkFboItem, &VtkFboItem::crossSectionUpdate);
		m_plane_orientation_change_flag = true;
	}

	//Allow adjusting cross section orientation plane
	if (m_currentPlaneOrientationId != m_vtkFboItem->getPlaneOrientationId())
	{
		//this->adjustCrossSection();
		this->adjustPlane();
	}

	//Allow adjusting vertical cross section selection
	if (m_currentCrossSectionNumber != m_vtkFboItem->getCrossSectionNumber())
	{
		////qDebug() << "TEST0: Before Switching The parameter = " << m_directionParameter;
		switch (m_currentPlaneOrientationId) {
			case 1:
				m_directionParameter = m_boxDimensionX;
				////qDebug() << "Case1:  Setting direction parameter to = " << m_directionParameter;
				break;
			case 2:
				m_directionParameter = m_boxDimensionY;
				////qDebug() << "Case2:  Setting direction parameter to = " << m_directionParameter << ", Cross-section number: " << m_currentCrossSectionNumber;
				//emit(rendererChangedCrossSectionNumber(m_currentCrossSectionNumber));
				break;
			case 3:
				//m_directionParameter = m_boxDimensionZ;
				m_directionParameter = m_boxDimensionZ * m_Flat_factor;
				////qDebug() << "Case3:  Setting direction parameter to = " << m_directionParameter;
				break;
		}
		
		this->adjustCrossSection();
	}
	
	if ((m_vtkFboItem->getBoxSizeX() != m_boxDimensionX))
	{
		////qDebug() << "Renderer : new dimension X:" << m_vtkFboItem->getBoxSizeX() << ", Y:" << m_vtkFboItem->getBoxSizeY() << ", Z:" << m_vtkFboItem->getBoxSizeZ();
		m_boxDimensionX = m_vtkFboItem->getBoxSizeX();
		m_dimension_change_flag = true;
	}
	if ((m_vtkFboItem->getBoxSizeY() != m_boxDimensionY))
	{
		////qDebug() << "Renderer : new dimension X:" << m_vtkFboItem->getBoxSizeX() << ", Y:" << m_vtkFboItem->getBoxSizeY() << ", Z:" << m_vtkFboItem->getBoxSizeZ();
		m_boxDimensionY = m_vtkFboItem->getBoxSizeY();
		m_dimension_change_flag = true;
	}
	if ((m_vtkFboItem->getBoxSizeZ() != m_boxDimensionZ))
	{
		////qDebug() << "Renderer : new dimension X:" << m_vtkFboItem->getBoxSizeX() << ", Y:" << m_vtkFboItem->getBoxSizeY() << ", Z:" << m_vtkFboItem->getBoxSizeZ();
		m_boxDimensionZ = m_vtkFboItem->getBoxSizeZ();
		m_dimension_change_flag = true;
	}

	if (m_dimension_change_flag)
	{
		this->changeDimension();
		this->adjustCamera();

		m_dimension_change_flag = false;
	}

	if (m_vtkFboItem->getVeFactor() != m_VE_factor)
	{
		m_VE_factor = m_vtkFboItem->getVeFactor();
		this->adjustCamera();
	}

	if (m_vtkFboItem->getFlatFactor() != m_Flat_factor)
	{
		m_Flat_factor = m_vtkFboItem->getFlatFactor();
		this->changeDimension();
	}

}
void Surface3dRenderer::acceptMouseEvents()
{
	if (m_mouseEvent && !m_mouseEvent->isAccepted())
	{
		m_vtkRenderWindowInteractor->SetEventInformationFlipY(m_mouseEvent->x(), m_mouseEvent->y(),
			(m_mouseEvent->modifiers() & Qt::ControlModifier) > 0 ? 1 : 0,
			(m_mouseEvent->modifiers() & Qt::ShiftModifier) > 0 ? 1 : 0, 0,
			m_mouseEvent->type() == QEvent::MouseButtonDblClick ? 1 : 0);

		////qDebug() << "Surface3dRenderer::render()-Leftbutton event";
		if (m_mouseEvent->type() == QEvent::MouseButtonPress)
		{
			m_vtkRenderWindowInteractor->InvokeEvent(vtkCommand::LeftButtonPressEvent, m_mouseEvent.get());

		}
		else if (m_mouseEvent->type() == QEvent::MouseButtonRelease)
		{
			m_vtkRenderWindowInteractor->InvokeEvent(vtkCommand::LeftButtonReleaseEvent, m_mouseEvent.get());

		}

		m_mouseEvent->accept();
	}
}

void Surface3dRenderer::handleMouseWheelDelta()
{
	if (m_wheelEvent && !m_wheelEvent->isAccepted())
	{
		////qDebug() << "Surface3dRenderer::handleMouseWheelDelta():" << m_wheelEvent->delta();
		//if (m_wheelEvent->delta() > 120 || m_wheelEvent->delta() < -120)
			//return;
		if (m_wheelEvent->delta() > 0)
		{
			m_vtkRenderWindowInteractor->InvokeEvent(vtkCommand::MouseWheelForwardEvent, m_wheelEvent.get());
		}
		else if (m_wheelEvent->delta() < 0)
		{
			m_vtkRenderWindowInteractor->InvokeEvent(vtkCommand::MouseWheelBackwardEvent, m_wheelEvent.get());
		}

		m_wheelEvent->accept();
	}
}
void Surface3dRenderer::handleMouseMovement()
{
	if (m_moveEvent && !m_moveEvent->isAccepted())
	{
		//Only the rightbutton movement will move the outline box
		//if (m_moveEvent->type() == QEvent::MouseMove && m_moveEvent->buttons() & Qt::RightButton)
		//Only the leftbutton movement will move the outline box
		//if (m_moveEvent->type() == QEvent::MouseMove && m_moveEvent->buttons() & Qt::LeftButton)
		//Both left and right button movement will move the outline box
		if (m_moveEvent->type() == QEvent::MouseMove && m_moveEvent->buttons())
		{
			moveTheOutlineBox();
		}

		m_moveEvent->accept();
	}
}
void Surface3dRenderer::executeActions()
{
	Action* action;
	while (!m_vtkFboItem->isQueueEmpty())
	{
		m_vtkFboItem->lockQueue();

		action = m_vtkFboItem->getQueueFront();
		if (!action->isReady())
		{
			m_vtkFboItem->unlockQueue();
			break;
		}
		m_vtkFboItem->popQueue();

		m_vtkFboItem->unlockQueue();

		action->execute();
	}
}
void Surface3dRenderer::render()
{
	m_vtkRenderWindow->PushState();
	this->openGLInitState();
	m_vtkRenderWindow->Start();

	if (m_firstRender)
	{
		this->InitBackground();
		m_firstRender = false;
	}

	//Accept Mouse events
	this->acceptMouseEvents();
	
	//Mouse Move event
	this->handleMouseMovement();

	//Mouse Wheel event
	this->handleMouseWheelDelta();

	//Process additional mouse events if needed
	//TBD

	//Navigate VtkFboItem actions queue and execute 
	this->executeActions();
	
	//Reset the view-up vector causes vtkRenderer error; so keeping the view-up vector towards x-axis
	//m_renderer->GetActiveCamera()->SetViewUp(0.0, 0.0, 1.0);

	//Update surface color
	m_commandProcessor->updateSurfaceColor();

	//Update region color
	m_commandProcessor->updateRegionColor();
	
	//Update surface visibility
	////qDebug() << "Render method: call";
	//m_commandProcessor->updateSurfaceVisibility();

	//Render the window state
	m_vtkRenderWindow->Render();
	m_vtkRenderWindow->PopState();
	m_vtkFboItem->window()->resetOpenGLState();
}

void Surface3dRenderer::InitBackground()
{
	m_vtkRenderWindow->SetOffScreenRendering(true);

	//Background color
	m_renderer->SetBackground(0.96, 0.96, 0.96); //top background //0.55, 0.55, 0.55
	m_renderer->SetBackground2(0.67, 0.67, 0.67); //bottom background //0.9, 0.9, 0.9
	m_renderer->GradientBackgroundOn();

	//Orientation Marker
	this->InitOrientationMarker();

	//Initial camera position
	this->resetCamera();

	vtkSmartPointer<vtkCallbackCommand> modifiedCallback = vtkSmartPointer<vtkCallbackCommand>::New();
	modifiedCallback->SetCallback(CameraModifiedCallback);
	m_renderer->GetActiveCamera()->AddObserver(vtkCommand::ModifiedEvent, modifiedCallback);

	//Init Transparent Cube
	this->InitImplicitPlaneWidget();
}

void Surface3dRenderer::addActor(const std::shared_ptr<Surface3d> surface)
{
	m_renderer->AddActor(surface->getActor());
	
}

void Surface3dRenderer::addActor(const std::shared_ptr<Region3d> region)
{
	//qDebug() << "Surface3dRenderer::addActor:" << region->getIndex();
	m_renderer->AddActor(region->getActor());

}

void Surface3dRenderer::removeActor(const std::shared_ptr<Surface3d> surface)
{
	try {
		if (surface !=nullptr) {
			m_renderer->RemoveActor(surface->getActor());
			
		}
		
	}
	catch (int error) {
		////qDebug() << "Surface3dRenderer::removeActor: exception" << error;

	}
	
}

void Surface3dRenderer::removeActor(const std::shared_ptr<Region3d> region)
{
	try {
		if (region != nullptr) {
			m_renderer->RemoveActor(region->getActor());

		}

	}
	catch (int error) {
		////qDebug() << "Surface3dRenderer::removeActor: exception" << error;

	}

}
void Surface3dRenderer::resetCamera()
{
	m_renderer->GetActiveCamera()->SetPosition(m_camPositionX, m_camPositionY, m_camPositionZ);
	m_renderer->GetActiveCamera()->SetFocalPoint(0.0, 0.0, 0.0);

	//ReSetting the Viewup vetor to Z axis causes vtkRenderer error: 
	//The view-up vector tells the renderer where the �up� of the camera is pointing to.
	//The view - up vector should, under normal circumstances, be orthogonal to the normal vector of viewing plane(in this case the X-axis).
	//Setting it parallel to the normal would result in ill - conditioned matrices and divisions by zero 
	//in the computations down the rendering pipeline.Hence the VTK error shows.
	//m_renderer->GetActiveCamera()->SetViewUp(0.0, 0.0, 1.0);

	//m_renderer->GetActiveCamera()->SetViewAngle(90.0);

	m_renderer->ResetCameraClippingRange();

}

void Surface3dRenderer::InitOrientationMarker()
{
	//Axes
	vtkSmartPointer<vtkAxesActor> axes = vtkSmartPointer<vtkAxesActor>::New();
	//Set Axes Length and Font
	double axes_length = 25.0;
	int16_t axes_label_font_size = 25;
	axes->SetTotalLength(axes_length, axes_length, axes_length);
	axes->GetXAxisCaptionActor2D()->GetTextActor()->SetTextScaleModeToNone();
	axes->GetYAxisCaptionActor2D()->GetTextActor()->SetTextScaleModeToNone();
	axes->GetZAxisCaptionActor2D()->GetTextActor()->SetTextScaleModeToNone();
	axes->GetXAxisCaptionActor2D()->GetCaptionTextProperty()->SetFontSize(axes_label_font_size);
	axes->GetYAxisCaptionActor2D()->GetCaptionTextProperty()->SetFontSize(axes_label_font_size);
	axes->GetZAxisCaptionActor2D()->GetCaptionTextProperty()->SetFontSize(axes_label_font_size);

	//Orientation Marker Widget
	m_orientation_marker = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
	m_orientation_marker->SetOrientationMarker(axes);
	// Position lower left in the viewport.
	m_orientation_marker->SetViewport(0.0, 0.0, 0.2, 0.2);//(0.2, 0.2, 0.8, 0.8);
	m_orientation_marker->SetInteractor(m_vtkRenderWindowInteractor);
	m_orientation_marker->EnabledOn();
	m_orientation_marker->InteractiveOn();
	m_orientation_marker->SetOutlineColor(1, 0.2, 0.5);
}

void Surface3dRenderer::InitImplicitPlaneWidget()
{
	//Create Cube
	cubeSource = vtkSmartPointer<vtkCubeSource>::New();
	
	// Create Plane
	m_plane = vtkSmartPointer<vtkPlane>::New();
	
	// Create a representation for the plane
	rep = vtkSmartPointer<vtkImplicitPlaneRepresentation>::New();
	
	//Create an ImplicitPlaneWidget
	m_planeWidget = vtkSmartPointer<vtkImplicitPlaneWidget2>::New();

	//To Prevent Color Bleeding
	vtkSmartPointer<vtkActorCollection> actorCollection = vtkSmartPointer<vtkActorCollection>::New();
	rep->GetActors(actorCollection);
	actorCollection->InitTraversal();

	for (vtkIdType i = 0; i < actorCollection->GetNumberOfItems(); i++)
	{
		vtkSmartPointer<vtkActor> actorObject = actorCollection->GetNextActor();
		//qDebug() << "TEST Length: object : " << i << " : " << actorObject->GetClassName();

		if (i == 0) //Bounding box outline
		{
			actorObject->GetProperty()->SetOpacity(1.0);//actorObject->GetProperty()->SetOpacity(0.1);
		}
		else if (i == 1) // The Cross-section plane
		{
			actorObject->GetProperty()->SetColor(192, 192, 192);//actorObject->GetProperty()->SetColor(254, 0, 0);
			actorObject->GetProperty()->SetOpacity(0.07);//actorObject->GetProperty()->SetOpacity(0.01);
			//actorObject->GetProperty()->EdgeVisibilityOff();
		}
		else if (i == 2) // Not sure!!
		{
			//actorObject->GetProperty()->SetOpacity(1.0);//actorObject->GetProperty()->SetOpacity(1.0);
			continue;

		}
		else if (i == 3) // The Arrow  
		{
			//actorObject->GetProperty()->SetColor(254, 0, 0);
			actorObject->GetProperty()->SetOpacity(0.0);
		}
		else break;
		
	}
	
}

void Surface3dRenderer::adjustPlane()
{
	if (m_currentPlaneOrientationId != m_vtkFboItem->getPlaneOrientationId())
	{
		int planeSelector = m_vtkFboItem->getPlaneOrientationId();

		////qDebug() << "TEST PlaneOrientation before: " << m_currentPlaneOrientationId << ", after: " << planeSelector;

		if (m_currentPlaneOrientationId != planeSelector)
		{
			m_currentPlaneOrientationId = planeSelector;
			if (m_currentPlaneOrientationId == 1) //WIDTH
			{
				//rep->SetNormal(m_plane->GetNormal());
				m_plane->SetNormal(-1.0, 0.0, 0.0);
				//m_plane->SetOrigin(0.5, 0.5, 0.5);
				//rep->SetEdgeColor(0, 204, 153);

				////qDebug() << "TEST Width: DirectionParameter value: " << directionParameter;
			}
			else if (m_currentPlaneOrientationId == 2) //LENGTH
			{
				//rep->SetNormal(m_plane->GetNormal());
				m_plane->SetNormal(0.0, 1.0, 0.0);
				//m_plane->SetOrigin(0.5, 0.5, 0.5);
				//rep->SetEdgeColor(178, 0, 0);

				////qDebug() << "TEST Length: DirectionParameter value: " << directionParameter;
			}
			else if (m_currentPlaneOrientationId == 3) //HEIGHT
			{
				m_plane->SetNormal(0.0, 0.0, -1.0);
				//m_plane->SetOrigin(0.5, 0.5, 0.5);
				//rep->SetEdgeColor(0, 153, 255);

				////qDebug() << "TEST Height: DirectionParameter value: " << directionParameter;
			}
			rep->SetNormal(m_plane->GetNormal());

			//Prepare for cross-secion update
			rep->PushPlane(m_directionParameter);
			m_plane_orientation_change_flag = false;
			m_currentCrossSectionNumber = 0;
			emit(planeOrientationAdjusted());
		}
		//Reset current cross-section
		//m_currentCrossSectionNumber = 64;
		
		////qDebug() << "Plane orientation updated! ";
		//emit(planeOrientationAdjusted());
	}
}

void Surface3dRenderer::adjustCrossSection()
{
	//directionParameter = m_model3dHandler->getLength();//m_boxDimensionZ;

	//Switch plane if needed
	int planeSelector = m_vtkFboItem->getPlaneOrientationId();
	
	////qDebug() << "TEST PlaneOrientation before: " << m_currentPlaneOrientationId << ", after: " << planeSelector;

	if (m_currentPlaneOrientationId != planeSelector)
	{
		m_currentPlaneOrientationId = planeSelector;
		if (m_currentPlaneOrientationId == 1) //WIDTH
		{
			//rep->SetNormal(m_plane->GetNormal());
			m_plane->SetNormal(-1.0, 0.0, 0.0);
			m_plane->SetOrigin(0.5, 0.5, 0.5);
			rep->SetEdgeColor(0, 204, 153);//rep->SetEdgeColor(178, 0, 0);

			//m_directionParameter = m_boxDimensionX;//m_model3dHandler->getWidth();
			////qDebug() << "TEST Width: DirectionParameter value: " << directionParameter;
		}
		else if (m_currentPlaneOrientationId == 2) //LENGTH
		{
			//rep->SetNormal(m_plane->GetNormal());
			m_plane->SetNormal(0.0, 1.0, 0.0);
			m_plane->SetOrigin(0.5, 0.5, 0.5);
			rep->SetEdgeColor(178, 0, 0); 

			//m_directionParameter = m_boxDimensionZ;//m_model3dHandler->getLength();
			////qDebug() << "TEST Length: DirectionParameter value: " << directionParameter;
		}
		else if (m_currentPlaneOrientationId == 3) //HEIGHT
		{
			m_plane->SetNormal(0.0, 0.0, -1.0);
			m_plane->SetOrigin(0.5, 0.5, 0.5);
			rep->SetEdgeColor(0, 153, 255);//rep->SetEdgeColor(0, 123, 0);

			//m_directionParameter = m_boxDimensionY;//m_model3dHandler->getHeight();
			////qDebug() << "TEST Height: DirectionParameter value: " << directionParameter;
		}
		rep->SetNormal(m_plane->GetNormal());

	}
	
	////qDebug() << "TEST: DirectionParameter value: " << m_directionParameter;
	

	//Set moving plane at the position of widthSlider
	double crossSectionNumber = m_vtkFboItem->getCrossSectionNumber();
	double deltaCrossSection = m_currentCrossSectionNumber - crossSectionNumber;
	
	////qDebug() << m_currentCrossSectionNumber << " != " << crossSectionNumber;
	if (m_currentCrossSectionNumber != crossSectionNumber)
	{
		m_currentCrossSectionNumber = crossSectionNumber;
		//rep->PushPlane(deltaCrossSection * m_model3dHandler->getLength()/64);
		
		//- Getting discretization from the model: using the width value, but I believe this is assuming all directions have the same discretization number
		int width, length;
		m_model3dHandler->getModelDiscretization(width, length);
		
		rep->PushPlane(deltaCrossSection * m_directionParameter / width);
	}
	
	//Save Cross-section info
	m_model3dHandler->saveSelectedCrossSection(m_currentCrossSectionNumber, m_currentPlaneOrientationId);
}

void Surface3dRenderer::changeDimension()
{
	double origin_x = 0;
	double origin_y = 0;
	double origin_z = 0;
	m_model3dHandler->getOrigin(origin_x, origin_z, origin_y);
	
	//Setup the bounding box using cubeSource
	cubeSource->SetCenter(origin_x + m_boxDimensionX / 2., origin_y + m_boxDimensionY / 2., origin_y + m_boxDimensionZ / 2.);
	////qDebug() << "Surface3dRenderer:: changeDimension: X:" << m_boxDimensionX << ", Y:" << m_boxDimensionY << ", Z:" << m_boxDimensionZ;
	cubeSource->SetXLength(m_boxDimensionX);
	cubeSource->SetYLength(m_boxDimensionY); //cubeSource->SetYLength(m_boxDimensionY);
	//cubeSource->SetZLength(m_boxDimensionZ);//cubeSource->SetZLength(m_boxDimensionZ);
	cubeSource->SetZLength(m_boxDimensionZ * m_Flat_factor);//cubeSource->SetZLength(m_boxDimensionZ);

	cubeSource->Update();

	vtkPolyData* cubeData = cubeSource->GetOutput();

	// Create the outline
	vtkSmartPointer<vtkOutlineFilter> outline = vtkSmartPointer<vtkOutlineFilter>::New();
	outline->SetInputData(cubeData);

	vtkSmartPointer<vtkPolyDataMapper>outlineMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	outlineMapper->SetInputConnection(outline->GetOutputPort());

	// Setup a visualization pipeline
	m_plane = vtkSmartPointer<vtkPlane>::New();
	m_plane->SetOrigin(origin_x + m_boxDimensionX / 2., origin_y + m_boxDimensionY / 2., origin_z + m_boxDimensionZ / 2.);//m_plane->SetOrigin(0.0, 0.0, 0.0);
	m_plane->SetNormal(0.0, 1.0, 0.0);
	
	m_plane->RemoveObserver(vtkCommand::MouseMoveEvent);

	// Create a mapper and actor
	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	outlineMapper->SetInputConnection(outline->GetOutputPort());//mapper->SetInputConnection(clipper->GetOutputPort());
	m_plane_actor = vtkSmartPointer<vtkActor>::New();
	m_plane_actor->SetMapper(outlineMapper);//m_plane_actor->SetMapper(mapper);
	m_plane_actor->VisibilityOff();

	m_renderer->AddActor(m_plane_actor);

	rep->SetPlaceFactor(1.0); // This must be set prior to placing the widget
	rep->PlaceWidget(m_plane_actor->GetBounds());
	rep->SetNormal(m_plane->GetNormal());
	rep->TubingOff();
	
	//Set the size of the plane handle to zero
	rep->SetHandleSize(0);
	//Stop translating the bounding box 
	rep->SetOutlineTranslation(0);
	//Force setting the normal to X-Axis 
	rep->SetNormalToXAxis(1);
	m_planeWidget->SetRepresentation(rep);

	/*
	//We don't need to use the Generic Window Interactor for the PlaneWidget  
	m_planeWidget->SetInteractor(m_vtkRenderWindowInteractor);

	//We don't want to use the default interaction events for RRM
	// The callback that comes with the PlaneWidget
	vtkSmartPointer<PlaneWidgetCallBack> myCallback = vtkSmartPointer<PlaneWidgetCallBack>::New();
	myCallback->Plane = m_plane;
	myCallback->Actor = m_plane_actor;
	m_planeWidget->AddObserver(vtkCommand::InteractionEvent, myCallback);
	m_vtkRenderWindowInteractor->Initialize();
	*/

	
	//We will use a custom interaction style for the PlaneWidget
	vtkSmartPointer<vtkRenderWindowInteractor> custom_renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	custom_renderWindowInteractor->SetRenderWindow(m_vtkRenderWindow);
	//Create custom interactor style
	vtkNew<customMouseInteractorStyle> style;
	custom_renderWindowInteractor->SetInteractorStyle(style);
	m_planeWidget->SetInteractor(custom_renderWindowInteractor);

	//Rather we will use the custom interactor
	custom_renderWindowInteractor->Initialize();
	
	m_vtkRenderWindow->Render();
	m_planeWidget->On();
	m_planeWidget->SetEnabled(1);

}

void Surface3dRenderer::adjustCamera()
{
	//Adjust camera postion to account for dimensions

	//When the ViewUp was Z-axis
	/*m_camPositionX += -m_boxDimensionX;
	m_camPositionY += -m_boxDimensionY;
	m_camPositionZ += m_boxDimensionZ;
	*/
	//When the ViewUp is set to X-axis
	m_camPositionX += m_boxDimensionX;
	m_camPositionY += -m_boxDimensionY;
	m_camPositionZ += m_boxDimensionZ;

	// Vertical exaggeration factor
	vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
	transform->Scale(1.0, 1.0, 1.0 * m_VE_factor);
	m_renderer->GetActiveCamera()->SetModelTransformMatrix(transform->GetMatrix());

	//Adjust camera so that the Bounding box is visible
	m_renderer->GetActiveCamera()->SetPosition(m_camPositionX, m_camPositionY, m_camPositionZ);
	m_renderer->GetActiveCamera()->SetFocalPoint(m_boxDimensionX /2.0 , m_boxDimensionY /2.0, m_boxDimensionZ / 2.0);
	
	//Keeping the view-up vector to default (X-axis) instead of setting it to z-axis
	//m_renderer->GetActiveCamera()->SetViewUp(0.0, 0.0, 1.0);

	m_renderer->ResetCamera(); // m_renderer->ResetCamera() sets the View angle to default 30
	//m_renderer->GetActiveCamera()->SetViewAngle(90.0);

}

void Surface3dRenderer::moveTheOutlineBox()
{
	m_vtkRenderWindowInteractor->SetEventInformationFlipY(m_moveEvent->x(), m_moveEvent->y(),
		(m_moveEvent->modifiers() & Qt::ControlModifier) > 0 ? 1 : 0,
		(m_moveEvent->modifiers() & Qt::ShiftModifier) > 0 ? 1 : 0, 0,
		m_moveEvent->type() == QEvent::MouseButtonDblClick ? 1 : 0);

	m_vtkRenderWindowInteractor->InvokeEvent(vtkCommand::MouseMoveEvent, m_moveEvent.get());

	//Since we changed the view-up vector to default (X-axis)
	//Adjustments to view-up vector is not required 
	//adjustCameraViewUp();

}

void Surface3dRenderer::adjustCameraViewUp()
{
	double* viewPlaneNormal = m_renderer->GetActiveCamera()->GetViewPlaneNormal();
	
	//Reset the view-up vector if the view plane normal is close to be parallel
	if ((viewPlaneNormal[2] >= 0.999) || (viewPlaneNormal[2] <= -0.999))
	{
		double* focalPoint = m_renderer->GetActiveCamera()->GetFocalPoint();
		double* position = m_renderer->GetActiveCamera()->GetPosition();
		double dist = std::sqrt((position[0] - focalPoint[0]) * (position[0] - focalPoint[0])
			+ (position[1] - focalPoint[1]) * (position[1] - focalPoint[1])
			+ (position[2] - focalPoint[2]) * (position[2] - focalPoint[2]));

		m_renderer->GetActiveCamera()->SetPosition(focalPoint[0], focalPoint[1], focalPoint[2] + dist);
		m_renderer->GetActiveCamera()->SetViewUp(1.0, 0.0, 0.0);
		//m_renderer->GetActiveCamera()->Yaw(-60);
		//m_renderer->GetActiveCamera()->Pitch(60);
		m_renderer->GetActiveCamera()->SetPosition(m_camPositionX, m_camPositionY, m_camPositionZ);
		m_renderer->GetActiveCamera()->SetFocalPoint(m_boxDimensionX / 2.0, m_boxDimensionY / 2.0, m_boxDimensionZ / 2.0);
		//m_renderer->GetActiveCamera()->SetViewUp(0.0, 0.0, 1.0);
		
		m_renderer->ResetCamera();
	}
	
}
