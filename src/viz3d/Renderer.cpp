#include "Renderer.h"

#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkSmartPointer.h>

Renderer::Renderer()
{
	//Renderer
	m_vtkRenderWindow = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
	m_renderer = vtkSmartPointer<vtkRenderer>::New();
	m_vtkRenderWindow->AddRenderer(m_renderer);

	//Interactor
	m_vtkRenderWindowInteractor = vtkSmartPointer<vtkGenericRenderWindowInteractor>::New();
	m_vtkRenderWindowInteractor->EnableRenderOff();
	m_vtkRenderWindow->SetInteractor(m_vtkRenderWindowInteractor);

	//Interactor Style
	vtkSmartPointer<vtkInteractorStyleTrackballCamera> style = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
	style->SetDefaultRenderer(m_renderer);
	style->SetMotionFactor(10.0);
	m_vtkRenderWindowInteractor->SetInteractorStyle(style);

	//Initialize the OpenGL context for the renderer
	m_vtkRenderWindow->OpenGLInitContext();

	//Picker
	m_picker = vtkSmartPointer<vtkCellPicker>::New();
	m_picker->SetTolerance(0.0);

}

void Renderer::openGLInitState()
{
	m_vtkRenderWindow->OpenGLInitState();
	m_vtkRenderWindow->MakeCurrent();
	QOpenGLFunctions::initializeOpenGLFunctions();
	QOpenGLFunctions::glUseProgram(0);
}

QOpenGLFramebufferObject* Renderer::createFramebufferObject(const QSize& size)
{
	QOpenGLFramebufferObjectFormat format;
	format.setAttachment(QOpenGLFramebufferObject::Depth);

	std::unique_ptr<QOpenGLFramebufferObject> framebufferObject(new QOpenGLFramebufferObject(size, format));

	m_vtkRenderWindow->SetBackLeftBuffer(GL_COLOR_ATTACHMENT0);
	m_vtkRenderWindow->SetFrontLeftBuffer(GL_COLOR_ATTACHMENT0);
	m_vtkRenderWindow->SetBackBuffer(GL_COLOR_ATTACHMENT0);
	m_vtkRenderWindow->SetFrontBuffer(GL_COLOR_ATTACHMENT0);
	m_vtkRenderWindow->SetSize(framebufferObject->size().width(), framebufferObject->size().height());
	m_vtkRenderWindow->SetOffScreenRendering(true);
	m_vtkRenderWindow->Modified();

	return framebufferObject.release();
}

vtkSmartPointer<vtkGenericOpenGLRenderWindow> Renderer::getVtkRenderWindow()
{
	return m_vtkRenderWindow;
}

vtkSmartPointer<vtkRenderer> Renderer::getVtkRenderer()
{
	return m_renderer;
}

vtkSmartPointer<vtkGenericRenderWindowInteractor> Renderer::getVtkRenderWindowInteractor()
{
	return m_vtkRenderWindowInteractor;
}

vtkSmartPointer<vtkCellPicker> Renderer::getPicker()
{
	return m_picker;
}