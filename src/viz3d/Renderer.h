
/***************************************************************************

* This class is part of the 3D Visualization module of Geologic models. 
* It Provides a Generic Renderer

* The QQuickFramebufferObject class enforces a strict separation between the
* item implementation and the FBO rendering b'cos, the rendering will occur
* on a dedicated thread. All item logic, such as properties and UI-related
* helper functions needed by QML should be located in a QQuickFramebufferObject
* class subclass. Everything that relates to rendering must be located in the
* QQuickFramebufferObject::Renderer class.

* It is the base class responsible for initializing openGL context and
* creating the frame buffer object and is used to implement the 
* rendering logic of QQuickFramebufferObject::Renderer

* It utlilizes the QQuickFramebufferObject class which is a 
* convenience class for integrating OpenGL rendering using a 
* framebuffer object (FBO) with QtQuick.

* The detail documentation is available here: 
* https://doc.qt.io/qt-5/qquickframebufferobject.html#details

*****************************************************************************/

#ifndef RENDERER_H
#define RENDERER_H

#include <QOpenGLFramebufferObject>
#include <QOpenGLFunctions>
#include <QQuickFramebufferObject>

#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkGenericRenderWindowInteractor.h>
#include <vtkCellPicker.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>

class Renderer : public QQuickFramebufferObject::Renderer, protected QOpenGLFunctions
{
	
public:

	/**
	* Constructor.
	*/
	Renderer();

	/**
	* Method that initializes openGL state for the vtk render window 
	* @return bool
	*/
	virtual void openGLInitState();

	
	/**
	* This method is called when a new FBO is needed.
	* This happens on the initial frame.
	* It is also called every time the dimensions of the item changes.
	* @param size of the QQuickitem
	* @return QOpenGLFramebufferObject*
	*/
	QOpenGLFramebufferObject* createFramebufferObject(const QSize& size);

	/**
	* Getter method for the vtk render window
	* @return vtkSmartPointer<vtkGenericOpenGLRenderWindow>
	*/
	vtkSmartPointer<vtkGenericOpenGLRenderWindow> getVtkRenderWindow();
	
	/**
	* Getter method for the vtk renderer
	* @return vtkSmartPointer<vtkRenderer>
	*/
	vtkSmartPointer<vtkRenderer> getVtkRenderer();

	/**
	* Getter method for the vtk interactor
	* @return vtkSmartPointer<vtkGenericRenderWindowInteractor>
	*/
	vtkSmartPointer<vtkGenericRenderWindowInteractor> getVtkRenderWindowInteractor();

	/**
	* Getter method for the vtk picker
	* @return vtkSmartPointer<vtkCellPicker>
	*/
	vtkSmartPointer<vtkCellPicker> getPicker();
		
private:

	//- Render window 
	vtkSmartPointer<vtkGenericOpenGLRenderWindow> m_vtkRenderWindow;

	//- Renderer 
	vtkSmartPointer<vtkRenderer> m_renderer;

	//- Interactor 
	vtkSmartPointer<vtkGenericRenderWindowInteractor> m_vtkRenderWindowInteractor;

	//- Picker 
	vtkSmartPointer<vtkCellPicker> m_picker;
		
};

#endif //RENDERER_H
