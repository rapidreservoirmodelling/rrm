/***************************************************************************

* This class is part of the 3D Visualization module of Geologic surfaces.

* Copyright (C) 2020, Fazilatur Rahman.

* It handles actions that need be applied on one or more objects i.e. surface(s), region(s) and single region in a domain
* inside the 3D view via the Surface3dRenderer

*****************************************************************************/

#ifndef ACTION_H
#define ACTION_H

#include <memory>
#include <QThread>

class Surface3d;
class Region3d;
class CommandProcessor;
class Surface3dRenderer;

class Action : public QObject
{
	Q_OBJECT

public:
	/**
	* Constructor.
	* @param commandProcessor that processes this command.
	* @param vtkFboRenderer that renders surfaces on to the 3D window
	* @param actionId that identifies the type of action
	*/
	Action(int actionId, std::shared_ptr<CommandProcessor> commandProcessor, Surface3dRenderer* vtkFboRenderer );

	/**
	* Method to setup thread
	* @return void
	*/
	void setup(QThread *cthread);

	/**
	* Method that retrieves the data structure needed to execute the command itself
	* @return void
	*/
	//void run();

	/**
	* Method to determine if the command is ready to be executed
	* @return bool
	*/
	bool isReady();
	
	/**
	* Method that implements the command's actions
	* @return void
	*/
	//void execute();

	/**
	* Set parameters for color change
	* @return void
	*/
	void SetColorChangeParameter(int index, int type, QString color);

	/**
	* Set parameters for visibility change
	* @return void
	*/
	void SetVisibilityChangeParameter(int index, int type, bool visibility);
	void SetVisibilityChangeParameter(int type, bool visibility);
	void SetSurfaceVisibilityChangeParameter(std::vector<int> indices, int type, bool visibility);

	/**
	* Set parameters for DOMAINS_REMOVE_SINGLE_REGION action
	* @return void
	*/
	void SetParameters4RemoveRegionFromDomain(int region_id, int domain_id);

	/**
	* Set parameters for DOMAINS_REMOVE_SINGLE_DOMAIN action
	* @return void
	*/
	void SetParameters4RemoveDomain(int domain_id);

signals:
	void ready();
	void done();

public slots:
	void run();
	void execute();

private:
	// - Thread instance
	QThread* m_thread;

	// - Command processor that processes this command
	std::shared_ptr<CommandProcessor> m_commandProcessor;

	// - Surface3d 
	std::shared_ptr<Surface3d> m_surface = nullptr;

	// - List of surfaces
	std::vector <std::shared_ptr<Surface3d>> m_surfaces;
	
	// - List of previewed surfaces
	std::vector <std::shared_ptr<Surface3d>> m_previewedSurfaces;
	
	// - Region3d 
	std::shared_ptr<Region3d> m_region = nullptr;

	// - List of regions
	std::vector <std::shared_ptr<Region3d>> m_regions;

	// - List of domain's regions
	std::vector <std::shared_ptr<Region3d>> m_domain_regions;
	
	// - Flag to determine if the command is ready to be executed
	bool m_ready = false;
	
	// - Viz3d action id
	int m_action_id = 0;

	// - Parameters for object color and visibility 
	int object_index;					/**< index of the selected object. */
	int object_type_id;					/**< type_id of the selected object. */
	QString object_color;				/**< Chosen color for the selected object. */
	bool object_visibility_flag;		/**< visibility of the selected object. */
	bool m_update_status = false;		/**< Update status of the selected object. */
	bool m_domain_region_update_status = false;		/**< Update status of the selected object region in a domain. */
	std::vector<int> object_indices;					/**< List of surface ids. */

	// - Parameters for DOMAINS_REMOVE_SINGLE_REGION action
	int m_region_id;											/**< id of the region to be removed. */
	int m_domain_id;											/**< id of the parent domain from which the region to be removed. */
	std::vector <std::shared_ptr<Region3d>> m_domain_3d_List;	/**< List of regions in the parent domain. */

protected:

	//- Renderer that will register the action 
	Surface3dRenderer* m_vtkFboRenderer;
};

#endif // ACTIONS_H
