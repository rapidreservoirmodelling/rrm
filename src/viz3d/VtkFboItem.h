/***************************************************************************

* This class is part of the 3D Visualization module of Geologic models.

* The QQuickFramebufferObject class enforces a strict separation between the
* item implementation and the FBO rendering b'cos, the rendering will occur
* on a dedicated thread which is separate from the main GUI thread. All item 
* logic, such as properties and UI-related helper functions needed by QML 
* should be located in this (QQuickFramebufferObject class) subclass. 

* It utlilizes the QQuickFramebufferObject class which is a
* convenience class for integrating OpenGL rendering using a
* framebuffer object (FBO) with QtQuick

* This class translates user events and sends signals to
* update the Surface3dRenderer

* The detail documentation is available here:
* https://doc.qt.io/qt-5/qquickframebufferobject.html#details

*****************************************************************************/

#ifndef VTKFBOITEM_H
#define VTKFBOITEM_H

#include <memory>
#include <queue>
#include <mutex>

#include <QtQuick/QQuickFramebufferObject>

class Action;
class CommandProcessor;
class Surface3dRenderer;

class VtkFboItem : public QQuickFramebufferObject
{
	Q_OBJECT

public:
	/**
	* Constructor.
	*/
	VtkFboItem();

	/**
	* Method that returns the surface renderer.
	* It returns the value of the private member.
	* @return QQuickFramebufferObject::Renderer
	*/
	Renderer *createRenderer() const Q_DECL_OVERRIDE;

	/**
	* Method that sets the surface renderer
	* @param surface renderer
	* @return void
	*/
	void setVtkFboRenderer(Surface3dRenderer*);

	/**
	* Method that checks if the renderer is null
	* @return bool
	*/
	bool isInitialized() const;
	
	/**
	* Method that sets the command processor
	* @param command processor
	* @return void
	*/
	void setCommandProcessor(const std::shared_ptr<CommandProcessor> commandProcessor);

	/**
	* Method that add surfaces by registering the CommandVectorAdd
	* @return void
	*/
	//void addSurfaces();

	/**
	* Method that remove surfaces by registering the CommandVectorRemove
	* @return void
	*/
	void removePreviousSurfaces();

	/**
	* Mouse wheel event handler
	* @param QWheelEvent
	* @return void
	*/
	void wheelEvent(QWheelEvent *e) override;
	
	/**
	* Mouse press event handler
	* @param QMouseEvent
	* @return void
	*/
	void mousePressEvent(QMouseEvent *e) override;
	
	/**
	* Mouse release event handler
	* @param QMouseEvent
	* @return void
	*/
	void mouseReleaseEvent(QMouseEvent *e) override;

	/**
	* Mouse move event handler
	* @param QMouseEvent
	* @return void
	*/
	void mouseMoveEvent(QMouseEvent *e) override;

	/**
	* Getter method for the last mouse left button event
	* @return QMouseEvent
	*/
	QMouseEvent *getLastMouseLeftButton();

	/**
	* Getter method for the last mouse button
	* @return QMouseEvent
	*/
	QMouseEvent *getLastMouseButton();

	/**
	* Getter method for the last mouse move event
	* @return QMouseEvent
	*/
	QMouseEvent *getLastMoveEvent();

	/**
	* Getter method for the last mouse wheel event
	* @return QWheelEvent
	*/
	QWheelEvent *getLastWheelEvent();


	/**
	* Method that resets camera changes for this QQuick item
	* @return void
	*/
	void resetCamera();
	
	/**
	* Getter method for command queue front
	* @return action que front pointer
	*/
	Action* getQueueFront() const;

	/**
	* Method that pops the first action from the queue front
	* @return void
	*/
	void popQueue();

	/**
	* Checks if the action queue is empty
	* @return bool
	*/
	bool isQueueEmpty() const;

	/**
	* Locks the action queue
	* @return void
	*/
	void lockQueue();

	/**
	* UnLocks the action queue
	* @return void
	*/
	void unlockQueue();

	/**
	* Sets the cross-section plane's position and orientation
	* @param crossSectionNumber is its position identifier (1.0 ... 64.0)
	* @param planeOrientation denotes if it is top(1) or front(0) plane
	* @return void
	*/
	void setCrossSection(const double crossSectionNumber);

	/**
	* Getter method for the crossSection number (i.e. position identifier)
	* @return double
	*/
	double getCrossSectionNumber() const;

	/**
	* Getter method for the plane orientation id
	* @return int
	*/
	int getPlaneOrientationId();
	
	/**
	* Setter method for the plane orientation id
	* @param id
	* @return void
	*/
	void setPlaneOrientation(int id);	

	/**
	* Method that changes the dimension of the 3D bounding box
	* @return void
	*/
	void changeDimension();

	/**
	* Getter method for the X-dimension of the 3D bounding box
	* @return double
	*/
	double getBoxSizeX();
	
	/**
	* Getter method for the Y-dimension of the 3D bounding box
	* @return double
	*/
	double getBoxSizeY();

	/**
	* Getter method for the Z-dimension of the 3D bounding box
	* @return double
	*/
	double getBoxSizeZ();

	/**
	* Getter method for the Z-dimension of the 3D bounding box
	* @return void
	*/
	void addRegions();

	/**
	* Method that remove regions 
	* @return void
	*/
	void removePreviousRegions();

	/**
	* Method that remove domains 
	* @return void
	*/
	void removePreviousDomains();

	/**
	* Method that resets the 3D view
	* @return void
	*/
	void resetView();

	/**
	* Method that cleans the inside of the Bounding Box 
	* @return void
	*/
	//void cleanBoundingBox();

	/**
	* Method that changes the visibility of a Surface3d
	* @param surface index
	* @param on/off flag for the corresponding Surface3d visibility
	* @return void
	*/
	void changeVisibility(int index, int object_type, bool flag);
		
	/**
	* Method that changes the visibility of STRATIGRAPHIC and STRUCTURAL surfaces
	* @param surface type
	* @param on/off flag for the corresponding Surface3d visibility
	* @return void
	*/
	void changeVisibilityBySurfaceCategory(int surface_type, bool flag);

	/**
	* Method that changes the visibility of STRATIGRAPHIC and STRUCTURAL surfaces
	* @param surface id list
	* @param surface type
	* @param on/off flag for the corresponding Surface3d visibility
	* @return void
	*/
	void changeSurfaceVisibilityList(std::vector<int> idList, int surface_type, bool flag);

	/**
	* Method that removes a single region from a domain
	* @param region id 
	* @param domain id 
	* @return void
	*/
	void removeSingleRegionFromDomain(int region, int domain);

	/**
	* Method that removes a single domain from the domain list
	* @param domain id
	* @return void
	*/
	void removeSingleDomain(int domain);
	
	/**
	* Method that adds the last surface preview
	* @return void
	*/
	void addSurfacePreview();

	/**
	* Method that removes the last surface preview
	* @return void
	*/
	void removeSurfacePreview();

	/**
	* Method that clears the last previewed surface when the preview button gets turned off
	* @return void
	*/
	void clearSurfacePreview();

	/**
	* Method that resets the 3D bounding box
	* @return void
	*/
	void resetBoundingBox();

	/**
	* Updates the cross-section 
	* @return void
	*/
	void updateCrossSection();

	/**
	* Getter method for the VE factor
	* @return double
	*/
	double getVeFactor();

	/**
	* Getter method for the Flat factor
	* @return double
	*/
	double getFlatFactor();

signals:
	void rendererInitialized();
	void regionsAdded();
	void regionsRemoved();
	void viewCleared();
	void surfacesRemoved();
	//void surfaceColorChanged();//void surfaceColorChanged(int index, QString color);
	void previewAdded();
	void previewRemoved();
	void surfaceUpdateCompleted();
	void crossSectionUpdate();
	void boundingBoxCleaned();

public slots: 
	void informRegionsAdded();//inform();
	void informRegionsRemoved();
	void refresh3dView();
	void cleanBoundingBox();
	void informSurfaceRemoval();
	void addSurfaces();
	void changeColor(int index, int type_id, QString color);
	void changeVeFactor(double veFactor);
	void adjust3dViewFlatFactor(bool flat_status, double flat_height);

private:

	/**
	* Method that adds actions to the queue
	* @param action 
	* @return void
	*/
	void addAction(Action* action);
	
	//- The surface renderer being contianed by this QQuick item 
	Surface3dRenderer *m_vtkFboRenderer = nullptr;

	//- Command Handling 
	std::shared_ptr<CommandProcessor> m_commandProcessor; /**< The command processor*/
	std::queue<Action*> m_actionQueue;				  /**< The action queue*/
	std::mutex m_actionMutex;					  /**< The action queue mutex*/

	//- Mouse events 
	std::shared_ptr<QMouseEvent> m_lastMouseLeftButton;
	std::shared_ptr<QMouseEvent> m_lastMouseButton;
	std::shared_ptr<QMouseEvent> m_lastMouseMove;
	std::shared_ptr<QWheelEvent> m_lastMouseWheel;

	//- Current cross-section position number 
	double m_crossSectionNumber = 64.0;

	//- Cross-section plane orientation marker
	int m_currentPlaneOrientationId = 2; // 1-WIDTH, 2-LENGTH, 3-HEIGHT

	//- 3D bounding box dimensions 
	double m_boxDimensionX = 0.0;
	double m_boxDimensionY = 0.0;
	double m_boxDimensionZ = 0.0;

	double m_VE_factor = 1.0;					/**< Vertical Exxageration scale factor*/

	double m_flat_height_factor = 1.0;					/**< Flattening height factor*/
	
	double m_original_height = 0.0;					/**< Flattening height factor*/
};

#endif // VTKFBOITEM_H
