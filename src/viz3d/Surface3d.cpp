#include "Surface3d.h"

#include <QDebug>

#include <vtkPolyData.h>
#include <vtkProperty.h>
#include <vtkTransform.h>

Surface3d::Surface3d(vtkSmartPointer<vtkPolyData> meshData, QColor colorHex, int index)
{
	m_index = index;
	m_meshData = meshData;
	m_Color = colorHex;

	//Translate the mesh data to (0.0, 0.0, 0.0)
	vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	translation->Translate(0.0, 0.0, 0.0);
	m_translateFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
	m_translateFilter->SetInputData(m_meshData);
	m_translateFilter->SetTransform(translation);
	m_translateFilter->Update();

	// Surface3d Mapper
	m_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	m_mapper->SetInputConnection(m_translateFilter->GetOutputPort());
	m_mapper->ScalarVisibilityOff();

	// Surface3d Actor
	m_actor = vtkSmartPointer<vtkActor>::New();
	m_actor->SetMapper(m_mapper);
	m_actor->GetProperty()->SetInterpolationToFlat();

	m_actor->GetProperty()->SetAmbient(0.7);
	m_actor->GetProperty()->SetDiffuse(0.8);
	m_actor->GetProperty()->SetSpecular(0.0);
	
	//Position the actor to (0.0, 0.0, 0.0)
	m_actor->SetPosition(0.0, 0.0, 0.0);
}

const vtkSmartPointer<vtkActor> &Surface3d::getActor() const
{
	return m_actor;
}

const QColor Surface3d::getColor() const
{
	return m_Color;
}

void Surface3d::updateSurfaceColor()
{
	if (this->getColor()!=nullptr)
	{
		this->setColor(this->getColor());
	}
	else
	{
		this->setColor(m_defaultColor);
	}
}

void Surface3d::setCustomColor(const QColor& color)
{
	m_Color = color;
	//m_actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
}

void Surface3d::setColor(const QColor &color)
{
	m_actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
}

void Surface3d::setVisibility(bool flag)
{
	m_visible = flag;
}

const bool Surface3d::getVisibility() const
{
	return m_visible;
}

void Surface3d::updateVisibility()
{
	if (this->getVisibility() != true)
	{
		this->setVisibility(this->getVisibility());
	}
	else
	{
		this->setVisibility(m_visible);
	}
}

const int Surface3d::getIndex() const
{
	return m_index;
}

void Surface3d::setOpacity(double opacity)
{
	m_opacity = opacity;
	m_actor->GetProperty()->SetOpacity(m_opacity);
}

void Surface3d::setCategory(int category)
{
	m_category = category;
}

int Surface3d::getCategory()
{
	return m_category;
}