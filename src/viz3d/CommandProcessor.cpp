#include "CommandProcessor.h"

#include <vtkPolyDataNormals.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkGeometryFilter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTriangle.h>

#include <QtConcurrent/QtConcurrentRun>

#include "Surface3d.h"
#include "Region3d.h"
#include "./Model3dHandler.h"
#include "./VisualizationHandler.h"
#include "./utils/vtk_utils.h"
#include "./utils/surface_colors.h"
#include "./object_tree/SurfaceItem.h"
#include "./object_tree/Region.h"
#include "./object_tree/Domain.h"

CommandProcessor::CommandProcessor()
{
	
}

/* Concurrent computation of the addSurfaces() */
std::vector <std::shared_ptr<Surface3d>> CommandProcessor::addSurfaces()
{
	QElapsedTimer timer;
	timer.start();

	//Empty the surface_list so that all modified surfaces displays properly
	if (m_surface_list.size() != 0)
		m_surface_list.clear();

	//Model3dHandler instance
	Model3dHandler* model3dHandler = Model3dHandler::getInstance(NULL);

	//VisualizationHandler instance
	VisualizationHandler* visualizationHandler = VisualizationHandler::getInstance(NULL, nullptr, NULL, NULL);
	std::vector < std::shared_ptr<SurfaceItem>> surfaceList = visualizationHandler->getSurfaceList();

	std::vector<int> index = model3dHandler->getSurfaceIndices();
	////qDebug() << "\n Model Surface Indices: " << index << ", total number of surfaces from the VizHandler = " << surfaceList.size();

	//std::vector< QFuture< vtkSmartPointer<vtkPolyData> > > futures;
	std::vector< QFuture< vtk_utils::vtkSurfaceData > > futures;

	int i = 0;
	for (const std::shared_ptr<SurfaceItem>& surface : surfaceList)
	//for (size_t i = 0; i < index.size(); i++)
	{
		std::vector<double> vertex_coordinates{};
		std::vector<std::size_t> vertex_indices{};

		//Retrieve mesh for the parent surface
		////qDebug() << "\n CommandProcessor::addSurfaces():Parent Surface Index: " << surface->getId();
		if(visualizationHandler->isFlattening())
			model3dHandler->getFlattenedSurfaceMesh(surface->getId(), vertex_coordinates, vertex_indices);
		else model3dHandler->getSurfaceMesh(surface->getId(), vertex_coordinates, vertex_indices);

		//qDebug() << "\nCommandProcessor::addSurfaces():surface_id= " << surface->getId()<< ", coordinate_size=" << vertex_coordinates.size()<< ", indices_size=" << vertex_indices.size()<< "\n";

		//Setup concurrent computation
		//QFuture<vtkSmartPointer<vtkPolyData> > future = QtConcurrent::run(&vtk_utils::triangleMesh2vtkPolyData, vertex_coordinates, vertex_indices);
		QFuture<vtk_utils::vtkSurfaceData > future = QtConcurrent::run(&vtk_utils::triangleMesh2vtkSurfaceData, surface->getId(), surface->getColor(), surface->getCategory(), vertex_coordinates, vertex_indices);
		futures.push_back(future);

		std::vector <std::shared_ptr<SurfaceItem>> childList = surface->getSurfaceChildList();
		if (childList.size() > 0)
		{
			int j = 0;
			for (const std::shared_ptr<SurfaceItem>& child : childList)
			{
				////qDebug() << "\n Child Surface Index: " << child->getId();
				model3dHandler->getSurfaceMesh(child->getId(), vertex_coordinates, vertex_indices);

				//Setup concurrent computation
				//QFuture<vtkSmartPointer<vtkPolyData> > future = QtConcurrent::run(&vtk_utils::triangleMesh2vtkPolyData, vertex_coordinates, vertex_indices);
				QFuture<vtk_utils::vtkSurfaceData > future = QtConcurrent::run(&vtk_utils::triangleMesh2vtkSurfaceData, child->getId(), child->getColor(), child->getCategory(), vertex_coordinates, vertex_indices);
				futures.push_back(future);
			}

		}
		i++;
	}

	// Wait until all concurrent computation is finished and results are ready
	// Then prepare the surface list
	i = 0;
	for (auto& f : futures)
	{
		vtk_utils::vtkSurfaceData surfaceData = f.result();
		std::shared_ptr<Surface3d> surface = std::make_shared<Surface3d>(surfaceData.polyData, surfaceData.color, surfaceData.surfaceId);
		surface->setCategory(surfaceData.category);
		m_surface_list.push_back(surface);

		i++;
	}

	//qDebug() << "\n****************************** Surface Submission *****************************";
	//qDebug() << "\n   CommandProcessor::addSurfaces(): took " << timer.elapsed() << "milliseconds";
	//qDebug() << "\n*******************************************************************************\n";
	return m_surface_list;
}

std::vector <std::shared_ptr<Surface3d>> CommandProcessor::addSurfacePreview()
{
	QElapsedTimer timer;
	timer.start();

	////qDebug() << "CommandProcessor::addSurfacePreview " << m_surface_preview_list.size();
	//Empty the surface_list so that all modified surfaces displays properly
	if (m_surface_preview_list.size() != 0)
		m_surface_preview_list.clear();

	//Model3dHandler instance
	Model3dHandler* model3dHandler = Model3dHandler::getInstance(NULL);

	std::vector<int> index = model3dHandler->getSurfaceIndices();
	////qDebug() << "CommandProcessor::addSurfacePreview " << index.size();

	std::vector<double> vertex_coordinates{};
	std::vector<std::size_t> vertex_indices{};
	//model3dHandler->getSurfaceMesh(index[index.size()-1], vertex_coordinates, vertex_indices);
	//VisualizationHandler instance
	VisualizationHandler* visualizationHandler = VisualizationHandler::getInstance(NULL, nullptr, NULL, NULL);
	if (visualizationHandler->isFlattening())
		model3dHandler->getFlattenedSurfaceMesh(index[index.size() - 1], vertex_coordinates, vertex_indices);
	else model3dHandler->getSurfaceMesh(index[index.size() - 1], vertex_coordinates, vertex_indices);

	//qDebug() << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Surface Preview ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	//qDebug() << "\n   CommandProcessor::addSurfacePreview(): took 1:" << timer.elapsed() << "milliseconds";
	//qDebug() << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

	//vtkSmartPointer<vtkPolyData> triangleMeshData = vtk_utils::triangleMesh2vtkPolyData(vertex_coordinates, vertex_indices);
	QFuture<vtkSmartPointer<vtkPolyData> > futurePolyData = QtConcurrent::run(&vtk_utils::triangleMesh2vtkPolyData, vertex_coordinates, vertex_indices);

	//qDebug() << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Surface Preview ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	//qDebug() << "\n   CommandProcessor::addSurfacePreview(): took 2:" << timer.elapsed() << "milliseconds";
	//qDebug() << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

	//std::shared_ptr<Surface3d> last_surface = std::make_shared<Surface3d>(triangleMeshData, "#FFAB91", -1);//#FFAB91
	std::shared_ptr<Surface3d> last_surface = std::make_shared<Surface3d>(futurePolyData.result(), "#FFAB91", -1);//#FFAB91
	last_surface->setOpacity(1.0);
	last_surface->updateSurfaceColor();

	//qDebug() << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Surface Preview ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	//qDebug() << "\n   CommandProcessor::addSurfacePreview(): took 3:" << timer.elapsed() << "milliseconds";
	//qDebug() << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

	m_surface_preview_list.push_back(last_surface);
	//qDebug() << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Surface Preview ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	//qDebug() << "\n   CommandProcessor::addSurfacePreview(): took 4" << timer.elapsed() << "milliseconds";
	//qDebug() << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
	
	return m_surface_preview_list;
}

void CommandProcessor::removeSurfacePreview()
{
	if (m_surface_preview_list.size() != 0)
		m_surface_preview_list.clear();
}

void CommandProcessor::removeSurfaces()
{
	if (m_surface_list.size() != 0) {
		m_surface_list.clear();
		
	}
}

void CommandProcessor::removeRegions()
{
	if (m_region_list.size() != 0) {
		m_region_list.clear();

	}

	/*if (m_domain_3d_list.size() != 0) {
		m_domain_3d_list.clear();

	}*/
}

std::vector <std::shared_ptr<Surface3d>> CommandProcessor::getAllSurfaces()
{
	return m_surface_list;
}

std::vector <std::shared_ptr<Surface3d>> CommandProcessor::getAllPreviewedSurfaces()
{
	return m_surface_preview_list;
}

std::vector <std::shared_ptr<Region3d>> CommandProcessor::getAllRegions()
{
	return m_region_list;
}

void CommandProcessor::updateSurfaceColor() const
{
	for (const std::shared_ptr<Surface3d>& surface : m_surface_list)
	{
		//if(surface->getVisibility())
			surface->updateSurfaceColor();
	}
}

void CommandProcessor::updateRegionColor() const
{
	for (const std::shared_ptr<Region3d>& region : m_region_list)
	{
		region->updateSurfaceColor();
	}
}

void CommandProcessor::updateSurfaceVisibility() const
{
	for (const std::shared_ptr<Surface3d>& surface : m_surface_list)
	{
		surface->updateVisibility();
	}
}

bool CommandProcessor::updateSurfaceVisibility(int surfaceId, bool visibility) const
{
	for (const std::shared_ptr<Surface3d>& surface : m_surface_list)
	{
		if (surface->getIndex() == surfaceId)
		{
			surface->setVisibility(visibility);
			return true;
		}
	}
	return false;
}

bool CommandProcessor::updateSurfaceVisibilityByTypeId(int object_type_id, bool object_visibility_flag) const
{
	for (const std::shared_ptr<Surface3d>& surface : m_surface_list)
	{
		if (surface->getCategory() == object_type_id)
		{
			surface->setVisibility(object_visibility_flag);
		}
	}
	return false;
}

bool CommandProcessor::updateSurfaceVisibilityByIdList(std::vector<int> id_list, int object_type_id, bool object_visibility_flag) const
{
	for (const std::shared_ptr<Surface3d>& surface : m_surface_list)
	{
		if (std::find(id_list.begin(), id_list.end(), surface->getIndex()) != id_list.end())
		{
			//qDebug() << "CommandProcessor::updateSurfaceVisibilityByIdList(): " << id_list;

			if ((surface->getCategory() == object_type_id))
			{
				surface->setVisibility(object_visibility_flag);
			}
		}
		//else //qDebug() << "CommandProcessor::updateSurfaceVisibilityByIdList(): not found: " << surface->getIndex() ;
		
	}
	return false;
}

bool CommandProcessor::updateRegionVisibility(int regionId, bool visibility) const
{
	for (const std::shared_ptr<Region3d>& region : m_region_list)
	{
		if (region->getIndex() == regionId)
		{
			region->setVisibility(visibility);
			return true;
		}
	}
	return false;
}
bool CommandProcessor::updateSurfaceColor(int surfaceId, QString color) const
{
	int position = 0;
	for (const std::shared_ptr<Surface3d>& surface : m_surface_list)
	{
		if (surface->getIndex() == surfaceId)
		{
			surface->setCustomColor(QColor(color));
			

			return true;
		}

		position++;
	}
	return false;
}

bool CommandProcessor::updateRegionColor(int regionId, QString color) const
{
	////qDebug() << "CommandProcessor::updateRegionColor(): region_id" << regionId;
	//int position = 0;
	for (const std::shared_ptr<Region3d>& region : m_region_list)
	{
		if (region->getIndex() == regionId)
		{
			region->setCustomColor(QColor(color));
			//return true;
		}

		//position++;
	}
	return false;
}

bool CommandProcessor::updateDomainColor(int domainId, QString color) const
{
	int position = 0;
	bool response = false;

	for (const std::shared_ptr<Region3d>& region : m_domain_3d_list)
	{
		if (region->getParentIndex() == domainId)
		{
			region->setCustomColor(QColor(color));
			///return true;
			response = true;
		}

		position++;
	}
	return response;
}

std::shared_ptr<Surface3d> CommandProcessor::getSurface(const vtkSmartPointer<vtkActor> actor) const
{
	try {
		for (const std::shared_ptr<Surface3d>& surface : m_surface_list)
		{
			if (surface->getActor() == actor)
			{
				return surface;
			}
			
		}
		
	}
	catch (int err) {
		//qDebug() << "CommandProcessor::getSurface 1";
	}
	//qDebug() << "CommandProcessor::getSurface 2";

	return nullptr;
}

vtkSmartPointer<vtkPolyData> CommandProcessor::preprocessMesh(const vtkSmartPointer<vtkPolyData> inputData) const
{
	//Translation is not necessary since the modeller already does it
	/*vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	translation->Translate(0., 0., 0.);

	vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
	transformFilter->SetInputData(inputData);
	transformFilter->SetTransform(translation);
	transformFilter->Update();
	*/
	vtkSmartPointer<vtkPolyDataNormals> normals = vtkSmartPointer<vtkPolyDataNormals>::New();
	//normals->SetInputData(transformFilter->GetOutput());
	normals->SetInputData(inputData);
	normals->ComputePointNormalsOn();
	normals->Update();

	return normals->GetOutput();
}

std::vector <std::shared_ptr<Region3d>> CommandProcessor::addRegions()
{
	//Empty the region_list so that all modified region display properly
	if (m_region_list.size() != 0)
		m_region_list.clear();

	//VisualizationHandler instance
	VisualizationHandler* visualizationHandler = VisualizationHandler::getInstance(NULL, nullptr, NULL, NULL);

	std::vector<QString> colorList = visualizationHandler->getRegionColorList();
	std::vector<bool> visibilityList = visualizationHandler->getVisibilityList();
	std::vector < std::shared_ptr<Region>> region_list = visualizationHandler->getRegionList();

	//Model3dHandler instance
	Model3dHandler* model3dHandler = Model3dHandler::getInstance(NULL);

	//std::vector<double> region_list = model3dHandler->getRegionList();

	for (size_t i = 0; i < region_list.size(); i++)
	{
		////qDebug() << "Region id:" << i + 1 << " - color:" << stub::getSurfaceQColor(i + 1);
		std::vector<double> vertex_coordinates{};
		std::vector<std::size_t> vertex_indices{};
		std::vector< std::vector<std::size_t> > element_list{};

		//model3dHandler->model().getRegionsTetrahedralMeshes(vertex_coordinates, element_list);
		if (visualizationHandler->isFlattening())
			model3dHandler->getFlattenedRegionsMeshes(vertex_coordinates, element_list);
		else model3dHandler->model().getRegionsTetrahedralMeshes(vertex_coordinates, element_list);

		vtkSmartPointer<vtkUnstructuredGrid> tetrahedralMeshData = vtk_utils::tetrahedralMesh2vtkUnstructuredGrid(vertex_coordinates, element_list[i]);
		std::shared_ptr<Region>& regionData = region_list.at(i);
		std::shared_ptr<Region3d> region = std::make_shared<Region3d>(tetrahedralMeshData, colorList.at(i), regionData->getId());

		m_region_list.push_back(region);
	}
	//model3dHandler->saveFile("C:\\Temp\\stratmod_surface.rrm");

	return m_region_list;
}

std::vector <std::shared_ptr<Region3d>> CommandProcessor::updateDomainVisibility(int domainId, bool visibility)
{
	if (m_domain_3d_list.size() != 0)
	{
		for (int i=0; i< m_domain_3d_list.size(); i++)
		{
			std::shared_ptr<Region3d>& region = m_domain_3d_list.at(i);

			if (region->getParentIndex() == domainId)
			{
				region->setVisibility(visibility); 
				
			}
			
		}
	}

	return m_domain_3d_list;
}

bool CommandProcessor::doesTheDomainAlreadyExist(int domainId)
{
	if (m_domain_3d_list.size() != 0)
	{
		for (int i = 0; i < m_domain_3d_list.size(); i++)
		{
			std::shared_ptr<Region3d>& region = m_domain_3d_list.at(i);

			if (region->getParentIndex() == domainId)
			{
				return true;
			}

		}
	}
	return false;
}

std::shared_ptr<Region3d> CommandProcessor::getRegionMesh(int regionId)
{
	if (m_region_list.size() != 0)
	{
		for (int i = 0; i < m_region_list.size(); i++)
		{
			std::shared_ptr<Region3d>& region = m_region_list.at(i);

			if (region->getIndex() == regionId)
			{
				return region;
			}

		}
	}

	return nullptr;
}

void CommandProcessor::addToDomainList(int domainId, int regionId, QString color)
{
	//qDebug() << "CommandProcessor::addToDomainList: domain id=" << domainId << " region id=" << regionId;

	std::shared_ptr<Region3d> region3d = getRegionMesh(regionId);
	if (region3d != nullptr)
	{
		std::shared_ptr<Region3d> domain3dChild = std::make_shared<Region3d>(*region3d);
		domain3dChild->setParentIndex(domainId);
		domain3dChild->setCustomColor(color);
		
		m_domain_3d_list.push_back(domain3dChild);
	}
	//else //qDebug() << "CommandProcessor::addToDomainList: region mesh not found!!";
}

std::vector <std::shared_ptr<Region3d>> CommandProcessor::getAllDomainRegions()
{
	return m_domain_3d_list;
}

std::vector <std::shared_ptr<Region3d>> CommandProcessor::getAllDomainRegionsByDomainId(int domain_id)
{
	std::vector <std::shared_ptr<Region3d>> output;

	for (std::shared_ptr<Region3d> region: m_domain_3d_list)
	{
		if (region->getParentIndex() == domain_id)
			output.push_back(region);
	}
	return output;
}

void CommandProcessor::removeRegionFromDomainList(int regionId, int domainId)
{
	if (m_domain_3d_list.size() != 0)
	{
		for (int i = 0; i < m_domain_3d_list.size(); i++)
		{
			std::shared_ptr<Region3d>& region = m_domain_3d_list.at(i);

			if( (region->getIndex() == regionId) && (region->getParentIndex() == domainId))
			{
				m_domain_3d_list.erase(m_domain_3d_list.begin() + i);
				break;
			}
			
		}
	}
	
}

void CommandProcessor::clearDomainList()
{
	if (m_domain_3d_list.size() != 0)
	{
		m_domain_3d_list.clear();
	}

}

