/***************************************************************************

* This class is part of the 3D Visualization module of Geologic surfaces
* Geologic surfaces are rendered inside a 3D bounding box which is 
* basically a vtk second generation widget

* It handles the callback for the interaction with vtkImplicitPlaneWidget2
* by updating the vtkPlane implicit function.

*****************************************************************************/
#include <vtkPlane.h>
#include <vtkActor.h>

#include <vtkCommand.h>
#include <vtkImplicitPlaneWidget2.h>
#include <vtkImplicitPlaneRepresentation.h>

class PlaneWidgetCallBack : public vtkCommand
{
public:
  static PlaneWidgetCallBack*New()
  { 
      return new PlaneWidgetCallBack; 
  }
  
  virtual void Execute(vtkObject *caller, unsigned long, void*)
  {
    vtkImplicitPlaneWidget2 *planeWidget = reinterpret_cast<vtkImplicitPlaneWidget2*>(caller);
    vtkImplicitPlaneRepresentation *rep = reinterpret_cast<vtkImplicitPlaneRepresentation*>(planeWidget->GetRepresentation());
    
    rep->GetPlane(this->Plane);
  }
  
  PlaneWidgetCallBack():Plane(0),Actor(0) 
  {

  }
  
  vtkPlane *Plane;
  vtkActor *Actor;

};