/***************************************************************************

* This class is part of the 3D Visualization module of Geologic surfaces.

* Copyright (C) 2020, Fazilatur Rahman.

* It represents the 3D representation of geologic surfaces and associated
* properties needed for rendering via VTK

*****************************************************************************/

#ifndef REGION3D_H
#define REGION3D_H

#include <QColor>

#include <vtkActor.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTransformFilter.h>
#include <vtkDataSetMapper.h>
#include <vtkSmartPointer.h>

class Region3d
{
public:

	/**
	* Constructor.
	* @param meshData of the surface represented by vtkPolyData.
	* @param colorHex of the surface.
	*/
	Region3d(vtkSmartPointer<vtkUnstructuredGrid> meshData, QColor colorHex, int index);

	/**
	* Copy Constructor.
	* @param other a const reference to another Region3d
	*/
	Region3d(const Region3d& obj);

	/**
	* Assignment operator
	* @param newRegion a const reference to another Region3d.
	*/
	Region3d& operator=(const Region3d& obj);

	/**
	* Method that returns the corresponding actor of a Region3d
	* @return vtkSmartPointer<vtkActor>
	*/
	const vtkSmartPointer<vtkActor>& getActor() const;

	/**
	* Returns the color of a Region3d
	* @return QColor
	*/
	const QColor getColor() const;

	/**
	* Updates the color of a Region3d
	* @return void
	*/
	void updateSurfaceColor();

	/**
	* Returns the visibility of a Region3d
	* @return bool
	*/
	const bool getVisibility() const;

	/**
	* Updates the visibility of a Region3d
	* @return void
	*/
	void updateVisibility();

	/**
	* Returns the index of a Region3D
	* @return int
	*/
	const int getIndex() const;

	/**
	* Sets the color of a Region3D
	* @return void
	*/
	void setCustomColor(const QColor& color);

	/**
	* Sets the visibility of a Region3d
	* @return void
	*/
	void setVisibility(bool flag);

	/**
	* Sets the parent index of a Region3d if it's a child of a domain
	* @return void
	*/
	void setParentIndex(int domainId);

	/**
	* Returns the parent index of a Region3d 
	* @return void
	*/
	int getParentIndex();


private:
	/**
	* Sets the color of a Region3d
	* @return void
	*/
	void setColor(const QColor& color);

	//- Region index
	int m_index;

	//- Region parent index
	int m_parent_index = -1;

	//- Default color of a surface if no color was assigned during surface creation
	QColor m_defaultColor = QColor{ "#0281ca" };

	//- Transform filter 
	vtkSmartPointer<vtkTransformFilter> m_translateFilter;

	//- Mesh data of the region
	vtkSmartPointer<vtkUnstructuredGrid> m_meshData;

	//- Region mapper
	vtkSmartPointer<vtkDataSetMapper> m_mapper;

	//- Surface actor 
	vtkSmartPointer<vtkActor> m_actor;

	//- Region color 
	QColor m_Color;

	//- Region Visibility
	bool m_visible = true;

};

#endif //REGION3D_H
