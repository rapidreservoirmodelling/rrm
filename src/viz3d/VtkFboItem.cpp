#include "VtkFboItem.h"

#include "ActionConstants.hpp"
#include "Action.h"
#include "Surface3dRenderer.h"
#include "./Model3dHandler.h"

VtkFboItem::VtkFboItem()
{
	//Flip Y-axis directions for OpenGL 
	this->setMirrorVertically(true); 

	//Enable All button actions
	setAcceptedMouseButtons(Qt::AllButtons);
	
	//Initialize mouse buttons for VTK interactions
	m_lastMouseLeftButton = std::make_shared<QMouseEvent>(QEvent::None, QPointF(0, 0), Qt::NoButton, Qt::NoButton, Qt::NoModifier);
	m_lastMouseButton = std::make_shared<QMouseEvent>(QEvent::None, QPointF(0, 0), Qt::NoButton, Qt::NoButton, Qt::NoModifier);
	m_lastMouseMove = std::make_shared<QMouseEvent>(QEvent::None, QPointF(0, 0), Qt::NoButton, Qt::NoButton, Qt::NoModifier);
	m_lastMouseWheel = std::make_shared<QWheelEvent>(QPointF(0, 0), 0, Qt::NoButton, Qt::NoModifier, Qt::Vertical);

	
}

QQuickFramebufferObject::Renderer *VtkFboItem::createRenderer() const
{
	return new Surface3dRenderer();
}

void VtkFboItem::setVtkFboRenderer(Surface3dRenderer* renderer)
{
	m_vtkFboRenderer = renderer;
	m_vtkFboRenderer->setCommandProcessor(m_commandProcessor);
}

bool VtkFboItem::isInitialized() const
{
	return (m_vtkFboRenderer != nullptr);
}

void VtkFboItem::setCommandProcessor(const std::shared_ptr<CommandProcessor> commandProcessor)
{
	m_commandProcessor = std::shared_ptr<CommandProcessor>(commandProcessor);
}

void VtkFboItem::removePreviousSurfaces()
{
	Action* action = new Action(ThreeDViewActions::Action::SURFACES_REMOVE, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);
	
	connect(action, &Action::ready, this, &VtkFboItem::update);
	connect(action, &Action::done, this, &VtkFboItem::informSurfaceRemoval);
	
	cThread->start();
	this->addAction(action);
}

void VtkFboItem::removePreviousRegions()
{
	Action* action = new Action(ThreeDViewActions::Action::REGIONS_REMOVE, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);
	
	connect(action, &Action::ready, this, &VtkFboItem::update);
	connect(action, &Action::done, this, &VtkFboItem::informRegionsRemoved);
	
	cThread->start();
	this->addAction(action);
}

void VtkFboItem::removePreviousDomains()
{
	Action* action = new Action(ThreeDViewActions::Action::DOMAINS_REMOVE_ALL, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);

	connect(action, &Action::ready, this, &VtkFboItem::update);
	connect(action, &Action::done, this, &VtkFboItem::informRegionsRemoved);
	
	cThread->start();
	this->addAction(action);
}

void VtkFboItem::addSurfaces()
{
	Action* action = new Action(ThreeDViewActions::Action::SURFACES_ADD, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);

	connect(action, &Action::ready, this, &VtkFboItem::update);
	connect(action, &Action::done, this, &VtkFboItem::surfaceUpdateCompleted);
	
	cThread->start();
	this->addAction(action);
}

void VtkFboItem::addAction(Action* action)
{
	m_actionMutex.lock();
	m_actionQueue.push(action);
	m_actionMutex.unlock();

	update();
}


void VtkFboItem::wheelEvent(QWheelEvent *e)
{
	m_lastMouseWheel = std::make_shared<QWheelEvent>(*e);
	m_lastMouseWheel->ignore();
	e->accept();
	update();
}

void VtkFboItem::mousePressEvent(QMouseEvent *e)
{
	//if (e->buttons() & Qt::RightButton)
	if (e->buttons())
	{
		m_lastMouseButton = std::make_shared<QMouseEvent>(*e);
		m_lastMouseButton->ignore();
		e->accept();
		update();
	}
}

void VtkFboItem::mouseReleaseEvent(QMouseEvent *e)
{
	m_lastMouseButton = std::make_shared<QMouseEvent>(*e);
	m_lastMouseButton->ignore();
	e->accept();
	update();
}

void VtkFboItem::mouseMoveEvent(QMouseEvent *e)
{
	//if (e->buttons() & Qt::RightButton)
	if (e->buttons() )
	{
		*m_lastMouseMove = *e;
		m_lastMouseMove->ignore();
		e->accept();
		update();
	}
}


QMouseEvent *VtkFboItem::getLastMouseLeftButton()
{
	return m_lastMouseLeftButton.get();
}

QMouseEvent *VtkFboItem::getLastMouseButton()
{
	return m_lastMouseButton.get();
}

QMouseEvent *VtkFboItem::getLastMoveEvent()
{
	return m_lastMouseMove.get();
}

QWheelEvent *VtkFboItem::getLastWheelEvent()
{
	return m_lastMouseWheel.get();
}

void VtkFboItem::resetCamera()
{
	m_vtkFboRenderer->resetCamera();
	update();
}

Action* VtkFboItem::getQueueFront() const
{
	return m_actionQueue.front();
}

void VtkFboItem::popQueue()
{
	m_actionQueue.pop();
}

bool VtkFboItem::isQueueEmpty() const
{
	return m_actionQueue.empty();
}

void VtkFboItem::lockQueue()
{
	m_actionMutex.lock();
}

void VtkFboItem::unlockQueue()
{
	m_actionMutex.unlock();
}

void VtkFboItem::setCrossSection(const double crossSectionNumber)
{
	try {
		if (this) {
			if (m_crossSectionNumber != crossSectionNumber)
			{
				m_crossSectionNumber = crossSectionNumber;
				update();
			}
		}
	}
	catch (int error) {
		//qDebug() << "VtkFboItem - setVerticalCrossSection(): exception" << error;

	}
	
}

double VtkFboItem::getCrossSectionNumber() const
{
	return m_crossSectionNumber;
}

void VtkFboItem::setPlaneOrientation(int id)
{
	try {
		if (this) {
			if (m_currentPlaneOrientationId != id)
			{
				m_currentPlaneOrientationId = id;
				update();
			}
		}
	}
	catch (int error) {
		//qDebug() << "VtkFboItem-setPlaneOrientation(): exception" << error;

	}
}

int VtkFboItem::getPlaneOrientationId()
{
	return m_currentPlaneOrientationId;
}

void VtkFboItem::changeDimension()
{
	try {
		if (this) {
			if ((m_boxDimensionX == 0.0)&& (m_boxDimensionY == 0.0)&& (m_boxDimensionZ == 0.0))
			{
				Model3dHandler::getInstance(nullptr)->getModelDimension(m_boxDimensionX, m_boxDimensionY, m_boxDimensionZ);
				m_currentPlaneOrientationId = 2;
				m_original_height = m_boxDimensionZ;
				update();
			}
		}
	}
	catch (int error) {
		//qDebug() << "VtkFboItem-setPlaneOrientation(): exception" << error;

	}
}

double VtkFboItem::getBoxSizeX()
{
	return m_boxDimensionX;
}

double VtkFboItem::getBoxSizeY()
{
	return m_boxDimensionY;
}

double VtkFboItem::getBoxSizeZ()
{
	return m_boxDimensionZ;
}

void VtkFboItem::addRegions()
{
	Action* action = new Action(ThreeDViewActions::Action::REGIONS_ADD, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);

	connect(action, &Action::ready, this, &VtkFboItem::update);
	connect(action, &Action::done, this, &VtkFboItem::informRegionsAdded);
	
	cThread->start();
	this->addAction(action);

}

void VtkFboItem::resetView()
{
	Action* action = new Action(ThreeDViewActions::Action::CLEAR, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);

	connect(action, &Action::ready, this, &VtkFboItem::update);
	connect(action, &Action::done, this, &VtkFboItem::viewCleared);
	
	cThread->start();
	this->addAction(action);
}

void VtkFboItem::informRegionsAdded()
{
	emit(regionsAdded());
}

void VtkFboItem::informRegionsRemoved()
{
	emit(regionsRemoved());
}

void VtkFboItem::refresh3dView()
{
	//emit(viewCleared());
	emit(surfacesRemoved());
}

void VtkFboItem::cleanBoundingBox()
{
	Action* action = new Action(ThreeDViewActions::Action::CLEAR, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);

	connect(action, &Action::done, this, &VtkFboItem::update);
	
	cThread->start();
	this->addAction(action);
}

void VtkFboItem::changeVisibility(int index, int object_type, bool flag)
{
	////qDebug() << "VtkFboItem::changeVisibility(): index=" << index << ", object type: " << object_type;
	Action* action = new Action(ThreeDViewActions::Action::CHANGE_VISIBILITY, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);
	action->SetVisibilityChangeParameter(index, object_type, flag);
	
	connect(action, &Action::ready, this, &VtkFboItem::update);
	cThread->start();
	this->addAction(action);
}

void VtkFboItem::changeVisibilityBySurfaceCategory(int surface_type, bool flag)
{
	//qDebug() << "VtkFboItem::changeVisibilityBySyrfaceCategory(): " << ", surface type: " << surface_type;
	Action* action = new Action(ThreeDViewActions::Action::CHANGE_VISIBILITY_SURFACE_TYPE, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);
	action->SetVisibilityChangeParameter(surface_type, flag);
	
	connect(action, &Action::ready, this, &VtkFboItem::update);
	
	cThread->start();
	this->addAction(action);
}

void VtkFboItem::changeSurfaceVisibilityList(std::vector<int> idList, int surface_type, bool flag)
{
	//qDebug() << "VtkFboItem::changeSurfaceVisibilityList(): " << idList;
	Action* action = new Action(ThreeDViewActions::Action::CHANGE_SURFACE_VISIBILITY_LIST, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);

	action->SetSurfaceVisibilityChangeParameter(idList, surface_type, flag);
	connect(action, &Action::ready, this, &VtkFboItem::update);
	
	cThread->start();
	this->addAction(action);
}
void VtkFboItem::changeColor(int index, int type_id, QString color)
{
	////qDebug() << "VtkFboItem::changeColor(): index_id" << index <<", type: " << type_id;

	Action* action = new Action(ThreeDViewActions::Action::CHANGE_COLOR, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);

	action->SetColorChangeParameter(index, type_id, color);
	connect(action, &Action::ready, this, &VtkFboItem::update);
	
	cThread->start();
	this->addAction(action);
}

void VtkFboItem::informSurfaceRemoval()
{
	emit(surfacesRemoved());
}

void VtkFboItem::removeSingleRegionFromDomain(int region, int domain)
{
	Action* action = new Action(ThreeDViewActions::Action::DOMAINS_REMOVE_SINGLE_REGION, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);

	action->SetParameters4RemoveRegionFromDomain(region, domain);
	connect(action, &Action::ready, this, &VtkFboItem::update);
	connect(action, &Action::done, this, &VtkFboItem::update);
	
	cThread->start();
	this->addAction(action);
}

void VtkFboItem::removeSingleDomain(int domain)
{
	qDebug() << "VtkFboItem::removeSingleDomain(): " << domain;
	Action* action = new Action(ThreeDViewActions::Action::DOMAINS_REMOVE_SINGLE_DOMAIN, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);

	action->SetParameters4RemoveDomain(domain);
	connect(action, &Action::ready, this, &VtkFboItem::update);
	connect(action, &Action::done, this, &VtkFboItem::update);

	cThread->start();
	this->addAction(action);
}

void VtkFboItem::addSurfacePreview()
{
	Action* action = new Action(ThreeDViewActions::Action::PREVIEW_ADD, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);

	connect(action, &Action::ready, this, &VtkFboItem::update);
	connect(action, &Action::done, this, &VtkFboItem::previewAdded);
	
	cThread->start();
	this->addAction(action);
}

void VtkFboItem::removeSurfacePreview()
{
	Action* action = new Action(ThreeDViewActions::Action::PREVIEW_REMOVE, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);

	connect(action, &Action::ready, this, &VtkFboItem::update);
	connect(action, &Action::done, this, &VtkFboItem::previewRemoved);
	
	cThread->start();
	this->addAction(action);

}

void VtkFboItem::clearSurfacePreview()
{
	Action* action = new Action(ThreeDViewActions::Action::PREVIEW_REMOVE, m_commandProcessor, m_vtkFboRenderer);
	QThread* cThread = new QThread;
	action->setup(cThread);
	action->moveToThread(cThread);

	connect(action, &Action::ready, this, &VtkFboItem::update);
	//connect(command, &Action::done, this, &VtkFboItem::previewRemoved);
	
	cThread->start();
	this->addAction(action);
}

void VtkFboItem::resetBoundingBox()
{
	m_boxDimensionX = 0.0;
	m_boxDimensionY = 0.0;
	m_boxDimensionZ = 0.0;

}

void VtkFboItem::updateCrossSection()
{
	//qDebug() << "VtkFboItem::updateCrossSection()";
	update();
}

void VtkFboItem::changeVeFactor(double veFactor)
{
	//qDebug() << "VtkFboItem::changeVeFactor:" << veFactor;
	
	try {
		if (this) {
			if ((m_VE_factor != veFactor) && (veFactor != 0.0))
			{
				m_VE_factor = veFactor;
				update();
			}
		}
	}
	catch (int error) {
		//qDebug() << "VtkFboItem::changeVeFactor:" << error;

	}
}

double VtkFboItem::getVeFactor()
{
	return m_VE_factor;
}

void VtkFboItem::adjust3dViewFlatFactor(bool flat_status, double flat_height)
{
	//qDebug() << "VtkFboItem::adjust3dViewFlatFactor:" << flat_height;
	if (flat_status)
	{
		double flatFactor = double(flat_height / m_boxDimensionZ);
		try {
			if (this) {
				if ((m_flat_height_factor != flatFactor) && (flatFactor != 0.0))
				{
					//Change the height
					m_flat_height_factor = flatFactor;
					update();
					//Clean the bounding box
					cleanBoundingBox();
					boundingBoxCleaned();
					//update();
					
					//Add surfaces
					//addSurfaces();
				}
			}
		}
		catch (int error) {
			//qDebug() << "VtkFboItem::changeVeFactor:" << error;

		}
	}
	else {
		m_flat_height_factor = 1.0;
		update();
		//Clean the bounding box
		cleanBoundingBox();
		boundingBoxCleaned();
		//Add surfaces
		//addSurfaces();

	}
}

double VtkFboItem::getFlatFactor()
{
	return m_flat_height_factor;
}