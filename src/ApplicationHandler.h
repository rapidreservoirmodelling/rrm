
/***************************************************************************

* This is the main application handler.

* Copyright (C) 2020, Fazilatur Rahman.

* It contains the main gui including the 3D visualization window and the 
* skectch canvas.

*****************************************************************************/

#ifndef APPLICATIONHANDLER_H
#define APPLICATIONHANDLER_H

#include <memory>

#include <QObject>
#include <QUrl>
#include <QApplication>

#include "utils/event_receiver.h"
#include "CanvasHandler.h"

#include "fd/fd_interface.hpp"

class CommandProcessor;
class VtkFboItem;

class Model3dHandler;
class RrmTreeModel;

class VisualizationHandler;

class ApplicationHandler : public QObject
{
	Q_OBJECT

public:
	/**
	* Constructor.
	*/
	ApplicationHandler(int argc, char **argv);

	/**
	* Method that initiates surface preview using the curve drawn on the Canvas
	* This is an invokable method that gets called from QML
	* @return void
	*/
	Q_INVOKABLE void addSurfacePreview() const;
		
	
signals: 
	
public slots:
	
	/**
	* Method that toggles FD flag
	* This is an invokable method that may be called from QML
	* @return void
	*/
	Q_INVOKABLE void toggleFdFlag();

	/**
	* Method that returns FD flag
	* This is an invokable method that may be called from QML
	* @return bool
	*/
	Q_INVOKABLE bool fdFlag();

	/**
	* Method that returns FD window width
	* This is an invokable method that may be called from QML
	* @return int
	*/
	Q_INVOKABLE int getFdWindowWidth();

	/**
	* Method that returns FD flag
	* This is an invokable method that may be called from QML
	* @return bool
	*/
	Q_INVOKABLE int getFdWindowHeight();

	/**
	* Method that connects to the temporary interface for FD integration
	* This is an invokable method that may be called from QML
	* @return void
	*/
	Q_INVOKABLE void connectToFdInterface();
	
	/**
	* Method that closes the FD window
	* This is an invokable method that may be called from QML
	* @return void
	*/
	Q_INVOKABLE void closeFdInterface();

	void setPath(std::string path);

private:
	//- Command Processor for 3D Visualization
	std::shared_ptr<CommandProcessor> m_commandProcessor; 
	
	//- Instance of the QML type that contains the VTK FrameBuffer Object for 3DViz
	VtkFboItem *m_vtkFboItem = nullptr;
	
	//- Facade for the stratmod library and the rules processor
	Model3dHandler* m_model3dHandler;
	
	//- QML event handler
	EventReceiver receiver;

	//- Drawing Canvas handler
	CanvasHandler canvasHandler;

	//- Object tree model
	RrmTreeModel* m_treeModel;

	//- Viz flag
	bool m_viz = false;

	//- 3D bounding box dimensions 
	double m_boxDimensionX = 0.0;
	double m_boxDimensionY = 0.0;
	double m_boxDimensionZ = 0.0;

	//- Fd window 
	bool m_fdWindowOpenFlag = false;
	int m_fdWindowWidth = 1024;
	int m_fdWindowHeight = 700;

	//- Facade for handling visualization and gui interaction
	VisualizationHandler* m_vizHandler;
	std::unique_ptr<FlowDiagnosticsInterface> fdInterface = std::make_unique<FlowDiagnosticsInterface>();

	//- Model path
	std::string m_modelPath = "";
};

#endif // APPLICATIONHANDLER_H
