<!-- Setup and installation details are available here: https://docs.google.com/document/d/1JpQ1T7bnzLMkdvyadHmuWKUTX1jxmuYJIpRpVL54h4M/edit#  -->

Rapid reservoir modelling (Phase 2)
===
Sketch-based geological 3D modelling with flow diagnostics: Exploring uncertainty through interactive prototyping
-----------------------------------------------------------------------

Rapid reservoir modelling (RRM) provides a unified graphical user interface (GUI) containing all the
necessary tools to create, visualize and test 3D geological models built from
cross-section and contour sketches. At its core sits a new modelling framework,
designed to create models with guarantees of geological consistency.
A description of geological operators and applications from a user's point of
view is available in [Jacquemyn et al
(2021)](https://doi.org/10.1144/jgs2020-187), while a description of flow
diagnostics is available in [Petrovskyy et al.
(2023)](https://doi.org/10.1016/j.ijggc.2023.103855). See also
[https://rapidreservoir.org](https://rapidreservoir.org).

RRM is built to allow interactive operation: modelling and visualization of
individual surfaces are performed in milliseconds on commodity, over the shelf
hardware and computational time scales linearly with the number of surfaces.

## Downloads

The latest release can be found at:

| Platform                | Files                                                                                                                     |
|-------------------------|---------------------------------------------------------------------------------------------------------------------------|
| Windows ZIP (portable)  | [rrm2-windows-x86_64](https://bitbucket.org/rapidreservoirmodelling/rrm/downloads/rrm2-windows-x86_64.zip)  |




To run RRM, unpack a ZIP archive to your preferred folder and simply run the
executable `RRM6.exe` (installation is not required).  All Windows binaries have
been tested in Windows 10.


## Building RRM form source

### Minimal Requirements

RRM uses the [CMake](https://cmake.org/download/) build system, and its main Sketching GUI currently depends on the
following external libraries that must be obtained and properly installed
before RRM can be compiled.

- [Qt5] (components: Core, Gui, Widgets, OpenGL, Svg)
- [Eigen3]
- [Glew]

[Qt5]: https://www.qt.io/download
[Eigen3]: http://eigen.tuxfamily.org/index.php?title=Main_Page
[Glew]: http://glew.sourceforge.net/

Instructions on how to obtain these libraries are platform dependent and will
be provided below.  [Git](https://git-scm.com/downloads) is also required. The necessary feature list is C++17.

### Building the Sketching GUI and Flow Diagnostics

This version of RRM includes both a Sketching GUI and a Flow Diagnostics App,
and both were built and tested in the following environments:

- Windows 10, MSVC 14.34

The Flow Diagnostics App requires VTK 8.2 in addition to the dependencies
required by the main Sketching GUI.  To compile both RRM's Sketching GUI and
the Flow Diagnostics App, we suggest using the
[vcpkg](https://vcpkg.io/en/index.html) C/C++ package manager to acquire and
build its dependencies.  To simplify the building process, we currently use
a [fork](https://github.com/dp-69/vcpkg) of `vcpkg`'s main repository, which
incorporates changes required for a smooth compilation process. 

#### Windows 10

The following instructions assume that our fork of `vcpkg`'s repository will be
cloned into the ```C:\``` folder, and that all commands are issued from a **x64
Native Tools Command Prompt for VS 2022** command prompt (which is a part of
Microsoft's Visual Studio toolset).

- Step 1 - Installing RRM's dependencies in vcpkg:
```cmd
> git clone https://github.com/dp-69/vcpkg.git
> cd vcpkg
> bootstrap-vcpkg.bat
> vcpkg.exe install rrmlibs-qtquick:x64-windows --overlay-ports=ports-rrm/core --overlay-ports=ports-rrm/qt-5.12.5
```

- Step 2 - RRM compilation:
```cmd
> git clone https://bitbucket.org/rapidreservoirmodelling/rrm.git
> cd rrm2
> git checkout phase2
> cmake --preset=win-rwdi
> cmake --build --preset=win-rwdi-rrm
```

- Step 3 - Executing RRM:
```cmd
> cd build/RelWithDebInfo/build
> RRM6.exe
```

## Copyright

The [RRM project's](https://rapidreservoir.org) research results are a copyright of the respective current and past members:

### University of Calgary, Canada
[Clarissa Coda Marques](https://www.linkedin.com/in/clarissa-marques-9bb49296)<br/>
[Julio Daniel Silva](https://www.linkedin.com/in/julio-machado-silva-490bb3251)<br/>
Felipe Moura de Carvalho<br/>
Emilio Vital Brazil<br/>
[Sicilia Judice](http://www.linkedin.com/in/sicilia-judice)<br/>
Fazilatur Rahman<br/>
Mario Costa Sousa  

### Delft University of Technology, Netherlands<sup>[1](#footnote-delft)</sup>
[Dmytro Petrovskyy](https://www.linkedin.com/in/dmytro-petrovskyy/)<br/>
Zhao Zang<br/>
[Sebastian Geiger](https://www.tudelft.nl/en/staff/s.geiger/)

### Imperial College London, UK
[Carl Jacquemyn](https://www.linkedin.com/in/carljacquemyn/)<br/>
Margaret E.H. Pataki<br/>
[Gary J. Hampson](https://www.linkedin.com/in/gary-hampson-icl/)<br/>
Matthew D. Jackson  

This software also includes [third party
libraries](https://bitbucket.org/rapidreservoirmodelling/rrm2/src/main/rrm_3rd_party_libraries.md)
developed outside of the RRM project. 
An overview of these libraries including brief descriptions and the last known
URL their code was made available at can be found
[here](https://bitbucket.org/rapidreservoirmodelling/rrm2/src/main/rrm_3rd_party_libraries.md).
Please, check the source code for details.

<sup><a name="footnote-delft">1</a></sup> Parts of the code were developed while S. Geiger and D. Petrovskyy were at Heriot-Watt University, with support from Z. Zhang.

## License

RRM is Free Software released under the 
[GPLv3](https://www.gnu.org/licenses/gpl.html) license.  It includes 
[third party libraries](https://bitbucket.org/rapidreservoirmodelling/rrm2/src/main/rrm_3rd_party_libraries.md)
with other licenses, which we understand to be compatible. An overview of these libraries including
brief descriptions, the last known URL the code was made available at, the date
it was retrieved, the license(s) in which the code was made available by their
authors at the retrieving date, and URL links to publicly available copies of
such licenses is available
[here](https://bitbucket.org/rapidreservoirmodelling/rrm2/src/main/rrm_3rd_party_libraries.md).
Please, check the source code for details on the licenses that apply to each
piece of software.


## Sponsors

This software was developed during the first phase of the [RRM
project](https://rapidreservoir.org) (from 2014 to 2018), which was sponsored
by:

- Equinor
- ExxonMobil
- IBM Research Brazil/IBM Centre for Advanced Studies (CAS) Alberta
- Petrobras
- Shell 

The current phase of the project (from 2019 to 2023) is being sponsored by:

- Equinor
- ExxonMobil
- Petrobras
- Petronas 
- Shell 

We thank our present and past sponsors for helping to make this research possible.
