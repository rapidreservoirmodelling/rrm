import QtQuick 2.0

import Constants 1.0

import "scripts/window.js" as WindowScript
import "scripts/promise.js" as Q

Item {

    //- Sketch curves
    property var curve: []          // To store the current sketch
    property var curves: []         // To store all current sketches
    property var temp_curve: []     // To store the current oversketch
    property var submittedCurves: []// To store the curves of each surface that intersect with the current cross section
    property var currentCurves: []  // To store the current sketches in each cross section
    property var colorMap: []               //To keep track of custom colors for curves
    property var visibilityMap: []          //To keep track of visibility for curves

    //- Trajectory curves
    property var path: []           // To store the current sketch of the trajectory
    property var temp_path: []      // To store the current oversketch of the trajectory
    property var submittedPaths: [] // To store the list of submitted trajectories

    //- Canvas Scale factors
    property real canvasZoomFactor: 1 // To store mouse wheel zoom factor

    //- Circle and Ellipse
    property real radiusX: 500.0          // To store the radius in X of the circle
    property real radiusY: 500.0          // To store the radius in Y of the circle

    //- Cross Section properties
    property real csMap_Width: 500.0      // Width of the model modified by the visualization scale
    property real csMap_Height: 500.0     // Height of the model modified by the visualization scale
    property real csIDPosition: 500.0     // identify the real position of the cross section in front view
    property real csHeightPosition: 500.0 // identify the real position of the cross section in map view

    //- Selection of canvas elements
    property bool selectSketch: false     // To keep track if the current sketch was selected by the user
    property bool selectPath: false       // To keep track if the current trajectory was selected by the user

    //- Guidelines
    property bool guidelines: false         // To keep track if the guidelines are visible or not
    property bool glMoving: true            // To keep track if the guidelines are moving or not
    property bool sketchLine: false         // To keep track if the sketch line are visible or not
    property bool sketchCircle: false       // To keep track if the sketch circle are visible or not
    property bool sketchEllipse: false       // To keep track if the sketch ellipse are visible or not
    property var glMouse: []                // To receive the mouse position to update the guidelines
    property int glMouseX: 0                // To store the mouseX position to update the guidelines
    property int glMouseY: 0                // To store the mouseY position to update the guidelines
    property real glHeight: 500.0           // To store the height position of the fixed horizontal guideline representing the cross section
    property real glWidth: 500.0            // To store the width position of the fixed horizontal guideline representing the cross section


    function reset() {
        curve = [];
        curves = [];
        temp_curve = [];
        submittedCurves = [];
        currentCurves = [];
        path = [];
        temp_path = [];
        submittedPaths = [];
        canvasZoomFactor = 1.0;
        radiusX = 500.0;
        radiusY = 500.0;
        csMap_Width = 500.0;
        csMap_Height = 500.0;
        csIDPosition = 500.0;
        csHeightPosition = 500.0;
        selectSketch = false;
        selectPath = false;
        guidelines = false;
        glMoving = true;
        sketchLine = false;
        sketchCircle = false;
        sketchEllipse = false;
        glMouse = [];
        glMouseX = 0;
        glMouseY = 0;
        glHeight = 500.0;
        glWidth = 500.0;
        colorMap = [];
        visibilityMap = [];

        updateScale();
        updateResize();
    }

    function updateScale() {
        canvasHandler.csMap_Scale = ((70.0 * myCanvas.width) / 100.0) / model3DHandler.size_x;
        if( (model3DHandler.size_y * canvasHandler.csMap_Scale) > ((70.0 * myCanvas.height) / 100.0) ) {
            canvasHandler.csMap_Scale = canvasHandler.csMap_Scale * (((70.0 * myCanvas.height) / 100.0) / (model3DHandler.size_y * canvasHandler.csMap_Scale));
        }
        csMap_Width = model3DHandler.size_x * canvasHandler.csMap_Scale;
        csMap_Height = model3DHandler.size_y * canvasHandler.csMap_Scale;

        radiusX = csMap_Height / 6.0;
        radiusY = csMap_Height / 8.0;
    }

    function updateResize() {
        canvasHandler.csMap_Origin_x = (myCanvas.width / 2.0) - (csMap_Width / 2.0);
        canvasHandler.csMap_Origin_y = (myCanvas.height / 2.0) - (csMap_Height / 2.0);

        if (canvasHandler.direction === DIRECTION.HEIGHT ) {
            canvasHandler.returnSketch();
            canvasHandler.updateCurrentContours();
            myFunctions.currentCurves = canvasHandler.returnCurrentContours();
        } else {
            canvasHandler.returnTrajectory();
        }
        myFunctions.submittedCurves = canvasHandler.returnSubmittedContours();
        myFunctions.submittedPaths = canvasHandler.returnSubmittedTrajectories();

        glHeight = ((csMap_Height * canvasHandler.crossSection) / model3DHandler.length_Discretization) + canvasHandler.csMap_Origin_y;
        glWidth  = ((csMap_Width  * canvasHandler.crossSection) / model3DHandler.width_Discretization)  + canvasHandler.csMap_Origin_x;
    }

    function updateVizData(color, visibility){
        colorMap = color
        visibilityMap = visibility
    }

    function rightButtonDoubleClickAction() {
        path = [];
        temp_path = [];
        currentCurves = [];
        curve = [];
        selectSketch = false;
        canvasHandler.resetHighlightSelection_MV();
        submittedPaths = canvasHandler.returnSubmittedTrajectories();
        submittedCurves = canvasHandler.returnSubmittedContours();
        myCanvas.requestPaint();
    }


    Connections {
        target: canvasHandler

        onSelectSketchMV: {
            selectSketch = response;
            myCanvas.requestPaint();
        }

        onHighlightSketch: {
            myCanvas.requestPaint();
        }

        onHighlightTrajectory: {
            myCanvas.requestPaint();
        }

        onSelectTrajectory: {
            selectPath = response;
            myCanvas.requestPaint();
        }

        onGetUpdatedSketch: {
            if(canvasHandler.direction === DIRECTION.HEIGHT) {
                curves = canvasHandler.returnCurrentSketches();
                myCanvas.requestPaint();
            }
        }

        onGetUpdatedSketches: {
            if(canvasHandler.direction === DIRECTION.HEIGHT) {
                curves = response;
                myCanvas.requestPaint();
            }
        }

        onGetUpdatedOverSketch: {
            if(canvasHandler.direction === DIRECTION.HEIGHT) {
                temp_curve = WindowScript.parseSketchData(response);
                myCanvas.requestPaint();
            }
        }

        onGetUpdatedTrajectory: {
            path = WindowScript.parseSketchData(response);
            myCanvas.requestPaint();
        }

        onGetUpdatedTrajectoryOverSketch: {
            temp_path = WindowScript.parseSketchData(response);
            myCanvas.requestPaint();
        }

        onRefreshSubmittedCurves: {
            submittedCurves = canvasHandler.returnSubmittedContours();
            submittedPaths = canvasHandler.returnSubmittedTrajectories();
            myCanvas.requestPaint();
        }

        onResetNew: {
            reset();
            myCanvas.requestPaint();
        }

        onDirectionChanged: {
            selectSketch = false;
            canvasHandler.resetHighlightSelection_MV();
            selectPath = false;

            if (canvasHandler.direction === DIRECTION.LENGTH) {
                csIDPosition = ((model3DHandler.size_y * canvasHandler.crossSection) / model3DHandler.length_Discretization);
            } else if (canvasHandler.direction === DIRECTION.WIDTH) {
                csIDPosition = ((model3DHandler.size_x * canvasHandler.crossSection) / model3DHandler.width_Discretization);
            } else if(canvasHandler.direction === DIRECTION.HEIGHT) {
                csHeightPosition = ((model3DHandler.size_z * canvasHandler.crossSection) / model3DHandler.width_Discretization);
            }
            myCanvas.requestPaint();
        }

        onUpdateLoadedData: {
            submittedCurves = canvasHandler.returnSubmittedContours();
            submittedPaths = canvasHandler.returnSubmittedTrajectories();
            myCanvas.requestPaint();
        }

        onPgeIsActiveChanged: {
            receiver.updatePreview();
        }
    }//end: Connections canvasHandler


    Connections {
        target: receiver //the C++ Object

        onUpdateCanvasAction: {
            sketchLine = false;
            sketchCircle = false;
            sketchEllipse = false;
            myPinch.enabled = false;
            if(action === ACTION.FB_FIX_GUIDELINES && !glMoving) {
                glMoving = true;
            } else {
                canvasHandler.canvasAction = action;
                if(canvasHandler.canvasAction === ACTION.FB_SKETCH_LINE) {
                    sketchLine = true;
                } else if (canvasHandler.canvasAction === ACTION.FB_SKETCH_CIRCLE) {
                    sketchCircle = true;
                } else if (canvasHandler.canvasAction === ACTION.FB_SKETCH_ELLIPSE) {
                    sketchEllipse = true;
                } else if(canvasHandler.canvasAction === ACTION.FB_ZOOM) {
                    myPinch.enabled = true;
                }
            }
            myCanvas.requestPaint();
        }

        onUpdateCanvasVisualizationMV: {
            sketchLine = false;
            sketchCircle = false;
            sketchEllipse = false;
            canvasHandler.delegateMapViewVisualization(action);
            switch(action)
            {
            case ACTION.FB_SHOW_GUIDELINES:
                guidelines = !guidelines;
                break;
            case ACTION.MV_SAVE_SKETCH_TEMPLATE:
                canvasHandler.saveSketchContourTemplate();
                break;
            case ACTION.MV_SMOOTH_SKETCH:
            case ACTION.MV_CLEAR_SKETCH:
            case ACTION.MV_REDO_SKETCH:
            case ACTION.MV_UNDO_SKETCH:
                receiver.updatePreview();
                break;
            }
            myCanvas.requestPaint();
        }

        onReceiveMouseFromFrontView: {
            if(guidelines && glMoving) {
                glMouse = WindowScript.parseSketchData(response);
                if (canvasHandler.direction === DIRECTION.LENGTH ) {
                    glMouseX = (((glMouse[0].x - glMouse[2].x) / glMouse[1].x) * csMap_Width)  + canvasHandler.csMap_Origin_x;
                } else if (canvasHandler.direction === DIRECTION.WIDTH ) {
                    //glMouseY = ((1-((glMouse[0].x - glMouse[2].x) / glMouse[1].x)) * csMap_Height) + canvasHandler.csMap_Origin_y;
                    glMouseY = ((((glMouse[0].x - glMouse[2].x) / glMouse[1].x)) * csMap_Height) + canvasHandler.csMap_Origin_y;
                }
                canvasHandler.fixGuidelineMapView(glMouseX,glMouseY);
                glMouseX = canvasHandler.getMV_gl_mouseX();
                glMouseY = canvasHandler.getMV_gl_mouseY();
                myCanvas.requestPaint();
            }
        }

        onSendFixMapViewGuidelines: {
            if(guidelines)
            {
                glMoving = false;
            }
        }

        onCrossSectionChanged: {
            canvasHandler.crossSection = id;
            selectSketch = false;
            canvasHandler.resetHighlightSelection_MV();
            selectPath = false;
            canvasHandler.returnSketch();
            csHeightPosition = ((model3DHandler.size_z * canvasHandler.crossSection) / model3DHandler.width_Discretization);
            glHeight = ((csMap_Height * canvasHandler.crossSection) / model3DHandler.length_Discretization) + canvasHandler.csMap_Origin_y;
            glWidth  = ((csMap_Width  * canvasHandler.crossSection) / model3DHandler.width_Discretization)  + canvasHandler.csMap_Origin_x;
            if (canvasHandler.direction === DIRECTION.HEIGHT ) {
                canvasHandler.updateCurrentContours();
                currentCurves = canvasHandler.returnCurrentContours();
            } else if (canvasHandler.direction === DIRECTION.LENGTH ) {
                csIDPosition = ((model3DHandler.size_y * canvasHandler.crossSection) / model3DHandler.length_Discretization);
            } else if (canvasHandler.direction === DIRECTION.WIDTH ) {
                csIDPosition = ((model3DHandler.size_x * canvasHandler.crossSection) / model3DHandler.width_Discretization);
            }
            myCanvas.requestPaint();
        }

        onSurfaceCreated: {
            if(canvasID === CANVAS_ID.FRONT_VIEW) {
                rightButtonDoubleClickAction();
            }
        }

        onSendTrajectoryTemplateIndex: {
            canvasHandler.loadTrajectoryTemplate(index);
            receiver.updatePreview();
        }

        onSendContourTemplateIndex: {
            canvasHandler.loadSketchContourTemplate(index);
            canvasHandler.addCrossSectionUsed(canvasHandler.crossSection, canvasHandler.direction);
            receiver.updatePreview();
            myMouseArea.mouseButtonIdentifier = MOUSE.NO_BUTTON;
            canvasHandler.resetHighlightSelection_MV();
            myCanvas.requestPaint();
        }
    }//end: Connections receiver

    Connections {
        target: newVizHandler

        onModelLoadingCompleted: {
            updateScale();
            updateResize();
            myCanvas.requestPaint();
        }

        onVizFlagChanged: {
            if(flag) {
                myCanvas.state = "REGION";
            } else {
                myCanvas.state = "SKETCH";
            }
        }

        onSurfaceMapUpdated: {
            updateVizData(colorMap, visibilityMap);
            myCanvas.requestPaint();
        }

    }//end: Connections VisualizationHandler


}
