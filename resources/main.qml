import QtQuick 2.12
import QtQuick.Controls 1.4// 1.4 works for splitview//2.2 does not work for swipeview
import QtQuick.Window 2.3

import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2

import Qt3dFbo 1.0
import QtQuick.Controls 2.12 //To allow version2 of the Controls
import QtQuick.Dialogs 1.2

import Constants 1.0

import "scripts/db.js" as ImageScript
import "scripts/treeview.js" as TreeScript

ApplicationWindow {
    id: main
    minimumWidth: 1024
    minimumHeight: 700
    width: 1024
    height: 700
    visible: true
    title: "RRM: Rapid Reservoir Modelling"
    visibility: "Maximized"

    Material.theme: Material.Light
    Material.accent: Material.LightBlue
    Material.primary: Material.Indigo

    //- Model Properties to be used in QML side
    property int model_discretization: 64

    //Handle closing event
    onClosing: {
        ////console.log ("I am in main.qml- ImageScript!" + JSON.stringify(ImageScript.readAllRows()) )
        ImageScript.dbClear()
        ////console.log ("I am in main.qml-TreeScript !" + JSON.stringify(TreeScript.readAllRows()) )
        TreeScript.dbClear()
    }

    //Screen orientation
    readonly property bool inPortrait: main.width < main.height

    function reset() {
        setupModel();

        frontViewSlider.value = model_discretization;
        mainSplitView.width = main.width;
        mainSplitView.height = main.height - overlayHeader.height;
        splitTop.height = 0.50  * mainSplitView.height;
        threeDItemParent.width = 0.50  * splitBottom.width;
    }

    function setupModel() {
        model3DHandler.setModelDiscretization(main.model_discretization);
        canvasHandler.crossSection = main.model_discretization;
    }

    Component.onCompleted: {
        settingsDialog.open()
    }

    //- File to manage popups and dialogs
    OverlayHandler {
        id: overlay
    }

    //- Open dialog to set the box dimensions
    OpenDialog2 {
        id: settingsDialog
    }

    //- Top ToolBar with the geological operation rules (and a settings button)
    TopToolBar {
        id: overlayHeader
    }

    //- Right side drawer with the system actions (load, save, new)
    NavigationDrawer {
        id: fileDrawer
    }

    //- Left side drawer with the object tree
    ObjectTree {
        id: drawer
        positionY: overlayHeader.height
        myWidth: main.width / 4.0
        myHeight: main.height - overlayHeader.height
    }

    //- Layout for the canvas and 3d window
    SplitView {
        id: mainSplitView

        property real xPosition: 0

        width: parent.width
        height: parent.height - overlayHeader.height
        
        x: xPosition
        y: 0
        
        orientation: Qt.Vertical

        function updateViewArea( viewID, max ){
            if(max)
            {
                splitTop.saveCurrentSize();
                splitBottom.saveCurrentSize();
                threeDItemParent.saveCurrentSize();
                rectMapView.saveCurrentSize();
            }

            switch(viewID)
            {
            case 1: //FrontView
                if(max)
                {
                    splitTop.maximizeFrontView();
                    splitBottom.hideSplitBottom();
                    threeDItemParent.hide3DView();
                    rectMapView.hideMapView();

                } else {
                    splitTop.showFrontView();
                    splitBottom.showSplitBottom();
                    threeDItemParent.show3DView();
                    rectMapView.showMapView();
                }
                break;

            case 2: //3D View
                if(max)
                {
                    splitTop.hideFrontView();
                    splitBottom.maximizeSplitBottom();
                    threeDItemParent.maximize3DView();
                    rectMapView.hideMapView();

                } else {
                    splitTop.showFrontView();
                    splitBottom.showSplitBottom();
                    threeDItemParent.show3DView();
                    rectMapView.showMapView();
                }
                break;

            case 3: //MapView
                if(max)
                {
                    splitTop.hideFrontView();
                    splitBottom.maximizeSplitBottom();
                    threeDItemParent.hide3DView();
                    rectMapView.maximizeMapView();

                } else {
                    splitTop.showFrontView();
                    splitBottom.showSplitBottom();
                    threeDItemParent.show3DView();
                    rectMapView.showMapView();
                }
                break;
            }
        }

        //- FrontView Canvas
        Rectangle {
            id: splitTop
            height: 0.50  * parent.height
            implicitHeight: 0.50  * parent.height
            Layout.minimumHeight: 0//0.15 * parent.height
            Layout.maximumHeight: parent.height//0.85 * parent.height
            color: MAIN_WINDOW.BACKGROUND_CANVAS_2D

            property real currentHeight: height

            function saveCurrentSize() {
                splitTop.currentHeight = height;
            }

            function maximizeFrontView() {
                splitTop.height = parent.height;
                splitTop.implicitHeight = parent.height;
                splitTop.visible = true;
            }

            function showFrontView() {
                splitTop.height = splitTop.currentHeight; //0.50 * parent.height;
                splitTop.implicitHeight = splitTop.currentHeight; //0.50 * parent.height;
                splitTop.visible = true;
            }

            function hideFrontView() {
                splitTop.height = 0;
                splitTop.implicitHeight = 0;
                splitTop.visible = false;
            }

            FrontViewDrawer {
                id: frontViewDrawer
                drawerY: overlayHeader.height
                z: 10
            }

            Rectangle {
                anchors.fill: parent
                Loader {
                    id: frontViewLoader
                    anchors.fill: parent
                    source: "FrontView.qml"
                    active: false
                }//end: Loader
            }//end: Rectangle

            Button {
                id: buttonMax1
                width: 40
                height: 40
                display: AbstractButton.IconOnly
                icon.source: max ? "/images/icons/max-2-icon.png" : "/images/icons/min-2-icon.png"
                icon.color: buttonMax1.down ? "gray" : "lightgray"
                icon.width: width
                icon.height: height

                anchors.right: splitTop.right
                anchors.rightMargin: 70//50
                anchors.top: splitTop.top
                anchors.topMargin: 10

                property bool max: true

                background: Rectangle {
                    implicitWidth: 30
                    implicitHeight: 30
                    color: "transparent"
                    border.color: "transparent"
                    border.width: 1
                    radius: 4
                }

                onClicked: {
                    mainSplitView.updateViewArea(1, buttonMax1.max);
                    buttonMax1.max = !buttonMax1.max;
                }
            }

        }//end: Rectangle FrontView


        //- Bottom region for 3D window and TopView Canvas
        Rectangle {
            id: splitBottom
            height: 0.50  * parent.height
            implicitHeight: 0.50  * parent.height
            Layout.minimumHeight: 0//0.15 * parent.height
            Layout.maximumHeight: parent.height//0.85 * parent.height
            width: parent.width

            property real currentHeight: height

            function saveCurrentSize() {
                splitBottom.currentHeight = height;
            }

            function maximizeSplitBottom() {
                splitBottom.height = parent.height;
                splitBottom.implicitHeight = parent.height;
                splitBottom.width = parent.width;
                splitBottom.visible = true;
            }

            function showSplitBottom() {
                splitBottom.height = splitBottom.currentHeight;//0.50 * parent.height;
                splitBottom.implicitHeight = splitBottom.currentHeight;//0.50 * parent.height;
                splitBottom.width = parent.width;
                splitBottom.visible = true;
            }

            function hideSplitBottom() {
                splitBottom.height = 0;
                splitBottom.implicitHeight = 0;
                splitBottom.width = 0;
                splitBottom.visible = false;
            }

            SplitView {
                id: splitViewBottom
                anchors.fill: parent
                orientation: Qt.Horizontal

                //- 3D Window
                Rectangle {
                    id: threeDItemParent
                    color: MAIN_WINDOW.BACKGROUND_CANVAS_3D
                    width: 0.50  * parent.width
                    implicitWidth: 0.50  * parent.width
                    Layout.minimumWidth: 0//0.15 * parent.width
                    Layout.maximumWidth: parent.width//0.85 * parent.width

                    property real currentWidth: width

                    function saveCurrentSize() {
                        threeDItemParent.currentWidth = width;
                    }

                    function maximize3DView() {
                        threeDItemParent.width = parent.width;
                        threeDItemParent.implicitWidth = parent.width;
                        threeDItemParent.visible = true;
                    }

                    function show3DView() {
                        threeDItemParent.width = threeDItemParent.currentWidth; // 0.50 * parent.width;
                        threeDItemParent.implicitWidth = threeDItemParent.currentWidth; // 0.50 * parent.width;
                        threeDItemParent.visible = true;
                    }

                    function hide3DView() {
                        threeDItemParent.width = 0;
                        threeDItemParent.implicitWidth = 0;
                        threeDItemParent.visible = false;
                    }

                    Rectangle {
                        id: screenCanvasUI
                        anchors.fill: parent

                        ModelViewerFboItem {
                            id: "modelViewerFboItem"
                            objectName: "modelViewerFboItem"
                            anchors.fill: parent
                            anchors.margins: 5
                        }//end: ModelViewerFboItem


                        Rectangle{
                            id: frontSliderRectangle
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 50
                            height: 50
                            width: 500
                            color: "transparent"

                            RowLayout {
                                anchors.centerIn: frontSliderRectangle
                                spacing: 2

                                ButtonToolBar {
                                    id: btnPreviousSketch
                                    isBordered: true
                                    hasIcon: true
                                    icon.source: "/images/icons/cs-previous.png"
                                    myToolTipText.text: "Previous Sketch"

                                    onClicked: {
                                        frontViewSlider.value = canvasHandler.getPreviousUsedCrossSection();
                                        newVizHandler.setCrossSection(frontViewSlider.value);
                                    }
                                }

                                Slider {
                                    id: frontViewSlider
                                    visible: true
                                    value: main.model_discretization
                                    stepSize: 1
                                    to: main.model_discretization
                                    from: 0
                                    snapMode: Slider.SnapAlways
                                    orientation: Qt.Horizontal

                                    onValueChanged: {
                                        ////console.log("Slider value: " + value + "Testing push")
                                        newVizHandler.setCrossSection(value);
                                        frontSliderLabel.text = value
                                    }
                                }//end: Slider

                                ButtonToolBar {
                                    id: btnNextSketch
                                    isBordered: true
                                    hasIcon: true
                                    icon.source: "/images/icons/cs-next.png"
                                    myToolTipText.text: "Next Sketch"

                                    onClicked: {
                                        frontViewSlider.value = canvasHandler.getNextUsedCrossSection();
                                        newVizHandler.setCrossSection(frontViewSlider.value);
                                    }
                                }
                            }//end: RowLayout
                        }//end: Rectangle

                        Rectangle{
                            id: frontLabelRectangle
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.top: frontSliderRectangle.bottom
                            height: 30
                            width: 200
                            color: "transparent"

                            RowLayout {
                                anchors.centerIn: frontLabelRectangle

                                Label {
                                    id: currentSliderPositonLabel
                                    text: "Cross section: "
                                    font.pixelSize: 16
                                    font.italic: true
                                }
                                Label {
                                    id: sliderLabelText
                                    text: frontSliderLabel.text
                                    font.pixelSize: 16
                                    font.italic: true
                                }
                                Label {
                                    id: planeOrientationLabel
                                    text: canvasHandler.direction===2 ? " (WE)" : (canvasHandler.direction===3? " (MAP)" : " (NS)")
                                    font.pixelSize: 16
                                    font.italic: true
                                }
                            }
                        }//end: Rectangle

                        Rectangle {
                            id: frontSliderLabelContainer
                            anchors.left: frontSliderRectangle.right
                            //anchors.top: parent.top +50
                            anchors.leftMargin: 2
                            anchors.topMargin: 100
                            height: 200
                            width: 50
                            color: "transparent"

                            Label {
                                id: frontSliderLabel
                                anchors.centerIn: parent
                                parent: frontViewSlider.handle
                                visible: frontViewSlider.pressed
                                font.pixelSize: 28
                                font.italic: true
                                text: main.model_discretization
                            }//end: Label
                        }//end: Rectangle

                    }//end: screenCanvasUI

                    Button {
                        id: buttonMax2
                        width: 40
                        height: 40
                        display: AbstractButton.IconOnly
                        icon.source: max ? "/images/icons/max-2-icon.png" : "/images/icons/min-2-icon.png"
                        icon.color: buttonMax1.down ? "gray" : "lightgray"
                        icon.width: width
                        icon.height: height

                        anchors.right: threeDItemParent.right
                        anchors.rightMargin: 30//20
                        anchors.top: threeDItemParent.top
                        anchors.topMargin: 10

                        property bool max: true

                        background: Rectangle {
                            implicitWidth: 30
                            implicitHeight: 30
                            color: "transparent"
                            border.color: "transparent"
                            border.width: 1
                            radius: 4
                        }

                        onClicked: {
                            mainSplitView.updateViewArea(2, buttonMax2.max);
                            buttonMax2.max = !buttonMax2.max;
                        }
                    }

                }//End: threeDItemParent

                //- TopView Canvas
                Rectangle {
                    id: rectMapView
                    width: 0.50  * parent.width
                    implicitWidth: 0.50  * parent.width
                    Layout.minimumWidth: 0//0.15 * parent.width
                    Layout.maximumWidth: parent.width//0.85 * parent.width

                    property real currentWidth: width

                    function saveCurrentSize() {
                        rectMapView.currentWidth = width;
                    }

                    function maximizeMapView() {
                        rectMapView.width = parent.width;
                        rectMapView.implicitWidth = parent.width;
                        rectMapView.visible = true;
                    }

                    function showMapView() {
                        rectMapView.width = rectMapView.currentWidth; // 0.50 * parent.width;
                        rectMapView.implicitWidth = rectMapView.currentWidth; // 0.50 * parent.width;
                        rectMapView.visible = true;
                    }

                    function hideMapView() {
                        rectMapView.width = 0;
                        rectMapView.implicitWidth = 0;
                        rectMapView.visible = false;
                    }

                    TopViewDrawer {
                        id: topViewDrawer
                        drawerY: splitTop.height + overlayHeader.height
                        drawerContoursY: splitTop.height + overlayHeader.height
                        z: 10
                    }

                    Rectangle {
                        anchors.fill: parent
                        Loader {
                            id: topViewLoader
                            anchors.fill: parent
                            source: "TopView.qml"
                            active: false
                        }//end: Loader

                    }//end: Rectangle

                    Button {
                        id: buttonMax3
                        width: 40
                        height: 40
                        display: AbstractButton.IconOnly
                        icon.source: max ? "/images/icons/max-2-icon.png" : "/images/icons/min-2-icon.png"
                        icon.color: buttonMax3.down ? "gray" : "lightgray"
                        icon.width: width
                        icon.height: height

                        anchors.right: rectMapView.right
                        anchors.rightMargin: 70//50
                        anchors.top: rectMapView.top
                        anchors.topMargin: 10

                        property bool max: true

                        background: Rectangle {
                            implicitWidth: 30
                            implicitHeight: 30
                            color: "transparent"
                            border.color: "transparent"
                            border.width: 1
                            radius: 4
                        }

                        onClicked: {
                            mainSplitView.updateViewArea(3, buttonMax3.max);
                            buttonMax3.max = !buttonMax3.max;
                        }
                    }

                }//end: Rectangle TopView

            }//end: SplitView

        }//end: Rectangle

        Connections {
            target: newVizHandler//vizHandler

            onResizeMainWindow: {
                ////console.log("Drawer closed/opened:" + shrink)
                if(shrink)
                {
                    mainSplitView.xPosition = 1.0 * mainSplitView.width * 0.20
                    mainSplitView.width = main.width - mainSplitView.width * 0.20
                }
                else {
                    mainSplitView.xPosition = 0
                    mainSplitView.width = main.width
                }
            }

            onPlaneOrientationChanged: {
                planeOrientationLabel.text = planeOrientation==2 ? "W-E" : (planeOrientation==3? "MAP" : "N-S")
            }

            onOpenDimensionDialog: {
                settingsDialog.open()
            }

            onCrossSectionChanged: {
                ////console.log("crossSectionChanged to " + crossSectionNumber)
                frontViewSlider.value = crossSectionNumber

            }

            onImageDataLoaded:{
                ////console.log("Loaded Image data: " + jsonData)
                if(jsonData)
                    ImageScript.fillImageData(jsonData)
                var imageJson = ImageScript.readAllRows();
                canvasHandler.loadCrossSectionUsed(imageJson);
            }
        }

        Connections {
            target: canvasHandler

            onResetNew: {
                main.reset();
            }
        }

        

        transform: Translate {
            //x: xPosition
            y: overlayHeader.height
        }

    }//end: SplitView

    FloatingButton {
        id: floatingButton
        maxHeight: parent.height - screenDpi.getDiP(70)
        maxWidth: parent.width - screenDpi.getDiP(70)
    }//end: FloatingButton


}//end: ApplicationWindow



