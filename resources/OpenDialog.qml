
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

//Dialog box allows the user to set Bounding box dimensions

Dialog {
    id: myDialog
    modality: Qt.ApplicationModal//Qt.WindowModal
    title: "RRM: Settings "

    function dialogInputAction() {
        model3DHandler.setSize(canvasWidthTextField.text, canvasLengthTextField.text, canvasHeightTextField.text);
        model3DHandler.setOrigin(0, 0, 0);

        frontViewLoader.sourceComponent = null;
        frontViewLoader.source = "FrontView.qml"
        frontViewLoader.active = true;

        topViewLoader.sourceComponent = null;
        topViewLoader.source = "TopView.qml"
        topViewLoader.active = true;

        newVizHandler.changeDimension();
        newVizHandler.setCrossSection(64);
    }

    onButtonClicked: {
        dialogInputAction()
    }

    onAccepted:{
        dialogInputAction()
    }

    onRejected: {
        dialogInputAction()
    }


    //////////////////Settings ////////////////////////
    ColumnLayout {
        id: column
        width: parent ? parent.width : screenDpi.getDiP(200)
        anchors.fill: parent

        Label {
            text: "<b>Please</b> set the dimensions:"
            Layout.columnSpan: 2
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            font.weight: Font.ExtraBold
            font.pixelSize: screenDpi.getDiP(16)
        }

        RowLayout {
            Layout.alignment: Qt.AlignRight //Qt.AlignHCenter

            Label {
                text: "Width (X; West->East):"
                Layout.alignment: Qt.AlignBaseline | Qt.AlignLeft
                font.weight: Font.ExtraBold
                font.pixelSize: screenDpi.getDiP(14)
            }

            Slider {
                id: sliderWidth
                value: 500.0
                stepSize: 10.0
                from: 10.0//minimumValue: 10.0 //from and to works for version2 of the QtQuickControls
                to: 10000.0//maximumValue: 10000.0
                width: 500
            }

            TextField {
                id: canvasWidthTextField
                text: sliderWidth.value
                font.weight: Font.ExtraBold
                font.pixelSize: screenDpi.getDiP(14)
                onTextChanged: sliderWidth.value = canvasWidthTextField.text
                validator: IntValidator {bottom: sliderWidth.minimumValue; top: sliderWidth.maximumValue;}
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignRight //Qt.AlignHCenter

            Label {
                text: "Length (Y; South->North):"
                Layout.alignment: Qt.AlignBaseline | Qt.AlignLeft
                font.weight: Font.ExtraBold
                font.pixelSize: screenDpi.getDiP(14)
            }

            Slider {
                id: sliderLength
                value: 500.0
                stepSize: 10.0
                from: 10.0//minimumValue: 10.0
                to: 10000.0//maximumValue: 10000.0
                width: 500
            }

            TextField {
                id: canvasLengthTextField
                text: sliderLength.value
                font.weight: Font.ExtraBold
                font.pixelSize: screenDpi.getDiP(14)
                onTextChanged: sliderLength.value = canvasLengthTextField.text
                validator: IntValidator {bottom: sliderLength.minimumValue; top: sliderLength.maximumValue;}
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignRight //Qt.AlignHCenter

            Label {
                text: "Height (Z):"
                Layout.alignment: Qt.AlignBaseline | Qt.AlignLeft
                font.weight: Font.ExtraBold
                font.pixelSize: screenDpi.getDiP(14)
            }

            Slider {
                id: sliderHeight
                value: 500.0
                stepSize: 10.0
                from: 10.0//minimumValue: 10.0
                to: 10000.0//maximumValue: 10000.0
                width: 500
            }

            TextField {
                id: canvasHeightTextField
                text: sliderHeight.value
                font.weight: Font.ExtraBold
                font.pixelSize: screenDpi.getDiP(14)
                onTextChanged: sliderHeight.value = canvasHeightTextField.text
                validator: IntValidator {bottom: sliderHeight.minimumValue; top: sliderHeight.maximumValue;}
            }

        }//End:RowLayout

    }//End:ColumnLayout
    ///////////////////////////////////////////////////


}//End: Dialog
