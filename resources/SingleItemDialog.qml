import QtQuick 2.12
import QtQuick.Controls 2.1
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12

import Constants 1.0

Dialog {
    id: root
    modal: true
    width: screenDpi.getDiP(800)
    height: screenDpi.getDiP(400)
    x: 0
    y: parent.height/2 - height/2
    z: 100

    property var treeItem
    property alias selectedTreeItem: root.treeItem

    onOpened: {
        footer.reset();
    }

    Timer {
        id: timer1

        function setTimeout(cb, delayTime) {
            timer1.interval = delayTime;
            timer1.repeat = false;
            timer1.triggered.connect(cb);
            timer1.triggered.connect(function release () {
                timer1.triggered.disconnect(cb); // This is important
                timer1.triggered.disconnect(release); // This is important as well
            });
            timer1.start();
        }
    }

    //title: "Object details"
    header: Rectangle {
        height: screenDpi.getDiP(0.25 * parent.height)
        width: screenDpi.getDiP(parent.width)
        color: "#EEEEEE"
        Text {
            text: qsTr(newVizHandler.getItemType(root.treeItem.parentTypeId, root.treeItem.dataId) ) + qsTr(" PROPERTIES:")
            font.pixelSize: screenDpi.getDiP(20)
            font.bold: false
            font.italic: true
            font.capitalization: Font.Capitalize
            anchors {
                left: parent.left
                leftMargin: screenDpi.getDiP(20)
                verticalCenter: parent.verticalCenter
            }
        }
    }

    contentItem: NotesForm {
        id: singleItemForm
        property var data

        itemNameText: qsTr(newVizHandler.getItemName(root.treeItem.parentTypeId, root.treeItem.dataId, root.treeItem.parentId) )
        itemDescription: qsTr(newVizHandler.getItemDescription(root.treeItem.parentTypeId, root.treeItem.dataId) )
        itemColor: qsTr(newVizHandler.getItemColor(root.treeItem.parentTypeId, root.treeItem.dataId) )
        parent_type_id: root.treeItem.parentTypeId

        onInfoChanged: {
            footer.enableSave();
        }
    }

    footer: Rectangle {
        id: footer
        height: screenDpi.getDiP(0.25 * parent.height)
        width: screenDpi.getDiP(parent.width)
        color: "#EEEEEE"

        function reset() {
            btnSave.enabled = false;
        }

        function disableSave() {
            btnSave.enabled = false;
        }

        function enableSave() {
            btnSave.enabled = true;
        }

        RowLayout {
            anchors.centerIn: parent
            spacing: screenDpi.getDiP(20)

            ButtonDialog {
                text: qsTr("Remove")
                backgroundDefaultColor: OBJECT_TREE.REMOVE_BUTTON_COLOR
                visible: (root.treeItem.parentTypeId ===4)|| (root.treeItem.parentTypeId ===5)? true: false

                Layout.fillHeight: false
                Layout.alignment: Qt.AlignLeft

                onClicked: {
                    //console.log("Dialog delete button clicked !" + JSON.stringify(root.treeItem))
                    newVizHandler.removeSingleObject(root.treeItem.parentTypeId, root.treeItem.dataId, root.treeItem.parentId)
                    root.close()
                }
            }

            ButtonDialog {
                text: qsTr("Unmerge")
                backgroundDefaultColor: OBJECT_TREE.REMOVE_BUTTON_COLOR
                visible: (root.treeItem.parentTypeId ===6)? true: false

                Layout.fillHeight: false
                Layout.alignment: Qt.AlignLeft

                onClicked: {
                    //console.log("Dialog delete button clicked !" + JSON.stringify(root.treeItem))
                    newVizHandler.unmergeSurfaceFromGroup(root.treeItem.parentTypeId, root.treeItem.dataId, root.treeItem.parentId)
                    root.close()
                }
            }

            ButtonDialog {
                id: btnSave
                text: qsTr("Save")
                backgroundDefaultColor: enabled? "#8BC34A" : "#aaaaaa"
                visible: true //(root.treeItem.parentTypeId !=5)? true: false
                enabled: false
                Layout.fillHeight: false
                Layout.alignment: Qt.AlignCenter

                onClicked: {
                    //console.log("SingleItemDialog save button clicked: " + singleItemForm.itemNameText + ", " +singleItemForm.itemDescription)
                    if(root.treeItem.parentTypeId===TreeNodeParentType.STRATIGRAPHIC_CHILD)
                        newVizHandler.saveChildSurfaceData(root.treeItem.parentId,root.treeItem.parentTypeId, root.treeItem.dataId, singleItemForm.itemNameText, singleItemForm.itemDescription)
                    else newVizHandler.saveItemFormData(root.treeItem.parentTypeId, root.treeItem.dataId, singleItemForm.itemNameText, singleItemForm.itemDescription)

                    timer1.setTimeout(function(){
                        root.close()
                    }, 100);
                }
            }

            ButtonDialog {
                text: qsTr("Close")
                backgroundDefaultColor: OBJECT_TREE.CLOSE_BUTTON_COLOR

                Layout.fillHeight: false
                Layout.alignment: Qt.AlignRight

                onClicked: {
                    root.close()
                }
            }
        }
    }
}

