import QtQuick 2.0

import Constants 1.0

Item {
    id: root

    property int m_origin_x
    property int m_origin_y

    property double m_width
    property double m_height

    property double m_scale

    CrossSection {
        id:mapViewCrossSection
        myOrigin_X: m_origin_x
        myOrigin_Y: m_origin_y
        myWidth:  m_width
        myHeight: m_height
        direction: DIRECTION.HEIGHT
    }


    //- Black rectangle that representes the scale on the bottom
    Rectangle {
        id: myScaleBottom
        width: 200 * m_scale
        height: 10
        x: m_origin_x
        y: m_origin_y + m_height + 20
        color: "black"

        Text {
            id: myScaleRightLabel
            text: "200 m"
            color: "black"
            anchors.left: myScaleBottom.right
            anchors.bottom: myScaleBottom.bottom
            anchors.leftMargin: 10
        }
    }
} //END Item
