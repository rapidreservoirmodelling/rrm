import QtQuick 2.0

import Constants 1.0

Item {
    id: root

    property int m_origin_x
    property int m_origin_y

    property double m_width
    property double m_height

    property double m_scale

    property bool isMap

    property bool backImgVisible: true

    CrossSection {
        id: myCrossSection
        myOrigin_X: m_origin_x
        myOrigin_Y: m_origin_y
        myWidth: m_width
        myHeight: m_height
        myLeftDirectionLabel: "N"
        myRightDirectionLabel: "S"
        isDisable: root.isMap
        direction: DIRECTION.WIDTH
        imgVisibility: backImgVisible
    }


    //- Black rectangle that representes the scale on the bottom
    Rectangle {
        id: myScaleBottom
        width: 200 * m_scale
        height: 10
        x: m_origin_x
        y: m_origin_y + m_height + 20
        color: "black"
        opacity: root.isMap ? 0.1 : 1.0

        Text {
            id: myScaleRightLabel
            text: "200 m"
            color: "black"
            anchors.left: myScaleBottom.right
            anchors.bottom: myScaleBottom.bottom
            anchors.leftMargin: 10
        }
    }


    //- Black rectangle that representes the scale on the left
    Rectangle {
        id: myScaleLeft
        width: 200 * m_scale
        height: 10
        x: m_origin_x - ((width + myScaleLeftLabel.width) / 2) + 5
        y: m_origin_y + m_height + 20 - ((width + myScaleLeftLabel.width) / 2)
        color: "black"
        opacity: root.isMap ? 0.1 : 1.0
        rotation: -90

        Text {
            id: myScaleLeftLabel
            text: "200 m"
            color: "black"
            anchors.left: myScaleLeft.right
            anchors.bottom: myScaleLeft.bottom
            anchors.leftMargin: 10
        }
    }
} //END Item
