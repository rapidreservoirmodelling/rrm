import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

import Constants 1.0

RowLayout {
    id: root

    function reset() {
        btnUndo.enabled = false;
        btnRedo.enabled = false;
        newVizHandler.setPreviewFlag();
        //receiver.setPreview(newVizHandler.getPreviewFlag());
    }

    Component.onCompleted: {
        if(!btnPreview.isActive)
            btnPreview.isActive = true
    }

    //Timer for the undo button
    Timer {
        id: timer3

        function setTimeout(cb, delayTime) {
            timer3.interval = delayTime;
            timer3.repeat = false;
            timer3.triggered.connect(cb);
            timer3.triggered.connect(function release () {
                timer3.triggered.disconnect(cb);
                timer3.triggered.disconnect(release);
            });
            timer3.start();
        }
    }//end: Timer3

    ButtonToolBar {
        id: btnUndo
        isBordered: true
        hasIcon: true
        icon.source: "/images/icons/surface-undo.png"
        myToolTipText.text: "Undo"
        enabled: false

        onClicked: {
            //btnUndo.enabled = false;
            if(!newVizHandler.getVizFlag()) {
                btnUndo.enabled = false;
                if(model3DHandler.undo()) {
                    //if(newVizHandler.undoLastObject()) {
                    //console.log("Undo successful!")
                }
                else {
                    //console.log("Undo error")
                }
            }
            
        }
    }

    ButtonToolBar {
        id: btnRedo
        isBordered: true
        hasIcon: true
        icon.source: "/images/icons/surface-redo.png"
        myToolTipText.text: "Redo"
        enabled: false

        onClicked: {
            //btnRedo.enabled = false;
            if(!newVizHandler.getVizFlag()) {
                btnRedo.enabled = false;
                if(model3DHandler.redo()) {
                    //if(newVizHandler.redoLastObject()) {
                    //console.log("Redo successful!")
                }
                else {
                    //console.log("Redo error")
                }
            }
        }
    }

    ButtonToolBar {
        id: btnPreview
        isBordered: true
        hasIcon: true
        icon.source: "/images/icons/surface-preview.png"
        myToolTipText.text: "Show Preview"

        isActive: false //Preview button activated by default

        onClicked: {
            newVizHandler.togglePreviewButtonStaus()
            if(!newVizHandler.getPreviewFlag())
            {
                newVizHandler.setPreviewFlag()
            }
            else
            {
                newVizHandler.resetPreviewFlag()
            }
            receiver.setPreview(newVizHandler.getPreviewFlag());
        }
    }

    Connections {
        target: canvasHandler

        onResetNew: {
            root.reset();
        }
    }

    Connections {
        target: newVizHandler

        onPreviewTurnedOn : {
            btnPreview.isActive = true

        }

        onPreviewTurnedOff : {
            btnPreview.isActive = false
        }

        onEnableUndoRedoButtons: {
            timer3.setTimeout(function() {
                btnRedo.enabled = true
                btnUndo.enabled = true
            }, 300);
        }

        onModelLoaded: {
            //console.log("Model loaded!")
            //root.reset()
        }
    }
}

