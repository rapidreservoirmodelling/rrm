import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import Constants 1.0

CheckDelegate {
    id: checkBox

    visible: true

    checked: checkBox.getCheckedStatus()//true

    function getCheckedStatus()
    {
        var checkedStatus = true;

        switch(styleData.value.dataId)
        {
            case 1: 
                checkedStatus = newVizHandler.stratigraphic_visibility
                break;
            case 2: 
                checkedStatus = newVizHandler.structural_visibility
                break;
            case 3: 
                checkedStatus = newVizHandler.region_visibility
                break;
            case 4: 
                checkedStatus = newVizHandler.domain_visibility
                break;
        }
        
        return checkedStatus;
    }
	indicator: Rectangle {
        implicitWidth: screenDpi.getDiP(OBJECT_TREE.CHECKBOX_WIDTH)
        implicitHeight: screenDpi.getDiP(OBJECT_TREE.CHECKBOX_HEIGHT)

        //x: checkBox.width - width - checkBox.rightPadding
        //y: checkBox.topPadding + checkBox.availableHeight / 2 - height / 2
        radius: 3
        
        color: "transparent"
        border.color: checkBox.down ? OBJECT_TREE.CHECKBOX_INDICATOR_ACTIVE_COLOR : OBJECT_TREE.CHECKBOX_INDICATOR_INACTIVE_COLOR 

        Rectangle {
            width: screenDpi.getDiP(14)
            height: screenDpi.getDiP(14)
            x: screenDpi.getDiP(6)
            y: screenDpi.getDiP(6)
            radius: 2
            color: checkBox.down ? OBJECT_TREE.CHECKBOX_ICON_DOWN_COLOR : OBJECT_TREE.CHECKBOX_ICON_UP_COLOR 
            visible: checkBox.checked
        }
	}

	background: Rectangle {
			implicitWidth: screenDpi.getDiP(OBJECT_TREE.CHECKBOX_BACKGROUND_WIDTH)
			implicitHeight: screenDpi.getDiP(OBJECT_TREE.CHECKBOX_BACKGROUND_HEIGHT)

			visible: checkBox.down || checkBox.highlighted
			color: checkBox.down ? OBJECT_TREE.CHECKBOX_BACKGROUND_ACTIVE_COLOR : OBJECT_TREE.CHECKBOX_BACKGROUND_INACTIVE_COLOR 
	}
		
	onClicked: {
        //console.log("Parent checkbox clicked! dataId:" + JSON.stringify(styleData.value))
        newVizHandler.setItemVisibilityAll(styleData.value.dataId, checkState)
	}

     
}


