import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Rectangle {
    Material.theme: Material.Light
    Material.accent: Material.DeepOrange
    width: 980
    height: 800
    property alias title: label.text

    Label {
        id: label
        x: 28
        y: 13
        text: qsTr("")
        property string property0: "label.text"
    }
}
