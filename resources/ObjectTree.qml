import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 1.4 //For TableViewColumn
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Material 2.4
import QtQuick.LocalStorage 2.12

import "scripts/treeview.js" as TreeScript
import Constants 1.0

Item {
    id: root
    property alias positionX: myObjectTree.x
    property alias positionY: myObjectTree.y

    property alias myWidth: myObjectTree.width
    property alias myHeight: myObjectTree.height

    function reset() {
        myObjectTree.close();
        newVizHandler.updateMainWindowSize(false, 0)
    }

    Drawer {
        id: myObjectTree
        closePolicy: Popup.NoAutoClose
        dragMargin: screenDpi.getDiP(100)

        modal: false
        //interactive: inPortrait
        //position: inPortrait ? 0 : 1
        //visible: !inPortrait

        onOpened: {
            if(COMMENTS.DEV_FAZILA) {
                //console.log("Opened the drawer")
            }
            newVizHandler.updateMainWindowSize(true, myObjectTree.x)
            
        }
        onClosed: {
            if(COMMENTS.DEV_FAZILA) {
                //console.log("Closed the drawer")
            }
            newVizHandler.updateMainWindowSize(false, 0)
        }
        Rectangle {
            id: myTreeRoot
            anchors.fill: parent

            border.color: "blue"

            //- Not being used
//            MessageDialog {
//              id: messageDialog
//              title: "Info"
//            }

            function expandTreeNodes(index, view)
            {
                var childList = theModel.getChildrenIndices(index)
                
                for(var i=0; i < childList.length; i++)
                {
                    var element = theModel.getNodeDataByIndex(index);   
                    var treeNodeData = TreeScript.getExpandStatus(element.indentation, element.dataId)
                    //console.log("In expandTree: " + JSON.stringify(treeNodeData) )
                    if(treeNodeData)
                    {
                        if(treeNodeData.expanded == 1)
                        {
                            expandTreeNodes(childList[i], view);

                            if(!view.isExpanded(index)) 
                            {
                                view.expand(index)
                            }
                        }
                    }
                    else {
                            expandTreeNodes(childList[i], view);

                            if(!view.isExpanded(index)) 
                            {
                                view.expand(index)
                            }
                    } 


                }
            }
            
            ColumnLayout {
                anchors.fill: parent
                spacing: 2

                RowLayout {
                    id: buttons
                    //anchors.fill: parent
                    //anchors.top: iconHolder.bottom
                    spacing: screenDpi.getDiP(6)
                    Layout.preferredHeight: 0.1 * parent.height
                    Layout.preferredWidth: parent.width
                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                    Layout.margins: screenDpi.getDiP(20)

                    Rectangle {
                        property real scalefactor: 0.02
                        Layout.fillWidth: true
                        //color: 'teal'
                        Layout.minimumWidth: screenDpi.getDiP(9200 * scalefactor)
                        Layout.preferredWidth: screenDpi.getDiP(9200 * scalefactor)
                        Layout.maximumWidth: screenDpi.getDiP(9200 * scalefactor)
                        Layout.minimumHeight: screenDpi.getDiP(3900 * scalefactor)
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
                        Layout.margins: screenDpi.getDiP(10)

                        Image {
                            id: iconHolder
                            fillMode: Image.PreserveAspectFit
                            anchors.centerIn: parent
                            height: parent.height
                            //smooth: true
                            //antialiasing: true
                            source: "/images/icons/rrm.png"
                        }
                    }

                    Rectangle {
                        property real scalefactor: 0.02
                        Layout.fillWidth: true
                        //color: 'teal'
                        Layout.minimumWidth: screenDpi.getDiP(9200 * scalefactor)
                        Layout.preferredWidth: screenDpi.getDiP(9200 * scalefactor)
                        Layout.maximumWidth: screenDpi.getDiP(9200 * scalefactor)
                        Layout.minimumHeight: screenDpi.getDiP(3900 * scalefactor)
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
                        Layout.margins: screenDpi.getDiP(10)

                        /*Image {
                            id: hammerHolder
                            fillMode: Image.PreserveAspectFit
                            anchors.centerIn: parent
                            height: parent.height
                            //smooth: true
                            //antialiasing: true
                            source: "/images/icons/flattening.png"
                            visible: false
                        }*/
                    }
                    Rectangle {
                        property real scaleFactorR: 1.3
                        //color: 'teal'
                        Layout.fillWidth: true
                        Layout.minimumWidth: screenDpi.getDiP(30 * scaleFactorR)
                        Layout.preferredWidth: screenDpi.getDiP(50 * scaleFactorR)
                        Layout.maximumWidth: screenDpi.getDiP(55 * scaleFactorR)
                        Layout.minimumHeight: screenDpi.getDiP(50 * scaleFactorR)
                        
                        RegionVizButton {
                            //anchors.centerIn: parent
                            
                        }
                    }
                    Rectangle {
                        property real scaleFactorF: 1.3
                        //color: 'plum'
                        Layout.fillWidth: true
                        Layout.minimumWidth: screenDpi.getDiP(30 * scaleFactorF)
                        Layout.preferredWidth: screenDpi.getDiP(50 * scaleFactorF)
                        Layout.preferredHeight: screenDpi.getDiP(50 * scaleFactorF)
                        Layout.maximumWidth: screenDpi.getDiP(55 * scaleFactorF)
                        Layout.minimumHeight: screenDpi.getDiP(50 * scaleFactorF)
                        
                        FdButton {
                            //anchors.centerIn: parent                            
                        }
                    }                    
                }

                TreeView {
                    id: rrmTree
                    //anchors.top: vizButtonHolder.bottom
                    //anchors.bottom: parent.bottom
                    Layout.preferredWidth: parent.width
                    Layout.preferredHeight: 0.9 * parent.height

                    model: theModel

                    Layout.fillWidth: true

                    property variant surfaceList: [0, 1,]

                    Component.onCompleted: {
                        TreeScript.dbClear()
                        TreeScript.dbInit()
                                               
                    }
                    Component.onDestruction:{
                        TreeScript.dbClear()
                    }

                    
                    selectionMode: SelectionMode.NoSelection  

                    rowDelegate: Rectangle {
                        id: rowSelectionRectangle
                        height: screenDpi.getDiP(OBJECT_TREE.TREE_ROW_HEIGHT)
                        color: styleData.selected ? OBJECT_TREE.ROW_SELECTION_COLOR : "transparent"
                        
                    }

                    TableViewColumn {
                        id: firstColumn
                        role: "title"
                        title: "Object Name"
                        width: screenDpi.getDiP(170)

                        delegate: ObjectNameDelegate {} //TextDelegate {}
                    }

                    TableViewColumn {
                        role: "visible"
                        title: "Visible"
                        width: screenDpi.getDiP(60)

                        delegate: VisibilityColumnDelegate {
                            //treeModel: theModel
                        }
                    }

                    TableViewColumn {
                        role: "color"
                        title: "Color"
                        width: screenDpi.getDiP(60)

                        delegate: ColorColumnDelegate {}
                    }

                    TableViewColumn {
                        role: "volume"
                        title: "Volume (Proportion)"
                        width: screenDpi.getDiP(200)

                        delegate: TextDelegate {}
                    }

                    
                    onPressAndHold: {
                        var element = theModel.getNodeDataByIndex(index);
                        
                        //console.log("Object tree item PressAndHold:" + JSON.stringify(element)  )
                    }

                    onClicked: {
                        var element = theModel.getNodeDataByIndex(index);
                        
                        //console.log("Object tree item clicked:" + JSON.stringify(element)  )

                        if(newVizHandler.flat_mode)
                            newVizHandler.setFlattenedSurfaceId(element.dataId)
                        
                    }

                    onCollapsed: {
                        var element = theModel.getNodeDataByIndex(index);
                        //console.log("Object tree item collapsed:" + JSON.stringify(element)  )
                        TreeScript.addNodeData(element.indentation, element.dataId, 0)
                        
                    }

                    onExpanded: {
                        var element = theModel.getNodeDataByIndex(index);
                        //console.log("Object tree item expanded:" + JSON.stringify(element)  )
                        TreeScript.addNodeData(element.indentation, element.dataId, 1)
                    }

                }//end:TreeView
            }//endColumnLayout
        }//end: Rectangle
    }//End: Drawer
    Connections {
        target: canvasHandler

        onUpdateSurfaceList: {
            if(COMMENTS.DEV_FAZILA) {
                console.log("Received in ObjectTree: " + JSON.stringify(surfaceList))
            }
            theModel.update(surfaceList)
        }

        onResetNew: {
            root.reset();
        }

        onFlatteningStarted:{
            //console.log("ObjectTree flattening started: surface id=" + surface_id_to_flat  + ", flat height=" + flat_height)
            newVizHandler.startFlattening(surface_id_to_flat, flat_height)
            //newVizHandler.updateSurfaceList()
        }

        onFlatteningStopped:{
            //console.log("ObjectTree flattening stopped")
        }
    }//end: canvasHandler Connection

    Connections {
        target: newVizHandler

        onNewDomainColorReady: {
            if(COMMENTS.DEV_FAZILA) {
                //console.log("Object tree:  domain color updated: " + colorMap)
            }
            theModel.updateDomainNodes(colorMap)
         }

         onClearRegionCurveBox: {
            theModel.removeRegions();
         }

         onFlatModeChanged: {
            //console.log("Object Tree: flat mode = " + flag)
            //hammerHolder.visible = flag
         }

         onTreeOpenFlagChanged: {
            if( newVizHandler.tree_open)
                myObjectTree.open()
            else myObjectTree.close()
         }
         onDropToDomainReady: {
            newVizHandler.dropToDomain()
         }

    }//end: vizHandler Connection

    Connections {
        target: theModel
                
        onExpandTreeNodes: {
            //Expand All
            for(var i=0; i < theModel.rowCount(); i++) {
                var index = theModel.index(i,0)
                myTreeRoot.expandTreeNodes(index, rrmTree)
            }
        }

        onDomainNameListUpdated: {
            theModel.reloadDomains()
        }

        onDomainNodesUpdated: {
            theModel.reloadDomains()
            rrmTree.positionViewAtRow(theModel.rowCount()-1, ListView.Contain)
        }

        onRegionNodesUpdated: {
            theModel.reloadRegions()
            //theModel.reloadDomains()
        }

        onSurfaceNodesItemsUpdated: {
            theModel.reloadSurfaces()
        }

        onDataChanged:{
            console.log("Treeview data changed!")
        }
    }//end: theModel Connection


}//end: Item
