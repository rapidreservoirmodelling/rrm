import QtQuick 2.12
import QtQuick.Controls 2.12

import Constants 1.0

//CheckBox{
CheckDelegate {
    id: cbox
    anchors.verticalCenter: parent.verticalCenter
    anchors.horizontalCenter: parent.horizontalCenter
    
    visible: (styleData.value.parentTypeId === TreeNodeParentType.STRATIGRAPHIC_CHILD) || (styleData.value.text==="") ? false : true //(styleData.value.text!="") ? true : false
    checked: (styleData.value.text==="true") ? true : false
    
    indicator: Rectangle {
        implicitWidth: screenDpi.getDiP(OBJECT_TREE.CHECKBOX_WIDTH)
        implicitHeight: screenDpi.getDiP(OBJECT_TREE.CHECKBOX_HEIGHT)

        x: cbox.width - width - cbox.rightPadding
        y: cbox.topPadding + cbox.availableHeight / 2 - height / 2
        radius: 3
        
        color: "transparent"
        border.color: cbox.down ? OBJECT_TREE.CHECKBOX_INDICATOR_ACTIVE_COLOR : OBJECT_TREE.CHECKBOX_INDICATOR_INACTIVE_COLOR 

        Rectangle {
            width: screenDpi.getDiP(14)
            height: screenDpi.getDiP(14)
            x: screenDpi.getDiP(6)
            y: screenDpi.getDiP(6)
            radius: 2
            color: cbox.down ? OBJECT_TREE.CHECKBOX_ICON_DOWN_COLOR : OBJECT_TREE.CHECKBOX_ICON_UP_COLOR 
            visible: cbox.checked
        }
    }

    background: Rectangle {
        implicitWidth: screenDpi.getDiP(OBJECT_TREE.CHECKBOX_BACKGROUND_WIDTH)
        implicitHeight: screenDpi.getDiP(OBJECT_TREE.CHECKBOX_BACKGROUND_HEIGHT)

        visible: cbox.down || cbox.highlighted
        color: cbox.down ? OBJECT_TREE.CHECKBOX_BACKGROUND_ACTIVE_COLOR : OBJECT_TREE.CHECKBOX_BACKGROUND_INACTIVE_COLOR 
    }

    /*nextCheckState: function() {
                        if (checkState === Qt.Checked)
                            return Qt.Unchecked
                        else
                            return Qt.Checked
    }*/

    onClicked: {
                //theModel.setProperty(styleData.row, "famous", famousDelegate.checked)
                //var val = checkBoxParent.treeModel.data(styleData.row, styleData.column)
                ////console.log("OnClicked Row:" + styleData.row + "-Column:"+styleData.column + "-"+cbox.checked )
                ////console.log("OnClicked data1:"+styleData.value.text )
                ////console.log("OnClicked checkbox data2:"+ JSON.stringify(styleData.value) )
                ////console.log("Bingo1: dataID: "+styleData.value.dataId + ", parent-type-id:"+styleData.value.parentTypeId+ ", visibility flag:"+checkState)
                newVizHandler.changeObjectVisibility(styleData.value.dataId, styleData.value.parentTypeId, checkState)

    }

    onCheckedChanged:{
        ////console.log("OnCheckChanged Row:"+styleData.row + "-"+cbox.checked)
        ////console.log("OnCheckChanged data3:"+ styleData.value.text )
    }
}
