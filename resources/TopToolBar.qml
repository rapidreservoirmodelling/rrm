import QtQuick 2.12
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2

ToolBar {
    id: myTopToolBar
    Material.primary: "#EEEEEE"

    z: screenDpi.getDiP(1)
    width: parent.width
    height: screenDpi.getDiP(72)

    function reset() {
        if( fileDrawer.opened) {
            fileDrawer.close();
        }
    }

    RowLayout {
        spacing: 20
        anchors.fill: parent

        ToolButton {
            Layout.alignment: Qt.AlignHCenter
            Layout.leftMargin: 100

            implicitWidth: screenDpi.getDiP(50)
            implicitHeight: screenDpi.getDiP(50)

            icon.name: "Setup"
            icon.source: "/images/icons/file-menu.png"
            icon.width: implicitWidth
            icon.height: implicitHeight
            flat: true

            onClicked: {
                //console.log("Clicked obj menu button")
                if( newVizHandler.tree_open)
                    newVizHandler.tree_open = false
                else
                    newVizHandler.tree_open = true
            }
        }//end: ToolButton

        RulesToolBar {
            id: rulesToolBar
            Layout.alignment : Qt.AlignHCenter
            spacing: 5
        }

        ToolButton {
            Layout.alignment :  Qt.AlignHCenter
            Layout.rightMargin: 100

            implicitWidth: screenDpi.getDiP(50)
            implicitHeight: screenDpi.getDiP(50)

            icon.name: "Setup"
            icon.source: "/images/icons/file-menu.png"
            icon.width: implicitWidth
            icon.height: implicitHeight
            flat: true

            onClicked: {
                if( fileDrawer.opened)
                    fileDrawer.close()
                else
                    fileDrawer.open()
            }
        }//end: ToolButton

    }//end: RowLayout

    Connections {
        target: canvasHandler

        onResetNew: {
            myTopToolBar.reset();
        }
    }

}//end: ToolBar
