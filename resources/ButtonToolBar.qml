import QtQuick 2.0
import QtQuick.Controls 2.5

import Constants 1.0

Button {
    id: button

    property bool isActive: false
    property bool isBordered: true
    property bool hasBackground: false
    property bool hasIcon: false

    property string colorDown: "#74d0f0"
    property string colorUp: "#707070"
    property string colorBackground: "white" //"#EEEEEE"

    property alias myBackground : rect
    property alias myToolTip : toolTip
    property alias myToolTipText : toolTipText

    implicitWidth: screenDpi.getDiP(BT_TOOLBAR.WIDTH)
    implicitHeight: screenDpi.getDiP(BT_TOOLBAR.HEIGHT)

    text: hasIcon ? "" : "Button"
    display: hasIcon ? AbstractButton.IconOnly : AbstractButton.TextOnly

    icon.color: enabled ? BT_TOOLBAR.ICON_COLOR_UP : BT_TOOLBAR.ICON_COLOR_DISABLE
    icon.source: hasIcon ? "images/button.png" : ""
    icon.width: 80//button.implicitWidth
    icon.height: 80//button.implicitHeight

    background: Rectangle {
        id: rect
        implicitWidth: button.implicitWidth
        implicitHeight: button.implicitHeight
        color: isActive ? BT_TOOLBAR.BACKGROUND_COLOR_DOWN : (hasBackground ? BT_TOOLBAR.BACKGROUND_COLOR_UP : ( button.down ? BT_TOOLBAR.BACKGROUND_COLOR_DOWN : "transparent"))
        border.color: isBordered ? ( enabled ? BT_TOOLBAR.BORDER_COLOR_UP : BT_TOOLBAR.BORDER_COLOR_DISABLE) : "transparent"
        border.width: button.isActive ? screenDpi.getDiP(BT_TOOLBAR.BORDER_WIDTH_ACTIVE) : (button.down ? screenDpi.getDiP(BT_TOOLBAR.BORDER_WIDTH_ACTIVE) : screenDpi.getDiP(BT_TOOLBAR.BORDER_WIDTH))
        radius: screenDpi.getDiP(BT_TOOLBAR.BORDER_RADIUS)
    }

    ToolTip {
        id: toolTip
        visible: button.hovered
        //delay: -1
        //timeout: 1
        contentItem: Text{
            id: toolTipText
            text: "Tool Tip"
            color: "gray"
            font.pixelSize: screenDpi.getDiP(BT_TOOLBAR.TOOLTIP_FONT_PIXEL_SIZE)
        }

        background: Rectangle {
            border.color: "transparent"
        }
    }
}

