import QtQuick 2.0
//import rrm.object 1.0

Item {
    Text {
        //anchors.fill: parent
        anchors.centerIn: parent
        color: styleData.textColor
        elide: styleData.elideMode
        text:  qsTr("%1").arg(styleData.value.text) 
        //text: (styleData.value.parentTypeId === 5) ? "" :  qsTr("%1").arg(styleData.value.text)
    }
}
