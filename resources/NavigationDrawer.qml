import QtQuick 2.12
import QtQuick.Controls 2.12

Drawer {
    id: myNavigationDrawer

    width: parent.width / 10
    height: parent.height - overlayHeader.height

    //dragMargin: 1//100.0
    edge: Qt.RightEdge

    modal: false

    /*Label {
        id: content
        text: "File Menu"
        font.pixelSize: screenDpi.getDiP(30)
        anchors.fill: parent
        verticalAlignment: Label.AlignBottom
        horizontalAlignment: Label.AlignHCenter
    }*/

    FileMenu {}

}//end: Drawer

