import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12


import Constants 1.0

Popup {
    id: popup
    width: screenDpi.getDiP(400)
    height: screenDpi.getDiP(300)
    modal: false
    dim: false
    //focus: true
    closePolicy: Popup.NoAutoClose

    property int m_region_id: -1
    property string m_region_name: ""
    property string m_region_volume: ""
    property string m_region_color: ""
    property int m_domain_id: -1
    property string m_domain_name: ""
    property string m_domain_volume: ""
    property string m_domain_color: ""
    property var m_domain_map: []

    signal regionMenuClosed();

    enter: Transition {
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0; duration: 500 }
    }

    exit: Transition {
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0; duration: 500 }
    }

    onOpened: {
        reloadListModel();
    }

    onClosed: {
        regionMenuClosed();
    }

    function reloadListModel() {
        lmDomains.clear();
        for(var i=0; i<m_domain_map.length; i++) {
            var domainObject = m_domain_map[i];
            lmDomains.append({color: domainObject.domain_color, domainId: domainObject.domain_id, domainName: model3DHandler.getDomainName(domainObject.domain_id)});
        }
    }

    function addDomainToGridview() {
        //- Add last added domain to the list model and update properties
        var domainObject = m_domain_map[m_domain_map.length-1];
        lmDomains.append({color: domainObject.domain_color, domainId: domainObject.domain_id, domainName: model3DHandler.getDomainName(domainObject.domain_id)});
        m_domain_id = domainObject.domain_id;
        m_domain_name = "domain"+m_domain_id.toString();
        var volume = model3DHandler.getDomainVolumeProportion(m_domain_id);
        volume = volume.toFixed(1);
        m_domain_volume = volume.toString();
    }

    function updateDomainProperties() {

        m_domain_id = -1;
        m_domain_name = "";
        m_domain_volume = "";

        //- For each domain
        for (var k=0; k<m_domain_map.length; k++) {
            var domainObject = m_domain_map[k];
            var regionList = domainObject.region_list;
            ////console.log("DOMAIN " + domainObject.domain_id +" regions: " + JSON.stringify(regionList))
            //- For each region in the domain k
            for(var j=0; j<regionList.length; j++) {
                var regionObject = regionList[j];
                var regionId = regionObject.id;
                if(regionId === m_region_id) {
                    m_domain_id = domainObject.domain_id;
                    m_domain_name = model3DHandler.getDomainName(m_domain_id);
                    var volume = model3DHandler.getDomainVolumeProportion(m_domain_id);
                    volume = volume.toFixed(1);
                    m_domain_volume = volume.toString();
                    break;
                }
            }
        }

        ////console.log("UPDATE DOMAIN PROPERTIES: ", m_domain_id + ", " + m_domain_name + ", " + m_domain_volume)
    }

    ListModel { id: lmDomains }

    background: Rectangle {
        id: rectBackground
        anchors.fill: parent
        color: "white"
        border.color: "gray"
        border.width: screenDpi.getDiP(2)
        radius: screenDpi.getDiP(20)

        Item {
            id: dragArea
            width: parent.width
            height: 0.2 * parent.height
            clip: true
            anchors {
                top: parent.top
                left: parent.left
            }
            Rectangle {
                anchors {
                    top: parent.top
                    left: parent.left
                }
                color: "transparent"
                radius: screenDpi.getDiP(20)
            }
            MouseArea {
                anchors.fill: parent
                property point pos
                onPressed: {
                    pos = Qt.point(mouse.x, mouse.y);
                }

                onPositionChanged: {
                    var diff = Qt.point(mouse.x - pos.x, mouse.y - pos.y);
                    popup.x += diff.x;
                    popup.y += diff.y;
                }
            }
        }
    }

    Rectangle {
        id: iconClose
        width: screenDpi.getDiP(15)
        height: screenDpi.getDiP(15)
        color: "transparent"
        radius: screenDpi.getDiP(5)
        anchors {
            top: parent.top
            topMargin: -5
            right: parent.right
            rightMargin: -5
        }

        Label {
            text: "x"
            font.pixelSize: 12
            font.bold: true
            color: "gray"
            anchors.centerIn: parent
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                popup.close();
            }
        }
    }

    Rectangle {
        id: rectContent
        anchors.fill: parent
        color: "transparent"

        Label {
            id: lblRegionName
            anchors {
                top: parent.top
                topMargin: screenDpi.getDiP(5)
                left: parent.left
                leftMargin: screenDpi.getDiP(10)
            }
            text: qsTr("Region:")
            font.pixelSize: screenDpi.getDiP(20)
            font.bold: true
            color: "black"
        }

        Label {
            id: lblRegionNameData
            anchors {
                verticalCenter: lblRegionName.verticalCenter
                left: lblRegionName.right
                leftMargin: screenDpi.getDiP(5)
            }
            text: m_region_name === "" ? qsTr(m_region_id.toString()) : qsTr(m_region_name.toString())
            font.pixelSize: screenDpi.getDiP(20)
            color: "black"
        }

        Label {
            id: lblVolumeData
            anchors {
                verticalCenter: lblRegionName.verticalCenter
                left: lblRegionNameData.right
                leftMargin: screenDpi.getDiP(5)
            }
            text: qsTr("("+m_region_volume+"\%)")
            font.pixelSize: screenDpi.getDiP(18)
            color: "black"
        }

        Label {
            id: lblDomain
            anchors {
                top: lblRegionName.bottom
                topMargin: screenDpi.getDiP(5)
                left: lblRegionName.left
            }
            text: qsTr("Domain:")
            font.pixelSize: screenDpi.getDiP(20)
            font.bold: true
            color: "black"
        }

        Label {
            id: lblDomainData
            anchors {
                verticalCenter: lblDomain.verticalCenter
                left: lblDomain.right
                leftMargin: screenDpi.getDiP(5)
            }
            text: m_domain_id === -1 ? "NOT ASSIGNED" : m_domain_name
            font.pixelSize: screenDpi.getDiP(20)
            color: "black"
        }

        Label {
            id: lblDomainVolume
            anchors {
                verticalCenter: lblDomain.verticalCenter
                left: lblDomainData.right
                leftMargin: screenDpi.getDiP(5)
            }
            visible: m_domain_id === -1 ? false : true
            text: qsTr("("+m_domain_volume+"\%)")
            font.pixelSize: screenDpi.getDiP(18)
            color: "black"
        }

        Button {
            id: btnAddDomain
            width: screenDpi.getDiP(50)
            height: width
            text: "+"
            visible: m_domain_id === -1 ? true : false
            anchors {
                top: parent.top
                topMargin: screenDpi.getDiP(20)
                right: parent.right
                rightMargin: screenDpi.getDiP(10)
            }
            onClicked: {
                newVizHandler.setSourceRegion(m_region_id, 3);
                newVizHandler.createAndsetDestinationDomain();
                if(newVizHandler.domain_visibility) {
                    newVizHandler.changeObjectVisibility(m_domain_id, 4, false);
                    newVizHandler.changeObjectVisibility(m_domain_id, 4, true);
                }
            }
        }

        Button {
            id: btnRemoveDomain
            width: screenDpi.getDiP(50)
            height: width
            text: "-"
            visible: m_domain_id === -1 ? false : true
            anchors {
                top: parent.top
                topMargin: screenDpi.getDiP(20)
                right: parent.right
                rightMargin: screenDpi.getDiP(10)
            }
            onClicked: {
                newVizHandler.removeSingleObject(5, m_region_id, m_domain_id);
            }
        }

        Rectangle {
            id: rectLine
            width: parent.width
            height: 1
            color: "gray"
            anchors {
                top: lblDomain.bottom
                topMargin: screenDpi.getDiP(20)
                horizontalCenter: parent.horizontalCenter
            }
        }

        Rectangle {
            id: rectGrid
            width: parent.width
            height: screenDpi.getDiP(150)
            color: "transparent"
            anchors {
                top: rectLine.bottom
                horizontalCenter: parent.horizontalCenter
                margins: screenDpi.getDiP(20)
            }

            Label {
                id: lblNoDomainCreated
                anchors.centerIn: parent
                visible: lmDomains.count > 0 ? false : true
                text: qsTr("NO DOMAIN CREATED")
                font.pixelSize: screenDpi.getDiP(30)
                color: "gray"
            }

            GridView {
                id: gridview
                anchors.fill: parent
                model: lmDomains
                cellWidth: screenDpi.getDiP(50)
                cellHeight: screenDpi.getDiP(50)
                clip: true
                visible: !lblNoDomainCreated.visible

                delegate: Rectangle {
                    id: itemDelegate
                    width: gridview.cellWidth
                    height: gridview.cellHeight
                    color: "transparent"
                    border.color: m_domain_id === -1 ? "gray" : (model.domainId === m_domain_id ? "black" : "gray")
                    border.width: m_domain_id === -1 ? 1 : (model.domainId === m_domain_id ? 2 : 1)
                    opacity: m_domain_id === -1 ? 1 : (model.domainId === m_domain_id ? 1 : 0.7)

                    Rectangle {
                        id: rectColor
                        anchors.fill: parent
                        anchors.margins: screenDpi.getDiP(5)
                        color: model.color

                        ToolTip.text: model.domainName
                        ToolTip.visible: mouseArea.containsMouse

                        MouseArea {
                            id: mouseArea
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                if(m_domain_id === -1) {
                                    newVizHandler.setSourceRegion(m_region_id, 3);
                                } else {
                                    newVizHandler.setRegionForRemovalFromDomain(m_domain_id, 5, m_region_id);
                                }
                                newVizHandler.setDestinationDomain( model.domainId );
                                if(newVizHandler.domain_visibility) {
                                    newVizHandler.changeObjectVisibility(m_domain_id, 4, false);
                                    newVizHandler.changeObjectVisibility(m_domain_id, 4, true);
                                }
                            }
                        }
                    }
                }
            }
        }

        Connections {
            target: newVizHandler

            onDomainMapUpdated: {
                var domainData = JSON.parse(domainMap);
                ////console.log("\LENGTH: ", domainData.domain_map.length, " > ", m_domain_map.length)
                if(domainData.domain_map.length > m_domain_map.length) {
                    m_domain_map = domainData.domain_map;
                    addDomainToGridview();
                } else {
                    m_domain_map = domainData.domain_map;
                    reloadListModel();
                    updateDomainProperties();
                }
            }

            onDomainListUpdated: {
                reloadListModel();
                updateDomainProperties();
            }

            onVizFlagChanged: {
                if(!flag) {
                    popup.close();
                }
            }

            onResetViz: {
                popup.close();
            }
        }
    }
}
