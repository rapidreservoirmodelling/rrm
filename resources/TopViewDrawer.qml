
import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

import TrajectoryModel 1.0
import Constants 1.0

Item {
    id: root
    anchors.fill: parent

    property alias drawerY: topViewDrawer.y
    property alias drawerContoursY: topViewDrawer_Contours.y

    property bool isSelected: false

    function reset() {
        isSelected = false;
        topViewDrawer_Contours.close();
        topViewDrawer.close();
        btPGE.isActive = false;
        visible = true;
    }

    onVisibleChanged: {
        topViewDrawer_Contours.close();
        topViewDrawer.close();
    }

    //- Button to open the right drawer
    ButtonCanvas {
        id: drawerButtonTop
        x: root.width - (width * 4)
        y: root.height - (height * 1.5)

        hasBackground: true
        hasIcon: true
        icon.source: "/images/icons/canvas-menu.png"
        myToolTip.text: "Menu"

        onClicked: {
            if(canvasHandler.direction === DIRECTION.HEIGHT)
            {
                topViewDrawer.close();
                if( topViewDrawer_Contours.opened)
                    topViewDrawer_Contours.close();
                else
                    topViewDrawer_Contours.open();
            } else {
                topViewDrawer_Contours.close();
                if( topViewDrawer.opened)
                    topViewDrawer.close();
                else
                    topViewDrawer.open();
            }
        }
    }


    //- Button to smooth the sketch
    ButtonCanvas {
        id: btSmooth
        hasBackground: true
        hasIcon: true
        icon.source: "/images/icons/canvas-smooth.png"
        myToolTip.text: "Smooth"

        anchors.right: drawerButtonTop.left //btGuidelines.left
        anchors.top: drawerButtonTop.top //btGuidelines.top
        anchors.rightMargin: 40

        enabled: root.isSelected

        onClicked: {
            receiver.setCanvasVisualizationMV(ACTION.MV_SMOOTH_SKETCH);
        }
    }

    //- Button to clear the sketch
    ButtonCanvas {
        id: btClear
        hasBackground: true
        hasIcon: true
        icon.source: "/images/icons/canvas-clear.png"
        myToolTip.text: "Clear"

        anchors.right: btSmooth.left
        anchors.top: btSmooth.top
        anchors.rightMargin: 10

        enabled: root.isSelected

        onClicked: {
            receiver.setCanvasVisualizationMV(ACTION.MV_CLEAR_SKETCH);
        }
    }


    //- Button to redo the sketch
    ButtonCanvas {
        id: btRedo
        hasBackground: true
        hasIcon: true
        icon.source: "/images/icons/canvas-redo.png"
        myToolTip.text: "Redo"

        anchors.right: btClear.left
        anchors.top: btClear.top
        anchors.rightMargin: 10

        enabled: root.isSelected

        onClicked: {
            receiver.setCanvasVisualizationMV(ACTION.MV_REDO_SKETCH);
        }
    }

    //- Button to undo the sketch
    ButtonCanvas {
        id: btUndo
        hasBackground: true
        hasIcon: true
        icon.source: "/images/icons/canvas-undo.png"
        myToolTip.text: "Undo"

        anchors.right: btRedo.left
        anchors.top: btRedo.top
        anchors.rightMargin: 10

        enabled: root.isSelected

        onClicked: {
            receiver.setCanvasVisualizationMV(ACTION.MV_UNDO_SKETCH);
        }
    }

    //- Button to remove the image from the cross-section
    ButtonCanvas {
        id: btRemoveImage
        hasBackground: true
        hasIcon: true
        icon.source: "/images/icons/remove-image.png"
        myToolTip.text: "Remove image"

        anchors.right: btUndo.left
        anchors.top: btUndo.top
        anchors.rightMargin: 10

        enabled: false

        onClicked: {
            //newVizHandler.removeCrossSectionImage(canvasHandler.direction, canvasHandler.crossSection)
            newVizHandler.removeCrossSectionImage(DIRECTION.HEIGHT, canvasHandler.crossSection)
        }
    }

    //- Button to activate Path Guided Extrusion (PGE)
    ButtonCanvas {
        id: btPGE
        hasBackground: true
        hasIcon: false
        text: isActive ? "PATH" : "LINEAR"
        myToolTip.text: "Path Guided"

        width: 70

        anchors.right:  btRemoveImage.left
        anchors.top: btRemoveImage.top
        anchors.rightMargin: 10

        visible: canvasHandler.direction === DIRECTION.HEIGHT ? false : true
        isActive: false
        myBackground.color: isActive ? BT_CANVAS.BACKGROUND_COLOR_DOWN : (hasBackground ? BT_CANVAS.BACKGROUND_COLOR_UP : ( button.down ? BT_CANVAS.BACKGROUND_COLOR_DOWN : "transparent"))

        onClicked: {
            isActive = !isActive;
            canvasHandler.pgeIsActive = isActive;
        }
    }


    property Component trajectory_templates: TrajectoryTemplatesControlView {}

    property var componentMap: {
        "Trajectory_Templates" : trajectory_templates
    }


    //- Right drawer
    Drawer {
        id: topViewDrawer
        width: 400
        height: root.height - (drawerButtonTop.height * 2)

        edge: Qt.RightEdge
        closePolicy: Popup.NoAutoClose
        modal: false

        ColumnLayout {
            spacing: 10

            Label {
                text: qsTr("Trajectory Templates:")
                font.pixelSize: 28
                Layout.topMargin: 20
                Layout.leftMargin: 20
            }

            Frame {
                id: myFrame
                Layout.fillWidth: true
                Layout.margins: 10

                ListView {
                    id: myListView
                    implicitWidth: topViewDrawer.width - 50
                    implicitHeight: topViewDrawer.height - 100
                    clip: true
                    anchors.fill: parent

                    model: TrajectoryModel {
                        id: myModel
                        list: trajectoryList
                    }

                    ButtonGroup {
                        id: btGroup
                    }

                    delegate: RowLayout {
                        width: parent.width
                        RadioDelegate {
                            ButtonGroup.group: btGroup
                            checked: model.selected
                            onClicked: {
                                model.selected = checked
                                receiver.setTrajectoryTemplateIndex(index);
                            }
                        }
                        TextField {
                            Layout.fillWidth: true
                            text: model.description
                            font.pixelSize: 24
                            onEditingFinished: model.description = text
                        }
                    }
                }//end: ListView
            }//end: Frame

            Connections {
                target: canvasHandler

                onUpdateTrajectoryTemplatesList: {                    
                    trajectoryList.appendItem();
                }

                onResetNew: {
                    trajectoryList.resetList();
                }
            }
        }//end: ColumnLayout
    }//end: Drawer


    //- Right drawer
    Drawer {
        id: topViewDrawer_Contours
        width: 400
        height: root.height - (drawerButtonTop.height * 2)

        edge: Qt.RightEdge
        closePolicy: Popup.NoAutoClose
        modal: false

        ColumnLayout {
            spacing: 10

            Label {
                text: qsTr("Sketch Templates:")
                font.pixelSize: 28
                Layout.topMargin: 20
                Layout.leftMargin: 20
            }

            Button {
                id: buttonSaveTemplate
                text: " Save Template"
                font.pixelSize: 24
                //height: screenDpi.getDiP(60)
                 Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter //anchors.horizontalCenter: myFrame.horizontalCenter

                 enabled: root.isSelected

                onClicked: {
                    receiver.setCanvasVisualizationMV(ACTION.MV_SAVE_SKETCH_TEMPLATE);
                }
            }

            Frame {
                id: myFrame_Contours
                Layout.fillWidth: true
                Layout.margins: 10

                ListView {
                    id: myListView_Contours
                    implicitWidth: topViewDrawer_Contours.width - 50
                    implicitHeight: topViewDrawer_Contours.height - 120 - buttonSaveTemplate.height
                    clip: true
                    anchors.fill: parent

                    model: TrajectoryModel {
                        id: myModel_Contours
                        list: contourList
                    }

                    ButtonGroup {
                        id: btGroup_Contours
                    }

                    delegate: RowLayout {
                        width: parent.width
                        RadioDelegate {
                            ButtonGroup.group: btGroup_Contours
                            checked: model.selected
                            onClicked: {
                                model.selected = checked
                                receiver.setContourTemplateIndex(index);
                            }
                        }
                        TextField {
                            Layout.fillWidth: true
                            text: model.description
                            font.pixelSize: 24
                            onEditingFinished: model.description = text
                        }
                    }
                }//end: ListView
            }//end: Frame

            Connections {
                target: canvasHandler

                onUpdateContourTemplatesList: {
                    contourList.appendItem();
                }

                onResetNew: {
                    contourList.resetList();
                }

                onDirectionChanged: {
                    if(topViewDrawer.opened) {
                        topViewDrawer.close();
                    }
                    if( topViewDrawer_Contours.opened) {
                        topViewDrawer_Contours.close();
                    }
                }
            }
        }
    }

    Connections {
        target: canvasHandler

        onSelectSketchMV: {
            root.isSelected = response;
        }

        onSelectTrajectory: {
            root.isSelected = response;
        }

        onDirectionChanged: {
            root.isSelected = false;
        }

        onResetNew: {
            root.reset();
        }
    }

    Connections {
        target: receiver

        onSurfaceCreated: {
            root.isSelected = false;
        }
    }

    Connections {
        target: newVizHandler

        onImageRemoveButtonEnabled: {
            ////console.log("Enabling image remove button")
            if(direction === DIRECTION.HEIGHT) {
                btRemoveImage.enabled = true
            }

        }

        onImageRemoveButtonDisabled: {
            ////console.log("Disabling image remove button")
            if(direction === DIRECTION.HEIGHT) {
                btRemoveImage.enabled = false
            }

        }
    }


}//ens: Item
