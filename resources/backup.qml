
import QtQuick 2.12
import QtQuick.Controls 2.12

import io.uc.rrm.backend 1.0

//import "../controls"
import "scripts/window.js" as WindowScript

Rectangle {
    id: r1
    anchors.fill: parent
    anchors.centerIn: parent

    property var curve: []
    property var temp_curve: []
    property bool over_sketch: false
    property var submittedCurves: []
    property bool submitted: false

    Loader {
           id: popupLoader
           active: false
           source: "Popup.qml"
           onLoaded: item.open()
           //onClosed: item.close()
    }

    Popup {
            id: submitConfirmation
            x: parent.width - 100
            y: 100
            width: 250
            height: 200
            focus: true

            Image {
                anchors.fill: parent
                source: '/images/icons/submit.png'
            }

    }//End:Popup

    BackEnd {
        id: backend

        onPointChanged:{
            //var data = JSON.parse(response);
            var parsedData = WindowScript.parseSketchData(response)
            ////console.log("Plot Response from C++:"+JSON.stringify(data.plottingData));
            if(r1.over_sketch){
                ////console.log("Oversketching: 1" + response)
                r1.temp_curve = []
                r1.temp_curve = parsedData
            }
            else{
                ////console.log("Regular: 1" + response)
                r1.curve = parsedData
            }
        }

        onButtonActionHandled: {
            myCanvas.canvasAction = WindowScript.getActionId(response)
            r1.curve = data.plottingData = WindowScript.getPlottingData(response)
            myCanvas.requestPaint();
        }

    }//End: BackEnd


    Canvas {
        id: myCanvas
        anchors.fill: parent
        anchors.leftMargin: 5
        anchors.rightMargin: 5
        anchors.topMargin: 5
        anchors.bottomMargin: 5
        opacity: 0.5

        property var ctx
        property int canvasAction: 0

        property real csWidth: 500.0            // Width of the Cross Section
        property real csHeight: 500.0           // Height of the Cross Section
        property real csOrigin_X: 0.0           // X coordinate of the origin of the cross section
        property real csOrigin_Y: 0.0           // Y coordinate of the origin of the cross section
        property real csScale: 1.0              // scale to make the cross section fit the canvas area
        property int  csID: 64                  // numerical ID of the current cross section
        property int  csDirection: 0            // numerical identification of the direction of the cross section: LENGTH (0), WIDTH (1), HEIGHT (2).

        property bool canvasCompleted: false    // To keep track of the completion of Canvas gets loaded
        property bool doScale: true             // To make sure that the cross section will be scaled only once at the beginning

        property bool guidelines: false
        property var selectedPolygonCorners: []


        function updateScale() {

            var margin = 0.0    // margin used when fitting the cross section inside the canvas area

            if(myCanvas.doScale)
            {
                if( myCanvas.csWidth > myCanvas.csHeight )
                {
                    margin = (15 * myCanvas.width) / 100

                    if( myCanvas.csWidth > myCanvas.width )
                    {
                        myCanvas.csScale = (myCanvas.width - margin) / myCanvas.csWidth
                        myCanvas.csWidth  *= myCanvas.csScale
                        myCanvas.csHeight *= myCanvas.csScale
                    }
                    else if( myCanvas.csHeight > myCanvas.height )
                    {
                        myCanvas.csScale = (myCanvas.height - margin) / myCanvas.csHeight
                        myCanvas.csWidth  *= myCanvas.csScale
                        myCanvas.csHeight *= myCanvas.csScale
                    }
                } else {
                    margin = (15 * myCanvas.height) / 100

                    if( myCanvas.csHeight > myCanvas.height )
                    {
                        myCanvas.csScale = (myCanvas.height - margin) / myCanvas.csHeight
                        myCanvas.csWidth  *= myCanvas.csScale
                        myCanvas.csHeight *= myCanvas.csScale
                    }
                    else if( myCanvas.csWidth > myCanvas.width )
                    {
                        myCanvas.csScale = (myCanvas.width - margin) / myCanvas.csWidth
                        myCanvas.csWidth  *= myCanvas.csScale
                        myCanvas.csHeight *= myCanvas.csScale
                    }
                }
            }
            myCanvas.doScale = false
        }


        Component.onCompleted: {
            backend.updateBoxDimensions()

            myCanvas.csWidth = model3DHandler.size_x
            myCanvas.csHeight = model3DHandler.size_z

            myCanvas.canvasCompleted = true

            ////console.log("---> CANVAS onCOMPLETED: ")
            ////console.log("--------> size: " + myCanvas.width + " x " + myCanvas.height)
        }


        onCanvasSizeChanged: {

            if(myCanvas.canvasCompleted)
            {
                updateScale()

                myCanvas.csOrigin_X = (myCanvas.width  / 2.0) - (myCanvas.csWidth  / 2.0)
                myCanvas.csOrigin_Y = (myCanvas.height / 2.0) - (myCanvas.csHeight / 2.0)

                backend.csFrontOrigin = JSON.stringify({'x':myCanvas.csOrigin_X, 'y':myCanvas.csOrigin_Y, 's':myCanvas.csScale})
                canvasHandler.setCSFrontOrigin(myCanvas.csOrigin_X,myCanvas.csOrigin_Y,myCanvas.csScale)

                //receive the array of curves
                r1.submittedCurves = backend.returnSubmittedSketches()

                //reset the sketching arrays and flags
                if(r1.submitted)
                {
                    r1.curve = []
                    r1.temp_curve = []
                    r1.over_sketch = false
                }

                r1.curve = WindowScript.parseSketchData(backend.returnSketch())

                myCanvas.selectedPolygonCorners = WindowScript.parseSketchData(canvasHandler.returnRegion())
            }
            myCanvas.requestPaint()
        }


        CrossSection {
            id: myCrossSection
            anchors.fill: parent

            myOrigin_X: myCanvas.csOrigin_X
            myOrigin_Y: myCanvas.csOrigin_Y

            myWidth:  myCanvas.csWidth
            myHeight: myCanvas.csHeight

        } //END CrossSection

        Connections {
            target: canvasHandler

            onGetPoint2D: {
                //console.log("Received in QML from C++: " + screen_x + " " + screen_y)
            }
        }

        Connections {
            target: receiver //the C++ Object
            onSendToQml: {
                //console.log("Received in QML from C++: " + response)
                if(response ===1)
                    myCanvas.canvasAction = 0

                else if (response === 10)
                    myCanvas.guidelines = !myCanvas.guidelines

                //More work is needed to allow clearing the model
                else if (response === 20)
                {
                    //TBD
                    //Call the model3DHandler to clear from the SModeller side
                    //model3DHandler.clear()
                    //Call the vizHandler to clear the 3D bounding box
                    //vizHandler.clearAllModels()
                    //Call the file-new action to clear the sketch canvas
                    //myCanvas.canvasAction = 20
                }

                else if(response === 30) {
                    var num = backend.saveSketchTemplate()
                    receiver.setNumTemplates(1)
                    //console.log(num)
                    //myCanvas.canvasAction = 0
                }

                else if(response === 31) {
                    backend.loadSketchTemplate()
                    r1.curve = []
                    r1.curve = WindowScript.parseSketchData(backend.returnSketch())
                    r1.over_sketch = true
                    //myCanvas.canvasAction = 0
                    myCanvas.requestPaint()
                }
                else if(response ===55 || response ===56)
                    myCanvas.canvasAction = response

                else
                    backend.actionOperator = response

                myCanvas.requestPaint()
            }

            onCrossSectionChanged: {
                //console.log("Received cross-section #: " + id)
                myCanvas.csID = id
                backend.currentCsFront = JSON.stringify({'id': id})
                canvasHandler.setCurrentCrossSection(myCanvas.csID, myCanvas.csDirection)

                //receive the array of curves
                r1.submittedCurves = backend.returnSubmittedSketches()

                //reset the sketching arrays and flags
                if(r1.submitted)
                {
                    r1.curve = []
                    r1.temp_curve = []
                    r1.over_sketch = false
                }

                r1.curve = WindowScript.parseSketchData(backend.returnSketch())

                if(model3DHandler.isPreserveRegionActive()){

                    canvasHandler.updateRegionBoundary()
                    myCanvas.selectedPolygonCorners = WindowScript.parseSketchData(canvasHandler.returnRegion())
                }

                myCanvas.requestPaint()
            }
        }


        function connect3DView(){
            vizHandler.addSurfacePreviewFromSBIM()
            //receiver.setRulesButtonOff();
        }


        onPaint: {

            myCanvas.ctx = getContext('2d')

            WindowScript.clearCanvas(myCanvas.ctx, myCanvas.width, myCanvas.height)

            if(model3DHandler.isPreserveRegionActive()){
                WindowScript.fillSelectedRegion(myCanvas.ctx, myCanvas.selectedPolygonCorners)
            }

            myPinch.enabled = false

            switch(myCanvas.canvasAction){
                case 0: //default drawing
                    //WindowScript.draw(myCanvas.ctx,r1.curve);
                    WindowScript.draw(myCanvas.ctx,r1.temp_curve);

                    break;
                case 1: //edit

                    break;
                case 2: //smooth
                    //backend.smoothSketch()
                    //WindowScript.draw(myCanvas.ctx,r1.curve);
                    break;
                case 3: //translate
                    //WindowScript.draw(myCanvas.ctx,r1.curve);
                    break;
                case 4: //rotate
                    //WindowScript.draw(myCanvas.ctx,r1.curve);
                    break;
                case 5: //scale
                    //WindowScript.draw(myCanvas.ctx,r1.curve);
                    break;
                case 6: //clear
                    r1.curve = []
                    r1.temp_curve = []
                    r1.over_sketch = false
                    //WindowScript.draw(myCanvas.ctx,r1.curve);
                    break;
                case 7: //undo
                    //WindowScript.draw(myCanvas.ctx,r1.curve);
                    break;
                case 8: //redo
                    //WindowScript.draw(myCanvas.ctx,r1.curve);
                    break;
                case 9: //zoom
                    myPinch.enabled = true;
                    //WindowScript.draw(myCanvas.ctx,r1.curve);
                    break;
                case 55: //start preserve region
                    WindowScript.fillSelectedRegion(myCanvas.ctx, myCanvas.selectedPolygonCorners, "#8ef0ad")
                    break;
                case 56: //stop preserve region
                    model3DHandler.stopPreserveRegion()
                    //Call the fillSelectedRegion with transparent (#ffffff) color
                    WindowScript.fillSelectedRegion(myCanvas.ctx, myCanvas.selectedPolygonCorners, "#ffffff")
                    myCanvas.selectedPolygonCorners = []
                    myCanvas.canvasAction = 0
                    break;
                case 20: //new
                    r1.curve = []
                    r1.temp_curve = []
                    r1.over_sketch = false
                    WindowScript.clearCanvas(myCanvas.ctx, myCanvas.width, myCanvas.height)
                    break;

                case 30: //save template
                    //WindowScript.draw(myCanvas.ctx,r1.curve);
                    break;

                case 31: //load template
                    //WindowScript.draw(myCanvas.ctx,r1.curve);
                    WindowScript.draw(myCanvas.ctx,r1.temp_curve);
                    break;

                case 100: //over_sketch redraw
                    //WindowScript.draw(myCanvas.ctx,r1.curve);
                    WindowScript.draw(myCanvas.ctx,r1.temp_curve, 100);
                    break;

                case 101: //draw submitted curves
                    WindowScript.displaySubmittedCurves(r1.submittedCurves, myCanvas.ctx);
                    break;
            }

            WindowScript.displaySubmittedCurves(r1.submittedCurves, myCanvas.ctx);
            WindowScript.draw(myCanvas.ctx,r1.curve);
            //WindowScript.showGrid(myCanvas.ctx,myCanvas.width, myCanvas.height, 50, 1)

            // Guidelines
            if(myCanvas.guidelines) {
                WindowScript.showGuidelines(myCanvas.ctx, area, myCanvas.width, myCanvas.height)
            }


        }
        //////////////////////Start: Mouse Button Actions////////////////////////////////////////////////////////
        function rightButtonDoubleClickAction()
            {
                //TODO: check if there is any unsubmitted sketch

                //call submitSketch action
                backend.submitSketch()
                r1.submitted = true

                //receive the array of curves
                r1.submittedCurves = backend.returnSubmittedSketches()

                //reset the sketching arrays and flags
                r1.curve = []
                r1.temp_curve = []
                r1.over_sketch = false

                ////console.log("Bingo: " + r1.submittedCurves)
                var curves = JSON.parse(r1.submittedCurves)
                ////console.log("Bingo: " + curves["curves_array"])

                //if (curves["curves_array"] != undefined || curves["curves_array"].length != 0) {
                if (curves.curves_array != undefined || curves.curves_array.length != 0) {
                    //request paint to draw the submitted sketches
                    myCanvas.requestPaint()

                    ////console.log("Testing Model3dHandler: " + model3DHandler.fontFamily)
                    myCanvas.connect3DView()
                    //popupLoader.active = true
                    submitConfirmation.open()


                    //update canvasAction to Sketch
                    receiver.setEvent(1)
                }
                else //console.log("No curve to submit ")
            }
            function leftButtonPressAction(mouseX, mouseY)
            {
                //console.log("Status: " + myCanvas.canvasAction)
                //backend.sketchedPoint = JSON.stringify({'x': mouseX, 'y': mouseY})
                canvasHandler.setPoint2D(mouseX,mouseY)

                if(myCanvas.canvasAction===0){
                    r1.submitted = false
                    backend.SketchPress()
                }
                else if(myCanvas.canvasAction===3){
                    //translate
                    backend.TranslatePress()
                    r1.curve = WindowScript.parseSketchData(backend.returnSketch())
                    myCanvas.requestPaint()
                }
                else if(myCanvas.canvasAction===4){
                    //rotate
                    backend.RotatePress()
                    r1.curve = WindowScript.parseSketchData(backend.returnSketch())
                    myCanvas.requestPaint()
                }
                else if(myCanvas.canvasAction===5){
                    //scale
                    backend.ScalePress()
                    r1.curve = WindowScript.parseSketchData(backend.returnSketch())
                    myCanvas.requestPaint()
                }
            }
            function leftButtonDragAction(mouseX, mouseY)
            {
                //backend.sketchedPoint = JSON.stringify({'x':mouseX, 'y':mouseY})
                canvasHandler.setPoint2D(mouseX,mouseY)

                if(myCanvas.canvasAction===0){
                    backend.SketchMove()
                }
                else if(myCanvas.canvasAction===3){
                    backend.TranslateMove()
                    r1.curve = WindowScript.parseSketchData(backend.returnSketch())
                }
                else if(myCanvas.canvasAction===4){
                    backend.RotateMove()
                    r1.curve = WindowScript.parseSketchData(backend.returnSketch())
                }
                else if(myCanvas.canvasAction===5){
                    backend.ScaleMove()
                    r1.curve = WindowScript.parseSketchData(backend.returnSketch())
                }

                myCanvas.requestPaint()
            }
            function leftButtonReleaseAction ()
            {
                if(myCanvas.canvasAction===0){
                    backend.SketchRelease()
                    r1.curve = WindowScript.parseSketchData(backend.returnSketch())

                    if(!r1.over_sketch)
                        r1.over_sketch = true
                    else {
                        //myCanvas.canvasAction = 100
                        r1.temp_curve = WindowScript.parseSketchData(backend.returnOverSketch())
                    }

                    myCanvas.requestPaint()
                }
                else if(myCanvas.canvasAction===3){
                        backend.TranslateRelease()
                        r1.curve = WindowScript.parseSketchData(backend.returnSketch())
                        myCanvas.requestPaint()
                }
                else if(myCanvas.canvasAction===4){
                        backend.RotateRelease()
                        r1.curve = WindowScript.parseSketchData(backend.returnSketch())
                        myCanvas.requestPaint()
                }
                else if(myCanvas.canvasAction===5){
                        backend.ScaleRelease()
                        r1.curve = WindowScript.parseSketchData(backend.returnSketch())
                        myCanvas.requestPaint()
                }
            }

            function pressAndHoldPreserveRegionAction (mouseX, mouseY)
            {
                if (myCanvas.canvasAction ===55)
                {
                    //Send this point to the canvasHandler and wait for response
                    canvasHandler.preserveRegionBoundary(mouseX,mouseY,myCanvas.csID,myCanvas.csDirection)
                    myCanvas.selectedPolygonCorners = WindowScript.parseSketchData(canvasHandler.returnRegion())


                    //Repaint canvas with the selectedPolygonCorners
                    myCanvas.requestPaint()

                    myCanvas.canvasAction = 0
                }
            }
        /////////////////////////////End: Mouse Button Actions////////////////////////////

        PinchArea {
            id: myPinch
            anchors.fill: parent
            pinch.target: myCanvas
            enabled: false
            pinch.maximumScale: 10.0
            pinch.minimumScale: 0.1
            pinch.maximumRotation: 0 //360
            pinch.minimumRotation: 0 //-360
            pinch.dragAxis: Pinch.XAndYAxis

            onPinchStarted: {
                area.mouseButtonIdentifier = 0
                //console.log("PinchArea onPinchStarted: " +myCanvas.canvasAction+ "\n")
                ////console.log("PinchArea onPinchStarted" + "\n")
            }

            MouseArea{
                id: area
                anchors.fill: parent
                hoverEnabled: true
                pressAndHoldInterval: 500 //reducing the interval from the default 800ms

                acceptedButtons: Qt.AllButtons
                property string buttonID
                property string info
                property string btnText
                property int mouseButtonIdentifier: 0 // 1:LeftButton and 2: RightButton

                onReleased: {
                    btnText = 'Released (isClick=' + mouse.isClick + ' wasHeld=' + mouse.wasHeld + ')'
                    //console.log(btnText)
                    if(mouseButtonIdentifier===1) // if the LeftButton is released
                    {
                        //console.log(btnText + "LeftButtton released");
                        myCanvas.leftButtonReleaseAction();
                        mouseButtonIdentifier = 0
                    }
                    else //console.log(btnText + "THe button identifier=" + mouseButtonIdentifier)
                }

                //! [clicks]
                onPositionChanged: {
                    if(mouseButtonIdentifier===1)
                    {
                        btnText = 'Action: Dragging'
                        //console.log(btnText)

                        myCanvas.leftButtonDragAction(mouseX, mouseY)
                    }
                    myCanvas.requestPaint()
                }

                onPressed: {
                    if (mouse.button === Qt.LeftButton)
                    {
                        buttonID = 'Button: LeftButton'
                        mouseButtonIdentifier = 1

                        myCanvas.leftButtonPressAction(mouseX, mouseY)

                    }
                    else if (mouse.button === Qt.RightButton)
                    {
                        buttonID = 'Button: RightButton'
                        mouseButtonIdentifier = 2
                    }
                    info = 'Pressed (' + buttonID + ' shift='
                        + (mouse.modifiers & Qt.ShiftModifier ? 'true' : 'false') + ')'
                    //console.log("==========================================")
                    //console.log(info)
                }

                onClicked: {
                    canvasHandler.setPoint2D(mouseX,mouseY)
                    canvasHandler.setCurrentCrossSection(myCanvas.csID, myCanvas.csDirection)

                    //btnText = 'Action: Clicked (wasHeld=' + mouse.wasHeld + ')'
                    ////console.log(btnText)

                    //- TO BE IMPLEMENTED
                    //if(myCanvas.canvasAction === 30) {
                    //    backend.sketchedPoint = JSON.stringify({'x': mouseX, 'y': mouseY})
                    //    backend.isCurveUnderMouse()
                    //}
                }

                onDoubleClicked: {
                    if(mouseButtonIdentifier===2)
                    {
                        btnText = 'Action: Double clicked Right Button'
                        //console.log(btnText)

                        myCanvas.rightButtonDoubleClickAction()
                    }
                }

                onPressAndHold: {
                    btnText = 'Action: Press and hold'
                    //console.log(btnText + ": " + mouseX + "," + mouseY)
                    myCanvas.pressAndHoldPreserveRegionAction(mouseX, mouseY)

                }

                /////////////Touch input Handling /////////////////////
                TapHandler {
                    acceptedDevices: PointerDevice.TouchScreen | PointerDevice.Stylus

                    onTapped:{
                        //console.log("tapped")
                        myCanvas.rightButtonDoubleClickAction()
                   }
                   //Commenting out for now; we will use if we need it in future
                   //MouseArea double click also works with single finger double tap
                   /*onLongPressed:{
                        //console.log("longPressed")
                   }*/

                }//end: TapHandler
            }//end: MouseArea
        }//end: PinchArea
    }//end: Canvas
}//end: Rectangle


