import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Extras 1.4
import QtQuick.Layouts 1.2

import Constants 1.0

Item {
    id: regionVisualizer
    anchors.fill: parent
    
    Timer {
        id: timer1

        function setTimeout(cb, delayTime) {
            timer1.interval = delayTime;
            timer1.repeat = false;
            timer1.triggered.connect(cb);
            timer1.triggered.connect(function release () {
                timer1.triggered.disconnect(cb); // This is important
                timer1.triggered.disconnect(release); // This is important as well
            });
            timer1.start();
        }
    }

    Button {
        id: regionVizButton
        width: parent.width
        height: parent.height

        //text: "Viz"
        checked: true

        Component.onCompleted: {
            regionVizButton.checked = false
        }

        icon.color: "transparent"
        icon.source: regionVizButton.checked ? "/images/icons/object-tree-region-on.png" : "/images/icons/object-tree-region-off.png"
        icon.width: regionVisualizer.implicitWidth
        icon.height: regionVisualizer.implicitHeight

        ToolTip {
            visible: regionVizButton.hovered
            y: regionVizButton.height
            contentItem: Text{
                text: "Regions & Domains"
                font.weight: Font.ExtraBold
                font.pixelSize: screenDpi.getDiP(16)
            }
            background: Rectangle {
                border.color: "#a6a6a6"
            }
        }
        
        onClicked: {
            //console.log("Regio Viz: surface count=" + newVizHandler.getSurfaceCount())
            if(newVizHandler.getSurfaceCount() > 1) {
                if(checked) {
                    overlay.loadingNotifier.message = "Loading Regions ...";
                } else {
                    overlay.loadingNotifier.message = "Unloading Regions ...";
                }
                overlay.loadingNotifier.open();

                timer1.setTimeout(function(){
                    newVizHandler.setVizFlag(checked)
                    //- Commented because it is not being used
                    //regionVizButton.startVal = regionVizButton.startVal + 1
                }, 100);
            } else {
                regionVizButton.checked = false
            }
        }
    }
    
    Connections {
        target: newVizHandler
        
        onResetViz: {
            regionVizButton.checked = false
        }

        onRegionsDisplayed: {
            timer1.setTimeout(function(){
                overlay.loadingNotifier.close();
            }, 500);
        }

        onRegionsRemoved: {
            //theModel.removeRegions();
            timer1.setTimeout(function(){
                overlay.loadingNotifier.close();
            }, 500);
        }
    }
}
