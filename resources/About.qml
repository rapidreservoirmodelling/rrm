import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import QtQuick.Extras 1.4

Rectangle {
    anchors.left: parent.left + screenDpi.getDiP(30)
    anchors.topMargin: screenDpi.getDiP(30)
    
    Component.onCompleted: {
        //aboutButton.clicked()
    }
    Button {
            id: aboutButton
            text: "About"
            font.weight: Font.ExtraBold
            font.pixelSize: screenDpi.getDiP(32)

            property bool isActive: false
            
            contentItem: Text {
                    text: aboutButton.text
                    font: aboutButton.font
                    opacity: enabled ? 1.0 : 0.3
                    color: aboutButton.isActive ? "black" : "#cccccc" //color: aboutButton.down ? "#595959": "#cccccc" //"#595959": "#737373"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight

            }
            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                opacity: enabled ? 1 : 0.3
                border.color: aboutButton.down ? "#595959": "#737373"
                color:  aboutButton.isActive ? "#74d0f0" : "#4d4d4d"
                border.width: 2
                radius: 2
            }
            
            ToolTip {
                visible: aboutButton.hovered
                contentItem: Text{
                    text: "Details"
                    font.weight: Font.ExtraBold
                    font.pixelSize: screenDpi.getDiP(16)//32
                }
                background: Rectangle {
                            border.color: "#a6a6a6"
                }

            }
            
            onClicked: {
                aboutDetailsPopup.open()
                
            }
        

    }//End:Button
	Popup {
            id: aboutDetailsPopup
            focus: true
            modal: true
            
            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent//Popup.NoAutoClose
            
            Overlay.modal: Rectangle {
                color: "#aacfdbe7"
                border.color: "black"
                border.width: 60
                radius: 100
                anchors.topMargin: 45
                
                GridLayout {
                    columns: 1
                    anchors.centerIn: parent

                    Text {
                        id: aboutTextHeading
                        text: '<h1>RRM Phase 2</h1>'
                           
                        textFormat: Text.StyledText
                        font.family: 'Courier New'
                        font.pixelSize: screenDpi.getDiP(60)
                        font.underline: true
                        font.capitalization: Font.SmallCaps

                        color: "black"
                    
                    }
                    Text {
                        id: aboutTextSubHeading
                    
                        text: '<h2>Version 4.0 (Alpha)</h2>'
                           
                        textFormat: Text.StyledText
                        font.family: 'Courier New'
                        font.pixelSize: screenDpi.getDiP(55)
                        font.capitalization: Font.SmallCaps
                    
                        color: "black"
                    
                    }
                    Text {
                        id: aboutText
                    
                        text: '<p> This is a proof of concept prototype, released for </p>
                               <p>  early testing and evaluation of the new SBIM functionality </p> 
                               <p> and the introduction of gestures. </p>
                               <p>This version is meant to be used in non-production or </p>
                               <p> virtualized environments and it comes with <b>NO WARRANTY</b>. </p>'

                        textFormat: Text.StyledText
                    
                        color: "black"
                        font { family: 'Courier New'; pixelSize: screenDpi.getDiP(50); italic: true; capitalization: Font.SmallCaps }

                    }
                }

            }
            
           
    }//End:Popup
    
}//End: Rectangle