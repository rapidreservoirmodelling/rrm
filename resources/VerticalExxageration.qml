import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

import Constants 1.0

RowLayout {
    id: root

    function reset() {
        veLabel.isActive = false;
        spinbox.value = 10;
        spinbox.enabled = false
        bkgImage.isActive = false;
        canvasHandler.veBkgImage = false;
        bkgImage.enabled = false;
        angle.isActive = false;
        angle.enabled = true;
        canvasHandler.veAngleVisible = false;
        spinboxAngle.value = 0;
        spinboxAngle.enabled = true;
    }

    Component.onCompleted: {
    }

//    Label {
//        id: veLabel
//        text: "VE:"
//        font.pixelSize: 16
//        font.italic: true
//    }

    ButtonToolBar {
        id: veLabel
        text: "VE"
        //implicitWidth: screenDpi.getDiP(120)
        myToolTipText.text: "Vertical Exxageration"
        isActive: false

        onClicked: {
            if(veLabel.isActive) {
                veLabel.isActive = false;
                spinbox.enabled = false;
                canvasHandler.veFactor = (1.0);
                newVizHandler.veFactor = (1.0);
                canvasHandler.veBkgImage = false;
                bkgImage.enabled = false;
                //angle.enabled = false;
                //canvasHandler.veAngleVisible = false;
                //spinboxAngle.enabled = false;
            } else {
                veLabel.isActive = true;
                spinbox.enabled = true;
                canvasHandler.veFactor = (spinbox.value/10.0);
                newVizHandler.veFactor = (spinbox.value/10.0);
                bkgImage.enabled = true;
                canvasHandler.veBkgImage = bkgImage.isActive;
                //angle.enabled = true;
                //canvasHandler.veAngleVisible = angle.isActive;
                //spinboxAngle.enabled = true;
            }
        }
    }

    SpinBox {
        id: spinbox
        from: 1
        value: 10
        to: 100000
        stepSize: 1
        editable: true
        enabled: false

        property int decimals: 1
        property real realValue: value / 10

        ToolTip {
            visible: spinbox.hovered
            contentItem: Text{
                text: "Vertical Exxageration"
                color: "gray"
                font.pixelSize: screenDpi.getDiP(BT_TOOLBAR.TOOLTIP_FONT_PIXEL_SIZE)
            }
            background: Rectangle {
                border.color: "transparent"
            }
        }

        validator: DoubleValidator {
            bottom: Math.min(spinbox.from, spinbox.to)
            top:  Math.max(spinbox.from, spinbox.to)
        }

        textFromValue: function(value, locale) {
            return Number(value / 10).toLocaleString(locale, 'f', spinbox.decimals)
        }

        valueFromText: function(text, locale) {
            return Number.fromLocaleString(locale, text) * 10
        }

        onValueChanged: {
            canvasHandler.veFactor = (value/10.0);
            newVizHandler.veFactor = (value/10.0);
        }
    }

    /*
    Label {
        id: angleLabel
        text: "Angle:"
        font.pixelSize: 16
        font.italic: true
    }
    */

    ButtonToolBar {
        id: bkgImage
        text: "IMG"
        //implicitWidth: screenDpi.getDiP(100)
        myToolTipText.text: "Apply VE to background image."
        isActive: false
        hasIcon: true
        icon.source: "/images/icons/image.png"
        enabled: false

        onClicked: {
            isActive = !isActive;
            canvasHandler.veBkgImage = isActive;
        }
    }

    ButtonToolBar {
        id: angle
        text: "ANGLE"
        //implicitWidth: screenDpi.getDiP(120)
        myToolTipText.text: "Dip angle guide"
        isActive: false
        hasIcon: true
        icon.source: "/images/icons/dip_angle.png"
        enabled: true

        onClicked: {
            if(angle.isActive) {
                angle.isActive = false;
                canvasHandler.veAngleVisible = false;
            } else {
                angle.isActive = true;
                canvasHandler.veAngleVisible = true;
            }
        }
    }

    SpinBox {
        id: spinboxAngle
        from: 0
        value: 0
        to: 900
        stepSize: 1
        editable: true
        enabled: true

        property int decimals: 1
        property real realValue: value / 10

        ToolTip {
            visible: spinboxAngle.hovered
            contentItem: Text{
                text: "Dip Angle"
                color: "gray"
                font.pixelSize: screenDpi.getDiP(BT_TOOLBAR.TOOLTIP_FONT_PIXEL_SIZE)
            }
            background: Rectangle {
                border.color: "transparent"
            }
        }

        validator: DoubleValidator {
            bottom: Math.min(spinboxAngle.from, spinboxAngle.to)
            top:  Math.max(spinboxAngle.from, spinboxAngle.to)
        }

        textFromValue: function(value, locale) {
            return Number(value / 10).toLocaleString(locale, 'f', spinboxAngle.decimals)
        }

        valueFromText: function(text, locale) {
            return Number.fromLocaleString(locale, text) * 10
        }

        onValueChanged: {
            canvasHandler.veAngle = (spinboxAngle.value/10.0);
        }
    }

    Connections {
        target: canvasHandler

        onResetNew: {
            root.reset();
        }
    }
}

