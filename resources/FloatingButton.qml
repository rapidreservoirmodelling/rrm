import QtQuick 2.12
import QtQuick.Controls 2.12
Item {
    id: dragItem
    anchors.fill: parent
    width: screenDpi.getDiP(200)
    height: screenDpi.getDiP(200)

    property alias maxHeight: dragArea.maxHeight
    property alias maxWidth: dragArea.maxWidth

    function reset() {
        if(rbutton.toolsOpen){
            optionsLoader.source = "";
            rbutton.toolsOpen = false;
        }
        visible = true;
    }

    RoundButton {
        id: rbutton
        x: screenDpi.getDiP(812)//120
        y: screenDpi.getDiP(120)//120
        
        width: screenDpi.getDiP(120)//200//120
        height: screenDpi.getDiP(120)//180//100

        icon.color: "transparent"
        icon.source: "/images/icons/floating-buttons.png"
        icon.width: rbutton.width
        icon.height: rbutton.height
        
        Drag.active: dragArea.drag.active
        Drag.hotSpot.x: 10
        Drag.hotSpot.y: 10

        property point beginDrag
        property bool caught: false
        property bool toolsOpen: false
        property bool landscape: false 
        property int numberOfCols: 2 

        Loader { id: optionsLoader }

        MouseArea {
            id: dragArea
            anchors.fill: parent

            //To restrict drag and drop within the visible MouseArea we need maxHeight and maxWidth values ;
            // Initializing using default values here
            //default values will get overridden by the values sent by parent element
            property int maxHeight: 100
            property int maxWidth: 100

            //Setting drag extent (drag and drop within the visible MouseArea)
            drag.maximumX: maxWidth - screenDpi.getDiP(50)
            drag.minimumX: screenDpi.getDiP(50)//40
            drag.maximumY: maxHeight - screenDpi.getDiP(50)
            drag.minimumY: screenDpi.getDiP(50)//30

            drag.target: parent

            onPressed: {
                rbutton.beginDrag = Qt.point(rbutton.x, rbutton.y);
            }

            onReleased: {
                if(!rbutton.caught ) {
                    backAnim2X.from = rbutton.x;
                    backAnim2X.to = beginDrag.x;
                    backAnim2Y.from = rbutton.y;
                    backAnim2Y.to = beginDrag.y;
                    backAnim2.start()
                }
            }

            onClicked: {
                if((!rbutton.caught)) {
                    rbutton.width = screenDpi.getDiP(120)//200
                    rbutton.height = screenDpi.getDiP(120)//180

                    //show content
                    if(!rbutton.toolsOpen){
                        //optionsLoader.source = "VerticalToolBar.qml"
                        optionsLoader.setSource("VerticalToolBar.qml", {"numberOfColumns":rbutton.numberOfCols});    
                        rbutton.toolsOpen = true
                    }
                    //Hide content
                    else if(rbutton.toolsOpen){
                        optionsLoader.source = ""
                        rbutton.toolsOpen = false
                    }
                }
            }

            onDoubleClicked: {
                if(rbutton.landscape) {
                    rbutton.numberOfCols = 2
                    rbutton.landscape = false 
                }
                else {
                    rbutton.numberOfCols = 6
                    rbutton.landscape = true
                }
                optionsLoader.source = ""
                optionsLoader.setSource("VerticalToolBar.qml", {"numberOfColumns":rbutton.numberOfCols});
            }
        }

        ParallelAnimation {
            id: backAnim2
            SpringAnimation { id: backAnim2X; target: rbutton; property: "x"; duration: 500; spring: 2; damping: 0.2 }
            SpringAnimation { id: backAnim2Y; target: rbutton; property: "y"; duration: 500; spring: 2; damping: 0.2 }
        }

        Connections {
            target: canvasHandler

            onResetNew: {
                dragItem.reset();
            }
        }
    }
}
