import QtQuick 2.0
import QtQuick.Controls 2.5

import Constants 1.0

Button {
    id: button

    property bool isActive: false
    property bool isBordered: true
    property bool hasBackground: false
    property bool hasIcon: false

    property alias myBackground : rect
    property alias myToolTip : toolTipText

    implicitWidth: screenDpi.getDiP(BT_CANVAS.WIDTH)
    implicitHeight: screenDpi.getDiP(BT_CANVAS.HEIGHT)

    text: hasIcon ? "" : "Button"
    display: hasIcon ? AbstractButton.IconOnly : AbstractButton.TextOnly

    icon.color: enabled ? (down ? BT_CANVAS.ICON_COLOR_DOWN : BT_CANVAS.ICON_COLOR_UP) : BT_CANVAS.ICON_COLOR_DISABLE
    icon.source: hasIcon ? "images/button.png" : ""
    icon.width: button.implicitWidth
    icon.height: button.implicitHeight

    background: Rectangle {
        id: rect
        implicitWidth: button.implicitWidth
        implicitHeight: button.implicitHeight
        color: down ? BT_CANVAS.BACKGROUND_COLOR_DOWN : (hasBackground ? BT_CANVAS.BACKGROUND_COLOR_UP : ( button.down ? BT_CANVAS.BACKGROUND_COLOR_DOWN : "transparent"))
        border.color: isBordered ? ( enabled ? (down ? BT_CANVAS.BORDER_COLOR_DOWN : BT_CANVAS.BORDER_COLOR_UP) : BT_CANVAS.BORDER_COLOR_DISABLE) : "transparent"
        border.width: button.isActive ? screenDpi.getDiP(BT_CANVAS.BORDER_WIDTH_ACTIVE) : (button.down ? screenDpi.getDiP(BT_CANVAS.BORDER_WIDTH_ACTIVE) : screenDpi.getDiP(BT_CANVAS.BORDER_WIDTH))
        radius: screenDpi.getDiP(BT_CANVAS.BORDER_RADIUS)
    }

    ToolTip {
        id: toolTip
        visible: button.hovered
        //delay: -1
        //timeout: 1
        contentItem: Text{
            id: toolTipText
            text: "Tool Tip"
            color: "gray"
            font.pixelSize: screenDpi.getDiP(BT_CANVAS.TOOLTIP_FONT_PIXEL_SIZE)
        }

        background: Rectangle {
            border.color: "transparent"
        }
    }
}
