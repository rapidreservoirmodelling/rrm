import QtQuick 2.0

import Constants 1.0

Item {
    id: root

    property alias myWidth: crossSectionArea.width
    property alias myHeight: crossSectionArea.height

    property alias myOrigin_X: crossSectionArea.x
    property alias myOrigin_Y: crossSectionArea.y

    property alias myLeftDirectionLabel: myLeftDirection.text
    property alias myRightDirectionLabel: myRightDirection.text

    property bool isDisable: false

    property int direction: DIRECTION.LENGTH

    property bool imgVisibility: true

    z: -1

    Rectangle {
        id: crossSectionArea
        border.width: 3
        color: "#C9F8D8"
        border.color: "black"
        opacity: root.isDisable ? 0.1 : 0.3

        width: 500
        height: 500

        x: 0
        y: 0

        Text {
            id: myLeftDirection
            color: "black"
            font.pointSize: 16
            anchors.right: crossSectionArea.left
            anchors.top: crossSectionArea.top
            anchors.rightMargin: screenDpi.getDiP(70) //12
        }

        Text {
            id: myRightDirection
            color: "black"
            font.pointSize: 16
            anchors.left: crossSectionArea.right
            anchors.top: crossSectionArea.top
            anchors.leftMargin: 12
        }
    } //END Rectangle

    Rectangle {
        width: crossSectionArea.width
        height: crossSectionArea.height
        anchors.centerIn: crossSectionArea
        color: "transparent"
        ImageDropWindow {
            direction: root.direction
            imageIsVisible: root.imgVisibility
            //opacity: root.imgVisibility ? 1 : 0.1
        }
    }

} //END Item
