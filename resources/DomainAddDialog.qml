import QtQuick 2.12
import QtQuick.Controls 2.1
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12

import Constants 1.0

Dialog {
    id: root
    modal: true
    width: screenDpi.getDiP(800)
    height: screenDpi.getDiP(400)
    x: 0
    y: parent.height/2 - height/2
    z: 100

    property var treeItem
    property alias selectedTreeItem: root.treeItem

    Timer {
        id: timer1

        function setTimeout(cb, delayTime) {
            timer1.interval = delayTime;
            timer1.repeat = false;
            timer1.triggered.connect(cb);
            timer1.triggered.connect(function release () {
                timer1.triggered.disconnect(cb); // This is important
                timer1.triggered.disconnect(release); // This is important as well
            });
            timer1.start();
        }
    }

    //title: "New Domain"
    header : Rectangle {
        height: screenDpi.getDiP(0.25 * parent.height)
        width: screenDpi.getDiP(parent.width)
        color: "#EEEEEE"
        Text {
            text: qsTr("New Domain")
            font.pixelSize: screenDpi.getDiP(20)
            font.bold: false
            font.italic: true
            font.capitalization: Font.Capitalize
            anchors {
                left: parent.left
                leftMargin: screenDpi.getDiP(20)
                verticalCenter: parent.verticalCenter
            }
        }
    }

    contentItem: NotesForm {
        id: domainCreationForm
        itemNameText: ""
        isSingleItemForm: false
    }

    footer: Rectangle {
        id: footer
        height: screenDpi.getDiP(0.2 * parent.height)
        width: screenDpi.getDiP(parent.width)
        color: "#EEEEEE"

        RowLayout {
            anchors.centerIn: parent
            spacing: screenDpi.getDiP(20)

            ButtonDialog {
                id: theDialogButton
                text: qsTr("Add")
                backgroundDefaultColor: OBJECT_TREE.ADD_BUTTON_COLOR
                //visible: (root.treeItem.parentTypeId !==5)? true: false
                Layout.fillHeight: false
                Layout.alignment: Qt.AlignCenter

                onClicked: {
                    //console.log("DomainAdd Dialog Add button clicked !")
                    if(domainCreationForm.itemNameText !=="") {
                        newVizHandler.addToDomainList(-1,domainCreationForm.itemNameText, domainCreationForm.itemDescription);
                        domainCreationForm.isWarningOn = false;
                        timer1.setTimeout(function(){
                            domainCreationForm.itemNameText = ""
                            domainCreationForm.itemDescription = ""
                            root.close()
                        }, 100);
                    } else {
                        //console.log("Domain name is required!");
                        domainCreationForm.warningMessage = "Domain name is required!"
                        domainCreationForm.isWarningOn = true;
                    }
                }
            }

            ButtonDialog {
                text: qsTr("Cancel")
                backgroundDefaultColor: OBJECT_TREE.CLOSE_BUTTON_COLOR
                //visible: (root.treeItem.parentTypeId !==5)? true: false
                Layout.fillHeight: false
                Layout.alignment: Qt.AlignCenter

                onClicked: {
                    //console.log("DomainAdd Dialog close button clicked !")
                    timer.setTimeout(function(){
                        domainCreationForm.itemNameText = ""
                        domainCreationForm.itemDescription = ""
                        root.close()
                    }, 100);
                }
            }
        }
    }
}
