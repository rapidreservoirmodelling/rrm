
import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

Item {
    id: root
    anchors.fill: parent
    anchors.centerIn: parent

    TopSketchWindow {
        id: topSketch
    }

}//end: Item
