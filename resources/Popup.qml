import QtQuick 2.12
import QtQuick.Controls 2.12

import Constants 1.0

Popup {
        id: popup
        anchors.centerIn: parent
        x: parent.x + screenDpi.getDiP(50)
        y: parent.y + screenDpi.getDiP(50)
        width: screenDpi.getDiP(200)//200
        height: screenDpi.getDiP(200)//200
        modal: false
        focus: true
        //closePolicy: Popup.CloseOnPressOutside | Popup.CloseOnPressOutsideParent

        Image {
            //id: rect
            anchors.fill: parent
            source: '/images/icons/submit.png'
        }
        PropertyAnimation {
            running: true
            target: popup
            property: 'visible'
            to: false
            duration: 3000 // turns to false after 3000 ms

            onStarted: {
                if(COMMENTS.DEV_FAZILA) {
                    //console.log("Now showing checkmark..")
                }
            }

            onStopped: {
                if(COMMENTS.DEV_FAZILA) {
                    //console.log("Now hiding checkmark..")
                }
                popup.close()
            }

        }
    }
