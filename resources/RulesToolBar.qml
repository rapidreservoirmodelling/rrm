import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import QtQuick.Extras 1.4
import QtQuick.Dialogs 1.2

import Constants 1.0

RowLayout{
    id: root

    function reset() {
        //- Preserve Region
        preserveRegionButton.isActive = false;
        preserveRegionButton.enabled = false;
        regionOpacity.value = 0.4;

        flattening.enabled = false;
        flattening.isActive = false;

        //- Geological Rules
        model3DHandler.removeAboveIntersection();
        receiver.setRulesButtonOff();
        removeAboveIntersectionButton.isActive = true;
        newVizHandler.setCurrentRuleOperator(GEOLOGICRULES.RAI)

        //- Direction
        directionWidth.enabled = true;
        directionLength.enabled = true;
        directionHeight.enabled = true;
        directionWidth.isActive = false;
        directionLength.isActive = true;
        directionHeight.isActive = false;
        canvasHandler.direction = DIRECTION.LENGTH;
        newVizHandler.setPlaneOrientation(DIRECTION.LENGTH, canvasHandler.crossSection);
    }

    Component.onCompleted: {
        //removeAboveButton.clicked()
        removeAboveIntersectionButton.clicked();
        canvasHandler.direction = DIRECTION.LENGTH;
    }

    ToolSeparator {}

    ThreeDViewDrawer {
        spacing: parent.spacing
    }
    
    ToolSeparator {}

    VerticalExxageration{}

    ToolSeparator {}

    ButtonToolBar {
        id: preserveRegionButton
        enabled: false
        isBordered: true
        text: "SR"
        myToolTip.contentItem: Item {
            GridLayout {
                columns: 1
                anchors.centerIn: parent
                Text {
                    text: '<b>Sketch Region</b>'
                    textFormat: Text.StyledText
                    font.weight: Font.ExtraBold
                    font.pixelSize: screenDpi.getDiP(16)
                    color: "black"
                }
                Text {
                    text: '<p>When this button is active, You may select available regions by:</p>'
                    font { family: 'Courier New'; pixelSize: screenDpi.getDiP(18); italic: true; capitalization: Font.SmallCaps }
                    color: "black"
                }
                Text {
                    text: '<p>1) using press-and-hold inside the region, or</p>
                           <p>2) sketching in the up direction on the surface, or</p>
                           <p>3) sketching in the below direction on the surface</p>'
                    font { family: 'Courier New'; pixelSize: screenDpi.getDiP(16); italic: true; capitalization: Font.SmallCaps }
                    color: "black"
                }
                Text {
                    text: '<p>Use right-button double click to confirm region selection</p>'
                    font { family: 'Courier New'; pixelSize: screenDpi.getDiP(18); italic: true; capitalization: Font.SmallCaps }
                    color: "black"
                }
            }
        }

        onClicked: {
            //receiver.setPreserveButtonOff()

            if(preserveRegionButton.isActive){
                preserveRegionButton.isActive = false
                //Disable PR
                receiver.setCanvasAction(ACTION.TB_STOP_SKETCH_REGION);
                receiver.setLastCanvasAction();
            }
            else if(!preserveRegionButton.isActive) {
                preserveRegionButton.isActive = true
                //Enable PR
                receiver.setCanvasAction(ACTION.TB_START_SKETCH_REGION);                
                //notificationPR.open()
            }
        }
        Popup {
            id: notificationPR
            x: 100
            y: 100
            width: 250
            height: 200
            modal: true
            focus: true
            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

            Text {
                text: "<b>Press and Hold</b> <i> the region you want to select!</i>"
            }
        }
    }

    Slider {
        id: regionOpacity
        visible: preserveRegionButton.isActive ? true : false
        implicitWidth: 100
        value: 0.4
        stepSize: 0.05
        to: 1
        from: 0
        snapMode: Slider.SnapAlways
        orientation: Qt.Horizontal

        onValueChanged: {
            receiver.setSketchRegionOpacity(regionOpacity.value)
        }
    }//end: Slider

    ToolSeparator {}

    ButtonToolBar {
        id: flattening
        text: "FLAT"
        enabled: false
        implicitWidth: screenDpi.getDiP(80)
        //hasIcon: true
        //icon.source: "/images/icons/flattening.png"
        myToolTipText.text: "Flattening"

        onClicked: {
            if(flattening.isActive) {
                flattening.isActive = false;
                receiver.setCanvasAction(ACTION.TB_STOP_FLATTENING);
                receiver.setLastCanvasAction();

                newVizHandler.flat_mode = false;
                newVizHandler.stopFlattening()
            } else {
                flattening.isActive = true;
                receiver.setCanvasAction(ACTION.TB_START_FLATTENING);

                newVizHandler.flat_mode = true;
            }
        }
    }

    ToolSeparator {}

    ButtonToolBar {
        id: removeAboveButton
        isBordered: true
        text: "RA"
        myToolTipText.text: "Remove Above"

        onClicked: {
            model3DHandler.removeAbove();
            receiver.setRulesButtonOff();
            newVizHandler.setCurrentRuleOperator(GEOLOGICRULES.RA) 
            removeAboveButton.isActive = !removeAboveButton.isActive;
        }
    }

    ButtonToolBar {
        id: removeBellowButton
        isBordered: true
        text: "RB"
        myToolTipText.text: "Remove Below"

        onClicked: {
            model3DHandler.removeBelow();
            receiver.setRulesButtonOff();
            newVizHandler.setCurrentRuleOperator(GEOLOGICRULES.RB)
            removeBellowButton.isActive = !removeBellowButton.isActive;
        }
    }

    ButtonToolBar {
        id: removeAboveIntersectionButton
        isBordered: true
        text: "RAI"
        myToolTipText.text: "Remove Above Intersection"

        onClicked: {
            model3DHandler.removeAboveIntersection();
            receiver.setRulesButtonOff();
            
            removeAboveIntersectionButton.isActive = !removeAboveIntersectionButton.isActive;
            newVizHandler.setCurrentRuleOperator(GEOLOGICRULES.RAI)
        }
    }

    ButtonToolBar {
        id: removeBellowIntersectionButton
        isBordered: true
        text: "RBI"
        myToolTipText.text: "Remove Below Intersection"

        onClicked: {
            model3DHandler.removeBelowIntersection();
            receiver.setRulesButtonOff();
            newVizHandler.setCurrentRuleOperator(GEOLOGICRULES.RBI)
            removeBellowIntersectionButton.isActive = !removeBellowIntersectionButton.isActive;
        }
    }

    ToolSeparator {}

    ButtonToolBar {
        id: directionWidth
        isBordered: true
        text: "N-S"
        implicitWidth: screenDpi.getDiP(80)
        myToolTipText.text: "North-South"
        enabled: true//false        

        onClicked: {
            canvasHandler.direction = DIRECTION.WIDTH;
            canvasHandler.isLength = false;
            canvasHandler.isWidth = true;
            canvasHandler.isHeight = false;
            canvasHandler.updateSketchDataStructure();
            canvasHandler.updateIsSurfaceCreated();
            newVizHandler.setPlaneOrientation(DIRECTION.WIDTH, canvasHandler.crossSection);
            //- Reset SR button
            //preserveRegionButton.isActive = false
            //receiver.setCanvasAction(ACTION.TB_STOP_SKETCH_REGION);
            //receiver.setLastCanvasAction();
        }
    }

    ButtonToolBar {
        id: directionLength
        isBordered: true
        text: "W-E"
        implicitWidth: screenDpi.getDiP(80)
        myToolTipText.text: "West-East"
        isActive: true

        onClicked: {
            canvasHandler.direction = DIRECTION.LENGTH;
            canvasHandler.isLength = true;
            canvasHandler.isWidth = false;
            canvasHandler.isHeight = false;
            canvasHandler.updateSketchDataStructure();
            canvasHandler.updateIsSurfaceCreated();
            newVizHandler.setPlaneOrientation(DIRECTION.LENGTH, canvasHandler.crossSection);
            //- Reset SR button
            //preserveRegionButton.isActive = false
            //receiver.setCanvasAction(ACTION.TB_STOP_SKETCH_REGION);
            //receiver.setLastCanvasAction();
        }
    }

    ButtonToolBar {
        id: directionHeight
        isBordered: true
        text: "Map"
        implicitWidth: screenDpi.getDiP(80)
        myToolTipText.text: "Map View"
        enabled: true//false

        onClicked: {
            canvasHandler.direction = DIRECTION.HEIGHT;
            canvasHandler.isHeight = true;
            canvasHandler.updateSketchDataStructure();
            canvasHandler.updateIsSurfaceCreated();
            newVizHandler.setPlaneOrientation(DIRECTION.HEIGHT, canvasHandler.crossSection);
            //- Reset SR button
            //preserveRegionButton.isActive = false
            //receiver.setCanvasAction(ACTION.TB_STOP_SKETCH_REGION);
            //receiver.setLastCanvasAction();
        }
    }

    ToolSeparator {}

    
    SurfaceTypeSwitch {
        spacing: parent.spacing
    }
    
    ToolSeparator {}

    Connections {
        target: receiver //the C++ Object
        
        onRemoveRuleEnabled: {
            //console.log("The rule that has been enabled is: " + removeRuleId)
        }

        onRulesButtonDisabled: {
            ////console.log("Bingo toggle rules buttons")
            //Disable RA button
            if(removeAboveButton.isActive)
                removeAboveButton.isActive = !removeAboveButton.isActive;
            //Disable RB button
            if(removeBellowButton.isActive)
                removeBellowButton.isActive = !removeBellowButton.isActive;
            //Disable RAI button
            if(removeAboveIntersectionButton.isActive)
                removeAboveIntersectionButton.isActive = !removeAboveIntersectionButton.isActive;
            //Disable RBI button
            if(removeBellowIntersectionButton.isActive)
                removeBellowIntersectionButton.isActive = !removeBellowIntersectionButton.isActive;

        }

        onPreserveButtonDisabled: {
            ////console.log("Bingo toggle preserve buttons")
            //Disable PR button
            if(preserveRegionButton.isActive)
                preserveRegionButton.isActive = !preserveRegionButton.isActive;
            //Disable PA button
            if(preserveAboveButton.isActive)
                preserveAboveButton.isActive = !preserveAboveButton.isActive;
            //Disable PB button
            if(preserveBelowButton.isActive)
                preserveBelowButton.isActive = !preserveBelowButton.isActive;
       }
    }

    Connections {
        target: canvasHandler

        onDirectionChanged: {
            directionWidth.isActive = false;
            directionLength.isActive = false;
            directionHeight.isActive = false;
            if(canvasHandler.direction === DIRECTION.WIDTH)
                directionWidth.isActive = true;
            else if(canvasHandler.direction === DIRECTION.LENGTH)
                directionLength.isActive = true;
            else if(canvasHandler.direction === DIRECTION.HEIGHT)
                directionHeight.isActive = true;
        }

        onUpdateDirection: {
            directionWidth.enabled = false;
            directionLength.enabled = false;
            directionHeight.enabled = false;
            if(canvasHandler.direction === DIRECTION.WIDTH)
                directionWidth.enabled = true;
            else if(canvasHandler.direction === DIRECTION.LENGTH)
                directionLength.enabled = true;
            else if(canvasHandler.direction === DIRECTION.HEIGHT)
                directionHeight.enabled = true;
        }

        onEnableDirection: {
            directionWidth.enabled = true;
            directionLength.enabled = true;
            directionHeight.enabled = true;
        }

        onSurfaceCreated: {
            preserveRegionButton.enabled = response;
            flattening.enabled = response;
        }

        onResetNew: {
            root.reset();
        }
    }

    Connections {
        target: newVizHandler

        onModelLoadingCompleted: {
            preserveRegionButton.enabled = true;
            flattening.enabled = true;
        }
    }

    
}//End RowLayOut
