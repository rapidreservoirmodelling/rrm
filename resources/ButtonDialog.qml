import QtQuick 2.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0

Button {
    id: dialogButton
    property color backgroundDefaultColor: BT_DIALOG.BACKGROUND_DEFAULT_COLOR  
    property color backgroundPressedColor: Qt.darker(BT_DIALOG.BACKGROUND_COLOR_DOWN, 1.2)
    property color contentItemTextColor: "white"

    //implicitWidth: screenDpi.getDiP(BT_DIALOG.BUTTON_WIDTH)
    //implicitHeight: screenDpi.getDiP(BT_DIALOG.BUTTON_HEIGHT)

    width:  screenDpi.getDiP(BT_DIALOG.BUTTON_WIDTH) //Math.max(screenDpi.getDiP(BT_DIALOG.BUTTON_WIDTH), implicitWidth)
    height: Math.max(screenDpi.getDiP(BT_DIALOG.BUTTON_WIDTH), screenDpi.getDiP(BT_DIALOG.BUTTON_HEIGHT))

    text: "Button"

    contentItem: Text {
        text: dialogButton.text
        color: dialogButton.contentItemTextColor
        font.pixelSize: screenDpi.getDiP(BT_DIALOG.BUTTON_FONT_PIXEL_SIZE) 
        font.family: "Arial"
        font.weight: Font.Thin
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle {
        implicitWidth: dialogButton.implicitWidth 
        implicitHeight: dialogButton.implicitHeight 
        color: dialogButton.down ? dialogButton.backgroundPressedColor : dialogButton.backgroundDefaultColor
        radius: BT_DIALOG.BORDER_RADIUS 
        
        border.color: dialogButton.down ? BT_DIALOG.BORDER_COLOR_DOWN : BT_DIALOG.BORDER_COLOR_UP 

        layer.enabled: true
        layer.effect: DropShadow {
            transparentBorder: true
            color: dialogButton.down ? BT_DIALOG.BORDER_COLOR_DOWN : BT_DIALOG.BORDER_COLOR_UP 
            samples: screenDpi.getDiP(BT_DIALOG.BUTTON_DROPSHADOW_SAMPLE_SIZE)
        }
    }
}
