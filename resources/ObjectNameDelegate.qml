import QtQuick 2.12
import QtQuick.Controls 2.1
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12

import Constants 1.0


Item {
    id: treeItem

    property var selectedElementSource
    property var selectedElementDestinationIndex
    property var selectedElementDestination
    property var selectedTreeItem

    //property bool flatMode: false
    z: 3000

    DropArea {
        id: dropArea
        anchors.fill: parent
        onDropped: {

            //console.log("On Dropped0:" + JSON.stringify(styleData.value))

            if(styleData.value.parentTypeId === TreeNodeParentType.DOMAIN_MAIN)
            {
                selectedElementDestination  = styleData.value;

                //console.log("On Dropped1:" + JSON.stringify(selectedElementDestination))

                dragRect.color = OBJECT_TREE.DRAG_SUCCESS_COLOR

                //Adding delay to see the drag success feedback
                timer.setTimeout(function(){
                    newVizHandler.setDestinationDomain( styleData.value.dataId )
                }, 500);

            }
            else if(styleData.value.parentTypeId === TreeNodeParentType.STRATIGRAPHIC) //Surface Merge
            {
                selectedElementDestination  = styleData.value;

                //console.log("On Dropped2:" + JSON.stringify(selectedElementDestination) )

                dragRect.color = OBJECT_TREE.DRAG_SUCCESS_COLOR

                //Adding delay to see the drag success feedback
                timer.setTimeout(function(){
                    newVizHandler.setDestinationSurface( styleData.value.dataId, styleData.value.parentTypeId )
                }, 500);

            }
            else if(styleData.value.text === "Domains")
            {
                //console.log("On Dropped3: " + styleData.value.text)
                timer.setTimeout(function(){
                    newVizHandler.createAndsetDestinationDomain()
                }, 500);
                dragRect.color = OBJECT_TREE.DRAG_SUCCESS_COLOR
            }
            else{

                //console.log("On Dropped Nothing: Not allowed")

            }

            timer.setTimeout(function(){
                dragRect.color = "transparent"
            }, 300);
        }
        onEntered: {
            dragRect.color = OBJECT_TREE.DRAG_ENTER_COLOR
            //selectedElementSource  = styleData.value;
            if(COMMENTS.DEV_FAZILA) {
                ////console.log("On Entered1:" + JSON.stringify(selectedElementSource))
                ////console.log("On Entered2:" + selectedElement.type + ", " + selectedElement.size)
            }

        }
        onExited: {
            dragRect.color = "transparent"
        }
    }

    Rectangle {
        id: dragRect
        color: "transparent"//"#454545"
        width: parent.width
        height: screenDpi.getDiP(30)
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter


        Text {
            anchors.fill: parent
            color: styleData.textColor
            elide: styleData.elideMode
            //text: styleData.value.indentation + ": " + styleData.value.text
            text:  qsTr("%1").arg(styleData.value.text) //styleData.value.text
            font.underline : (newVizHandler.flat_mode===true && styleData.value.parentTypeId===1 && styleData.value.flattened === true) ? true : false
            font.bold : (newVizHandler.flat_mode===true && styleData.value.parentTypeId===1 && styleData.value.flattened === true) ? true : false
        }

        function longPressAction ()
        {
            //console.log("ObjectName - On PressAndHold:" + JSON.stringify( styleData.value))
            var data = styleData.value
            //console.log("ObjectName - On PressAndHold:" + JSON.stringify( data))
            treeItem.selectedTreeItem = data
            //console.log("ObjectName - On PressAndHold:" + JSON.stringify(treeItem.selectedTreeItem))

            if((data.parentTypeId ===0) && (data.text ==="Domains")) {
                //overlay.addDomain.treeItem = treeItem.selectedTreeItem;
                //overlay.addDomain.open();
            } else if ((data.parentTypeId !== 0) && (data.parentTypeId !== 3) && (data.parentTypeId !== 5)) {
                overlay.itemProperties.treeItem = treeItem.selectedTreeItem;
                overlay.itemProperties.open();
            } else if (data.parentTypeId === 5) {
                overlay.itemProperties.treeItem = treeItem.selectedTreeItem;
                overlay.itemProperties.open();
            } else if (data.parentTypeId === 3) {
                overlay.itemProperties.treeItem = treeItem.selectedTreeItem;
                overlay.itemProperties.open();
            }
        }

        MouseArea{
            id:rowMouseArea
            anchors.fill: parent
            hoverEnabled: true
            property  bool hovered : false
            acceptedButtons: Qt.LeftButton | Qt.RightButton

            drag.target: dragRect
            drag.onActiveChanged: {
                if (rowMouseArea.drag.active) {
                    //dragIndex = styleData.index;

                    //console.log("On ActiveChanged0:" + JSON.stringify(styleData.value) )

                    switch(styleData.value.parentTypeId) {

                    case TreeNodeParentType.STRATIGRAPHIC_CHILD:
                        selectedElementSource  = styleData.value;
                        //console.log("On ActiveChanged1:" + JSON.stringify(selectedElementSource))
                        newVizHandler.setSourceSurface(selectedElementSource.parentId, selectedElementSource.dataId, styleData.value.parentTypeId)

                        break;

                    case TreeNodeParentType.STRATIGRAPHIC:
                        selectedElementSource  = styleData.value;
                        //console.log("On ActiveChanged1:" + JSON.stringify(selectedElementSource))
                        newVizHandler.setSourceSurface(selectedElementSource.parentId, selectedElementSource.dataId, styleData.value.parentTypeId)

                        break;


                    case TreeNodeParentType.REGION:
                        selectedElementSource  = styleData.value;
                        //console.log("On ActiveChanged2:" + JSON.stringify(selectedElementSource))
                        newVizHandler.setSourceRegion(selectedElementSource.dataId, styleData.value.parentTypeId)

                        break;

                    case TreeNodeParentType.DOMAIN_MAIN:
                        selectedElementSource  = styleData.value;
                        //console.log("On ActiveChanged3:" + JSON.stringify(selectedElementSource))
                        ////console.log("Dragging from Domain:" + JSON.stringify(selectedElementSource))

                        //newVizHandler.setSourceRegion(selectedElementSource.dataId, styleData.value.parentTypeId)
                        //newVizHandler.removeFromSourceDomain(selectedElementSource.dataId, styleData.value.parentTypeId)

                        break;

                    case TreeNodeParentType.DOMAIN_CHILD:
                        selectedElementSource  = styleData.value;
                        //console.log("On ActiveChanged4:" + JSON.stringify(selectedElementSource))
                        //console.log("Dragging from Domain:" + JSON.stringify(selectedElementSource))

                        newVizHandler.setRegionForRemovalFromDomain(selectedElementSource.parentId, styleData.value.parentTypeId, styleData.value.dataId)

                        break;

                    default:
                        //console.log("On ActiveChanged5: Not allowed")
                    }//end:Switch

                }//end: if (rowMouseArea.drag.active)

                dragRect.Drag.drop();
            }

            onPressAndHold: {
                dragRect.longPressAction()
            }

            onClicked: {
                //dragRect.longPressAction()
                //console.log("ObjectName - onClicked:" + JSON.stringify( styleData.value) + ", flat_mode=" + newVizHandler.flat_mode)
                //dragRect.color = "green"

                if(newVizHandler.flat_mode){
                    newVizHandler.setFlattenedSurfaceId(styleData.value.dataId)
                }
                else dragRect.longPressAction()
            }

            TapHandler {
                acceptedDevices: PointerDevice.TouchScreen |  PointerDevice.Stylus;
                onLongPressed:{
                    if(COMMENTS.DEV_FAZILA) {
                        //console.log("Long tap")
                    }
                    dragRect.longPressAction()
                }

            }//end: TapHandler
        }//end: MouseArea

        states: [
            State {
                when: dragRect.Drag.active
                ParentChange {
                    target: dragRect
                    parent: rrmTree; width: dragRect.width; height: dragRect.height
                }

                AnchorChanges {
                    target: dragRect
                    anchors.horizontalCenter: undefined
                    anchors.verticalCenter: undefined
                }
            }
        ]
        Drag.active: rowMouseArea.drag.active
        Drag.supportedActions: Qt.CopyAction;
        Drag.hotSpot.x: dragRect.width / 2
        Drag.hotSpot.y: dragRect.height / 2
    }

    Timer {
        id: timer

        function setTimeout(cb, delayTime) {
            timer.interval = delayTime;
            timer.repeat = false;
            timer.triggered.connect(cb);
            timer.triggered.connect(function release () {
                timer.triggered.disconnect(cb);
                timer.triggered.disconnect(release);
            });
            timer.start();
        }
    }//end: Timer

    Connections {
        target: newVizHandler

        onDuplicateItemNameDetected: {
            //console.log("DUPLICATE " + itemPrefix);
            overlay.itemMessage.message = "Duplicate " + itemPrefix + "!\n\nPlease try again."
            overlay.itemMessage.icon = true
            overlay.itemMessage.open()

            //Close popup notification
            timer.setTimeout(function(){
                overlay.itemMessage.close()
                dragRect.color = "transparent"
            }, 3000);
        }

//        onDropToDomainReady: {
//            //newVizHandler.dropToDomain()
//        }

        onDuplicateRegionDropDetected: {
            overlay.itemMessage.message = source + " already belongs to " + destination + "!\n\nPlease try again."
            overlay.itemMessage.icon = true
            overlay.itemMessage.open()

            //Close popup notification
            timer.setTimeout(function(){
                overlay.itemMessage.close()
                dragRect.color = "transparent"
            }, 3000);
        }

        onItemDescriptionUpdated: {
            overlay.itemProperties.close();
        }

        onDropToDomainNotAllowed: {
            overlay.itemMessage.message = "Copy not allowed" + "!\n\nPlease try again."
            overlay.itemMessage.icon = true
            overlay.itemMessage.open()

            //Close popup notification
            timer.setTimeout(function(){
                overlay.itemMessage.close()
                dragRect.color = "transparent"
            }, 3000);
        }


    }//end:Connections VizHandler

    Connections {
        target: theModel

        onDomainNodesUpdated: {
            overlay.itemProperties.close();
            overlay.addDomain.close();
            overlay.itemMessage.close();
        }

    }//end:Connections TreeModel

}//end: item
