import QtQuick 2.12
import QtQuick.Controls 2.12

Loader {
        anchors.fill: parent
        source: (styleData.value.parentTypeId ==0) ? "GroupCheckBoxDelegate.qml" : "SingleCheckBoxDelegate.qml" 
        active: true
}