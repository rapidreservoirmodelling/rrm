import QtQuick 2.12
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

//Dialog box allows the user to set Bounding box dimensions

Dialog {
    id: myOpenDialog
    
    anchors.centerIn: parent
    width: screenDpi.getDiP(OPEN_DIALOG.WIDTH)
    height: screenDpi.getDiP(OPEN_DIALOG.HEIGHT)

    x: screenDpi.getDiP((parent.width - width) / 2)
    y: screenDpi.getDiP(((parent.height - height) / 2)-30)
    
    modal: true
    closePolicy: Dialog.NoAutoClose
    z: 1000

    function dialogInputAction() {
        model3DHandler.setSize(canvasWidthTextField.text, canvasLengthTextField.text, canvasHeightTextField.text);
        model3DHandler.setOrigin(0, 0, 0);

        frontViewLoader.sourceComponent = null;
        frontViewLoader.source = "FrontView.qml"
        frontViewLoader.active = true;

        topViewLoader.sourceComponent = null;
        topViewLoader.source = "TopView.qml"
        topViewLoader.active = true;

        newVizHandler.changeDimension();
        newVizHandler.setCrossSection(main.model_discretization);

        main.setupModel();
    }

    onAccepted:{
        dialogInputAction()
    }

    onRejected: {
        dialogInputAction()
    }

    header : Rectangle {
        height: screenDpi.getDiP(0.25 * parent.height)
        width: screenDpi.getDiP(parent.width)
        color: "#EEEEEE"

        Image {
            id: iconHolder
            anchors.centerIn: parent
            fillMode: Image.PreserveAspectFit
            height: parent.height
            source: "/images/icons/rrm.png"
        }
    }

    footer: Rectangle {
        height: screenDpi.getDiP(0.15 * parent.height)
        width: screenDpi.getDiP(parent.width)
        color: "#EEEEEE"

        Button {
            text: qsTr("OK")
            anchors.centerIn: parent
            anchors.margins: screenDpi.getDiP(15)
            Material.foreground: screenDpi.getDiP(OPEN_DIALOG.BUTTON_FONT_COLOR)
            highlighted: true
            font.pixelSize: screenDpi.getDiP(OPEN_DIALOG.BUTTON_FONT_PIXEL_SIZE)
            width: screenDpi.getDiP(OPEN_DIALOG.BUTTON_WIDTH)
            height: screenDpi.getDiP(OPEN_DIALOG.BUTTON_HEIGHT)

            onClicked: {
                dialogInputAction()
                myOpenDialog.close()
            }
        }
    }//end:footer

    contentItem: ColumnLayout {
        id: column
        
        Layout.preferredHeight: screenDpi.getDiP(0.7 * parent.height)
        Layout.preferredWidth: screenDpi.getDiP(0.8 * parent.width)

        Label {
            text: "<b>Please</b> set the dimensions:"
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            font.weight: Font.ExtraBold
            font.pixelSize: screenDpi.getDiP(OPEN_DIALOG.FORM_ELEMENT_FONT_PIXEL_SIZE + 1)
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter
            Layout.maximumHeight: 0.1 * parent.height
            Layout.maximumWidth: parent.width

            Label {
                text: "Width (X; West->East):"
                Layout.alignment: Qt.AlignRight
                font.weight: Font.ExtraBold
                font.pixelSize: screenDpi.getDiP(OPEN_DIALOG.FORM_ELEMENT_FONT_PIXEL_SIZE)
            }

            Slider {
                id: sliderWidth
                value: 1000.0
                stepSize: 10.0
                from: 10.0
                to: 10000.0
                width: 500
                Layout.alignment: Qt.AlignBaseline | Qt.AlignRight
            }

            TextField {
                id: canvasWidthTextField
                text: sliderWidth.value
                font.weight: Font.ExtraBold
                font.pixelSize: screenDpi.getDiP(OPEN_DIALOG.FORM_ELEMENT_FONT_PIXEL_SIZE)
                onTextChanged: sliderWidth.value = canvasWidthTextField.text
                validator: IntValidator {bottom: sliderWidth.from; top: sliderWidth.to;}
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter
            Layout.maximumHeight: 0.1 * parent.height
            Layout.maximumWidth: parent.width

            Label {
                text: "Length (Y; South->North):"
                Layout.alignment: Qt.AlignRight
                font.weight: Font.ExtraBold
                font.pixelSize: screenDpi.getDiP(OPEN_DIALOG.FORM_ELEMENT_FONT_PIXEL_SIZE)
            }

            Slider {
                id: sliderLength
                value: 1000.0
                stepSize: 10.0
                from: 10.0
                to: 10000.0
                width: 500
                Layout.alignment: Qt.AlignBaseline | Qt.AlignRight
            }

            TextField {
                id: canvasLengthTextField
                text: sliderLength.value
                font.weight: Font.ExtraBold
                font.pixelSize: screenDpi.getDiP(OPEN_DIALOG.FORM_ELEMENT_FONT_PIXEL_SIZE)
                onTextChanged: sliderLength.value = canvasLengthTextField.text
                validator: IntValidator {bottom: sliderLength.from; top: sliderLength.to;}
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignRight
            Layout.maximumHeight: 0.1 * parent.height
            Layout.maximumWidth: parent.width

            Label {
                text: "Height (Z):                  "
                Layout.alignment: Qt.AlignRight
                font.weight: Font.ExtraBold
                font.pixelSize: screenDpi.getDiP(OPEN_DIALOG.FORM_ELEMENT_FONT_PIXEL_SIZE)
            }

            Slider {
                id: sliderHeight
                value: 200.0
                stepSize: 10.0
                from: 10.0
                to: 10000.0
                width: 500
                Layout.alignment: Qt.AlignBaseline | Qt.AlignRight
            }

            TextField {
                id: canvasHeightTextField
                text: sliderHeight.value
                font.weight: Font.ExtraBold
                font.pixelSize: screenDpi.getDiP(OPEN_DIALOG.FORM_ELEMENT_FONT_PIXEL_SIZE)
                onTextChanged: sliderHeight.value = canvasHeightTextField.text
                validator: IntValidator {bottom: sliderHeight.from; top: sliderHeight.to;}
            }

        }//End:RowLayout

    }//End:ColumnLayout

}//End: Dialog
