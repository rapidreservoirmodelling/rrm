import QtQuick 2.12
import QtQuick.Controls 2.12

import "scripts/window.js" as WindowScript
import "scripts/promise.js" as Q

import Constants 1.0

Rectangle {
    id: root
    anchors.fill: parent
    clip: true
    color: "white"//"gray"

    Timer {
        id: timer2
        function setTimeout(cb, delayTime) {
            timer2.interval = delayTime;
            timer2.repeat = false;
            timer2.triggered.connect(cb);
            timer2.triggered.connect(function release () {
                timer2.triggered.disconnect(cb);
                timer2.triggered.disconnect(release);
            });
            timer2.start();
        }
    }//end: Timer


    Popup {
        id: vizOnNotification
        x: 200
        y: 100
        width: screenDpi.getDiP(350)
        height: screenDpi.getDiP(100)
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
        Text {
            text: "<b>Turn off the viz button </b> <i> and try again!</i>"
        }
    }//end: Popup


    Popup {
        id: previewOnNotification
        x: 200
        y: 100
        width: screenDpi.getDiP(400)
        height: screenDpi.getDiP(100)
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
        Text {
            text: "<b>Turn off the PREVIEW button </b> <i> to sketch again!</i>"
        }
    }//end: Popup


    Popup {
        id: progressNotifierPathGuided
        anchors.centerIn: parent
        width: 120//250
        height: 90//100
        modal: true
        focus: true
        closePolicy: Popup.NoAutoClose//Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

        BusyIndicator {
            running: true

            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                opacity: 0.3
                color: "white"//"lightgreen"
            }
        }
    }//end: Popup


    ScrollView {
        id: view
        anchors.fill: parent
        anchors.bottomMargin: screenDpi.getDiP(20)
        anchors.rightMargin: screenDpi.getDiP(5)
        contentWidth: myCanvas.width
        contentHeight: myCanvas.height

        ScrollBar.vertical: ScrollBar {
            id: scrollBarV
            parent: view.parent
            policy: ScrollBar.AlwaysOn
            anchors.top: view.top
            anchors.right: view.right
            anchors.bottom: view.bottom
            height: view.availableHeight
            opacity: 0.5

            contentItem: Rectangle {
                width: screenDpi.getDiP(15)
                height: screenDpi.getDiP(15)
                anchors.rightMargin: screenDpi.getDiP(10)
                radius: screenDpi.getDiP(16)
                color: "#74d0f0"
            }
        }//end: ScrollBar vertical

        ScrollBar.horizontal: ScrollBar {
            id: scrollBarH
            parent: view.parent
            policy: ScrollBar.AlwaysOn
            anchors.right: view.right
            anchors.left: view.left
            anchors.bottom: view.bottom
            width: view.availableWidth
            opacity: 0.5

            contentItem: Rectangle {
                width: screenDpi.getDiP(15)
                height: screenDpi.getDiP(15)
                anchors.bottomMargin: screenDpi.getDiP(10)
                radius: screenDpi.getDiP(16)
                color: "#74d0f0"
            }
        }//end: ScrollBar horizontal


        Canvas {
            id: myCanvas
            anchors.centerIn: parent
            width: root.width * 1.1 * myFunctions.zoomFactor
            height: root.height * 1.1 * myFunctions.zoomFactor * canvasHandler.veFactor

            property var ctx // 2D context

            state: "SKETCH"

            states: [
                State {
                    name: "SKETCH"
                    PropertyChanges { target: floatingButton; visible: true }
                    PropertyChanges { target: frontViewDrawer; visible: true }
                    PropertyChanges { target: imgHere; visible: false }
                },
                State {
                    name: "REGION"
                    PropertyChanges { target: floatingButton; visible: false }
                    PropertyChanges { target: frontViewDrawer; visible: false }
                    PropertyChanges { target: imgHere; visible: false }
                }
            ]

            FrontSketchFunctions {
                id: myFunctions
            }


            Component.onCompleted: {
                myCanvas.state = "SKETCH";
                myFunctions.updateScale();
                myFunctions.updateResize();
                myCanvas.requestPaint();
                receiver.setCanvasAction(ACTION.FB_SKETCH);
                canvasHandler.crossSection = model3DHandler.length_Discretization;
            }


            onCanvasSizeChanged: {
                myFunctions.updateResize();
                if(myFunctions.guidelines && !myFunctions.glMoving)
                {
                    myFunctions.glMouseX = canvasHandler.getFV_gl_mouseX();
                    myFunctions.glMouseY = canvasHandler.getFV_gl_mouseY();
                }
                myCanvas.requestPaint();
            }

            //- Border of the Canvas
            Rectangle {
                anchors.fill: parent
                color: "white"
                border.color: "white"
                border.width: 3
                z:-10
            }

            CrossSection_WE {
                id: csWE
                m_origin_x: canvasHandler.csWE_Origin_x
                m_origin_y: canvasHandler.csWE_Origin_y
                m_width: myFunctions.csWE_Width
                m_height: myFunctions.csWE_Height
                m_scale: canvasHandler.csWE_Scale
                visible: canvasHandler.isLength
                isMap: canvasHandler.isHeight ? true : false
                z: -1
            }

            CrossSection_NS {
                id: csNS
                m_origin_x: canvasHandler.csNS_Origin_x
                m_origin_y: canvasHandler.csNS_Origin_y
                m_width: myFunctions.csNS_Width
                m_height: myFunctions.csNS_Height
                m_scale: canvasHandler.csNS_Scale
                visible: canvasHandler.isWidth
                isMap: canvasHandler.isHeight ? true : false
                z: -1
            }

            Image {
                id: imgHere
                source: "/images/icons/icon_here.png"
                width: screenDpi.getDiP(0.1 * 345)
                height: screenDpi.getDiP(0.1 * 570)
                visible: false
            }

            Connections {
                target: overlay.regionMenu
                onRegionMenuClosed: {
                    imgHere.visible = false;
                }
            }


            onPaint: {
                myCanvas.ctx = getContext('2d');

                WindowScript.clearCanvas( myCanvas.ctx, myCanvas.width, myCanvas.height);

                if(model3DHandler.isPreserveRegionActive()) {
                    WindowScript.fillSelectedRegion(myCanvas.ctx, myFunctions.selectedRegion, "#8ef0ad", myFunctions.alpha_SR);
                }

                WindowScript.draw( myCanvas.ctx, myFunctions.sketchRegion );

                if(newVizHandler.getVizFlag()) {
                    if(newVizHandler.region_visibility) {
                        //console.log("FrontSketchWindow 1: " +  JSON.stringify(myFunctions.regionCurveBox))
                        WindowScript.drawRegionPolygonUsingCustomMapData(myCanvas.ctx, myFunctions.regionCurveBox, myFunctions.regionColor, myFunctions.regionVisibility);
                        //console.log("FrontSketchWindow 1: FINISHED")
                    }
                    if(newVizHandler.domain_visibility) {
                        //console.log("FrontSketchWindow 2: " +  JSON.stringify(myFunctions.regionCurveBox))
                        WindowScript.drawDomainPolygonV2(myCanvas.ctx, myFunctions.regionCurveBox, myFunctions.domain_map);
                    }
                }

                WindowScript.displaySubmittedCurvesUsingObjectMapping(myFunctions.submittedCurves, myCanvas.ctx, myFunctions.colorMap, myFunctions.visibilityMap, canvasHandler.isHeight, myFunctions.highlightFlatt );

                if(canvasHandler.highlight_FV > -1 || myFunctions.selectSketch)
                    WindowScript.drawWithHighlight( myCanvas.ctx, myFunctions.curve);
                else
                    WindowScript.draw( myCanvas.ctx, myFunctions.curve);

                WindowScript.draw( myCanvas.ctx, myFunctions.temp_curve );

                if (canvasHandler.direction === DIRECTION.LENGTH || canvasHandler.direction === DIRECTION.WIDTH) {
                    if(myFunctions.guidelines) {
                        WindowScript.showGuidelines( myCanvas.ctx, myFunctions.glMouseX, myFunctions.glMouseY, myCanvas.width, myCanvas.height, CANVAS_ID.FRONT_VIEW );
                        if(canvasHandler.veAngleVisible) {
                            WindowScript.showGuidelinesAngle( myCanvas.ctx, myFunctions.glMouseX, myFunctions.glMouseY, myCanvas.width, myCanvas.height, CANVAS_ID.FRONT_VIEW, canvasHandler.veAngle, canvasHandler.veFactor );
                        }
                    }
                    if(myFunctions.sketchLine) {
                        WindowScript.showSketchLine( myCanvas.ctx, myFunctions.glMouseY, myCanvas.width);
                    }
                } else if (canvasHandler.direction === DIRECTION.HEIGHT ) {
                    if(canvasHandler.isLength) {
                        WindowScript.showCrossSectionGuidelineWE( myCanvas.ctx, myFunctions.glHeightWE, canvasHandler.csWE_Origin_x, myFunctions.csWE_Width, myFunctions.csIDPosition );
                    } else if (canvasHandler.isWidth) {
                        WindowScript.showCrossSectionGuidelineWE( myCanvas.ctx, myFunctions.glHeightNS, canvasHandler.csNS_Origin_x, myFunctions.csNS_Width, myFunctions.csIDPosition );
                    }
                }

                if(newVizHandler.getPreviewFlag()) {
                    WindowScript.drawSingleCurveWithColorAndDots(myCanvas.ctx, myFunctions.previewCurve,  myFunctions.previewCurveColor);
                }
                                
            }//end: onPaint


            PinchArea {
                id: myPinch
                anchors.fill: parent
                pinch.target: myCanvas
                enabled: false
                pinch.maximumScale: 10.0
                pinch.minimumScale: 1.0
                pinch.maximumRotation: 0 //360
                pinch.minimumRotation: 0 //-360
                pinch.dragAxis: Pinch.XAndYAxis


                MouseArea {
                    id: myMouseArea
                    anchors.fill: parent
                    hoverEnabled: true
                    pressAndHoldInterval: 500 //reducing the interval from the default 800ms
                    preventStealing: true

                    acceptedButtons: Qt.AllButtons
                    property int mouseButtonIdentifier: 0


                    onClicked: {
                        if(myCanvas.state === "SKETCH") {
                            if(canvasHandler.canvasAction === ACTION.FB_FIX_GUIDELINES) {
                                myFunctions.glMoving = false;
                                myFunctions.glMouseX = mouseX;
                                myFunctions.glMouseY = mouseY;
                                canvasHandler.fixGuidelineFrontView(mouseX,mouseY);
                                if(canvasHandler.direction === DIRECTION.LENGTH) {
                                    receiver.setMouseFromFrontView( mouseX, mouseY, myFunctions.csWE_Width, myFunctions.csWE_Height, canvasHandler.csWE_Origin_x, canvasHandler.csWE_Origin_y );
                                } else if (canvasHandler.direction === DIRECTION.WIDTH) {
                                    receiver.setMouseFromFrontView( mouseX, mouseY, myFunctions.csNS_Width, myFunctions.csNS_Height, canvasHandler.csNS_Origin_x, canvasHandler.csNS_Origin_y );
                                }
                                receiver.fixMapViewGuidelines();
                                myCanvas.requestPaint();
                                receiver.setLastCanvasAction();
                            } else
                            if (mouse.button === Qt.LeftButton) {
                                canvasHandler.delegateFrontViewAction(MOUSE.LEFT, MOUSE.CLICK, mouseX, mouseY);

                                if(newVizHandler.getPreviewFlag() && canvasHandler.isPreview) {
                                    //console.log("FRONTSketchWindow: onClicked: Left_Button")
                                    myFunctions.surfacePreviewAction();
                                } else {
                                    canvasHandler.isPreview = false;
                                }
                                myCanvas.requestPaint();
                            }
                        } else if(myCanvas.state === "REGION") {
                            if (mouse.button === Qt.LeftButton) {
                                overlay.regionMenu.clickX = mouseX;
                                overlay.regionMenu.clickY = mouseY;
                                imgHere.x = mouseX - imgHere.width/2;
                                imgHere.y = mouseY - imgHere.height;
                                imgHere.visible = true;
                                canvasHandler.delegateRegionAction(CANVAS_ID.FRONT_VIEW, MOUSE.LEFT, MOUSE.CLICK, mouseX, mouseY);
                            }
                        }
                    }


                    onPressed: {
                        if(myCanvas.state === "SKETCH") {
                            if (mouse.button === Qt.LeftButton) {
                                canvasHandler.delegateFrontViewAction(MOUSE.LEFT, MOUSE.PRESS, mouseX, mouseY);
                                mouseButtonIdentifier = MOUSE.LEFT;

                            } else if (mouse.button === Qt.RightButton) {
                                mouseButtonIdentifier = MOUSE.RIGHT;
                            }
                        }
                    }


                    onPositionChanged: {
                        if(myCanvas.state === "SKETCH") {
                            if( mouseButtonIdentifier === MOUSE.LEFT && myMouseArea.pressed) {
                                canvasHandler.delegateFrontViewAction(MOUSE.LEFT, MOUSE.MOVE, mouseX, mouseY);
                                myCanvas.requestPaint();
                            }

                            if( myFunctions.guidelines && myFunctions.glMoving) {
                                myFunctions.glMouseX = mouseX;
                                myFunctions.glMouseY = mouseY;
                                canvasHandler.fixGuidelineFrontView(mouseX,mouseY);
                                if(canvasHandler.direction === DIRECTION.LENGTH) {
                                    receiver.setMouseFromFrontView( mouseX, mouseY, myFunctions.csWE_Width, myFunctions.csWE_Height, canvasHandler.csWE_Origin_x, canvasHandler.csWE_Origin_y );
                                } else if (canvasHandler.direction === DIRECTION.WIDTH) {
                                    receiver.setMouseFromFrontView( mouseX, mouseY, myFunctions.csNS_Width, myFunctions.csNS_Height, canvasHandler.csNS_Origin_x, canvasHandler.csNS_Origin_y );
                                }
                                myCanvas.requestPaint();
                            }

                            if( myFunctions.sketchLine) {
                                myFunctions.glMouseY = mouseY;
                                myCanvas.requestPaint();
                            }

                            if( mouseButtonIdentifier === MOUSE.NO_BUTTON ) {
                                canvasHandler.delegateFrontViewAction(MOUSE.NO_BUTTON, MOUSE.MOVE, mouseX, mouseY);
                                myCanvas.requestPaint();
                            }
                        }
                    }


                    onReleased: {
                        if(myCanvas.state === "SKETCH") {
                            if( mouseButtonIdentifier === MOUSE.LEFT ) {
                                canvasHandler.delegateFrontViewAction(MOUSE.LEFT, MOUSE.RELEASE, mouseX, mouseY);
                                myFunctions.temp_curve = [];
                                mouseButtonIdentifier = MOUSE.NO_BUTTON;

                                if(newVizHandler.getPreviewFlag() && canvasHandler.isPreview && canvasHandler.canvasAction !== ACTION.FB_FIX_GUIDELINES) {
                                    //console.log("FRONTSketchWindow: onReleased")
                                    myFunctions.surfacePreviewAction();
                                } else {
                                    canvasHandler.isPreview = false;

                                }
                            }

                            canvasHandler.resetHighlightSelection_FV();
                            myCanvas.requestPaint();
                        }
                    }


                    onDoubleClicked: {
                        if(myCanvas.state === "SKETCH") {
                            if( mouseButtonIdentifier === MOUSE.RIGHT ) {
                                myFunctions.highlightFlatt = -1;
                                myMouseArea.submitAction();
                            }
                            mouseButtonIdentifier = MOUSE.NO_BUTTON;
                        }
                    }


                    onPressAndHold: {
                        if(myCanvas.state === "SKETCH") {
                            if( mouseButtonIdentifier === MOUSE.LEFT ) {
                                canvasHandler.delegateFrontViewAction(MOUSE.LEFT, MOUSE.PRESS_AND_HOLD, mouseX, mouseY);
                            }
                        }
                    }

                    onWheel: {
                        if (wheel.angleDelta.y > 0) {
                            myFunctions.zoomFactor += 0.1;
                        } else {
                            myFunctions.zoomFactor -= 0.1;
                            if (myFunctions.zoomFactor < (1.00 / canvasHandler.veFactor))
                                myFunctions.zoomFactor = (1.00 / canvasHandler.veFactor);
                        }
                        if(canvasHandler.flatHeight) {
                            myFunctions.updateFlatteningScale(canvasHandler.flatHeight);
                        } else {
                            myFunctions.updateScale();
                        }
                        myFunctions.updateResize();
                        myCanvas.requestPaint();
                    }

                    TapHandler {
                        acceptedDevices: PointerDevice.TouchScreen |  PointerDevice.Stylus;


                        onSingleTapped:{
                            if(myCanvas.state === "SKETCH") {
                                myMouseArea.submitAction();
                            }
                        }

                        onDoubleTapped:{
                            if(myCanvas.state === "SKETCH") {
                                myMouseArea.submitAction();
                            }
                        }

                        onLongPressed:{
                            ////console.log("Long tap")
                            //This may be used for preview
                            //But, it is in-efficient
                        }

                    }//end: TapHandler


                    function submitAction() {
                        if(canvasHandler.canvasAction === ACTION.TB_START_SKETCH_REGION ||
                                canvasHandler.canvasAction === ACTION.TB_START_FLATTENING) {
                            canvasHandler.delegateFrontViewAction(MOUSE.RIGHT, MOUSE.DOUBLE_CLICK, mouseX, mouseY);
                            myCanvas.requestPaint();
                            receiver.setLastCanvasAction();
                        } else {
                            //If Preview is enabled
                            if(newVizHandler.getPreviewFlag() && canvasHandler.isSubmitAllowed) {
                                newVizHandler.resetPreviewFlag();
                            }
                            myFunctions.rightButtonDoubleClickAction();
                        }
                    }//end: function submitAction()

                }//end: MouseArea

            }//end: PinchArea

        }//end: Canvas

    }//end: ScrollView

}//end: Rectangle
