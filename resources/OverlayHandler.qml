import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    id: root
    anchors.fill: parent

    property alias loadingNotifier: progressNotifier
    property alias regionMenu: popupRegionMenu
    property alias itemProperties: singleItemDialog
    property alias itemMessage: singleItemMessage
    property alias addDomain: domainAddDialog

    //- Close all overlays
    function closeAll() {
        progressNotifier.close();
        popupRegionMenu.close();
        singleItemDialog.close();
        singleItemMessage.close();
        domainAddDialog.close();
    }

    Popup {
        id: progressNotifier
        anchors.centerIn: parent
        width: parent.width
        height: 150
        modal: true
        focus: true
        closePolicy: Popup.NoAutoClose

        property alias message: title.text

        RowLayout {
            spacing: screenDpi.getDiP(100)
            anchors.centerIn: parent
            Text {
                id: title
                text: "Loading ..."
                font.pixelSize: screenDpi.getDiP(40)
                font.bold: true
                color: "#4e92c7"
                Layout.alignment: Layout.horizontalCenter
            }
            BusyIndicator {
                running: true
                Layout.alignment: Layout.horizontalCenter
            }
        }
    }

    PopupRegionMenu {
        id: popupRegionMenu
        property int clickX
        property int clickY
    }

    SingleItemDialog {
        id: singleItemDialog
    }

    SingleItemMessage {
        id: singleItemMessage
    }

    DomainAddDialog {
        id: domainAddDialog
    }
}
