import QtQuick 2.12
import QtQuick.Controls 2.1
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12

Popup {
    id: root
    modal: true
    focus: true
    width: screenDpi.getDiP(800)
    height: screenDpi.getDiP(400)
    x: 0
    y: parent.height/2 - height/2
    closePolicy: Popup.NoAutoClose

    property alias message: title.text
    property alias icon: icon.visible

    ColumnLayout {
        spacing: screenDpi.getDiP(30)
        anchors.centerIn: parent
        Rectangle {
            id: icon
            width: screenDpi.getDiP(50)
            height: width
            radius: 0.5 * width
            color: "#990e0e"
            Layout.alignment: Layout.horizontalCenter
            Text {
                text: qsTr("!")
                font.pixelSize: screenDpi.getDiP(40)
                font.bold: true
                color: "white"
                anchors.centerIn: parent
            }
        }

        Label {
            id: title
            text: "Message"
            font.pixelSize: screenDpi.getDiP(22)
            font.bold: false
            color: black
            Layout.alignment: Layout.horizontalCenter
        }
    }
}

