import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0

import "scripts/window.js" as WindowScript
import Constants 1.0

Rectangle {
    id: tools
    anchors.left: parent.right;
    anchors.top: parent.bottom;
    anchors.topMargin: screenDpi.getDiP(100)//160;
    
    property alias numberOfColumns: toolbarItem.gridColumns


    Component.onCompleted: {
        toolbarItem.updateHighlight();
    }


    /*
    BorderImage {
        id: name
        anchors.fill: parent
        source: "/images/icons/drawer_background.png"
        width: screenDpi.getDiP(100)
        height: screenDpi.getDiP(200)
        border.left: 5
        border.top: 5
        border.right: 5
        border.bottom: 5
        horizontalTileMode: BorderImage.Stretch
        verticalTileMode: BorderImage.Stretch
    }
    */


    GridLayout {
        id: toolbarItem
        anchors.fill: parent
        anchors.topMargin: 15;
        anchors.leftMargin: 10;

        property int lastAction: 1
        property int hintSize: 18
        property int gridColumns:2
        property var buttonsHighlight: []

        columns: gridColumns//2

        ColorOverlay {
            color: "white"
        }        

        Connections {
            target: receiver //the C++ Object

            onUpdateHighlight: {
                toolbarItem.updateHighlight();
            }
        }

        function updateHighlight() {
            bfSketch.isActive = receiver.buttonSketch;
            bfSketchLine.isActive = receiver.buttonSketchLine;
            bfSketchCircle.isActive = receiver.buttonSketchCircle;
            bfSketchEllipse.isActive = receiver.buttonSketchEllipse;
            bfTranslate.isActive = receiver.buttonTranslate;
            bfRotate.isActive = receiver.buttonRotate;
            bfScale.isActive = receiver.buttonScale;
            bfGuidelines.isActive = receiver.buttonGuide;
            bfFixGuidelines.isActive = receiver.buttonFixGuide;
            bfZoom.isActive = receiver.buttonZoom;

            if(bfGuidelines.isActive)
                bfFixGuidelines.enabled = true;
            else
                bfFixGuidelines.enabled = false;
        }

        ButtonFloating {
            id: bfSketch
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            isBordered: true
            hasBackground: true
            hasIcon: true
            icon.source: "/images/icons/floating-sketch.png"
            myToolTip.text: "Sketch"

            isActive: receiver.buttonSketch

            onClicked: {
                receiver.setCanvasAction(ACTION.FB_SKETCH);
                receiver.resetFloatingButtons();
                receiver.buttonSketch = true;
                receiver.lastCanvasAction = ACTION.FB_SKETCH;
            }
        }

        ButtonFloating {
            id: bfSketchLine
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            isBordered: true
            hasBackground: true
            hasIcon: true
            icon.source: "/images/icons/floating-sketch-line.png"
            myToolTip.text: "Sketch Line"

            isActive: receiver.buttonSketchLine

            onClicked: {
                receiver.setCanvasAction(ACTION.FB_SKETCH_LINE);
                receiver.resetFloatingButtons();
                receiver.buttonSketchLine = true;
                receiver.lastCanvasAction = ACTION.FB_SKETCH_LINE;
            }
        }


        ButtonFloating {
            id: bfSketchCircle
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            isBordered: true
            hasBackground: true
            hasIcon: true
            icon.source: "/images/icons/floating-circle.png"
            myToolTip.text: "Sketch Circle"
            enabled: canvasHandler.direction === DIRECTION.HEIGHT ? true : false
            isActive: receiver.buttonSketchCircle

            onClicked: {
                receiver.setCanvasAction(ACTION.FB_SKETCH_CIRCLE);
                receiver.resetFloatingButtons();
                receiver.buttonSketchCircle = true;
                receiver.lastCanvasAction = ACTION.FB_SKETCH_CIRCLE;
            }
        }


        ButtonFloating {
            id: bfSketchEllipse
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            isBordered: true
            hasBackground: true
            hasIcon: true
            icon.source: "/images/icons/floating-ellipse.png"
            myToolTip.text: "Sketch Ellipse"
            enabled: canvasHandler.direction === DIRECTION.HEIGHT ? true : false
            isActive: receiver.buttonSketchEllipse

            onClicked: {
                receiver.setCanvasAction(ACTION.FB_SKETCH_ELLIPSE);
                receiver.resetFloatingButtons();
                receiver.buttonSketchEllipse = true;
                receiver.lastCanvasAction = ACTION.FB_SKETCH_ELLIPSE;
            }
        }


        ButtonFloating {
            id: bfTranslate
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            isBordered: true
            hasBackground: true
            hasIcon: true
            icon.source: "/images/icons/floating-translate.png"
            myToolTip.text: "Translate"

            isActive: receiver.buttonTranslate

            onClicked: {
                receiver.setCanvasAction(ACTION.FB_TRANSLATE);
                receiver.resetFloatingButtons();
                receiver.buttonTranslate = true;
                receiver.lastCanvasAction = ACTION.FB_TRANSLATE;
            }
        }

        ButtonFloating {
            id: bfRotate
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            isBordered: true
            hasBackground: true
            hasIcon: true
            icon.source: "/images/icons/floating-rotate.png"
            myToolTip.text: "Rotate"

            isActive: receiver.buttonRotate

            onClicked: {
                receiver.setCanvasAction(ACTION.FB_ROTATE);
                receiver.resetFloatingButtons();
                receiver.buttonRotate = true;
                receiver.lastCanvasAction = ACTION.FB_ROTATE;
            }
        }

        ButtonFloating {
            id: bfScale
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            isBordered: true
            hasBackground: true
            hasIcon: true
            icon.source: "/images/icons/floating-scale.png"
            myToolTip.text: "Scale"

            isActive: receiver.buttonScale

            onClicked: {
                receiver.setCanvasAction(ACTION.FB_SCALE);
                receiver.resetFloatingButtons();
                receiver.buttonScale = true;
                receiver.lastCanvasAction = ACTION.FB_SCALE;
            }
        }

        ButtonFloating {
            id: bfZoom
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            isBordered: true
            hasBackground: true
            hasIcon: true
            icon.source: "/images/icons/floating-zoom.png"
            myToolTip.text: "Zoom"

            isActive: receiver.buttonZoom

            onClicked: {
                receiver.setCanvasAction(ACTION.FB_ZOOM);
                receiver.resetFloatingButtons();
                receiver.buttonZoom = true;
                receiver.lastCanvasAction = ACTION.FB_ZOOM;
            }
        }

        ButtonFloating {
            id: bfGuidelines
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            isBordered: true
            hasBackground: true
            hasIcon: true
            icon.source: "/images/icons/floating-guidelines.png"
            myToolTip.text: "Guidelines"

            isActive: receiver.buttonGuide

            onClicked: {
                receiver.setCanvasVisualizationFV(ACTION.FB_SHOW_GUIDELINES);
                receiver.setCanvasVisualizationMV(ACTION.FB_SHOW_GUIDELINES);
                receiver.setLastCanvasAction();
                receiver.buttonGuide = !receiver.buttonGuide;

                if(bfGuidelines.isActive)
                    bfFixGuidelines.enabled = true;
                else {
                    bfFixGuidelines.enabled = false;
                    bfFixGuidelines.isActive = false;
                }
            }
        }

        ButtonFloating {
            id: bfFixGuidelines
            enabled: false
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            isBordered: true
            hasBackground: true
            hasIcon: true
            icon.source: "/images/icons/floating-fix-guidelines.png"
            myToolTip.text: "Fix Guidelines"

            isActive: receiver.buttonFixGuide

            onClicked: {
                receiver.setCanvasAction(ACTION.FB_FIX_GUIDELINES);
                receiver.buttonFixGuide = !receiver.buttonFixGuide;                
            }
        }

    }//GridLayout

    Connections {
        target: canvasHandler

        onVeAngleVisibleChanged: {
            if(canvasHandler.veAngleVisible) {
                if(!bfGuidelines.isActive) {
                    bfGuidelines.isActive = true;
                    bfFixGuidelines.enabled = true;
                    receiver.setCanvasVisualizationFV(ACTION.FB_SHOW_GUIDELINES);
                    receiver.setCanvasVisualizationMV(ACTION.FB_SHOW_GUIDELINES);
                    receiver.setLastCanvasAction();
                    receiver.buttonGuide = !receiver.buttonGuide;
                }
            }
        }
    }
}//Rectangle
