import QtQuick 2.12
import QtQuick.LocalStorage 2.12

import "scripts/db.js" as ImageScript
import Constants 1.0

DropArea {
    id: myDropArea
    anchors.fill: parent

    property string imagePath: ""

    property int direction: 0

    property bool imageIsVisible: true

    property bool isMapView: false

    
    Component.onCompleted: {
        ImageScript.dbClear()
        ImageScript.dbInit()
        
    }

    Component.onDestruction:{
        ////console.log ("I am done!" + JSON.stringify(ImageScript.readAllRows()) )
        ImageScript.dbClear()
    }

    Connections {
        target: newVizHandler

        onCrossSectionImageRemoved: {
            
            if(myDropArea.direction === directionId) {
                var output = ImageScript.deleteImage(directionId, crossSectionId)
                ////console.log("DB message: " + output)
                if(output === "SUCCESS") {
                    myDropArea.imagePath = ""
                    newVizHandler.completeImageRemoval(directionId, crossSectionId)
                    newVizHandler.disableImageRemoveButton(myDropArea.direction)
                }
            }
        }
    }

    Connections {
        target: receiver
        
        onCrossSectionChanged: {
            ////console.log("Cross-section id from the RECEIVER: " + id + ", DIR: " + canvasHandler.direction)
            //var output = ImageScript.getImage(myDropArea.direction, id)
            var output = ""
            if(myDropArea.direction == 3) {
                output = ImageScript.getMapViewImage(myDropArea.direction)
            } else {
                output = ImageScript.getImage(myDropArea.direction, id)
                ////console.log("ImageDropWindow getImage output: cross-section-id=" +id + " image path:"+ JSON.stringify(output) )
            }

            ////console.log("ImageDropWindow getImage output: "+ output)
            if(output !== "") {
                ////console.log("ImageDropWindow getImage output: cross-section-id=" +id + ", "+ output.absolute_path )
                myDropArea.imagePath = output.absolute_path
            } else {
                myDropArea.imagePath = ""
                newVizHandler.disableImageRemoveButton(myDropArea.direction)
            }
            
            if(myDropArea.imagePath != "") {
                ////console.log("===> CS CHANGED: myDropArea " + myDropArea.direction + ", output: " +  output)
                if(myDropArea.direction === output.direction_id) {
                    newVizHandler.loadCrossSectionImage(canvasHandler.direction, id, myDropArea.imagePath)
                } else {
                    myDropArea.imagePath = ""
                }
            }
        }
    }
    
    onDropped: {
        ////console.log(drop.text) // file path
        myDropArea.imagePath = drop.text

        ////console.log("ImageDropWindow: -----Dropped-----------")
        ////console.log("Dropping to Cross-section id= " + canvasHandler.crossSection +", direction= " + canvasHandler.direction + ", " + myDropArea.imagePath)

        //ImageScript.addImage(canvasHandler.direction, canvasHandler.crossSection, "", myDropArea.imagePath)
        ////console.log("Dropping to Cross-section id= " + canvasHandler.crossSection +", direction= " + myDropArea.direction + ", " + myDropArea.imagePath)
        if(myDropArea.direction === 3) {
            ImageScript.addSameImageForAllMapViewCrossSections(myDropArea.direction, canvasHandler.crossSection, "", myDropArea.imagePath)
        } else {
            ImageScript.addImage(myDropArea.direction, canvasHandler.crossSection, "", myDropArea.imagePath)
        }
        canvasHandler.addCrossSectionUsed(canvasHandler.crossSection, myDropArea.direction);
        newVizHandler.enableImageRemoveButton(myDropArea.direction);
    }

    Item {
        id: main
        width: myDropArea.width
        height: myDropArea.height

        Image {
            id: myImage
            parent: myDropArea
            x: main.x + 5; y: main.y + 5
            width: main.width - 10; height: main.height - 10;
            //fillMode: myDropArea.direction !== 3 ? (canvasHandler.veFactor === 1 ? Image.PreserveAspectFit : Image.Stretch) : Image.PreserveAspectFit            
            //fillMode: Image.PreserveAspectFit
            fillMode: canvasHandler.veBkgImage ?
                          myDropArea.direction !== 3 ?
                              (canvasHandler.veFactor === 1 ? Image.PreserveAspectFit : Image.Stretch)
                            : Image.PreserveAspectFit : Image.PreserveAspectFit
            smooth: true
            source: myDropArea.imagePath
            visible: myDropArea.imageIsVisible

            Rectangle {
                anchors.fill: parent;
                //border.color: "#326487"; border.width: 1
                color: "transparent"; radius: 5
                visible: myImage.state = "active"
                opacity: 1.0
            }
        }
    }
}
