import QtQuick 2.12
import QtQuick.Controls 2.12

import Constants 1.0

Item {
    id: fdFirstPassIntegration
    anchors.fill: parent
    
    Button {
        id: fdButton
        width: parent.width//screenDpi.getDiP(50)//75)
        height: parent.height//screenDpi.getDiP(50)//75)
        
        icon.color: "transparent"
        icon.source: "/images/icons/object-tree-fd.png"
        icon.width: fdButton.width
        icon.height: fdButton.height

        ToolTip {
            visible: fdButton.hovered
            y: fdButton.height
            
            contentItem: Text{
                        text: "Open Flow Diagnostics"
                        font.weight: Font.ExtraBold
                        font.pixelSize: screenDpi.getDiP(16)//32
            }
            background: Rectangle {
                        border.color: "#a6a6a6"
            }

        }
        
        onClicked: {
            if(COMMENTS.DEV_FAZILA) {
                ////console.log("Click FD button " )
            }
            if(vizHandler.fdFlag()===false)
            {
                if(COMMENTS.DEV_FAZILA) {
                    //console.log("Connecting to FD window ..." )
                }
                //Commenting this out for now 
                //This will be needed for ideal integration
                /*var comp = Qt.createComponent("qrc:/resources/FdOnTop.qml");
                var obj1 = comp.createObject(fdFirstPassIntegration, {});
                vizHandler.toggleFdFlag();
                */
                //This is needed for first pass integration of FD
                if(newVizHandler.viz_flag == true)
                    vizHandler.connectToFdInterface();
                
            }
            
        }//end: onClicked

    }//end: Button

}//end: Item

