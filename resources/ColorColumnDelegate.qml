import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.4
import QtQuick.Dialogs 1.2

import Constants 1.0

Item {
    anchors.fill: parent

    property string selectedColor: ""
    property var selectedIndex: ""

    ColorDialog {
        id: colorPicker
        title: "Please choose a color"
        modality: Qt.ApplicationModal

        onAccepted: {
            //console.log("You chose: " + colorPicker.color)
            //colorRectangale.color = colorPicker.color
            //console.log("Selected index: " + styleData.value.dataId + "-parent ID: " + styleData.value.parentTypeId)
            newVizHandler.setItemColor(styleData.value.dataId, styleData.value.parentTypeId, colorPicker.color)
        }

        onRejected: {
            //console.log("Canceled color dialog")
        }
    }

    RowLayout {
        anchors.fill: parent
        spacing: 6

        Rectangle {
            id: colorRectangale
            Layout.minimumHeight: (styleData.value.parentTypeId === 5) ? 0.5 * parent.height : parent.height //parent.height
            Layout.fillWidth: true
            Layout.minimumWidth: (styleData.value.parentTypeId === 5) ?  0.3 * parent.width : 0.6 * parent.width // 0.6 * parent.width
            color:  (styleData.value.text === "") ? "transparent" :  qsTr("%1").arg(styleData.value.text) //(styleData.value.parentTypeId === 5) || (styleData.value.text === "") ? "transparent" :  qsTr("%1").arg(styleData.value.text) //(styleData.value.parentTypeId === 5) ||(styleData.value.parentTypeId === 4) || (styleData.value.text === "") ? "transparent" :  qsTr("%1").arg(styleData.value.text)
            visible: (styleData.value.parentTypeId === TreeNodeParentType.STRATIGRAPHIC_CHILD) || (styleData.value.text === "") ? false : true
            border.color: Material.primary
            border.width: screenDpi.getDiP(2)

            radius: (styleData.value.parentTypeId === 5) ? 0.7 * parent.width : 0

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    //console.log("Clicked color column: " + JSON.stringify(styleData.value))
                    //colorItem.selectedIndex = styleData.value.dataId
                    //console.log("Color cliked: " + colorRectangale.color)
                    //if(styleData.value.parentTypeId != 5)
                    colorPicker.open()
                }
            }
        }
    }
}
