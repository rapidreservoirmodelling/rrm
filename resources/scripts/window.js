// window.js

var colormap = { 
    getSurfaceColorHex: function (surface_id) {
        ////console.log("Surface id: " + surface_id);

        var color = "#FF0000";
        if (surface_id < 0)
        {
            // return red if surface_id is invalid
            return color;
        }
        
        // Set2 colormap from
        // https://colorbrewer2.org/#type=qualitative&scheme=Set2&n=12
        var num_colors = 12;
        let colormap = [
                "#a6cee3",
                "#1f78b4",
                "#b2df8a",
                "#33a02c",
                "#fb9a99",
                "#e31a1c",
                "#fdbf6f",
                "#ff7f00",
                "#cab2d6",
                "#6a3d9a",
                "#e0e087",
                "#b15928"
            ];

        var i = surface_id % num_colors;
        color = colormap[i];
        
        return color;
    }
} 

function getActionId(response){
    var responseFromBackEnd = JSON.parse(response)
    return responseFromBackEnd.action_id;

}

function getPlottingData(response){
    var responseFromBackEnd = JSON.parse(response)
    var data = JSON.parse(responseFromBackEnd.point_array)
    return data.plottingData;

}

function parseSketchData(response){
    if(response.length < 1)
        return;

    var data = JSON.parse(response);
    return data.plottingData
}

function displaySubmittedCurves( curves, ctx ){
    ////console.log("Submitted curves:" +JSON.stringify(curves));
    if(curves.length <1 )
        return;

    var data = JSON.parse(curves);
    var curves_array = data.curves_array

    //var ctx = getContext('2d')
    //ctx.beginPath()

    for(var j=0;j<curves_array.length;j++){
        var eachCurve = curves_array[j]
        ctx.beginPath()

        for (var i=0; i<eachCurve.length; i++) {
            ////console.log("point"+prop + " = " + JSON.stringify(theCurve[prop]));
            var pointObject = eachCurve[i];
            ////console.log("Point:"+pointObject.x + ","+

            if (i>0) {
                ctx.lineTo(pointObject.x, pointObject.y)
                ctx.moveTo(pointObject.x, pointObject.y)
            }
            else {
                ctx.moveTo(pointObject.x, pointObject.y)
            }

        }//end: for i

        ctx.lineWidth = 2
        ctx.strokeStyle = colormap.getSurfaceColorHex(j+1) //"blue"
        ctx.stroke()
        ctx.closePath()
        ctx.save()

    }//end for j
}

function displaySubmittedContours( curves, ctx ){
    ////console.log("Submitted curves:" +JSON.stringify(curves));
    if(curves.length <1 )
        return;

    var data = JSON.parse(curves);
    var curves_array = data.curves_array

    if(curves_array.length <1 )
        return;

    for(var k=0; k<curves_array.length; k++)
    {
        var eachCurve = curves_array[k]
        var segments_array = eachCurve.segments
        var index = eachCurve.index

        ctx.beginPath()
        for(var j=0;j<segments_array.length;j++)
        {
            var eachCurve = segments_array[j]
            for (var i=0; i<eachCurve.length; i++) {
                ////console.log("point"+prop + " = " + JSON.stringify(theCurve[prop]));
                var pointObject = eachCurve[i];
                ////console.log("Point:"+pointObject.x + ","+

                if (i>0) {
                    ctx.lineTo(pointObject.x, pointObject.y)
                    ctx.moveTo(pointObject.x, pointObject.y)
                }
                else {
                    ctx.moveTo(pointObject.x, pointObject.y)
                }

            }//end: for i

            ctx.lineWidth = 2
            ctx.strokeStyle = "#067297"
            ctx.stroke()
            ctx.closePath()
            ctx.save()

        }//end for j

    }//end for k
}


function displayCurrentContours( curves, ctx ){
    if(curves.length <1 )
        return;

    var data = JSON.parse(curves);
    var curves_array = data.curves_array

    if(curves_array.length <1 )
        return;

    for(var k=0; k<curves_array.length; k++)
    {
        var eachCurve = curves_array[k]
        var segments_array = eachCurve.segments
        var index = eachCurve.index

        ctx.beginPath()
        for(var j=0;j<segments_array.length;j++)
        {
            var eachCurve = segments_array[j]
            for (var i=0; i<eachCurve.length; i++) {
                ////console.log("point"+prop + " = " + JSON.stringify(theCurve[prop]));
                var pointObject = eachCurve[i];
                ////console.log("Point:"+pointObject.x + ","+

                if (i>0) {
                    ctx.lineTo(pointObject.x, pointObject.y)
                    ctx.moveTo(pointObject.x, pointObject.y)
                }
                else {
                    ctx.moveTo(pointObject.x, pointObject.y)
                }

            }//end: for i

            ctx.lineWidth = 2
            ctx.strokeStyle = "#cbcbcb"
            ctx.stroke()
            ctx.closePath()
            ctx.save()

        }//end for j

    }//end for k
}

function displaySubmittedCurvesUsingColorMap_OLD( curves, ctx , colors){

    ////console.log("Submitted curves:" +JSON.stringify(curves));
    ////console.log("Submitted curves color map:" +JSON.stringify(colors));

    ////console.log("Submitted curves color map:" +colors[0]);
    
    if(curves.length <1 )
        return;

    var data = JSON.parse(curves);
    var curves_array = data.curves_array

    //var ctx = getContext('2d')
    //ctx.beginPath()

    for(var j=0;j<curves_array.length;j++){
        var eachCurve = curves_array[j]
        ctx.beginPath()

        for (var i=0; i<eachCurve.length; i++) {
            ////console.log("point"+prop + " = " + JSON.stringify(theCurve[prop]));
            var pointObject = eachCurve[i];
            ////console.log("Point:"+pointObject.x + ","+

            if (i>0) {
                ctx.lineTo(pointObject.x, pointObject.y)
                ctx.moveTo(pointObject.x, pointObject.y)
            }
            else {
                ctx.moveTo(pointObject.x, pointObject.y)
            }

        }//end: for i

        ctx.lineWidth = 2
        ctx.strokeStyle = colors[j] //"blue"
        ctx.stroke()
        ctx.closePath()
        ctx.save()

    }//end for j
}


function displaySubmittedCurvesUsingColorMap( curves, ctx , colors){

    ////console.log("Submitted curves:" +JSON.stringify(curves));
    ////console.log("Submitted curves color map:" +JSON.stringify(colors));


    var data = JSON.parse(curves);
    var curves_array = data.curves_array

    if(curves_array.length <1 )
        return;

    for(var k=0; k<curves_array.length; k++)
    {
        var eachCurve = curves_array[k]
        var segments_array = eachCurve.segments
        var index = eachCurve.index

        ctx.beginPath()
        for(var j=0;j<segments_array.length;j++)
        {
            var eachCurve = segments_array[j]
            for (var i=0; i<eachCurve.length; i++) {
                ////console.log("point"+prop + " = " + JSON.stringify(theCurve[prop]));
                var pointObject = eachCurve[i];
                ////console.log("Point:"+pointObject.x + ","+

                if (i>0) {
                    ctx.lineTo(pointObject.x, pointObject.y)
                    ctx.moveTo(pointObject.x, pointObject.y)
                }
                else {
                    ctx.moveTo(pointObject.x, pointObject.y)
                }

            }//end: for i

            ctx.lineWidth = 2
            ctx.strokeStyle = colors[index] //"blue"
            ctx.stroke()
            ctx.closePath()
            ctx.save()

        }//end for j

    }//end for k
}

function displaySubmittedCurvesUsingObjectMapping( curves, ctx , colors, visibility, alpha, highlight){

    ////console.log("WindowScript Submitted curves:" +JSON.stringify(curves));
    ////console.log("Submitted curves color map:" +JSON.stringify(colors));

    ////console.log("Submitted curves visibility map:" +JSON.stringify(visibility));
    ////console.log("Submitted curves color map:" +JSON.stringify(colors));


    if(alpha)
        ctx.globalAlpha = 0.4
    else
        ctx.globalAlpha = 1.0

    var data = JSON.parse(curves);
    var curves_array = data.curves_array

    ////console.log("Submitted curves size:" + curves_array.length);

    if(curves_array.length <1 )
        return;

    for(var k=0; k<curves_array.length; k++)
    {
        var each_Curve = curves_array[k]
        var segments_array = each_Curve.segments
        var index = each_Curve.index

        if(visibility[k])
        {
            ////console.log("YES, Visible")

            ctx.beginPath()
            for(var j=0;j<segments_array.length;j++)
            {
                var eachCurve = segments_array[j]
                for (var i=0; i<eachCurve.length; i++) {
                    ////console.log("point"+prop + " = " + JSON.stringify(theCurve[prop]));
                    var pointObject = eachCurve[i];
                    ////console.log("Point:"+pointObject.x + ","+

                    if (i>0) {
                        ctx.lineTo(pointObject.x, pointObject.y)
                        ctx.moveTo(pointObject.x, pointObject.y)
                    }
                    else {
                        ctx.moveTo(pointObject.x, pointObject.y)
                    }

                }//end: for i

                ctx.lineWidth = 2

                if(highlight == index) {
                    ctx.strokeStyle = "#74d0f0"
                } else {
                    ctx.strokeStyle = colors[index] //"blue"
                }

                ctx.stroke()
                ctx.closePath()
                ctx.save()

            }//end for j
        }//endif
    }//end for k
}

function displaySubmittedTrajectories( curves, ctx ){
    ////console.log("Submitted curves:" +JSON.stringify(curves));
    if(curves.length <1 )
        return;

    var data = JSON.parse(curves);
    var curves_array = data.curves_array

    //var ctx = getContext('2d')
    //ctx.beginPath()

    for(var j=0;j<curves_array.length;j++){
        var eachCurve = curves_array[j]
        ctx.beginPath()

        for (var i=0; i<eachCurve.length; i++) {
            ////console.log("point"+prop + " = " + JSON.stringify(theCurve[prop]));
            var pointObject = eachCurve[i];
            ////console.log("Point:"+pointObject.x + ","+

            if (i>0) {
                ctx.lineTo(pointObject.x, pointObject.y)
                ctx.moveTo(pointObject.x, pointObject.y)
            }
            else {
                ctx.moveTo(pointObject.x, pointObject.y)
            }

        }//end: for i

        ctx.lineWidth = 2
        ctx.strokeStyle = "blue"
        ctx.stroke()
        ctx.closePath()
        ctx.save()

    }//end for j
}


function displayCurrentSketches( curves, ctx, highlightIndex, selectIndex ){
    if(curves.length <1 )
        return;

    var data = JSON.parse(curves);
    var curves_array = data.curves_array

    for(var j=0; j<curves_array.length; j++)
    {
        var eachCurve = curves_array[j]
        ctx.beginPath()

        for (var i=0; i<eachCurve.length; i++)
        {
            var pointObject = eachCurve[i];

            if (i>0) {
                ctx.lineTo(pointObject.x, pointObject.y)
                ctx.moveTo(pointObject.x, pointObject.y)
            }
            else {
                ctx.moveTo(pointObject.x, pointObject.y)
            }

        }//end: for i

        ctx.lineWidth = 4

        if (highlightIndex === j || selectIndex === j) {
            ctx.strokeStyle = "#74d0f0"
        } else {
            ctx.strokeStyle = "#a30d1f"
        }

        ctx.stroke()
        ctx.closePath()
        ctx.save()

    }//end for j
}

function clearCanvas(graphicsContext, width, height) {
    //graphicsContext = getContext('2d');
    graphicsContext.beginPath()
    graphicsContext.clearRect(0, 0, width, height);
    graphicsContext.closePath()
}

function draw(graphicsContext, theCurve){
    graphicsContext.beginPath()

    //var theCurve = r1.curve;
    ////console.log("-------> Array size:"+theCurve.length)

    for (var i=0;i<theCurve.length; i++) {
        var pointObject = theCurve[i];
        ////console.log("Point: "+ pointObject.x + "," + pointObject.y );

        if (i>0) {
            graphicsContext.lineTo(pointObject.x, pointObject.y)
            graphicsContext.moveTo(pointObject.x, pointObject.y)
        }
        else {
            graphicsContext.moveTo(pointObject.x, pointObject.y)
        }
        i++;
    }
    graphicsContext.lineWidth = 4
    graphicsContext.strokeStyle = "#a30d1f"
    graphicsContext.stroke()
    graphicsContext.closePath()
}


function drawWithHighlight(graphicsContext, theCurve){
    graphicsContext.beginPath()

    for (var i=0;i<theCurve.length; i++) {
        var pointObject = theCurve[i];

        if (i>0) {
            graphicsContext.lineTo(pointObject.x, pointObject.y)
            graphicsContext.moveTo(pointObject.x, pointObject.y)
        }
        else {
            graphicsContext.moveTo(pointObject.x, pointObject.y)
        }
        i++;
    }
    graphicsContext.lineWidth = 4
    graphicsContext.strokeStyle = "#74d0f0"
    graphicsContext.stroke()
    graphicsContext.closePath()
}


function drawArrow(ctx, path, angle)
{
    var arrowHeadLength = 40;

    if (path.length < 2) {return;}

    //ctx.lineJoin = "round";
    //ctx.lineCap = "round";
    //ctx.lineWidth = 4;
    //ctx.strokeStyle = "gray"
    //ctx.fillStyle = "gray"

    var p1 = path[0];
    var p2 = path[1];

    ctx.beginPath()
    /*
    ctx.moveTo(p1.x, p1.y);

    var i = 0;
    for ( i = 1; i < path.length-1; i++)
    {
        // we pick the point between pi+1 & pi+2 as the
        // end point and p1 as our control point
        var midPoint = midPointBtw(p1, p2);
        ctx.quadraticCurveTo(p1.x, p1.y, midPoint.x, midPoint.y);
        p1 = path[i];
        p2 = path[i+1];

        if  (Math.pow(p2.x - path[path.length-1].x, 2) + Math.pow(p2.y - path[path.length-1].y, 2) < Math.pow(arrowHeadLength, 2)) {
            break;
        }
    }
    */
    for (var i=0; i<path.length; i++)
    {
        var pointObject = path[i];

        if (i>0)
        {
            ctx.lineTo(pointObject.x, pointObject.y)
            ctx.moveTo(pointObject.x, pointObject.y)
        }
        else
        {
            ctx.moveTo(pointObject.x, pointObject.y)
        }
        i++;
    }


    //ctx.lineTo(p2.x, p2.y);
    ctx.lineWidth = 4;
    ctx.strokeStyle = "gray"
    ctx.stroke();
    ctx.closePath();
    ctx.save();

    p2 = path[path.length-1];

    ctx.beginPath();
    ctx.translate(p2.x, p2.y);
    //ctx.rotate(angle*Math.PI/180);
    ctx.lineTo(0, 20);
    ctx.lineTo(arrowHeadLength, 0);
    ctx.lineTo(0, -20);
    ctx.lineWidth = 4;
    ctx.strokeStyle = "gray"
    ctx.stroke();
    ctx.closePath();
    //ctx.fill();

}

function midPointBtw(p1, p2) {
    return {
        x: p1.x + (p2.x - p1.x) / 2,
        y: p1.y + (p2.y - p1.y) / 2
    };
}


function drawCurve(graphicsContext, theCurve){
    graphicsContext.beginPath()

    for (var i=0; i<theCurve.length; i+=2) {
        if (i>0) {
            graphicsContext.lineTo(theCurve[i], theCurve[i+1])
            graphicsContext.moveTo(theCurve[i], theCurve[i+1])
        }
        else {
            graphicsContext.moveTo(theCurve[i], theCurve[i+1])
        }
        //i++;
    }
    graphicsContext.lineWidth = 4
    graphicsContext.strokeStyle = "#a30d1f"
    graphicsContext.stroke()
    graphicsContext.closePath()
}

function drawString(ctx, text, x, y) {
    //var ctx = getContext("2d");
    //ctx.font = font;
    ctx.fillStyle = "blue";
    ctx.fillText(qsTr(text.toString()), x, y);
    ctx.stroke();
}

function showGrid(ctx, width, height, wgrid, lineWidth){
    ////console.log("Bingo Sowing grid:" + wgrid);
    //var ctx = getContext("2d")
    //var ctx = myCanvas.ctx;
    ctx.lineWidth = lineWidth
    ctx.strokeStyle = "black"
    ctx.fillStyle = "blue";
    ctx.beginPath()

    //Draw horizontal lines
    var nrows = height/wgrid;
    for(var i=0; i < nrows+1; i++){
        //myCanvas.drawString(nrows-i,0, wgrid*i)
        ctx.fillText(qsTr((nrows-i).toString()), 0, wgrid*i-0.6);
        ctx.moveTo(0, wgrid*i);
        ctx.lineTo(width, wgrid*i);
    }

    //Draw the final horizontal line of the graph
    ctx.moveTo(0, height);
    ctx.lineTo(width, height);

    //Draw the vertical lines
    var ncols = width/wgrid
    for(var j=0; j < ncols+1; j++){
        //Print on x-axis top
        ctx.fillText(qsTr(j.toString()), wgrid*j,height);
        if(j>0)
            ctx.fillText(qsTr(j.toString()), wgrid*j+1,10);
        else ctx.fillText("North", wgrid*j+1,10);
        ctx.moveTo(wgrid*j, 0);
        ctx.lineTo(wgrid*j, height);
    }
    //show the origin
    //myCanvas.drawString("0",-1, height+1)
    ctx.fillText(qsTr("0".toString()), -1, height+1);

    //Draw the final vertical line of the graph
    ctx.moveTo(width, 0);
    ctx.lineTo(width, height);

    ctx.closePath()
    ctx.stroke()

}

function showGuidelines(ctx, mouseX, mouseY, canvasWidth, canvasHeight, canvasID){
    ctx.lineWidth = 1
    ctx.strokeStyle = "gray"

    // horizontal
    //if(canvasID === 1)
    //{
    ctx.beginPath()
    ctx.moveTo(0, mouseY)
    ctx.lineTo(canvasWidth, mouseY)
    ctx.closePath()
    ctx.stroke()
    //}

    // vertical
    ctx.beginPath()
    ctx.moveTo(mouseX, 0)
    ctx.lineTo(mouseX, canvasHeight)
    ctx.closePath()
    ctx.stroke()
}

function showGuidelinesWE(ctx, mouseX, canvasHeight){
    ctx.lineWidth = 1
    ctx.strokeStyle = "gray"

    // vertical
    ctx.beginPath()
    ctx.moveTo(mouseX, 0)
    ctx.lineTo(mouseX, canvasHeight)
    ctx.closePath()
    ctx.stroke()
}

function showGuidelinesNS(ctx, mouseY, canvasWidth){
    ctx.lineWidth = 1
    ctx.strokeStyle = "gray"

    // horizontal
    ctx.beginPath()
    ctx.moveTo(0, mouseY)
    ctx.lineTo(canvasWidth, mouseY)
    ctx.closePath()
    ctx.stroke()
}


function showGuidelinesAngle(ctx, mouseX, mouseY, canvasWidth, canvasHeight, canvasID, angle, veFactor) {
    ctx.setLineDash([1, 2]);
    ctx.lineWidth = 1.5
    ctx.strokeStyle = "gray"

    var lineSize = 75;

    //- Define the coordinates of the points
    var newX1 = mouseX - lineSize;
    var newX2 = mouseX + lineSize;
    var newY1 = mouseY;
    var newY2 = mouseY;

    //- Move center to (0,0)
    newX1 = newX1 - mouseX;
    newX2 = newX2 - mouseX;
    newY1 = newY1 - mouseY;
    newY2 = newY2 - mouseY;

    //-Rotation (x1, mouseY)
    var newX1R = Math.cos(angle*(Math.PI/180))*newX1 - Math.sin(angle*(Math.PI/180))*newY1;
    var newY1R = Math.sin(angle*(Math.PI/180))*newX1 + Math.cos(angle*(Math.PI/180))*newY1;

    //-Rotation (x2, mouseY)
    var newX2R = Math.cos(angle*(Math.PI/180))*newX2 - Math.sin(angle*(Math.PI/180))*newY2;
    var newY2R = Math.sin(angle*(Math.PI/180))*newX2 + Math.cos(angle*(Math.PI/180))*newY2;

    //- Apply VE factor
    newY1R = newY1R * veFactor;
    newY2R = newY2R * veFactor;

    //- Move back to center (mouseX,mouseY)
    newX1R = newX1R + mouseX;
    newX2R = newX2R + mouseX;
    newY1R = newY1R + mouseY;
    newY2R = newY2R + mouseY;

    // horizontal
    ctx.beginPath()
    ctx.moveTo(newX1R, newY1R)
    ctx.lineTo(newX2R, newY2R)
    ctx.closePath()
    ctx.stroke()

    //- Define the coordinates of the points
    var newX3 = mouseX - lineSize; //mouseX;
    var newX4 = mouseX + lineSize; //mouseX;
    var newY3 = mouseY; //mouseY - lineSize;
    var newY4 = mouseY; //mouseY + lineSize;

    //- Move center to (0,0)
    newX3 = newX3 - mouseX;
    newX4 = newX4 - mouseX;
    newY3 = newY3 - mouseY;
    newY4 = newY4 - mouseY;

    //-Rotation
    var newX3R = Math.cos(-angle*(Math.PI/180))*newX3 - Math.sin(-angle*(Math.PI/180))*newY3;
    var newY3R = Math.sin(-angle*(Math.PI/180))*newX3 + Math.cos(-angle*(Math.PI/180))*newY3;

    //-Rotation
    var newX4R = Math.cos(-angle*(Math.PI/180))*newX4 - Math.sin(-angle*(Math.PI/180))*newY4;
    var newY4R = Math.sin(-angle*(Math.PI/180))*newX4 + Math.cos(-angle*(Math.PI/180))*newY4;

    //- Apply VE factor
    newY3R = newY3R * veFactor;
    newY4R = newY4R * veFactor;

    //- Move back to center (mouseX,mouseY)
    newX3R = newX3R + mouseX;
    newX4R = newX4R + mouseX;
    newY3R = newY3R + mouseY;
    newY4R = newY4R + mouseY;

    // vertical
    ctx.beginPath()
    ctx.moveTo(newX3R, newY3R)
    ctx.lineTo(newX4R, newY4R)
    ctx.closePath()
    ctx.stroke()

    ctx.lineWidth = 1
    ctx.setLineDash([]);
}

function showSketchLine(ctx, mouseY, canvasWidth){
    ctx.lineWidth = 1
    ctx.strokeStyle = "red"

    // horizontal
    ctx.beginPath()
    ctx.moveTo(0, mouseY)
    ctx.lineTo(canvasWidth, mouseY)
    ctx.closePath()
    ctx.stroke()
}

function showSketchCircle(ctx, mouseX, mouseY, radius) {
    ctx.lineWidth = 1
    ctx.strokeStyle = "red"

    // circle
    ctx.beginPath()
    //ctx.moveTo(mouseX, mouseY)
    ctx.arc(mouseX, mouseY, radius, 0, Math.PI*2, false)
    //ctx.lineTo(mouseX, mouseY)
    ctx.closePath()
    ctx.stroke()
}

function showSketchEllipse(ctx, mouseX, mouseY, radiusX, radiusY) {
    ctx.lineWidth = 1
    ctx.strokeStyle = "red"

    // circle
    ctx.beginPath()
    ctx.ellipse(mouseX-(radiusX/2), mouseY-(radiusY/2), radiusX, radiusY, 0, 0, Math.PI*2)
    ctx.stroke()
}

function showCrossSectionGuidelineWE(ctx, lineHeight, csOriginX, csWidth, csID){
    ctx.lineWidth = 1;
    ctx.strokeStyle = "#930a1a";
    ctx.fillStyle = "#930a1a";
    ctx.font = "normal 20px sans-serif";

    // horizontal - fixed according to the cross section position
    ctx.beginPath()
    ctx.fillText(qsTr(csID.toFixed(0).toString()) + " m", csOriginX - 100, lineHeight - 10);
    ctx.moveTo(csOriginX - 100, lineHeight)
    ctx.lineTo(csOriginX+csWidth + 100, lineHeight)
    ctx.closePath()
    ctx.stroke()
}

function showCrossSectionGuidelineNS(ctx, lineWidth, csOriginY, csHeight, csID){
    ctx.lineWidth = 1;
    ctx.strokeStyle = "#930a1a";
    ctx.fillStyle = "#930a1a";
    ctx.font = "normal 20px sans-serif";

    // horizontal - fixed according to the cross section position
    ctx.beginPath()
    ctx.fillText(qsTr(csID.toFixed(0).toString()) + " m", lineWidth + 10, csOriginY - 50);
    ctx.moveTo(lineWidth, csOriginY - 50)
    ctx.lineTo(lineWidth, csOriginY+csHeight + 50)
    ctx.closePath()
    ctx.stroke()
}

function showCrossSectionHeight(ctx, lineHeight, csOriginX, csWidth, csID){
    ctx.lineWidth = 1;
    ctx.strokeStyle = "#930a1a";
    ctx.fillStyle = "#930a1a";
    ctx.font = "normal 20px sans-serif";

    // horizontal - fixed according to the cross section position
    ctx.beginPath()
    ctx.fillText(qsTr("Height: " + csID.toFixed(0).toString()) + " m", csOriginX - 160, lineHeight - 10);
    //ctx.moveTo(csOriginX - 100, lineHeight)
    //ctx.lineTo(csOriginX+csWidth + 100, lineHeight)
    ctx.closePath()
    ctx.stroke()

}

//---------- SICILIA'S NEW CODE BLOCK --------------
// The idea was to use a stamp around the mouse cursor
// to indicate that the region is being selected.
// but the stamp is not following the mouse properly
// there is a delay on the visualization.
function regionCursor(ctx, area){
    ctx.lineWidth = 1
    ctx.strokeStyle = "gray"

    var centreX = area.mouseX;
    var centreY = area.mouseY;

    ctx.beginPath();
    ctx.fillStyle = "gray";
    ctx.moveTo(centreX, centreY);
    ctx.arc(centreX, centreY, 50 / 4, 0, Math.PI * 0.5, false);
    ctx.lineTo(centreX, centreY);
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = "red";
    ctx.moveTo(centreX, centreY);
    ctx.arc(centreX, centreY, 50 / 4, Math.PI * 0.5, Math.PI * 2, false);
    ctx.lineTo(centreX, centreY);
    ctx.fill();
}

function drawSketchRegionLine(ctx, theCurve){
    ctx.lineWidth = 2
    ctx.strokeStyle = "gray"

    if(typeof theCurve[0] != "undefined")
    {
        ctx.beginPath();
        var firstPoint = theCurve[0];
        ctx.moveTo(firstPoint.x, firstPoint.y)
        var lastPoint = theCurve[theCurve.length-1];
        ctx.lineTo(lastPoint.x, lastPoint.y)
        ctx.closePath()
        ctx.stroke()
    }
}

//------------------------------------------------------
function fillSelectedRegion(graphicsContext, theCorners, fillColor, opacity){

    graphicsContext.beginPath()
    graphicsContext.globalAlpha = opacity

    graphicsContext.fillStyle = fillColor;//"#8ef0ad";//"#cce6ff";"blue";

    for (var i=0;i<theCorners.length; i++) {
        //for (var i=theCorners.length - 1;i>=0; i--) {
        var pointObject = theCorners[i];
        ////console.log("Point:"+pointObject.x + ","+ pointObject.y)

        if (i===0) {
            graphicsContext.moveTo(pointObject.x, pointObject.y)

        }
        else {
            graphicsContext.lineTo(pointObject.x, pointObject.y)
        }
        
    }

    graphicsContext.lineWidth = 2
    graphicsContext.closePath()
    graphicsContext.fill()
    
}

function drawRegionPolygon(graphicsContext, polygons, fillColor){
    
    ////console.log("-------------This is drawRegionPolygon")
    ////console.log("HERE:" + JSON.stringify(polygons));
    var polygonList = polygons['regionListData']
    ////console.log("HERE:" + JSON.stringify(polygonList));
    //graphicsContext.beginPath()

    for (var k=0;k<polygonList.length; k++) {
        //for (var k=polygonList.length-1;k>=0; k--) {
        ////console.log("----Polygon:" + k)

        var eachRegion = polygonList[k]//polygonList[k.toString()]

        ////console.log("----Polygon:" + k +" -data: " +JSON.stringify(eachRegion))
        ////console.log("----Length of each region: "+ eachRegion[k.toString()].length)

        var regionObject = eachRegion[k.toString()];
        fillSelectedRegion(graphicsContext, regionObject, colormap.getSurfaceColorHex(k+1) )
    }
    
}

function drawRegionPolygonUsingCustomMapData(graphicsContext, polygons, customColorMap, customVisibilityMap){
    
    ////console.log("-------------This is drawRegionPolygon")
    ////console.log("HERE0:" + JSON.stringify(polygons));
	////console.log("HERE1:" + JSON.stringify(customColorMap));
	////console.log("HERE2:" + JSON.stringify(customVisibilityMap));
    var polygonList = polygons['regionListData']
    ////console.log("HERE:" + JSON.stringify(polygonList));
    //graphicsContext.beginPath()

    for (var k=0;k<polygonList.length; k++) {
        if(customVisibilityMap[k]){

            var eachRegion = polygonList[k]//polygonList[k.toString()]

            ////console.log("----Polygon:" + k +" -data: " +JSON.stringify(eachRegion))
            ////console.log("----Length of each region: "+ eachRegion[k.toString()].length)

            var regionObject = eachRegion[k.toString()];
            fillSelectedRegion(graphicsContext, regionObject, customColorMap[k] )
        }
    }
    
}

function drawDomainPolygon(graphicsContext, polygons, domainMap){
    
    ////console.log("-------------This is drawRegionPolygon")
    ////console.log("HERE1:" + JSON.stringify(domainMap));
    var polygonList = polygons['regionListData']
    ////console.log("HERE:" + JSON.stringify(polygonList));
    //graphicsContext.beginPath()


    for (var k=0;k<domainMap.length; k++) {
        var domainObject = domainMap[k]

        if(domainObject.visibility)
        {
            var regionList = domainObject.region_list;
            ////console.log("HERE2:" + JSON.stringify(regionList));

            for(var j=0; j<regionList.length; j++)
            {
                var regionObject = regionList[j]
                ////console.log("HERE3:" + JSON.stringify(regionObject));
                var position = regionObject.id

                var eachRegion = polygonList[position.toString()]//polygonList[k.toString()]

                ////console.log("----Polygon:" + k +" -data: " +JSON.stringify(eachRegion))
                ////console.log("----Length of each region: "+ eachRegion[position.toString()].length)

                var regionMesh = eachRegion[position.toString()];
                fillSelectedRegion(graphicsContext, regionMesh, regionObject.color )
            }
        }
    }


}

function drawDomainPolygonV2(graphicsContext, polygons, domainMap){
    
    ////console.log("-------------This is drawRegionPolygonV2")
    ////console.log("HERE1:" + JSON.stringify(domainMap));
    var polygonList = polygons['regionListData']
    ////console.log("HERE:" + JSON.stringify(polygonList));
    //graphicsContext.beginPath()


    for (var k=0;k<domainMap.length; k++) {
        var domainObject = domainMap[k]

        if(domainObject.visibility)
        {
            var regionList = domainObject.region_list;
            ////console.log("HERE2:" + JSON.stringify(regionList));

            for(var j=0; j<regionList.length; j++)
            {
                var regionObject = regionList[j]
                ////console.log("HERE3:" + JSON.stringify(regionObject));
                var position = regionObject.id

                var eachRegion = polygonList[position.toString()]//polygonList[k.toString()]

                ////console.log("----Polygon:" + k +" -data: " +JSON.stringify(eachRegion))
                ////console.log("----Length of each region: "+ eachRegion[position.toString()].length)

                var regionMesh = eachRegion[position.toString()];
                fillSelectedRegion(graphicsContext, regionMesh, domainObject.domain_color )
            }
        }
    }


}

function drawSingleCurveWithColor(graphicsContext, theCurve, color){
    ////console.log(" plot_array:"+JSON.stringify(theCurve));
    //myCanvas.ctx = getContext('2d')
    graphicsContext.beginPath()

    //var theCurve = r1.curve;
    ////console.log("-------> Array size:"+theCurve.length)

    for (var i=0;i<theCurve.length; i++) {
        var pointObject = theCurve[i];
        ////console.log("Point: "+ pointObject.x + "," + pointObject.y );

        if (i>0) {
            graphicsContext.lineTo(pointObject.x, pointObject.y)
            graphicsContext.moveTo(pointObject.x, pointObject.y)
        }
        else {
            graphicsContext.moveTo(pointObject.x, pointObject.y)
        }
        i++;
    }
    graphicsContext.lineWidth = 4
    graphicsContext.strokeStyle = color
    graphicsContext.stroke()
    graphicsContext.closePath()
}

function drawSingleCurveWithColorAndDots(graphicsContext, theCurve, color){

    ////console.log(" plot_array:"+theCurve);
    //myCanvas.ctx = getContext('2d')

    graphicsContext.setLineDash([1, 2]);
    graphicsContext.beginPath()

    //var theCurve = r1.curve;
    ////console.log("-------> Array size:"+theCurve.length)

    for (var i=0;i<theCurve.length; i++) {
        var pointObject = theCurve[i];
        ////console.log("Point: "+ pointObject.x + "," + pointObject.y );

        if (i>0) {
            graphicsContext.lineTo(pointObject.x, pointObject.y)
            graphicsContext.moveTo(pointObject.x, pointObject.y)
        }
        else {
            graphicsContext.moveTo(pointObject.x, pointObject.y)
        }
        i++;
    }
    graphicsContext.lineWidth = 4
    graphicsContext.strokeStyle = color
    graphicsContext.stroke()
    graphicsContext.closePath()

    graphicsContext.setLineDash([]);
}

class Car {
    constructor(brand) {
        this.carname = brand;
    }
    present(x) {
        return x + ", I have a " + this.carname;
    }
}


