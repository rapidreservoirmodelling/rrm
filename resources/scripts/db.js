function dbInit() {
    var db = LocalStorage.openDatabaseSync("cross_section_images_DB", "", "Keep Track of images", 1000000)
    try {
        db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS images_log (position_id INTEGER, direction_id INTEGER, absolute_path TEXT); ')
            tx.executeSql('CREATE UNIQUE INDEX IF NOT EXISTS idx_images_log on images_log (position_id, direction_id); ')


        })
    } catch (err) {
        //console.log("Error creating table in database: " + err)
    };
}

function dbClear() {
    var db = LocalStorage.openDatabaseSync("cross_section_images_DB", "", "Keep Track of images", 1000000)

    try {
        db.transaction(function (tx) {
            tx.executeSql('DELETE FROM images_log;')

        })
    } catch (err) {
        //console.log("Error clearing table in database: " + err)
    };
}
function dbGetHandle()
{
    try {
        var db = LocalStorage.openDatabaseSync("cross_section_images_DB", "", "Keep Track of images", 1000000)
    } catch (err) {
        //console.log("Error opening database: " + err)
    }
    return db
}

function addImage(direction, cross_section, file_name, absolute_path)
{
    ////console.log("Inserting: direction: " + direction +" cs_id= " +cross_section)

    var db = dbGetHandle()
    var rowid = 0;
    db.transaction(function (tx) {
        tx.executeSql('INSERT OR REPLACE INTO images_log (position_id, direction_id, absolute_path) VALUES(?, ?, ?)',
                      [ cross_section, direction, absolute_path])
        var result = tx.executeSql('SELECT last_insert_rowid()')
        rowid = result.insertId
    })

    return rowid;
}

function readAllRows()
{
    var listModel = [];
    var db = dbGetHandle()
    db.transaction(function (tx) {
        //var results = tx.executeSql('SELECT rowid, direction_id, cross_section_id, image_file_name, absolute_path FROM images_log order by rowid desc')
        var results = tx.executeSql('SELECT * FROM images_log order by rowid desc')

        for (var i = 0; i < results.rows.length; i++) {
            var fullPath = results.rows.item(i).absolute_path
            var filename = fullPath.replace(/^.*[\\\/]/, '')

            listModel.push( {
                                 id: results.rows.item(i).rowid,
                                 direction_id: results.rows.item(i).direction_id,
                                 cross_section_id: results.rows.item(i).position_id,
                                 absolute_path: results.rows.item(i).absolute_path,
                                 file_name: filename

                             })
        }
    })

    return listModel;
}

function update(Prowid, Pdirection_id, Pcross_section_id, Pimage_file_name, Pabsolute_path)
{
    var db = dbGetHandle()
    db.transaction(function (tx) {
        tx.executeSql(
                    'update images_log set direction_id=?, position_id=?, absolute_path=? where rowid = ?', [Pdirection_id, Pcross_section_id, Pabsolute_path, Prowid])
    })
}

function deleteRow(Prowid)
{
    var db = dbGetHandle()
    db.transaction(function (tx) {
        tx.executeSql('delete from images_log where rowid = ?', [Prowid])
    })
}

function getImage(Pdirection_id, Pcross_section_id)
{
    var output = "";
    var db = dbGetHandle()
    db.transaction(function (tx) {
        var results = tx.executeSql(
                    'SELECT * FROM images_log where direction_id = ? AND position_id = ?', [Pdirection_id, Pcross_section_id], 'LIMIT 1' )

        for (var i = 0; i < results.rows.length; i++)
        {
            output = {

                        "direction_id": results.rows.item(0).direction_id ,
                        "cross_section_id": results.rows.item(0).position_id,
                        "absolute_path": results.rows.item(0).absolute_path

                        }
        }

    })
    return output
}

function getMapViewImage(Pdirection_id) {
    var output = "";
    var db = dbGetHandle()
    db.transaction(function (tx) {
        var results = tx.executeSql(
            'SELECT * FROM images_log where direction_id = ? ', [Pdirection_id], 'LIMIT 1')

        for (var i = 0; i < results.rows.length; i++) {
            output = {

                "direction_id": results.rows.item(0).direction_id,
                "cross_section_id": results.rows.item(0).position_id,
                "absolute_path": results.rows.item(0).absolute_path

            }
        }

    })
    return output
}

function deleteImage(Pdirection_id, Pcross_section_id)
{
    var db = dbGetHandle()
    var output = "SUCCESS"
    if (Pdirection_id == 3) {
        try {
            db.transaction(function (tx) {
                var results = tx.executeSql(
                    'DELETE FROM images_log where direction_id = ? ', [Pdirection_id], 'LIMIT 1')



            })
        } catch (err) {
            //console.log("Error deleting mapview image: " + err);
            output = "ERROR"
        };
    }
    else {
        try {
            db.transaction(function (tx) {
                var results = tx.executeSql(
                    'DELETE FROM images_log where direction_id = ? AND position_id = ?', [Pdirection_id, Pcross_section_id], 'LIMIT 1')



            })
        } catch (err) {
            //console.log("Error deleting row: " + err);
            output = "ERROR"
        };
    }
    

    return output
}

function addSameImageForAllMapViewCrossSections(direction, cross_section, file_name, absolute_path) {
    ////console.log("Inserting: direction: " + direction +" cs_id= " +cross_section)

    var db = dbGetHandle()

    /*for (var i = 0; i < 65; i++) {

        db.transaction(function (tx) {
            tx.executeSql('INSERT OR REPLACE INTO images_log (position_id, direction_id, absolute_path) VALUES(?, ?, ?)',
                [i, direction, absolute_path])
            
        })

    }*/
    db.transaction(function (tx) {
            tx.executeSql('INSERT OR REPLACE INTO images_log (position_id, direction_id, absolute_path) VALUES(?, ?, ?)',
                [-1, direction, absolute_path])

        })

}

function fillImageData(jsonData) {
    var imageDataObject = JSON.parse(jsonData)

    var db = dbGetHandle()
    if (imageDataObject.length >= 0) {
        for (var i = 0; i < imageDataObject.length; i++) {
            var eachImage = imageDataObject[i]

            db.transaction(function (tx) {
                tx.executeSql('INSERT OR REPLACE INTO images_log (position_id, direction_id, absolute_path) VALUES(?, ?, ?)',
                    [eachImage.cross_section_id, eachImage.direction_id, eachImage.absolute_path])

            })
        }
    }
}