function dbInit() {
    var db = LocalStorage.openDatabaseSync("tree_view_status_DB", "", "Keep Track of expand status", 1000000)
    try {
        db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS expand_status (indentation INTEGER, data_id INTEGER, expanded INTEGER); ')
            tx.executeSql('CREATE UNIQUE INDEX IF NOT EXISTS idx_expand_status on expand_status (indentation, data_id); ')


        })
    } catch (err) {
        console.log("Error creating table in database: " + err)
    };
}

function dbClear() {
    var db = LocalStorage.openDatabaseSync("tree_view_status_DB", "", "Keep Track of expand status", 1000000)

    try {
        db.transaction(function (tx) {
            tx.executeSql('DELETE FROM expand_status;')

        })
    } catch (err) {
        console.log("Error clearing table in database: " + err)
    };
}
function dbGetHandle()
{
    try {
        var db = LocalStorage.openDatabaseSync("tree_view_status_DB", "", "Keep Track of expand status", 1000000)
    } catch (err) {
        console.log("Error opening database: " + err)
    }
    return db
}

function addNodeData(indentation, data_id, expanded)
{
    //console.log("Inserting: indentation: " + indentation+" data_id= " + data_id)

    var db = dbGetHandle()
    var rowid = 0;
    db.transaction(function (tx) {
        tx.executeSql('INSERT OR REPLACE INTO expand_status (indentation, data_id, expanded) VALUES(?, ?, ?)',
                      [ indentation, data_id, expanded])
        var result = tx.executeSql('SELECT last_insert_rowid()')
        rowid = result.insertId
    })

    return rowid;
}

function readAllRows()
{
    var listModel = [];
    var db = dbGetHandle()
    db.transaction(function (tx) {
        var results = tx.executeSql('SELECT * FROM expand_status order by rowid desc')

        for (var i = 0; i < results.rows.length; i++) {
            listModel.push( {
                                 id: results.rows.item(i).rowid,
                                 data_id: results.rows.item(i).data_id,
                                 indentation: results.rows.item(i).indentation,
                                 expanded: results.rows.item(i).expanded
                             })
        }
    })

    return listModel;
}

function update(Prowid, Pdirection_id, Pcross_section_id, Pimage_file_name, Pabsolute_path)
{
    var db = dbGetHandle()
    db.transaction(function (tx) {
        tx.executeSql(
                    'update images_log set direction_id=?, position_id=?, absolute_path=? where rowid = ?', [Pdirection_id, Pcross_section_id, Pabsolute_path, Prowid])
    })
}

function deleteRow(Prowid)
{
    var db = dbGetHandle()
    db.transaction(function (tx) {
        tx.executeSql('delete from expand_status where rowid = ?', [Prowid])
    })
}

function getExpandStatus(indentation, data_id)
{
    var output = "";
    var db = dbGetHandle()
    db.transaction(function (tx) {
        var results = tx.executeSql(
                    'SELECT * FROM expand_status where indentation = ? AND data_id = ?', [indentation, data_id], 'LIMIT 1' )

        for (var i = 0; i < results.rows.length; i++)
        {
            output = {

                        "id": results.rows.item(i).rowid,
                        "data_id": results.rows.item(i).data_id,
                        "indentation": results.rows.item(i).indentation,
                        "expanded": results.rows.item(i).expanded

                        }
        }

    })
    return output
}





