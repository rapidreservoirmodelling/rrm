import QtQuick 2.0

import Constants 1.0

import "scripts/window.js" as WindowScript
import "scripts/promise.js" as Q

Item {

    //- Sketch curves
    property var curve:             []  // To store the current sketch
    property var temp_curve:        []  // To store the current oversketch
    property var submittedCurves:   []  // To store the curves of each surface that intersect with the current cross section
    property var sketchRegion:      []  // To store the sketch used to select regions
    property bool sketchLine:     false // To keep track if the sketch line are visible or not
    property var colorMap:          []  // To keep track of custom colors for curves
    property var visibilityMap:     []  // To keep track of visibility for curves

    //- Cross Section properties
    property real csWE_Width:   500.0   // Width of the model modified by the visualization scale
    property real csWE_Height:  500.0   // Height of the model modified by the visualization scale
    property real csNS_Width:   500.0   // Width of the model modified by the visualization scale
    property real csNS_Height:  500.0   // Height of the model modified by the visualization scale
    property real csIDPosition: 500.0   // identify the real position of the cross section in front view
    property real modelWidth:   500.0   // Width of the model
    property real modelHeight:  500.0   // Height of the model

    //- Selection of canvas elements
    property bool selectSketch: false   // To keep track if the current sketch was selected by the user

    //- Canvas Scale factors
    property real zoomFactor:   1       // To store mouse wheel zoom factor

    //- Guidelines
    property bool guidelines:   false   // To keep track if the guidelines are visible or not
    property bool glMoving:     true    // To keep track if the guidelines are moving or not
    property var glMouse:       []      // To receive the mouse position to update the guidelines
    property int glMouseX:      0       // To store the mouseX position to update the guidelines
    property int glMouseY:      0       // To store the mouseY position to update the guidelines
    property real glHeightWE:   500.0   // To store the height position of the fixed horizontal guideline representing the cross section
    property real glHeightNS:   500.0   // To store the height position of the fixed horizontal guideline representing the cross section

    //- Regions
    property var selectedRegion:        []  // To store the polygon of the selected region
    property var regionCurveBox:        []  // To store the curve box of visualized regions
    property var regionColor:        []  // To keep track of custom colors for regions
    property var regionVisibility:   []  // To keep track of visibility for regions
    property real alpha_SR:             0.4 // To control the opacity of the sketch region

    //- Domains
    property var domain_map:             []  // To keep track of domains

    //- Preview
    property var previewCurve:          []  // To detect selected curve object
    property string previewCurveColor:  ""  // To detect selected color
    property var preivewFlag:         false

    //- Flattening
    property int highlightFlatt:        -1


    function reset() {
        curve = [];
        temp_curve = [];
        submittedCurves = [];
        sketchRegion = [];
        zoomFactor = 1.0;
        csWE_Width = 500.0;
        csWE_Height = 500.0;
        csNS_Width = 500.0;
        csNS_Height = 500.0;
        csIDPosition = 500.0;
        modelWidth = 500.0;
        modelHeight = 500.0;
        selectSketch = false;
        guidelines = false;
        glMoving = true;
        sketchLine = false;
        glMouse = [];
        glMouseX = 0;
        glMouseY = 0;
        glHeightWE = 500.0;
        glHeightNS = 500.0;
        selectedRegion = [];
        regionCurveBox = [];
        previewCurve = [];
        previewCurveColor = "";
        colorMap = [];
        visibilityMap = [];
        regionColor = [];
        regionVisibility = [];
        domain_map = [];
        preivewFlag = false;// This is available in all editors.
        alpha_SR = 0.4;
        highlightFlatt = -1;
        overlay.regionMenu.close();

        updateScale();
        updateResize();
        newVizHandler.reset();        
    }


    function updateScale() {
        modelWidth = model3DHandler.size_x;
        modelHeight = model3DHandler.size_z * canvasHandler.veFactor;

        //- WE Cross Section
        canvasHandler.csWE_Scale = ((70.0 * myCanvas.width) / 100.0) / modelWidth;
        if( (modelHeight * canvasHandler.csWE_Scale) > ((70.0 * myCanvas.height) / 100.0) ) {
            canvasHandler.csWE_Scale = canvasHandler.csWE_Scale * (((70.0 * myCanvas.height) / 100.0) / (modelHeight * canvasHandler.csWE_Scale));
        }
        csWE_Width = modelWidth * canvasHandler.csWE_Scale;
        csWE_Height = modelHeight * canvasHandler.csWE_Scale;

        //- NS Cross Section
        canvasHandler.csNS_Scale = ((70.0 * myCanvas.width) / 100.0) / model3DHandler.size_y;
        if( (modelHeight * canvasHandler.csNS_Scale) > ((70.0 * myCanvas.height) / 100.0) ) {
            canvasHandler.csNS_Scale = canvasHandler.csNS_Scale * (((70.0 * myCanvas.height) / 100.0) / (modelHeight * canvasHandler.csNS_Scale));
        }
        csNS_Width = model3DHandler.size_y * canvasHandler.csNS_Scale;
        csNS_Height = modelHeight * canvasHandler.csNS_Scale;
    }


    function updateFlatteningScale( height ) {
        modelWidth = model3DHandler.size_x;
        modelHeight = height * canvasHandler.veFactor;

        //- WE Cross Section
        canvasHandler.csWE_Scale = ((70.0 * myCanvas.width) / 100.0) / modelWidth;
        if( (modelHeight * canvasHandler.csWE_Scale) > ((70.0 * myCanvas.height) / 100.0) ) {
            canvasHandler.csWE_Scale = canvasHandler.csWE_Scale * (((70.0 * myCanvas.height) / 100.0) / (modelHeight * canvasHandler.csWE_Scale));
        }
        csWE_Width = modelWidth * canvasHandler.csWE_Scale;
        csWE_Height = modelHeight * canvasHandler.csWE_Scale;

        //- NS Cross Section
        canvasHandler.csNS_Scale = ((70.0 * myCanvas.width) / 100.0) / model3DHandler.size_y;
        if( (modelHeight * canvasHandler.csNS_Scale) > ((70.0 * myCanvas.height) / 100.0) ) {
            canvasHandler.csNS_Scale = canvasHandler.csNS_Scale * (((70.0 * myCanvas.height) / 100.0) / (modelHeight * canvasHandler.csNS_Scale));
        }
        csNS_Width = model3DHandler.size_y * canvasHandler.csNS_Scale;
        csNS_Height = modelHeight * canvasHandler.csNS_Scale;
    }


    function updateVisualization(){
        submittedCurves = canvasHandler.getSubmittedCurves(CANVAS_ID.FRONT_VIEW);
        canvasHandler.returnSketch();
        canvasHandler.returnSketchRegion();
        previewCurve = WindowScript.parseSketchData(canvasHandler.returnPreview(CANVAS_ID.FRONT_VIEW));

        selectedRegion = WindowScript.parseSketchData(canvasHandler.returnRegion());
        if(newVizHandler.getVizFlag()) {
            regionCurveBox = JSON.parse(canvasHandler.getRegionPolygon());
        }
    }


    function updateResize() {
        canvasHandler.csWE_Origin_x = (myCanvas.width  / 2.0) - (csWE_Width  / 2.0);
        canvasHandler.csWE_Origin_y = (myCanvas.height / 2.0) - (csWE_Height / 2.0);
        canvasHandler.csNS_Origin_x = (myCanvas.width  / 2.0) - (csNS_Width  / 2.0);
        canvasHandler.csNS_Origin_y = (myCanvas.height / 2.0) - (csNS_Height / 2.0);

        glHeightWE = csWE_Height - (((csWE_Height * canvasHandler.crossSection) / model3DHandler.length_Discretization)) + canvasHandler.csWE_Origin_y;
        glHeightNS = csNS_Height - (((csNS_Height * canvasHandler.crossSection) / model3DHandler.width_Discretization)) + canvasHandler.csNS_Origin_y;

        updateVisualization();
    }


    function updateVizData(color, visibility) {
        colorMap = color;
        visibilityMap = visibility;
    }


    function connect3DView() {
        var promise1 = new Q.promise(function() {
            newVizHandler.initiateSurfaceSubmissoin();
        });
        promise1.then(function() {
            receiver.setLastCanvasAction();
        });
    }

    function rightButtonDoubleClickAction() {
        if(!newVizHandler.getVizFlag()) {
            //- Activate progress bar for the submission process
            progressNotifierPathGuided.open();

            //- Timer to allow the progress notifier to get visible
            timer2.setTimeout(function() {
                //- Call submission sketch process
                canvasHandler.futureSubmitSketch();
                curve = [];
                temp_curve = [];
                selectSketch = false;
                canvasHandler.resetHighlightSelection_FV();
                receiver.sendSurfaceCreated(CANVAS_ID.FRONT_VIEW);

                //Remove selected object visualization before submitting
                previewCurve = [];
                previewCurveColor = "";
            }, 100);

            canvasHandler.isPathGuided = false;
        } else {
            vizOnNotification.open();
            timer2.setTimeout(function(){
                vizOnNotification.close();
            }, 1200);
            myCanvas.requestPaint();
        }
    }//end: function rightButtonDoubleClickAction


    /*
    function rightButtonDoubleClickAction() {
        if(!newVizHandler.getVizFlag()) {
            if(canvasHandler.isPathGuided && canvasHandler.isSubmitAllowed)
                progressNotifierPathGuided.open();

            timer2.setTimeout(function(){
                var submitted;
                submitted = canvasHandler.submitSketch();
                curve = [];
                temp_curve = [];
                selectSketch = false;
                canvasHandler.resetHighlightSelection_FV();
                receiver.sendSurfaceCreated(CANVAS_ID.FRONT_VIEW);

                //Remove selected object visualization before submitting
                previewCurve = [];
                previewCurveColor = "";

                if(submitted) {
                    canvasHandler.isPreview = false;
                    submittedCurves = canvasHandler.getSubmittedCurves(CANVAS_ID.FRONT_VIEW);

                    var curves = JSON.parse(submittedCurves);

                    if (curves.curves_array != undefined || curves.curves_array.length != 0) {
                        connect3DView();
                    }
                }
                myCanvas.requestPaint();
            }, 100);

            canvasHandler.isPathGuided = false;
        } else {
            vizOnNotification.open();
            timer2.setTimeout(function(){
                vizOnNotification.close();
            }, 1200);
            myCanvas.requestPaint();
        }
    }//end: function rightButtonDoubleClickAction

      */

    function surfacePreviewAction() {
        if(!newVizHandler.getVizFlag()) {
            progressNotifierPathGuided.open();

            timer2.setTimeout(function(){
                
                previewCurveColor = "#FFCC80";

                canvasHandler.futureSubmitPreview();
                                
            }, 100);
        } else {
            vizOnNotification.open();
            timer2.setTimeout(function(){
                vizOnNotification.close();
            }, 1200);
        }
        myCanvas.requestPaint();
    }//end: function surfacePreviewAction()

    /*function surfacePreviewAction() {
        if(!newVizHandler.getVizFlag()) {
            if(canvasHandler.isPathGuided && canvasHandler.isSubmitAllowed)
                progressNotifierPathGuided.open();

            timer2.setTimeout(function(){
                var submitted;
                submitted = canvasHandler.submitPreview(newVizHandler.getPreviewFlag());
                previewCurveColor = "#FFCC80";

                if(submitted) {
                    previewCurve = WindowScript.parseSketchData(canvasHandler.returnPreview(CANVAS_ID.FRONT_VIEW));
                    myCanvas.requestPaint();

                    var curves = previewCurve;
                    if (curves != undefined || curves.length != 0) {
                        newVizHandler.removeSurfacePreview();
                    }
                } else {
                    previewCurve = [];
                }
            }, 100);
        } else {
            vizOnNotification.open();
            timer2.setTimeout(function(){
                vizOnNotification.close();
            }, 1200);
        }
        myCanvas.requestPaint();
    }//end: function surfacePreviewAction()*/

    Connections {
        target: canvasHandler

        onSelectSketchFV: {
            selectSketch = response;
            myCanvas.requestPaint();
        }

        onHighlightSketch: {
            myCanvas.requestPaint();
        }

        onGetUpdatedSketch: {
            if(canvasHandler.direction === DIRECTION.LENGTH || canvasHandler.direction === DIRECTION.WIDTH) {
                curve = WindowScript.parseSketchData(response);
                myCanvas.requestPaint();
            }
        }

        onGetUpdatedOverSketch: {
            if(canvasHandler.direction === DIRECTION.LENGTH || canvasHandler.direction === DIRECTION.WIDTH) {
                temp_curve = WindowScript.parseSketchData(response);
                myCanvas.requestPaint();
            }
        }

        onResetNew: {
            reset();
            myCanvas.requestPaint();
        }

        onPullRegionCurveBox: {
            regionCurveBox = JSON.parse(canvasHandler.getRegionPolygon());
            myCanvas.requestPaint();
        }

        onClearRegionCurveBox: {            
            regionCurveBox = [];
            myCanvas.state = "SKETCH";
            myCanvas.requestPaint();
        }

        onRefreshSubmittedCurves: {
            submittedCurves = canvasHandler.getSubmittedCurves(CANVAS_ID.FRONT_VIEW);
            if(model3DHandler.isPreserveRegionActive()) {
                canvasHandler.updateRegionBoundary();
                selectedRegion = WindowScript.parseSketchData(canvasHandler.returnRegion());
            }
            myCanvas.requestPaint();
        }

        onGetUpdatedSketchRegion: {
            sketchRegion = WindowScript.parseSketchData(response);
            myCanvas.requestPaint();
        }

        onGetUpdatedPolygonCorners: {
            selectedRegion = WindowScript.parseSketchData(response);
            myCanvas.requestPaint();
        }

        onDirectionChanged: {
            selectSketch = false;
            canvasHandler.resetHighlightSelection_FV();
            myCanvas.requestPaint();
            if(canvasHandler.direction === DIRECTION.HEIGHT) {
                csIDPosition = ((model3DHandler.size_z * canvasHandler.crossSection) / model3DHandler.width_Discretization);
            }
        }

        onUpdateLoadedData: {
            previewCurve = [];
        }

        onHighlightSurfaceCreated: {
            if(index === -1) {
                highlightFlatt = index;
            } else {
                highlightFlatt = index - 1;
            }
            myCanvas.requestPaint();
        }

        onUpdateBBoxFlattening: {
            if(height) {
                updateFlatteningScale(height);
                ////console.log("FrontSketchFunctions: width=" + model3DHandler.size_x + ", length=" + model3DHandler.size_y + ", height=" + height)
                //newVizHandler.changeDimension();
            } else {
                updateScale();
            }
            updateResize();
            myCanvas.requestPaint();
        }

        onVeFactorChanged: {
            if(canvasHandler.flatHeight) {
                updateFlatteningScale(canvasHandler.flatHeight);                
            } else {
                updateScale();                
            }
            updateResize();
            myCanvas.requestPaint();
        }

        onVeAngleChanged: {
            myCanvas.requestPaint();
        }

        onRegionClicked: {
            if(_region >= 0 && _region < _regionSize) {
                overlay.regionMenu.m_region_id = _region;
                overlay.regionMenu.m_region_name = _regionName;
                overlay.regionMenu.m_region_volume = _regionVolume;
                overlay.regionMenu.m_region_color = _regionColor;
                overlay.regionMenu.m_domain_id = _domain;
                overlay.regionMenu.m_domain_name = _domainName;
                overlay.regionMenu.m_domain_volume = _domainVolume;
                overlay.regionMenu.m_domain_color = _domainColor;
                overlay.regionMenu.m_domain_map = domain_map;
                if(!overlay.regionMenu.opened) {
                    overlay.regionMenu.x = overlay.regionMenu.clickX;
                    overlay.regionMenu.y = overlay.regionMenu.clickY;
                    overlay.regionMenu.open();
                }
            }
        }

        onSketchSubmitted: {
            timer2.setTimeout(function(){
                progressNotifierPathGuided.close();
            }, 100);

            canvasHandler.isPreview = false;
            if(model3DHandler.isPreserveRegionActive()) {
                canvasHandler.updateRegionBoundary();
                selectedRegion = WindowScript.parseSketchData(canvasHandler.returnRegion());
            }
            submittedCurves = canvasHandler.getSubmittedCurves(CANVAS_ID.FRONT_VIEW);
            var curves = JSON.parse(submittedCurves);
            if (curves.curves_array !== undefined || curves.curves_array.length !== 0) {
                newVizHandler.process3DMesh(false)//connect3DView();
            }
            myCanvas.requestPaint();
        }

        onPreviewSubmitted: {
            previewCurve = WindowScript.parseSketchData(canvasHandler.returnPreview(CANVAS_ID.FRONT_VIEW));
            myCanvas.requestPaint();

            var curves = previewCurve;
            if (curves !== undefined || curves.length !== 0) {
                newVizHandler.process3DMesh(true)//newVizHandler.removeSurfacePreview();
            }
            /*timer2.setTimeout(function(){
                progressNotifierPathGuided.close();
            }, 100);
            */
        }

    }//end: Connections canvasHandler


    Connections {
        target: receiver

        onUpdateCanvasAction: {
            sketchLine = false;
            myPinch.enabled = false;

            if(action === ACTION.TB_START_FLATTENING) {
                csNS.backImgVisible = false;
                csWE.backImgVisible = false;
            } else if (action === ACTION.TB_STOP_FLATTENING) {
                csNS.backImgVisible = true;
                csWE.backImgVisible = true;
            }

            if(action === ACTION.FB_FIX_GUIDELINES && !glMoving) {
                glMoving = true;
            } else {
                canvasHandler.canvasAction = action;
                if(canvasHandler.canvasAction === ACTION.FB_SKETCH_LINE) {
                    sketchLine = true;
                } else if(canvasHandler.canvasAction === ACTION.FB_ZOOM) {
                    myPinch.enabled = true;
                } else if (canvasHandler.canvasAction === ACTION.TB_STOP_SKETCH_REGION ||
                           canvasHandler.canvasAction === ACTION.TB_STOP_FLATTENING) {
                    canvasHandler.delegateFrontViewAction(MOUSE.NO_BUTTON, MOUSE.NO_ACTION, 0, 0);
                    selectedRegion = [];
                }
            }
            myCanvas.requestPaint();
        }

        onUpdateCanvasVisualizationFV: {
            sketchLine = false;
            canvasHandler.delegateFrontViewVisualization(action);
            switch(action) {
            case ACTION.FB_SHOW_GUIDELINES:
                guidelines = !guidelines;
                if(!guidelines) {
                    canvasHandler.veAngleVisible = false;
                }

                break;

            case ACTION.FV_SAVE_SKETCH_TEMPLATE:
                canvasHandler.saveSketchTemplate();
                break;

            case ACTION.FV_SMOOTH_SKETCH:
                if(newVizHandler.getPreviewFlag() && canvasHandler.isPreview) {
                    surfacePreviewAction();
                 }
                break;

            case ACTION.FV_CLEAR_SKETCH:
                newVizHandler.clearSurfacePreview();
                if(newVizHandler.getPreviewFlag() && canvasHandler.isPreview) {
                    surfacePreviewAction();
                }
                break;

            case ACTION.FV_REDO_SKETCH:
                if(newVizHandler.getPreviewFlag() && canvasHandler.isPreview){
                    surfacePreviewAction();
                }
                break;

            case ACTION.FV_UNDO_SKETCH:
                if(newVizHandler.getPreviewFlag() && canvasHandler.isPreview){
                    surfacePreviewAction();
                }
                break;
            }
            myCanvas.requestPaint();
        }

        onCrossSectionChanged: {
            canvasHandler.crossSection = id;
            selectSketch = false;
            canvasHandler.resetHighlightSelection_FV();

            glHeightWE = csWE_Height - (((csWE_Height * canvasHandler.crossSection) / model3DHandler.length_Discretization)) + canvasHandler.csWE_Origin_y;
            glHeightNS = csNS_Height - (((csNS_Height * canvasHandler.crossSection) / model3DHandler.width_Discretization)) + canvasHandler.csNS_Origin_y;

            if(canvasHandler.direction === DIRECTION.HEIGHT) {
                csIDPosition = ((model3DHandler.size_z * canvasHandler.crossSection) / model3DHandler.width_Discretization);
            } else if(canvasHandler.direction === DIRECTION.WIDTH || canvasHandler.direction === DIRECTION.LENGTH) {
                submittedCurves = canvasHandler.getSubmittedCurves(CANVAS_ID.FRONT_VIEW);
                canvasHandler.returnSketch();
                canvasHandler.returnSketchRegion();

                if(model3DHandler.isPreserveRegionActive()) {
                    canvasHandler.updateRegionBoundary();
                    selectedRegion = WindowScript.parseSketchData(canvasHandler.returnRegion());
                }

                if(newVizHandler.getPreviewFlag()) {
                    previewCurve = WindowScript.parseSketchData(canvasHandler.returnPreview(CANVAS_ID.FRONT_VIEW));
                }              

                if(newVizHandler.getVizFlag()) {
                    regionCurveBox = JSON.parse(canvasHandler.getRegionPolygon());
                }
            }
            myCanvas.requestPaint();
        }

        onReceiveMouseFromMapView: {
            if(guidelines && glMoving) {
                glMouse = WindowScript.parseSketchData(response);
                if (canvasHandler.direction === DIRECTION.LENGTH) {
                    glMouseX = (((glMouse[0].x - glMouse[2].x) / glMouse[1].x) * csWE_Width)  + canvasHandler.csWE_Origin_x;
                    glMouseY = ((csWE_Height * canvasHandler.crossSection) / model3DHandler.length_Discretization) + canvasHandler.csWE_Origin_y;
                } else if (canvasHandler.direction === DIRECTION.WIDTH) {
                    glMouseX = ((1-((glMouse[0].y - glMouse[2].y) / glMouse[1].y)) * csNS_Width)  + canvasHandler.csNS_Origin_x; //((myFunctions.csNS_Width * canvasHandler.crossSection) / model3DHandler.length_Discretization) + canvasHandler.csNS_Origin_x;
                    glMouseY = ((csNS_Height * canvasHandler.crossSection) / model3DHandler.width_Discretization) + canvasHandler.csNS_Origin_y;//((1-((myCanvas.glMouse[0].x - myCanvas.glMouse[2].x) / myCanvas.glMouse[1].x)) * myFunctions.csNS_Height)  + canvasHandler.csNS_Origin_y;
                }
                canvasHandler.fixGuidelineFrontView(glMouseX,glMouseY);
                myCanvas.requestPaint();
            }
        }

        onSendFixFrontViewGuidelines: {
            if(guidelines) {
                glMoving = false;
            }
        }

        onSurfaceCreated: {
            if(canvasID === CANVAS_ID.MAP_VIEW) {
                if(newVizHandler.getPreviewFlag() && canvasHandler.isSubmitAllowed) {
                    newVizHandler.resetPreviewFlag();
                }
                rightButtonDoubleClickAction();
            }
        }

        onSendSketchTemplateIndex: {
            canvasHandler.loadSketchTemplate(index);
            canvasHandler.addCrossSectionUsed(canvasHandler.crossSection, canvasHandler.direction);
            canvasHandler.isPreview = true;
            if(newVizHandler.getPreviewFlag() && canvasHandler.isPreview) {
                surfacePreviewAction();
            }
            myMouseArea.mouseButtonIdentifier = MOUSE.NO_BUTTON;
            canvasHandler.resetHighlightSelection_FV();
            myCanvas.requestPaint();
        }

        onPreviewChanged: {
            canvasHandler.isPreview = flag;
            if(newVizHandler.getPreviewFlag() && canvasHandler.isPreview) {
                surfacePreviewAction();
            }
            myCanvas.requestPaint();
        }

        onUpdatePreviewVisualization: {
            newVizHandler.clearSurfacePreview();
            if(newVizHandler.getPreviewFlag() && canvasHandler.isPreview) {
                surfacePreviewAction();
            }
            myCanvas.requestPaint();
        }

        onGetSketchRegionOpacity: {
            alpha_SR = alpha;
            myCanvas.requestPaint();
        }
    }//end: Connections receiver


    Connections {
        target: newVizHandler

        onVizFlagChanged: {
            if(flag) {
                myCanvas.state = "REGION";
                overlay.regionMenu.close();
                regionCurveBox = JSON.parse(canvasHandler.getRegionPolygon())
                //console.log("FrontSketchFunctions: regionCurveBox: " + regionCurveBox)
            } else {
                myCanvas.state = "SKETCH";
            }
            myCanvas.requestPaint()
        }

        onSurfaceMapUpdated: {
            updateVizData(colorMap, visibilityMap);
            myCanvas.requestPaint();
        }

        onRegionMapUpdated: {
            regionColor = regionColorMap;
            regionVisibility = regionVisibilityMap;
            ////console.log("onRegionMapUpdated: " + regionColor + ", visibility:" + regionVisibility)
            myCanvas.state = "REGION";
            myCanvas.requestPaint();
            
        }

        onDomainMapUpdated: {
            var domainData = JSON.parse(domainMap);
            domain_map = domainData.domain_map;
            myCanvas.requestPaint();
        }

        onRefreshSubmittedCurvesAfterUndoRedo: {
            submittedCurves = canvasHandler.getSubmittedCurves(CANVAS_ID.FRONT_VIEW);
            colorMap = colorMap;
            myCanvas.requestPaint();
        }

        onPlaneOrientationChanged: {
            submittedCurves = canvasHandler.getSubmittedCurves(CANVAS_ID.FRONT_VIEW);
            if(newVizHandler.getVizFlag()) {
                regionCurveBox = JSON.parse(canvasHandler.getRegionPolygon());
            }
            if(model3DHandler.isPreserveRegionActive()) {
                canvasHandler.updateRegionBoundary();
                selectedRegion = WindowScript.parseSketchData(canvasHandler.returnRegion());
            }
            myCanvas.requestPaint();
        }

        onPreviewTurnedOff: {
            newVizHandler.clearSurfacePreview();
        }

        onEnableUndoRedoButtons : {
            timer2.setTimeout(function(){
                progressNotifierPathGuided.close();
            }, 100);
        }

        onCompletedPreview: {
            timer2.setTimeout(function(){
                progressNotifierPathGuided.close();
            }, 100);
        }

        onCrossSectionImageLoaded: {
            if (imagePath != "") {
                newVizHandler.enableImageRemoveButton(directionId);
            }
            myCanvas.requestPaint()
        }

        onImageRemovalCompleted: {            
            myCanvas.requestPaint();
        }

        onModelLoadingCompleted: {
            updateScale();
            updateResize();
            myCanvas.requestPaint();
        }

        onMeshProcessingCompleted:{
            timer2.setTimeout(function(){
                progressNotifierPathGuided.close();
            }, 100);
        }
    }//end: Connections VisualizationHandler
}
