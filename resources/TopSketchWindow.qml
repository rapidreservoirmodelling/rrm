
import QtQuick 2.12
import QtQuick.Controls 2.12

import "scripts/window.js" as WindowScript
import "scripts/promise.js" as Q

import Constants 1.0

Item {
    anchors.fill: parent
    anchors.centerIn: parent

    Rectangle {
        id: root
        anchors.fill: parent
        clip: true
        color: "white"//"gray"

        ScrollView {
            id: view
            anchors.fill: parent

            //anchors.leftMargin: screenDpi.getDiP(10)
            //anchors.topMargin: screenDpi.getDiP(10)
            anchors.bottomMargin: screenDpi.getDiP(5)
            anchors.rightMargin: screenDpi.getDiP(5)

            contentWidth: myCanvas.width //* myFunctions.canvasZoomFactor
            contentHeight: myCanvas.height //* myFunctions.canvasZoomFactor

            ScrollBar.vertical: ScrollBar {
                id: scrollBarV
                parent: view.parent
                policy: ScrollBar.AlwaysOn
                anchors.top: view.top
                anchors.right: view.right
                anchors.bottom: view.bottom
                height: view.availableHeight
                opacity: 0.5

                contentItem: Rectangle {
                    implicitWidth: 10
                    implicitHeight: 10
                    anchors.rightMargin: 10

                    radius: 16
                    color: "#74d0f0"
                }
            }

            ScrollBar.horizontal: ScrollBar {
                id: scrollBarH
                parent: view.parent
                policy: ScrollBar.AlwaysOn
                anchors.right: view.right
                anchors.left: view.left
                anchors.bottom: view.bottom
                width: view.availableWidth
                opacity: 0.5

                contentItem: Rectangle {
                    implicitWidth: 10
                    implicitHeight: 10
                    anchors.bottomMargin: 10

                    radius: 16
                    color: "#74d0f0"
                }
            }

            Canvas {
                id:myCanvas
                anchors.centerIn: parent
                width: root.width * 1.1 * myFunctions.canvasZoomFactor
                height: root.height * 1.1 * myFunctions.canvasZoomFactor
                //scale: myFunctions.canvasZoomFactor

                property var ctx

                state: "SKETCH"

                states: [
                    State {
                        name: "SKETCH"
                        PropertyChanges { target: topViewDrawer; visible: true }
                    },
                    State {
                        name: "REGION"
                        PropertyChanges { target: topViewDrawer; visible: false }
                    }
                ]


                TopSketchFunctions {
                    id: myFunctions
                }


                Component.onCompleted: {
                    myFunctions.updateScale();
                    myFunctions.updateResize();
                    myCanvas.requestPaint();
                    receiver.setCanvasAction(ACTION.FB_SKETCH);
                }


                onCanvasSizeChanged: {
                    myFunctions.updateResize();
                    if(myFunctions.guidelines && !myFunctions.glMoving)
                    {
                        myFunctions.glMouseX = canvasHandler.getMV_gl_mouseX();
                        myFunctions.glMouseY = canvasHandler.getMV_gl_mouseY();
                    }
                    myCanvas.requestPaint();
                }


                //- Border of the Canvas
                Rectangle {
                    anchors.fill: parent
                    color: "white"
                    border.color: "white" //"orange"
                    border.width: 3
                    z:-10
                }


                CrossSection_Mapview {
                    m_origin_x: canvasHandler.csMap_Origin_x
                    m_origin_y: canvasHandler.csMap_Origin_y
                    m_width: myFunctions.csMap_Width
                    m_height: myFunctions.csMap_Height
                    m_scale: canvasHandler.csMap_Scale
                    visible: true
                    z: -1
                }


                onPaint: {
                    myCanvas.ctx = getContext('2d');
                    WindowScript.clearCanvas( myCanvas.ctx, myCanvas.width, myCanvas.height );

                    if (canvasHandler.direction === DIRECTION.HEIGHT ) {
                        //WindowScript.displayCurrentSketches( myFunctions.curves, myCanvas.ctx, canvasHandler.highlightIndex, canvasHandler.selectIndex );
                        WindowScript.displayCurrentSketches( myFunctions.curves, myCanvas.ctx, canvasHandler.highlight_MV, canvasHandler.selectIndex );

                        WindowScript.draw( myCanvas.ctx, myFunctions.temp_curve );

                        WindowScript.displayCurrentContours( myFunctions.currentCurves, myCanvas.ctx );
                        WindowScript.showCrossSectionHeight( myCanvas.ctx, canvasHandler.csMap_Origin_y + myFunctions.csMap_Height, canvasHandler.csMap_Origin_x, myFunctions.csMap_Width, myFunctions.csHeightPosition );

                        if(myFunctions.guidelines) {
                            WindowScript.showGuidelines( myCanvas.ctx, myFunctions.glMouseX, myFunctions.glMouseY, myCanvas.width, myCanvas.height, CANVAS_ID.MAP_VIEW );
                        }

                        if(myFunctions.sketchLine) {
                            WindowScript.showSketchLine( myCanvas.ctx, myFunctions.glMouseY, myCanvas.width);
                        }

                        if(myFunctions.sketchCircle) {
                            WindowScript.showSketchCircle( myCanvas.ctx, myFunctions.glMouseX, myFunctions.glMouseY, myFunctions.radiusX);
                        }

                        if(myFunctions.sketchEllipse) {
                            WindowScript.showSketchEllipse( myCanvas.ctx, myFunctions.glMouseX, myFunctions.glMouseY, myFunctions.radiusX, myFunctions.radiusY);
                        }
                    } else {
                        if(canvasHandler.highlight_TR > -1 || myFunctions.selectPath)
                            WindowScript.drawWithHighlight( myCanvas.ctx, myFunctions.path);
                        else
                            WindowScript.draw( myCanvas.ctx, myFunctions.path );

                        WindowScript.draw( myCanvas.ctx, myFunctions.temp_path );

                        if (canvasHandler.direction === DIRECTION.WIDTH ) {
                            if(myFunctions.guidelines) {
                                WindowScript.showGuidelinesNS( myCanvas.ctx, myFunctions.glMouseY, myCanvas.width);
                            }
                            WindowScript.showCrossSectionGuidelineNS( myCanvas.ctx, myFunctions.glWidth, canvasHandler.csMap_Origin_y, myFunctions.csMap_Height, myFunctions.csIDPosition );
                        } else if (canvasHandler.direction === DIRECTION.LENGTH ) {
                            if(myFunctions.guidelines) {
                                WindowScript.showGuidelinesWE( myCanvas.ctx, myFunctions.glMouseX, myCanvas.height);
                            }
                            WindowScript.showCrossSectionGuidelineWE( myCanvas.ctx, myFunctions.glHeight, canvasHandler.csMap_Origin_x, myFunctions.csMap_Width, myFunctions.csIDPosition );
                        }
                    }

                    WindowScript.displaySubmittedCurvesUsingObjectMapping(myFunctions.submittedPaths, myCanvas.ctx, myFunctions.colorMap, myFunctions.visibilityMap, false );
                    WindowScript.displaySubmittedCurvesUsingObjectMapping(myFunctions.submittedCurves, myCanvas.ctx, myFunctions.colorMap, myFunctions.visibilityMap, false );
                }


                PinchArea {
                    id: myPinch
                    anchors.fill: parent
                    pinch.target: myCanvas
                    enabled: false
                    pinch.maximumScale: 10.0
                    pinch.minimumScale: 1.0
                    pinch.maximumRotation: 0
                    pinch.minimumRotation: 0
                    pinch.dragAxis: Pinch.XAndYAxis

                    MouseArea {
                        id: myMouseArea
                        anchors.fill: parent
                        hoverEnabled: true
                        pressAndHoldInterval: 500 //reducing the interval from the default 800ms
                        preventStealing: true

                        acceptedButtons: Qt.AllButtons
                        property int mouseButtonIdentifier: 0


                        onClicked: {
                            if(myCanvas.state === "SKETCH") {
                                if(canvasHandler.canvasAction === ACTION.FB_FIX_GUIDELINES) {
                                    myFunctions.glMoving = false;
                                    myFunctions.glMouseX = mouseX;
                                    myFunctions.glMouseY = mouseY;
                                    canvasHandler.fixGuidelineMapView(mouseX,mouseY);
                                    receiver.setMouseFromMapView( mouseX, mouseY, myFunctions.csMap_Width, myFunctions.csMap_Height, canvasHandler.csMap_Origin_x, canvasHandler.csMap_Origin_y );
                                    receiver.fixFrontViewGuidelines();
                                    myCanvas.requestPaint();
                                    receiver.setLastCanvasAction();
                                }

                                if (mouse.button === Qt.LeftButton) {
                                    canvasHandler.delegateMapViewAction(MOUSE.LEFT, MOUSE.CLICK, mouseX, mouseY, myFunctions.radiusX, myFunctions.radiusY);
                                }
                            } else if(myCanvas.state === "REGION") {
                                if (mouse.button === Qt.LeftButton) {
                                    canvasHandler.delegateRegionAction(CANVAS_ID.MAP_VIEW, MOUSE.LEFT, MOUSE.CLICK, mouseX, mouseY);
                                }
                            }
                        }


                        onPressed: {
                            if(myCanvas.state === "SKETCH") {
                                if (mouse.button === Qt.LeftButton) {
                                    canvasHandler.delegateMapViewAction(MOUSE.LEFT, MOUSE.PRESS, mouseX, mouseY, myFunctions.radiusX, myFunctions.radiusY);
                                    mouseButtonIdentifier = MOUSE.LEFT;
                                }
                                else if (mouse.button === Qt.RightButton) {
                                    mouseButtonIdentifier = MOUSE.RIGHT;
                                }
                            }
                        }


                        onPositionChanged: {
                            if(myCanvas.state === "SKETCH") {
                                if( mouseButtonIdentifier === MOUSE.LEFT  && myMouseArea.pressed) {
                                    canvasHandler.delegateMapViewAction(MOUSE.LEFT, MOUSE.MOVE, mouseX, mouseY, myFunctions.radiusX, myFunctions.radiusY);
                                    myCanvas.requestPaint();
                                }

                                if( myFunctions.guidelines  && myFunctions.glMoving ) {
                                    myFunctions.glMouseX = mouseX;
                                    myFunctions.glMouseY = mouseY;
                                    canvasHandler.fixGuidelineMapView(mouseX,mouseY);
                                    receiver.setMouseFromMapView( mouseX, mouseY, myFunctions.csMap_Width, myFunctions.csMap_Height, canvasHandler.csMap_Origin_x, canvasHandler.csMap_Origin_y );
                                    myCanvas.requestPaint();
                                }

                                if( myFunctions.sketchLine ) {
                                    myFunctions.glMouseY = mouseY;
                                    myCanvas.requestPaint();
                                }

                                if( myFunctions.sketchCircle || myFunctions.sketchEllipse) {
                                    myFunctions.glMouseX = mouseX;
                                    myFunctions.glMouseY = mouseY;
                                    myCanvas.requestPaint();
                                }

                                if( mouseButtonIdentifier === MOUSE.NO_BUTTON ) {
                                    canvasHandler.delegateMapViewAction(MOUSE.NO_BUTTON, MOUSE.MOVE, mouseX, mouseY, myFunctions.radiusX, myFunctions.radiusY);
                                }
                            }
                        }


                        onReleased: {
                            if(myCanvas.state === "SKETCH") {
                                if( mouseButtonIdentifier === MOUSE.LEFT ) {
                                    canvasHandler.delegateMapViewAction(MOUSE.LEFT, MOUSE.RELEASE, mouseX, mouseY, myFunctions.radiusX, myFunctions.radiusY);
                                    myFunctions.temp_curve = [];
                                    myFunctions.temp_path = [];
                                    mouseButtonIdentifier = MOUSE.NO_BUTTON;
                                    canvasHandler.resetHighlightSelection_MV();
                                    if (canvasHandler.direction === DIRECTION.HEIGHT ) {
                                        canvasHandler.updateCurrentContours();
                                        myFunctions.currentCurves = canvasHandler.returnCurrentContours();
                                    }
                                    receiver.updatePreview();
                                    myCanvas.requestPaint();
                                }
                            }
                        }


                        onDoubleClicked: {
                            if(myCanvas.state === "SKETCH") {
                                if( mouseButtonIdentifier === MOUSE.RIGHT ) {
                                    myFunctions.selectSketch = false;
                                    canvasHandler.resetHighlightSelection_MV();
                                    receiver.sendSurfaceCreated(CANVAS_ID.MAP_VIEW);
                                }
                            }
                        }

                        onWheel: {
                            if (wheel.angleDelta.y > 0)
                            {
                                myFunctions.canvasZoomFactor += 0.1
                            } else {
                                myFunctions.canvasZoomFactor -= 0.1
                                if (myFunctions.canvasZoomFactor < 1.00)
                                    myFunctions.canvasZoomFactor = 1.00
                            }
                            myFunctions.updateScale();
                            myFunctions.updateResize();
                            myCanvas.requestPaint()
                        }

                        TapHandler {
                            acceptedDevices: PointerDevice.TouchScreen |  PointerDevice.Stylus;

                            onSingleTapped:{
                                if(myCanvas.state === "SKETCH") {
                                    myFunctions.selectSketch = false;
                                    canvasHandler.resetHighlightSelection_MV();
                                    receiver.sendSurfaceCreated(CANVAS_ID.MAP_VIEW);
                                }
                            }

                            onDoubleTapped:{
                                if(myCanvas.state === "SKETCH") {
                                    receiver.sendSurfaceCreated(CANVAS_ID.MAP_VIEW);
                                }
                            }

                        }//end: TapHandler

                    }//end: MouseArea

                }//end: PinchArea

            }//end: Canvas

        }//end: ScrollView

    }//end: Rectangle

}//end: Item
