
import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

import TrajectoryModel 1.0
import Constants 1.0

Rectangle {
    anchors.fill: parent
    //color: "orange"

    Keys.onReleased: {
        if(event.key === Qt.Key_Back)
        {
            myStackView.pop();
            event.accepted = true;
        }
    }

    Connections {
        target: canvasHandler

        onUpdateTrajectoryTemplatesList: {
            ////console.log("UPDATE");
        }
    }

    ColumnLayout {
        spacing: 10

        Label {
            text: qsTr("Trajectory Templates:")
            font.pixelSize: 28
            Layout.topMargin: 20
            Layout.leftMargin: 20
        }

        Frame {
            Layout.fillWidth: true
            Layout.margins: 10

            ListView {
                implicitWidth: topViewDrawer.width - 50
                implicitHeight: topViewDrawer.height - 100 - button.height
                clip: true
                anchors.fill: parent

                model: TrajectoryModel {
                    list: trajectoryList
                }

                ButtonGroup {
                    id: btGroup
                }

                delegate: RowLayout {
                    width: parent.width
                    RadioDelegate {
                        ButtonGroup.group: btGroup
                        checked: model.selected
                        onClicked: model.selected = checked
                    }
                    TextField {
                        Layout.fillWidth: true
                        text: model.description
                        font.pixelSize: 24
                        onEditingFinished: model.description = text
                    }
                }
            }//end: ListView
        }//end: Frame


        RowLayout {
            id: myRowLayout
            Layout.margins: 10

            Button {
                text: qsTr("Add new item")
                Layout.fillWidth: true
                onClicked: trajectoryList.appendItem()
            }
            Button {
                text: qsTr("Remove completed")
                Layout.fillWidth: true
                onClicked: trajectoryList.removeCompletedItems()
            }
        }//end: RowLayout

    }//end: ColumnLayout


    Button {
        id: button
        width: parent.width
        height: 50
        //text: title
        icon.source: "/images/icons/icon-go-back.png"
        anchors.bottom: parent.bottom

        contentItem: Item {
            Image {
                source: button.icon.source
                fillMode: Image.PreserveAspectFit // For not stretching image (optional)
                height: parent.height
                anchors.left: parent.left
            }
        }

        onClicked: myStackView.pop()
    }
}
