import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Rectangle {
    id: notesForm

    property int parentType: 0

    property alias itemNameText: nameTextField.text
    property alias itemDescription: descriptionTextArea.text
    property alias itemColor: colorRectangle.color
    property alias parent_type_id: notesForm.parentType

    property bool isSingleItemForm: true    
    property var notificationText: ""

    property bool isWarningOn: false
    property alias warningMessage: warningMessage.text

    width: Math.max(screenDpi.getDiP(OBJECT_TREE.NOTES_FORM_WIDTH), implicitWidth)
    height: Math.max(screenDpi.getDiP(OBJECT_TREE.NOTES_FORM_HEIGHT), implicitHeight)
    
    signal infoChanged();

    ColumnLayout {
        anchors.margins: screenDpi.getDiP(12)
        anchors.fill: parent

        RowLayout {
            Layout.alignment: Qt.AlignTop | Qt.AlignLeft
            spacing: screenDpi.getDiP(20)

            Label {
                text: "Name:"
                font.pixelSize: screenDpi.getDiP(24)
                font.bold: true                
            }

            TextField {
                id: nameTextField
                text: ""
                validator: RegExpValidator { regExp: /[A-Za-z0-9]+/ }
                onTextChanged: acceptableInput ? infoChanged() : print("Input not acceptable")
                font.pixelSize: screenDpi.getDiP(20)
            }

            Rectangle {
                id: colorRectangle
                color: "transparent"
                width: screenDpi.getDiP(50)
                height: screenDpi.getDiP(50)
                radius: width * 0.5

                border.color: Material.Indigo
                border.width: screenDpi.getDiP(2)

                visible: (notesForm.isSingleItemForm == true) ? true : false
            }
        }

        RowLayout {
            id: warningMessagePlace
            visible: isWarningOn
            spacing: screenDpi.getDiP(20)
            Rectangle {
                width: screenDpi.getDiP(20)
                height: screenDpi.getDiP(20)
                radius: width * 0.5
                color: "#990e0e"
                Text {
                    text: qsTr("!")
                    font.pixelSize: screenDpi.getDiP(16)
                    font.bold: true
                    color: "white"
                    anchors.centerIn: parent
                }
            }
            Label {
                id: warningMessage
                text: qsTr("Name already exists, please chose another one.")
                color: "#990e0e"
                font.pixelSize: screenDpi.getDiP(16)
                font.bold: true
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignTop | Qt.AlignLeft
            spacing: screenDpi.getDiP(20)

            Label {
                text: "Description:"
                font.pixelSize: screenDpi.getDiP(24)
                font.bold: true
                Layout.bottomMargin: screenDpi.getDiP(10)
                visible: (notesForm.parentType ===6 || notesForm.parentType ===1)? false: true
            }

            Flickable {
                id: flickable
                Layout.fillWidth: true
                Layout.fillHeight: true
                height: screenDpi.getDiP(OBJECT_TREE.NOTES_FORM_TEXTAREA_HEIGHT)
                flickableDirection: Flickable.VerticalFlick
                visible: (notesForm.parentType ===6 || notesForm.parentType ===1)? false: true

                TextArea.flickable: TextArea {
                    id: descriptionTextArea
                    wrapMode: TextArea.Wrap
                    leftPadding: 6
                    rightPadding: 6
                    topPadding: 0
                    bottomPadding: 0
                    background: Rectangle{
                        color: OBJECT_TREE.NOTES_FORM_TEXTAREA_COLOR
                    }
                    font.pixelSize: screenDpi.getDiP(20)

                    onTextChanged: {
                        var pos = descriptionTextArea.positionAt(1, flickable.height + 1);
                        if(descriptionTextArea.length >= pos)
                        {
                            descriptionTextArea.remove(pos, descriptionTextArea.length);
                        }
                        infoChanged();
                    }
                }

                ScrollBar.vertical: ScrollBar {}
            }
        }
    }
}
