import QtQuick 2.0
//import QtQuick.Window 2.0
import QtQuick.Controls 1.4

import QmlWidget 1.0
import Constants 1.0

ApplicationWindow {
    id: fdWindow
    title: "Flow Diagnostic"

    minimumWidth: screenDpi.getDiP(vizHandler.getFdWindowWidth())//1024
    minimumHeight: screenDpi.getDiP(vizHandler.getFdWindowHeight())//700
    
    flags:  Qt.Window | Qt.WindowSystemMenuHint
            | Qt.WindowTitleHint | Qt.WindowMinimizeButtonHint
            | Qt.WindowMaximizeButtonHint | Qt.WindowStaysOnTopHint | Qt.CustomizeWindowHint | WindowCloseButtonHint


    visible: true
    modality: Qt.NonModal 

    Rectangle {
        color: "lightskyblue"
        anchors.fill: parent
        
        z:14

        QmlWidget {
            anchors.fill: parent
        }
    }
    onClosing:{
        if(COMMENTS.DEV_FAZILA) {
            ////console.log("Closing FD")
        }
        vizHandler.toggleFdFlag();
    }

}
