import QtQuick 2.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.12
import QtQuick.Controls.Material 2.12

import Constants 1.0

Button {
    id: button

    property bool isActive: false
    property bool isBordered: true
    property bool hasBackground: false
    
    property string colorDown: "#74d0f0"
    property string colorUp: "#707070"
    property string colorBackground: "white" //"#EEEEEE"

    property alias myBackground : rect
    
    property string buttonText : ""

    display: AbstractButton.TextBesideIcon 

    icon.color: enabled ? BT_FILEMENU.ICON_COLOR_UP : BT_FILEMENU.ICON_COLOR_DISABLE
    //icon.source: "images/button.png" 

    icon.width: screenDpi.getDiP(BT_FILEMENU.WIDTH) 
    icon.height: screenDpi.getDiP(BT_FILEMENU.HEIGHT) 
    
    text: buttonText
    Material.foreground: Material.Teal

    background: Rectangle {
        id: rect
        implicitWidth: 2 * screenDpi.getDiP(BT_FILEMENU.WIDTH) 
        implicitHeight: 1.5 * screenDpi.getDiP(BT_FILEMENU.HEIGHT) 
        
        color: isActive ? BT_FILEMENU.BACKGROUND_COLOR_DOWN : (hasBackground ? BT_FILEMENU.BACKGROUND_COLOR_UP : ( button.down ? BT_FILEMENU.BACKGROUND_COLOR_DOWN : BT_FILEMENU.BACKGROUND_COLOR_UP))
        border.color: isBordered ? ( enabled ? BT_FILEMENU.BORDER_COLOR_ENABLE : BT_FILEMENU.BORDER_COLOR_DISABLE ) : "transparent" 
        border.width: button.down ? screenDpi.getDiP(BT_FILEMENU.BORDER_WIDTH_ACTIVE) : screenDpi.getDiP(BT_FILEMENU.BORDER_WIDTH)
        radius: screenDpi.getDiP(BT_FILEMENU.BORDER_RADIUS)
        
        layer.enabled: true
            layer.effect: DropShadow {
                transparentBorder: true
                horizontalOffset: screenDpi.getDiP(BT_FILEMENU.DROPSHADOW_HORIZONTAL_OFFSET)
                verticalOffset: screenDpi.getDiP(BT_FILEMENU.DROPSHADOW_VERTICAL_OFFSET)
            }
    }

}

