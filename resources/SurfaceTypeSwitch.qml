import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

RowLayout {
    id: root

    function reset() {
        stratigraphyButton.isActive = true;
        structureButton.isActive = false;
        canvasHandler.setSurfaceType(0);
    }

    ButtonToolBar {
        id: stratigraphyButton
        isBordered: true
        text: qsTr("Stratigraphy")
        //implicitWidth: screenDpi.getDiP(200)
        myToolTipText.text: "Sketch Stratigraphy"
        isActive: true
        hasIcon: true
        icon.source: "/images/icons/strat.png"

        onClicked: {
            if(!stratigraphyButton.isActive) {
                stratigraphyButton.isActive = true;
                structureButton.isActive = false;
                canvasHandler.setSurfaceType(0);
            }
        }
    }

    ButtonToolBar {
        id: structureButton
        isBordered: true
        text: qsTr("Structure")
        //implicitWidth: screenDpi.getDiP(200)
        myToolTipText.text: "Sketch Structure"
        hasIcon: true
        icon.source: "/images/icons/struct.png"

        onClicked: {
            if(!structureButton.isActive) {
                structureButton.isActive = true;
                stratigraphyButton.isActive = false;
                canvasHandler.setSurfaceType(1);
            }
        }
    }

    Connections {
        target: canvasHandler

        onResetNew: {
            root.reset();
        }
    }
}
