import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import Qt.labs.platform 1.1

ToolBar {

    anchors.topMargin: 15

    RowLayout {
        anchors.fill: parent
        anchors.centerIn: parent
        anchors.topMargin: 55
        ToolButton {
            id: button20
            text: qsTr("SR")
            onClicked: {
                //receiver.setEvent(20)
                //drawer.close()
            }
        }
        ToolButton {
            text: qsTr("Sketch Operators")
        }

        ToolButton {
            text: qsTr("Rules Operators")
        }

        ToolSeparator {
            orientation: Qt.Vertical
        }

        ToolButton {
            text: qsTr("RA")
        }

        ToolButton {
            text: qsTr("RAI")
        }
        ToolSeparator {
            orientation: Qt.Vertical
        }

        ToolButton {
            text: qsTr("RB")
        }

        ToolButton {
            text: qsTr("RBI")
        }
        ToolSeparator {
            orientation: Qt.Vertical
        }



        /*Item {
            Layout.fillWidth: true
        }*/
    }
}

/*
//Spinnable wheel of items that can be selected
Tumbler {
    id: control
    model: 15

    background: Item {
        Rectangle {
            opacity: control.enabled ? 0.2 : 0.1
            border.color: "#000000"
            width: parent.width
            height: 1
            anchors.top: parent.top
        }

        Rectangle {
            opacity: control.enabled ? 0.2 : 0.1
            border.color: "#000000"
            width: parent.width
            height: 1
            anchors.bottom: parent.bottom
        }
    }

    delegate: Text {
        text: qsTr("Item %1").arg(modelData + 1)
        font: control.font
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        opacity: 1.0 - Math.abs(Tumbler.displacement) / (control.visibleItemCount / 2)
    }

    Rectangle {
        anchors.horizontalCenter: control.horizontalCenter
        y: control.height * 0.4
        width: 40
        height: 1
        color: "#21be2b"
    }

    Rectangle {
        anchors.horizontalCenter: control.horizontalCenter
        y: control.height * 0.6
        width: 40
        height: 1
        color: "#21be2b"
    }
}*/
/*Pane {
    id: pane

    Column {
        spacing: 40
        width: parent.width


        Button {
            text: "Close"
            anchors.horizontalCenter: parent.horizontalCenter
            width: Math.max(implicitWidth, Math.min(implicitWidth * 2, pane.availableWidth / 3))

            onClicked: drawer.close()
        }
        Rectangle {
            Text {
                id: drawerLabel
                text: qsTr("Navigation Drawer")
            }
        }
    }


}*/
/*
Rectangle{
    Column{
        spacing: 5
        Label{
            text:"Menu item 1"
        }
        Label{
            text:"Menu item 2"
        }
        Label{
            text:"Menu item 3"
        }
        Label{
            text:"Menu item 4"
        }
    }
}*/
