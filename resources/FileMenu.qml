import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
//import Qt.labs.platform 1.1
import "scripts/db.js" as ImageScript

import Constants 1.0

Rectangle {
    id: root
    x: parent.x + screenDpi.getDiP(20)
    y: parent.y + screenDpi.getDiP(100)
    
    Timer {
        id: timer

        function setTimeout(cb, delayTime) {
            timer.interval = delayTime;
            timer.repeat = false;
            timer.triggered.connect(cb);
            timer.triggered.connect(function release () {
                timer.triggered.disconnect(cb);
                timer.triggered.disconnect(release);
            });
            timer.start();
        }
    }

    ColumnLayout {
        id: fileMenuItem
        anchors.fill: parent
        spacing: 1

        property int hintSize: 18
        property string msg: ""

        //Button New
        ButtonMenuItem {
            id: buttonNew
            buttonText: "New"
            icon.source: "/images/icons/file-new.png"
            
            onClicked: {
                buttonNew.highlighted = true
                buttonLoad.highlighted = false
                buttonSave.highlighted = false
                buttonExport.highlighted = false
                buttonAbout.highlighted = false

                model3DHandler.resetNew();
                receiver.reset();
            }
        }

        //Button Load
        ButtonMenuItem {
            id: buttonLoad
            buttonText: "Load"
            icon.source: "/images/icons/file-open.png"
            
            onClicked: {
                buttonNew.highlighted = false
                buttonLoad.highlighted = true
                buttonSave.highlighted = false
                buttonExport.highlighted = false
                buttonAbout.highlighted = false

                newVizHandler.showOpenFileDialog = true
            }
        }

        //Button Save
        ButtonMenuItem {
            id: buttonSave
            buttonText: "Save"
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            icon.source: "/images/icons/file-save.png"
            
            onClicked: {
                buttonNew.highlighted = false
                buttonLoad.highlighted = false
                buttonSave.highlighted = true
                buttonExport.highlighted = false
                buttonAbout.highlighted = false

                newVizHandler.showSaveFileDialog = true
            }
        }

        ButtonMenuItem {
            id: buttonExport
            text: "Export"
            buttonText: "Export"
            icon.source: "/images/icons/file-export.png"
            
            onClicked: {
                buttonNew.highlighted = false
                buttonLoad.highlighted = false
                buttonSave.highlighted = false
                buttonExport.highlighted = true
                buttonAbout.highlighted = false
                
                if(newVizHandler.exportToIRAPgrid()) {
                    overlay.loadingNotifier.message = "Success :)"
                } else {
                    overlay.loadingNotifier.message = "Failed!"
                }

                //Display popup notification
                overlay.loadingNotifier.open();

                //Close popup notification
                timer.setTimeout(function(){
                    overlay.loadingNotifier.close();
                    buttonExport.highlighted = false
                }, 1000);
            }
        }

        MessageDialog {
                    id: messageDialog
                    title: "About"
                    icon: StandardIcon.NoIcon        
                    
                    
                    property var isInit: false

                    text: qsTr('
                    <style>
                      p {
                        margin: 0;
                        padding: 0;
                      }
            
                      p.author {
                        text-indent: %1;
                      }
                    </style>

                    <p>Rapid Reservoir Modelling Phase 2 Prototype</p>
                    <br/>
                    <center>
                      <p><a href="https://rapidreservoir.org/">Website</a></p>
                      <p><a href="https://bitbucket.org/rapidreservoirmodelling/rrm/">Source code</a></p>
                      <p>commit <a href="https://bitbucket.org/rapidreservoirmodelling/rrm/commits/%1">%2</a></p>
                    </center>
                    <br/>
                    

                    <p>University of Calgary, Canada</p>
                    <p class="author">Julio Machado Silva, Sicilia Judice, Fazilatur Rahman, Mario Costa Sousa</p>                    
                    <br/>
                    <p>Imperial College London, United Kingdom</p>
                    <p class="author">Carl Jacquemyn, Matthew Jackson, Gary Hampson</p>
                    <br/>
                    <p>Delft University of Technology, Netherlands</p>
                    <p>Heriot-Watt University, United Kingdom</p>
                    <p class="author">Dmytro Petrovskyy, Sebastian Geiger</p>




                    
                    ')
                    onAccepted: {
                        //console.log("And of course you could only agree.")
                        //Qt.quit()
                    }
                 //   Component.onCompleted: visible = true
                }

        ButtonMenuItem {
            id: buttonAbout
            buttonText: "About"
            // icon.source: "/images/icons/file-export.png"
            
            onClicked: {
                buttonNew.highlighted = false
                buttonLoad.highlighted = false
                buttonSave.highlighted = false
                buttonExport.highlighted = false
                buttonAbout.highlighted = true


                if (!messageDialog.isInit) {
                  let hash = newVizHandler.getCurrentGitHash()                
                  messageDialog.text = messageDialog.text.arg(hash).arg(hash.substring(0, 7))
                  messageDialog.isInit = true
                }

                messageDialog.open()



                // if(newVizHandler.exportToIRAPgrid()) {
                //     overlay.loadingNotifier.message = "Success :)"
                // } else {
                //     overlay.loadingNotifier.message = "Failed!"
                // }

                // //Display popup notification
                // overlay.loadingNotifier.open();

                // //Close popup notification
                // timer.setTimeout(function(){
                //     overlay.loadingNotifier.close();
                //     buttonExport.highlighted = false
                // }, 1000);
            }
        }
    }

    //For Opening
    FileDialog {
        id: theFileOpenDialog
        visible: newVizHandler.showOpenFileDialog
        title: "Open model"
        folder: shortcuts.documents
        nameFilters: ["Model files" + "(*.rrm6 )", "All files" + "(*)"]

        onAccepted: {
            newVizHandler.showOpenFileDialog = false;
            buttonLoad.highlighted = false
            overlay.loadingNotifier.message = "Loading ..."
            //Display popup notification
            overlay.loadingNotifier.message = "Loading file ...";
            overlay.loadingNotifier.open();
            
            timer.setTimeout(function() {
                //console.log("Load file path: " + fileUrl)
                newVizHandler.loadModel(fileUrl);
                newVizHandler.loadImageDb(fileUrl);
                receiver.reset();
                //- This is to make sure the preserve region flag is set to false when a model is saved.
                model3DHandler.stopPreserveRegion();
            }, 100);
        }

        onRejected: {
            newVizHandler.showOpenFileDialog = false;
            buttonLoad.highlighted = false
        }
    }

    //For Save FileDialog
    FileDialog {
        id: theFileSaveDialog
        visible: newVizHandler.showSaveFileDialog
        title: "Save model"
        folder: shortcuts.documents
        nameFilters: ["Model files" + "(*.rrm6 )", "All files" + "(*)"]
        selectExisting: false

        onAccepted: {
            newVizHandler.showSaveFileDialog = false
            //console.log("Save file path: " + fileUrl)
            canvasHandler.saveSbimData(fileUrl);

            var path = newVizHandler.getLocalPath(fileUrl);
            //console.log("Save file path2: " + path)

            if(model3DHandler.saveFile(path)) {
                var imageData = ImageScript.readAllRows()
                //console.log("Save Image data: " + JSON.stringify(imageData) )
                if(imageData.length > 0) {
                    newVizHandler.saveImageDb(path, JSON.stringify(imageData))
                }
                overlay.loadingNotifier.message = "Success :)"
            } else {
                overlay.loadingNotifier.message = "Failed!";
            }

            //Display popup notification
            overlay.loadingNotifier.open();

            //Close popup notification
            timer.setTimeout(function(){
                overlay.loadingNotifier.close();
                buttonSave.highlighted = false
            }, 1000);
        }

        onRejected: {
            newVizHandler.showSaveFileDialog = false
        }
    }
    
    Connections {
        target: newVizHandler

        onModelLoadingCompleted:{
            //console.log("FileMEnu: Model Loading Complteted!")
            timer.setTimeout(function(){
                overlay.loadingNotifier.close();
                buttonLoad.highlighted = true
            }, 100);
        }
    }
}
